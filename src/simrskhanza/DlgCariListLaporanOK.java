package simrskhanza;

import kepegawaian.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.validasi;
import java.util.Date;
import fungsi.akses;
import fungsi.sekuel;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author dosen
 */
public final class DlgCariListLaporanOK extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private validasi Valid=new validasi();
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private PreparedStatement ps;
    private ResultSet rs;
    private File file;
    private FileWriter fileWriter;
    private String iyem;
    private ObjectMapper mapper = new ObjectMapper();
    private JsonNode root;
    private JsonNode response;
    private FileReader myObj;
    /** Creates new form DlgPenyakit
     * @param parent
     * @param modal */
    public DlgCariListLaporanOK(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(10,2);
        setSize(656,250);

        Object[] row={
            "no_rawat",
            "no_rkm_medis",
            "jk",
            "nm_pasien",
            "tanggal",
            "jam",
            "jenis_anasthesi",
            "kategori",
            "kd_operator1",
            "nm_operator1",
            "kd_operator2",
            "nm_operator2",
            "kd_dranak",
            "nm_dranak",
            "kd_dranestesi",
            "nm_dranestesi",
            "kd_drpjanak",
            "nm_pjdranak",
            "kd_drumum",
            "nm_drumum",
            "kd_asis1",
            "nm_asis1",
            "kd_asis2",
            "nm_asis2",
            "kd_instrumen",
            "nm_instrumen",
            "kd_resusitas",
            "nm_resusitas",
            "kd_asan",
            "nm_asan",
            "kd_asan2",
            "nm_asan2",
            "kd_bidan",
            "nm_bidan",
            "kd_omloop",
            "nm_omloop"
            
        };
        tabMode=new DefaultTableModel(null,row){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbKamar.setModel(tabMode);
        //tbPenyakit.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbPenyakit.getBackground()));
        tbKamar.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbKamar.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 36; i++) {
            TableColumn column = tbKamar.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(120);
            }else if(i==1){
                column.setPreferredWidth(100);
            }else if(i==2){
                column.setPreferredWidth(50);
            }else if(i==3){
                column.setPreferredWidth(150);
            }else if(i==4){
                column.setPreferredWidth(100);
            }else if(i==5){
                column.setPreferredWidth(180);
            }else if(i==6){
                column.setPreferredWidth(180);
            }else if(i==7){
                column.setPreferredWidth(200);
            }else if(i==8){
                column.setPreferredWidth(200);
            }else if(i==9){
                column.setPreferredWidth(200);
            }else if(i==10){
                column.setPreferredWidth(200);
            }else if(i==11){
                column.setPreferredWidth(200);
            }else if(i==12){
                column.setPreferredWidth(200);
            }else if(i==13){
                column.setPreferredWidth(200);
            }else if(i==14){
                column.setPreferredWidth(200);
            }else if(i==15){
                column.setPreferredWidth(200);
            }else if(i==16){
                column.setPreferredWidth(200);
            }else if(i==17){
                column.setPreferredWidth(200);
            }else if(i==18){
                column.setPreferredWidth(200);
            }else if(i==19){
                column.setPreferredWidth(200);
            }else if(i==20){
                column.setPreferredWidth(200);
            }else if(i==21){
                column.setPreferredWidth(200);
            }else if(i==22){
                column.setPreferredWidth(200);
            }else if(i==23){
                column.setPreferredWidth(200);
            }else if(i==24){
                column.setPreferredWidth(200);
            }else if(i==25){
                column.setPreferredWidth(200);
            }else if(i==26){
                column.setPreferredWidth(200);
            }else if(i==27){
                column.setPreferredWidth(200);
            }else if(i==28){
                column.setPreferredWidth(200);
            }else if(i==29){
                column.setPreferredWidth(200);
            }else if(i==30){
                column.setPreferredWidth(200);
            }else if(i==31){
                column.setPreferredWidth(200);
            }else if(i==32){
                column.setPreferredWidth(200);
            }else if(i==33){
                column.setPreferredWidth(200);
            }else if(i==34){
                column.setPreferredWidth(200);
            }else if(i==35){
                column.setPreferredWidth(200);
            }
        }
        tbKamar.setDefaultRenderer(Object.class, new WarnaTable());
        TCari.setDocument(new batasInput((byte)100).getKata(TCari));
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        } 
        
        
    }
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbKamar = new widget.Table();
        panelisi3 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        label9 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        label10 = new widget.Label();
        LCount = new widget.Label();
        BtnKeluar = new widget.Button();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Laporan Oeprasi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);

        tbKamar.setAutoCreateRowSorter(true);
        tbKamar.setName("tbKamar"); // NOI18N
        tbKamar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbKamarMouseClicked(evt);
            }
        });
        tbKamar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbKamarKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbKamar);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        panelisi3.setName("panelisi3"); // NOI18N
        panelisi3.setPreferredSize(new java.awt.Dimension(100, 43));
        panelisi3.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        jLabel19.setText("Tgl.  :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi3.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "30-08-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(95, 23));
        panelisi3.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelisi3.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "30-08-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(95, 23));
        panelisi3.add(DTPCari2);

        label9.setText("Key Word :");
        label9.setName("label9"); // NOI18N
        label9.setPreferredSize(new java.awt.Dimension(68, 23));
        panelisi3.add(label9);

        TCari.setEditable(false);
        TCari.setMinimumSize(new java.awt.Dimension(44, 24));
        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(200, 23));
        TCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TCariActionPerformed(evt);
            }
        });
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelisi3.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('1');
        BtnCari.setToolTipText("Alt+1");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelisi3.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('2');
        BtnAll.setToolTipText("2Alt+2");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelisi3.add(BtnAll);

        label10.setText("Record :");
        label10.setName("label10"); // NOI18N
        label10.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi3.add(label10);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(50, 23));
        panelisi3.add(LCount);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('4');
        BtnKeluar.setToolTipText("Alt+4");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        panelisi3.add(BtnKeluar);

        internalFrame1.add(panelisi3, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
            tbKamar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnCari, TCari);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbKamarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbKamarMouseClicked
        if(tabMode.getRowCount()!=0){
            if(evt.getClickCount()==2){
                dispose();
            }
        }
}//GEN-LAST:event_tbKamarMouseClicked

    private void tbKamarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbKamarKeyPressed
        if(tabMode.getRowCount()!=0){
            if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                dispose();
            }else if(evt.getKeyCode()==KeyEvent.VK_SHIFT){
                TCari.setText("");
                TCari.requestFocus();
            }
        }
}//GEN-LAST:event_tbKamarKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
    }//GEN-LAST:event_BtnKeluarActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        emptTeks();
    }//GEN-LAST:event_formWindowActivated

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
      
                tampil();
        
    }//GEN-LAST:event_formWindowOpened

    private void TCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TCariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TCariActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            DlgCariListLaporanOK dialog = new DlgCariListLaporanOK(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnCari;
    private widget.Button BtnKeluar;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.Label LCount;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.InternalFrame internalFrame1;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label label10;
    private widget.Label label9;
    private widget.panelisi panelisi3;
    private widget.Table tbKamar;
    // End of variables declaration//GEN-END:variables

    private void tampil() {  
        Valid.tabelKosong(tabMode);
        try{
          if(TCari.getText().trim().equals("")){
            ps=koneksi.prepareStatement("select reg_periksa.no_rawat,reg_periksa.tgl_registrasi,reg_periksa.jam_reg,reg_periksa.no_rkm_medis,reg_periksa.kd_poli, "+
                    "pasien.nm_pasien,poliklinik.nm_poli,pasien.jk,operasi.*, "+
                    "o1.kd_dokter as kd_operator1,o1.nm_dokter as nm_operator1, "+
                    "o2.kd_dokter as kd_operator2,o2.nm_dokter as nm_operator2, "+
                    "da.kd_dokter as kd_dranak,da.nm_dokter as nm_dranak, "+
                    "das.kd_dokter as kd_dranestesi,das.nm_dokter as nm_dranestesi, "+
                    "dpjan.kd_dokter as kd_drpjanak,dpjan.nm_dokter as nm_pjdranak, "+
                    "du.kd_dokter as kd_drumum,du.nm_dokter as nm_drumum, "+
                    "as1.nip as kd_asis1,as1.nama as nm_asis1, "+
                    "as2.nip as kd_asis2,as2.nama as nm_asis2, "+
                    "ins.nip as kd_instrumen,ins.nama as nm_instrumen, "+
                    "res.nip as kd_resusitas,res.nama as nm_resusitas, "+
                    "asan.nip as kd_asan,asan.nama as nm_asan, "+
                    "asan2.nip as kd_asan2,asan2.nama as nm_asan2, "+
                    "bid.nip as kd_bidan,bid.nama as nm_bidan, "+
                    "oml.nip as kd_omloop,oml.nama as nm_omloop "+
                    "from operasi "+
                    "left join reg_periksa on reg_periksa.no_rawat = operasi.no_rawat "+
                    "left join pasien on pasien.no_rkm_medis=reg_periksa.no_rkm_medis "+
                    "left join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli "+ 
                    "inner join dokter as o1 on o1.kd_dokter=operasi.operator1 "+            
                    "inner join dokter as o2 on o2.kd_dokter=operasi.operator2 "+
                    "inner join dokter as da on da.kd_dokter=operasi.dokter_anak "+
                    "inner join dokter as das on das.kd_dokter=operasi.dokter_anestesi "+
                    "inner join dokter as dpjan on dpjan.kd_dokter=operasi.dokter_pjanak "+
                    "inner join dokter as du on du.kd_dokter=operasi.dokter_umum "+
                    "inner join petugas as as1 on as1.nip=operasi.asisten_operator1 "+
                    "inner join petugas as as2 on as2.nip=operasi.asisten_operator2 "+
                    "inner join petugas as ins on ins.nip=operasi.instrumen "+
                    "inner join petugas as res on res.nip=operasi.perawaat_resusitas "+
                    "inner join petugas as asan on asan.nip=operasi.asisten_anestesi "+
                    "inner join petugas as asan2 on asan2.nip=operasi.asisten_anestesi2 "+
                    "inner join petugas as bid on bid.nip=operasi.bidan "+
                    "inner join petugas as oml on oml.nip=operasi.omloop "+
                    "where reg_periksa.stts !='Batal' and operasi.tgl_operasi between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59"+"' order by reg_periksa.no_rawat asc");
            
          }else{
            ps=koneksi.prepareStatement("select reg_periksa.no_rawat,reg_periksa.tgl_registrasi,reg_periksa.jam_reg,reg_periksa.no_rkm_medis,reg_periksa.kd_poli, "+
                      "pasien.nm_pasien,poliklinik.nm_poli,pasien.jk,operasi.*, "+
                    "o1.kd_dokter as kd_operator1,o1.nm_dokter as nm_operator1, "+
                    "o2.kd_dokter as kd_operator2,o2.nm_dokter as nm_operator2, "+
                    "da.kd_dokter as kd_dranak,da.nm_dokter as nm_dranak, "+
                    "das.kd_dokter as kd_dranestesi,das.nm_dokter as nm_dranestesi, "+
                    "dpjan.kd_dokter as kd_drpjanak,dpjan.nm_dokter as nm_pjdranak, "+
                    "du.kd_dokter as kd_drumum,du.nm_dokter as nm_drumum, "+
                    "as1.nip as kd_asis1,as1.nama as nm_asis1, "+
                    "as2.nip as kd_asis2,as2.nama as nm_asis2, "+
                    "ins.nip as kd_instrumen,ins.nama as nm_instrumen, "+
                    "res.nip as kd_resusitas,res.nama as nm_resusitas, "+
                    "asan.nip as kd_asan,asan.nama as nm_asan, "+
                    "asan2.nip as kd_asan2,asan2.nama as nm_asan2, "+
                    "bid.nip as kd_bidan,bid.nama as nm_bidan, "+
                    "oml.nip as kd_omloop,oml.nama as nm_omloop "+
                    "from operasi "+
                    "left join reg_periksa on reg_periksa.no_rawat = operasi.no_rawat "+
                    "left join pasien on pasien.no_rkm_medis=reg_periksa.no_rkm_medis "+
                    "left join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli "+ 
                    "inner join dokter as o1 on o1.kd_dokter=operasi.operator1 "+            
                    "inner join dokter as o2 on o2.kd_dokter=operasi.operator2 "+
                    "inner join dokter as da on da.kd_dokter=operasi.dokter_anak "+
                    "inner join dokter as das on das.kd_dokter=operasi.dokter_anestesi "+
                    "inner join dokter as dpjan on dpjan.kd_dokter=operasi.dokter_pjanak "+
                    "inner join dokter as du on du.kd_dokter=operasi.dokter_umum "+
                    "inner join petugas as as1 on as1.nip=operasi.asisten_operator1 "+
                    "inner join petugas as as2 on as2.nip=operasi.asisten_operator2 "+
                    "inner join petugas as ins on ins.nip=operasi.instrumen "+
                    "inner join petugas as res on res.nip=operasi.perawaat_resusitas "+
                    "inner join petugas as asan on asan.nip=operasi.asisten_anestesi "+
                    "inner join petugas as asan2 on asan2.nip=operasi.asisten_anestesi2 "+
                    "inner join petugas as bid on bid.nip=operasi.bidan "+
                    "inner join petugas as oml on oml.nip=operasi.omloop "+
                    "where reg_periksa.stts !='Batal' and operasi.no_rawat=? and operasi.tgl_operasi between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59"+"' order by reg_periksa.no_rawat asc");  
          }
            try {
                ps.setString(1,TCari.getText());
                rs=ps.executeQuery();
                
                while(rs.next()){
                    tabMode.addRow(new Object[]{
                        rs.getString("no_rawat"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("jk"),
                        rs.getString("nm_pasien"),
                        rs.getString("tgl_operasi"),
                        rs.getString("tgl_operasi"),
                        rs.getString("jenis_anasthesi"),
                        rs.getString("kategori"),
                        rs.getString("kd_operator1"),
                        rs.getString("nm_operator1"),
                        rs.getString("kd_operator2"),
                        rs.getString("nm_operator2"),
                        rs.getString("kd_dranak"),
                        rs.getString("nm_dranak"),
                        rs.getString("kd_dranestesi"),
                        rs.getString("nm_dranestesi"),
                        rs.getString("kd_drpjanak"),
                        rs.getString("nm_pjdranak"),
                        rs.getString("kd_drumum"),
                        rs.getString("nm_drumum"),
                        rs.getString("kd_asis1"),
                        rs.getString("nm_asis1"),
                        rs.getString("kd_asis2"),
                        rs.getString("nm_asis2"),
                        rs.getString("kd_instrumen"),
                        rs.getString("nm_instrumen"),
                        rs.getString("kd_resusitas"),
                        rs.getString("nm_resusitas"),
                        rs.getString("kd_asan"),
                        rs.getString("nm_asan"),
                        rs.getString("kd_asan2"),
                        rs.getString("nm_asan2"),
                        rs.getString("kd_bidan"),
                        rs.getString("nm_bidan"),
                        rs.getString("kd_omloop"),
                        rs.getString("nm_omloop")
                    });
                    
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
          
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TCari.requestFocus();
    }

    public JTable getTable(){
        return tbKamar;
    }
    
    public void isCek(){        
      tampil();
    }
    public void settgl(Date DTP1, Date DTP2){
        DTPCari1.setDate(new Date()); 
        DTPCari2.setDate(new Date());
       
    }
    
    public void setnorawat(String norawat){
       TCari.setText(norawat);
       
    }
  
}
