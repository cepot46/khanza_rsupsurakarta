/*
  Kontribusi Anggit Nurhidayah
 */

package rekammedis;

import setting.*;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.WarnaTable;
import fungsi.akses;
import inventory.DlgBarang;
import inventory.DlgCariJenis;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariPetugas;

/**
 *
 * @author perpustakaan
 */
public class RMOKAsuhanPerioperatif extends javax.swing.JDialog {
    private final DefaultTableModel tabModePreOp,tabModeInOp,tabModePostOp;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private PreparedStatement ps;
    private ResultSet rs;
    private DlgBarang barang=new DlgBarang(null,false);
    private String finger="",finger2="";

    /**
     *@param parent
     *@param modal*/
    public RMOKAsuhanPerioperatif(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        tabModePreOp=new DefaultTableModel(null,new Object[]{
              "No.Rawat","Tanggal","Jam","Kode Petugas","Petugas","No.RM","Nama","JK","TD","SpO2","HR","RR","Suhu","Keluhan Nyeri","Skala Nyeri",
              "Kesadaran","Orientasi","Catatan Orientasi","Status Pernafasan","Catatan Status Pernafasan","Suara Nafas","Alat Bantu","AB Terpasang O2","AB Terpasang CTT / ETT","AB Tidak Ada","Status CardioPurmonal","Lokasi Cardiopurmonal","AB Pace Maker","AB Kateter Jantung","AB Tidak Ada","Kulit","Ket Kulit",
              "Status Muskulokeletal","Sistem Perkemihan TAK","Sistem Perkemihan Kateter","Sistem Perkemihan Lain-Lain","Catatan Sistem Perkemihan","Kerusakan Sensori","Alat Bantu Dengar","Alat Bantu Kacamata",
              "Tingkat Nyeri Skala","Tingkat Nyeri 1","Tingkat Nyeri 2","Tingkat Nyeri 3","Manajemen Nyeri 1","Manajemen Nyeri 2","Manajemen Nyeri 3","Evaluasi","Tingkat Ansietas 1","Tingkat Ansietas 2","Tingkat Ansietas 3","Tingkat Ansietas 4","Reduksi Ansietas 1","Reduksi Ansietas 2","Reduksi Ansietas 3","Reduksi Ansietas 4","Reduksi Ansietas 5","Reduksi Ansietas 6","Evaluasi 1","Evaluasi 2","Evaluasi 3","Evaluasi 4"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        tbPreOp.setModel(tabModePreOp);
        tbPreOp.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPreOp.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 62; i++) {
            TableColumn column = tbPreOp.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(80);
            }if(i==1){
                column.setPreferredWidth(80);
            }if(i==2){
                column.setPreferredWidth(80);
            }if(i==3){
                column.setPreferredWidth(80);
            }if(i==4){
                column.setPreferredWidth(80);
            }if(i==5){
                column.setPreferredWidth(80);
            }if(i==6){
                column.setPreferredWidth(80);
            }if(i==7){
                column.setPreferredWidth(100);
            }if(i==8){
                column.setPreferredWidth(120);
            }if(i==9){
                column.setPreferredWidth(120);
            }if(i==10){
                column.setPreferredWidth(120);
            }if(i==11){
                column.setPreferredWidth(120);
            }if(i==12){
                column.setPreferredWidth(120);
            }if(i==13){
                column.setPreferredWidth(120);
            }if(i==14){
                column.setPreferredWidth(120);
            }if(i==15){
                column.setPreferredWidth(120);
            }if(i==16){
                column.setPreferredWidth(120);
            }if(i==17){
                column.setPreferredWidth(120);
            }if(i==18){
                column.setPreferredWidth(120);
            }if(i==19){
                column.setPreferredWidth(120);
            }if(i==20){
                column.setPreferredWidth(120);
            }if(i==21){
                column.setPreferredWidth(120);
            }if(i==22){
                column.setPreferredWidth(120);
            }if(i==23){
                column.setPreferredWidth(120);
            }if(i==24){
                column.setPreferredWidth(120);
            }if(i==25){
                column.setPreferredWidth(120);
            }if(i==26){
                column.setPreferredWidth(120);
            }if(i==27){
                column.setPreferredWidth(120);
            }if(i==28){
                column.setPreferredWidth(120);
            }if(i==29){
                column.setPreferredWidth(120);
            }if(i==30){
                column.setPreferredWidth(120);
            }if(i==31){
                column.setPreferredWidth(120);
            }if(i==32){
                column.setPreferredWidth(120);
            }if(i==33){
                column.setPreferredWidth(120);
            }if(i==34){
                column.setPreferredWidth(120);
            }if(i==35){
                column.setPreferredWidth(120);
            }if(i==36){
                column.setPreferredWidth(120);
            }if(i==37){
                column.setPreferredWidth(120);
            }if(i==38){
                column.setPreferredWidth(120);
            }if(i==39){
                column.setPreferredWidth(120);
            }if(i==40){
                column.setPreferredWidth(120);
            }if(i==41){
                column.setPreferredWidth(120);
            }if(i==42){
                column.setPreferredWidth(120);
            }if(i==43){
                column.setPreferredWidth(120);
            }if(i==44){
                column.setPreferredWidth(120);
            }if(i==45){
                column.setPreferredWidth(120);
            }if(i==46){
                column.setPreferredWidth(120);
            }if(i==47){
                column.setPreferredWidth(120);
            }if(i==48){
                column.setPreferredWidth(120);
            }if(i==49){
                column.setPreferredWidth(120);
            }if(i==50){
                column.setPreferredWidth(120);
            }if(i==51){
                column.setPreferredWidth(120);
            }if(i==52){
                column.setPreferredWidth(120);
            }if(i==53){
                column.setPreferredWidth(120);
            }if(i==54){
                column.setPreferredWidth(120);
            }if(i==55){
                column.setPreferredWidth(120);
            }if(i==56){
                column.setPreferredWidth(120);
            }if(i==57){
                column.setPreferredWidth(120);
            }if(i==59){
                column.setPreferredWidth(120);
            }if(i==60){
                column.setPreferredWidth(120);
            }if(i==61){
                column.setPreferredWidth(120);
            }
        }

        tbPreOp.setDefaultRenderer(Object.class, new WarnaTable());
       
        td.setDocument(new batasInput((byte)10).getKata(td));
        spo.setDocument(new batasInput((byte)10).getKata(spo));
        hr.setDocument(new batasInput((byte)10).getKata(hr));
        rr.setDocument(new batasInput((byte)10).getKata(rr));
        suhu.setDocument(new batasInput((byte)10).getKata(suhu));
        orientasi_ket.setDocument(new batasInput((byte)10).getKata(orientasi_ket));
        KdPetugas.setDocument(new batasInput((byte)4).getKata(KdPetugas));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        TCari1.setDocument(new batasInput((int)100).getKata(TCari1));
         
        // TABEL UNTUK  INTA OPERATIF
         tabModeInOp=new DefaultTableModel(null,new Object[]{
              "No.Rawat","Tanggal","Jam","Kode Petugas","Petugas","No.RM","Nama","JK","TD","SpO2","HR","RR","Suhu","Pendarahan Kassa","Irigasi/pencucian",
              "Suction","Luka Bersih","Luka Tekontaminasi","Luka Terkontaminasi","Luka Lembab","Kulit Dingin","Kulit Hangat","Kulit Kering","Kulit Lembab","Kebersihan Tangan Meningkat","Kontrol Resiko","Indentifikasi LO Bersih","Indentifikasi LO Terkontaminasi","Indentifikasi LO Bersih Terkontaminasi","Indentifikasi LO Kotor/Infeksi","Sebelum Kontak","Sebelum Aseptik","Sebelum Kontak Dengan lingkungan Pasien",
              "Sesudah Kontak Dengan Pasien","Setelah Terkena Cairan Tubuh","Sebelum Operasi","Menggunakan APD","Prinsip dan Teknik Aseptik","Preparasi Kulit area Operasi","Membatasi Maks 10 Personel OK","Evaluasi 5 Momen Cuci Tangan",
              "Evaluasi Teknik Aseptik","Evaluasi Kesesuaian Jumlah Personel OK","Menggigil Menurun","Pucat Menurun","Suhu Tubuh Membaik","Suhu Kulit Membaik","Evaluasi1 Tidak Menggigil","Evaluasi1 Tidak Pucat",
              "Evaluasi1 Suhu Tubuh Normal","Evaluasi1 Suhu Kulit Hangat","Monitor Suhu Tubuh","Penghangatan Aktif","Penghangatan Pasif","Asupan Cairan Meningkat","Haluan Urine Meningkat","Tekanan Darah Membaik",
              "Membran Mukosa Membaik","Menghitung Pendarahan OP","Menghitung Keluaran Urine","Mengukur Tanda Tanda Vital","Mencatat Jumlah & Jenis Cairan Diberikan","Asupan Cairan Cukup","Tekanan Darah Membaik","Keluaran Urine Normal","Membran Mukosa Lembab"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        tbInOp.setModel(tabModeInOp);
        tbInOp.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbInOp.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 65; i++) {
            TableColumn column = tbInOp.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(80);
            }if(i==1){
                column.setPreferredWidth(80);
            }if(i==2){
                column.setPreferredWidth(80);
            }if(i==3){
                column.setPreferredWidth(80);
            }if(i==4){
                column.setPreferredWidth(80);
            }if(i==5){
                column.setPreferredWidth(80);
            }if(i==6){
                column.setPreferredWidth(80);
            }if(i==7){
                column.setPreferredWidth(100);
            }if(i==8){
                column.setPreferredWidth(120);
            }if(i==9){
                column.setPreferredWidth(120);
            }if(i==10){
                column.setPreferredWidth(120);
            }if(i==11){
                column.setPreferredWidth(120);
            }if(i==12){
                column.setPreferredWidth(120);
            }if(i==13){
                column.setPreferredWidth(120);
            }if(i==14){
                column.setPreferredWidth(120);
            }if(i==15){
                column.setPreferredWidth(120);
            }if(i==16){
                column.setPreferredWidth(120);
            }if(i==17){
                column.setPreferredWidth(120);
            }if(i==18){
                column.setPreferredWidth(120);
            }if(i==19){
                column.setPreferredWidth(120);
            }if(i==20){
                column.setPreferredWidth(120);
            }if(i==21){
                column.setPreferredWidth(120);
            }if(i==22){
                column.setPreferredWidth(120);
            }if(i==23){
                column.setPreferredWidth(120);
            }if(i==24){
                column.setPreferredWidth(120);
            }if(i==25){
                column.setPreferredWidth(120);
            }if(i==26){
                column.setPreferredWidth(120);
            }if(i==27){
                column.setPreferredWidth(120);
            }if(i==28){
                column.setPreferredWidth(120);
            }if(i==29){
                column.setPreferredWidth(120);
            }if(i==30){
                column.setPreferredWidth(120);
            }if(i==31){
                column.setPreferredWidth(120);
            }if(i==32){
                column.setPreferredWidth(120);
            }if(i==33){
                column.setPreferredWidth(120);
            }if(i==34){
                column.setPreferredWidth(120);
            }if(i==35){
                column.setPreferredWidth(120);
            }if(i==36){
                column.setPreferredWidth(120);
            }if(i==37){
                column.setPreferredWidth(120);
            }if(i==38){
                column.setPreferredWidth(120);
            }if(i==39){
                column.setPreferredWidth(120);
            }if(i==40){
                column.setPreferredWidth(120);
            }if(i==41){
                column.setPreferredWidth(120);
            }if(i==42){
                column.setPreferredWidth(120);
            }if(i==43){
                column.setPreferredWidth(120);
            }if(i==44){
                column.setPreferredWidth(120);
            }if(i==45){
                column.setPreferredWidth(120);
            }if(i==46){
                column.setPreferredWidth(120);
            }if(i==47){
                column.setPreferredWidth(120);
            }if(i==48){
                column.setPreferredWidth(120);
            }if(i==49){
                column.setPreferredWidth(120);
            }if(i==50){
                column.setPreferredWidth(120);
            }if(i==51){
                column.setPreferredWidth(120);
            }if(i==52){
                column.setPreferredWidth(120);
            }if(i==53){
                column.setPreferredWidth(120);
            }if(i==54){
                column.setPreferredWidth(120);
            }if(i==55){
                column.setPreferredWidth(120);
            }if(i==56){
                column.setPreferredWidth(120);
            }if(i==57){
                column.setPreferredWidth(120);
            }if(i==59){
                column.setPreferredWidth(120);
            }if(i==60){
                column.setPreferredWidth(120);
            }if(i==61){
                column.setPreferredWidth(120);
            }if(i==62){
                column.setPreferredWidth(120);
            }if(i==63){
                column.setPreferredWidth(120);
            }if(i==64){
                column.setPreferredWidth(120);
            }
        }

        tbInOp.setDefaultRenderer(Object.class, new WarnaTable());
        
        
        // TABEL UNTUK  POST OPERATIF
         tabModePostOp=new DefaultTableModel(null,new Object[]{
                "No. Rawat","Tanggal", "Jam","Kode Petugas","Nama Petugas", 
                "No. Rekam Medis", "Nama Pasien", "JK", "TD", "SPO2", 
                "hr", "rr", "Suhu", "Skala nyeri", "Tidak bisa dikaji", 
                "Mual", "Muntah", "Pusing", "Kulit dingin", "Kulit hangat", 
                "Kulit kering", "Kulit lembab", "Kulit pucat", "Kesadaran CM", "Apatis", 
                "Sopor", "Coma", "Suara Nafas Gurgling", "Snoring", "Wheezing", 
                "Vesikuler", "Sianosis Menurun", "Pola Nafas Membaik", "Wheezing/ronkhi/gurgling menurun", "Gelisah Menurun", 
                "Frekuensi Nafas membaik", "Dispnea menurun", "Monitor Pola Nafas", "Pertahankan kepatenan jalan nafas", "Penghisapan Lendir", 
                "Monitor Bunyi Nafas ", "Berikan Oksigen", "Ajarkan Batuk Efektif", "Pola Nafas Membaik", "Frekuensi Nafas Membaik", 
                "keluhan Mual Menurun", "Perasaan ingin muntah menurun", "Ajarkan Relaksasi", "Kolaborasi Pemberian Antiemetik", "Ev. Keluhan Mual Menurun", 
                "ev. Perasaan Ingin Muntah Menurun", "Skala Nyeri", "Keluhan Nyeri Menurun", "Gelisah Menurun", "Meringis Menurun", 
                "Identifikasi Skala Nyeri", "Mengajarkan Tarik Nafas Dalam (Teknik Distraksi)", "Kolaborasi Pemberian Analgetik", "Skala Nyeri","Tingkat Jatuh",
                "Pasang Handrail Tempat Tidur", "Pastikan Roda Tempat Tidur Terkunci", "Pastikan Pasien Tidak Ditinggal Sendirian", 
                "Ev. Terpasang Handrail", "Ev. Pasien Selalu Ditemani", "Post  Op Ke Ruang Intensif", "Ruang Rawat Inap", 
                "Rumah","Stretcher","Kursi Roda","Berjalan Sendiri"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        tbPostOp.setModel(tabModePostOp);
        tbPostOp.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPostOp.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 67; i++) {
            TableColumn column = tbPostOp.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(80);
            }if(i==1){
                column.setPreferredWidth(80);
            }if(i==2){
                column.setPreferredWidth(80);
            }if(i==3){
                column.setPreferredWidth(80);
            }if(i==4){
                column.setPreferredWidth(80);
            }if(i==5){
                column.setPreferredWidth(80);
            }if(i==6){
                column.setPreferredWidth(80);
            }if(i==7){
                column.setPreferredWidth(100);
            }if(i==8){
                column.setPreferredWidth(120);
            }if(i==9){
                column.setPreferredWidth(120);
            }if(i==10){
                column.setPreferredWidth(120);
            }if(i==11){
                column.setPreferredWidth(120);
            }if(i==12){
                column.setPreferredWidth(120);
            }if(i==13){
                column.setPreferredWidth(120);
            }if(i==14){
                column.setPreferredWidth(120);
            }if(i==15){
                column.setPreferredWidth(120);
            }if(i==16){
                column.setPreferredWidth(120);
            }if(i==17){
                column.setPreferredWidth(120);
            }if(i==18){
                column.setPreferredWidth(120);
            }if(i==19){
                column.setPreferredWidth(120);
            }if(i==20){
                column.setPreferredWidth(120);
            }if(i==21){
                column.setPreferredWidth(120);
            }if(i==22){
                column.setPreferredWidth(120);
            }if(i==23){
                column.setPreferredWidth(120);
            }if(i==24){
                column.setPreferredWidth(120);
            }if(i==25){
                column.setPreferredWidth(120);
            }if(i==26){
                column.setPreferredWidth(120);
            }if(i==27){
                column.setPreferredWidth(120);
            }if(i==28){
                column.setPreferredWidth(120);
            }if(i==29){
                column.setPreferredWidth(120);
            }if(i==30){
                column.setPreferredWidth(120);
            }if(i==31){
                column.setPreferredWidth(120);
            }if(i==32){
                column.setPreferredWidth(120);
            }if(i==33){
                column.setPreferredWidth(120);
            }if(i==34){
                column.setPreferredWidth(120);
            }if(i==35){
                column.setPreferredWidth(120);
            }if(i==36){
                column.setPreferredWidth(120);
            }if(i==37){
                column.setPreferredWidth(120);
            }if(i==38){
                column.setPreferredWidth(120);
            }if(i==39){
                column.setPreferredWidth(120);
            }if(i==40){
                column.setPreferredWidth(120);
            }if(i==41){
                column.setPreferredWidth(120);
            }if(i==42){
                column.setPreferredWidth(120);
            }if(i==43){
                column.setPreferredWidth(120);
            }if(i==44){
                column.setPreferredWidth(120);
            }if(i==45){
                column.setPreferredWidth(120);
            }if(i==46){
                column.setPreferredWidth(120);
            }if(i==47){
                column.setPreferredWidth(120);
            }if(i==48){
                column.setPreferredWidth(120);
            }if(i==49){
                column.setPreferredWidth(120);
            }if(i==50){
                column.setPreferredWidth(120);
            }if(i==51){
                column.setPreferredWidth(120);
            }if(i==52){
                column.setPreferredWidth(120);
            }if(i==53){
                column.setPreferredWidth(120);
            }if(i==54){
                column.setPreferredWidth(120);
            }if(i==55){
                column.setPreferredWidth(120);
            }if(i==56){
                column.setPreferredWidth(120);
            }if(i==57){
                column.setPreferredWidth(120);
            }if(i==59){
                column.setPreferredWidth(120);
            }if(i==60){
                column.setPreferredWidth(120);
            }if(i==61){
                column.setPreferredWidth(120);
            }if(i==62){
                column.setPreferredWidth(120);
            }if(i==63){
                column.setPreferredWidth(120);
            }if(i==64){
                column.setPreferredWidth(120);
            }if(i==65){
                column.setPreferredWidth(120);
            }if(i==65){
                column.setPreferredWidth(120);
            }if(i==66){
                column.setPreferredWidth(120);
            }
        }

        tbPostOp.setDefaultRenderer(Object.class, new WarnaTable());
    
        
         petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(TabOK.getSelectedIndex()==0){
                    if(petugas.getTable().getSelectedRow()!= -1){     
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString()); 
                    }
                }else if(TabOK.getSelectedIndex()==1){
                    if(petugas.getTable().getSelectedRow()!= -1){     
                    KdPetugas1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString()); 
                }
                } else if(TabOK.getSelectedIndex()==2){
                    if(petugas.getTable().getSelectedRow()!= -1){     
                    KdPetugas2.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas2.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString()); 
                }
                }
                
                
                
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        jam();
        jam1();
        jam2();
        ChkAccor.setSelected(false);
        ChkAccor1.setSelected(false);
        ChkAccor2.setSelected(false);
        isPhoto();
        isPhoto1();
        isPhoto2();
        }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Popup = new javax.swing.JPopupMenu();
        pp2 = new javax.swing.JMenuItem();
        Popup1 = new javax.swing.JPopupMenu();
        pp1 = new javax.swing.JMenuItem();
        Popup2 = new javax.swing.JPopupMenu();
        ppCetakPreOp = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelisi1 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnKeluar = new widget.Button();
        TabOK = new javax.swing.JTabbedPane();
        internalFrame3 = new widget.InternalFrame();
        panelisi5 = new widget.panelisi();
        label9 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        label10 = new widget.Label();
        LCount = new widget.Label();
        scrollInput1 = new widget.ScrollPane();
        FormInput1 = new widget.PanelBiasa();
        TNoRw1 = new widget.TextBox();
        TPasien1 = new widget.TextBox();
        TNoRM1 = new widget.TextBox();
        label14 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnPetugas = new widget.Button();
        jLabel11 = new widget.Label();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        Scroll6 = new widget.ScrollPane();
        tbMasalahKeperawatan = new widget.Table();
        label48 = new widget.Label();
        cmbKulit = new widget.ComboBox();
        label50 = new widget.Label();
        label56 = new widget.Label();
        chkCardiopurmonal1 = new widget.CekBox();
        label57 = new widget.Label();
        chkCardiopurmonal2 = new widget.CekBox();
        perkemihan_ket = new widget.TextBox();
        label59 = new widget.Label();
        chkalatbantu1 = new widget.CekBox();
        chkalatbantu2 = new widget.CekBox();
        label60 = new widget.Label();
        chkalatbantu3 = new widget.CekBox();
        label61 = new widget.Label();
        label62 = new widget.Label();
        label63 = new widget.Label();
        label64 = new widget.Label();
        chkKemih1 = new widget.CekBox();
        label65 = new widget.Label();
        chkKemih2 = new widget.CekBox();
        label66 = new widget.Label();
        chkKemih3 = new widget.CekBox();
        label67 = new widget.Label();
        Cardiopurmonal_ket = new widget.TextBox();
        label68 = new widget.Label();
        cmbMuskolokeletal = new widget.ComboBox();
        label69 = new widget.Label();
        chkTingkatnyeri3 = new widget.CekBox();
        label70 = new widget.Label();
        chkAlatbantuFix2 = new widget.CekBox();
        label71 = new widget.Label();
        label72 = new widget.Label();
        cmbEvaluasinyeri = new widget.ComboBox();
        label73 = new widget.Label();
        label74 = new widget.Label();
        label75 = new widget.Label();
        chkAlatbantuFix1 = new widget.CekBox();
        chkTingkatnyeri1 = new widget.CekBox();
        chkTingkatnyeri2 = new widget.CekBox();
        label76 = new widget.Label();
        chkManajemen1 = new widget.CekBox();
        label77 = new widget.Label();
        chkManajemen2 = new widget.CekBox();
        label78 = new widget.Label();
        chkManajemen3 = new widget.CekBox();
        label79 = new widget.Label();
        label80 = new widget.Label();
        label81 = new widget.Label();
        cmbSensori = new widget.ComboBox();
        label82 = new widget.Label();
        chkAnsietas1 = new widget.CekBox();
        label83 = new widget.Label();
        chkAnsietas2 = new widget.CekBox();
        label84 = new widget.Label();
        chkAnsietas3 = new widget.CekBox();
        label85 = new widget.Label();
        label86 = new widget.Label();
        chkAnsietas4 = new widget.CekBox();
        label87 = new widget.Label();
        chkReduksi1 = new widget.CekBox();
        label88 = new widget.Label();
        chkReduksi2 = new widget.CekBox();
        label89 = new widget.Label();
        chkReduksi3 = new widget.CekBox();
        label90 = new widget.Label();
        label91 = new widget.Label();
        chkReduksi4 = new widget.CekBox();
        label92 = new widget.Label();
        chkReduksi5 = new widget.CekBox();
        chkReduksi6 = new widget.CekBox();
        label93 = new widget.Label();
        label94 = new widget.Label();
        chkEvaluasi1 = new widget.CekBox();
        label95 = new widget.Label();
        chkEvaluasi2 = new widget.CekBox();
        label96 = new widget.Label();
        label97 = new widget.Label();
        chkEvaluasi3 = new widget.CekBox();
        label98 = new widget.Label();
        chkEvaluasi4 = new widget.CekBox();
        label55 = new widget.Label();
        cmbPernafasanalatbantu = new widget.ComboBox();
        label52 = new widget.Label();
        chkPernafasan1 = new widget.CekBox();
        label54 = new widget.Label();
        chkPernafasan2 = new widget.CekBox();
        label53 = new widget.Label();
        chkPernafasan3 = new widget.CekBox();
        label43 = new widget.Label();
        cmbSuaranafas = new widget.ComboBox();
        label49 = new widget.Label();
        label58 = new widget.Label();
        cmbStatuspernafasan = new widget.ComboBox();
        statuspernafasan_ket = new widget.TextBox();
        orientasi_ket = new widget.TextBox();
        cmbOrientasi = new widget.ComboBox();
        label47 = new widget.Label();
        label46 = new widget.Label();
        cmbKesadaran = new widget.ComboBox();
        skalanyeri_ket = new widget.TextBox();
        cmbSkalanyeri = new widget.ComboBox();
        label35 = new widget.Label();
        label37 = new widget.Label();
        td = new widget.TextBox();
        label40 = new widget.Label();
        spo = new widget.TextBox();
        label41 = new widget.Label();
        hr = new widget.TextBox();
        label42 = new widget.Label();
        rr = new widget.TextBox();
        label51 = new widget.Label();
        suhu = new widget.TextBox();
        jLabel17 = new widget.Label();
        Tanggal = new widget.Tanggal();
        Jam = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Detik = new widget.ComboBox();
        ChkJam1 = new widget.CekBox();
        jk = new widget.TextBox();
        label99 = new widget.Label();
        cmbTingkatNyeriSkala = new widget.ComboBox();
        label189 = new widget.Label();
        ket_kulit = new widget.TextBox();
        PanelAccor = new widget.PanelBiasa();
        ChkAccor = new widget.CekBox();
        TabData = new javax.swing.JTabbedPane();
        FormTelaah = new widget.PanelBiasa();
        FormPass3 = new widget.PanelBiasa();
        BtnRefreshPhoto1 = new widget.Button();
        Scroll5 = new widget.ScrollPane();
        tbPreOp = new widget.Table();
        internalFrame4 = new widget.InternalFrame();
        panelisi6 = new widget.panelisi();
        label11 = new widget.Label();
        TCari1 = new widget.TextBox();
        BtnCari1 = new widget.Button();
        BtnAll1 = new widget.Button();
        label12 = new widget.Label();
        LCount1 = new widget.Label();
        scrollInput2 = new widget.ScrollPane();
        FormInput2 = new widget.PanelBiasa();
        TNoRw2 = new widget.TextBox();
        TPasien2 = new widget.TextBox();
        TNoRM2 = new widget.TextBox();
        label15 = new widget.Label();
        KdPetugas1 = new widget.TextBox();
        NmPetugas1 = new widget.TextBox();
        BtnPetugas1 = new widget.Button();
        jLabel12 = new widget.Label();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        label100 = new widget.Label();
        label104 = new widget.Label();
        chk_kontrol_1 = new widget.CekBox();
        chk_kulit_2 = new widget.CekBox();
        chk_kulit_3 = new widget.CekBox();
        label107 = new widget.Label();
        label113 = new widget.Label();
        label150 = new widget.Label();
        label151 = new widget.Label();
        label36 = new widget.Label();
        label38 = new widget.Label();
        td1 = new widget.TextBox();
        label45 = new widget.Label();
        spo1 = new widget.TextBox();
        label152 = new widget.Label();
        hr1 = new widget.TextBox();
        label153 = new widget.Label();
        rr1 = new widget.TextBox();
        label154 = new widget.Label();
        t_kassa = new widget.TextBox();
        jLabel18 = new widget.Label();
        Tanggal1 = new widget.Tanggal();
        Jam1 = new widget.ComboBox();
        Menit1 = new widget.ComboBox();
        Detik1 = new widget.ComboBox();
        ChkJam2 = new widget.CekBox();
        jk1 = new widget.TextBox();
        label144 = new widget.Label();
        label145 = new widget.Label();
        label146 = new widget.Label();
        label147 = new widget.Label();
        label148 = new widget.Label();
        label149 = new widget.Label();
        label156 = new widget.Label();
        label157 = new widget.Label();
        label158 = new widget.Label();
        label159 = new widget.Label();
        label101 = new widget.Label();
        label102 = new widget.Label();
        label103 = new widget.Label();
        label108 = new widget.Label();
        label109 = new widget.Label();
        label110 = new widget.Label();
        label111 = new widget.Label();
        label112 = new widget.Label();
        jSeparator9 = new javax.swing.JSeparator();
        label114 = new widget.Label();
        label115 = new widget.Label();
        label116 = new widget.Label();
        label117 = new widget.Label();
        label118 = new widget.Label();
        label119 = new widget.Label();
        label120 = new widget.Label();
        label121 = new widget.Label();
        label122 = new widget.Label();
        label124 = new widget.Label();
        label123 = new widget.Label();
        label125 = new widget.Label();
        label126 = new widget.Label();
        label127 = new widget.Label();
        label128 = new widget.Label();
        jSeparator10 = new javax.swing.JSeparator();
        label129 = new widget.Label();
        label130 = new widget.Label();
        label131 = new widget.Label();
        label132 = new widget.Label();
        label133 = new widget.Label();
        label134 = new widget.Label();
        label135 = new widget.Label();
        label136 = new widget.Label();
        label137 = new widget.Label();
        label138 = new widget.Label();
        label139 = new widget.Label();
        label140 = new widget.Label();
        label141 = new widget.Label();
        label142 = new widget.Label();
        label143 = new widget.Label();
        label155 = new widget.Label();
        label39 = new widget.Label();
        label160 = new widget.Label();
        label161 = new widget.Label();
        chk_kulit_4 = new widget.CekBox();
        label162 = new widget.Label();
        chk_kulit_1 = new widget.CekBox();
        chk_cuci_1 = new widget.CekBox();
        chk_cuci_2 = new widget.CekBox();
        chk_cuci_5 = new widget.CekBox();
        chk_cuci_6 = new widget.CekBox();
        chk_cuci_7 = new widget.CekBox();
        chk_cuci_8 = new widget.CekBox();
        chk_cuci_9 = new widget.CekBox();
        label163 = new widget.Label();
        hipo_2 = new widget.CekBox();
        chk_jenisluka_1 = new widget.CekBox();
        label164 = new widget.Label();
        chk_jenisluka_2 = new widget.CekBox();
        label165 = new widget.Label();
        label166 = new widget.Label();
        chk_jenisluka_3 = new widget.CekBox();
        label167 = new widget.Label();
        chk_jenisluka_4 = new widget.CekBox();
        chk_cuci_10 = new widget.CekBox();
        evaluasi_1 = new widget.CekBox();
        evaluasi_2 = new widget.CekBox();
        evaluasi_3 = new widget.CekBox();
        termo_1 = new widget.CekBox();
        evaluasi2_4 = new widget.CekBox();
        hipo_1 = new widget.CekBox();
        termo_2 = new widget.CekBox();
        termo_3 = new widget.CekBox();
        termo_4 = new widget.CekBox();
        hipo_3 = new widget.CekBox();
        evaluasi2_1 = new widget.CekBox();
        evaluasi2_2 = new widget.CekBox();
        evaluasi2_3 = new widget.CekBox();
        cairan_4 = new widget.CekBox();
        cairan_1 = new widget.CekBox();
        cairan_2 = new widget.CekBox();
        cairan_3 = new widget.CekBox();
        manaj_1 = new widget.CekBox();
        manaj_2 = new widget.CekBox();
        manaj_3 = new widget.CekBox();
        manaj_4 = new widget.CekBox();
        evaluasi3_4 = new widget.CekBox();
        evaluasi3_3 = new widget.CekBox();
        evaluasi3_2 = new widget.CekBox();
        evaluasi3_1 = new widget.CekBox();
        label44 = new widget.Label();
        suhu1 = new widget.TextBox();
        label168 = new widget.Label();
        t_suction = new widget.TextBox();
        label169 = new widget.Label();
        t_pencucian = new widget.TextBox();
        label170 = new widget.Label();
        chk_luka_1 = new widget.CekBox();
        label171 = new widget.Label();
        chk_luka_2 = new widget.CekBox();
        label172 = new widget.Label();
        label173 = new widget.Label();
        chk_luka_4 = new widget.CekBox();
        label175 = new widget.Label();
        chk_luka_3 = new widget.CekBox();
        chk_cuci_3 = new widget.CekBox();
        chk_cuci_4 = new widget.CekBox();
        chk_kontrol_2 = new widget.CekBox();
        PanelAccor1 = new widget.PanelBiasa();
        ChkAccor1 = new widget.CekBox();
        TabData1 = new javax.swing.JTabbedPane();
        FormTelaah1 = new widget.PanelBiasa();
        FormPass4 = new widget.PanelBiasa();
        BtnRefreshPhoto2 = new widget.Button();
        Scroll8 = new widget.ScrollPane();
        tbInOp = new widget.Table();
        internalFrame5 = new widget.InternalFrame();
        panelisi7 = new widget.panelisi();
        label13 = new widget.Label();
        TCari2 = new widget.TextBox();
        BtnCari2 = new widget.Button();
        BtnAll2 = new widget.Button();
        label16 = new widget.Label();
        LCount2 = new widget.Label();
        scrollInput3 = new widget.ScrollPane();
        FormInput3 = new widget.PanelBiasa();
        TNoRw3 = new widget.TextBox();
        TPasien3 = new widget.TextBox();
        TNoRM3 = new widget.TextBox();
        label17 = new widget.Label();
        KdPetugas2 = new widget.TextBox();
        NmPetugas2 = new widget.TextBox();
        BtnPetugas2 = new widget.Button();
        jLabel13 = new widget.Label();
        jSeparator11 = new javax.swing.JSeparator();
        jSeparator12 = new javax.swing.JSeparator();
        label106 = new widget.Label();
        jn1 = new widget.CekBox();
        sn2 = new widget.CekBox();
        sn3 = new widget.CekBox();
        label174 = new widget.Label();
        label176 = new widget.Label();
        label177 = new widget.Label();
        label178 = new widget.Label();
        label180 = new widget.Label();
        td2 = new widget.TextBox();
        label181 = new widget.Label();
        spo2 = new widget.TextBox();
        label182 = new widget.Label();
        hr2 = new widget.TextBox();
        label183 = new widget.Label();
        rr2 = new widget.TextBox();
        label184 = new widget.Label();
        jLabel19 = new widget.Label();
        Tanggal2 = new widget.Tanggal();
        Jam2 = new widget.ComboBox();
        Menit2 = new widget.ComboBox();
        Detik2 = new widget.ComboBox();
        ChkJam3 = new widget.CekBox();
        jk2 = new widget.TextBox();
        label185 = new widget.Label();
        label187 = new widget.Label();
        label188 = new widget.Label();
        label200 = new widget.Label();
        label201 = new widget.Label();
        label202 = new widget.Label();
        label204 = new widget.Label();
        jSeparator14 = new javax.swing.JSeparator();
        label219 = new widget.Label();
        label220 = new widget.Label();
        label221 = new widget.Label();
        label223 = new widget.Label();
        label224 = new widget.Label();
        label225 = new widget.Label();
        label226 = new widget.Label();
        label227 = new widget.Label();
        label229 = new widget.Label();
        label230 = new widget.Label();
        label231 = new widget.Label();
        label232 = new widget.Label();
        label233 = new widget.Label();
        label235 = new widget.Label();
        label236 = new widget.Label();
        sn4 = new widget.CekBox();
        label237 = new widget.Label();
        sn1 = new widget.CekBox();
        label241 = new widget.Label();
        mjn1 = new widget.CekBox();
        ev_2_1 = new widget.CekBox();
        ev_2_2 = new widget.CekBox();
        tn_1 = new widget.CekBox();
        tn_2 = new widget.CekBox();
        tn_3 = new widget.CekBox();
        mn_1 = new widget.CekBox();
        mn_2 = new widget.CekBox();
        mn_3 = new widget.CekBox();
        pj_2 = new widget.CekBox();
        pj_1 = new widget.CekBox();
        jatuh = new widget.CekBox();
        label243 = new widget.Label();
        suhu2 = new widget.TextBox();
        s1 = new widget.CekBox();
        label247 = new widget.Label();
        s2 = new widget.CekBox();
        label248 = new widget.Label();
        label249 = new widget.Label();
        s4 = new widget.CekBox();
        label250 = new widget.Label();
        s3 = new widget.CekBox();
        label179 = new widget.Label();
        label234 = new widget.Label();
        label244 = new widget.Label();
        label245 = new widget.Label();
        kk1 = new widget.CekBox();
        label251 = new widget.Label();
        kk2 = new widget.CekBox();
        label252 = new widget.Label();
        kk3 = new widget.CekBox();
        label253 = new widget.Label();
        kk4 = new widget.CekBox();
        label254 = new widget.Label();
        kk5 = new widget.CekBox();
        label255 = new widget.Label();
        k1 = new widget.CekBox();
        label256 = new widget.Label();
        k2 = new widget.CekBox();
        label257 = new widget.Label();
        label258 = new widget.Label();
        k3 = new widget.CekBox();
        label259 = new widget.Label();
        chknyeri3 = new widget.CekBox();
        nyeri_1 = new widget.ComboBox();
        label246 = new widget.Label();
        jn2 = new widget.CekBox();
        label186 = new widget.Label();
        label190 = new widget.Label();
        jn3 = new widget.CekBox();
        label191 = new widget.Label();
        jn6 = new widget.CekBox();
        jn5 = new widget.CekBox();
        label192 = new widget.Label();
        label193 = new widget.Label();
        jn4 = new widget.CekBox();
        label260 = new widget.Label();
        mjn2 = new widget.CekBox();
        label261 = new widget.Label();
        mjn3 = new widget.CekBox();
        label262 = new widget.Label();
        mjn6 = new widget.CekBox();
        mjn5 = new widget.CekBox();
        label263 = new widget.Label();
        label264 = new widget.Label();
        mjn4 = new widget.CekBox();
        label194 = new widget.Label();
        ev_1_1 = new widget.CekBox();
        label265 = new widget.Label();
        label266 = new widget.Label();
        ev_1_2 = new widget.CekBox();
        label195 = new widget.Label();
        label196 = new widget.Label();
        kel_1 = new widget.CekBox();
        label267 = new widget.Label();
        kel_2 = new widget.CekBox();
        label268 = new widget.Label();
        label197 = new widget.Label();
        mual_1 = new widget.CekBox();
        label269 = new widget.Label();
        mual_2 = new widget.CekBox();
        label270 = new widget.Label();
        label271 = new widget.Label();
        nyeri_2 = new widget.ComboBox();
        label272 = new widget.Label();
        nyeri_3 = new widget.ComboBox();
        label222 = new widget.Label();
        pj_3 = new widget.CekBox();
        label228 = new widget.Label();
        label238 = new widget.Label();
        label239 = new widget.Label();
        ev_3_2 = new widget.CekBox();
        ev_3_1 = new widget.CekBox();
        label240 = new widget.Label();
        label242 = new widget.Label();
        label273 = new widget.Label();
        po_1 = new widget.CekBox();
        label274 = new widget.Label();
        po_2 = new widget.CekBox();
        label275 = new widget.Label();
        po_3 = new widget.CekBox();
        label276 = new widget.Label();
        label277 = new widget.Label();
        pindah_1 = new widget.CekBox();
        label278 = new widget.Label();
        pindah_2 = new widget.CekBox();
        label279 = new widget.Label();
        pindah_3 = new widget.CekBox();
        label280 = new widget.Label();
        PanelAccor2 = new widget.PanelBiasa();
        ChkAccor2 = new widget.CekBox();
        TabData2 = new javax.swing.JTabbedPane();
        FormTelaah2 = new widget.PanelBiasa();
        FormPass5 = new widget.PanelBiasa();
        BtnRefreshPhoto3 = new widget.Button();
        Scroll9 = new widget.ScrollPane();
        tbPostOp = new widget.Table();

        Popup.setName("Popup"); // NOI18N

        pp2.setBackground(new java.awt.Color(255, 255, 254));
        pp2.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        pp2.setForeground(new java.awt.Color(50, 50, 50));
        pp2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        pp2.setText("-");
        pp2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pp2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        pp2.setName("pp2"); // NOI18N
        pp2.setPreferredSize(new java.awt.Dimension(300, 25));
        pp2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pp2ActionPerformed(evt);
            }
        });
        Popup.add(pp2);

        Popup1.setName("Popup1"); // NOI18N

        pp1.setBackground(new java.awt.Color(255, 255, 254));
        pp1.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        pp1.setForeground(new java.awt.Color(50, 50, 50));
        pp1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        pp1.setText("Cetak Inta Operatif");
        pp1.setActionCommand("-");
        pp1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pp1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        pp1.setName("pp1"); // NOI18N
        pp1.setPreferredSize(new java.awt.Dimension(300, 25));
        pp1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pp1ActionPerformed(evt);
            }
        });
        Popup1.add(pp1);

        Popup2.setName("Popup2"); // NOI18N

        ppCetakPreOp.setBackground(new java.awt.Color(255, 255, 254));
        ppCetakPreOp.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppCetakPreOp.setForeground(new java.awt.Color(50, 50, 50));
        ppCetakPreOp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        ppCetakPreOp.setText("Cetak Formulir");
        ppCetakPreOp.setActionCommand("-");
        ppCetakPreOp.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppCetakPreOp.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppCetakPreOp.setName("ppCetakPreOp"); // NOI18N
        ppCetakPreOp.setPreferredSize(new java.awt.Dimension(300, 25));
        ppCetakPreOp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppCetakPreOpActionPerformed(evt);
            }
        });
        Popup2.add(ppCetakPreOp);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::Asuhan Perioperatif ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi1.setName("panelisi1"); // NOI18N
        panelisi1.setPreferredSize(new java.awt.Dimension(100, 54));
        panelisi1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelisi1.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelisi1.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelisi1.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setIconTextGap(3);
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelisi1.add(BtnEdit);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelisi1.add(BtnKeluar);

        internalFrame1.add(panelisi1, java.awt.BorderLayout.PAGE_END);

        TabOK.setBackground(new java.awt.Color(255, 255, 254));
        TabOK.setForeground(new java.awt.Color(50, 50, 50));
        TabOK.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        TabOK.setName("TabOK"); // NOI18N
        TabOK.setPreferredSize(new java.awt.Dimension(452, 300));
        TabOK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabOKMouseClicked(evt);
            }
        });

        internalFrame3.setBackground(new java.awt.Color(255, 255, 255));
        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi5.setName("panelisi5"); // NOI18N
        panelisi5.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        label9.setText("Key Word :");
        label9.setName("label9"); // NOI18N
        label9.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi5.add(label9);

        TCari.setEditable(false);
        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(350, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelisi5.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('1');
        BtnCari.setToolTipText("Alt+1");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelisi5.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('2');
        BtnAll.setToolTipText("Alt+2");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelisi5.add(BtnAll);

        label10.setText("Record :");
        label10.setName("label10"); // NOI18N
        label10.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi5.add(label10);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi5.add(LCount);

        internalFrame3.add(panelisi5, java.awt.BorderLayout.PAGE_END);

        scrollInput1.setName("scrollInput1"); // NOI18N
        scrollInput1.setPreferredSize(new java.awt.Dimension(102, 200));

        FormInput1.setBackground(new java.awt.Color(255, 255, 255));
        FormInput1.setBorder(null);
        FormInput1.setName("FormInput1"); // NOI18N
        FormInput1.setPreferredSize(new java.awt.Dimension(870, 900));
        FormInput1.setLayout(null);

        TNoRw1.setHighlighter(null);
        TNoRw1.setName("TNoRw1"); // NOI18N
        TNoRw1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRw1KeyPressed(evt);
            }
        });
        FormInput1.add(TNoRw1);
        TNoRw1.setBounds(74, 10, 131, 23);

        TPasien1.setEditable(false);
        TPasien1.setHighlighter(null);
        TPasien1.setName("TPasien1"); // NOI18N
        FormInput1.add(TPasien1);
        TPasien1.setBounds(309, 10, 260, 23);

        TNoRM1.setEditable(false);
        TNoRM1.setHighlighter(null);
        TNoRM1.setName("TNoRM1"); // NOI18N
        FormInput1.add(TNoRM1);
        TNoRM1.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput1.add(label14);
        label14.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput1.add(KdPetugas);
        KdPetugas.setBounds(74, 40, 100, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput1.add(NmPetugas);
        NmPetugas.setBounds(176, 40, 180, 23);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas.setMnemonic('2');
        BtnPetugas.setToolTipText("Alt+2");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        FormInput1.add(BtnPetugas);
        BtnPetugas.setBounds(358, 40, 28, 23);

        jLabel11.setText("No.Rawat :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput1.add(jLabel11);
        jLabel11.setBounds(0, 10, 70, 23);

        jSeparator5.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator5.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator5.setName("jSeparator5"); // NOI18N
        FormInput1.add(jSeparator5);
        jSeparator5.setBounds(0, 70, 880, 1);

        jSeparator6.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator6.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator6.setName("jSeparator6"); // NOI18N
        FormInput1.add(jSeparator6);
        jSeparator6.setBounds(0, 120, 880, 1);

        Scroll6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll6.setName("Scroll6"); // NOI18N
        Scroll6.setOpaque(true);
        Scroll6.setPreferredSize(new java.awt.Dimension(452, 200));

        tbMasalahKeperawatan.setName("tbMasalahKeperawatan"); // NOI18N
        Scroll6.setViewportView(tbMasalahKeperawatan);

        FormInput1.add(Scroll6);
        Scroll6.setBounds(10, 1210, 400, 133);

        label48.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label48.setText("h. Kerusakan Sensori :");
        label48.setName("label48"); // NOI18N
        label48.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label48);
        label48.setBounds(20, 490, 110, 23);

        cmbKulit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Dingin", "Hangat", "Lembab", "Kering", "Utuh", "Luka", "Tato", "Lain-lain" }));
        cmbKulit.setName("cmbKulit"); // NOI18N
        cmbKulit.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbKulit);
        cmbKulit.setBounds(150, 390, 110, 23);

        label50.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label50.setText("Tidak");
        label50.setName("label50"); // NOI18N
        label50.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label50);
        label50.setBounds(270, 330, 30, 23);

        label56.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label56.setText("Status Cardiopurmonal  (Edema) :");
        label56.setName("label56"); // NOI18N
        label56.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label56);
        label56.setBounds(70, 330, 170, 23);

        chkCardiopurmonal1.setBorder(null);
        chkCardiopurmonal1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkCardiopurmonal1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkCardiopurmonal1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkCardiopurmonal1.setName("chkCardiopurmonal1"); // NOI18N
        FormInput1.add(chkCardiopurmonal1);
        chkCardiopurmonal1.setBounds(240, 330, 23, 23);

        label57.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label57.setText("Ya, lokasi");
        label57.setName("label57"); // NOI18N
        label57.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label57);
        label57.setBounds(390, 330, 50, 23);

        chkCardiopurmonal2.setBorder(null);
        chkCardiopurmonal2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkCardiopurmonal2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkCardiopurmonal2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkCardiopurmonal2.setName("chkCardiopurmonal2"); // NOI18N
        FormInput1.add(chkCardiopurmonal2);
        chkCardiopurmonal2.setBounds(360, 330, 23, 23);

        perkemihan_ket.setHighlighter(null);
        perkemihan_ket.setName("perkemihan_ket"); // NOI18N
        perkemihan_ket.setPreferredSize(new java.awt.Dimension(50, 23));
        perkemihan_ket.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                perkemihan_ketMouseMoved(evt);
            }
        });
        perkemihan_ket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                perkemihan_ketMouseExited(evt);
            }
        });
        perkemihan_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                perkemihan_ketKeyPressed(evt);
            }
        });
        FormInput1.add(perkemihan_ket);
        perkemihan_ket.setBounds(450, 460, 110, 23);

        label59.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label59.setText("Pace Maker");
        label59.setName("label59"); // NOI18N
        label59.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label59);
        label59.setBounds(180, 360, 60, 23);

        chkalatbantu1.setBorder(null);
        chkalatbantu1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkalatbantu1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkalatbantu1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkalatbantu1.setName("chkalatbantu1"); // NOI18N
        FormInput1.add(chkalatbantu1);
        chkalatbantu1.setBounds(150, 360, 23, 23);

        chkalatbantu2.setBorder(null);
        chkalatbantu2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkalatbantu2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkalatbantu2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkalatbantu2.setName("chkalatbantu2"); // NOI18N
        FormInput1.add(chkalatbantu2);
        chkalatbantu2.setBounds(240, 360, 23, 23);

        label60.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label60.setText("Kateter Jantung");
        label60.setName("label60"); // NOI18N
        label60.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label60);
        label60.setBounds(270, 360, 80, 23);

        chkalatbantu3.setBorder(null);
        chkalatbantu3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkalatbantu3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkalatbantu3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkalatbantu3.setName("chkalatbantu3"); // NOI18N
        FormInput1.add(chkalatbantu3);
        chkalatbantu3.setBounds(360, 360, 23, 23);

        label61.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label61.setText("Tidak Ada");
        label61.setName("label61"); // NOI18N
        label61.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label61);
        label61.setBounds(390, 360, 80, 23);

        label62.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label62.setText("d. Alat Bantu (terpasang) :");
        label62.setName("label62"); // NOI18N
        label62.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label62);
        label62.setBounds(20, 360, 130, 23);

        label63.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label63.setText("g. Sistem Perkemihan :");
        label63.setName("label63"); // NOI18N
        label63.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label63);
        label63.setBounds(20, 460, 120, 23);

        label64.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label64.setText("f. Status Muskolokeletal :");
        label64.setName("label64"); // NOI18N
        label64.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label64);
        label64.setBounds(20, 420, 130, 23);

        chkKemih1.setBorder(null);
        chkKemih1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkKemih1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkKemih1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkKemih1.setName("chkKemih1"); // NOI18N
        FormInput1.add(chkKemih1);
        chkKemih1.setBounds(150, 460, 23, 23);

        label65.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label65.setText("TAK");
        label65.setName("label65"); // NOI18N
        label65.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label65);
        label65.setBounds(180, 460, 60, 23);

        chkKemih2.setBorder(null);
        chkKemih2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkKemih2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkKemih2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkKemih2.setName("chkKemih2"); // NOI18N
        FormInput1.add(chkKemih2);
        chkKemih2.setBounds(240, 460, 23, 23);

        label66.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label66.setText("Terpasang Kateter");
        label66.setName("label66"); // NOI18N
        label66.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label66);
        label66.setBounds(270, 460, 90, 23);

        chkKemih3.setBorder(null);
        chkKemih3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkKemih3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkKemih3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkKemih3.setName("chkKemih3"); // NOI18N
        FormInput1.add(chkKemih3);
        chkKemih3.setBounds(370, 460, 23, 23);

        label67.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label67.setText("Lain-lain");
        label67.setName("label67"); // NOI18N
        label67.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label67);
        label67.setBounds(400, 460, 80, 23);

        Cardiopurmonal_ket.setHighlighter(null);
        Cardiopurmonal_ket.setName("Cardiopurmonal_ket"); // NOI18N
        Cardiopurmonal_ket.setPreferredSize(new java.awt.Dimension(50, 23));
        Cardiopurmonal_ket.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                Cardiopurmonal_ketMouseMoved(evt);
            }
        });
        Cardiopurmonal_ket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                Cardiopurmonal_ketMouseExited(evt);
            }
        });
        Cardiopurmonal_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Cardiopurmonal_ketKeyPressed(evt);
            }
        });
        FormInput1.add(Cardiopurmonal_ket);
        Cardiopurmonal_ket.setBounds(450, 330, 110, 23);

        label68.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label68.setText("e. Kulit :");
        label68.setName("label68"); // NOI18N
        label68.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label68);
        label68.setBounds(20, 390, 50, 23);

        cmbMuskolokeletal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TAK", "Paralisis", "Traksi", "Alat Bantu" }));
        cmbMuskolokeletal.setName("cmbMuskolokeletal"); // NOI18N
        cmbMuskolokeletal.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbMuskolokeletal);
        cmbMuskolokeletal.setBounds(150, 420, 110, 23);

        label69.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label69.setText("Meringis Menurun");
        label69.setName("label69"); // NOI18N
        label69.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label69);
        label69.setBounds(60, 670, 120, 23);

        chkTingkatnyeri3.setBorder(null);
        chkTingkatnyeri3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkTingkatnyeri3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkTingkatnyeri3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkTingkatnyeri3.setName("chkTingkatnyeri3"); // NOI18N
        FormInput1.add(chkTingkatnyeri3);
        chkTingkatnyeri3.setBounds(30, 670, 23, 23);

        label70.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label70.setText("Alat Dengar");
        label70.setName("label70"); // NOI18N
        label70.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label70);
        label70.setBounds(120, 530, 60, 23);

        chkAlatbantuFix2.setBorder(null);
        chkAlatbantuFix2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkAlatbantuFix2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkAlatbantuFix2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkAlatbantuFix2.setName("chkAlatbantuFix2"); // NOI18N
        FormInput1.add(chkAlatbantuFix2);
        chkAlatbantuFix2.setBounds(180, 530, 23, 23);

        label71.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label71.setText("Kaca Mata");
        label71.setName("label71"); // NOI18N
        label71.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label71);
        label71.setBounds(210, 530, 90, 23);

        label72.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label72.setText("i. Alat Bantu :");
        label72.setName("label72"); // NOI18N
        label72.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label72);
        label72.setBounds(20, 530, 70, 23);

        cmbEvaluasinyeri.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        cmbEvaluasinyeri.setName("cmbEvaluasinyeri"); // NOI18N
        cmbEvaluasinyeri.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbEvaluasinyeri);
        cmbEvaluasinyeri.setBounds(570, 590, 60, 23);

        label73.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label73.setText("Tingkat Nyeri :");
        label73.setName("label73"); // NOI18N
        label73.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label73);
        label73.setBounds(30, 570, 110, 23);

        label74.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label74.setText("Keluhan Nyeri Menurun");
        label74.setName("label74"); // NOI18N
        label74.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label74);
        label74.setBounds(60, 630, 120, 23);

        label75.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label75.setText("Gelisah menurun");
        label75.setName("label75"); // NOI18N
        label75.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label75);
        label75.setBounds(60, 650, 120, 23);

        chkAlatbantuFix1.setBorder(null);
        chkAlatbantuFix1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkAlatbantuFix1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkAlatbantuFix1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkAlatbantuFix1.setName("chkAlatbantuFix1"); // NOI18N
        FormInput1.add(chkAlatbantuFix1);
        chkAlatbantuFix1.setBounds(90, 530, 23, 23);

        chkTingkatnyeri1.setBorder(null);
        chkTingkatnyeri1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkTingkatnyeri1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkTingkatnyeri1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkTingkatnyeri1.setName("chkTingkatnyeri1"); // NOI18N
        FormInput1.add(chkTingkatnyeri1);
        chkTingkatnyeri1.setBounds(30, 630, 23, 23);

        chkTingkatnyeri2.setBorder(null);
        chkTingkatnyeri2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkTingkatnyeri2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkTingkatnyeri2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkTingkatnyeri2.setName("chkTingkatnyeri2"); // NOI18N
        FormInput1.add(chkTingkatnyeri2);
        chkTingkatnyeri2.setBounds(30, 650, 23, 23);

        label76.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label76.setText("Evaluasi :");
        label76.setName("label76"); // NOI18N
        label76.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label76);
        label76.setBounds(460, 570, 50, 23);

        chkManajemen1.setBorder(null);
        chkManajemen1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkManajemen1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkManajemen1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkManajemen1.setName("chkManajemen1"); // NOI18N
        FormInput1.add(chkManajemen1);
        chkManajemen1.setBounds(190, 590, 23, 23);

        label77.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label77.setText("Identifikasi skala nyeri");
        label77.setName("label77"); // NOI18N
        label77.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label77);
        label77.setBounds(220, 590, 120, 23);

        chkManajemen2.setBorder(null);
        chkManajemen2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkManajemen2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkManajemen2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkManajemen2.setName("chkManajemen2"); // NOI18N
        FormInput1.add(chkManajemen2);
        chkManajemen2.setBounds(190, 610, 23, 23);

        label78.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label78.setText("Mengajarkan tarik nafas dalam, teknik distraksi");
        label78.setName("label78"); // NOI18N
        label78.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label78);
        label78.setBounds(220, 610, 330, 23);

        chkManajemen3.setBorder(null);
        chkManajemen3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkManajemen3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkManajemen3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkManajemen3.setName("chkManajemen3"); // NOI18N
        FormInput1.add(chkManajemen3);
        chkManajemen3.setBounds(190, 630, 23, 23);

        label79.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label79.setText("Kolaborasi Pemberian Analgetik");
        label79.setName("label79"); // NOI18N
        label79.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label79);
        label79.setBounds(220, 630, 240, 23);

        label80.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label80.setText("Manajemen Nyeri :");
        label80.setName("label80"); // NOI18N
        label80.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label80);
        label80.setBounds(190, 570, 110, 23);

        label81.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label81.setText("Skala Nyeri ( 0 - 10 ) ");
        label81.setName("label81"); // NOI18N
        label81.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label81);
        label81.setBounds(460, 590, 160, 23);

        cmbSensori.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TAK", "Pendengaran", "Bahasa", "Penglihatan" }));
        cmbSensori.setName("cmbSensori"); // NOI18N
        cmbSensori.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbSensori);
        cmbSensori.setBounds(150, 490, 110, 23);

        label82.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label82.setText("D.0080 Ansietas b.d kurang terpapar informasi");
        label82.setName("label82"); // NOI18N
        label82.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label82);
        label82.setBounds(30, 700, 290, 23);

        chkAnsietas1.setBorder(null);
        chkAnsietas1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkAnsietas1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkAnsietas1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkAnsietas1.setName("chkAnsietas1"); // NOI18N
        FormInput1.add(chkAnsietas1);
        chkAnsietas1.setBounds(30, 740, 23, 23);

        label83.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label83.setText("Gelisah Menurun");
        label83.setName("label83"); // NOI18N
        label83.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label83);
        label83.setBounds(60, 740, 120, 23);

        chkAnsietas2.setBorder(null);
        chkAnsietas2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkAnsietas2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkAnsietas2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkAnsietas2.setName("chkAnsietas2"); // NOI18N
        FormInput1.add(chkAnsietas2);
        chkAnsietas2.setBounds(30, 760, 23, 23);

        label84.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label84.setText("Tekanan darah menurun");
        label84.setName("label84"); // NOI18N
        label84.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label84);
        label84.setBounds(60, 760, 120, 23);

        chkAnsietas3.setBorder(null);
        chkAnsietas3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkAnsietas3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkAnsietas3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkAnsietas3.setName("chkAnsietas3"); // NOI18N
        FormInput1.add(chkAnsietas3);
        chkAnsietas3.setBounds(30, 780, 23, 23);

        label85.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label85.setText("Perilaku tegang menurun");
        label85.setName("label85"); // NOI18N
        label85.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label85);
        label85.setBounds(60, 780, 120, 23);

        label86.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label86.setText("Palpitasi Menurun");
        label86.setName("label86"); // NOI18N
        label86.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label86);
        label86.setBounds(60, 800, 120, 23);

        chkAnsietas4.setBorder(null);
        chkAnsietas4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkAnsietas4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkAnsietas4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkAnsietas4.setName("chkAnsietas4"); // NOI18N
        FormInput1.add(chkAnsietas4);
        chkAnsietas4.setBounds(30, 800, 23, 23);

        label87.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label87.setText("Evaluasi :");
        label87.setName("label87"); // NOI18N
        label87.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label87);
        label87.setBounds(480, 720, 110, 23);

        chkReduksi1.setBorder(null);
        chkReduksi1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkReduksi1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkReduksi1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkReduksi1.setName("chkReduksi1"); // NOI18N
        FormInput1.add(chkReduksi1);
        chkReduksi1.setBounds(190, 740, 23, 23);

        label88.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label88.setText("Monitor tanda ansietas verbal / non verbal");
        label88.setName("label88"); // NOI18N
        label88.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label88);
        label88.setBounds(220, 740, 250, 23);

        chkReduksi2.setBorder(null);
        chkReduksi2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkReduksi2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkReduksi2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkReduksi2.setName("chkReduksi2"); // NOI18N
        FormInput1.add(chkReduksi2);
        chkReduksi2.setBounds(190, 760, 23, 23);

        label89.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label89.setText("Ciptakan Suasana Menumbuhkan kepercayaan");
        label89.setName("label89"); // NOI18N
        label89.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label89);
        label89.setBounds(220, 760, 270, 23);

        chkReduksi3.setBorder(null);
        chkReduksi3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkReduksi3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkReduksi3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkReduksi3.setName("chkReduksi3"); // NOI18N
        FormInput1.add(chkReduksi3);
        chkReduksi3.setBounds(190, 780, 23, 23);

        label90.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label90.setText("Temani Pasien");
        label90.setName("label90"); // NOI18N
        label90.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label90);
        label90.setBounds(220, 780, 120, 23);

        label91.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label91.setText("Dengarkan Penuh Perhatian");
        label91.setName("label91"); // NOI18N
        label91.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label91);
        label91.setBounds(220, 800, 140, 23);

        chkReduksi4.setBorder(null);
        chkReduksi4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkReduksi4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkReduksi4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkReduksi4.setName("chkReduksi4"); // NOI18N
        FormInput1.add(chkReduksi4);
        chkReduksi4.setBounds(190, 800, 23, 23);

        label92.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label92.setText("Jelaskan Prosedur dan Sensasi yang Mungkin Dialami");
        label92.setName("label92"); // NOI18N
        label92.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label92);
        label92.setBounds(220, 820, 280, 23);

        chkReduksi5.setBorder(null);
        chkReduksi5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkReduksi5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkReduksi5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkReduksi5.setName("chkReduksi5"); // NOI18N
        FormInput1.add(chkReduksi5);
        chkReduksi5.setBounds(190, 820, 23, 23);

        chkReduksi6.setBorder(null);
        chkReduksi6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkReduksi6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkReduksi6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkReduksi6.setName("chkReduksi6"); // NOI18N
        FormInput1.add(chkReduksi6);
        chkReduksi6.setBounds(190, 840, 23, 23);

        label93.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label93.setText("Latih Teknik Relaksasi");
        label93.setName("label93"); // NOI18N
        label93.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label93);
        label93.setBounds(220, 840, 110, 23);

        label94.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label94.setText("I.09314 Reduksi Ansietas :");
        label94.setName("label94"); // NOI18N
        label94.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label94);
        label94.setBounds(190, 720, 240, 23);

        chkEvaluasi1.setBorder(null);
        chkEvaluasi1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkEvaluasi1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkEvaluasi1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkEvaluasi1.setName("chkEvaluasi1"); // NOI18N
        FormInput1.add(chkEvaluasi1);
        chkEvaluasi1.setBounds(480, 740, 23, 23);

        label95.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label95.setText("Gelisah Menurun");
        label95.setName("label95"); // NOI18N
        label95.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label95);
        label95.setBounds(510, 740, 120, 23);

        chkEvaluasi2.setBorder(null);
        chkEvaluasi2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkEvaluasi2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkEvaluasi2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkEvaluasi2.setName("chkEvaluasi2"); // NOI18N
        FormInput1.add(chkEvaluasi2);
        chkEvaluasi2.setBounds(480, 760, 23, 23);

        label96.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label96.setText("Perilaku Tegang  Menurun");
        label96.setName("label96"); // NOI18N
        label96.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label96);
        label96.setBounds(510, 760, 180, 23);

        label97.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label97.setText("Tekanan Darah Menurun");
        label97.setName("label97"); // NOI18N
        label97.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label97);
        label97.setBounds(510, 780, 180, 23);

        chkEvaluasi3.setBorder(null);
        chkEvaluasi3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkEvaluasi3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkEvaluasi3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkEvaluasi3.setName("chkEvaluasi3"); // NOI18N
        FormInput1.add(chkEvaluasi3);
        chkEvaluasi3.setBounds(480, 780, 23, 23);

        label98.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label98.setText("Palpitasi Menurun");
        label98.setName("label98"); // NOI18N
        label98.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label98);
        label98.setBounds(510, 800, 180, 23);

        chkEvaluasi4.setBorder(null);
        chkEvaluasi4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkEvaluasi4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkEvaluasi4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkEvaluasi4.setName("chkEvaluasi4"); // NOI18N
        FormInput1.add(chkEvaluasi4);
        chkEvaluasi4.setBounds(480, 800, 23, 23);

        label55.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label55.setText("Alat Bantu :");
        label55.setName("label55"); // NOI18N
        label55.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label55);
        label55.setBounds(70, 300, 70, 23);

        cmbPernafasanalatbantu.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Trakheostomi", "Tidak Ada" }));
        cmbPernafasanalatbantu.setName("cmbPernafasanalatbantu"); // NOI18N
        cmbPernafasanalatbantu.setPreferredSize(new java.awt.Dimension(120, 23));
        cmbPernafasanalatbantu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPernafasanalatbantuActionPerformed(evt);
            }
        });
        FormInput1.add(cmbPernafasanalatbantu);
        cmbPernafasanalatbantu.setBounds(150, 300, 110, 23);

        label52.setText("Terpasang :");
        label52.setName("label52"); // NOI18N
        label52.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput1.add(label52);
        label52.setBounds(270, 300, 70, 23);

        chkPernafasan1.setBorder(null);
        chkPernafasan1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkPernafasan1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkPernafasan1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkPernafasan1.setName("chkPernafasan1"); // NOI18N
        FormInput1.add(chkPernafasan1);
        chkPernafasan1.setBounds(340, 300, 23, 23);

        label54.setText("O2");
        label54.setName("label54"); // NOI18N
        label54.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput1.add(label54);
        label54.setBounds(360, 300, 20, 23);

        chkPernafasan2.setBorder(null);
        chkPernafasan2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkPernafasan2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkPernafasan2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkPernafasan2.setName("chkPernafasan2"); // NOI18N
        chkPernafasan2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkPernafasan2ActionPerformed(evt);
            }
        });
        FormInput1.add(chkPernafasan2);
        chkPernafasan2.setBounds(400, 300, 23, 23);

        label53.setText("CTT/ETT");
        label53.setName("label53"); // NOI18N
        label53.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput1.add(label53);
        label53.setBounds(420, 300, 50, 23);

        chkPernafasan3.setBorder(null);
        chkPernafasan3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkPernafasan3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkPernafasan3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chkPernafasan3.setName("chkPernafasan3"); // NOI18N
        FormInput1.add(chkPernafasan3);
        chkPernafasan3.setBounds(480, 300, 23, 23);

        label43.setText("Tidak Ada");
        label43.setName("label43"); // NOI18N
        label43.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput1.add(label43);
        label43.setBounds(500, 300, 60, 23);

        cmbSuaranafas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Ronkhi", "Wheezing" }));
        cmbSuaranafas.setName("cmbSuaranafas"); // NOI18N
        cmbSuaranafas.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbSuaranafas);
        cmbSuaranafas.setBounds(150, 270, 90, 23);

        label49.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label49.setText("Suara Nafas :");
        label49.setName("label49"); // NOI18N
        label49.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label49);
        label49.setBounds(70, 270, 70, 23);

        label58.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label58.setText("c. Status Pernafasan :");
        label58.setName("label58"); // NOI18N
        label58.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label58);
        label58.setBounds(20, 240, 110, 23);

        cmbStatuspernafasan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Sesak" }));
        cmbStatuspernafasan.setName("cmbStatuspernafasan"); // NOI18N
        cmbStatuspernafasan.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbStatuspernafasan);
        cmbStatuspernafasan.setBounds(150, 240, 90, 23);

        statuspernafasan_ket.setHighlighter(null);
        statuspernafasan_ket.setName("statuspernafasan_ket"); // NOI18N
        statuspernafasan_ket.setPreferredSize(new java.awt.Dimension(50, 23));
        statuspernafasan_ket.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                statuspernafasan_ketMouseMoved(evt);
            }
        });
        statuspernafasan_ket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                statuspernafasan_ketMouseExited(evt);
            }
        });
        statuspernafasan_ket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statuspernafasan_ketActionPerformed(evt);
            }
        });
        statuspernafasan_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                statuspernafasan_ketKeyPressed(evt);
            }
        });
        FormInput1.add(statuspernafasan_ket);
        statuspernafasan_ket.setBounds(250, 240, 260, 23);

        orientasi_ket.setHighlighter(null);
        orientasi_ket.setName("orientasi_ket"); // NOI18N
        orientasi_ket.setPreferredSize(new java.awt.Dimension(50, 23));
        orientasi_ket.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                orientasi_ketMouseMoved(evt);
            }
        });
        orientasi_ket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                orientasi_ketMouseExited(evt);
            }
        });
        orientasi_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                orientasi_ketKeyPressed(evt);
            }
        });
        FormInput1.add(orientasi_ket);
        orientasi_ket.setBounds(250, 210, 260, 23);

        cmbOrientasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Baik", "Disorientasi", "Gelisah", "Banyak Bertanya", "Lain-lain" }));
        cmbOrientasi.setName("cmbOrientasi"); // NOI18N
        cmbOrientasi.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbOrientasi);
        cmbOrientasi.setBounds(140, 210, 90, 23);

        label47.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label47.setText("b. Orientasi :");
        label47.setName("label47"); // NOI18N
        label47.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label47);
        label47.setBounds(20, 210, 80, 23);

        label46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label46.setText("a. Kesadaran :");
        label46.setName("label46"); // NOI18N
        label46.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label46);
        label46.setBounds(20, 180, 80, 23);

        cmbKesadaran.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "CM", "Apatis", "Sopor", "Coma" }));
        cmbKesadaran.setName("cmbKesadaran"); // NOI18N
        cmbKesadaran.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbKesadaran);
        cmbKesadaran.setBounds(140, 180, 90, 23);

        skalanyeri_ket.setHighlighter(null);
        skalanyeri_ket.setName("skalanyeri_ket"); // NOI18N
        skalanyeri_ket.setPreferredSize(new java.awt.Dimension(50, 23));
        skalanyeri_ket.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                skalanyeri_ketMouseMoved(evt);
            }
        });
        skalanyeri_ket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                skalanyeri_ketMouseExited(evt);
            }
        });
        skalanyeri_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                skalanyeri_ketKeyPressed(evt);
            }
        });
        FormInput1.add(skalanyeri_ket);
        skalanyeri_ket.setBounds(320, 130, 50, 23);

        cmbSkalanyeri.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya, Skala Nyeri [ 0 - 10 ]", "Tidak Nyeri", "Tidak Bisa Dikaji" }));
        cmbSkalanyeri.setName("cmbSkalanyeri"); // NOI18N
        cmbSkalanyeri.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbSkalanyeri);
        cmbSkalanyeri.setBounds(110, 130, 200, 23);

        label35.setText("Keluhan Nyeri :");
        label35.setName("label35"); // NOI18N
        label35.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label35);
        label35.setBounds(20, 130, 80, 23);

        label37.setText("TD :");
        label37.setName("label37"); // NOI18N
        label37.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput1.add(label37);
        label37.setBounds(20, 80, 30, 23);

        td.setHighlighter(null);
        td.setName("td"); // NOI18N
        td.setPreferredSize(new java.awt.Dimension(50, 23));
        td.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tdMouseMoved(evt);
            }
        });
        td.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tdMouseExited(evt);
            }
        });
        td.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tdKeyPressed(evt);
            }
        });
        FormInput1.add(td);
        td.setBounds(50, 80, 50, 23);

        label40.setText("SpO2 :");
        label40.setName("label40"); // NOI18N
        label40.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput1.add(label40);
        label40.setBounds(100, 80, 40, 23);

        spo.setName("spo"); // NOI18N
        spo.setPreferredSize(new java.awt.Dimension(50, 23));
        spo.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                spoMouseMoved(evt);
            }
        });
        spo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                spoMouseExited(evt);
            }
        });
        spo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                spoActionPerformed(evt);
            }
        });
        spo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                spoKeyPressed(evt);
            }
        });
        FormInput1.add(spo);
        spo.setBounds(140, 80, 50, 23);

        label41.setText("HR :");
        label41.setName("label41"); // NOI18N
        label41.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput1.add(label41);
        label41.setBounds(190, 80, 30, 23);

        hr.setName("hr"); // NOI18N
        hr.setPreferredSize(new java.awt.Dimension(50, 23));
        hr.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                hrMouseMoved(evt);
            }
        });
        hr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                hrMouseExited(evt);
            }
        });
        hr.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                hrKeyPressed(evt);
            }
        });
        FormInput1.add(hr);
        hr.setBounds(220, 80, 50, 23);

        label42.setText("RR :");
        label42.setName("label42"); // NOI18N
        label42.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput1.add(label42);
        label42.setBounds(270, 80, 30, 23);

        rr.setName("rr"); // NOI18N
        rr.setPreferredSize(new java.awt.Dimension(50, 23));
        rr.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                rrMouseMoved(evt);
            }
        });
        rr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                rrMouseExited(evt);
            }
        });
        rr.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rrKeyPressed(evt);
            }
        });
        FormInput1.add(rr);
        rr.setBounds(300, 80, 50, 23);

        label51.setText("Suhu :");
        label51.setName("label51"); // NOI18N
        label51.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput1.add(label51);
        label51.setBounds(340, 80, 50, 23);

        suhu.setName("suhu"); // NOI18N
        suhu.setPreferredSize(new java.awt.Dimension(50, 23));
        suhu.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                suhuMouseMoved(evt);
            }
        });
        suhu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                suhuMouseExited(evt);
            }
        });
        suhu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                suhuKeyPressed(evt);
            }
        });
        FormInput1.add(suhu);
        suhu.setBounds(390, 80, 50, 23);

        jLabel17.setText("Tanggal :");
        jLabel17.setName("jLabel17"); // NOI18N
        jLabel17.setVerifyInputWhenFocusTarget(false);
        FormInput1.add(jLabel17);
        jLabel17.setBounds(380, 40, 75, 23);

        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "24-10-2024" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput1.add(Tanggal);
        Tanggal.setBounds(460, 40, 110, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setName("Jam"); // NOI18N
        Jam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JamActionPerformed(evt);
            }
        });
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        FormInput1.add(Jam);
        Jam.setBounds(580, 40, 50, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        FormInput1.add(Menit);
        Menit.setBounds(640, 40, 50, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        FormInput1.add(Detik);
        Detik.setBounds(700, 40, 50, 23);

        ChkJam1.setBorder(null);
        ChkJam1.setSelected(true);
        ChkJam1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam1.setName("ChkJam1"); // NOI18N
        FormInput1.add(ChkJam1);
        ChkJam1.setBounds(750, 40, 23, 23);

        jk.setEditable(false);
        jk.setName("jk"); // NOI18N
        jk.setPreferredSize(new java.awt.Dimension(50, 23));
        jk.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jkMouseMoved(evt);
            }
        });
        jk.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jkMouseExited(evt);
            }
        });
        jk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jkActionPerformed(evt);
            }
        });
        jk.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jkKeyPressed(evt);
            }
        });
        FormInput1.add(jk);
        jk.setBounds(580, 10, 50, 23);

        label99.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label99.setText("Skala Nyeri");
        label99.setName("label99"); // NOI18N
        label99.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label99);
        label99.setBounds(110, 600, 60, 23);

        cmbTingkatNyeriSkala.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        cmbTingkatNyeriSkala.setName("cmbTingkatNyeriSkala"); // NOI18N
        cmbTingkatNyeriSkala.setPreferredSize(new java.awt.Dimension(120, 23));
        FormInput1.add(cmbTingkatNyeriSkala);
        cmbTingkatNyeriSkala.setBounds(30, 600, 70, 23);

        label189.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label189.setText("L.09093 Tingkat Ansietas :");
        label189.setName("label189"); // NOI18N
        label189.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label189);
        label189.setBounds(30, 720, 160, 23);

        ket_kulit.setHighlighter(null);
        ket_kulit.setName("ket_kulit"); // NOI18N
        ket_kulit.setPreferredSize(new java.awt.Dimension(50, 23));
        ket_kulit.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                ket_kulitMouseMoved(evt);
            }
        });
        ket_kulit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ket_kulitMouseExited(evt);
            }
        });
        ket_kulit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ket_kulitKeyPressed(evt);
            }
        });
        FormInput1.add(ket_kulit);
        ket_kulit.setBounds(270, 390, 290, 23);

        scrollInput1.setViewportView(FormInput1);

        internalFrame3.add(scrollInput1, java.awt.BorderLayout.CENTER);

        PanelAccor.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor.setName("PanelAccor"); // NOI18N
        PanelAccor.setPreferredSize(new java.awt.Dimension(445, 43));
        PanelAccor.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setSelected(true);
        ChkAccor.setFocusable(false);
        ChkAccor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor.setName("ChkAccor"); // NOI18N
        ChkAccor.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccorActionPerformed(evt);
            }
        });
        PanelAccor.add(ChkAccor, java.awt.BorderLayout.WEST);

        TabData.setBackground(new java.awt.Color(254, 255, 254));
        TabData.setForeground(new java.awt.Color(50, 50, 50));
        TabData.setName("TabData"); // NOI18N
        TabData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabDataMouseClicked(evt);
            }
        });

        FormTelaah.setBackground(new java.awt.Color(255, 255, 255));
        FormTelaah.setBorder(null);
        FormTelaah.setName("FormTelaah"); // NOI18N
        FormTelaah.setPreferredSize(new java.awt.Dimension(115, 73));
        FormTelaah.setLayout(new java.awt.BorderLayout());

        FormPass3.setBackground(new java.awt.Color(255, 255, 255));
        FormPass3.setBorder(null);
        FormPass3.setName("FormPass3"); // NOI18N
        FormPass3.setPreferredSize(new java.awt.Dimension(115, 40));

        BtnRefreshPhoto1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto1.setMnemonic('U');
        BtnRefreshPhoto1.setText("Refresh");
        BtnRefreshPhoto1.setToolTipText("Alt+U");
        BtnRefreshPhoto1.setName("BtnRefreshPhoto1"); // NOI18N
        BtnRefreshPhoto1.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto1ActionPerformed(evt);
            }
        });
        FormPass3.add(BtnRefreshPhoto1);

        FormTelaah.add(FormPass3, java.awt.BorderLayout.PAGE_END);

        Scroll5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll5.setName("Scroll5"); // NOI18N
        Scroll5.setOpaque(true);
        Scroll5.setPreferredSize(new java.awt.Dimension(200, 200));

        tbPreOp.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbPreOp.setComponentPopupMenu(Popup2);
        tbPreOp.setName("tbPreOp"); // NOI18N
        tbPreOp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPreOpMouseClicked(evt);
            }
        });
        tbPreOp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPreOpKeyPressed(evt);
            }
        });
        Scroll5.setViewportView(tbPreOp);

        FormTelaah.add(Scroll5, java.awt.BorderLayout.CENTER);

        TabData.addTab("Data Pre Operasi", FormTelaah);

        PanelAccor.add(TabData, java.awt.BorderLayout.CENTER);

        internalFrame3.add(PanelAccor, java.awt.BorderLayout.EAST);

        TabOK.addTab("Perawatan Pre Operatif", internalFrame3);

        internalFrame4.setBackground(new java.awt.Color(255, 255, 255));
        internalFrame4.setBorder(null);
        internalFrame4.setName("internalFrame4"); // NOI18N
        internalFrame4.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi6.setName("panelisi6"); // NOI18N
        panelisi6.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        label11.setText("Key Word :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi6.add(label11);

        TCari1.setEditable(false);
        TCari1.setName("TCari1"); // NOI18N
        TCari1.setPreferredSize(new java.awt.Dimension(350, 23));
        TCari1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCari1KeyPressed(evt);
            }
        });
        panelisi6.add(TCari1);

        BtnCari1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari1.setMnemonic('1');
        BtnCari1.setToolTipText("Alt+1");
        BtnCari1.setName("BtnCari1"); // NOI18N
        BtnCari1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCari1ActionPerformed(evt);
            }
        });
        BtnCari1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCari1KeyPressed(evt);
            }
        });
        panelisi6.add(BtnCari1);

        BtnAll1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll1.setMnemonic('2');
        BtnAll1.setToolTipText("Alt+2");
        BtnAll1.setName("BtnAll1"); // NOI18N
        BtnAll1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAll1ActionPerformed(evt);
            }
        });
        BtnAll1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAll1KeyPressed(evt);
            }
        });
        panelisi6.add(BtnAll1);

        label12.setText("Record :");
        label12.setName("label12"); // NOI18N
        label12.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi6.add(label12);

        LCount1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount1.setText("0");
        LCount1.setName("LCount1"); // NOI18N
        LCount1.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi6.add(LCount1);

        internalFrame4.add(panelisi6, java.awt.BorderLayout.PAGE_END);

        scrollInput2.setName("scrollInput2"); // NOI18N
        scrollInput2.setPreferredSize(new java.awt.Dimension(102, 200));

        FormInput2.setBackground(new java.awt.Color(255, 255, 255));
        FormInput2.setBorder(null);
        FormInput2.setName("FormInput2"); // NOI18N
        FormInput2.setPreferredSize(new java.awt.Dimension(870, 1250));
        FormInput2.setLayout(null);

        TNoRw2.setHighlighter(null);
        TNoRw2.setName("TNoRw2"); // NOI18N
        TNoRw2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRw2KeyPressed(evt);
            }
        });
        FormInput2.add(TNoRw2);
        TNoRw2.setBounds(74, 10, 131, 23);

        TPasien2.setEditable(false);
        TPasien2.setHighlighter(null);
        TPasien2.setName("TPasien2"); // NOI18N
        FormInput2.add(TPasien2);
        TPasien2.setBounds(309, 10, 260, 23);

        TNoRM2.setEditable(false);
        TNoRM2.setHighlighter(null);
        TNoRM2.setName("TNoRM2"); // NOI18N
        FormInput2.add(TNoRM2);
        TNoRM2.setBounds(207, 10, 100, 23);

        label15.setText("Petugas :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput2.add(label15);
        label15.setBounds(0, 40, 70, 23);

        KdPetugas1.setEditable(false);
        KdPetugas1.setName("KdPetugas1"); // NOI18N
        KdPetugas1.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugas1KeyPressed(evt);
            }
        });
        FormInput2.add(KdPetugas1);
        KdPetugas1.setBounds(74, 40, 100, 23);

        NmPetugas1.setEditable(false);
        NmPetugas1.setName("NmPetugas1"); // NOI18N
        NmPetugas1.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput2.add(NmPetugas1);
        NmPetugas1.setBounds(176, 40, 180, 23);

        BtnPetugas1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas1.setMnemonic('2');
        BtnPetugas1.setToolTipText("Alt+2");
        BtnPetugas1.setName("BtnPetugas1"); // NOI18N
        BtnPetugas1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugas1ActionPerformed(evt);
            }
        });
        BtnPetugas1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugas1KeyPressed(evt);
            }
        });
        FormInput2.add(BtnPetugas1);
        BtnPetugas1.setBounds(358, 40, 28, 23);

        jLabel12.setText("No.Rawat :");
        jLabel12.setName("jLabel12"); // NOI18N
        FormInput2.add(jLabel12);
        jLabel12.setBounds(0, 10, 70, 23);

        jSeparator7.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator7.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator7.setName("jSeparator7"); // NOI18N
        FormInput2.add(jSeparator7);
        jSeparator7.setBounds(0, 70, 880, 1);

        jSeparator8.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator8.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator8.setName("jSeparator8"); // NOI18N
        FormInput2.add(jSeparator8);
        jSeparator8.setBounds(0, 120, 880, 1);

        label100.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label100.setText("Suhu kulit nembaik");
        label100.setName("label100"); // NOI18N
        label100.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label100);
        label100.setBounds(260, 810, 120, 20);

        label104.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label104.setText("Dingin");
        label104.setName("label104"); // NOI18N
        label104.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label104);
        label104.setBounds(150, 220, 40, 23);

        chk_kontrol_1.setBorder(null);
        chk_kontrol_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_kontrol_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_kontrol_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_kontrol_1.setName("chk_kontrol_1"); // NOI18N
        FormInput2.add(chk_kontrol_1);
        chk_kontrol_1.setBounds(30, 320, 23, 23);

        chk_kulit_2.setBorder(null);
        chk_kulit_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_kulit_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_kulit_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_kulit_2.setName("chk_kulit_2"); // NOI18N
        FormInput2.add(chk_kulit_2);
        chk_kulit_2.setBounds(190, 220, 23, 23);

        chk_kulit_3.setBorder(null);
        chk_kulit_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_kulit_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_kulit_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_kulit_3.setName("chk_kulit_3"); // NOI18N
        FormInput2.add(chk_kulit_3);
        chk_kulit_3.setBounds(260, 220, 23, 23);

        label107.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label107.setText("Kontrol resiko");
        label107.setName("label107"); // NOI18N
        label107.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label107);
        label107.setBounds(240, 320, 70, 23);

        label113.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label113.setText("Kondisi Kulit :");
        label113.setName("label113"); // NOI18N
        label113.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label113);
        label113.setBounds(50, 220, 70, 23);

        label150.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label150.setText("Jenis Luka Operasi :");
        label150.setName("label150"); // NOI18N
        label150.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label150);
        label150.setBounds(20, 190, 100, 20);

        label151.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label151.setText("Irigasi/Pencucian :");
        label151.setName("label151"); // NOI18N
        label151.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label151);
        label151.setBounds(30, 160, 100, 23);

        label36.setText("Suction :");
        label36.setName("label36"); // NOI18N
        label36.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label36);
        label36.setBounds(340, 130, 80, 23);

        label38.setText("TD :");
        label38.setName("label38"); // NOI18N
        label38.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label38);
        label38.setBounds(20, 80, 30, 23);

        td1.setHighlighter(null);
        td1.setName("td1"); // NOI18N
        td1.setPreferredSize(new java.awt.Dimension(50, 23));
        td1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                td1MouseMoved(evt);
            }
        });
        td1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                td1MouseExited(evt);
            }
        });
        td1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                td1KeyPressed(evt);
            }
        });
        FormInput2.add(td1);
        td1.setBounds(50, 80, 50, 23);

        label45.setText("SpO2 :");
        label45.setName("label45"); // NOI18N
        label45.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label45);
        label45.setBounds(100, 80, 40, 23);

        spo1.setName("spo1"); // NOI18N
        spo1.setPreferredSize(new java.awt.Dimension(50, 23));
        spo1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                spo1MouseMoved(evt);
            }
        });
        spo1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                spo1MouseExited(evt);
            }
        });
        spo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                spo1ActionPerformed(evt);
            }
        });
        spo1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                spo1KeyPressed(evt);
            }
        });
        FormInput2.add(spo1);
        spo1.setBounds(140, 80, 50, 23);

        label152.setText("HR :");
        label152.setName("label152"); // NOI18N
        label152.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label152);
        label152.setBounds(190, 80, 30, 23);

        hr1.setName("hr1"); // NOI18N
        hr1.setPreferredSize(new java.awt.Dimension(50, 23));
        hr1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                hr1MouseMoved(evt);
            }
        });
        hr1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                hr1MouseExited(evt);
            }
        });
        hr1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                hr1KeyPressed(evt);
            }
        });
        FormInput2.add(hr1);
        hr1.setBounds(220, 80, 50, 23);

        label153.setText("RR :");
        label153.setName("label153"); // NOI18N
        label153.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label153);
        label153.setBounds(270, 80, 30, 23);

        rr1.setName("rr1"); // NOI18N
        rr1.setPreferredSize(new java.awt.Dimension(50, 23));
        rr1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                rr1MouseMoved(evt);
            }
        });
        rr1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                rr1MouseExited(evt);
            }
        });
        rr1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rr1KeyPressed(evt);
            }
        });
        FormInput2.add(rr1);
        rr1.setBounds(300, 80, 50, 23);

        label154.setText("Suhu :");
        label154.setName("label154"); // NOI18N
        label154.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label154);
        label154.setBounds(340, 80, 50, 23);

        t_kassa.setName("t_kassa"); // NOI18N
        t_kassa.setPreferredSize(new java.awt.Dimension(50, 23));
        t_kassa.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                t_kassaMouseMoved(evt);
            }
        });
        t_kassa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                t_kassaMouseExited(evt);
            }
        });
        t_kassa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                t_kassaKeyPressed(evt);
            }
        });
        FormInput2.add(t_kassa);
        t_kassa.setBounds(170, 130, 100, 23);

        jLabel18.setText("Tanggal :");
        jLabel18.setName("jLabel18"); // NOI18N
        jLabel18.setVerifyInputWhenFocusTarget(false);
        FormInput2.add(jLabel18);
        jLabel18.setBounds(380, 40, 75, 23);

        Tanggal1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "24-10-2024" }));
        Tanggal1.setDisplayFormat("dd-MM-yyyy");
        Tanggal1.setName("Tanggal1"); // NOI18N
        Tanggal1.setOpaque(false);
        Tanggal1.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput2.add(Tanggal1);
        Tanggal1.setBounds(460, 40, 110, 23);

        Jam1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam1.setName("Jam1"); // NOI18N
        Jam1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam1ActionPerformed(evt);
            }
        });
        Jam1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam1KeyPressed(evt);
            }
        });
        FormInput2.add(Jam1);
        Jam1.setBounds(580, 40, 50, 23);

        Menit1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit1.setName("Menit1"); // NOI18N
        Menit1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit1KeyPressed(evt);
            }
        });
        FormInput2.add(Menit1);
        Menit1.setBounds(640, 40, 50, 23);

        Detik1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik1.setName("Detik1"); // NOI18N
        Detik1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik1KeyPressed(evt);
            }
        });
        FormInput2.add(Detik1);
        Detik1.setBounds(700, 40, 50, 23);

        ChkJam2.setBorder(null);
        ChkJam2.setSelected(true);
        ChkJam2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam2.setName("ChkJam2"); // NOI18N
        FormInput2.add(ChkJam2);
        ChkJam2.setBounds(750, 40, 23, 23);

        jk1.setEditable(false);
        jk1.setName("jk1"); // NOI18N
        jk1.setPreferredSize(new java.awt.Dimension(50, 23));
        jk1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jk1MouseMoved(evt);
            }
        });
        jk1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jk1MouseExited(evt);
            }
        });
        jk1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jk1ActionPerformed(evt);
            }
        });
        jk1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jk1KeyPressed(evt);
            }
        });
        FormInput2.add(jk1);
        jk1.setBounds(580, 10, 50, 23);

        label144.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label144.setText("D.0142 Risiko Infeksi b.d efek prosedur invasif");
        label144.setName("label144"); // NOI18N
        label144.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label144);
        label144.setBounds(20, 270, 250, 23);

        label145.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label145.setText("Cuci tangan 5 momen");
        label145.setName("label145"); // NOI18N
        label145.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label145);
        label145.setBounds(30, 450, 220, 23);

        label146.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label146.setText("L.14137 Tingkat Infeksi");
        label146.setName("label146"); // NOI18N
        label146.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label146);
        label146.setBounds(20, 300, 250, 23);

        label147.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label147.setText("I. 14539 Pencegahan Infeksi");
        label147.setName("label147"); // NOI18N
        label147.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label147);
        label147.setBounds(20, 350, 150, 23);

        label148.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label148.setText("Mengidentifikasi jenis luka operasi");
        label148.setName("label148"); // NOI18N
        label148.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label148);
        label148.setBounds(30, 370, 220, 23);

        label149.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label149.setText("Sebelum kontak pasien");
        label149.setName("label149"); // NOI18N
        label149.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label149);
        label149.setBounds(60, 470, 130, 23);

        label156.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label156.setText("Sebelum tidakan aseptik");
        label156.setName("label156"); // NOI18N
        label156.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label156);
        label156.setBounds(60, 490, 130, 23);

        label157.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label157.setText("Setelah kontak dengan lingkungan pasien");
        label157.setName("label157"); // NOI18N
        label157.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label157);
        label157.setBounds(60, 510, 210, 23);

        label158.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label158.setText("Sesudah kontak dengan pasien");
        label158.setName("label158"); // NOI18N
        label158.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label158);
        label158.setBounds(290, 470, 210, 23);

        label159.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label159.setText("Setelah terkena cairan tubuh");
        label159.setName("label159"); // NOI18N
        label159.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label159);
        label159.setBounds(290, 490, 210, 23);

        label101.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label101.setText("Melakukan cuci tangan bedah/steril sebelum operasi");
        label101.setName("label101"); // NOI18N
        label101.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label101);
        label101.setBounds(60, 540, 260, 23);

        label102.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label102.setText("Menggunakan APD yang sesuai");
        label102.setName("label102"); // NOI18N
        label102.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label102);
        label102.setBounds(60, 560, 260, 23);

        label103.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label103.setText("Melaksanakan prinsip dan teknik aseptik");
        label103.setName("label103"); // NOI18N
        label103.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label103);
        label103.setBounds(60, 580, 260, 23);

        label108.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label108.setText("Melakukan preparasi kulit area operasi sesuai prosedur");
        label108.setName("label108"); // NOI18N
        label108.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label108);
        label108.setBounds(60, 600, 280, 23);

        label109.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label109.setText("membatasi personel dikamar operasi (Maksimal 10 Orang)");
        label109.setName("label109"); // NOI18N
        label109.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label109);
        label109.setBounds(60, 620, 280, 23);

        label110.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label110.setText("Evaluasi");
        label110.setName("label110"); // NOI18N
        label110.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label110);
        label110.setBounds(20, 650, 50, 23);

        label111.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label111.setText("Kepatuhan 5 momen cuci tangan");
        label111.setName("label111"); // NOI18N
        label111.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label111);
        label111.setBounds(60, 670, 200, 23);

        label112.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label112.setText("Kepatuhan teknik aseptik");
        label112.setName("label112"); // NOI18N
        label112.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label112);
        label112.setBounds(60, 690, 200, 23);

        jSeparator9.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator9.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator9.setName("jSeparator9"); // NOI18N
        FormInput2.add(jSeparator9);
        jSeparator9.setBounds(0, 650, 880, 3);

        label114.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label114.setText("Kesesuaian jumlah personel dikamar operasi");
        label114.setName("label114"); // NOI18N
        label114.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label114);
        label114.setBounds(60, 710, 280, 23);

        label115.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label115.setText("L. 03020");
        label115.setName("label115"); // NOI18N
        label115.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label115);
        label115.setBounds(20, 940, 60, 23);

        label116.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label116.setText("I. 14507 Manajemen Hipotermia");
        label116.setName("label116"); // NOI18N
        label116.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label116);
        label116.setBounds(20, 840, 170, 23);

        label117.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label117.setText("Menggigil menurun");
        label117.setName("label117"); // NOI18N
        label117.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label117);
        label117.setBounds(60, 790, 120, 20);

        label118.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label118.setText("Pucat menurun");
        label118.setName("label118"); // NOI18N
        label118.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label118);
        label118.setBounds(60, 810, 120, 20);

        label119.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label119.setText("Suhu tubuh membaik");
        label119.setName("label119"); // NOI18N
        label119.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label119);
        label119.setBounds(260, 790, 120, 20);

        label120.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label120.setText("L. 14134 Termoregulasi");
        label120.setName("label120"); // NOI18N
        label120.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label120);
        label120.setBounds(20, 770, 120, 23);

        label121.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label121.setText("Monitor suhu tubuh");
        label121.setName("label121"); // NOI18N
        label121.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label121);
        label121.setBounds(60, 860, 120, 20);

        label122.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label122.setText("Lakukan penghangatan aktif");
        label122.setName("label122"); // NOI18N
        label122.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label122);
        label122.setBounds(60, 880, 160, 20);

        label124.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label124.setText("Lakukan penghangatan pasif");
        label124.setName("label124"); // NOI18N
        label124.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label124);
        label124.setBounds(260, 860, 150, 20);

        label123.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label123.setText("Evaluasi");
        label123.setName("label123"); // NOI18N
        label123.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label123);
        label123.setBounds(470, 770, 170, 23);

        label125.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label125.setText("Tidak menggigil");
        label125.setName("label125"); // NOI18N
        label125.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label125);
        label125.setBounds(500, 790, 120, 20);

        label126.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label126.setText("Tidak pucat");
        label126.setName("label126"); // NOI18N
        label126.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label126);
        label126.setBounds(500, 810, 160, 20);

        label127.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label127.setText("Suhu kulit hangat");
        label127.setName("label127"); // NOI18N
        label127.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label127);
        label127.setBounds(710, 810, 150, 20);

        label128.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label128.setText("Suhu tubuh normal");
        label128.setName("label128"); // NOI18N
        label128.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label128);
        label128.setBounds(710, 790, 150, 20);

        jSeparator10.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator10.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator10.setName("jSeparator10"); // NOI18N
        FormInput2.add(jSeparator10);
        jSeparator10.setBounds(0, 911, 880, 3);

        label129.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label129.setText("D. Resiko Hipotermi b.d Suhu Lingkungan Rendah, Efek Agen Farmakologis");
        label129.setName("label129"); // NOI18N
        label129.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label129);
        label129.setBounds(20, 750, 410, 23);

        label130.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label130.setText("D. 0036 Resiko Ketidakseimbangan Cairan b.d Prosedur Pembedahan Mayor");
        label130.setName("label130"); // NOI18N
        label130.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label130);
        label130.setBounds(20, 920, 410, 23);

        label131.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label131.setText("Haluan urine meningkat");
        label131.setName("label131"); // NOI18N
        label131.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label131);
        label131.setBounds(60, 980, 160, 20);

        label132.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label132.setText("Asupan cairan meningkat");
        label132.setName("label132"); // NOI18N
        label132.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label132);
        label132.setBounds(60, 960, 120, 20);

        label133.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label133.setText("Membran mukosa membaik");
        label133.setName("label133"); // NOI18N
        label133.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label133);
        label133.setBounds(60, 1020, 150, 20);

        label134.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label134.setText("Tekanan darah membaik");
        label134.setName("label134"); // NOI18N
        label134.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label134);
        label134.setBounds(60, 1000, 150, 20);

        label135.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label135.setText("I 03098 Manajemen cairan");
        label135.setName("label135"); // NOI18N
        label135.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label135);
        label135.setBounds(470, 940, 180, 23);

        label136.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label136.setText("Menghitung perdarahan selama operasi");
        label136.setName("label136"); // NOI18N
        label136.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label136);
        label136.setBounds(500, 960, 200, 20);

        label137.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label137.setText("Menghitung keluaran urine");
        label137.setName("label137"); // NOI18N
        label137.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label137);
        label137.setBounds(500, 980, 160, 20);

        label138.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label138.setText("Mengukur tanda-tanda vital");
        label138.setName("label138"); // NOI18N
        label138.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label138);
        label138.setBounds(500, 1000, 150, 20);

        label139.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label139.setText("Mencatat jumlah dan jenis cairan yang diberikan");
        label139.setName("label139"); // NOI18N
        label139.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label139);
        label139.setBounds(500, 1020, 280, 20);

        label140.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label140.setText("Evaluasi");
        label140.setName("label140"); // NOI18N
        label140.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label140);
        label140.setBounds(20, 1060, 60, 23);

        label141.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label141.setText("Asupan cairan cukup");
        label141.setName("label141"); // NOI18N
        label141.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label141);
        label141.setBounds(60, 1080, 120, 20);

        label142.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label142.setText("Tekanan darah membaik");
        label142.setName("label142"); // NOI18N
        label142.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label142);
        label142.setBounds(60, 1100, 160, 20);

        label143.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label143.setText("Keluaran urine normal");
        label143.setName("label143"); // NOI18N
        label143.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label143);
        label143.setBounds(60, 1120, 150, 20);

        label155.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label155.setText("Membran mukosa lembab");
        label155.setName("label155"); // NOI18N
        label155.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label155);
        label155.setBounds(60, 1140, 150, 20);

        label39.setText("cc");
        label39.setName("label39"); // NOI18N
        label39.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label39);
        label39.setBounds(270, 130, 20, 23);

        label160.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label160.setText("Kering");
        label160.setName("label160"); // NOI18N
        label160.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label160);
        label160.setBounds(290, 220, 40, 23);

        label161.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label161.setText("Hangat");
        label161.setName("label161"); // NOI18N
        label161.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label161);
        label161.setBounds(220, 220, 40, 23);

        chk_kulit_4.setBorder(null);
        chk_kulit_4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_kulit_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_kulit_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_kulit_4.setName("chk_kulit_4"); // NOI18N
        FormInput2.add(chk_kulit_4);
        chk_kulit_4.setBounds(330, 220, 23, 23);

        label162.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label162.setText("Lembab");
        label162.setName("label162"); // NOI18N
        label162.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label162);
        label162.setBounds(360, 220, 50, 23);

        chk_kulit_1.setBorder(null);
        chk_kulit_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_kulit_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_kulit_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_kulit_1.setName("chk_kulit_1"); // NOI18N
        chk_kulit_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chk_kulit_1ActionPerformed(evt);
            }
        });
        FormInput2.add(chk_kulit_1);
        chk_kulit_1.setBounds(120, 220, 23, 23);

        chk_cuci_1.setBorder(null);
        chk_cuci_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_1.setName("chk_cuci_1"); // NOI18N
        FormInput2.add(chk_cuci_1);
        chk_cuci_1.setBounds(30, 470, 23, 23);

        chk_cuci_2.setBorder(null);
        chk_cuci_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_2.setName("chk_cuci_2"); // NOI18N
        FormInput2.add(chk_cuci_2);
        chk_cuci_2.setBounds(30, 490, 23, 23);

        chk_cuci_5.setBorder(null);
        chk_cuci_5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_5.setName("chk_cuci_5"); // NOI18N
        FormInput2.add(chk_cuci_5);
        chk_cuci_5.setBounds(260, 490, 23, 23);

        chk_cuci_6.setBorder(null);
        chk_cuci_6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_6.setName("chk_cuci_6"); // NOI18N
        FormInput2.add(chk_cuci_6);
        chk_cuci_6.setBounds(30, 540, 23, 23);

        chk_cuci_7.setBorder(null);
        chk_cuci_7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_7.setName("chk_cuci_7"); // NOI18N
        FormInput2.add(chk_cuci_7);
        chk_cuci_7.setBounds(30, 560, 23, 23);

        chk_cuci_8.setBorder(null);
        chk_cuci_8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_8.setName("chk_cuci_8"); // NOI18N
        FormInput2.add(chk_cuci_8);
        chk_cuci_8.setBounds(30, 580, 23, 23);

        chk_cuci_9.setBorder(null);
        chk_cuci_9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_9.setName("chk_cuci_9"); // NOI18N
        FormInput2.add(chk_cuci_9);
        chk_cuci_9.setBounds(30, 600, 23, 23);

        label163.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label163.setText("Kebersihan tangan meningkat");
        label163.setName("label163"); // NOI18N
        label163.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label163);
        label163.setBounds(60, 320, 150, 23);

        hipo_2.setBorder(null);
        hipo_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        hipo_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hipo_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        hipo_2.setName("hipo_2"); // NOI18N
        FormInput2.add(hipo_2);
        hipo_2.setBounds(30, 880, 23, 23);

        chk_jenisluka_1.setBorder(null);
        chk_jenisluka_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_jenisluka_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_jenisluka_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_jenisluka_1.setName("chk_jenisluka_1"); // NOI18N
        FormInput2.add(chk_jenisluka_1);
        chk_jenisluka_1.setBounds(30, 390, 23, 23);

        label164.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label164.setText("Bersih");
        label164.setName("label164"); // NOI18N
        label164.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label164);
        label164.setBounds(60, 390, 70, 23);

        chk_jenisluka_2.setBorder(null);
        chk_jenisluka_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_jenisluka_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_jenisluka_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_jenisluka_2.setName("chk_jenisluka_2"); // NOI18N
        FormInput2.add(chk_jenisluka_2);
        chk_jenisluka_2.setBounds(30, 410, 23, 23);

        label165.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label165.setText("Terkontaminasi");
        label165.setName("label165"); // NOI18N
        label165.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label165);
        label165.setBounds(60, 410, 90, 23);

        label166.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label166.setText("Bersih Terkontaminasi");
        label166.setName("label166"); // NOI18N
        label166.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label166);
        label166.setBounds(290, 390, 120, 23);

        chk_jenisluka_3.setBorder(null);
        chk_jenisluka_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_jenisluka_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_jenisluka_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_jenisluka_3.setName("chk_jenisluka_3"); // NOI18N
        FormInput2.add(chk_jenisluka_3);
        chk_jenisluka_3.setBounds(260, 390, 23, 23);

        label167.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label167.setText("Kotor / Infeksi");
        label167.setName("label167"); // NOI18N
        label167.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label167);
        label167.setBounds(290, 410, 120, 23);

        chk_jenisluka_4.setBorder(null);
        chk_jenisluka_4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_jenisluka_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_jenisluka_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_jenisluka_4.setName("chk_jenisluka_4"); // NOI18N
        FormInput2.add(chk_jenisluka_4);
        chk_jenisluka_4.setBounds(260, 410, 23, 23);

        chk_cuci_10.setBorder(null);
        chk_cuci_10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_10.setName("chk_cuci_10"); // NOI18N
        FormInput2.add(chk_cuci_10);
        chk_cuci_10.setBounds(30, 620, 23, 23);

        evaluasi_1.setBorder(null);
        evaluasi_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi_1.setName("evaluasi_1"); // NOI18N
        FormInput2.add(evaluasi_1);
        evaluasi_1.setBounds(30, 670, 23, 23);

        evaluasi_2.setBorder(null);
        evaluasi_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi_2.setName("evaluasi_2"); // NOI18N
        FormInput2.add(evaluasi_2);
        evaluasi_2.setBounds(30, 690, 23, 23);

        evaluasi_3.setBorder(null);
        evaluasi_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi_3.setName("evaluasi_3"); // NOI18N
        FormInput2.add(evaluasi_3);
        evaluasi_3.setBounds(30, 710, 23, 23);

        termo_1.setBorder(null);
        termo_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        termo_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        termo_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        termo_1.setName("termo_1"); // NOI18N
        FormInput2.add(termo_1);
        termo_1.setBounds(30, 790, 23, 23);

        evaluasi2_4.setBorder(null);
        evaluasi2_4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi2_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi2_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi2_4.setName("evaluasi2_4"); // NOI18N
        FormInput2.add(evaluasi2_4);
        evaluasi2_4.setBounds(680, 810, 23, 23);

        hipo_1.setBorder(null);
        hipo_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        hipo_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hipo_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        hipo_1.setName("hipo_1"); // NOI18N
        FormInput2.add(hipo_1);
        hipo_1.setBounds(30, 860, 23, 23);

        termo_2.setBorder(null);
        termo_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        termo_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        termo_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        termo_2.setName("termo_2"); // NOI18N
        FormInput2.add(termo_2);
        termo_2.setBounds(30, 810, 23, 23);

        termo_3.setBorder(null);
        termo_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        termo_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        termo_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        termo_3.setName("termo_3"); // NOI18N
        FormInput2.add(termo_3);
        termo_3.setBounds(230, 790, 23, 23);

        termo_4.setBorder(null);
        termo_4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        termo_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        termo_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        termo_4.setName("termo_4"); // NOI18N
        FormInput2.add(termo_4);
        termo_4.setBounds(230, 810, 23, 23);

        hipo_3.setBorder(null);
        hipo_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        hipo_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hipo_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        hipo_3.setName("hipo_3"); // NOI18N
        FormInput2.add(hipo_3);
        hipo_3.setBounds(230, 860, 23, 23);

        evaluasi2_1.setBorder(null);
        evaluasi2_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi2_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi2_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi2_1.setName("evaluasi2_1"); // NOI18N
        FormInput2.add(evaluasi2_1);
        evaluasi2_1.setBounds(470, 790, 23, 23);

        evaluasi2_2.setBorder(null);
        evaluasi2_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi2_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi2_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi2_2.setName("evaluasi2_2"); // NOI18N
        FormInput2.add(evaluasi2_2);
        evaluasi2_2.setBounds(470, 810, 23, 23);

        evaluasi2_3.setBorder(null);
        evaluasi2_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi2_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi2_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi2_3.setName("evaluasi2_3"); // NOI18N
        FormInput2.add(evaluasi2_3);
        evaluasi2_3.setBounds(680, 790, 23, 23);

        cairan_4.setBorder(null);
        cairan_4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cairan_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cairan_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cairan_4.setName("cairan_4"); // NOI18N
        FormInput2.add(cairan_4);
        cairan_4.setBounds(30, 1020, 23, 23);

        cairan_1.setBorder(null);
        cairan_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cairan_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cairan_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cairan_1.setName("cairan_1"); // NOI18N
        FormInput2.add(cairan_1);
        cairan_1.setBounds(30, 960, 23, 23);

        cairan_2.setBorder(null);
        cairan_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cairan_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cairan_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cairan_2.setName("cairan_2"); // NOI18N
        FormInput2.add(cairan_2);
        cairan_2.setBounds(30, 980, 23, 23);

        cairan_3.setBorder(null);
        cairan_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cairan_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cairan_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cairan_3.setName("cairan_3"); // NOI18N
        FormInput2.add(cairan_3);
        cairan_3.setBounds(30, 1000, 23, 23);

        manaj_1.setBorder(null);
        manaj_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        manaj_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        manaj_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        manaj_1.setName("manaj_1"); // NOI18N
        FormInput2.add(manaj_1);
        manaj_1.setBounds(470, 960, 23, 23);

        manaj_2.setBorder(null);
        manaj_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        manaj_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        manaj_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        manaj_2.setName("manaj_2"); // NOI18N
        FormInput2.add(manaj_2);
        manaj_2.setBounds(470, 980, 23, 23);

        manaj_3.setBorder(null);
        manaj_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        manaj_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        manaj_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        manaj_3.setName("manaj_3"); // NOI18N
        FormInput2.add(manaj_3);
        manaj_3.setBounds(470, 1000, 23, 23);

        manaj_4.setBorder(null);
        manaj_4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        manaj_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        manaj_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        manaj_4.setName("manaj_4"); // NOI18N
        FormInput2.add(manaj_4);
        manaj_4.setBounds(470, 1020, 23, 23);

        evaluasi3_4.setBorder(null);
        evaluasi3_4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi3_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi3_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi3_4.setName("evaluasi3_4"); // NOI18N
        FormInput2.add(evaluasi3_4);
        evaluasi3_4.setBounds(30, 1140, 23, 23);

        evaluasi3_3.setBorder(null);
        evaluasi3_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi3_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi3_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi3_3.setName("evaluasi3_3"); // NOI18N
        FormInput2.add(evaluasi3_3);
        evaluasi3_3.setBounds(30, 1120, 20, 23);

        evaluasi3_2.setBorder(null);
        evaluasi3_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi3_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi3_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi3_2.setName("evaluasi3_2"); // NOI18N
        FormInput2.add(evaluasi3_2);
        evaluasi3_2.setBounds(30, 1100, 23, 23);

        evaluasi3_1.setBorder(null);
        evaluasi3_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        evaluasi3_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        evaluasi3_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        evaluasi3_1.setName("evaluasi3_1"); // NOI18N
        FormInput2.add(evaluasi3_1);
        evaluasi3_1.setBounds(30, 1080, 23, 23);

        label44.setText("Pendarahan :");
        label44.setName("label44"); // NOI18N
        label44.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label44);
        label44.setBounds(40, 130, 80, 23);

        suhu1.setName("suhu1"); // NOI18N
        suhu1.setPreferredSize(new java.awt.Dimension(50, 23));
        suhu1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                suhu1MouseMoved(evt);
            }
        });
        suhu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                suhu1MouseExited(evt);
            }
        });
        suhu1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                suhu1KeyPressed(evt);
            }
        });
        FormInput2.add(suhu1);
        suhu1.setBounds(390, 80, 50, 23);

        label168.setText("Kassa");
        label168.setName("label168"); // NOI18N
        label168.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label168);
        label168.setBounds(130, 130, 30, 23);

        t_suction.setName("t_suction"); // NOI18N
        t_suction.setPreferredSize(new java.awt.Dimension(50, 23));
        t_suction.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                t_suctionMouseMoved(evt);
            }
        });
        t_suction.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                t_suctionMouseExited(evt);
            }
        });
        t_suction.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                t_suctionKeyPressed(evt);
            }
        });
        FormInput2.add(t_suction);
        t_suction.setBounds(430, 130, 100, 23);

        label169.setText("cc");
        label169.setName("label169"); // NOI18N
        label169.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label169);
        label169.setBounds(530, 130, 20, 23);

        t_pencucian.setName("t_pencucian"); // NOI18N
        t_pencucian.setPreferredSize(new java.awt.Dimension(50, 23));
        t_pencucian.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                t_pencucianMouseMoved(evt);
            }
        });
        t_pencucian.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                t_pencucianMouseExited(evt);
            }
        });
        t_pencucian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                t_pencucianKeyPressed(evt);
            }
        });
        FormInput2.add(t_pencucian);
        t_pencucian.setBounds(170, 160, 100, 23);

        label170.setText("cc");
        label170.setName("label170"); // NOI18N
        label170.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label170);
        label170.setBounds(270, 160, 20, 23);

        chk_luka_1.setBorder(null);
        chk_luka_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_luka_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_luka_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_luka_1.setName("chk_luka_1"); // NOI18N
        FormInput2.add(chk_luka_1);
        chk_luka_1.setBounds(120, 190, 23, 23);

        label171.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label171.setText("Bersih");
        label171.setName("label171"); // NOI18N
        label171.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label171);
        label171.setBounds(150, 190, 40, 23);

        chk_luka_2.setBorder(null);
        chk_luka_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_luka_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_luka_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_luka_2.setName("chk_luka_2"); // NOI18N
        FormInput2.add(chk_luka_2);
        chk_luka_2.setBounds(190, 190, 23, 23);

        label172.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label172.setText("Terkontaminasi");
        label172.setName("label172"); // NOI18N
        label172.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label172);
        label172.setBounds(220, 190, 80, 23);

        label173.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label173.setText("Bersih Terkontaminasi");
        label173.setName("label173"); // NOI18N
        label173.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label173);
        label173.setBounds(340, 190, 110, 23);

        chk_luka_4.setBorder(null);
        chk_luka_4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_luka_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_luka_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_luka_4.setName("chk_luka_4"); // NOI18N
        FormInput2.add(chk_luka_4);
        chk_luka_4.setBounds(450, 190, 23, 23);

        label175.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label175.setText("Lembab");
        label175.setName("label175"); // NOI18N
        label175.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput2.add(label175);
        label175.setBounds(480, 190, 50, 23);

        chk_luka_3.setBorder(null);
        chk_luka_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_luka_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_luka_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_luka_3.setName("chk_luka_3"); // NOI18N
        FormInput2.add(chk_luka_3);
        chk_luka_3.setBounds(310, 190, 23, 23);

        chk_cuci_3.setBorder(null);
        chk_cuci_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_3.setName("chk_cuci_3"); // NOI18N
        FormInput2.add(chk_cuci_3);
        chk_cuci_3.setBounds(30, 510, 23, 23);

        chk_cuci_4.setBorder(null);
        chk_cuci_4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_cuci_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_cuci_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_cuci_4.setName("chk_cuci_4"); // NOI18N
        FormInput2.add(chk_cuci_4);
        chk_cuci_4.setBounds(260, 470, 23, 23);

        chk_kontrol_2.setBorder(null);
        chk_kontrol_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chk_kontrol_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chk_kontrol_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chk_kontrol_2.setName("chk_kontrol_2"); // NOI18N
        FormInput2.add(chk_kontrol_2);
        chk_kontrol_2.setBounds(210, 320, 23, 23);

        scrollInput2.setViewportView(FormInput2);

        internalFrame4.add(scrollInput2, java.awt.BorderLayout.CENTER);

        PanelAccor1.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor1.setName("PanelAccor1"); // NOI18N
        PanelAccor1.setPreferredSize(new java.awt.Dimension(445, 43));
        PanelAccor1.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor1.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor1.setSelected(true);
        ChkAccor1.setFocusable(false);
        ChkAccor1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor1.setName("ChkAccor1"); // NOI18N
        ChkAccor1.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor1.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor1.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccor1ActionPerformed(evt);
            }
        });
        PanelAccor1.add(ChkAccor1, java.awt.BorderLayout.WEST);

        TabData1.setBackground(new java.awt.Color(254, 255, 254));
        TabData1.setForeground(new java.awt.Color(50, 50, 50));
        TabData1.setName("TabData1"); // NOI18N
        TabData1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabData1MouseClicked(evt);
            }
        });

        FormTelaah1.setBackground(new java.awt.Color(255, 255, 255));
        FormTelaah1.setBorder(null);
        FormTelaah1.setName("FormTelaah1"); // NOI18N
        FormTelaah1.setPreferredSize(new java.awt.Dimension(115, 73));
        FormTelaah1.setLayout(new java.awt.BorderLayout());

        FormPass4.setBackground(new java.awt.Color(255, 255, 255));
        FormPass4.setBorder(null);
        FormPass4.setName("FormPass4"); // NOI18N
        FormPass4.setPreferredSize(new java.awt.Dimension(115, 40));

        BtnRefreshPhoto2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto2.setMnemonic('U');
        BtnRefreshPhoto2.setText("Refresh");
        BtnRefreshPhoto2.setToolTipText("Alt+U");
        BtnRefreshPhoto2.setName("BtnRefreshPhoto2"); // NOI18N
        BtnRefreshPhoto2.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto2ActionPerformed(evt);
            }
        });
        FormPass4.add(BtnRefreshPhoto2);

        FormTelaah1.add(FormPass4, java.awt.BorderLayout.PAGE_END);

        Scroll8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll8.setName("Scroll8"); // NOI18N
        Scroll8.setOpaque(true);
        Scroll8.setPreferredSize(new java.awt.Dimension(200, 200));

        tbInOp.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbInOp.setComponentPopupMenu(Popup2);
        tbInOp.setName("tbInOp"); // NOI18N
        tbInOp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbInOpMouseClicked(evt);
            }
        });
        tbInOp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbInOpKeyPressed(evt);
            }
        });
        Scroll8.setViewportView(tbInOp);

        FormTelaah1.add(Scroll8, java.awt.BorderLayout.CENTER);

        TabData1.addTab("Data Intra Operatif", FormTelaah1);

        PanelAccor1.add(TabData1, java.awt.BorderLayout.CENTER);

        internalFrame4.add(PanelAccor1, java.awt.BorderLayout.EAST);

        TabOK.addTab("Perawatan Intra Operatif", internalFrame4);

        internalFrame5.setBackground(new java.awt.Color(255, 255, 255));
        internalFrame5.setBorder(null);
        internalFrame5.setName("internalFrame5"); // NOI18N
        internalFrame5.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi7.setName("panelisi7"); // NOI18N
        panelisi7.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        label13.setText("Key Word :");
        label13.setName("label13"); // NOI18N
        label13.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi7.add(label13);

        TCari2.setEditable(false);
        TCari2.setName("TCari2"); // NOI18N
        TCari2.setPreferredSize(new java.awt.Dimension(350, 23));
        TCari2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCari2KeyPressed(evt);
            }
        });
        panelisi7.add(TCari2);

        BtnCari2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari2.setMnemonic('1');
        BtnCari2.setToolTipText("Alt+1");
        BtnCari2.setName("BtnCari2"); // NOI18N
        BtnCari2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCari2ActionPerformed(evt);
            }
        });
        BtnCari2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCari2KeyPressed(evt);
            }
        });
        panelisi7.add(BtnCari2);

        BtnAll2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll2.setMnemonic('2');
        BtnAll2.setToolTipText("Alt+2");
        BtnAll2.setName("BtnAll2"); // NOI18N
        BtnAll2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAll2ActionPerformed(evt);
            }
        });
        BtnAll2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAll2KeyPressed(evt);
            }
        });
        panelisi7.add(BtnAll2);

        label16.setText("Record :");
        label16.setName("label16"); // NOI18N
        label16.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi7.add(label16);

        LCount2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount2.setText("0");
        LCount2.setName("LCount2"); // NOI18N
        LCount2.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi7.add(LCount2);

        internalFrame5.add(panelisi7, java.awt.BorderLayout.PAGE_END);

        scrollInput3.setName("scrollInput3"); // NOI18N
        scrollInput3.setPreferredSize(new java.awt.Dimension(102, 200));

        FormInput3.setBackground(new java.awt.Color(255, 255, 255));
        FormInput3.setBorder(null);
        FormInput3.setName("FormInput3"); // NOI18N
        FormInput3.setPreferredSize(new java.awt.Dimension(870, 1250));
        FormInput3.setLayout(null);

        TNoRw3.setHighlighter(null);
        TNoRw3.setName("TNoRw3"); // NOI18N
        TNoRw3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRw3KeyPressed(evt);
            }
        });
        FormInput3.add(TNoRw3);
        TNoRw3.setBounds(74, 10, 131, 23);

        TPasien3.setEditable(false);
        TPasien3.setHighlighter(null);
        TPasien3.setName("TPasien3"); // NOI18N
        FormInput3.add(TPasien3);
        TPasien3.setBounds(309, 10, 260, 23);

        TNoRM3.setEditable(false);
        TNoRM3.setHighlighter(null);
        TNoRM3.setName("TNoRM3"); // NOI18N
        FormInput3.add(TNoRM3);
        TNoRM3.setBounds(207, 10, 100, 23);

        label17.setText("Petugas :");
        label17.setName("label17"); // NOI18N
        label17.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput3.add(label17);
        label17.setBounds(0, 40, 70, 23);

        KdPetugas2.setEditable(false);
        KdPetugas2.setName("KdPetugas2"); // NOI18N
        KdPetugas2.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugas2KeyPressed(evt);
            }
        });
        FormInput3.add(KdPetugas2);
        KdPetugas2.setBounds(74, 40, 100, 23);

        NmPetugas2.setEditable(false);
        NmPetugas2.setName("NmPetugas2"); // NOI18N
        NmPetugas2.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput3.add(NmPetugas2);
        NmPetugas2.setBounds(176, 40, 180, 23);

        BtnPetugas2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas2.setMnemonic('2');
        BtnPetugas2.setToolTipText("Alt+2");
        BtnPetugas2.setName("BtnPetugas2"); // NOI18N
        BtnPetugas2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugas2ActionPerformed(evt);
            }
        });
        BtnPetugas2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugas2KeyPressed(evt);
            }
        });
        FormInput3.add(BtnPetugas2);
        BtnPetugas2.setBounds(358, 40, 28, 23);

        jLabel13.setText("No.Rawat :");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput3.add(jLabel13);
        jLabel13.setBounds(0, 10, 70, 23);

        jSeparator11.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator11.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator11.setName("jSeparator11"); // NOI18N
        FormInput3.add(jSeparator11);
        jSeparator11.setBounds(0, 70, 880, 1);

        jSeparator12.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator12.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator12.setName("jSeparator12"); // NOI18N
        FormInput3.add(jSeparator12);
        jSeparator12.setBounds(0, 120, 880, 1);

        label106.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label106.setText(":");
        label106.setName("label106"); // NOI18N
        label106.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label106);
        label106.setBounds(130, 130, 10, 23);

        jn1.setBorder(null);
        jn1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jn1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jn1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jn1.setName("jn1"); // NOI18N
        FormInput3.add(jn1);
        jn1.setBounds(80, 330, 23, 23);

        sn2.setBorder(null);
        sn2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        sn2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sn2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        sn2.setName("sn2"); // NOI18N
        FormInput3.add(sn2);
        sn2.setBounds(210, 250, 23, 23);

        sn3.setBorder(null);
        sn3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        sn3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sn3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        sn3.setName("sn3"); // NOI18N
        FormInput3.add(sn3);
        sn3.setBounds(280, 250, 23, 23);

        label174.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label174.setText("Sianosis menurun");
        label174.setName("label174"); // NOI18N
        label174.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label174);
        label174.setBounds(110, 330, 110, 23);

        label176.setText("Suara Napas");
        label176.setName("label176"); // NOI18N
        label176.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label176);
        label176.setBounds(50, 250, 70, 23);

        label177.setText("Kesadaran");
        label177.setName("label177"); // NOI18N
        label177.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label177);
        label177.setBounds(60, 220, 60, 20);

        label178.setText("Kondisi Kulit");
        label178.setName("label178"); // NOI18N
        label178.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label178);
        label178.setBounds(60, 190, 60, 23);

        label180.setText("TD :");
        label180.setName("label180"); // NOI18N
        label180.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput3.add(label180);
        label180.setBounds(20, 80, 30, 23);

        td2.setHighlighter(null);
        td2.setName("td2"); // NOI18N
        td2.setPreferredSize(new java.awt.Dimension(50, 23));
        td2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                td2MouseMoved(evt);
            }
        });
        td2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                td2MouseExited(evt);
            }
        });
        td2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                td2KeyPressed(evt);
            }
        });
        FormInput3.add(td2);
        td2.setBounds(50, 80, 50, 23);

        label181.setText("SpO2 :");
        label181.setName("label181"); // NOI18N
        label181.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label181);
        label181.setBounds(100, 80, 40, 23);

        spo2.setName("spo2"); // NOI18N
        spo2.setPreferredSize(new java.awt.Dimension(50, 23));
        spo2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                spo2MouseMoved(evt);
            }
        });
        spo2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                spo2MouseExited(evt);
            }
        });
        spo2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                spo2ActionPerformed(evt);
            }
        });
        spo2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                spo2KeyPressed(evt);
            }
        });
        FormInput3.add(spo2);
        spo2.setBounds(140, 80, 50, 23);

        label182.setText("HR :");
        label182.setName("label182"); // NOI18N
        label182.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label182);
        label182.setBounds(190, 80, 30, 23);

        hr2.setName("hr2"); // NOI18N
        hr2.setPreferredSize(new java.awt.Dimension(50, 23));
        hr2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                hr2MouseMoved(evt);
            }
        });
        hr2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                hr2MouseExited(evt);
            }
        });
        hr2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                hr2KeyPressed(evt);
            }
        });
        FormInput3.add(hr2);
        hr2.setBounds(220, 80, 50, 23);

        label183.setText("RR :");
        label183.setName("label183"); // NOI18N
        label183.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label183);
        label183.setBounds(270, 80, 30, 23);

        rr2.setName("rr2"); // NOI18N
        rr2.setPreferredSize(new java.awt.Dimension(50, 23));
        rr2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                rr2MouseMoved(evt);
            }
        });
        rr2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                rr2MouseExited(evt);
            }
        });
        rr2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rr2KeyPressed(evt);
            }
        });
        FormInput3.add(rr2);
        rr2.setBounds(300, 80, 50, 23);

        label184.setText("Suhu :");
        label184.setName("label184"); // NOI18N
        label184.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label184);
        label184.setBounds(340, 80, 50, 23);

        jLabel19.setText("Tanggal :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setVerifyInputWhenFocusTarget(false);
        FormInput3.add(jLabel19);
        jLabel19.setBounds(380, 40, 75, 23);

        Tanggal2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "24-10-2024" }));
        Tanggal2.setDisplayFormat("dd-MM-yyyy");
        Tanggal2.setName("Tanggal2"); // NOI18N
        Tanggal2.setOpaque(false);
        Tanggal2.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput3.add(Tanggal2);
        Tanggal2.setBounds(460, 40, 110, 23);

        Jam2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam2.setName("Jam2"); // NOI18N
        Jam2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam2ActionPerformed(evt);
            }
        });
        Jam2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam2KeyPressed(evt);
            }
        });
        FormInput3.add(Jam2);
        Jam2.setBounds(580, 40, 50, 23);

        Menit2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit2.setName("Menit2"); // NOI18N
        Menit2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit2KeyPressed(evt);
            }
        });
        FormInput3.add(Menit2);
        Menit2.setBounds(640, 40, 50, 23);

        Detik2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik2.setName("Detik2"); // NOI18N
        Detik2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik2KeyPressed(evt);
            }
        });
        FormInput3.add(Detik2);
        Detik2.setBounds(700, 40, 50, 23);

        ChkJam3.setBorder(null);
        ChkJam3.setSelected(true);
        ChkJam3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam3.setName("ChkJam3"); // NOI18N
        FormInput3.add(ChkJam3);
        ChkJam3.setBounds(750, 40, 23, 23);

        jk2.setEditable(false);
        jk2.setName("jk2"); // NOI18N
        jk2.setPreferredSize(new java.awt.Dimension(50, 23));
        jk2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jk2MouseMoved(evt);
            }
        });
        jk2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jk2MouseExited(evt);
            }
        });
        jk2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jk2ActionPerformed(evt);
            }
        });
        jk2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jk2KeyPressed(evt);
            }
        });
        FormInput3.add(jk2);
        jk2.setBounds(580, 10, 50, 23);

        label185.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label185.setText("D.0001 Bersihkan jalan napas tidak efektif b.d efek agen farmakologis");
        label185.setName("label185"); // NOI18N
        label185.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label185);
        label185.setBounds(60, 280, 490, 23);

        label187.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label187.setText("L.01001 Bersihan Jalan Napas");
        label187.setName("label187"); // NOI18N
        label187.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label187);
        label187.setBounds(60, 300, 250, 23);

        label188.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label188.setText("I.03117 Manajemen Mual");
        label188.setName("label188"); // NOI18N
        label188.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label188);
        label188.setBounds(50, 640, 240, 23);

        label200.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label200.setText("Evaluasi");
        label200.setName("label200"); // NOI18N
        label200.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label200);
        label200.setBounds(50, 690, 50, 23);

        label201.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label201.setText("Keluhan mual menurun");
        label201.setName("label201"); // NOI18N
        label201.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label201);
        label201.setBounds(110, 710, 200, 23);

        label202.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label202.setText("Perasaan ingin muntah menurun");
        label202.setName("label202"); // NOI18N
        label202.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label202);
        label202.setBounds(110, 730, 200, 23);

        label204.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label204.setText("L.08066 Tingkat Nyeri");
        label204.setName("label204"); // NOI18N
        label204.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label204);
        label204.setBounds(50, 800, 250, 23);

        jSeparator14.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator14.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator14.setName("jSeparator14"); // NOI18N
        FormInput3.add(jSeparator14);
        jSeparator14.setBounds(20, 763, 880, 3);

        label219.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label219.setText("Evaluasi");
        label219.setName("label219"); // NOI18N
        label219.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label219);
        label219.setBounds(50, 1120, 50, 23);

        label220.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label220.setText("Gelisah meurun");
        label220.setName("label220"); // NOI18N
        label220.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label220);
        label220.setBounds(90, 870, 160, 20);

        label221.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label221.setText("Keluhan nyeri meurun");
        label221.setName("label221"); // NOI18N
        label221.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label221);
        label221.setBounds(90, 850, 120, 20);

        label223.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label223.setText("Meringis menurun");
        label223.setName("label223"); // NOI18N
        label223.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label223);
        label223.setBounds(90, 890, 150, 20);

        label224.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label224.setText("I.08238 Manajemen nyeri");
        label224.setName("label224"); // NOI18N
        label224.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label224);
        label224.setBounds(500, 800, 180, 23);

        label225.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label225.setText("Identifikasi skala nyeri");
        label225.setName("label225"); // NOI18N
        label225.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label225);
        label225.setBounds(530, 820, 200, 20);

        label226.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label226.setText("Mengajarkan tariik napas dalam, teknik distraksi");
        label226.setName("label226"); // NOI18N
        label226.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label226);
        label226.setBounds(530, 840, 260, 20);

        label227.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label227.setText("Kolaborasi pemberian analgetik (jika perlu)");
        label227.setName("label227"); // NOI18N
        label227.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label227);
        label227.setBounds(530, 860, 240, 20);

        label229.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label229.setText("Evaluasi :");
        label229.setName("label229"); // NOI18N
        label229.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label229);
        label229.setBounds(510, 890, 50, 23);

        label230.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label230.setText("Risiko jatuh dari tempat tidur");
        label230.setName("label230"); // NOI18N
        label230.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label230);
        label230.setBounds(90, 990, 200, 20);

        label231.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label231.setText("Pasang handrail tempat tidur");
        label231.setName("label231"); // NOI18N
        label231.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label231);
        label231.setBounds(90, 1040, 160, 20);

        label232.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label232.setText("Pastikan roda tempat tidur terkunci");
        label232.setName("label232"); // NOI18N
        label232.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label232);
        label232.setBounds(90, 1060, 180, 20);

        label233.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label233.setText("Pasatikan pasien tidak ditinggal sendirian");
        label233.setName("label233"); // NOI18N
        label233.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label233);
        label233.setBounds(90, 1080, 220, 20);

        label235.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label235.setText("Wheezing");
        label235.setName("label235"); // NOI18N
        label235.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label235);
        label235.setBounds(310, 250, 60, 23);

        label236.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label236.setText("Snoring");
        label236.setName("label236"); // NOI18N
        label236.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label236);
        label236.setBounds(240, 250, 40, 23);

        sn4.setBorder(null);
        sn4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        sn4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sn4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        sn4.setName("sn4"); // NOI18N
        FormInput3.add(sn4);
        sn4.setBounds(360, 250, 23, 23);

        label237.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label237.setText("Vesikuler");
        label237.setName("label237"); // NOI18N
        label237.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label237);
        label237.setBounds(390, 250, 50, 23);

        sn1.setBorder(null);
        sn1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        sn1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sn1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        sn1.setName("sn1"); // NOI18N
        sn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sn1ActionPerformed(evt);
            }
        });
        FormInput3.add(sn1);
        sn1.setBounds(140, 250, 23, 23);

        label241.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label241.setText("Monitor pola napas");
        label241.setName("label241"); // NOI18N
        label241.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label241);
        label241.setBounds(110, 430, 120, 23);

        mjn1.setBorder(null);
        mjn1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mjn1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mjn1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mjn1.setName("mjn1"); // NOI18N
        FormInput3.add(mjn1);
        mjn1.setBounds(80, 430, 23, 23);

        ev_2_1.setBorder(null);
        ev_2_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ev_2_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ev_2_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ev_2_1.setName("ev_2_1"); // NOI18N
        FormInput3.add(ev_2_1);
        ev_2_1.setBounds(80, 710, 23, 23);

        ev_2_2.setBorder(null);
        ev_2_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ev_2_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ev_2_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ev_2_2.setName("ev_2_2"); // NOI18N
        FormInput3.add(ev_2_2);
        ev_2_2.setBounds(80, 730, 23, 23);

        tn_1.setBorder(null);
        tn_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tn_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tn_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tn_1.setName("tn_1"); // NOI18N
        FormInput3.add(tn_1);
        tn_1.setBounds(60, 850, 23, 23);

        tn_2.setBorder(null);
        tn_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tn_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tn_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tn_2.setName("tn_2"); // NOI18N
        FormInput3.add(tn_2);
        tn_2.setBounds(60, 870, 23, 23);

        tn_3.setBorder(null);
        tn_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tn_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tn_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tn_3.setName("tn_3"); // NOI18N
        FormInput3.add(tn_3);
        tn_3.setBounds(60, 890, 23, 23);

        mn_1.setBorder(null);
        mn_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mn_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mn_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mn_1.setName("mn_1"); // NOI18N
        FormInput3.add(mn_1);
        mn_1.setBounds(500, 820, 23, 23);

        mn_2.setBorder(null);
        mn_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mn_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mn_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mn_2.setName("mn_2"); // NOI18N
        FormInput3.add(mn_2);
        mn_2.setBounds(500, 840, 23, 23);

        mn_3.setBorder(null);
        mn_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mn_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mn_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mn_3.setName("mn_3"); // NOI18N
        FormInput3.add(mn_3);
        mn_3.setBounds(500, 860, 23, 23);

        pj_2.setBorder(null);
        pj_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        pj_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        pj_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pj_2.setName("pj_2"); // NOI18N
        FormInput3.add(pj_2);
        pj_2.setBounds(60, 1060, 23, 23);

        pj_1.setBorder(null);
        pj_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        pj_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        pj_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pj_1.setName("pj_1"); // NOI18N
        FormInput3.add(pj_1);
        pj_1.setBounds(60, 1040, 23, 23);

        jatuh.setBorder(null);
        jatuh.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jatuh.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jatuh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jatuh.setName("jatuh"); // NOI18N
        FormInput3.add(jatuh);
        jatuh.setBounds(60, 990, 23, 23);

        label243.setText("Skala nyeri");
        label243.setName("label243"); // NOI18N
        label243.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label243);
        label243.setBounds(140, 130, 60, 23);

        suhu2.setName("suhu2"); // NOI18N
        suhu2.setPreferredSize(new java.awt.Dimension(50, 23));
        suhu2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                suhu2MouseMoved(evt);
            }
        });
        suhu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                suhu2MouseExited(evt);
            }
        });
        suhu2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                suhu2KeyPressed(evt);
            }
        });
        FormInput3.add(suhu2);
        suhu2.setBounds(390, 80, 50, 23);

        s1.setBorder(null);
        s1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        s1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        s1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        s1.setName("s1"); // NOI18N
        FormInput3.add(s1);
        s1.setBounds(140, 220, 23, 23);

        label247.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label247.setText("CM");
        label247.setName("label247"); // NOI18N
        label247.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label247);
        label247.setBounds(170, 220, 40, 23);

        s2.setBorder(null);
        s2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        s2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        s2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        s2.setName("s2"); // NOI18N
        FormInput3.add(s2);
        s2.setBounds(210, 220, 23, 23);

        label248.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label248.setText("Apastis");
        label248.setName("label248"); // NOI18N
        label248.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label248);
        label248.setBounds(240, 220, 40, 23);

        label249.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label249.setText("Sopor");
        label249.setName("label249"); // NOI18N
        label249.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label249);
        label249.setBounds(310, 220, 40, 23);

        s4.setBorder(null);
        s4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        s4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        s4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        s4.setName("s4"); // NOI18N
        FormInput3.add(s4);
        s4.setBounds(360, 220, 23, 23);

        label250.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label250.setText("Coma");
        label250.setName("label250"); // NOI18N
        label250.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label250);
        label250.setBounds(390, 220, 30, 23);

        s3.setBorder(null);
        s3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        s3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        s3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        s3.setName("s3"); // NOI18N
        FormInput3.add(s3);
        s3.setBounds(280, 220, 23, 23);

        label179.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label179.setText("Gurgling");
        label179.setName("label179"); // NOI18N
        label179.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label179);
        label179.setBounds(170, 250, 40, 23);

        label234.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label234.setText(":");
        label234.setName("label234"); // NOI18N
        label234.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label234);
        label234.setBounds(130, 250, 10, 23);

        label244.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label244.setText(":");
        label244.setName("label244"); // NOI18N
        label244.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label244);
        label244.setBounds(130, 220, 10, 23);

        label245.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label245.setText(":");
        label245.setName("label245"); // NOI18N
        label245.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label245);
        label245.setBounds(130, 190, 10, 23);

        kk1.setBorder(null);
        kk1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        kk1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kk1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        kk1.setName("kk1"); // NOI18N
        FormInput3.add(kk1);
        kk1.setBounds(140, 190, 23, 23);

        label251.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label251.setText("Dingin");
        label251.setName("label251"); // NOI18N
        label251.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label251);
        label251.setBounds(170, 190, 40, 23);

        kk2.setBorder(null);
        kk2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        kk2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kk2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        kk2.setName("kk2"); // NOI18N
        FormInput3.add(kk2);
        kk2.setBounds(210, 190, 23, 23);

        label252.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label252.setText("Hangat");
        label252.setName("label252"); // NOI18N
        label252.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label252);
        label252.setBounds(240, 190, 40, 23);

        kk3.setBorder(null);
        kk3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        kk3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kk3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        kk3.setName("kk3"); // NOI18N
        FormInput3.add(kk3);
        kk3.setBounds(280, 190, 23, 23);

        label253.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label253.setText("Kering");
        label253.setName("label253"); // NOI18N
        label253.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label253);
        label253.setBounds(310, 190, 40, 23);

        kk4.setBorder(null);
        kk4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        kk4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kk4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        kk4.setName("kk4"); // NOI18N
        FormInput3.add(kk4);
        kk4.setBounds(360, 190, 23, 23);

        label254.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label254.setText("Lembab");
        label254.setName("label254"); // NOI18N
        label254.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label254);
        label254.setBounds(390, 190, 50, 23);

        kk5.setBorder(null);
        kk5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        kk5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kk5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        kk5.setName("kk5"); // NOI18N
        FormInput3.add(kk5);
        kk5.setBounds(440, 190, 23, 23);

        label255.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label255.setText("Pucat");
        label255.setName("label255"); // NOI18N
        label255.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label255);
        label255.setBounds(470, 190, 40, 23);

        k1.setBorder(null);
        k1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        k1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        k1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        k1.setName("k1"); // NOI18N
        FormInput3.add(k1);
        k1.setBounds(140, 160, 23, 23);

        label256.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label256.setText("Mual");
        label256.setName("label256"); // NOI18N
        label256.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label256);
        label256.setBounds(170, 160, 40, 23);

        k2.setBorder(null);
        k2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        k2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        k2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        k2.setName("k2"); // NOI18N
        FormInput3.add(k2);
        k2.setBounds(210, 160, 23, 23);

        label257.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label257.setText("Muntah");
        label257.setName("label257"); // NOI18N
        label257.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label257);
        label257.setBounds(240, 160, 40, 23);

        label258.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label258.setText("Pusing");
        label258.setName("label258"); // NOI18N
        label258.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label258);
        label258.setBounds(310, 160, 40, 23);

        k3.setBorder(null);
        k3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        k3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        k3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        k3.setName("k3"); // NOI18N
        FormInput3.add(k3);
        k3.setBounds(280, 160, 23, 23);

        label259.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label259.setText("Tidak bisa dikaji");
        label259.setName("label259"); // NOI18N
        label259.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label259);
        label259.setBounds(310, 130, 90, 23);

        chknyeri3.setBorder(null);
        chknyeri3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chknyeri3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chknyeri3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        chknyeri3.setName("chknyeri3"); // NOI18N
        FormInput3.add(chknyeri3);
        chknyeri3.setBounds(280, 130, 23, 23);

        nyeri_1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        nyeri_1.setName("nyeri_1"); // NOI18N
        nyeri_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nyeri_1ActionPerformed(evt);
            }
        });
        nyeri_1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nyeri_1KeyPressed(evt);
            }
        });
        FormInput3.add(nyeri_1);
        nyeri_1.setBounds(210, 130, 60, 23);

        label246.setText("Keluhan");
        label246.setName("label246"); // NOI18N
        label246.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label246);
        label246.setBounds(40, 130, 80, 23);

        jn2.setBorder(null);
        jn2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jn2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jn2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jn2.setName("jn2"); // NOI18N
        FormInput3.add(jn2);
        jn2.setBounds(80, 350, 23, 23);

        label186.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label186.setText("Pola napas membaik");
        label186.setName("label186"); // NOI18N
        label186.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label186);
        label186.setBounds(110, 350, 110, 23);

        label190.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label190.setText("Wheezing/rokhing/gurgling menurun");
        label190.setName("label190"); // NOI18N
        label190.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label190);
        label190.setBounds(110, 370, 190, 23);

        jn3.setBorder(null);
        jn3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jn3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jn3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jn3.setName("jn3"); // NOI18N
        FormInput3.add(jn3);
        jn3.setBounds(80, 370, 23, 23);

        label191.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label191.setText("Dispnea menurun");
        label191.setName("label191"); // NOI18N
        label191.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label191);
        label191.setBounds(330, 370, 120, 23);

        jn6.setBorder(null);
        jn6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jn6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jn6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jn6.setName("jn6"); // NOI18N
        FormInput3.add(jn6);
        jn6.setBounds(300, 370, 23, 23);

        jn5.setBorder(null);
        jn5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jn5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jn5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jn5.setName("jn5"); // NOI18N
        FormInput3.add(jn5);
        jn5.setBounds(300, 350, 23, 23);

        label192.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label192.setText("Frekuensi napas membaik");
        label192.setName("label192"); // NOI18N
        label192.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label192);
        label192.setBounds(330, 350, 180, 23);

        label193.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label193.setText("Gelisah");
        label193.setName("label193"); // NOI18N
        label193.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label193);
        label193.setBounds(330, 330, 110, 23);

        jn4.setBorder(null);
        jn4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jn4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jn4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jn4.setName("jn4"); // NOI18N
        FormInput3.add(jn4);
        jn4.setBounds(300, 330, 23, 23);

        label260.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label260.setText("Pertahankan kepatehan jalan napas");
        label260.setName("label260"); // NOI18N
        label260.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label260);
        label260.setBounds(110, 450, 190, 23);

        mjn2.setBorder(null);
        mjn2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mjn2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mjn2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mjn2.setName("mjn2"); // NOI18N
        FormInput3.add(mjn2);
        mjn2.setBounds(80, 450, 23, 23);

        label261.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label261.setText("Lakukan penghisapan lendir");
        label261.setName("label261"); // NOI18N
        label261.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label261);
        label261.setBounds(110, 470, 180, 23);

        mjn3.setBorder(null);
        mjn3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mjn3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mjn3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mjn3.setName("mjn3"); // NOI18N
        FormInput3.add(mjn3);
        mjn3.setBounds(80, 470, 23, 23);

        label262.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label262.setText("Ajarkan batuk efektif");
        label262.setName("label262"); // NOI18N
        label262.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label262);
        label262.setBounds(330, 470, 180, 23);

        mjn6.setBorder(null);
        mjn6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mjn6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mjn6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mjn6.setName("mjn6"); // NOI18N
        FormInput3.add(mjn6);
        mjn6.setBounds(300, 470, 23, 23);

        mjn5.setBorder(null);
        mjn5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mjn5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mjn5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mjn5.setName("mjn5"); // NOI18N
        FormInput3.add(mjn5);
        mjn5.setBounds(300, 450, 23, 23);

        label263.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label263.setText("Berikan oksigen");
        label263.setName("label263"); // NOI18N
        label263.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label263);
        label263.setBounds(330, 450, 190, 23);

        label264.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label264.setText("Monitor bunyi napas");
        label264.setName("label264"); // NOI18N
        label264.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label264);
        label264.setBounds(330, 430, 120, 20);

        mjn4.setBorder(null);
        mjn4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mjn4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mjn4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mjn4.setName("mjn4"); // NOI18N
        FormInput3.add(mjn4);
        mjn4.setBounds(300, 430, 23, 23);

        label194.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label194.setText("Evaluasi");
        label194.setName("label194"); // NOI18N
        label194.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label194);
        label194.setBounds(60, 510, 150, 23);

        ev_1_1.setBorder(null);
        ev_1_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ev_1_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ev_1_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ev_1_1.setName("ev_1_1"); // NOI18N
        FormInput3.add(ev_1_1);
        ev_1_1.setBounds(80, 530, 23, 23);

        label265.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label265.setText("Pola nafas membaik");
        label265.setName("label265"); // NOI18N
        label265.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label265);
        label265.setBounds(110, 530, 120, 23);

        label266.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label266.setText("Frekuensi napas membaik");
        label266.setName("label266"); // NOI18N
        label266.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label266);
        label266.setBounds(260, 530, 130, 23);

        ev_1_2.setBorder(null);
        ev_1_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ev_1_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ev_1_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ev_1_2.setName("ev_1_2"); // NOI18N
        FormInput3.add(ev_1_2);
        ev_1_2.setBounds(230, 530, 23, 23);

        label195.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label195.setText("I.01001 Manajemen Jalan Napas");
        label195.setName("label195"); // NOI18N
        label195.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label195);
        label195.setBounds(60, 410, 190, 23);

        label196.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label196.setText("D.0076 Nausea/mual b.d efek agen farmakologis");
        label196.setName("label196"); // NOI18N
        label196.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label196);
        label196.setBounds(50, 570, 240, 23);

        kel_1.setBorder(null);
        kel_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        kel_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kel_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        kel_1.setName("kel_1"); // NOI18N
        FormInput3.add(kel_1);
        kel_1.setBounds(80, 610, 23, 23);

        label267.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label267.setText("Keluhan mual menurun");
        label267.setName("label267"); // NOI18N
        label267.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label267);
        label267.setBounds(110, 610, 120, 23);

        kel_2.setBorder(null);
        kel_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        kel_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kel_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        kel_2.setName("kel_2"); // NOI18N
        FormInput3.add(kel_2);
        kel_2.setBounds(230, 610, 23, 23);

        label268.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label268.setText("Perasaan ingin muntah menurun");
        label268.setName("label268"); // NOI18N
        label268.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label268);
        label268.setBounds(260, 610, 160, 23);

        label197.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label197.setText("L.08065 Tingkat Nausea / Mual");
        label197.setName("label197"); // NOI18N
        label197.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label197);
        label197.setBounds(50, 590, 240, 23);

        mual_1.setBorder(null);
        mual_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mual_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mual_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mual_1.setName("mual_1"); // NOI18N
        FormInput3.add(mual_1);
        mual_1.setBounds(80, 660, 23, 23);

        label269.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label269.setText("Ajarkan relaksasi");
        label269.setName("label269"); // NOI18N
        label269.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label269);
        label269.setBounds(110, 660, 120, 23);

        mual_2.setBorder(null);
        mual_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mual_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mual_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mual_2.setName("mual_2"); // NOI18N
        FormInput3.add(mual_2);
        mual_2.setBounds(230, 660, 23, 23);

        label270.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label270.setText("Kolaborasi pemberian antiemetik, jika perlu");
        label270.setName("label270"); // NOI18N
        label270.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label270);
        label270.setBounds(260, 660, 210, 23);

        label271.setText("Skala nyeri");
        label271.setName("label271"); // NOI18N
        label271.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label271);
        label271.setBounds(60, 830, 60, 23);

        nyeri_2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        nyeri_2.setName("nyeri_2"); // NOI18N
        nyeri_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nyeri_2ActionPerformed(evt);
            }
        });
        nyeri_2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nyeri_2KeyPressed(evt);
            }
        });
        FormInput3.add(nyeri_2);
        nyeri_2.setBounds(130, 830, 60, 23);

        label272.setText("Skala nyeri");
        label272.setName("label272"); // NOI18N
        label272.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label272);
        label272.setBounds(500, 910, 60, 23);

        nyeri_3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        nyeri_3.setName("nyeri_3"); // NOI18N
        nyeri_3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nyeri_3ActionPerformed(evt);
            }
        });
        nyeri_3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nyeri_3KeyPressed(evt);
            }
        });
        FormInput3.add(nyeri_3);
        nyeri_3.setBounds(570, 910, 60, 23);

        label222.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label222.setText("D.0077 Nyeri akut b.d agen pencedera fisik (trauma)");
        label222.setName("label222"); // NOI18N
        label222.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label222);
        label222.setBounds(50, 780, 260, 23);

        pj_3.setBorder(null);
        pj_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        pj_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        pj_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pj_3.setName("pj_3"); // NOI18N
        FormInput3.add(pj_3);
        pj_3.setBounds(60, 1080, 23, 23);

        label228.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label228.setText("D.0143 Risiko jatuh b.d kondisi pasca operasi");
        label228.setName("label228"); // NOI18N
        label228.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label228);
        label228.setBounds(50, 940, 230, 23);

        label238.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label238.setText("L.14138 Tingkat Jatuh");
        label238.setName("label238"); // NOI18N
        label238.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label238);
        label238.setBounds(50, 960, 230, 23);

        label239.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label239.setText("Post Operasi Ke :");
        label239.setName("label239"); // NOI18N
        label239.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label239);
        label239.setBounds(500, 1000, 90, 23);

        ev_3_2.setBorder(null);
        ev_3_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ev_3_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ev_3_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ev_3_2.setName("ev_3_2"); // NOI18N
        FormInput3.add(ev_3_2);
        ev_3_2.setBounds(60, 1160, 23, 23);

        ev_3_1.setBorder(null);
        ev_3_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ev_3_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ev_3_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ev_3_1.setName("ev_3_1"); // NOI18N
        FormInput3.add(ev_3_1);
        ev_3_1.setBounds(60, 1140, 23, 23);

        label240.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label240.setText("Terpasang handrail/pengaman tempat tidur");
        label240.setName("label240"); // NOI18N
        label240.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label240);
        label240.setBounds(90, 1140, 230, 20);

        label242.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label242.setText("Pasien selalu ditemani perawat/keluarga");
        label242.setName("label242"); // NOI18N
        label242.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label242);
        label242.setBounds(90, 1160, 220, 20);

        label273.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label273.setText("I.14540 Pencegahan Jatuh");
        label273.setName("label273"); // NOI18N
        label273.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label273);
        label273.setBounds(50, 1020, 230, 23);

        po_1.setBorder(null);
        po_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        po_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        po_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        po_1.setName("po_1"); // NOI18N
        FormInput3.add(po_1);
        po_1.setBounds(500, 1020, 23, 23);

        label274.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label274.setText("Ruang Intensif");
        label274.setName("label274"); // NOI18N
        label274.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label274);
        label274.setBounds(530, 1020, 90, 20);

        po_2.setBorder(null);
        po_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        po_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        po_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        po_2.setName("po_2"); // NOI18N
        FormInput3.add(po_2);
        po_2.setBounds(500, 1040, 23, 23);

        label275.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label275.setText("Ruang Rawat Inap");
        label275.setName("label275"); // NOI18N
        label275.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label275);
        label275.setBounds(530, 1040, 100, 20);

        po_3.setBorder(null);
        po_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        po_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        po_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        po_3.setName("po_3"); // NOI18N
        FormInput3.add(po_3);
        po_3.setBounds(500, 1060, 23, 23);

        label276.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label276.setText("Rumah");
        label276.setName("label276"); // NOI18N
        label276.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label276);
        label276.setBounds(530, 1060, 40, 20);

        label277.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label277.setText("Cara Pindah :");
        label277.setName("label277"); // NOI18N
        label277.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label277);
        label277.setBounds(500, 1110, 90, 23);

        pindah_1.setBorder(null);
        pindah_1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        pindah_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        pindah_1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pindah_1.setName("pindah_1"); // NOI18N
        FormInput3.add(pindah_1);
        pindah_1.setBounds(500, 1130, 23, 23);

        label278.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label278.setText("Stretcher");
        label278.setName("label278"); // NOI18N
        label278.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label278);
        label278.setBounds(530, 1130, 90, 20);

        pindah_2.setBorder(null);
        pindah_2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        pindah_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        pindah_2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pindah_2.setName("pindah_2"); // NOI18N
        FormInput3.add(pindah_2);
        pindah_2.setBounds(500, 1150, 23, 23);

        label279.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label279.setText("Kursi Roda");
        label279.setName("label279"); // NOI18N
        label279.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label279);
        label279.setBounds(530, 1150, 100, 20);

        pindah_3.setBorder(null);
        pindah_3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        pindah_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        pindah_3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pindah_3.setName("pindah_3"); // NOI18N
        FormInput3.add(pindah_3);
        pindah_3.setBounds(500, 1170, 23, 23);

        label280.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label280.setText("Berjalan Sendiri");
        label280.setName("label280"); // NOI18N
        label280.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput3.add(label280);
        label280.setBounds(530, 1170, 100, 20);

        scrollInput3.setViewportView(FormInput3);

        internalFrame5.add(scrollInput3, java.awt.BorderLayout.CENTER);

        PanelAccor2.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor2.setName("PanelAccor2"); // NOI18N
        PanelAccor2.setPreferredSize(new java.awt.Dimension(445, 43));
        PanelAccor2.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor2.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor2.setSelected(true);
        ChkAccor2.setFocusable(false);
        ChkAccor2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor2.setName("ChkAccor2"); // NOI18N
        ChkAccor2.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor2.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor2.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor2.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccor2ActionPerformed(evt);
            }
        });
        PanelAccor2.add(ChkAccor2, java.awt.BorderLayout.WEST);

        TabData2.setBackground(new java.awt.Color(254, 255, 254));
        TabData2.setForeground(new java.awt.Color(50, 50, 50));
        TabData2.setName("TabData2"); // NOI18N
        TabData2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabData2MouseClicked(evt);
            }
        });

        FormTelaah2.setBackground(new java.awt.Color(255, 255, 255));
        FormTelaah2.setBorder(null);
        FormTelaah2.setName("FormTelaah2"); // NOI18N
        FormTelaah2.setPreferredSize(new java.awt.Dimension(115, 73));
        FormTelaah2.setLayout(new java.awt.BorderLayout());

        FormPass5.setBackground(new java.awt.Color(255, 255, 255));
        FormPass5.setBorder(null);
        FormPass5.setName("FormPass5"); // NOI18N
        FormPass5.setPreferredSize(new java.awt.Dimension(115, 40));

        BtnRefreshPhoto3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto3.setMnemonic('U');
        BtnRefreshPhoto3.setText("Refresh");
        BtnRefreshPhoto3.setToolTipText("Alt+U");
        BtnRefreshPhoto3.setName("BtnRefreshPhoto3"); // NOI18N
        BtnRefreshPhoto3.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto3ActionPerformed(evt);
            }
        });
        FormPass5.add(BtnRefreshPhoto3);

        FormTelaah2.add(FormPass5, java.awt.BorderLayout.PAGE_END);

        Scroll9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll9.setName("Scroll9"); // NOI18N
        Scroll9.setOpaque(true);
        Scroll9.setPreferredSize(new java.awt.Dimension(200, 200));

        tbPostOp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbPostOp.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbPostOp.setComponentPopupMenu(Popup2);
        tbPostOp.setName("tbPostOp"); // NOI18N
        tbPostOp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPostOpMouseClicked(evt);
            }
        });
        tbPostOp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPostOpKeyPressed(evt);
            }
        });
        Scroll9.setViewportView(tbPostOp);

        FormTelaah2.add(Scroll9, java.awt.BorderLayout.CENTER);

        TabData2.addTab("Data Post Operatif", FormTelaah2);

        PanelAccor2.add(TabData2, java.awt.BorderLayout.CENTER);

        internalFrame5.add(PanelAccor2, java.awt.BorderLayout.EAST);

        TabOK.addTab("Perawatan Post Operatif", internalFrame5);

        internalFrame1.add(TabOK, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(TNoRw1.getText().trim().equals("")){
                Valid.textKosong(TNoRw1,"Silahkan pilih data pasien terlebih dahulu !");
            }else if(spo.getText().trim().equals("")){
                Valid.textKosong(spo,"SPO");
            }else if(hr.getText().trim().equals("")){
                Valid.textKosong(hr,"HR");
            }else if(rr.getText().trim().equals("")){
                Valid.textKosong(rr,"RR");
            }else if(suhu.getText().trim().equals("")){
                Valid.textKosong(suhu,"Suhu");
            }else{     
                  String pernafasan = "",pernafasanett = "",pernafasantdk = "",cardiopurmonal="",alatbantupace="",alatbantukateter="",alatbantutdk="",kemih1="",kemih2="",kemih3="",ab_alatdengar="",ab_kacamata="";
                  if (chkPernafasan1.isSelected()) {
                            pernafasan = "true";
                        } else {
                            pernafasan = "false";
                        }

                        if (chkPernafasan2.isSelected()) {
                            pernafasanett = "true";
                        } else {
                            pernafasanett = "false";
                        }

                        if (chkPernafasan3.isSelected()) {
                            pernafasantdk = "true";
                        } else {
                            pernafasantdk = "false";
                        }
                        // NOMOR 2
                        if(chkCardiopurmonal1.isSelected()) {
                            cardiopurmonal = "false";
                        }
                        if(chkCardiopurmonal2.isSelected()) {
                            cardiopurmonal = "true";
                        }

                        // NOMOR 3
                        if(chkalatbantu1.isSelected()) {
                            alatbantupace = "true";
                        } else {
                            alatbantupace = "false";
                        }

                        if (chkalatbantu2.isSelected()) {
                            alatbantukateter = "true";
                        } else {
                            alatbantukateter = "false";
                        }
                        if (chkalatbantu3.isSelected()) {
                            alatbantutdk = "true";
                        } else {
                            alatbantutdk = "false";
                        }
                        // NOMOR 4
                        if (chkalatbantu1.isSelected()) {
                            kemih1 = "true";
                        } else {
                            kemih1 = "false";
                        }

                        if (chkalatbantu2.isSelected()) {
                            kemih2 = "true";
                        } else {
                            kemih2 = "false";
                        }
                        if (chkalatbantu3.isSelected()) {
                            kemih3 = "true";
                        } else {
                            kemih3 = "false";
                        }
                        
                        //alat bantu terakhir
                        if (chkAlatbantuFix1.isSelected()) {
                            ab_alatdengar = "true";
                        } else {
                            ab_alatdengar = "false";
                        }
                        if (chkAlatbantuFix2.isSelected()) {
                            ab_kacamata = "true";
                        } else {
                            ab_kacamata = "false";
                        }
                        
                        String tingkatnyeri1="",tingkatnyeri2="",tingkatnyeri3="";
                         //alat bantu terakhir
                        if (chkTingkatnyeri1.isSelected()) {
                            tingkatnyeri1 = "true";
                        } else {
                            tingkatnyeri1 = "false";
                        }
                        if (chkTingkatnyeri2.isSelected()) {
                            tingkatnyeri2 = "true";
                        } else {
                            tingkatnyeri2 = "false";
                        }
                        if (chkTingkatnyeri3.isSelected()) {
                            tingkatnyeri3 = "true";
                        } else {
                            tingkatnyeri3 = "false";
                        }
                        
                        String mannyeri1="",mannyeri2="",mannyeri3="";
                         //manajemen nyeri
                        if (chkManajemen1.isSelected()) {
                            mannyeri1 = "true";
                        } else {
                            mannyeri1 = "false";
                        }
                        if (chkManajemen2.isSelected()) {
                            mannyeri2 = "true";
                        } else {
                            mannyeri2 = "false";
                        }
                        if (chkManajemen3.isSelected()) {
                            mannyeri3 = "true";
                        } else {
                            mannyeri3 = "false";
                        }
                        
                        String ansietas1="",ansietas2="",ansietas3="",ansietas4="";
                         //ansietas
                        if (chkAnsietas1.isSelected()) {
                            ansietas1 = "true";
                        } else {
                            ansietas1 = "false";
                        }
                        if (chkAnsietas2.isSelected()) {
                            ansietas2 = "true";
                        } else {
                            ansietas2 = "false";
                        }
                        if (chkAnsietas3.isSelected()) {
                            ansietas3 = "true";
                        } else {
                            ansietas3 = "false";
                        }
                        if (chkAnsietas4.isSelected()) {
                            ansietas4 = "true";
                        } else {
                            ansietas4 = "false";
                        }
                        
                        String reduksi1="",reduksi2="",reduksi3="",reduksi4="",reduksi5="",reduksi6="";
                         //reduksi
                        if (chkReduksi1.isSelected()) {
                            reduksi1 = "true";
                        } else {
                            reduksi1 = "false";
                        }
                        if (chkReduksi2.isSelected()) {
                            reduksi2 = "true";
                        } else {
                            reduksi2 = "false";
                        }
                        if (chkReduksi3.isSelected()) {
                            reduksi3 = "true";
                        } else {
                            reduksi3 = "false";
                        }
                        if (chkReduksi4.isSelected()) {
                            reduksi4 = "true";
                        } else {
                            reduksi4 = "false";
                        }if (chkReduksi5.isSelected()) {
                            reduksi5 = "true";
                        } else {
                            reduksi5 = "false";
                        }
                        if (chkReduksi6.isSelected()) {
                            reduksi6 = "true";
                        } else {
                            reduksi6 = "false";
                        }
                        
                        
                         String evaluasi1="",evaluasi2="",evaluasi3="",evaluasi4="",evaluasi5="",evaluasi6="";
                         //reduksi
                        if (chkEvaluasi1.isSelected()) {
                            evaluasi1 = "true";
                        } else {
                            evaluasi1 = "false";
                        }
                        if (chkEvaluasi2.isSelected()) {
                            evaluasi2 = "true";
                        } else {
                            evaluasi2 = "false";
                        }
                        if (chkEvaluasi3.isSelected()) {
                            evaluasi3 = "true";
                        } else {
                            evaluasi3 = "false";
                        }
                        if (chkEvaluasi4.isSelected()) {
                            evaluasi4 = "true";
                        } else {
                            evaluasi4 = "false";
                        }
                        
                if(Sequel.menyimpantf("penilaian_awal_ok_preoperasi","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Awal Pre Operasi",58,new String[]{
                        TNoRw1.getText(),
                        Valid.SetTgl(Tanggal.getSelectedItem()+""),
                        Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                        KdPetugas.getText(),
                        td.getText(),
                        spo.getText(),
                        hr.getText(),
                        rr.getText(),
                        suhu.getText(),
                        cmbSkalanyeri.getSelectedItem().toString(),
                        skalanyeri_ket.getText(),
                        cmbKesadaran.getSelectedItem().toString(),
                        cmbOrientasi.getSelectedItem().toString(),
                        orientasi_ket.getText(),
                        cmbStatuspernafasan.getSelectedItem().toString(),
                        statuspernafasan_ket.getText(),
                        cmbSuaranafas.getSelectedItem().toString(),
                        cmbPernafasanalatbantu.getSelectedItem().toString(),
                        pernafasan,
                        pernafasanett,
                        pernafasantdk,
                        cardiopurmonal,
                        Cardiopurmonal_ket.getText(),
                        alatbantupace,
                        alatbantukateter,
                        alatbantutdk,
                        cmbKulit.getSelectedItem().toString(),
                        ket_kulit.getText(),
                        cmbMuskolokeletal.getSelectedItem().toString(),
                        kemih1,
                        kemih2,
                        kemih3,
                        perkemihan_ket.getText(),
                        cmbSensori.getSelectedItem().toString(),
                        ab_alatdengar,
                        ab_kacamata,
                        cmbTingkatNyeriSkala.getSelectedItem().toString(),
                        tingkatnyeri1,
                        tingkatnyeri2,
                        tingkatnyeri3,
                        mannyeri1,
                        mannyeri2,
                        mannyeri3,
                        cmbEvaluasinyeri.getSelectedItem().toString(),
                        ansietas1,
                        ansietas2,
                        ansietas3,
                        ansietas4,
                        reduksi1,
                        reduksi2,
                        reduksi3,
                        reduksi4,
                        reduksi5,
                        reduksi6,
                        evaluasi1,
                        evaluasi2,
                        evaluasi3,
                        evaluasi4
                })==true){
                    tampil();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Menyimpan Data");
            }
               
      }
        }else if(TabOK.getSelectedIndex()==1){
            if(TNoRw2.getText().trim().equals("")){
                Valid.textKosong(TNoRw2,"Silahkan pilih data pasien terlebih dahulu !");
            }else if(spo1.getText().trim().equals("")){
                Valid.textKosong(spo1,"SPO");
            }else if(hr1.getText().trim().equals("")){
                Valid.textKosong(hr1,"HR");
            }else if(rr1.getText().trim().equals("")){
                Valid.textKosong(rr1,"RR");
            }else if(suhu1.getText().trim().equals("")){
                Valid.textKosong(suhu1,"Suhu");
            }else{ 
                if(Sequel.menyimpantf("penilaian_awal_ok_intraoperatif","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Awal Intra Operatif",62,new String[]{
                        TNoRw2.getText(),
                        Valid.SetTgl(Tanggal1.getSelectedItem()+""),
                        Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                        KdPetugas1.getText(),
                        td1.getText(),
                        spo1.getText(),
                        hr1.getText(),
                        rr1.getText(),
                        suhu1.getText(),
                        t_kassa.getText(),
                        t_pencucian.getText(),
                        t_suction.getText(),
                        String.valueOf(chk_luka_1.isSelected()),
                        String.valueOf(chk_luka_2.isSelected()),
                        String.valueOf(chk_luka_3.isSelected()),
                        String.valueOf(chk_luka_4.isSelected()),
                        String.valueOf(chk_kulit_1.isSelected()),
                        String.valueOf(chk_kulit_2.isSelected()),
                        String.valueOf(chk_kulit_3.isSelected()),
                        String.valueOf(chk_kulit_4.isSelected()),
                        String.valueOf(chk_kontrol_1.isSelected()),
                        String.valueOf(chk_kontrol_2.isSelected()),
                        String.valueOf(chk_jenisluka_1.isSelected()),
                        String.valueOf(chk_jenisluka_2.isSelected()),
                        String.valueOf(chk_jenisluka_3.isSelected()),
                        String.valueOf(chk_jenisluka_4.isSelected()),
                        String.valueOf(chk_cuci_1.isSelected()),
                        String.valueOf(chk_cuci_2.isSelected()),
                        String.valueOf(chk_cuci_3.isSelected()),
                        String.valueOf(chk_cuci_4.isSelected()),
                        String.valueOf(chk_cuci_5.isSelected()),
                        String.valueOf(chk_cuci_6.isSelected()),
                        String.valueOf(chk_cuci_7.isSelected()),
                        String.valueOf(chk_cuci_8.isSelected()),
                        String.valueOf(chk_cuci_9.isSelected()),
                        String.valueOf(chk_cuci_10.isSelected()),
                        String.valueOf(evaluasi_1.isSelected()),
                        String.valueOf(evaluasi_2.isSelected()),
                        String.valueOf(evaluasi_3.isSelected()),
                        String.valueOf(termo_1.isSelected()),
                        String.valueOf(termo_2.isSelected()),
                        String.valueOf(termo_3.isSelected()),
                        String.valueOf(termo_4.isSelected()),
                        String.valueOf(evaluasi2_1.isSelected()),
                        String.valueOf(evaluasi2_2.isSelected()),
                        String.valueOf(evaluasi2_3.isSelected()),
                        String.valueOf(evaluasi2_4.isSelected()),
                        String.valueOf(hipo_1.isSelected()),
                        String.valueOf(hipo_2.isSelected()),
                        String.valueOf(hipo_3.isSelected()),
                        String.valueOf(cairan_1.isSelected()),
                        String.valueOf(cairan_2.isSelected()),
                        String.valueOf(cairan_3.isSelected()),
                        String.valueOf(cairan_4.isSelected()),
                        String.valueOf(manaj_1.isSelected()),
                        String.valueOf(manaj_2.isSelected()),
                        String.valueOf(manaj_3.isSelected()),
                        String.valueOf(manaj_4.isSelected()),
                        String.valueOf(evaluasi3_1.isSelected()),
                        String.valueOf(evaluasi3_2.isSelected()),
                        String.valueOf(evaluasi3_3.isSelected()),
                        String.valueOf(evaluasi3_4.isSelected())    
                })==true){
                    tampilinop();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Menyimpan Data");
            }  
          }
        }else if(TabOK.getSelectedIndex()==2){
           if(TNoRw3.getText().trim().equals("")){
                Valid.textKosong(TNoRw3,"Silahkan pilih data pasien terlebih dahulu !");
            }else if(spo2.getText().trim().equals("")){
                Valid.textKosong(spo2,"SPO");
            }else if(hr2.getText().trim().equals("")){
                Valid.textKosong(hr2,"HR");
            }else if(rr2.getText().trim().equals("")){
                Valid.textKosong(rr2,"RR");
            }else if(suhu2.getText().trim().equals("")){
                Valid.textKosong(suhu2,"Suhu");
            }else{    
                if(Sequel.menyimpantf("penilaian_awal_ok_postoperatif","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Awal Post Operatif",67,new String[]{
                        TNoRw2.getText(),
                        Valid.SetTgl(Tanggal2.getSelectedItem()+""),
                        Jam2.getSelectedItem()+":"+Menit2.getSelectedItem()+":"+Detik2.getSelectedItem(),
                        KdPetugas2.getText(),
                        td2.getText(),
                        spo2.getText(),
                        hr2.getText(),
                        rr2.getText(),
                        suhu2.getText(),
                        nyeri_1.getSelectedItem().toString(),
                        String.valueOf(chknyeri3.isSelected()),
                        String.valueOf(k1.isSelected()),
                        String.valueOf(k2.isSelected()),
                        String.valueOf(k3.isSelected()),
                        String.valueOf(kk1.isSelected()),
                        String.valueOf(kk2.isSelected()),
                        String.valueOf(kk3.isSelected()),
                        String.valueOf(kk4.isSelected()),
                        String.valueOf(kk5.isSelected()),
                        String.valueOf(s1.isSelected()),
                        String.valueOf(s2.isSelected()),
                        String.valueOf(s3.isSelected()),
                        String.valueOf(s4.isSelected()),
                        String.valueOf(sn1.isSelected()),
                        String.valueOf(sn2.isSelected()),
                        String.valueOf(sn3.isSelected()),
                        String.valueOf(sn4.isSelected()),
                        String.valueOf(jn1.isSelected()),
                        String.valueOf(jn2.isSelected()),
                        String.valueOf(jn3.isSelected()),
                        String.valueOf(jn4.isSelected()),
                        String.valueOf(jn5.isSelected()),
                        String.valueOf(jn6.isSelected()),
                        String.valueOf(mjn1.isSelected()),
                        String.valueOf(mjn2.isSelected()),
                        String.valueOf(mjn3.isSelected()),
                        String.valueOf(mjn4.isSelected()),
                        String.valueOf(mjn5.isSelected()),
                        String.valueOf(mjn6.isSelected()),
                        String.valueOf(ev_1_1.isSelected()),
                        String.valueOf(ev_1_2.isSelected()),
                        String.valueOf(kel_1.isSelected()),
                        String.valueOf(kel_2.isSelected()),
                        String.valueOf(mual_1.isSelected()),
                        String.valueOf(mual_2.isSelected()),
                        String.valueOf(ev_2_1.isSelected()),
                        String.valueOf(ev_2_2.isSelected()),
                        nyeri_2.getSelectedItem().toString(),
                        String.valueOf(tn_1.isSelected()),
                        String.valueOf(tn_2.isSelected()),
                        String.valueOf(tn_3.isSelected()),
                        String.valueOf(mn_1.isSelected()),
                        String.valueOf(mn_2.isSelected()),
                        String.valueOf(mn_3.isSelected()),
                        nyeri_3.getSelectedItem().toString(),
                        String.valueOf(jatuh.isSelected()),
                        String.valueOf(pj_1.isSelected()),
                        String.valueOf(pj_2.isSelected()),
                        String.valueOf(pj_3.isSelected()),
                        String.valueOf(ev_3_1.isSelected()),
                        String.valueOf(ev_3_2.isSelected()),
                        String.valueOf(po_1.isSelected()),
                        String.valueOf(po_2.isSelected()),
                        String.valueOf(po_3.isSelected()),
                        String.valueOf(pindah_1.isSelected()),
                        String.valueOf(pindah_2.isSelected()),
                        String.valueOf(pindah_3.isSelected())
                })==true){
                    tampilpostop();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Menyimpan Data");
            }  
          }
        }else if(TabOK.getSelectedIndex()==3){
           
       
        }            
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            if(TabOK.getSelectedIndex()==0){
                Valid.pindah(evt,td,BtnBatal);
            }else if(TabOK.getSelectedIndex()==1){
                Valid.pindah(evt,td,BtnBatal);
            }else if(TabOK.getSelectedIndex()==2){
                Valid.pindah(evt,td,BtnBatal);
            }else if(TabOK.getSelectedIndex()==3){
                Valid.pindah(evt,td,BtnBatal);
            }                
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(tabModePreOp.getRowCount()!=0){
                if(tbPreOp.getSelectedRow()>-1){
                    if(Sequel.queryu2tf("delete from penilaian_awal_ok_preoperasi where no_rawat=?",1,new String[]{
                    tbPreOp.getValueAt(tbPreOp.getSelectedRow(),0).toString()
                })==true){
                    tampil();
                    emptTeks();
                    
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Asuhan Pre Op terlebih dahulu..!!");
            }   
        }  
           
        }else if(TabOK.getSelectedIndex()==1){
           if(tabModeInOp.getRowCount()!=0){
                if(tbInOp.getSelectedRow()>-1){
                    if(Sequel.queryu2tf("delete from penilaian_awal_ok_intraoperatif where no_rawat=?",1,new String[]{
                    tbInOp.getValueAt(tbInOp.getSelectedRow(),0).toString()
                })==true){
                    tampilinop();
                    emptTeks();
                    
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Asuhan Intra Operatif terlebih dahulu..!!");
            }   
        }  
        }else if(TabOK.getSelectedIndex()==2){
             if(tabModePostOp.getRowCount()!=0){
                if(tbPostOp.getSelectedRow()>-1){
                    if(Sequel.queryu2tf("delete from penilaian_awal_ok_postoperatif where no_rawat=?",1,new String[]{
                    tbPostOp.getValueAt(tbPostOp.getSelectedRow(),0).toString()
                })==true){
                    tampilpostop();
                    emptTeks();
                    
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Asuhan Post Operatif terlebih dahulu..!!");
            }   
        } 
        }else if(TabOK.getSelectedIndex()==3){
         
        }             
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnHapus);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
    dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            dispose();
        }else{Valid.pindah(evt,BtnHapus,BtnKeluar);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
//        TabSettingMouseClicked(null);
    }//GEN-LAST:event_formWindowOpened

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(TNoRw1.getText().trim().equals("")){
                Valid.textKosong(TNoRw1,"Nomor Rawat");
            }else if(TNoRM1.getText().trim().equals("")){
                Valid.textKosong(TNoRM1,"Nomor RM");
            }else if(TPasien1.getText().trim().equals("")){
                Valid.textKosong(TPasien1,"Pasien");
            }else{
                if(tbPreOp.getSelectedRow()>-1){
                    String pernafasan = "",pernafasanett = "",pernafasantdk = "",cardiopurmonal="",alatbantupace="",alatbantukateter="",alatbantutdk="",kemih1="",kemih2="",kemih3="",ab_alatdengar="",ab_kacamata="";
                        if(chkPernafasan1.isSelected()) {
                            pernafasan = "true";
                        }else{
                            pernafasan = "false";
                        }

                        if(chkPernafasan2.isSelected()) {
                            pernafasanett = "true";
                        }else{
                            pernafasanett = "false";
                        }

                        if(chkPernafasan3.isSelected()) {
                            pernafasantdk = "true";
                        }else{
                            pernafasantdk = "false";
                        }
                        // NOMOR 2
                        if(chkCardiopurmonal1.isSelected()) {
                            cardiopurmonal = "false";
                        }else{
                          
                        }

                        if(chkCardiopurmonal2.isSelected()) {
                            cardiopurmonal = "true";
                        }else{
                           
                        }

                        // NOMOR 3
                        if(chkalatbantu1.isSelected()) {
                            alatbantupace = "true";
                        }else{
                            alatbantupace = "false";
                        }

                        if(chkalatbantu2.isSelected()) {
                            alatbantukateter = "true";
                        }else{
                            alatbantukateter = "false";
                        }
                        if(chkalatbantu3.isSelected()) {
                            alatbantutdk = "true";
                        }else{
                            alatbantutdk = "false";
                        }
                        // NOMOR 4
                        if(chkalatbantu1.isSelected()) {
                            kemih1 = "true";
                        }else{
                            kemih1 = "false";
                        }

                        if(chkalatbantu2.isSelected()) {
                            kemih2 = "true";
                        }else{
                            kemih2 = "false";
                        }
                        if(chkalatbantu3.isSelected()) {
                            kemih3 = "true";
                        }else{
                            kemih3 = "false";
                        }
                        
                        //alat bantu terakhir
                        if(chkAlatbantuFix1.isSelected()) {
                            ab_alatdengar = "true";
                        }else{
                            ab_alatdengar = "false";
                        }
                        if(chkAlatbantuFix2.isSelected()) {
                            ab_kacamata = "true";
                        }else{
                            ab_kacamata= "false";
                        }
                        
                        String tingkatnyeri1="",tingkatnyeri2="",tingkatnyeri3="";
                         //alat bantu terakhir
                        if(chkTingkatnyeri1.isSelected()) {
                            tingkatnyeri1 = "true";
                        }else {
                            tingkatnyeri1 = "false";
                        }
                        if(chkTingkatnyeri2.isSelected()) {
                            tingkatnyeri2 = "true";
                        }else{
                            tingkatnyeri2 = "false";
                        }
                        if(chkTingkatnyeri3.isSelected()) {
                            tingkatnyeri3 = "true";
                        }else{
                            tingkatnyeri3 = "false";
                        }
                        
                        String mannyeri1="",mannyeri2="",mannyeri3="";
                         //manajemen nyeri
                        if(chkManajemen1.isSelected()) {
                            mannyeri1 = "true";
                        }else{
                            mannyeri1 = "false";
                        }
                        if(chkManajemen2.isSelected()) {
                            mannyeri2 = "true";
                        }else{
                            mannyeri2 = "false";
                        }
                        if(chkManajemen3.isSelected()) {
                            mannyeri3 = "true";
                        }else{
                            mannyeri3 = "false";
                        }
                        
                        String ansietas1="",ansietas2="",ansietas3="",ansietas4="";
                         //ansietas
                        if(chkAnsietas1.isSelected()){
                            ansietas1 = "true";
                        }else{
                            ansietas1 = "false";
                        }
                        if(chkAnsietas2.isSelected()) {
                            ansietas2 = "true";
                        }else{
                            ansietas2 = "false";
                        }
                        if(chkAnsietas3.isSelected()) {
                            ansietas3 = "true";
                        }else{
                            ansietas3 = "false";
                        }
                        if(chkAnsietas4.isSelected()) {
                            ansietas4 = "true";
                        }else{
                            ansietas4 = "false";
                        }
                        
                        String reduksi1="",reduksi2="",reduksi3="",reduksi4="",reduksi5="",reduksi6="";
                         //reduksi
                        if(chkReduksi1.isSelected()) {
                            reduksi1 = "true";
                        }else{
                            reduksi1 = "false";
                        }
                        if(chkReduksi2.isSelected()) {
                            reduksi2 = "true";
                        }else{
                            reduksi2 = "false";
                        }
                        if(chkReduksi3.isSelected()) {
                           reduksi3 = "true";
                        }else{
                            reduksi3 = "false";
                        }
                        if(chkReduksi4.isSelected()) {
                            reduksi4 = "true";
                        }else{
                            reduksi4 = "false";
                        }if(chkReduksi5.isSelected()) {
                            reduksi5 = "true";
                        }else{
                            reduksi5 = "false";
                        }
                        if(chkReduksi6.isSelected()) {
                            reduksi6 = "true";
                        }else{
                            reduksi6 = "false";
                        }
                        
                        
                         String evaluasi1="",evaluasi2="",evaluasi3="",evaluasi4="",evaluasi5="",evaluasi6="";
                         //reduksi
                        if(chkEvaluasi1.isSelected()) {
                            evaluasi1 = "true";
                        }else {
                            evaluasi1 = "false";
                        }
                        if (chkEvaluasi2.isSelected()) {
                            evaluasi2 = "true";
                        } else {
                            evaluasi2 = "false";
                        }
                        if (chkEvaluasi3.isSelected()) {
                            evaluasi3 = "true";
                        } else {
                            evaluasi3 = "false";
                        }
                        if (chkEvaluasi4.isSelected()) {
                            evaluasi4 = "true";
                        } else {
                            evaluasi4 = "false";
                        }
                        if(Sequel.mengedittf("penilaian_awal_ok_preoperasi","no_rawat=?","no_rawat=?, tanggal=?, jam=?, kd_petugas=?, td=?, spo=?, hr=?, rr=?, suhu=?, keluhan_nyeri=?, keluhan_nyeri_ket=?, kesadaran=?, orientasi=?, orientasi_ket=?, status_pernafasan=?, status_pernafasan_ket=?, suara_nafas=?, alat_bantu_nafas=?, alat_bantu_o2=?, alat_bantu_ett=?, alat_bantu_tidakada=?, cardiopurmonal=?, cardiopurmonal_lokasi=?, ab_pacemaker=?, ab_kateterjantung=?, ab_tidakada=?, kulit=?,ket_kulit=?, muskolokeletal=?, perkemihan=?, perkemihan2=?, perkemihan3=?, perkemihan_ket=?, sensori=?, ab_alatdengar=?, ab_kacamata=?, tingkat_nyeri_skala=?,tingkat_nyeri_1=?, tingkat_nyeri_2=?, tingkat_nyeri_3=?, manajemen_nyeri_1=?, manajemen_nyeri_2=?, manajemen_nyeri_3=?, evaluasi_nyeri=?, ansietas_1=?, ansietas_2=?, ansietas_3=?, ansietas_4=?, reduksi_1=?, reduksi_2=?, reduksi_3=?, reduksi_4=?, reduksi_5=?, reduksi_6=?, evaluasi_1=?, evaluasi_2=?, evaluasi_3=?, evaluasi_4=?",59,new String[]{
                               TNoRw1.getText(),
                               Valid.SetTgl(Tanggal.getSelectedItem()+""),
                               Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                               KdPetugas.getText(),
                               td.getText(),
                               spo.getText(),
                               hr.getText(),
                               rr.getText(),
                               suhu.getText(),
                               cmbSkalanyeri.getSelectedItem().toString(),
                               skalanyeri_ket.getText(),
                               cmbKesadaran.getSelectedItem().toString(),
                               cmbOrientasi.getSelectedItem().toString(),
                               orientasi_ket.getText(),
                               cmbStatuspernafasan.getSelectedItem().toString(),
                               statuspernafasan_ket.getText(),
                               cmbSuaranafas.getSelectedItem().toString(),
                               cmbPernafasanalatbantu.getSelectedItem().toString(),
                               pernafasan,
                               pernafasanett,
                               pernafasantdk,
                               cardiopurmonal,
                               Cardiopurmonal_ket.getText(),
                               alatbantupace,
                               alatbantukateter,
                               alatbantutdk,
                               cmbKulit.getSelectedItem().toString(),
                               ket_kulit.getText(),
                               cmbMuskolokeletal.getSelectedItem().toString(),
                               kemih1,
                               kemih2,
                               kemih3,
                               perkemihan_ket.getText(),
                               cmbSensori.getSelectedItem().toString(),
                               ab_alatdengar,
                               ab_kacamata,
                               cmbTingkatNyeriSkala.getSelectedItem().toString(),
                               tingkatnyeri1,
                               tingkatnyeri2,
                               tingkatnyeri3,
                               mannyeri1,
                               mannyeri2,
                               mannyeri3,
                               cmbEvaluasinyeri.getSelectedItem().toString(),
                               ansietas1,
                               ansietas2,
                               ansietas3,
                               ansietas4,
                               reduksi1,
                               reduksi2,
                               reduksi3,
                               reduksi4,
                               reduksi5,
                               reduksi6,
                               evaluasi1,
                               evaluasi2,
                               evaluasi3,
                               evaluasi4,
                               tbPreOp.getValueAt(tbPreOp.getSelectedRow(),0).toString()
                       })==true){
                           JOptionPane.showMessageDialog(rootPane,"Berhasil Mengedit..!!");
                           tampil();
                           emptTeks();
                   }
                
                }else{
                    JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Pre Op terlebih dahulu..!!");
                }
                tampil();
                emptTeks();
            }
        }else if(TabOK.getSelectedIndex()==1){            
            if(TNoRw2.getText().trim().equals("")){
                Valid.textKosong(TNoRw2,"Silahkan pilih data pasien terlebih dahulu !");
            }else if(spo1.getText().trim().equals("")){
                Valid.textKosong(spo1,"SPO");
            }else if(hr1.getText().trim().equals("")){
                Valid.textKosong(hr1,"HR");
            }else if(rr1.getText().trim().equals("")){
                Valid.textKosong(rr1,"RR");
            }else if(suhu1.getText().trim().equals("")){
                Valid.textKosong(suhu1,"Suhu");
            }else{ 
                if(Sequel.mengedittf("penilaian_awal_ok_intraoperatif","no_rawat=?","no_rawat=?, tanggal=?, jam=?, kd_petugas=?, td=?, spo2=?, hr=?, rr=?, suhu=?, kassa=?, irigasi=?, suction=?, jlo_1=?, jlo_2=?, jlo_3=?, jlo_4=?, kulit_1=?, kulit_2=?, kulit_3=?, kulit_4=?,kebersihan_tangan1=?, kebersihan_tangan=?, cegah_1=?, cegah_2=?, cegah_3=?, cegah_4=?, ct5_1=?, ct5_2=?, ct5_3=?, ct5_4=?, ct5_5=?, ct5_6=?, ct5_7=?, ct5_8=?, ct5_9=?, ct5_10=?, evaluasi_1=?, evaluasi_2=?, evaluasi_3=?, termo_1=?, termo_2=?, termo_3=?, termo_4=?, evaluasi2_1=?, evaluasi2_2=?, evaluasi2_3=?, evaluasi2_4=?, hipo_1=?, hipo_2=?, hipo_3=?, cairan_1=?, cairan_2=?, cairan_3=?, cairan_4=?, mnj_cairan_1=?, mnj_cairan_2=?, mnj_cairan_3=?, mnj_cairan_4=?, evaluasi3_1=?, evaluasi3_2=?, evaluasi3_3=?, evaluasi3_4=?",63,new String[]{
                        TNoRw2.getText(),
                        Valid.SetTgl(Tanggal1.getSelectedItem()+""),
                        Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                        KdPetugas1.getText(),
                        td1.getText(),
                        spo1.getText(),
                        hr1.getText(),
                        rr1.getText(),
                        suhu1.getText(),
                        t_kassa.getText(),
                        t_pencucian.getText(),
                        t_suction.getText(),
                        String.valueOf(chk_luka_1.isSelected()),
                        String.valueOf(chk_luka_2.isSelected()),
                        String.valueOf(chk_luka_3.isSelected()),
                        String.valueOf(chk_luka_4.isSelected()),
                        String.valueOf(chk_kulit_1.isSelected()),
                        String.valueOf(chk_kulit_2.isSelected()),
                        String.valueOf(chk_kulit_3.isSelected()),
                        String.valueOf(chk_kulit_4.isSelected()),
                        String.valueOf(chk_kontrol_1.isSelected()),
                        String.valueOf(chk_kontrol_2.isSelected()),
                        String.valueOf(chk_jenisluka_1.isSelected()),
                        String.valueOf(chk_jenisluka_2.isSelected()),
                        String.valueOf(chk_jenisluka_3.isSelected()),
                        String.valueOf(chk_jenisluka_4.isSelected()),
                        String.valueOf(chk_cuci_1.isSelected()),
                        String.valueOf(chk_cuci_2.isSelected()),
                        String.valueOf(chk_cuci_3.isSelected()),
                        String.valueOf(chk_cuci_4.isSelected()),
                        String.valueOf(chk_cuci_5.isSelected()),
                        String.valueOf(chk_cuci_6.isSelected()),
                        String.valueOf(chk_cuci_7.isSelected()),
                        String.valueOf(chk_cuci_8.isSelected()),
                        String.valueOf(chk_cuci_9.isSelected()),
                        String.valueOf(chk_cuci_10.isSelected()),
                        String.valueOf(evaluasi_1.isSelected()),
                        String.valueOf(evaluasi_2.isSelected()),
                        String.valueOf(evaluasi_3.isSelected()),
                        String.valueOf(termo_1.isSelected()),
                        String.valueOf(termo_2.isSelected()),
                        String.valueOf(termo_3.isSelected()),
                        String.valueOf(termo_4.isSelected()),
                        String.valueOf(evaluasi2_1.isSelected()),
                        String.valueOf(evaluasi2_2.isSelected()),
                        String.valueOf(evaluasi2_3.isSelected()),
                        String.valueOf(evaluasi2_4.isSelected()),
                        String.valueOf(hipo_1.isSelected()),
                        String.valueOf(hipo_2.isSelected()),
                        String.valueOf(hipo_3.isSelected()),
                        String.valueOf(cairan_1.isSelected()),
                        String.valueOf(cairan_2.isSelected()),
                        String.valueOf(cairan_3.isSelected()),
                        String.valueOf(cairan_4.isSelected()),
                        String.valueOf(manaj_1.isSelected()),
                        String.valueOf(manaj_2.isSelected()),
                        String.valueOf(manaj_3.isSelected()),
                        String.valueOf(manaj_4.isSelected()),
                        String.valueOf(evaluasi3_1.isSelected()),
                        String.valueOf(evaluasi3_2.isSelected()),
                        String.valueOf(evaluasi3_3.isSelected()),
                        String.valueOf(evaluasi3_4.isSelected()),
                        tbInOp.getValueAt(tbInOp.getSelectedRow(),0).toString()
                })==true){
                    tampil();
                    tampilinop();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Megubah Data");
            }  
         }
          
        }else if(TabOK.getSelectedIndex()==2){
            if(TNoRw1.getText().trim().equals("")){
                Valid.textKosong(TNoRw1,"Nomor Rawat");
            }else if(TNoRM1.getText().trim().equals("")){
                Valid.textKosong(TNoRM1,"Nomor RM");
            }else if(TPasien1.getText().trim().equals("")){
                Valid.textKosong(TPasien1,"Pasien");
            }else{
              
                 if(Sequel.mengedittf("penilaian_awal_ok_postoperatif","no_rawat=?","no_rawat=?, tanggal=?, jam=?, kd_petugas=?, td=?, spo2=?, hr=?, rr=?, suhu=?, nyeri_1=?, nyeri_2=?, k1=?, k2=?, k3=?, kk1=?, kk2=?, kk3=?, kk4=?, kk5=?, s1=?, s2=?, s3=?, s4=?, sn1=?, sn2=?, sn3=?, sn4=?, jn1=?, jn2=?, jn3=?, jn4=?, jn5=?, jn6=?, mjn1=?, mjn2=?, mjn3=?, mjn4=?, mjn5=?, mjn6=?, ev_1_1=?, ev_1_2=?, kel_1=?, kel_2=?, mual_1=?, mual_2=?, ev_2_1=?, ev_2_2=?, nyeri_3=?, tn_1=?, tn_2=?, tn_3=?, mn_1=?, mn_2=?, mn_3=?, nyeri_4=?, jatuh=?, pj_1=?, pj_2=?, pj_3=?, po_1=?, po_2=?, po_3=?, ev_3_1=?, ev_3_2=?, pindah_1=?, pindah_2=?, pindah_3=?",68,new String[]{
                        TNoRw2.getText(),
                        Valid.SetTgl(Tanggal2.getSelectedItem()+""),
                        Jam2.getSelectedItem()+":"+Menit2.getSelectedItem()+":"+Detik2.getSelectedItem(),
                        KdPetugas2.getText(),
                        td2.getText(),
                        spo2.getText(),
                        hr2.getText(),
                        rr2.getText(),
                        suhu2.getText(),
                        nyeri_1.getSelectedItem().toString(),
                        String.valueOf(chknyeri3.isSelected()),
                        String.valueOf(k1.isSelected()),
                        String.valueOf(k2.isSelected()),
                        String.valueOf(k3.isSelected()),
                        String.valueOf(kk1.isSelected()),
                        String.valueOf(kk2.isSelected()),
                        String.valueOf(kk3.isSelected()),
                        String.valueOf(kk4.isSelected()),
                        String.valueOf(kk5.isSelected()),
                        String.valueOf(s1.isSelected()),
                        String.valueOf(s2.isSelected()),
                        String.valueOf(s3.isSelected()),
                        String.valueOf(s4.isSelected()),
                        String.valueOf(sn1.isSelected()),
                        String.valueOf(sn2.isSelected()),
                        String.valueOf(sn3.isSelected()),
                        String.valueOf(sn4.isSelected()),
                        String.valueOf(jn1.isSelected()),
                        String.valueOf(jn2.isSelected()),
                        String.valueOf(jn3.isSelected()),
                        String.valueOf(jn4.isSelected()),
                        String.valueOf(jn5.isSelected()),
                        String.valueOf(jn6.isSelected()),
                        String.valueOf(mjn1.isSelected()),
                        String.valueOf(mjn2.isSelected()),
                        String.valueOf(mjn3.isSelected()),
                        String.valueOf(mjn4.isSelected()),
                        String.valueOf(mjn5.isSelected()),
                        String.valueOf(mjn6.isSelected()),
                        String.valueOf(ev_1_1.isSelected()),
                        String.valueOf(ev_1_2.isSelected()),
                        String.valueOf(kel_1.isSelected()),
                        String.valueOf(kel_2.isSelected()),
                        String.valueOf(mual_1.isSelected()),
                        String.valueOf(mual_2.isSelected()),
                        String.valueOf(ev_2_1.isSelected()),
                        String.valueOf(ev_2_2.isSelected()),
                        nyeri_2.getSelectedItem().toString(),
                        String.valueOf(tn_1.isSelected()),
                        String.valueOf(tn_2.isSelected()),
                        String.valueOf(tn_3.isSelected()),
                        String.valueOf(mn_1.isSelected()),
                        String.valueOf(mn_2.isSelected()),
                        String.valueOf(mn_3.isSelected()),
                        nyeri_3.getSelectedItem().toString(),
                        String.valueOf(jatuh.isSelected()),
                        String.valueOf(pj_1.isSelected()),
                        String.valueOf(pj_2.isSelected()),
                        String.valueOf(pj_3.isSelected()),
                        String.valueOf(ev_3_1.isSelected()),
                        String.valueOf(ev_3_2.isSelected()),
                        String.valueOf(po_1.isSelected()),
                        String.valueOf(po_2.isSelected()),
                        String.valueOf(po_3.isSelected()),
                        String.valueOf(pindah_1.isSelected()),
                        String.valueOf(pindah_2.isSelected()),
                        String.valueOf(pindah_3.isSelected()),
                        tbPostOp.getValueAt(tbPostOp.getSelectedRow(),0).toString()
                })==true){
                    tampil();
                    tampilinop();
                    tampilpostop();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Megubah Data");
            }  
         
            }
        }else if(TabOK.getSelectedIndex()==3){
           
        
        }              
    }//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnKeluar);
        }
    }//GEN-LAST:event_BtnEditKeyPressed

    private void pp2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pp2ActionPerformed
              if(tabModePreOp.getRowCount()!=0){
                   try {
                      int row=tbPreOp.getSelectedRow();
                        if(row!= -1){
                                  if(tbPreOp.getSelectedRow()>-1){
                                        Map<String, Object> param = new HashMap<>();
                                        param.put("namars",akses.getnamars());
                                        param.put("alamatrs",akses.getalamatrs());
                                        param.put("kotars",akses.getkabupatenrs());
                                        param.put("propinsirs",akses.getpropinsirs());
                                        param.put("kontakrs",akses.getkontakrs());
                                        param.put("emailrs",akses.getemailrs());
                                        param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                                        param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                                        param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                                        param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                                        param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                                        param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                                        param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                                        finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbPreOp.getValueAt(tbPreOp.getSelectedRow(),3).toString());
                                        param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),4).toString()+"\nID "+(finger.equals("")?tbPreOp.getValueAt(tbPreOp.getSelectedRow(),4).toString():finger)+"\n"+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),1).toString()+" "+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),2).toString());  
                                        Valid.MyReportqry("rptFormulirTimeOutSebelumInsisi.jasper","report","::[ Formulir Time-Out Sebelum Tindakan Insisi ]::",
                                                "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                                                "timeout_sebelum_insisi.*,d1.kd_dokter as kd_operator1,d1.nm_dokter as nm_operator1,d2.kd_dokter as kd_operator2,d2.nm_dokter as nm_operator2, "+
                                                "d3.kd_dokter as kd_anestesi,d3.nm_dokter as nm_anestesi,d4.kd_dokter as kd_anak,d4.nm_dokter as nm_anak,d5.nip as kd_perinatologi,d5.nama as nm_perinatologi,d6.nip as kd_instrumen,d6.nama as nm_instrumen,a0.nip as kd_petugas,a0.nama as nm_petugas, "+
                                                "a1.nip as kd_asisten1,a1.nama as nm_asisten1,a2.nip as kd_asisten2,a2.nama as nm_asisten2,pa.nip as kd_prw_anestesi,pa.nama as nm_prw_anestesi "+
                                                "from timeout_sebelum_insisi inner join reg_periksa on timeout_sebelum_insisi.no_rawat=reg_periksa.no_rawat "+
                                                "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                                                "inner join dokter as d1 on d1.kd_dokter=timeout_sebelum_insisi.kd_operator1 "+
                                                "inner join dokter as d2 on d2.kd_dokter=timeout_sebelum_insisi.kd_operator2 "+
                                                "inner join dokter as d3 on d3.kd_dokter=timeout_sebelum_insisi.kd_anestesi "+
                                                "inner join dokter as d4 on d4.kd_dokter=timeout_sebelum_insisi.kd_anak "+
                                                "inner join petugas as d5 on d5.nip=timeout_sebelum_insisi.kd_perinatologi "+
                                                "inner join petugas as d6 on d6.nip=timeout_sebelum_insisi.kd_instrumen "+
                                                "inner join petugas as a0 on a0.nip=timeout_sebelum_insisi.kd_petugas "+
                                                "inner join petugas as a1 on a1.nip=timeout_sebelum_insisi.kd_asisten1 "+
                                                "inner join petugas as a2 on a2.nip=timeout_sebelum_insisi.kd_asisten2 "+
                                                "inner join petugas as pa on pa.nip=timeout_sebelum_insisi.kd_prw_anestesi "+
                                                "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                                                "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                                                "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                                                "where timeout_sebelum_insisi.no_rawat='"+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),0).toString()+"' and timeout_sebelum_insisi.tanggal='"+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),5).toString()+"' and timeout_sebelum_insisi.jam='"+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),6).toString()+"' ",param);
                                    }
                        }
                    } catch (java.lang.NullPointerException e) {
                   }
      }
    }//GEN-LAST:event_pp2ActionPerformed

    private void TabOKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabOKMouseClicked
        if(TabOK.getSelectedIndex()==0){
            tampil();
        }else if(TabOK.getSelectedIndex()==1){
            tampil();
        }else if(TabOK.getSelectedIndex()==2){
            tampil();
        }else if(TabOK.getSelectedIndex()==3){
            tampil();
        }
    }//GEN-LAST:event_TabOKMouseClicked

    private void pp1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pp1ActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        for (int i = 0; i < tbPreOp.getRowCount(); i++) {             
                                     
        }            
        this.setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_pp1ActionPerformed

    private void ppCetakPreOpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppCetakPreOpActionPerformed
        if(TabOK.getSelectedIndex()==0){
                if(tbPreOp.getSelectedRow()>-1){
                    Map<String, Object> param = new HashMap<>();
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());
                    param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                    param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                    param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                    param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                    param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                    param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                    param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                    finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbPreOp.getValueAt(tbPreOp.getSelectedRow(),3).toString());
                    param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),4).toString()+"\nID "+(finger.equals("")?tbPreOp.getValueAt(tbPreOp.getSelectedRow(),3).toString():finger)+"\n"+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),1).toString()+" "+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),2).toString()); 
                    Valid.MyReportqry("rptCetakPenialaianPerioperatifPreop.jasper","report","::[ Formulir Asuhan Perioperatif Pre Operasi ]::",
                            "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                            "penilaian_awal_ok_preoperasi.*,pa.nip,pa.nama " +
                            "from penilaian_awal_ok_preoperasi inner join reg_periksa on penilaian_awal_ok_preoperasi.no_rawat=reg_periksa.no_rawat "+
                            "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                            "inner join petugas as pa on pa.nip=penilaian_awal_ok_preoperasi.kd_petugas "+
                            "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                            "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                            "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                            "where penilaian_awal_ok_preoperasi.no_rawat='"+tbPreOp.getValueAt(tbPreOp.getSelectedRow(),0).toString()+"'",param);
                }
        }else if(TabOK.getSelectedIndex()==1) {
                 if(tbInOp.getSelectedRow()>-1){
                    Map<String, Object> param = new HashMap<>();
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());
                    param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                    param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                    param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                    param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                    param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                    param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                    param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                    finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbInOp.getValueAt(tbInOp.getSelectedRow(),3).toString());
                    param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbInOp.getValueAt(tbInOp.getSelectedRow(),4).toString()+"\nID "+(finger.equals("")?tbInOp.getValueAt(tbInOp.getSelectedRow(),3).toString():finger)+"\n"+tbInOp.getValueAt(tbInOp.getSelectedRow(),1).toString()+" "+tbInOp.getValueAt(tbInOp.getSelectedRow(),2).toString()); 
                    Valid.MyReportqry("rptCetakPenilaianPerioperatifInop.jasper","report","::[ Formulir Asuhan Perioperatif Intra Operatif ]::",
                            "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                            "penilaian_awal_ok_intraoperatif.*,pa.nip,pa.nama " +
                            "from penilaian_awal_ok_intraoperatif inner join reg_periksa on penilaian_awal_ok_intraoperatif.no_rawat=reg_periksa.no_rawat "+
                            "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                            "inner join petugas as pa on pa.nip=penilaian_awal_ok_intraoperatif.kd_petugas "+
                            "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                            "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                            "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                            "where penilaian_awal_ok_intraoperatif.no_rawat='"+tbInOp.getValueAt(tbInOp.getSelectedRow(),0).toString()+"'",param);
                }
        }else if(TabOK.getSelectedIndex()==2){
            if(tbPostOp.getSelectedRow()>-1){
                    Map<String, Object> param = new HashMap<>();
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());
                    param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                    param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                    param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                    param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                    param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                    param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                    param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                    finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbPostOp.getValueAt(tbPostOp.getSelectedRow(),3).toString());
                    param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbPostOp.getValueAt(tbPostOp.getSelectedRow(),4).toString()+"\nID "+(finger.equals("")?tbPostOp.getValueAt(tbPostOp.getSelectedRow(),3).toString():finger)+"\n"+tbPostOp.getValueAt(tbPostOp.getSelectedRow(),1).toString()+" "+tbPostOp.getValueAt(tbPostOp.getSelectedRow(),2).toString()); 
                    Valid.MyReportqry("rptCetakPenilaianPerioperatifPostop.jasper","report","::[ Formulir Asuhan Perioperatif Post Operatif ]::",
                            "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                            "penilaian_awal_ok_postoperatif.*,pa.nip,pa.nama " +
                            "from penilaian_awal_ok_postoperatif inner join reg_periksa on penilaian_awal_ok_postoperatif.no_rawat=reg_periksa.no_rawat "+
                            "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                            "inner join petugas as pa on pa.nip=penilaian_awal_ok_postoperatif.kd_petugas "+
                            "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                            "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                            "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                            "where penilaian_awal_ok_postoperatif.no_rawat='"+tbPostOp.getValueAt(tbPostOp.getSelectedRow(),0).toString()+"'",param);
                }
        }
    }//GEN-LAST:event_ppCetakPreOpActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnCari, TCari);
        }
    }//GEN-LAST:event_BtnAllKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
    }//GEN-LAST:event_BtnAllActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
    }//GEN-LAST:event_BtnCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
    }//GEN-LAST:event_BtnCariActionPerformed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCariKeyPressed

    private void Cardiopurmonal_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Cardiopurmonal_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Cardiopurmonal_ketKeyPressed

    private void Cardiopurmonal_ketMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Cardiopurmonal_ketMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_Cardiopurmonal_ketMouseExited

    private void Cardiopurmonal_ketMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Cardiopurmonal_ketMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_Cardiopurmonal_ketMouseMoved

    private void perkemihan_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_perkemihan_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_perkemihan_ketKeyPressed

    private void perkemihan_ketMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_perkemihan_ketMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_perkemihan_ketMouseExited

    private void perkemihan_ketMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_perkemihan_ketMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_perkemihan_ketMouseMoved

    private void chkPernafasan2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkPernafasan2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkPernafasan2ActionPerformed

    private void cmbPernafasanalatbantuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPernafasanalatbantuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbPernafasanalatbantuActionPerformed

    private void statuspernafasan_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_statuspernafasan_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_statuspernafasan_ketKeyPressed

    private void statuspernafasan_ketMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_statuspernafasan_ketMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_statuspernafasan_ketMouseExited

    private void statuspernafasan_ketMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_statuspernafasan_ketMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_statuspernafasan_ketMouseMoved

    private void skalanyeri_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_skalanyeri_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_skalanyeri_ketKeyPressed

    private void skalanyeri_ketMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_skalanyeri_ketMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_skalanyeri_ketMouseExited

    private void skalanyeri_ketMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_skalanyeri_ketMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_skalanyeri_ketMouseMoved

    private void suhuKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_suhuKeyPressed
        Valid.pindah(evt,rr,orientasi_ket);
    }//GEN-LAST:event_suhuKeyPressed

    private void suhuMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_suhuMouseExited
      
    }//GEN-LAST:event_suhuMouseExited

    private void suhuMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_suhuMouseMoved
      
    }//GEN-LAST:event_suhuMouseMoved

    private void rrKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rrKeyPressed
        Valid.pindah(evt,hr,suhu);
    }//GEN-LAST:event_rrKeyPressed

    private void rrMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rrMouseExited
       
    }//GEN-LAST:event_rrMouseExited

    private void rrMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rrMouseMoved
        if(rr.getText().equals("0")||rr.getText().equals("0.0")){
            rr.setText("");
        }
    }//GEN-LAST:event_rrMouseMoved

    private void hrKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_hrKeyPressed
        Valid.pindah(evt,spo,rr);
    }//GEN-LAST:event_hrKeyPressed

    private void hrMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hrMouseExited
       
    }//GEN-LAST:event_hrMouseExited

    private void hrMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hrMouseMoved
      
    }//GEN-LAST:event_hrMouseMoved

    private void spoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_spoKeyPressed
        Valid.pindah(evt,td,hr);
    }//GEN-LAST:event_spoKeyPressed

    private void spoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_spoMouseExited
       
    }//GEN-LAST:event_spoMouseExited

    private void spoMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_spoMouseMoved
        
    }//GEN-LAST:event_spoMouseMoved

    private void tdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tdKeyPressed
        Valid.pindah(evt,BtnSimpan,spo);
    }//GEN-LAST:event_tdKeyPressed

    private void tdMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tdMouseExited
       
    }//GEN-LAST:event_tdMouseExited

    private void tdMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tdMouseMoved
       
    }//GEN-LAST:event_tdMouseMoved

    private void orientasi_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_orientasi_ketKeyPressed
        Valid.pindah(evt,suhu,td);
    }//GEN-LAST:event_orientasi_ketKeyPressed

    private void orientasi_ketMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_orientasi_ketMouseExited

    }//GEN-LAST:event_orientasi_ketMouseExited

    private void orientasi_ketMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_orientasi_ketMouseMoved

    }//GEN-LAST:event_orientasi_ketMouseMoved

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
     
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed

        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed

    }//GEN-LAST:event_KdPetugasKeyPressed

    private void TNoRw1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRw1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
          
        }else{
            Valid.pindah(evt,TCari,BtnPetugas);
        }
    }//GEN-LAST:event_TNoRw1KeyPressed

    private void JamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JamActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamActionPerformed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MenitKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DetikKeyPressed

    private void spoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_spoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_spoActionPerformed

    private void ChkAccorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccorActionPerformed
        if(!(TNoRw1.getText().equals(""))){
            if(TNoRw1.getText().equals("")){
                Valid.textKosong(TCari,"Pasien");
            }else{
                isPhoto();
                TabDataMouseClicked(null);
            }
        }else{
            ChkAccor.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih Pasien..!!!");
        }
    }//GEN-LAST:event_ChkAccorActionPerformed

    private void BtnRefreshPhoto1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto1ActionPerformed
        tampil();
    }//GEN-LAST:event_BtnRefreshPhoto1ActionPerformed

    private void TabDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabDataMouseClicked
        if(TabData.getSelectedIndex()==0){
           
        }else if(TabData.getSelectedIndex()==1){
           
        }
    }//GEN-LAST:event_TabDataMouseClicked

    private void tbPreOpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPreOpMouseClicked
      if(tabModePreOp.getRowCount()!=0){
                   try {
                       getData();
                   } catch (java.lang.NullPointerException e) {
                   }
      }  
    }//GEN-LAST:event_tbPreOpMouseClicked

    private void tbPreOpKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPreOpKeyPressed
               if(tabModePreOp.getRowCount()!=0){
                   try {
                       getData();
                   } catch (java.lang.NullPointerException e) {
                   }
               }     
    }//GEN-LAST:event_tbPreOpKeyPressed

    private void BtnAll1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAll1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnCari, TCari);
        }
    }//GEN-LAST:event_BtnAll1KeyPressed

    private void BtnAll1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAll1ActionPerformed
        TCari1.setText("");
        tampil();
    }//GEN-LAST:event_BtnAll1ActionPerformed

    private void BtnCari1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCari1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
    }//GEN-LAST:event_BtnCari1KeyPressed

    private void BtnCari1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCari1ActionPerformed
        tampil();
    }//GEN-LAST:event_BtnCari1ActionPerformed

    private void TCari1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCari1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCari1ActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari1.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCari1KeyPressed

    private void jkMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jkMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jkMouseMoved

    private void jkMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jkMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jkMouseExited

    private void jkKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jkKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jkKeyPressed

    private void jkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jkActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jkActionPerformed

    private void TNoRw2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRw2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TNoRw2KeyPressed

    private void KdPetugas1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugas1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdPetugas1KeyPressed

    private void BtnPetugas1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugas1ActionPerformed
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnPetugas1ActionPerformed

    private void BtnPetugas1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugas1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugas1KeyPressed

    private void td1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_td1MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_td1MouseMoved

    private void td1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_td1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_td1MouseExited

    private void td1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_td1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_td1KeyPressed

    private void spo1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_spo1MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_spo1MouseMoved

    private void spo1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_spo1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_spo1MouseExited

    private void spo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_spo1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_spo1ActionPerformed

    private void spo1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_spo1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_spo1KeyPressed

    private void hr1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hr1MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_hr1MouseMoved

    private void hr1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hr1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_hr1MouseExited

    private void hr1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_hr1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_hr1KeyPressed

    private void rr1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rr1MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_rr1MouseMoved

    private void rr1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rr1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_rr1MouseExited

    private void rr1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rr1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_rr1KeyPressed

    private void t_kassaMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t_kassaMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_t_kassaMouseMoved

    private void t_kassaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t_kassaMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_t_kassaMouseExited

    private void t_kassaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_t_kassaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_t_kassaKeyPressed

    private void Jam1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1ActionPerformed

    private void Jam1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1KeyPressed

    private void Menit1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit1KeyPressed

    private void Detik1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik1KeyPressed

    private void jk1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk1MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1MouseMoved

    private void jk1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1MouseExited

    private void jk1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jk1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1ActionPerformed

    private void jk1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jk1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1KeyPressed

    private void ChkAccor1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccor1ActionPerformed
     if(!(TNoRw1.getText().equals(""))){
            if(TNoRw1.getText().equals("")){
                Valid.textKosong(TCari,"Pasien");
            }else{
                isPhoto1();
                TabDataMouseClicked(null);
            }
        }else{
            ChkAccor.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih Pasien..!!!");
        }
    }//GEN-LAST:event_ChkAccor1ActionPerformed

    private void BtnRefreshPhoto2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto2ActionPerformed
      tampilinop();
    }//GEN-LAST:event_BtnRefreshPhoto2ActionPerformed

    private void tbInOpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbInOpMouseClicked
        getData2();
    }//GEN-LAST:event_tbInOpMouseClicked

    private void tbInOpKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbInOpKeyPressed
       getData2();
    }//GEN-LAST:event_tbInOpKeyPressed

    private void TabData1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabData1MouseClicked
        if(TabData1.getSelectedIndex()==0){
           
        }else if(TabData1.getSelectedIndex()==1){
           
        }
    }//GEN-LAST:event_TabData1MouseClicked

    private void suhu1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_suhu1MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_suhu1MouseMoved

    private void suhu1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_suhu1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_suhu1MouseExited

    private void suhu1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_suhu1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_suhu1KeyPressed

    private void t_suctionMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t_suctionMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_t_suctionMouseMoved

    private void t_suctionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t_suctionMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_t_suctionMouseExited

    private void t_suctionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_t_suctionKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_t_suctionKeyPressed

    private void t_pencucianMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t_pencucianMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_t_pencucianMouseMoved

    private void t_pencucianMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t_pencucianMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_t_pencucianMouseExited

    private void t_pencucianKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_t_pencucianKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_t_pencucianKeyPressed

    private void chk_kulit_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chk_kulit_1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chk_kulit_1ActionPerformed

    private void TCari2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCari2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TCari2KeyPressed

    private void BtnCari2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCari2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnCari2ActionPerformed

    private void BtnCari2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCari2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnCari2KeyPressed

    private void BtnAll2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAll2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAll2ActionPerformed

    private void BtnAll2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAll2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAll2KeyPressed

    private void TNoRw3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRw3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TNoRw3KeyPressed

    private void KdPetugas2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugas2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdPetugas2KeyPressed

    private void BtnPetugas2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugas2ActionPerformed
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnPetugas2ActionPerformed

    private void BtnPetugas2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugas2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugas2KeyPressed

    private void td2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_td2MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_td2MouseMoved

    private void td2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_td2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_td2MouseExited

    private void td2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_td2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_td2KeyPressed

    private void spo2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_spo2MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_spo2MouseMoved

    private void spo2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_spo2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_spo2MouseExited

    private void spo2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_spo2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_spo2ActionPerformed

    private void spo2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_spo2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_spo2KeyPressed

    private void hr2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hr2MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_hr2MouseMoved

    private void hr2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hr2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_hr2MouseExited

    private void hr2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_hr2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_hr2KeyPressed

    private void rr2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rr2MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_rr2MouseMoved

    private void rr2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rr2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_rr2MouseExited

    private void rr2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rr2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_rr2KeyPressed

    private void Jam2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam2ActionPerformed

    private void Jam2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam2KeyPressed

    private void Menit2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit2KeyPressed

    private void Detik2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik2KeyPressed

    private void jk2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk2MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2MouseMoved

    private void jk2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2MouseExited

    private void jk2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jk2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2ActionPerformed

    private void jk2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jk2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2KeyPressed

    private void sn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sn1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sn1ActionPerformed

    private void suhu2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_suhu2MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_suhu2MouseMoved

    private void suhu2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_suhu2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_suhu2MouseExited

    private void suhu2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_suhu2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_suhu2KeyPressed

    private void ChkAccor2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccor2ActionPerformed
        if(!(TNoRw3.getText().equals(""))){
            if(TNoRw3.getText().equals("")){
                Valid.textKosong(TCari2,"Pasien");
            }else{
                isPhoto2();
                TabDataMouseClicked(null);
            }
        }else{
            ChkAccor2.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih Pasien..!!!");
        }
    }//GEN-LAST:event_ChkAccor2ActionPerformed

    private void BtnRefreshPhoto3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto3ActionPerformed
            tampilpostop();
    }//GEN-LAST:event_BtnRefreshPhoto3ActionPerformed

    private void tbPostOpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPostOpMouseClicked
     getData3();
    }//GEN-LAST:event_tbPostOpMouseClicked

    private void tbPostOpKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPostOpKeyPressed
     getData3();
    }//GEN-LAST:event_tbPostOpKeyPressed

    private void TabData2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabData2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_TabData2MouseClicked

    private void nyeri_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nyeri_1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nyeri_1ActionPerformed

    private void nyeri_1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nyeri_1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_nyeri_1KeyPressed

    private void statuspernafasan_ketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statuspernafasan_ketActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_statuspernafasan_ketActionPerformed

    private void nyeri_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nyeri_2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nyeri_2ActionPerformed

    private void nyeri_2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nyeri_2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_nyeri_2KeyPressed

    private void nyeri_3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nyeri_3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nyeri_3ActionPerformed

    private void nyeri_3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nyeri_3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_nyeri_3KeyPressed

    private void ket_kulitMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ket_kulitMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_ket_kulitMouseMoved

    private void ket_kulitMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ket_kulitMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_ket_kulitMouseExited

    private void ket_kulitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ket_kulitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ket_kulitKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMOKAsuhanPerioperatif dialog = new RMOKAsuhanPerioperatif(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnAll1;
    private widget.Button BtnAll2;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnCari1;
    private widget.Button BtnCari2;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPetugas;
    private widget.Button BtnPetugas1;
    private widget.Button BtnPetugas2;
    private widget.Button BtnRefreshPhoto1;
    private widget.Button BtnRefreshPhoto2;
    private widget.Button BtnRefreshPhoto3;
    private widget.Button BtnSimpan;
    private widget.TextBox Cardiopurmonal_ket;
    private widget.CekBox ChkAccor;
    private widget.CekBox ChkAccor1;
    private widget.CekBox ChkAccor2;
    private widget.CekBox ChkJam1;
    private widget.CekBox ChkJam2;
    private widget.CekBox ChkJam3;
    private widget.ComboBox Detik;
    private widget.ComboBox Detik1;
    private widget.ComboBox Detik2;
    private widget.PanelBiasa FormInput1;
    private widget.PanelBiasa FormInput2;
    private widget.PanelBiasa FormInput3;
    private widget.PanelBiasa FormPass3;
    private widget.PanelBiasa FormPass4;
    private widget.PanelBiasa FormPass5;
    private widget.PanelBiasa FormTelaah;
    private widget.PanelBiasa FormTelaah1;
    private widget.PanelBiasa FormTelaah2;
    private widget.ComboBox Jam;
    private widget.ComboBox Jam1;
    private widget.ComboBox Jam2;
    private widget.TextBox KdPetugas;
    private widget.TextBox KdPetugas1;
    private widget.TextBox KdPetugas2;
    private widget.Label LCount;
    private widget.Label LCount1;
    private widget.Label LCount2;
    private widget.ComboBox Menit;
    private widget.ComboBox Menit1;
    private widget.ComboBox Menit2;
    private widget.TextBox NmPetugas;
    private widget.TextBox NmPetugas1;
    private widget.TextBox NmPetugas2;
    private widget.PanelBiasa PanelAccor;
    private widget.PanelBiasa PanelAccor1;
    private widget.PanelBiasa PanelAccor2;
    private javax.swing.JPopupMenu Popup;
    private javax.swing.JPopupMenu Popup1;
    private javax.swing.JPopupMenu Popup2;
    private widget.ScrollPane Scroll5;
    private widget.ScrollPane Scroll6;
    private widget.ScrollPane Scroll8;
    private widget.ScrollPane Scroll9;
    private widget.TextBox TCari;
    private widget.TextBox TCari1;
    private widget.TextBox TCari2;
    private widget.TextBox TNoRM1;
    private widget.TextBox TNoRM2;
    private widget.TextBox TNoRM3;
    private widget.TextBox TNoRw1;
    private widget.TextBox TNoRw2;
    private widget.TextBox TNoRw3;
    private widget.TextBox TPasien1;
    private widget.TextBox TPasien2;
    private widget.TextBox TPasien3;
    private javax.swing.JTabbedPane TabData;
    private javax.swing.JTabbedPane TabData1;
    private javax.swing.JTabbedPane TabData2;
    private javax.swing.JTabbedPane TabOK;
    private widget.Tanggal Tanggal;
    private widget.Tanggal Tanggal1;
    private widget.Tanggal Tanggal2;
    private widget.CekBox cairan_1;
    private widget.CekBox cairan_2;
    private widget.CekBox cairan_3;
    private widget.CekBox cairan_4;
    private widget.CekBox chkAlatbantuFix1;
    private widget.CekBox chkAlatbantuFix2;
    private widget.CekBox chkAnsietas1;
    private widget.CekBox chkAnsietas2;
    private widget.CekBox chkAnsietas3;
    private widget.CekBox chkAnsietas4;
    private widget.CekBox chkCardiopurmonal1;
    private widget.CekBox chkCardiopurmonal2;
    private widget.CekBox chkEvaluasi1;
    private widget.CekBox chkEvaluasi2;
    private widget.CekBox chkEvaluasi3;
    private widget.CekBox chkEvaluasi4;
    private widget.CekBox chkKemih1;
    private widget.CekBox chkKemih2;
    private widget.CekBox chkKemih3;
    private widget.CekBox chkManajemen1;
    private widget.CekBox chkManajemen2;
    private widget.CekBox chkManajemen3;
    private widget.CekBox chkPernafasan1;
    private widget.CekBox chkPernafasan2;
    private widget.CekBox chkPernafasan3;
    private widget.CekBox chkReduksi1;
    private widget.CekBox chkReduksi2;
    private widget.CekBox chkReduksi3;
    private widget.CekBox chkReduksi4;
    private widget.CekBox chkReduksi5;
    private widget.CekBox chkReduksi6;
    private widget.CekBox chkTingkatnyeri1;
    private widget.CekBox chkTingkatnyeri2;
    private widget.CekBox chkTingkatnyeri3;
    private widget.CekBox chk_cuci_1;
    private widget.CekBox chk_cuci_10;
    private widget.CekBox chk_cuci_2;
    private widget.CekBox chk_cuci_3;
    private widget.CekBox chk_cuci_4;
    private widget.CekBox chk_cuci_5;
    private widget.CekBox chk_cuci_6;
    private widget.CekBox chk_cuci_7;
    private widget.CekBox chk_cuci_8;
    private widget.CekBox chk_cuci_9;
    private widget.CekBox chk_jenisluka_1;
    private widget.CekBox chk_jenisluka_2;
    private widget.CekBox chk_jenisluka_3;
    private widget.CekBox chk_jenisluka_4;
    private widget.CekBox chk_kontrol_1;
    private widget.CekBox chk_kontrol_2;
    private widget.CekBox chk_kulit_1;
    private widget.CekBox chk_kulit_2;
    private widget.CekBox chk_kulit_3;
    private widget.CekBox chk_kulit_4;
    private widget.CekBox chk_luka_1;
    private widget.CekBox chk_luka_2;
    private widget.CekBox chk_luka_3;
    private widget.CekBox chk_luka_4;
    private widget.CekBox chkalatbantu1;
    private widget.CekBox chkalatbantu2;
    private widget.CekBox chkalatbantu3;
    private widget.CekBox chknyeri3;
    private widget.ComboBox cmbEvaluasinyeri;
    private widget.ComboBox cmbKesadaran;
    private widget.ComboBox cmbKulit;
    private widget.ComboBox cmbMuskolokeletal;
    private widget.ComboBox cmbOrientasi;
    private widget.ComboBox cmbPernafasanalatbantu;
    private widget.ComboBox cmbSensori;
    private widget.ComboBox cmbSkalanyeri;
    private widget.ComboBox cmbStatuspernafasan;
    private widget.ComboBox cmbSuaranafas;
    private widget.ComboBox cmbTingkatNyeriSkala;
    private widget.CekBox ev_1_1;
    private widget.CekBox ev_1_2;
    private widget.CekBox ev_2_1;
    private widget.CekBox ev_2_2;
    private widget.CekBox ev_3_1;
    private widget.CekBox ev_3_2;
    private widget.CekBox evaluasi2_1;
    private widget.CekBox evaluasi2_2;
    private widget.CekBox evaluasi2_3;
    private widget.CekBox evaluasi2_4;
    private widget.CekBox evaluasi3_1;
    private widget.CekBox evaluasi3_2;
    private widget.CekBox evaluasi3_3;
    private widget.CekBox evaluasi3_4;
    private widget.CekBox evaluasi_1;
    private widget.CekBox evaluasi_2;
    private widget.CekBox evaluasi_3;
    private widget.CekBox hipo_1;
    private widget.CekBox hipo_2;
    private widget.CekBox hipo_3;
    private widget.TextBox hr;
    private widget.TextBox hr1;
    private widget.TextBox hr2;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame3;
    private widget.InternalFrame internalFrame4;
    private widget.InternalFrame internalFrame5;
    private widget.Label jLabel11;
    private widget.Label jLabel12;
    private widget.Label jLabel13;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private widget.CekBox jatuh;
    private widget.TextBox jk;
    private widget.TextBox jk1;
    private widget.TextBox jk2;
    private widget.CekBox jn1;
    private widget.CekBox jn2;
    private widget.CekBox jn3;
    private widget.CekBox jn4;
    private widget.CekBox jn5;
    private widget.CekBox jn6;
    private widget.CekBox k1;
    private widget.CekBox k2;
    private widget.CekBox k3;
    private widget.CekBox kel_1;
    private widget.CekBox kel_2;
    private widget.TextBox ket_kulit;
    private widget.CekBox kk1;
    private widget.CekBox kk2;
    private widget.CekBox kk3;
    private widget.CekBox kk4;
    private widget.CekBox kk5;
    private widget.Label label10;
    private widget.Label label100;
    private widget.Label label101;
    private widget.Label label102;
    private widget.Label label103;
    private widget.Label label104;
    private widget.Label label106;
    private widget.Label label107;
    private widget.Label label108;
    private widget.Label label109;
    private widget.Label label11;
    private widget.Label label110;
    private widget.Label label111;
    private widget.Label label112;
    private widget.Label label113;
    private widget.Label label114;
    private widget.Label label115;
    private widget.Label label116;
    private widget.Label label117;
    private widget.Label label118;
    private widget.Label label119;
    private widget.Label label12;
    private widget.Label label120;
    private widget.Label label121;
    private widget.Label label122;
    private widget.Label label123;
    private widget.Label label124;
    private widget.Label label125;
    private widget.Label label126;
    private widget.Label label127;
    private widget.Label label128;
    private widget.Label label129;
    private widget.Label label13;
    private widget.Label label130;
    private widget.Label label131;
    private widget.Label label132;
    private widget.Label label133;
    private widget.Label label134;
    private widget.Label label135;
    private widget.Label label136;
    private widget.Label label137;
    private widget.Label label138;
    private widget.Label label139;
    private widget.Label label14;
    private widget.Label label140;
    private widget.Label label141;
    private widget.Label label142;
    private widget.Label label143;
    private widget.Label label144;
    private widget.Label label145;
    private widget.Label label146;
    private widget.Label label147;
    private widget.Label label148;
    private widget.Label label149;
    private widget.Label label15;
    private widget.Label label150;
    private widget.Label label151;
    private widget.Label label152;
    private widget.Label label153;
    private widget.Label label154;
    private widget.Label label155;
    private widget.Label label156;
    private widget.Label label157;
    private widget.Label label158;
    private widget.Label label159;
    private widget.Label label16;
    private widget.Label label160;
    private widget.Label label161;
    private widget.Label label162;
    private widget.Label label163;
    private widget.Label label164;
    private widget.Label label165;
    private widget.Label label166;
    private widget.Label label167;
    private widget.Label label168;
    private widget.Label label169;
    private widget.Label label17;
    private widget.Label label170;
    private widget.Label label171;
    private widget.Label label172;
    private widget.Label label173;
    private widget.Label label174;
    private widget.Label label175;
    private widget.Label label176;
    private widget.Label label177;
    private widget.Label label178;
    private widget.Label label179;
    private widget.Label label180;
    private widget.Label label181;
    private widget.Label label182;
    private widget.Label label183;
    private widget.Label label184;
    private widget.Label label185;
    private widget.Label label186;
    private widget.Label label187;
    private widget.Label label188;
    private widget.Label label189;
    private widget.Label label190;
    private widget.Label label191;
    private widget.Label label192;
    private widget.Label label193;
    private widget.Label label194;
    private widget.Label label195;
    private widget.Label label196;
    private widget.Label label197;
    private widget.Label label200;
    private widget.Label label201;
    private widget.Label label202;
    private widget.Label label204;
    private widget.Label label219;
    private widget.Label label220;
    private widget.Label label221;
    private widget.Label label222;
    private widget.Label label223;
    private widget.Label label224;
    private widget.Label label225;
    private widget.Label label226;
    private widget.Label label227;
    private widget.Label label228;
    private widget.Label label229;
    private widget.Label label230;
    private widget.Label label231;
    private widget.Label label232;
    private widget.Label label233;
    private widget.Label label234;
    private widget.Label label235;
    private widget.Label label236;
    private widget.Label label237;
    private widget.Label label238;
    private widget.Label label239;
    private widget.Label label240;
    private widget.Label label241;
    private widget.Label label242;
    private widget.Label label243;
    private widget.Label label244;
    private widget.Label label245;
    private widget.Label label246;
    private widget.Label label247;
    private widget.Label label248;
    private widget.Label label249;
    private widget.Label label250;
    private widget.Label label251;
    private widget.Label label252;
    private widget.Label label253;
    private widget.Label label254;
    private widget.Label label255;
    private widget.Label label256;
    private widget.Label label257;
    private widget.Label label258;
    private widget.Label label259;
    private widget.Label label260;
    private widget.Label label261;
    private widget.Label label262;
    private widget.Label label263;
    private widget.Label label264;
    private widget.Label label265;
    private widget.Label label266;
    private widget.Label label267;
    private widget.Label label268;
    private widget.Label label269;
    private widget.Label label270;
    private widget.Label label271;
    private widget.Label label272;
    private widget.Label label273;
    private widget.Label label274;
    private widget.Label label275;
    private widget.Label label276;
    private widget.Label label277;
    private widget.Label label278;
    private widget.Label label279;
    private widget.Label label280;
    private widget.Label label35;
    private widget.Label label36;
    private widget.Label label37;
    private widget.Label label38;
    private widget.Label label39;
    private widget.Label label40;
    private widget.Label label41;
    private widget.Label label42;
    private widget.Label label43;
    private widget.Label label44;
    private widget.Label label45;
    private widget.Label label46;
    private widget.Label label47;
    private widget.Label label48;
    private widget.Label label49;
    private widget.Label label50;
    private widget.Label label51;
    private widget.Label label52;
    private widget.Label label53;
    private widget.Label label54;
    private widget.Label label55;
    private widget.Label label56;
    private widget.Label label57;
    private widget.Label label58;
    private widget.Label label59;
    private widget.Label label60;
    private widget.Label label61;
    private widget.Label label62;
    private widget.Label label63;
    private widget.Label label64;
    private widget.Label label65;
    private widget.Label label66;
    private widget.Label label67;
    private widget.Label label68;
    private widget.Label label69;
    private widget.Label label70;
    private widget.Label label71;
    private widget.Label label72;
    private widget.Label label73;
    private widget.Label label74;
    private widget.Label label75;
    private widget.Label label76;
    private widget.Label label77;
    private widget.Label label78;
    private widget.Label label79;
    private widget.Label label80;
    private widget.Label label81;
    private widget.Label label82;
    private widget.Label label83;
    private widget.Label label84;
    private widget.Label label85;
    private widget.Label label86;
    private widget.Label label87;
    private widget.Label label88;
    private widget.Label label89;
    private widget.Label label9;
    private widget.Label label90;
    private widget.Label label91;
    private widget.Label label92;
    private widget.Label label93;
    private widget.Label label94;
    private widget.Label label95;
    private widget.Label label96;
    private widget.Label label97;
    private widget.Label label98;
    private widget.Label label99;
    private widget.CekBox manaj_1;
    private widget.CekBox manaj_2;
    private widget.CekBox manaj_3;
    private widget.CekBox manaj_4;
    private widget.CekBox mjn1;
    private widget.CekBox mjn2;
    private widget.CekBox mjn3;
    private widget.CekBox mjn4;
    private widget.CekBox mjn5;
    private widget.CekBox mjn6;
    private widget.CekBox mn_1;
    private widget.CekBox mn_2;
    private widget.CekBox mn_3;
    private widget.CekBox mual_1;
    private widget.CekBox mual_2;
    private widget.ComboBox nyeri_1;
    private widget.ComboBox nyeri_2;
    private widget.ComboBox nyeri_3;
    private widget.TextBox orientasi_ket;
    private widget.panelisi panelisi1;
    private widget.panelisi panelisi5;
    private widget.panelisi panelisi6;
    private widget.panelisi panelisi7;
    private widget.TextBox perkemihan_ket;
    private widget.CekBox pindah_1;
    private widget.CekBox pindah_2;
    private widget.CekBox pindah_3;
    private widget.CekBox pj_1;
    private widget.CekBox pj_2;
    private widget.CekBox pj_3;
    private widget.CekBox po_1;
    private widget.CekBox po_2;
    private widget.CekBox po_3;
    private javax.swing.JMenuItem pp1;
    private javax.swing.JMenuItem pp2;
    private javax.swing.JMenuItem ppCetakPreOp;
    private widget.TextBox rr;
    private widget.TextBox rr1;
    private widget.TextBox rr2;
    private widget.CekBox s1;
    private widget.CekBox s2;
    private widget.CekBox s3;
    private widget.CekBox s4;
    private widget.ScrollPane scrollInput1;
    private widget.ScrollPane scrollInput2;
    private widget.ScrollPane scrollInput3;
    private widget.TextBox skalanyeri_ket;
    private widget.CekBox sn1;
    private widget.CekBox sn2;
    private widget.CekBox sn3;
    private widget.CekBox sn4;
    private widget.TextBox spo;
    private widget.TextBox spo1;
    private widget.TextBox spo2;
    private widget.TextBox statuspernafasan_ket;
    private widget.TextBox suhu;
    private widget.TextBox suhu1;
    private widget.TextBox suhu2;
    private widget.TextBox t_kassa;
    private widget.TextBox t_pencucian;
    private widget.TextBox t_suction;
    private widget.Table tbInOp;
    private widget.Table tbMasalahKeperawatan;
    private widget.Table tbPostOp;
    private widget.Table tbPreOp;
    private widget.TextBox td;
    private widget.TextBox td1;
    private widget.TextBox td2;
    private widget.CekBox termo_1;
    private widget.CekBox termo_2;
    private widget.CekBox termo_3;
    private widget.CekBox termo_4;
    private widget.CekBox tn_1;
    private widget.CekBox tn_2;
    private widget.CekBox tn_3;
    // End of variables declaration//GEN-END:variables

    private void tampil() {
        Valid.tabelKosong(tabModePreOp);
        try{
            ps=koneksi.prepareStatement(
                   "select penilaian_awal_ok_preoperasi.*,pasien.nm_pasien,pasien.jk,pasien.no_rkm_medis,petugas.nip,petugas.nama "+
                   "from penilaian_awal_ok_preoperasi "+
                   "left join reg_periksa on reg_periksa.no_rawat = penilaian_awal_ok_preoperasi.no_rawat "+
                   "left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                   "left join petugas on penilaian_awal_ok_preoperasi.kd_petugas = petugas.nip "+
                   "where penilaian_awal_ok_preoperasi.no_rawat like ? order by penilaian_awal_ok_preoperasi.no_rawat");
            try {
                ps.setString(1,"%"+TCari.getText().trim()+"%");
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModePreOp.addRow(new Object[]{
                        rs.getString("no_rawat"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("nip"),
                        rs.getString("nama"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("jk"),
                        rs.getString("td"),
                        rs.getString("spo"),
                        rs.getString("hr"),
                        rs.getString("rr"),
                        rs.getString("suhu"),
                        rs.getString("keluhan_nyeri"),
                        rs.getString("keluhan_nyeri_ket"),
                        rs.getString("kesadaran"),
                        rs.getString("orientasi"),
                        rs.getString("orientasi_ket"),
                        rs.getString("status_pernafasan"),
                        rs.getString("status_pernafasan_ket"),
                        rs.getString("suara_nafas"),
                        rs.getString("alat_bantu_nafas"),
                        rs.getString("alat_bantu_o2"),
                        rs.getString("alat_bantu_ett"),
                        rs.getString("alat_bantu_tidakada"),
                        rs.getString("cardiopurmonal"),
                        rs.getString("cardiopurmonal_lokasi"),
                        rs.getString("ab_pacemaker"),
                        rs.getString("ab_kateterjantung"),
                        rs.getString("ab_tidakada"),
                        rs.getString("kulit"),
                        rs.getString("ket_kulit"),
                        rs.getString("muskolokeletal"),
                        rs.getString("perkemihan"),
                        rs.getString("perkemihan2"),
                        rs.getString("perkemihan3"),
                        rs.getString("perkemihan_ket"),
                        rs.getString("sensori"),
                        rs.getString("ab_alatdengar"),
                        rs.getString("ab_kacamata"),
                        rs.getString("tingkat_nyeri_skala"),
                        rs.getString("tingkat_nyeri_1"),
                        rs.getString("tingkat_nyeri_2"),
                        rs.getString("tingkat_nyeri_3"),
                        rs.getString("manajemen_nyeri_1"),
                        rs.getString("manajemen_nyeri_2"),
                        rs.getString("manajemen_nyeri_3"),
                        rs.getString("evaluasi_nyeri"),
                        rs.getString("ansietas_1"),
                        rs.getString("ansietas_2"),
                        rs.getString("ansietas_3"),
                        rs.getString("ansietas_4"),
                        rs.getString("reduksi_1"),
                        rs.getString("reduksi_2"),
                        rs.getString("reduksi_3"),
                        rs.getString("reduksi_4"),
                        rs.getString("reduksi_5"),
                        rs.getString("reduksi_6"),
                        rs.getString("evaluasi_1"),
                        rs.getString("evaluasi_2"),
                        rs.getString("evaluasi_3"),
                        rs.getString("evaluasi_4")
                        
                    });
                }
            } catch (Exception e) {
                System.out.println("Notifikasi : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            LCount.setText(""+tabModePreOp.getRowCount());
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
     
    private void tampilinop() {
        Valid.tabelKosong(tabModeInOp);
        try{
            ps=koneksi.prepareStatement(
                  "select penilaian_awal_ok_intraoperatif.*,petugas.nip,petugas.nama,pasien.nm_pasien,pasien.jk,pasien.no_rkm_medis "+
                   "from penilaian_awal_ok_intraoperatif "+
                   "left join reg_periksa on reg_periksa.no_rawat = penilaian_awal_ok_intraoperatif.no_rawat "+
                   "left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                   "left join petugas on penilaian_awal_ok_intraoperatif.kd_petugas = petugas.nip "+
                   "where penilaian_awal_ok_intraoperatif.no_rawat = '"+TNoRw1.getText()+"' order by penilaian_awal_ok_intraoperatif.no_rawat");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeInOp.addRow(new Object[]{
                        rs.getString("no_rawat"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("petugas.nip"),
                        rs.getString("petugas.nama"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("jk"),
                        rs.getString("td"),
                        rs.getString("spo2"),
                        rs.getString("hr"),
                        rs.getString("rr"),
                        rs.getString("suhu"),
                        rs.getString("kassa"),
                        rs.getString("irigasi"),
                        rs.getString("suction"),
                        rs.getString("jlo_1"),
                        rs.getString("jlo_2"),
                        rs.getString("jlo_3"),
                        rs.getString("jlo_4"),
                        rs.getString("kulit_1"),
                        rs.getString("kulit_2"),
                        rs.getString("kulit_3"),
                        rs.getString("kulit_4"),
                        rs.getString("kebersihan_tangan1"),
                        rs.getString("kebersihan_tangan"),
                        rs.getString("cegah_1"),
                        rs.getString("cegah_2"),
                        rs.getString("cegah_3"),
                        rs.getString("cegah_4"),
                        rs.getString("ct5_1"),
                        rs.getString("ct5_2"),
                        rs.getString("ct5_3"),
                        rs.getString("ct5_4"),
                        rs.getString("ct5_5"),
                        rs.getString("ct5_6"),
                        rs.getString("ct5_7"),
                        rs.getString("ct5_8"),
                        rs.getString("ct5_9"),
                        rs.getString("ct5_10"),
                        rs.getString("evaluasi_1"),
                        rs.getString("evaluasi_2"),
                        rs.getString("evaluasi_3"),
                        rs.getString("termo_1"),
                        rs.getString("termo_2"),
                        rs.getString("termo_3"),
                        rs.getString("termo_4"),
                        rs.getString("evaluasi2_1"),
                        rs.getString("evaluasi2_2"),
                        rs.getString("evaluasi2_3"),
                        rs.getString("evaluasi2_4"),
                        rs.getString("hipo_1"),
                        rs.getString("hipo_2"),
                        rs.getString("hipo_3"),
                        rs.getString("cairan_1"),
                        rs.getString("cairan_2"),
                        rs.getString("cairan_3"),
                        rs.getString("cairan_4"),
                        rs.getString("mnj_cairan_1"),
                        rs.getString("mnj_cairan_2"),
                        rs.getString("mnj_cairan_3"),
                        rs.getString("mnj_cairan_4"),
                        rs.getString("evaluasi3_1"),
                        rs.getString("evaluasi3_2"),
                        rs.getString("evaluasi3_3"),
                        rs.getString("evaluasi3_4")

                    });
                }
            } catch (Exception e) {
                System.out.println("Notifikasi : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
    
    private void tampilpostop() {
        Valid.tabelKosong(tabModePostOp);
        try{
            ps=koneksi.prepareStatement(
                  "select penilaian_awal_ok_postoperatif.*,petugas.nip,petugas.nama,pasien.nm_pasien,pasien.jk,pasien.no_rkm_medis "+
                   "from penilaian_awal_ok_postoperatif "+
                   "left join reg_periksa on reg_periksa.no_rawat = penilaian_awal_ok_postoperatif.no_rawat "+
                   "left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                   "left join petugas on penilaian_awal_ok_postoperatif.kd_petugas = petugas.nip "+
                   "where penilaian_awal_ok_postoperatif.no_rawat = '"+TNoRw3.getText()+"' order by penilaian_awal_ok_postoperatif.no_rawat");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModePostOp.addRow(new Object[]{
                        rs.getString("no_rawat"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("petugas.nip"),
                        rs.getString("petugas.nama"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("jk"),
                        rs.getString("td"),
                        rs.getString("spo2"),
                        rs.getString("hr"),
                        rs.getString("rr"),
                        rs.getString("suhu"),
                        rs.getString("nyeri_1"),
                        rs.getString("nyeri_2"),
                        rs.getString("k1"),
                        rs.getString("k2"),
                        rs.getString("k3"),
                        rs.getString("kk1"),
                        rs.getString("kk2"),
                        rs.getString("kk3"),
                        rs.getString("kk4"),
                        rs.getString("kk5"),
                        rs.getString("s1"),
                        rs.getString("s2"),
                        rs.getString("s3"),
                        rs.getString("s4"),
                        rs.getString("sn1"),
                        rs.getString("sn2"),
                        rs.getString("sn3"),
                        rs.getString("sn4"),
                        rs.getString("jn1"),
                        rs.getString("jn2"),
                        rs.getString("jn3"),
                        rs.getString("jn4"),
                        rs.getString("jn5"),
                        rs.getString("jn6"),
                        rs.getString("mjn1"),
                        rs.getString("mjn2"),
                        rs.getString("mjn3"),
                        rs.getString("mjn4"),
                        rs.getString("mjn5"),
                        rs.getString("mjn6"),
                        rs.getString("ev_1_1"),
                        rs.getString("ev_1_2"),
                        rs.getString("kel_1"),
                        rs.getString("kel_2"),
                        rs.getString("mual_1"),
                        rs.getString("mual_2"),
                        rs.getString("ev_2_1"),
                        rs.getString("ev_2_2"),
                        rs.getString("nyeri_3"),
                        rs.getString("tn_1"),
                        rs.getString("tn_2"),
                        rs.getString("tn_3"),
                        rs.getString("mn_1"),
                        rs.getString("mn_2"),
                        rs.getString("mn_3"),
                        rs.getString("nyeri_4"),
                        rs.getString("jatuh"),
                        rs.getString("pj_1"),
                        rs.getString("pj_2"),
                        rs.getString("pj_3"),
                        rs.getString("ev_3_1"),
                        rs.getString("ev_3_2"),
                        rs.getString("po_1"),
                        rs.getString("po_2"),
                        rs.getString("po_3"),
                        rs.getString("pindah_1"),
                        rs.getString("pindah_2"),
                        rs.getString("pindah_3"),
                      

                    });
                }
            } catch (Exception e) {
                System.out.println("Notifikasi : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }

    
    public void setNoRm(String NoRawat){
        TNoRw1.setText(NoRawat);
        TCari.setText(NoRawat);
        TNoRw2.setText(NoRawat);
        TCari1.setText(NoRawat);
        TNoRw3.setText(NoRawat);
        TCari2.setText(NoRawat);
        tampil();
        tampilinop();
        tampilpostop();
        
        String no_rm_pasien = Sequel.cariIsi("select reg_periksa.no_rkm_medis from reg_periksa where reg_periksa.no_rawat='"+TNoRw1.getText()+"'");
        String nama_pasien = Sequel.cariIsi("select pasien.nm_pasien from pasien where pasien.no_rkm_medis='"+no_rm_pasien+"'");
        String jk_pasien = Sequel.cariIsi("select pasien.jk from pasien where pasien.no_rkm_medis='"+no_rm_pasien+"'");
        TNoRM1.setText(no_rm_pasien);
        TNoRM2.setText(no_rm_pasien);
        TNoRM3.setText(no_rm_pasien);
        TPasien1.setText(nama_pasien);
        TPasien2.setText(nama_pasien);
        TPasien3.setText(nama_pasien);
        
        jk.setText(jk_pasien);
        jk1.setText(jk_pasien);
        jk2.setText(jk_pasien);
        
        ChkJam1.setSelected(true);
        ChkJam2.setSelected(true);
        ChkJam3.setSelected(true);
       
        
    }
    public void setNoRm2(String NoRawat,String NoRkmMedis,String Nama,String Tanggal){
        tampil();
        tampilinop();
        tampilpostop();
    }
    private void getData() {
        int row=tbPreOp.getSelectedRow();
        if(row!= -1){
            ChkJam1.setSelected(false);
            TNoRw1.setText(tbPreOp.getValueAt(row,0).toString());
            Valid.SetTgl2(Tanggal,tbPreOp.getValueAt(tbPreOp.getSelectedRow(),1).toString());
            Jam.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),2).toString().substring(0,2));
            Menit.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),2).toString().substring(3,5));
            Detik.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),2).toString().substring(6,8));
            KdPetugas.setText(tbPreOp.getValueAt(row,3).toString());
            NmPetugas.setText(tbPreOp.getValueAt(row,4).toString());
            TNoRM1.setText(tbPreOp.getValueAt(row,5).toString());
            TPasien1.setText(tbPreOp.getValueAt(row,6).toString());
            jk.setText(tbPreOp.getValueAt(row,7).toString());       
            td.setText(tbPreOp.getValueAt(row,8).toString());
            spo.setText(tbPreOp.getValueAt(row,9).toString());
            hr.setText(tbPreOp.getValueAt(row,10).toString());
            rr.setText(tbPreOp.getValueAt(row,11).toString());
            suhu.setText(tbPreOp.getValueAt(row,12).toString());
            cmbSkalanyeri.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),13).toString());
            skalanyeri_ket.setText(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),14).toString());
            cmbKesadaran.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),15).toString());
            cmbOrientasi.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),16).toString());
            orientasi_ket.setText(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),17).toString());
            cmbStatuspernafasan.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),18).toString());
            statuspernafasan_ket.setText(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),19).toString());
            cmbSuaranafas.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),20).toString());
            cmbPernafasanalatbantu.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),21).toString());
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),22).toString().equals("true")) {
                chkPernafasan1.setSelected(true);
            }else{
                chkPernafasan1.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),23).toString().equals("true")) {
                chkPernafasan2.setSelected(true);
            }else{
                chkPernafasan2.setSelected(false);
            }
            
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),24).toString().equals("true")) {
                chkPernafasan3.setSelected(true);
            }else{
                chkPernafasan3.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),25).toString().equals("true")) {
                chkCardiopurmonal1.setSelected(false);
                chkCardiopurmonal2.setSelected(true);
            }else{
                chkCardiopurmonal2.setSelected(false);
                chkCardiopurmonal1.setSelected(true);
            } 
            
       
            Cardiopurmonal_ket.setText(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),26).toString());
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),27).toString().equals("true")) {
                chkalatbantu1.setSelected(true);
            }else{
                chkalatbantu1.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),28).toString().equals("true")) {
                chkalatbantu2.setSelected(true);
            }else{
                chkalatbantu2.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),29).toString().equals("true")) {
                chkalatbantu3.setSelected(true);
            }else{
                chkalatbantu3.setSelected(false);
            }
            
            cmbKulit.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),30).toString());
            ket_kulit.setText(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),31).toString());
            cmbMuskolokeletal.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),32).toString());
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),33).toString().equals("true")) {
                 chkKemih1.setSelected(true);
            }else{
                 chkKemih1.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),34).toString().equals("true")) {
                 chkKemih2.setSelected(true);
            }else{
                 chkKemih2.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),35).toString().equals("true")) {
                 chkKemih3.setSelected(true);
            }else{
                 chkKemih3.setSelected(false);
            }
            perkemihan_ket.setText(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),36).toString());  
            cmbSensori.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),37).toString());
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),38).toString().equals("true")) {
                 chkAlatbantuFix1.setSelected(true);
            }else{
                 chkAlatbantuFix1.setSelected(false);
            }
            
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),39).toString().equals("true")) {
                 chkAlatbantuFix2.setSelected(true);
            }else{
                 chkAlatbantuFix2.setSelected(false);
            }
            cmbTingkatNyeriSkala.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),40).toString());
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),41).toString().equals("true")) {
                 chkTingkatnyeri1.setSelected(true);
            }else{
                 chkTingkatnyeri1.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),42).toString().equals("true")) {
                 chkTingkatnyeri2.setSelected(true);
            }else{
                 chkTingkatnyeri2.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),43).toString().equals("true")) {
                 chkTingkatnyeri3.setSelected(true);
            }else{
                 chkTingkatnyeri3.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),44).toString().equals("true")) {
                 chkManajemen1.setSelected(true);
            }else{
                 chkManajemen1.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),45).toString().equals("true")) {
                 chkManajemen2.setSelected(true);
            }else{
                 chkManajemen2.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),46).toString().equals("true")) {
                 chkManajemen3.setSelected(true);
            }else{
                 chkManajemen3.setSelected(false);
            }
            
            cmbEvaluasinyeri.setSelectedItem(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),47).toString());
            
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),48).toString().equals("true")) {
                 chkAnsietas1.setSelected(true);
            }else{
                 chkAnsietas1.setSelected(false);
            }
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),49).toString().equals("true")) {
                 chkAnsietas2.setSelected(true);
            }else{
                 chkAnsietas2.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),50).toString().equals("true")) {
                 chkAnsietas3.setSelected(true);
            }else{
                 chkAnsietas3.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),51).toString().equals("true")) {
                 chkAnsietas4.setSelected(true);
            }else{
                 chkAnsietas4.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),52).toString().equals("true")) {
                  chkReduksi1.setSelected(true);
            }else{
                  chkReduksi1.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),53).toString().equals("true")) {
                  chkReduksi2.setSelected(true);
            }else{
                  chkReduksi2.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),54).toString().equals("true")) {
                  chkReduksi3.setSelected(true);
            }else{
                  chkReduksi3.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),55).toString().equals("true")) {
                  chkReduksi4.setSelected(true);
            }else{
                  chkReduksi4.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),56).toString().equals("true")) {
                  chkReduksi5.setSelected(true);
            }else{
                  chkReduksi5.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),57).toString().equals("true")) {
                  chkReduksi6.setSelected(true);
            }else{
                  chkReduksi6.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),58).toString().equals("true")) {
                  chkEvaluasi1.setSelected(true);
            }else{
                  chkEvaluasi1.setSelected(false);
            }
           if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),59).toString().equals("true")) {
                  chkEvaluasi2.setSelected(true);
            }else{
                  chkEvaluasi2.setSelected(false);
            }
        
            if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),60).toString().equals("true")) {
                  chkEvaluasi3.setSelected(true);
            }else{
                  chkEvaluasi3.setSelected(false);
            }
          if(tbPreOp.getValueAt(tbPreOp.getSelectedRow(),61).toString().equals("true")) {
                  chkEvaluasi4.setSelected(true);
            }else{
                  chkEvaluasi4.setSelected(false);
            }
        }
    }
    
  private void getData2() {
        int row=tbInOp.getSelectedRow();
        int selectedRow = tbInOp.getSelectedRow();
        if(row!= -1){
            ChkJam2.setSelected(false);
            TNoRw2.setText(tbInOp.getValueAt(row,0).toString());
            Valid.SetTgl2(Tanggal1,tbInOp.getValueAt(tbInOp.getSelectedRow(),1).toString());
            Jam1.setSelectedItem(tbInOp.getValueAt(tbInOp.getSelectedRow(),2).toString().substring(0,2));
            Menit1.setSelectedItem(tbInOp.getValueAt(tbInOp.getSelectedRow(),2).toString().substring(3,5));
            Detik1.setSelectedItem(tbInOp.getValueAt(tbInOp.getSelectedRow(),2).toString().substring(6,8));
            KdPetugas1.setText(tbInOp.getValueAt(row,3).toString());
            NmPetugas1.setText(tbInOp.getValueAt(row,4).toString());
            TNoRM2.setText(tbInOp.getValueAt(row,5).toString());
            TPasien2.setText(tbInOp.getValueAt(row,6).toString());
            jk1.setText(tbInOp.getValueAt(row,7).toString());       
            td1.setText(tbInOp.getValueAt(row,8).toString());
            spo1.setText(tbInOp.getValueAt(row,9).toString());
            hr1.setText(tbInOp.getValueAt(row,10).toString());
            rr1.setText(tbInOp.getValueAt(row,11).toString());
            suhu1.setText(tbInOp.getValueAt(row,12).toString());
            t_kassa.setText(tbInOp.getValueAt(tbInOp.getSelectedRow(), 13).toString());
            t_pencucian.setText(tbInOp.getValueAt(tbInOp.getSelectedRow(), 14).toString());
            t_suction.setText(tbInOp.getValueAt(tbInOp.getSelectedRow(), 15).toString());
            chk_luka_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 16))));
            chk_luka_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 17))));
            chk_luka_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 18))));
            chk_luka_4.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 19))));
            chk_kulit_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 20))));
            chk_kulit_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 21))));
            chk_kulit_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 22))));
            chk_kulit_4.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 23))));
            chk_kontrol_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 24))));
            chk_kontrol_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 25))));
            chk_jenisluka_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 26))));
            chk_jenisluka_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 27))));
            chk_jenisluka_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 28))));
            chk_jenisluka_4.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 29))));
            chk_cuci_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 30))));
            chk_cuci_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 31))));
            chk_cuci_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 32))));
            chk_cuci_4.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 33))));
            chk_cuci_5.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 34))));
            chk_cuci_6.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 35))));
            chk_cuci_7.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 36))));
            chk_cuci_8.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 37))));
            chk_cuci_9.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 38))));
            chk_cuci_10.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 39))));
            evaluasi_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 40))));
            evaluasi_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 41))));
            evaluasi_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 42))));
            termo_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 43))));
            termo_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 44))));
            termo_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 45))));
            termo_4.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 46))));
            evaluasi2_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 47))));
            evaluasi2_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 48))));
            evaluasi2_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 49))));
            evaluasi2_4.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 50))));
            hipo_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 51))));
            hipo_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 52))));
            hipo_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 53))));
            cairan_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 54))));
            cairan_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 55))));
            cairan_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 56))));
            cairan_4.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 57))));
            manaj_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 58))));
            manaj_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 59))));
            manaj_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 60))));
            manaj_4.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 61))));
            evaluasi3_1.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 62))));
            evaluasi3_2.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 63))));
            evaluasi3_3.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 64))));
            evaluasi3_4.setSelected("true".equals(String.valueOf(tbInOp.getValueAt(selectedRow, 65))));
        }
  }
  
  
  private void getData3() {
        int row=tbPostOp.getSelectedRow();
        int selectedRow = tbPostOp.getSelectedRow();
        if(row!= -1){
            ChkJam3.setSelected(false);
            TNoRw3.setText(tbPostOp.getValueAt(row,0).toString());
            Valid.SetTgl2(Tanggal2,tbPostOp.getValueAt(tbPostOp.getSelectedRow(),1).toString());
            Jam2.setSelectedItem(tbPostOp.getValueAt(tbPostOp.getSelectedRow(),2).toString().substring(0,2));
            Menit2.setSelectedItem(tbPostOp.getValueAt(tbPostOp.getSelectedRow(),2).toString().substring(3,5));
            Detik2.setSelectedItem(tbPostOp.getValueAt(tbPostOp.getSelectedRow(),2).toString().substring(6,8));
            KdPetugas2.setText(tbPostOp.getValueAt(row,3).toString());
            NmPetugas2.setText(tbPostOp.getValueAt(row,4).toString());
            TNoRM3.setText(tbPostOp.getValueAt(row,5).toString());
            TPasien3.setText(tbPostOp.getValueAt(row,6).toString());
            jk2.setText(tbPostOp.getValueAt(row,7).toString());       
            td2.setText(tbPostOp.getValueAt(row,8).toString());
            spo2.setText(tbPostOp.getValueAt(row,9).toString());
            hr2.setText(tbPostOp.getValueAt(row,10).toString());
            rr2.setText(tbPostOp.getValueAt(row,11).toString());
            suhu2.setText(tbPostOp.getValueAt(row,12).toString());
            nyeri_1.setSelectedItem(tbPostOp.getValueAt(row,13).toString());
            chknyeri3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 14))));
            k1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 15))));
            k2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 16))));
            k3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 17))));
            kk1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 18))));
            kk2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 19))));
            kk3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 20))));
            kk4.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 21))));
            kk5.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 22))));
            s1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 23))));
            s2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 24))));
            s3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 25))));
            s4.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 26))));
            sn1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 27))));
            sn2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 28))));
            sn3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 29))));
            sn4.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 30))));
            jn1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 31))));
            jn2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 32))));
            jn3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 33))));
            jn4.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 34))));
            jn5.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 35))));
            jn6.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 36))));
            mjn1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 37))));
            mjn2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 38))));
            mjn3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 39))));
            mjn4.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 40))));
            mjn5.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 41))));
            mjn6.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 42))));
            ev_1_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 43))));
            ev_1_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 44))));
            kel_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 45))));
            kel_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 46))));
            mual_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 47))));
            mual_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 48))));
            ev_2_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 49))));
            ev_2_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 50))));
            nyeri_2.setSelectedItem(tbPostOp.getValueAt(row,51).toString());
            tn_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 52))));
            tn_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 53))));
            tn_3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 54))));
            mn_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 55))));
            mn_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 56))));
            mn_3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 57))));
            nyeri_3.setSelectedItem(tbPostOp.getValueAt(row,58).toString());
            jatuh.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 59))));
            pj_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 60))));
            pj_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 61))));
            pj_3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 62))));
            ev_3_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 63))));
            ev_3_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 64))));
            po_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 65))));
            po_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 66))));
            po_3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 67))));
            pindah_1.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 68))));
            pindah_2.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 69))));
            pindah_3.setSelected("true".equals(String.valueOf(tbPostOp.getValueAt(selectedRow, 70))));
        }
  }

    
    public void emptTeks() {
        if(TabOK.getSelectedIndex()==0){
                        ChkJam1.setSelected(true);
                        KdPetugas.setText("");
                        NmPetugas.setText("");
                        td.setText("");
                        spo.setText("");
                        hr.setText("");
                        rr.setText("");
                        suhu.setText("");
                        cmbSkalanyeri.setSelectedIndex(0);
                        skalanyeri_ket.setText("");
                        cmbKesadaran.setSelectedIndex(0);
                        cmbOrientasi.setSelectedIndex(0);
                        orientasi_ket.setText("");
                        cmbStatuspernafasan.setSelectedIndex(0);
                        statuspernafasan_ket.setText("");
                        cmbSuaranafas.setSelectedIndex(0);
                        cmbPernafasanalatbantu.setSelectedIndex(0);
                        chkPernafasan1.setSelected(false);
                        chkPernafasan2.setSelected(false);
                        chkPernafasan3.setSelected(false);
                        chkCardiopurmonal1.setSelected(false);
                        chkCardiopurmonal2.setSelected(false);
                        Cardiopurmonal_ket.setText("");
                        chkalatbantu1.setSelected(false);
                        chkalatbantu2.setSelected(false);
                        chkalatbantu3.setSelected(false);
                        cmbKulit.setSelectedIndex(0);
                        ket_kulit.setText("");
                        cmbMuskolokeletal.setSelectedIndex(0);
                        chkKemih1.setSelected(false);
                        chkKemih2.setSelected(false);
                        chkKemih3.setSelected(false);
                        perkemihan_ket.setText("");
                        cmbSensori.setSelectedIndex(0);
                        chkAlatbantuFix1.setSelected(false);
                        chkAlatbantuFix2.setSelected(false);
                        cmbTingkatNyeriSkala.setSelectedIndex(0);
                        chkTingkatnyeri1.setSelected(false);
                        chkTingkatnyeri2.setSelected(false);
                        chkTingkatnyeri3.setSelected(false);
                        chkManajemen1.setSelected(false);
                        chkManajemen2.setSelected(false);
                        chkManajemen3.setSelected(false);
                        cmbEvaluasinyeri.setSelectedIndex(0);
                        chkAnsietas1.setSelected(false);
                        chkAnsietas2.setSelected(false);
                        chkAnsietas3.setSelected(false);
                        chkAnsietas4.setSelected(false);
                        chkReduksi1.setSelected(false);
                        chkReduksi2.setSelected(false);
                        chkReduksi3.setSelected(false);
                        chkReduksi4.setSelected(false);
                        chkReduksi5.setSelected(false);
                        chkReduksi6.setSelected(false);
                        chkEvaluasi1.setSelected(false);
                        chkEvaluasi2.setSelected(false);
                        chkEvaluasi3.setSelected(false);
                        chkEvaluasi4.setSelected(false);
        }else if(TabOK.getSelectedIndex()==1){
                        ChkJam2.setSelected(true);
                        KdPetugas1.setText("");
                        NmPetugas1.setText("");
                        td1.setText("");
                        spo1.setText("");
                        hr1.setText("");
                        rr1.setText("");
                        suhu1.setText("");
                        t_kassa.setText("");
                        t_pencucian.setText("");
                        t_suction.setText("");
                        chk_luka_1.setSelected(false);
                        chk_luka_2.setSelected(false);
                        chk_luka_3.setSelected(false);
                        chk_luka_4.setSelected(false);
                        chk_kulit_1.setSelected(false);
                        chk_kulit_2.setSelected(false);
                        chk_kulit_3.setSelected(false);
                        chk_kulit_4.setSelected(false);
                        chk_kontrol_1.setSelected(false);
                        chk_kontrol_2.setSelected(false);
                        chk_jenisluka_1.setSelected(false);
                        chk_jenisluka_2.setSelected(false);
                        chk_jenisluka_3.setSelected(false);
                        chk_jenisluka_4.setSelected(false);
                        chk_cuci_1.setSelected(false);
                        chk_cuci_2.setSelected(false);
                        chk_cuci_3.setSelected(false);
                        chk_cuci_4.setSelected(false);
                        chk_cuci_5.setSelected(false);
                        chk_cuci_6.setSelected(false);
                        chk_cuci_7.setSelected(false);
                        chk_cuci_8.setSelected(false);
                        chk_cuci_9.setSelected(false);
                        chk_cuci_10.setSelected(false);
                        evaluasi_1.setSelected(false);
                        evaluasi_2.setSelected(false);
                        evaluasi_3.setSelected(false);
                        termo_1.setSelected(false);
                        termo_2.setSelected(false);
                        termo_3.setSelected(false);
                        termo_4.setSelected(false);
                        evaluasi2_1.setSelected(false);
                        evaluasi2_2.setSelected(false);
                        evaluasi2_3.setSelected(false);
                        evaluasi2_4.setSelected(false);
                        hipo_1.setSelected(false);
                        hipo_2.setSelected(false);
                        hipo_3.setSelected(false);
                        cairan_1.setSelected(false);
                        cairan_2.setSelected(false);
                        cairan_3.setSelected(false);
                        cairan_4.setSelected(false);
                        manaj_1.setSelected(false);
                        manaj_2.setSelected(false);
                        manaj_3.setSelected(false);
                        manaj_4.setSelected(false);
                        evaluasi3_1.setSelected(false);
                        evaluasi3_2.setSelected(false);
                        evaluasi3_3.setSelected(false);
                        evaluasi3_4.setSelected(false);
        }else if(TabOK.getSelectedIndex()==2){
                        ChkJam3.setSelected(true);
                        KdPetugas2.setText("");
                        NmPetugas2.setText("");
                        td2.setText("");
                        spo2.setText("");
                        hr2.setText("");
                        rr2.setText("");
                        suhu2.setText("");
                        nyeri_1.setSelectedIndex(0);
                        chknyeri3.setSelected(false);
                        k1.setSelected(false);
                        k2.setSelected(false);
                        k3.setSelected(false);
                        kk1.setSelected(false);
                        kk2.setSelected(false);
                        kk3.setSelected(false);
                        kk4.setSelected(false);
                        kk5.setSelected(false);
                        s1.setSelected(false);
                        s2.setSelected(false);
                        s3.setSelected(false);
                        s4.setSelected(false);
                        sn1.setSelected(false);
                        sn2.setSelected(false);
                        sn3.setSelected(false);
                        sn4.setSelected(false);
                        jn1.setSelected(false);
                        jn2.setSelected(false);
                        jn3.setSelected(false);
                        jn4.setSelected(false);
                        jn5.setSelected(false);
                        jn6.setSelected(false);
                        mjn1.setSelected(false);
                        mjn2.setSelected(false);
                        mjn3.setSelected(false);
                        mjn4.setSelected(false);
                        mjn5.setSelected(false);
                        mjn6.setSelected(false);
                        ev_1_1.setSelected(false);
                        ev_1_2.setSelected(false);
                        kel_1.setSelected(false);
                        kel_2.setSelected(false);
                        mual_1.setSelected(false);
                        mual_2.setSelected(false);
                        ev_2_1.setSelected(false);
                        ev_2_2.setSelected(false);
                        nyeri_2.setSelectedIndex(0);
                        nyeri_3.setSelectedIndex(0);
                        tn_1.setSelected(false);
                        tn_2.setSelected(false);
                        tn_3.setSelected(false);
                        mn_1.setSelected(false);
                        mn_2.setSelected(false);
                        mn_3.setSelected(false);
                        jatuh.setSelected(false);
                        po_1.setSelected(false);
                        po_2.setSelected(false);
                        po_3.setSelected(false);
                        pj_1.setSelected(false);
                        pj_2.setSelected(false);
                        pj_3.setSelected(false);
                        ev_3_1.setSelected(false);
                        ev_3_2.setSelected(false);
                        pindah_1.setSelected(false);
                        pindah_2.setSelected(false);
                        pindah_3.setSelected(false);
                     
        }else if(TabOK.getSelectedIndex()==3){
                       
                        
           
        }            
    }
    
    private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam1.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkJam1.isSelected()==false){
                    nilai_jam =Jam.getSelectedIndex();
                    nilai_menit =Menit.getSelectedIndex();
                    nilai_detik =Detik.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam.setSelectedItem(jam);
                Menit.setSelectedItem(menit);
                Detik.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    private void jam1(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam2;
            private int nilai_menit2;
            private int nilai_detik2;
            public void actionPerformed(ActionEvent e) {
                String nol_jam2 = "";
                String nol_menit2 = "";
                String nol_detik2 = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam2.isSelected()==true){
                    nilai_jam2 = now.getHours();
                    nilai_menit2 = now.getMinutes();
                    nilai_detik2 = now.getSeconds();
                }else if(ChkJam2.isSelected()==false){
                    nilai_jam2 =Jam1.getSelectedIndex();
                    nilai_menit2 =Menit1.getSelectedIndex();
                    nilai_detik2 =Detik1.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam2 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam2 = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit2 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit2 = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik2 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik2 = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam2 = nol_jam2 + Integer.toString(nilai_jam2);
                String menit2 = nol_menit2 + Integer.toString(nilai_menit2);
                String detik2 = nol_detik2 + Integer.toString(nilai_detik2);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam1.setSelectedItem(jam2);
                Menit1.setSelectedItem(menit2);
                Detik1.setSelectedItem(detik2);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
    private void jam2(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam3;
            private int nilai_menit3;
            private int nilai_detik3;
            public void actionPerformed(ActionEvent e) {
                String nol_jam3 = "";
                String nol_menit3 = "";
                String nol_detik3 = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam3.isSelected()==true){
                    nilai_jam3 = now.getHours();
                    nilai_menit3 = now.getMinutes();
                    nilai_detik3 = now.getSeconds();
                }else if(ChkJam3.isSelected()==false){
                    nilai_jam3 =Jam2.getSelectedIndex();
                    nilai_menit3 =Menit2.getSelectedIndex();
                    nilai_detik3 =Detik2.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam3 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam3 = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit3 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit3 = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik3 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik3 = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam3 = nol_jam3 + Integer.toString(nilai_jam3);
                String menit3 = nol_menit3 + Integer.toString(nilai_menit3);
                String detik3 = nol_detik3 + Integer.toString(nilai_detik3);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam2.setSelectedItem(jam3);
                Menit2.setSelectedItem(menit3);
                Detik2.setSelectedItem(detik3);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
    private void isPhoto(){
        if(ChkAccor.isSelected()==true){
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(internalFrame1.getWidth()-650,HEIGHT));
            TabData.setVisible(true);  
            ChkAccor.setVisible(true);
        }else if(ChkAccor.isSelected()==false){    
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(15,HEIGHT));
            TabData.setVisible(false);  
            ChkAccor.setVisible(true);
        }
    }
    private void isPhoto1(){
        if(ChkAccor1.isSelected()==true){
            ChkAccor1.setVisible(false);
            PanelAccor1.setPreferredSize(new Dimension(internalFrame1.getWidth()-650,HEIGHT));
            TabData1.setVisible(true);  
            ChkAccor1.setVisible(true);
        }else if(ChkAccor1.isSelected()==false){    
            ChkAccor1.setVisible(false);
            PanelAccor1.setPreferredSize(new Dimension(15,HEIGHT));
            TabData1.setVisible(false);  
            ChkAccor1.setVisible(true);
        }
    }
    private void isPhoto2(){
        if(ChkAccor2.isSelected()==true){
            ChkAccor2.setVisible(false);
            PanelAccor2.setPreferredSize(new Dimension(internalFrame1.getWidth()-650,HEIGHT));
            TabData2.setVisible(true);  
            ChkAccor2.setVisible(true);
        }else if(ChkAccor2.isSelected()==false){    
            ChkAccor2.setVisible(false);
            PanelAccor2.setPreferredSize(new Dimension(15,HEIGHT));
            TabData2.setVisible(false);  
            ChkAccor2.setVisible(true);
        }
    }
}
