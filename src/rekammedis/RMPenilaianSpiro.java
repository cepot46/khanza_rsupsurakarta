/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * kontribusi dari Anggit Nurhidayah
 */

package rekammedis;

import surat.*;
import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariDokter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.Timer;
import kepegawaian.DlgCariPetugas;


/**
 * 
 * @author salimmulyana
 */
public final class RMPenilaianSpiro extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private ResultSet rs;
    private int i=0,pilihan=0;
    private String tgl,finger="",kodedokter="",namadokter="";
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMPenilaianSpiro(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(628,674);
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.Rekam Medis","Nama Pasien",
            "Tanggal Periksa","Jam","Kode Dokter","Dokter","NIP","Petugas","TB","BB","Pekerjaan","Merokok","Lama Merokok","Jumlah Merokok","Pengobatan","Pemeriksaan 1A",
            "Pemeriksaan 1B","Pemeriksaan 1C","Prediksi 1A","Hasil 2A","Pemeriksaan 3A","Pemeriksaan 3B","Pemeriksaan 3C","Prediksi 3A","Hasil 4A","Pemeriksaan 5A","Pemeriksaan 5B","Pemeriksaan 5C",
            "Prediksi 5A","Hasil 6A","Hasil 7A","Pemeriksaan 8A","Pemeriksaan 8B","Pemeriksaan 8C","Kesimpulan Hasil A","Kesimpulan Hasil B","Kesimpulan Hasil C","Uji 4A","Uji 5A","Uji 5B","Uji 5C","Uji 6A","VEP 4","VEP 5","Catatan Dokter"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 45; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(190);
            }else if(i==1){
                column.setPreferredWidth(105);
            }else if(i==2){
                column.setPreferredWidth(120);
            }else if(i==3){
                column.setPreferredWidth(200);
            }else if(i==4){
                column.setPreferredWidth(165);
            }else if(i==5){
                column.setPreferredWidth(135);
            }else if(i==6){
                column.setPreferredWidth(135);
            }else if(i==7){
                column.setPreferredWidth(100);
            }else if(i==8){
                column.setPreferredWidth(100);
            }else if(i==9){
                column.setPreferredWidth(100);
            }else if(i==10){
                column.setPreferredWidth(150);
            }else if(i==11){
                column.setPreferredWidth(120);
            }else if(i==12){
                column.setPreferredWidth(120);
            }else if(i==13){
                column.setPreferredWidth(120);
            }else if(i==14){
                column.setPreferredWidth(120);
            }else if(i==15){
                column.setPreferredWidth(120);
            }else if(i==16){
                column.setPreferredWidth(120);
            }else if(i==18){
                column.setPreferredWidth(120);
            }else if(i==19||i==44){
                column.setPreferredWidth(120);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
      
        BB.setDocument(new batasInput((byte)3).getKata(BB));
        TB.setDocument(new batasInput((byte)3).getKata(TB));
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));  
        TCari.setDocument(new batasInput((byte)100).getKata(TCari));           
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){                   
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                }  
                KdPetugas.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){      
                        KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                        btnOperator1.requestFocus();
                }  
                    
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        ChkInput.setSelected(true);
        isForm();
        jam();
        tampil();
    }
        
        

    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnVerifikasiSpiro = new javax.swing.JMenuItem();
        MnCetakSpiro = new javax.swing.JMenuItem();
        DlgVerifikasi = new javax.swing.JDialog();
        internalFrame7 = new widget.InternalFrame();
        panelBiasa6 = new widget.PanelBiasa();
        BtnSimpanVerifikasi = new widget.Button();
        BtnKeluarSP = new widget.Button();
        NoRawat = new widget.TextBox();
        jLabel72 = new widget.Label();
        jLabel73 = new widget.Label();
        NoRM = new widget.TextBox();
        NmPasien = new widget.TextBox();
        jLabel78 = new widget.Label();
        jLabel42 = new widget.Label();
        jLabel65 = new widget.Label();
        jLabel66 = new widget.Label();
        jLabel67 = new widget.Label();
        jLabel68 = new widget.Label();
        jLabel69 = new widget.Label();
        cb_kesimpulan3 = new widget.ComboBox();
        cb_kesimpulan2 = new widget.ComboBox();
        cb_kesimpulan1 = new widget.ComboBox();
        jLabel70 = new widget.Label();
        btnOperator1 = new widget.Button();
        NmDokter = new widget.TextBox();
        KdDokter = new widget.TextBox();
        scrollPane3 = new widget.ScrollPane();
        catatandokter = new widget.TextArea();
        jLabel75 = new widget.Label();
        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        jPanel3 = new javax.swing.JPanel();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        PanelInput = new javax.swing.JPanel();
        ChkInput = new widget.CekBox();
        Scroll1 = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        jLabel4 = new widget.Label();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        CbPengobatan = new widget.ComboBox();
        Tanggal = new widget.Tanggal();
        jLabel16 = new widget.Label();
        jLabel11 = new widget.Label();
        TB = new widget.TextBox();
        BB = new widget.TextBox();
        jLabel15 = new widget.Label();
        jLabel17 = new widget.Label();
        jLabel20 = new widget.Label();
        jLabel24 = new widget.Label();
        ChkJam1 = new widget.CekBox();
        Detik = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Jam = new widget.ComboBox();
        jLabel18 = new widget.Label();
        jLabel25 = new widget.Label();
        jLabel26 = new widget.Label();
        jLabel27 = new widget.Label();
        jLabel29 = new widget.Label();
        pekerjaan = new widget.TextBox();
        lama = new widget.TextBox();
        jLabel30 = new widget.Label();
        jLabel31 = new widget.Label();
        jumlah = new widget.TextBox();
        CbMerokok = new widget.ComboBox();
        jLabel32 = new widget.Label();
        jLabel33 = new widget.Label();
        jLabel34 = new widget.Label();
        jLabel35 = new widget.Label();
        jLabel36 = new widget.Label();
        jLabel37 = new widget.Label();
        jLabel38 = new widget.Label();
        jLabel39 = new widget.Label();
        jLabel40 = new widget.Label();
        jLabel41 = new widget.Label();
        jLabel43 = new widget.Label();
        jLabel44 = new widget.Label();
        jLabel45 = new widget.Label();
        jLabel46 = new widget.Label();
        pem_3c = new widget.TextBox();
        pem_1a = new widget.TextBox();
        lama3 = new widget.TextBox();
        pem_1b = new widget.TextBox();
        jLabel47 = new widget.Label();
        jLabel48 = new widget.Label();
        jLabel49 = new widget.Label();
        jLabel50 = new widget.Label();
        pem_1c = new widget.TextBox();
        pre_1a = new widget.TextBox();
        has_2a = new widget.TextBox();
        pem_3a = new widget.TextBox();
        pem_3b = new widget.TextBox();
        pre_3a = new widget.TextBox();
        jLabel51 = new widget.Label();
        jLabel52 = new widget.Label();
        jLabel53 = new widget.Label();
        jLabel54 = new widget.Label();
        lama11 = new widget.TextBox();
        has_4a = new widget.TextBox();
        pem_5a = new widget.TextBox();
        pem_5b = new widget.TextBox();
        pem_5c = new widget.TextBox();
        jLabel55 = new widget.Label();
        jLabel56 = new widget.Label();
        jLabel57 = new widget.Label();
        pre_5a = new widget.TextBox();
        jLabel58 = new widget.Label();
        jLabel59 = new widget.Label();
        has_6a = new widget.TextBox();
        lama18 = new widget.TextBox();
        has_7a = new widget.TextBox();
        lama20 = new widget.TextBox();
        lama21 = new widget.TextBox();
        jLabel60 = new widget.Label();
        pem_8a = new widget.TextBox();
        pem_8b = new widget.TextBox();
        pem_8c = new widget.TextBox();
        jLabel61 = new widget.Label();
        jLabel62 = new widget.Label();
        jLabel63 = new widget.Label();
        jLabel64 = new widget.Label();
        jLabel22 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        btnPetugas = new widget.Button();
        uji_4a = new widget.TextBox();
        uji_5a = new widget.TextBox();
        uji_5b = new widget.TextBox();
        uji_5c = new widget.TextBox();
        jLabel71 = new widget.Label();
        jLabel74 = new widget.Label();
        jLabel76 = new widget.Label();
        jLabel77 = new widget.Label();
        jLabel79 = new widget.Label();
        jLabel80 = new widget.Label();
        vep_5 = new widget.TextArea();
        vep_4 = new widget.TextArea();
        uji_6a = new widget.TextBox();
        jLabel81 = new widget.Label();
        jLabel82 = new widget.Label();

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnVerifikasiSpiro.setBackground(new java.awt.Color(255, 255, 254));
        MnVerifikasiSpiro.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnVerifikasiSpiro.setForeground(new java.awt.Color(50, 50, 50));
        MnVerifikasiSpiro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnVerifikasiSpiro.setText("Verifikasi Kesimpulan Spirometri");
        MnVerifikasiSpiro.setActionCommand("Verifikasi Hasil Spirometri");
        MnVerifikasiSpiro.setName("MnVerifikasiSpiro"); // NOI18N
        MnVerifikasiSpiro.setPreferredSize(new java.awt.Dimension(240, 26));
        MnVerifikasiSpiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnVerifikasiSpiroActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnVerifikasiSpiro);

        MnCetakSpiro.setBackground(new java.awt.Color(250, 250, 250));
        MnCetakSpiro.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakSpiro.setForeground(new java.awt.Color(50, 50, 50));
        MnCetakSpiro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetakSpiro.setLabel("Cetak Lembar Spiro");
        MnCetakSpiro.setName("MnCetakSpiro"); // NOI18N
        MnCetakSpiro.setPreferredSize(new java.awt.Dimension(200, 26));
        MnCetakSpiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakSpiroActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakSpiro);

        DlgVerifikasi.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        DlgVerifikasi.setName("DlgVerifikasi"); // NOI18N
        DlgVerifikasi.setUndecorated(true);

        internalFrame7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(230, 235, 225)), "::[Verifikasi Hasil Spirometri ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 70, 50))); // NOI18N
        internalFrame7.setMinimumSize(new java.awt.Dimension(10, 35));
        internalFrame7.setName("internalFrame7"); // NOI18N
        internalFrame7.setPreferredSize(new java.awt.Dimension(10, 35));
        internalFrame7.setLayout(new java.awt.BorderLayout(1, 1));

        panelBiasa6.setBackground(new java.awt.Color(255, 255, 255));
        panelBiasa6.setName("panelBiasa6"); // NOI18N
        panelBiasa6.setLayout(null);

        BtnSimpanVerifikasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpanVerifikasi.setMnemonic('T');
        BtnSimpanVerifikasi.setText("Simpan Kesimpulan");
        BtnSimpanVerifikasi.setToolTipText("Alt+T");
        BtnSimpanVerifikasi.setName("BtnSimpanVerifikasi"); // NOI18N
        BtnSimpanVerifikasi.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpanVerifikasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanVerifikasiActionPerformed(evt);
            }
        });
        panelBiasa6.add(BtnSimpanVerifikasi);
        BtnSimpanVerifikasi.setBounds(40, 240, 160, 30);

        BtnKeluarSP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluarSP.setMnemonic('K');
        BtnKeluarSP.setText("Tutup");
        BtnKeluarSP.setToolTipText("Alt+K");
        BtnKeluarSP.setName("BtnKeluarSP"); // NOI18N
        BtnKeluarSP.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluarSP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarSPActionPerformed(evt);
            }
        });
        panelBiasa6.add(BtnKeluarSP);
        BtnKeluarSP.setBounds(650, 240, 100, 30);

        NoRawat.setEditable(false);
        NoRawat.setName("NoRawat"); // NOI18N
        NoRawat.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NoRawat);
        NoRawat.setBounds(100, 10, 220, 23);

        jLabel72.setText("No Rawat :");
        jLabel72.setName("jLabel72"); // NOI18N
        jLabel72.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel72);
        jLabel72.setBounds(30, 10, 60, 23);

        jLabel73.setText("Nama Pasien :");
        jLabel73.setName("jLabel73"); // NOI18N
        jLabel73.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel73);
        jLabel73.setBounds(10, 40, 80, 23);

        NoRM.setEditable(false);
        NoRM.setName("NoRM"); // NOI18N
        NoRM.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NoRM);
        NoRM.setBounds(100, 40, 80, 23);

        NmPasien.setEditable(false);
        NmPasien.setName("NmPasien"); // NOI18N
        NmPasien.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NmPasien);
        NmPasien.setBounds(190, 40, 280, 23);

        jLabel78.setText("Dokter Pemeriksa  : ");
        jLabel78.setName("jLabel78"); // NOI18N
        jLabel78.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel78);
        jLabel78.setBounds(30, 190, 110, 23);

        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel42.setText("3. OBSTRUKSI");
        jLabel42.setName("jLabel42"); // NOI18N
        panelBiasa6.add(jLabel42);
        jLabel42.setBounds(50, 160, 70, 23);

        jLabel65.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel65.setText("C. KESIMPULAN / HASIL");
        jLabel65.setName("jLabel65"); // NOI18N
        panelBiasa6.add(jLabel65);
        jLabel65.setBounds(10, 80, 160, 23);

        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel66.setText(":");
        jLabel66.setName("jLabel66"); // NOI18N
        panelBiasa6.add(jLabel66);
        jLabel66.setBounds(130, 160, 10, 20);

        jLabel67.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel67.setText("2. RESTRIKSI");
        jLabel67.setName("jLabel67"); // NOI18N
        panelBiasa6.add(jLabel67);
        jLabel67.setBounds(50, 130, 70, 23);

        jLabel68.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel68.setText("Catatan Dokter   :");
        jLabel68.setName("jLabel68"); // NOI18N
        panelBiasa6.add(jLabel68);
        jLabel68.setBounds(250, 130, 90, 23);

        jLabel69.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel69.setText(":");
        jLabel69.setName("jLabel69"); // NOI18N
        panelBiasa6.add(jLabel69);
        jLabel69.setBounds(130, 100, 10, 20);

        cb_kesimpulan3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ringan", "Sedang", "Berat" }));
        cb_kesimpulan3.setName("cb_kesimpulan3"); // NOI18N
        cb_kesimpulan3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb_kesimpulan3KeyPressed(evt);
            }
        });
        panelBiasa6.add(cb_kesimpulan3);
        cb_kesimpulan3.setBounds(150, 160, 90, 23);

        cb_kesimpulan2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ringan", "Sedang", "Berat" }));
        cb_kesimpulan2.setName("cb_kesimpulan2"); // NOI18N
        cb_kesimpulan2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb_kesimpulan2KeyPressed(evt);
            }
        });
        panelBiasa6.add(cb_kesimpulan2);
        cb_kesimpulan2.setBounds(150, 130, 90, 23);

        cb_kesimpulan1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "NORMAL", "TIDAK NORMAL" }));
        cb_kesimpulan1.setName("cb_kesimpulan1"); // NOI18N
        cb_kesimpulan1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb_kesimpulan1KeyPressed(evt);
            }
        });
        panelBiasa6.add(cb_kesimpulan1);
        cb_kesimpulan1.setBounds(150, 100, 90, 23);

        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel70.setText(":");
        jLabel70.setName("jLabel70"); // NOI18N
        panelBiasa6.add(jLabel70);
        jLabel70.setBounds(130, 130, 10, 20);

        btnOperator1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnOperator1.setMnemonic('2');
        btnOperator1.setToolTipText("ALt+2");
        btnOperator1.setName("btnOperator1"); // NOI18N
        btnOperator1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOperator1ActionPerformed(evt);
            }
        });
        btnOperator1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnOperator1KeyPressed(evt);
            }
        });
        panelBiasa6.add(btnOperator1);
        btnOperator1.setBounds(700, 190, 28, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        panelBiasa6.add(NmDokter);
        NmDokter.setBounds(340, 190, 350, 23);

        KdDokter.setEditable(false);
        KdDokter.setHighlighter(null);
        KdDokter.setName("KdDokter"); // NOI18N
        panelBiasa6.add(KdDokter);
        KdDokter.setBounds(150, 190, 180, 23);

        scrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane3.setName("scrollPane3"); // NOI18N

        catatandokter.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        catatandokter.setColumns(20);
        catatandokter.setRows(5);
        catatandokter.setName("catatandokter"); // NOI18N
        catatandokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                catatandokterKeyPressed(evt);
            }
        });
        scrollPane3.setViewportView(catatandokter);

        panelBiasa6.add(scrollPane3);
        scrollPane3.setBounds(350, 110, 330, 60);

        jLabel75.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel75.setText("1. NORMAL");
        jLabel75.setName("jLabel75"); // NOI18N
        panelBiasa6.add(jLabel75);
        jLabel75.setBounds(50, 100, 70, 23);

        internalFrame7.add(panelBiasa6, java.awt.BorderLayout.CENTER);

        DlgVerifikasi.getContentPane().add(internalFrame7, java.awt.BorderLayout.CENTER);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Penilaian Spirometri ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbObatKeyReleased(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(44, 100));
        jPanel3.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        jPanel3.add(panelGlass8, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl. Surat :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(67, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "06-01-2025" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "06-01-2025" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(205, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(65, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(50, 23));
        panelGlass9.add(LCount);

        jPanel3.add(panelGlass9, java.awt.BorderLayout.PAGE_START);

        internalFrame1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        PanelInput.setName("PanelInput"); // NOI18N
        PanelInput.setOpaque(false);
        PanelInput.setPreferredSize(new java.awt.Dimension(192, 126));
        PanelInput.setLayout(new java.awt.BorderLayout(1, 1));

        ChkInput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setMnemonic('I');
        ChkInput.setText(".: Input Data");
        ChkInput.setToolTipText("Alt+I");
        ChkInput.setBorderPainted(true);
        ChkInput.setBorderPaintedFlat(true);
        ChkInput.setFocusable(false);
        ChkInput.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ChkInput.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ChkInput.setName("ChkInput"); // NOI18N
        ChkInput.setPreferredSize(new java.awt.Dimension(192, 20));
        ChkInput.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkInputActionPerformed(evt);
            }
        });
        PanelInput.add(ChkInput, java.awt.BorderLayout.PAGE_END);

        Scroll1.setName("Scroll1"); // NOI18N
        Scroll1.setOpaque(true);
        Scroll1.setPreferredSize(new java.awt.Dimension(452, 1100));

        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(100, 1099));
        FormInput.setLayout(null);

        jLabel4.setText("No.Rawat :");
        jLabel4.setName("jLabel4"); // NOI18N
        FormInput.add(jLabel4);
        jLabel4.setBounds(0, 10, 75, 23);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(79, 10, 150, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(335, 10, 230, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(232, 10, 100, 23);

        CbPengobatan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        CbPengobatan.setName("CbPengobatan"); // NOI18N
        CbPengobatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CbPengobatanKeyPressed(evt);
            }
        });
        FormInput.add(CbPengobatan);
        CbPengobatan.setBounds(140, 220, 60, 23);

        Tanggal.setForeground(new java.awt.Color(50, 70, 50));
        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "06-01-2025" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.setPreferredSize(new java.awt.Dimension(141, 18));
        Tanggal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TanggalActionPerformed(evt);
            }
        });
        Tanggal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TanggalKeyPressed(evt);
            }
        });
        FormInput.add(Tanggal);
        Tanggal.setBounds(90, 40, 90, 23);

        jLabel16.setText("Tanggal :");
        jLabel16.setName("jLabel16"); // NOI18N
        FormInput.add(jLabel16);
        jLabel16.setBounds(20, 40, 60, 23);

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel11.setText("Kenaikan Vep 1");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(590, 260, 90, 23);

        TB.setHighlighter(null);
        TB.setName("TB"); // NOI18N
        TB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TBActionPerformed(evt);
            }
        });
        TB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TBKeyPressed(evt);
            }
        });
        FormInput.add(TB);
        TB.setBounds(140, 100, 140, 23);

        BB.setHighlighter(null);
        BB.setName("BB"); // NOI18N
        BB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BBActionPerformed(evt);
            }
        });
        BB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BBKeyPressed(evt);
            }
        });
        FormInput.add(BB);
        BB.setBounds(140, 130, 140, 23);

        jLabel15.setText("TB :");
        jLabel15.setName("jLabel15"); // NOI18N
        FormInput.add(jLabel15);
        jLabel15.setBounds(60, 100, 70, 23);

        jLabel17.setText("Cm");
        jLabel17.setName("jLabel17"); // NOI18N
        FormInput.add(jLabel17);
        jLabel17.setBounds(280, 100, 20, 23);

        jLabel20.setText("BB :");
        jLabel20.setName("jLabel20"); // NOI18N
        FormInput.add(jLabel20);
        jLabel20.setBounds(60, 130, 70, 23);

        jLabel24.setText("Kg");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(280, 130, 20, 23);

        ChkJam1.setBorder(null);
        ChkJam1.setSelected(true);
        ChkJam1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam1.setName("ChkJam1"); // NOI18N
        FormInput.add(ChkJam1);
        ChkJam1.setBounds(360, 40, 23, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        FormInput.add(Detik);
        Detik.setBounds(310, 40, 50, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        FormInput.add(Menit);
        Menit.setBounds(250, 40, 50, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setName("Jam"); // NOI18N
        Jam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JamActionPerformed(evt);
            }
        });
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        FormInput.add(Jam);
        Jam.setBounds(190, 40, 50, 23);

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel18.setText(":");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(130, 220, 10, 20);

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel25.setText("ml");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(360, 320, 30, 23);

        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel26.setText("1. Pekerjaan");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(50, 160, 70, 23);

        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel27.setText(":");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(130, 160, 10, 23);

        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel29.setText(":");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(130, 190, 10, 23);

        pekerjaan.setHighlighter(null);
        pekerjaan.setName("pekerjaan"); // NOI18N
        pekerjaan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pekerjaanActionPerformed(evt);
            }
        });
        pekerjaan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pekerjaanKeyPressed(evt);
            }
        });
        FormInput.add(pekerjaan);
        pekerjaan.setBounds(140, 160, 340, 23);

        lama.setHighlighter(null);
        lama.setName("lama"); // NOI18N
        lama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lamaActionPerformed(evt);
            }
        });
        lama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lamaKeyPressed(evt);
            }
        });
        FormInput.add(lama);
        lama.setBounds(250, 190, 110, 23);

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel30.setText("2. Merokok");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(50, 190, 70, 23);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("Jumlah 20 btg / hari");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(370, 190, 110, 23);

        jumlah.setHighlighter(null);
        jumlah.setName("jumlah"); // NOI18N
        jumlah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jumlahActionPerformed(evt);
            }
        });
        jumlah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jumlahKeyPressed(evt);
            }
        });
        FormInput.add(jumlah);
        jumlah.setBounds(480, 190, 110, 23);

        CbMerokok.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        CbMerokok.setName("CbMerokok"); // NOI18N
        CbMerokok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CbMerokokKeyPressed(evt);
            }
        });
        FormInput.add(CbMerokok);
        CbMerokok.setBounds(140, 190, 60, 23);

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel32.setText("3. Pengobatan");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(50, 220, 80, 23);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel33.setText(" Detik 1 ( VEP 1)");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(60, 570, 110, 23);

        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel34.setText("Hasil");
        jLabel34.setName("jLabel34"); // NOI18N
        FormInput.add(jLabel34);
        jLabel34.setBounds(220, 260, 30, 23);

        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel35.setText("Prediksi");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(310, 260, 60, 23);

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel36.setText("Normal");
        jLabel36.setName("jLabel36"); // NOI18N
        FormInput.add(jLabel36);
        jLabel36.setBounds(400, 260, 60, 23);

        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel37.setText("Uji Bromkodilator");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(480, 260, 120, 23);

        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel38.setText("PEMERIKSAAN");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(50, 260, 80, 23);

        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel39.setText("1. KAPASITAS VITAL");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(50, 300, 100, 23);

        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel40.setText("2. % KV (KV / KV Prediksi )");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(50, 380, 130, 23);

        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel41.setText("3.Kapasitas Vital Paksa (KVP)");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(50, 420, 160, 23);

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel43.setText("4. % KV (KV / KV Prediksi)");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(50, 510, 140, 23);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel44.setText("6. % VEP 1 ( VEP 1 / Prediksi )");
        jLabel44.setName("jLabel44"); // NOI18N
        FormInput.add(jLabel44);
        jLabel44.setBounds(50, 640, 160, 23);

        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel45.setText("7.  VEP 1 ( VEP 1 / KVP )");
        jLabel45.setName("jLabel45"); // NOI18N
        FormInput.add(jLabel45);
        jLabel45.setBounds(50, 690, 160, 23);

        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel46.setText("8. Arus Puncak Ekspirasi (APE)");
        jLabel46.setName("jLabel46"); // NOI18N
        FormInput.add(jLabel46);
        jLabel46.setBounds(50, 730, 160, 23);

        pem_3c.setHighlighter(null);
        pem_3c.setName("pem_3c"); // NOI18N
        pem_3c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_3cActionPerformed(evt);
            }
        });
        pem_3c.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_3cKeyPressed(evt);
            }
        });
        FormInput.add(pem_3c);
        pem_3c.setBounds(200, 480, 60, 23);

        pem_1a.setHighlighter(null);
        pem_1a.setName("pem_1a"); // NOI18N
        pem_1a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_1aActionPerformed(evt);
            }
        });
        pem_1a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_1aKeyPressed(evt);
            }
        });
        FormInput.add(pem_1a);
        pem_1a.setBounds(200, 290, 60, 23);

        lama3.setEditable(false);
        lama3.setText("80 %");
        lama3.setHighlighter(null);
        lama3.setName("lama3"); // NOI18N
        lama3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lama3ActionPerformed(evt);
            }
        });
        lama3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lama3KeyPressed(evt);
            }
        });
        FormInput.add(lama3);
        lama3.setBounds(380, 380, 60, 23);

        pem_1b.setHighlighter(null);
        pem_1b.setName("pem_1b"); // NOI18N
        pem_1b.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_1bActionPerformed(evt);
            }
        });
        pem_1b.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_1bKeyPressed(evt);
            }
        });
        FormInput.add(pem_1b);
        pem_1b.setBounds(200, 320, 60, 23);

        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel47.setText("Lama :");
        jLabel47.setName("jLabel47"); // NOI18N
        FormInput.add(jLabel47);
        jLabel47.setBounds(210, 190, 50, 23);

        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel48.setText("MI");
        jLabel48.setName("jLabel48"); // NOI18N
        FormInput.add(jLabel48);
        jLabel48.setBounds(270, 290, 30, 23);

        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel49.setText("MI");
        jLabel49.setName("jLabel49"); // NOI18N
        FormInput.add(jLabel49);
        jLabel49.setBounds(270, 320, 30, 23);

        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel50.setText("MI");
        jLabel50.setName("jLabel50"); // NOI18N
        FormInput.add(jLabel50);
        jLabel50.setBounds(270, 350, 30, 23);

        pem_1c.setHighlighter(null);
        pem_1c.setName("pem_1c"); // NOI18N
        pem_1c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_1cActionPerformed(evt);
            }
        });
        pem_1c.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_1cKeyPressed(evt);
            }
        });
        FormInput.add(pem_1c);
        pem_1c.setBounds(200, 350, 60, 23);

        pre_1a.setHighlighter(null);
        pre_1a.setName("pre_1a"); // NOI18N
        pre_1a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pre_1aActionPerformed(evt);
            }
        });
        pre_1a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pre_1aKeyPressed(evt);
            }
        });
        FormInput.add(pre_1a);
        pre_1a.setBounds(290, 320, 60, 23);

        has_2a.setHighlighter(null);
        has_2a.setName("has_2a"); // NOI18N
        has_2a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                has_2aActionPerformed(evt);
            }
        });
        has_2a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                has_2aKeyPressed(evt);
            }
        });
        FormInput.add(has_2a);
        has_2a.setBounds(200, 380, 60, 23);

        pem_3a.setHighlighter(null);
        pem_3a.setName("pem_3a"); // NOI18N
        pem_3a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_3aActionPerformed(evt);
            }
        });
        pem_3a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_3aKeyPressed(evt);
            }
        });
        FormInput.add(pem_3a);
        pem_3a.setBounds(200, 420, 60, 23);

        pem_3b.setHighlighter(null);
        pem_3b.setName("pem_3b"); // NOI18N
        pem_3b.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_3bActionPerformed(evt);
            }
        });
        pem_3b.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_3bKeyPressed(evt);
            }
        });
        FormInput.add(pem_3b);
        pem_3b.setBounds(200, 450, 60, 23);

        pre_3a.setHighlighter(null);
        pre_3a.setName("pre_3a"); // NOI18N
        pre_3a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pre_3aActionPerformed(evt);
            }
        });
        pre_3a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pre_3aKeyPressed(evt);
            }
        });
        FormInput.add(pre_3a);
        pre_3a.setBounds(290, 450, 60, 23);

        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel51.setText("ml");
        jLabel51.setName("jLabel51"); // NOI18N
        FormInput.add(jLabel51);
        jLabel51.setBounds(360, 450, 30, 23);

        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel52.setText("MI");
        jLabel52.setName("jLabel52"); // NOI18N
        FormInput.add(jLabel52);
        jLabel52.setBounds(270, 480, 20, 23);

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel53.setText("MI");
        jLabel53.setName("jLabel53"); // NOI18N
        FormInput.add(jLabel53);
        jLabel53.setBounds(270, 450, 20, 23);

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("MI");
        jLabel54.setName("jLabel54"); // NOI18N
        FormInput.add(jLabel54);
        jLabel54.setBounds(270, 420, 20, 23);

        lama11.setEditable(false);
        lama11.setText("80 %");
        lama11.setHighlighter(null);
        lama11.setName("lama11"); // NOI18N
        lama11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lama11ActionPerformed(evt);
            }
        });
        lama11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lama11KeyPressed(evt);
            }
        });
        FormInput.add(lama11);
        lama11.setBounds(380, 510, 60, 23);

        has_4a.setHighlighter(null);
        has_4a.setName("has_4a"); // NOI18N
        has_4a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                has_4aActionPerformed(evt);
            }
        });
        has_4a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                has_4aKeyPressed(evt);
            }
        });
        FormInput.add(has_4a);
        has_4a.setBounds(200, 510, 60, 23);

        pem_5a.setHighlighter(null);
        pem_5a.setName("pem_5a"); // NOI18N
        pem_5a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_5aActionPerformed(evt);
            }
        });
        pem_5a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_5aKeyPressed(evt);
            }
        });
        FormInput.add(pem_5a);
        pem_5a.setBounds(200, 550, 60, 23);

        pem_5b.setHighlighter(null);
        pem_5b.setName("pem_5b"); // NOI18N
        pem_5b.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_5bActionPerformed(evt);
            }
        });
        pem_5b.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_5bKeyPressed(evt);
            }
        });
        FormInput.add(pem_5b);
        pem_5b.setBounds(200, 580, 60, 23);

        pem_5c.setHighlighter(null);
        pem_5c.setName("pem_5c"); // NOI18N
        pem_5c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_5cActionPerformed(evt);
            }
        });
        pem_5c.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_5cKeyPressed(evt);
            }
        });
        FormInput.add(pem_5c);
        pem_5c.setBounds(200, 610, 60, 23);

        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel55.setText("MI");
        jLabel55.setName("jLabel55"); // NOI18N
        FormInput.add(jLabel55);
        jLabel55.setBounds(270, 610, 20, 23);

        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel56.setText("MI");
        jLabel56.setName("jLabel56"); // NOI18N
        FormInput.add(jLabel56);
        jLabel56.setBounds(270, 580, 20, 23);

        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel57.setText("MI");
        jLabel57.setName("jLabel57"); // NOI18N
        FormInput.add(jLabel57);
        jLabel57.setBounds(270, 550, 20, 23);

        pre_5a.setHighlighter(null);
        pre_5a.setName("pre_5a"); // NOI18N
        pre_5a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pre_5aActionPerformed(evt);
            }
        });
        pre_5a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pre_5aKeyPressed(evt);
            }
        });
        FormInput.add(pre_5a);
        pre_5a.setBounds(290, 580, 60, 23);

        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel58.setText("ml");
        jLabel58.setName("jLabel58"); // NOI18N
        FormInput.add(jLabel58);
        jLabel58.setBounds(360, 690, 30, 23);

        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel59.setText("5. Volome Ekspirasi Paksa");
        jLabel59.setName("jLabel59"); // NOI18N
        FormInput.add(jLabel59);
        jLabel59.setBounds(50, 550, 210, 23);

        has_6a.setHighlighter(null);
        has_6a.setName("has_6a"); // NOI18N
        has_6a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                has_6aActionPerformed(evt);
            }
        });
        has_6a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                has_6aKeyPressed(evt);
            }
        });
        FormInput.add(has_6a);
        has_6a.setBounds(200, 640, 60, 23);

        lama18.setEditable(false);
        lama18.setText("80 %");
        lama18.setHighlighter(null);
        lama18.setName("lama18"); // NOI18N
        lama18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lama18ActionPerformed(evt);
            }
        });
        lama18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lama18KeyPressed(evt);
            }
        });
        FormInput.add(lama18);
        lama18.setBounds(380, 640, 60, 23);

        has_7a.setHighlighter(null);
        has_7a.setName("has_7a"); // NOI18N
        has_7a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                has_7aActionPerformed(evt);
            }
        });
        has_7a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                has_7aKeyPressed(evt);
            }
        });
        FormInput.add(has_7a);
        has_7a.setBounds(200, 690, 60, 23);

        lama20.setEditable(false);
        lama20.setText("%");
        lama20.setHighlighter(null);
        lama20.setName("lama20"); // NOI18N
        lama20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lama20ActionPerformed(evt);
            }
        });
        lama20.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lama20KeyPressed(evt);
            }
        });
        FormInput.add(lama20);
        lama20.setBounds(290, 690, 60, 23);

        lama21.setEditable(false);
        lama21.setText("75 %");
        lama21.setHighlighter(null);
        lama21.setName("lama21"); // NOI18N
        lama21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lama21ActionPerformed(evt);
            }
        });
        lama21.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lama21KeyPressed(evt);
            }
        });
        FormInput.add(lama21);
        lama21.setBounds(380, 690, 60, 23);

        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel60.setText("ml");
        jLabel60.setName("jLabel60"); // NOI18N
        FormInput.add(jLabel60);
        jLabel60.setBounds(360, 580, 30, 23);

        pem_8a.setHighlighter(null);
        pem_8a.setName("pem_8a"); // NOI18N
        pem_8a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_8aActionPerformed(evt);
            }
        });
        pem_8a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_8aKeyPressed(evt);
            }
        });
        FormInput.add(pem_8a);
        pem_8a.setBounds(200, 730, 60, 23);

        pem_8b.setHighlighter(null);
        pem_8b.setName("pem_8b"); // NOI18N
        pem_8b.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_8bActionPerformed(evt);
            }
        });
        pem_8b.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_8bKeyPressed(evt);
            }
        });
        FormInput.add(pem_8b);
        pem_8b.setBounds(200, 760, 60, 23);

        pem_8c.setHighlighter(null);
        pem_8c.setName("pem_8c"); // NOI18N
        pem_8c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pem_8cActionPerformed(evt);
            }
        });
        pem_8c.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pem_8cKeyPressed(evt);
            }
        });
        FormInput.add(pem_8c);
        pem_8c.setBounds(200, 790, 60, 23);

        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel61.setText("MI");
        jLabel61.setName("jLabel61"); // NOI18N
        FormInput.add(jLabel61);
        jLabel61.setBounds(270, 730, 20, 23);

        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel62.setText("MI");
        jLabel62.setName("jLabel62"); // NOI18N
        FormInput.add(jLabel62);
        jLabel62.setBounds(270, 760, 20, 23);

        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel63.setText("MI");
        jLabel63.setName("jLabel63"); // NOI18N
        FormInput.add(jLabel63);
        jLabel63.setBounds(270, 790, 20, 23);

        jLabel64.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel64.setText("9. Air Trapping");
        jLabel64.setName("jLabel64"); // NOI18N
        FormInput.add(jLabel64);
        jLabel64.setBounds(50, 820, 160, 23);

        jLabel22.setText("Petugas :");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(30, 860, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setHighlighter(null);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(110, 860, 94, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(210, 860, 180, 23);

        btnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnPetugas.setMnemonic('2');
        btnPetugas.setToolTipText("ALt+2");
        btnPetugas.setName("btnPetugas"); // NOI18N
        btnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPetugasActionPerformed(evt);
            }
        });
        btnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPetugasKeyPressed(evt);
            }
        });
        FormInput.add(btnPetugas);
        btnPetugas.setBounds(400, 860, 28, 23);

        uji_4a.setHighlighter(null);
        uji_4a.setName("uji_4a"); // NOI18N
        uji_4a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uji_4aActionPerformed(evt);
            }
        });
        uji_4a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                uji_4aKeyPressed(evt);
            }
        });
        FormInput.add(uji_4a);
        uji_4a.setBounds(490, 510, 60, 23);

        uji_5a.setHighlighter(null);
        uji_5a.setName("uji_5a"); // NOI18N
        uji_5a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uji_5aActionPerformed(evt);
            }
        });
        uji_5a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                uji_5aKeyPressed(evt);
            }
        });
        FormInput.add(uji_5a);
        uji_5a.setBounds(490, 550, 60, 23);

        uji_5b.setHighlighter(null);
        uji_5b.setName("uji_5b"); // NOI18N
        uji_5b.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uji_5bActionPerformed(evt);
            }
        });
        uji_5b.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                uji_5bKeyPressed(evt);
            }
        });
        FormInput.add(uji_5b);
        uji_5b.setBounds(490, 580, 60, 23);

        uji_5c.setHighlighter(null);
        uji_5c.setName("uji_5c"); // NOI18N
        uji_5c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uji_5cActionPerformed(evt);
            }
        });
        uji_5c.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                uji_5cKeyPressed(evt);
            }
        });
        FormInput.add(uji_5c);
        uji_5c.setBounds(490, 610, 60, 23);

        jLabel71.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel71.setText("%");
        jLabel71.setName("jLabel71"); // NOI18N
        FormInput.add(jLabel71);
        jLabel71.setBounds(560, 640, 20, 23);

        jLabel74.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel74.setText("MI");
        jLabel74.setName("jLabel74"); // NOI18N
        FormInput.add(jLabel74);
        jLabel74.setBounds(560, 580, 20, 23);

        jLabel76.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel76.setText("MI");
        jLabel76.setName("jLabel76"); // NOI18N
        FormInput.add(jLabel76);
        jLabel76.setBounds(560, 610, 20, 23);

        jLabel77.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel77.setText("3.");
        jLabel77.setName("jLabel77"); // NOI18N
        FormInput.add(jLabel77);
        jLabel77.setBounds(470, 610, 20, 23);

        jLabel79.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel79.setText("1.");
        jLabel79.setName("jLabel79"); // NOI18N
        FormInput.add(jLabel79);
        jLabel79.setBounds(470, 550, 20, 23);

        jLabel80.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel80.setText("2.");
        jLabel80.setName("jLabel80"); // NOI18N
        FormInput.add(jLabel80);
        jLabel80.setBounds(470, 580, 20, 23);

        vep_5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        vep_5.setColumns(20);
        vep_5.setRows(5);
        vep_5.setName("vep_5"); // NOI18N
        vep_5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                vep_5KeyPressed(evt);
            }
        });
        FormInput.add(vep_5);
        vep_5.setBounds(590, 560, 80, 240);

        vep_4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        vep_4.setColumns(20);
        vep_4.setRows(5);
        vep_4.setName("vep_4"); // NOI18N
        vep_4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                vep_4KeyPressed(evt);
            }
        });
        FormInput.add(vep_4);
        vep_4.setBounds(590, 420, 80, 110);

        uji_6a.setHighlighter(null);
        uji_6a.setName("uji_6a"); // NOI18N
        uji_6a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uji_6aActionPerformed(evt);
            }
        });
        uji_6a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                uji_6aKeyPressed(evt);
            }
        });
        FormInput.add(uji_6a);
        uji_6a.setBounds(490, 640, 60, 23);

        jLabel81.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel81.setText("MI");
        jLabel81.setName("jLabel81"); // NOI18N
        FormInput.add(jLabel81);
        jLabel81.setBounds(560, 550, 20, 23);

        jLabel82.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel82.setText("%");
        jLabel82.setName("jLabel82"); // NOI18N
        FormInput.add(jLabel82);
        jLabel82.setBounds(560, 510, 20, 23);

        Scroll1.setViewportView(FormInput);

        PanelInput.add(Scroll1, java.awt.BorderLayout.CENTER);

        internalFrame1.add(PanelInput, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);
        internalFrame1.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
            isPsien();
        }else{            
            Valid.pindah(evt,TCari,TNoRw);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(BB.getText().trim().equals("")){
            Valid.textKosong(BB,"Berat Badan");
        }else if(TB.getText().trim().equals("")){
            Valid.textKosong(TB,"Tinggi Badan");
        }else if(TB.getText().trim().equals("")){
            Valid.textKosong(TB,"Pekerjaan");
        }else{
            if(Sequel.menyimpantf("penilaian_spiro","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",42,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(Tanggal.getSelectedItem()+""),
                Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(), 
                KdPetugas.getText(),
                "-",
                TB.getText(),
                BB.getText(),
                pekerjaan.getText(),
                CbMerokok.getSelectedItem()+"",
                lama.getText(),
                jumlah.getText(),
                CbPengobatan.getSelectedItem()+"",
                pem_1a.getText(),
                pem_1b.getText(),
                pem_1c.getText(),
                pre_1a.getText(),
                has_2a.getText(),
                pem_3a.getText(),
                pem_3b.getText(),
                pem_3c.getText(),
                pre_3a.getText(),
                has_4a.getText(),
                pem_5a.getText(),
                pem_5b.getText(),
                pem_5c.getText(),
                pre_5a.getText(),
                has_6a.getText(),
                has_7a.getText(),
                pem_8a.getText(),
                pem_8b.getText(),
                pem_8c.getText(),
                "",
                "",
                "",
                uji_4a.getText(),
                uji_5a.getText(),
                uji_5b.getText(),
                uji_5c.getText(),
                uji_6a.getText(),
                vep_4.getText(),
                vep_5.getText(),
                "",
                })==true){
                tampil();
                emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,CbPengobatan,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
        ChkInput.setSelected(true);
        isForm(); 
        
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        Valid.hapusTable(tabMode,TNoRw,"penilaian_spiro","no_rawat");
        tampil();
        emptTeks();
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");    
        }else if(BB.getText().trim().equals("")){
            Valid.textKosong(BB,"Berat Badan");
        }else if(TB.getText().trim().equals("")){
            Valid.textKosong(TB,"Tinggi Badan");
        }else{    
            if(tbObat.getSelectedRow()!= -1){
                if(Sequel.mengedittf("penilaian_spiro","no_rawat=?","no_rawat=?,tanggal=?,jam=?,kd_petugas=?,kd_dokter=?,tb=?,bb=?,pekerjaan=?,merokok=?,lama_merokok=?,jumlah_merokok=?,pengobatan=?,pemeriksaan_1a=?,pemeriksaan_1b=?,pemeriksaan_1c=?,prediksi_1a=?,hasil_2a=?,pemeriksaan_3a=?,pemeriksaan_3b=?,pemeriksaan_3c=?,prediksi_3a=?,hasil_4a=?,pemeriksaan_5a=?,pemeriksaan_5b=?,pemeriksaan_5c=?,prediksi_5a=?,hasil_6a=?,hasil_7a=?,pemeriksaan_8a=?,pemeriksaan_8b=?,pemeriksaan_8c=?,kesimpulan_hasil_a=?,kesimpulan_hasil_b=?,kesimpulan_hasil_c=?,uji_4a=?,uji_5a=?,uji_5b=?,uji_5c=?,uji_6a=?,vep_4=?,vep_5=?",42,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(Tanggal.getSelectedItem()+""),
                Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(), 
                KdPetugas.getText(),
                "-",
                TB.getText(),
                BB.getText(),
                pekerjaan.getText(),
                CbMerokok.getSelectedItem()+"",
                lama.getText(),
                jumlah.getText(),
                CbPengobatan.getSelectedItem()+"",
                pem_1a.getText(),
                pem_1b.getText(),
                pem_1c.getText(),
                pre_1a.getText(),
                has_2a.getText(),
                pem_3a.getText(),
                pem_3b.getText(),
                pem_3c.getText(),
                pre_3a.getText(),
                has_4a.getText(),
                pem_5a.getText(),
                pem_5b.getText(),
                pem_5c.getText(),
                pre_5a.getText(),
                has_6a.getText(),
                has_7a.getText(),
                pem_8a.getText(),
                pem_8b.getText(),
                pem_8c.getText(),
                cb_kesimpulan1.getSelectedItem().toString(),
                cb_kesimpulan2.getSelectedItem().toString(),
                cb_kesimpulan3.getSelectedItem().toString(),    
                uji_4a.getText(),
                uji_5a.getText(),
                uji_5b.getText(),
                uji_5c.getText(),
                uji_6a.getText(),
                vep_4.getText(),
                vep_5.getText(),
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
                })==true){
                    tampil();
                    emptTeks();
                }
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            dispose();
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
          
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            tampil();
            TCari.setText("");
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed
   
                                  
    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void ChkInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkInputActionPerformed
       isForm();
    }//GEN-LAST:event_ChkInputActionPerformed

    private void tbObatKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyReleased
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
    }//GEN-LAST:event_tbObatKeyReleased

    private void MnCetakSpiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakSpiroActionPerformed
       if(TPasien.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Maaf, Silahkan anda pilih dulu pasien...!!!");
        }else{
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                Map<String, Object> param = new HashMap<>();
                param.put("namars",akses.getnamars());
                param.put("alamatrs",akses.getalamatrs());
                param.put("kotars",akses.getkabupatenrs());
                param.put("propinsirs",akses.getpropinsirs());
                param.put("kontakrs",akses.getkontakrs());
                param.put("emailrs",akses.getemailrs());  
                param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
                param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan")); 
                param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan")); 
                //START CUSTOM
                if(Sequel.cariIsi("select status_lanjut from reg_periksa where no_rawat=?", TNoRw.getText()).equals("Ranap")){
                    // CUSTOM AMBIL DOKTER DPJP & TANGGAL UNTUK BARCODE TTD SURAT
                    kodedokter=Sequel.cariIsi("select dpjp_ranap.kd_dokter from dpjp_ranap where dpjp_ranap.no_rawat=? limit 1",TNoRw.getText());
                    namadokter=Sequel.cariIsi("select dokter.nm_dokter from dokter where dokter.kd_dokter=?",kodedokter);
            
                    param.put("nm_dokter",namadokter);
                    finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",kodedokter);
                    param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+namadokter+"\nID "+(finger.equals("")?kodedokter:finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString()));                     
                }else{
                    // CUSTOM AMBIL DOKTER dari registrasi
                    kodedokter=Sequel.cariIsi("select reg_periksa.kd_dokter from reg_periksa where reg_periksa.no_rawat=? ",TNoRw.getText());
                    namadokter=Sequel.cariIsi("select dokter.nm_dokter from dokter where dokter.kd_dokter=?",kodedokter);
                   
                    param.put("nm_dokter",namadokter);
                    finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",kodedokter);
                    param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+namadokter+"\nID "+(finger.equals("")?kodedokter:finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString())); 
                }
                              
                Valid.MyReportqry("rptCetakPenilaianSpiro.jasper","report","::[ Penilaian Spirometri ]::","select penilaian_spiro.*,reg_periksa.no_rkm_medis,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.tgl_lahir,if(pasien.jk='L','LAKI-LAKI','PEREMPUAN') as jk," +
                "pasien.nm_pasien,pasien.tmp_lahir,dokter.kd_dokter,dokter.nm_dokter,"+ 
                "pasien.alamat,kelurahan.nm_kel,kecamatan.nm_kec,kabupaten.nm_kab,petugas.nip,petugas.nama,"+ 
                "CONCAT(DATE_FORMAT(pasien.tgl_lahir, '%d '),"+ 
                "CASE MONTH(pasien.tgl_lahir) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, CONCAT(' ', YEAR(pasien.tgl_lahir))) AS ttl,dokter.no_ijn_praktek as sip " +
                "from penilaian_spiro inner join reg_periksa on penilaian_spiro.no_rawat=reg_periksa.no_rawat " +
                "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis " +
                "INNER JOIN kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                "INNER JOIN kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                "INNER JOIN kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                "INNER JOIN dokter ON penilaian_spiro.kd_dokter = dokter.kd_dokter " +
                "INNER JOIN petugas ON penilaian_spiro.kd_petugas = petugas.nip " +
                "where penilaian_spiro.no_rawat ='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by penilaian_spiro.no_rawat",param);
                this.setCursor(Cursor.getDefaultCursor());  
       }
    }//GEN-LAST:event_MnCetakSpiroActionPerformed

    private void TanggalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TanggalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TanggalActionPerformed

    private void TanggalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TanggalKeyPressed
        Valid.pindah(evt,TCari,TNoRw);
    }//GEN-LAST:event_TanggalKeyPressed

    private void CbPengobatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CbPengobatanKeyPressed
        Valid.pindah(evt,TNoRw,BtnSimpan);
    }//GEN-LAST:event_CbPengobatanKeyPressed

    private void TBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TBActionPerformed

    private void TBKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TBKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TBKeyPressed

    private void BBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BBActionPerformed

    private void BBKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BBKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BBKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DetikKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MenitKeyPressed

    private void JamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JamActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamActionPerformed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamKeyPressed

    private void pekerjaanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pekerjaanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pekerjaanActionPerformed

    private void pekerjaanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pekerjaanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pekerjaanKeyPressed

    private void lamaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lamaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lamaActionPerformed

    private void lamaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lamaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_lamaKeyPressed

    private void jumlahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jumlahActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jumlahActionPerformed

    private void jumlahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jumlahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jumlahKeyPressed

    private void CbMerokokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CbMerokokKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CbMerokokKeyPressed

    private void pem_3cActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_3cActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_3cActionPerformed

    private void pem_3cKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_3cKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_3cKeyPressed

    private void pem_1aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_1aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_1aActionPerformed

    private void pem_1aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_1aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_1aKeyPressed

    private void lama3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lama3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama3ActionPerformed

    private void lama3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lama3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama3KeyPressed

    private void pem_1bActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_1bActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_1bActionPerformed

    private void pem_1bKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_1bKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_1bKeyPressed

    private void pem_1cActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_1cActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_1cActionPerformed

    private void pem_1cKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_1cKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_1cKeyPressed

    private void pre_1aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pre_1aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pre_1aActionPerformed

    private void pre_1aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pre_1aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pre_1aKeyPressed

    private void has_2aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_has_2aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_has_2aActionPerformed

    private void has_2aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_has_2aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_has_2aKeyPressed

    private void pem_3aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_3aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_3aActionPerformed

    private void pem_3aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_3aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_3aKeyPressed

    private void pem_3bActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_3bActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_3bActionPerformed

    private void pem_3bKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_3bKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_3bKeyPressed

    private void pre_3aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pre_3aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pre_3aActionPerformed

    private void pre_3aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pre_3aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pre_3aKeyPressed

    private void lama11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lama11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama11ActionPerformed

    private void lama11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lama11KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama11KeyPressed

    private void has_4aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_has_4aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_has_4aActionPerformed

    private void has_4aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_has_4aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_has_4aKeyPressed

    private void pem_5aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_5aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_5aActionPerformed

    private void pem_5aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_5aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_5aKeyPressed

    private void pem_5bActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_5bActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_5bActionPerformed

    private void pem_5bKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_5bKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_5bKeyPressed

    private void pem_5cActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_5cActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_5cActionPerformed

    private void pem_5cKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_5cKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_5cKeyPressed

    private void pre_5aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pre_5aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pre_5aActionPerformed

    private void pre_5aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pre_5aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pre_5aKeyPressed

    private void has_6aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_has_6aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_has_6aActionPerformed

    private void has_6aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_has_6aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_has_6aKeyPressed

    private void lama18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lama18ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama18ActionPerformed

    private void lama18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lama18KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama18KeyPressed

    private void has_7aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_has_7aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_has_7aActionPerformed

    private void has_7aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_has_7aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_has_7aKeyPressed

    private void lama20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lama20ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama20ActionPerformed

    private void lama20KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lama20KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama20KeyPressed

    private void lama21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lama21ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama21ActionPerformed

    private void lama21KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lama21KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_lama21KeyPressed

    private void pem_8aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_8aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_8aActionPerformed

    private void pem_8aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_8aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_8aKeyPressed

    private void pem_8bActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_8bActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_8bActionPerformed

    private void pem_8bKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_8bKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_8bKeyPressed

    private void pem_8cActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pem_8cActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_8cActionPerformed

    private void pem_8cKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pem_8cKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pem_8cKeyPressed

    private void cb_kesimpulan3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb_kesimpulan3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_kesimpulan3KeyPressed

    private void cb_kesimpulan2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb_kesimpulan2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_kesimpulan2KeyPressed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            NmPetugas.setText(petugas.tampil3(KdPetugas.getText()));
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            Detik.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
            btnPetugasActionPerformed(null);
        }
    }//GEN-LAST:event_KdPetugasKeyPressed

    private void btnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPetugasActionPerformed
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_btnPetugasActionPerformed

    private void btnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPetugasKeyPressed

    }//GEN-LAST:event_btnPetugasKeyPressed

    private void cb_kesimpulan1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb_kesimpulan1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_kesimpulan1KeyPressed

    private void BtnSimpanVerifikasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanVerifikasiActionPerformed
        // START CUSTOM ANGGIT
        if(NoRawat.getText().trim().equals("")){
            Valid.textKosong(NoRawat,"Nomor Rawat");
        }else if(KdDokter.getText().trim().equals("")) {
            Valid.textKosong(KdDokter,"Dokter");
        }else{
            if(akses.getkode().equals("Admin Utama")){
                if(Sequel.queryu2tf("update penilaian_spiro set kesimpulan_hasil_a=?, kesimpulan_hasil_b=?, kesimpulan_hasil_c=?,kd_dokter=?,catatandokter=? where no_rawat=?",6,new String[]{
                    cb_kesimpulan1.getSelectedItem().toString(),
                    cb_kesimpulan2.getSelectedItem().toString(),
                    cb_kesimpulan3.getSelectedItem().toString(),
                    KdDokter.getText(),
                    catatandokter.getText(),
                    NoRawat.getText(),
                })==true){
                    JOptionPane.showMessageDialog(null,"Hasil Kesimpulan Spirometri "+ NmPasien.getText() +" berhasil di verifikasi");
                    DlgVerifikasi.dispose();
                    tampil();
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Verifikasi gagal disimpan");
                }

            }else{
          
                if(Sequel.queryu2tf("update penilaian_spiro set kesimpulan_hasil_a=?, kesimpulan_hasil_b=?, kesimpulan_hasil_c=?,kd_dokter=?,catatandokter=? where no_rawat=?",6,new String[]{
                    cb_kesimpulan1.getSelectedItem().toString(),
                    cb_kesimpulan2.getSelectedItem().toString(),
                    cb_kesimpulan3.getSelectedItem().toString(),
                    KdDokter.getText(),
                    catatandokter.getText(),
                    NoRawat.getText(),
                    })==true){
                        JOptionPane.showMessageDialog(null,"Hasil Kesimpulan Spirometri "+ NmPasien.getText() +" berhasil di verifikasi");
                        DlgVerifikasi.dispose();
                        tampil();
                    }else{
                        JOptionPane.showMessageDialog(null,"Maaf, Verifikasi gagal disimpan");
                    }
                
            }

        }
        // END CUSTOM ANGGIT
    }//GEN-LAST:event_BtnSimpanVerifikasiActionPerformed

    private void BtnKeluarSPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarSPActionPerformed
        // TODO add your handling code here:
        DlgVerifikasi.dispose();
    }//GEN-LAST:event_BtnKeluarSPActionPerformed

    private void uji_4aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uji_4aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_4aActionPerformed

    private void uji_4aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_uji_4aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_4aKeyPressed

    private void uji_5aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uji_5aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_5aActionPerformed

    private void uji_5aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_uji_5aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_5aKeyPressed

    private void uji_5bActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uji_5bActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_5bActionPerformed

    private void uji_5bKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_uji_5bKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_5bKeyPressed

    private void uji_5cActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uji_5cActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_5cActionPerformed

    private void uji_5cKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_uji_5cKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_5cKeyPressed

    private void vep_5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_vep_5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_vep_5KeyPressed

    private void vep_4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_vep_4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_vep_4KeyPressed

    private void uji_6aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uji_6aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_6aActionPerformed

    private void uji_6aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_uji_6aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_uji_6aKeyPressed

    private void MnVerifikasiSpiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnVerifikasiSpiroActionPerformed
        // TODO add your handling code here:
        if(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString().trim().equals("")){
            Valid.textKosong(TCari,"Nomor Rawat");
        }else{
            DlgVerifikasi.setSize(800,570);
            DlgVerifikasi.setLocationRelativeTo(internalFrame1);
            DlgVerifikasi.setVisible(true);
            NoRawat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            NoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            NmPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            catatandokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),45).toString());
            Sequel.cariIsi("select kd_dokter from dokter where kd_dokter=?",KdDokter,akses.getkode());
            Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?",NmDokter,akses.getkode());
       
        }
    }//GEN-LAST:event_MnVerifikasiSpiroActionPerformed

    private void btnOperator1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnOperator1KeyPressed

    }//GEN-LAST:event_btnOperator1KeyPressed

    private void btnOperator1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOperator1ActionPerformed
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_btnOperator1ActionPerformed

    private void catatandokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_catatandokterKeyPressed
       
    }//GEN-LAST:event_catatandokterKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMPenilaianSpiro dialog = new RMPenilaianSpiro(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
      
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.TextBox BB;
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnKeluarSP;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Button BtnSimpanVerifikasi;
    private widget.ComboBox CbMerokok;
    private widget.ComboBox CbPengobatan;
    private widget.CekBox ChkInput;
    private widget.CekBox ChkJam1;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox Detik;
    private javax.swing.JDialog DlgVerifikasi;
    private widget.PanelBiasa FormInput;
    private widget.ComboBox Jam;
    private widget.TextBox KdDokter;
    private widget.TextBox KdPetugas;
    private widget.Label LCount;
    private widget.ComboBox Menit;
    private javax.swing.JMenuItem MnCetakSpiro;
    private javax.swing.JMenuItem MnVerifikasiSpiro;
    private widget.TextBox NmDokter;
    private widget.TextBox NmPasien;
    private widget.TextBox NmPetugas;
    private widget.TextBox NoRM;
    private widget.TextBox NoRawat;
    private javax.swing.JPanel PanelInput;
    private widget.ScrollPane Scroll;
    private widget.ScrollPane Scroll1;
    private widget.TextBox TB;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.Tanggal Tanggal;
    private widget.Button btnOperator1;
    private widget.Button btnPetugas;
    private widget.TextArea catatandokter;
    private widget.ComboBox cb_kesimpulan1;
    private widget.ComboBox cb_kesimpulan2;
    private widget.ComboBox cb_kesimpulan3;
    private widget.TextBox has_2a;
    private widget.TextBox has_4a;
    private widget.TextBox has_6a;
    private widget.TextBox has_7a;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame7;
    private widget.Label jLabel11;
    private widget.Label jLabel15;
    private widget.Label jLabel16;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel29;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel4;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel48;
    private widget.Label jLabel49;
    private widget.Label jLabel50;
    private widget.Label jLabel51;
    private widget.Label jLabel52;
    private widget.Label jLabel53;
    private widget.Label jLabel54;
    private widget.Label jLabel55;
    private widget.Label jLabel56;
    private widget.Label jLabel57;
    private widget.Label jLabel58;
    private widget.Label jLabel59;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel61;
    private widget.Label jLabel62;
    private widget.Label jLabel63;
    private widget.Label jLabel64;
    private widget.Label jLabel65;
    private widget.Label jLabel66;
    private widget.Label jLabel67;
    private widget.Label jLabel68;
    private widget.Label jLabel69;
    private widget.Label jLabel7;
    private widget.Label jLabel70;
    private widget.Label jLabel71;
    private widget.Label jLabel72;
    private widget.Label jLabel73;
    private widget.Label jLabel74;
    private widget.Label jLabel75;
    private widget.Label jLabel76;
    private widget.Label jLabel77;
    private widget.Label jLabel78;
    private widget.Label jLabel79;
    private widget.Label jLabel80;
    private widget.Label jLabel81;
    private widget.Label jLabel82;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private widget.TextBox jumlah;
    private widget.TextBox lama;
    private widget.TextBox lama11;
    private widget.TextBox lama18;
    private widget.TextBox lama20;
    private widget.TextBox lama21;
    private widget.TextBox lama3;
    private widget.PanelBiasa panelBiasa6;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.TextBox pekerjaan;
    private widget.TextBox pem_1a;
    private widget.TextBox pem_1b;
    private widget.TextBox pem_1c;
    private widget.TextBox pem_3a;
    private widget.TextBox pem_3b;
    private widget.TextBox pem_3c;
    private widget.TextBox pem_5a;
    private widget.TextBox pem_5b;
    private widget.TextBox pem_5c;
    private widget.TextBox pem_8a;
    private widget.TextBox pem_8b;
    private widget.TextBox pem_8c;
    private widget.TextBox pre_1a;
    private widget.TextBox pre_3a;
    private widget.TextBox pre_5a;
    private widget.ScrollPane scrollPane3;
    private widget.Table tbObat;
    private widget.TextBox uji_4a;
    private widget.TextBox uji_5a;
    private widget.TextBox uji_5b;
    private widget.TextBox uji_5c;
    private widget.TextBox uji_6a;
    private widget.TextArea vep_4;
    private widget.TextArea vep_5;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().equals("")){
                ps=koneksi.prepareStatement(
                     "select penilaian_spiro.*,reg_periksa.no_rkm_medis,pasien.tgl_lahir,if(pasien.jk='L','LAKI-LAKI','PEREMPUAN') as jk,pasien.nm_pasien,pasien.tmp_lahir,dokter.kd_dokter,dokter.nm_dokter,petugas.nip,petugas.nama,CONCAT( pasien.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) as alamatpasien,CONCAT(DATE_FORMAT(pasien.tgl_lahir, '%d '),CASE MONTH(pasien.tgl_lahir) WHEN 1 THEN 'JANUARI' WHEN 2 THEN 'FEBRUARI' WHEN 3 THEN 'MARET' WHEN 4 THEN 'APRIL' WHEN 5 THEN 'MEI' WHEN 6 THEN 'JUNI' WHEN 7 THEN 'JULI' WHEN 8 THEN 'AGUSTUS' WHEN 9 THEN 'SEPTEMBER' WHEN 10 THEN 'OKTOBER' WHEN 11 THEN 'NOVEMBER' WHEN 12 THEN 'DESEMBER' END, CONCAT(' ', YEAR(pasien.tgl_lahir))) AS ttl "+
                     "from penilaian_spiro inner join reg_periksa on penilaian_spiro.no_rawat=reg_periksa.no_rawat "+
                     "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                     "INNER JOIN kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                     "INNER JOIN kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                     "INNER JOIN kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                     "INNER JOIN dokter ON penilaian_spiro.kd_dokter = dokter.kd_dokter " +
                     "INNER JOIN petugas ON petugas.nip = penilaian_spiro.kd_petugas " +
                     "where penilaian_spiro.tanggal between ? and ? order by penilaian_spiro.tanggal desc,penilaian_spiro.jam desc");
            }else{
                ps=koneksi.prepareStatement(
                     "select penilaian_spiro.*,reg_periksa.no_rkm_medis,pasien.tgl_lahir,if(pasien.jk='L','LAKI-LAKI','PEREMPUAN') as jk,pasien.nm_pasien,pasien.tmp_lahir,dokter.kd_dokter,dokter.nm_dokter,petugas.nip,petugas.nama,CONCAT( pasien.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) as alamatpasien,CONCAT(DATE_FORMAT(pasien.tgl_lahir, '%d '),CASE MONTH(pasien.tgl_lahir) WHEN 1 THEN 'JANUARI' WHEN 2 THEN 'FEBRUARI' WHEN 3 THEN 'MARET' WHEN 4 THEN 'APRIL' WHEN 5 THEN 'MEI' WHEN 6 THEN 'JUNI' WHEN 7 THEN 'JULI' WHEN 8 THEN 'AGUSTUS' WHEN 9 THEN 'SEPTEMBER' WHEN 10 THEN 'OKTOBER' WHEN 11 THEN 'NOVEMBER' WHEN 12 THEN 'DESEMBER' END, CONCAT(' ', YEAR(pasien.tgl_lahir))) AS ttl "+
                     "from penilaian_spiro inner join reg_periksa on penilaian_spiro.no_rawat=reg_periksa.no_rawat "+
                     "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                     "INNER JOIN kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                     "INNER JOIN kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                     "INNER JOIN kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                     "INNER JOIN dokter ON penilaian_spiro.kd_dokter = dokter.kd_dokter " +
                     "INNER JOIN petugas ON petugas.nip = penilaian_spiro.kd_petugas " +
                     "where pasien.no_rkm_medis =? order by penilaian_spiro.tanggal desc,penilaian_spiro.jam desc limit 1 ");
            
            }
                if(TCari.getText().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                }else{
                    ps.setString(1,TNoRM.getText());
                }  
                
                
     
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("kd_dokter"),
                        rs.getString("nm_dokter"),
                        rs.getString("nip"),
                        rs.getString("nama"),
                        rs.getString("tb"),
                        rs.getString("bb"),
                        rs.getString("pekerjaan"),
                        rs.getString("merokok"),
                        rs.getString("lama_merokok"),
                        rs.getString("jumlah_merokok"),
                        rs.getString("pengobatan"),
                        rs.getString("pemeriksaan_1a"),
                        rs.getString("pemeriksaan_1b"),
                        rs.getString("pemeriksaan_1c"),
                        rs.getString("prediksi_1a"),
                        rs.getString("hasil_2a"),
                        rs.getString("pemeriksaan_3a"),
                        rs.getString("pemeriksaan_3b"),
                        rs.getString("pemeriksaan_3c"),
                        rs.getString("prediksi_3a"),
                        rs.getString("hasil_4a"),
                        rs.getString("pemeriksaan_5a"),
                        rs.getString("pemeriksaan_5b"),
                        rs.getString("pemeriksaan_5c"),
                        rs.getString("prediksi_5a"),
                        rs.getString("hasil_6a"),
                        rs.getString("hasil_7a"),
                        rs.getString("pemeriksaan_8a"),
                        rs.getString("pemeriksaan_8b"),
                        rs.getString("pemeriksaan_8c"),
                        rs.getString("kesimpulan_hasil_a"),
                        rs.getString("kesimpulan_hasil_b"),
                        rs.getString("kesimpulan_hasil_c"),
                        rs.getString("uji_4a"),
                        rs.getString("uji_5a"),
                        rs.getString("uji_5b"),
                        rs.getString("uji_5c"),
                        rs.getString("uji_6a"),
                        rs.getString("vep_4"),
                        rs.getString("vep_5"),
                        rs.getString("catatandokter"),
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        ChkJam1.setSelected(true);
        BB.setText("");
        TB.setText("");
   
        Tanggal.setDate(new Date());
        CbPengobatan.setSelectedIndex(0);
        KdPetugas.setText("");
        NmPetugas.setText("");
        TB.setText("");
        BB.setText("");
        pekerjaan.setText("");
        CbMerokok.setSelectedIndex(0);
        lama.setText("");
        jumlah.setText("");
        pem_1a.setText("");
        pem_1b.setText("");
        pem_1c.setText("");
        pre_1a.setText("");
        has_2a.setText("");
        pem_3a.setText("");
        pem_3b.setText("");
        pem_3c.setText("");
        pre_3a.setText("");
        has_4a.setText("");
        pem_5a.setText("");
        pem_5b.setText("");
        pem_5c.setText("");
        pre_5a.setText("");
        has_6a.setText("");
        has_7a.setText("");
        pem_8a.setText("");
        pem_8b.setText("");
        pem_8c.setText("");
        cb_kesimpulan1.setSelectedIndex(0);
        cb_kesimpulan2.setSelectedIndex(0);
        cb_kesimpulan3.setSelectedIndex(0);
        catatandokter.setText("");
        uji_4a.setText("");
        uji_5a.setText("");
        uji_5b.setText("");
        uji_5c.setText("");
        uji_6a.setText("");
        vep_4.setText("");
        vep_5.setText("");
    }

 
    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            ChkJam1.setSelected(false);
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            Valid.SetTgl(Tanggal,tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Jam.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString().substring(0,2));
            Menit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString().substring(3,5));
            Detik.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString().substring(6,8));
           
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            TB.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            BB.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            pekerjaan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            CbMerokok.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            lama.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            jumlah.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            CbPengobatan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            pem_1a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            pem_1b.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            pem_1c.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            pre_1a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            has_2a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            pem_3a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            pem_3b.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            pem_3c.setText(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            pre_3a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            has_4a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            pem_5a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            pem_5b.setText(tbObat.getValueAt(tbObat.getSelectedRow(),27).toString());
            pem_5c.setText(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            pre_5a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            has_6a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
            has_7a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            pem_8a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());
            pem_8b.setText(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString());
            pem_8c.setText(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());
            cb_kesimpulan1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),35).toString());
            cb_kesimpulan2.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),36).toString());
            cb_kesimpulan3.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString());
            uji_4a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),38).toString());
            uji_5a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),39).toString());
            uji_5b.setText(tbObat.getValueAt(tbObat.getSelectedRow(),40).toString());
            uji_5c.setText(tbObat.getValueAt(tbObat.getSelectedRow(),41).toString());
            uji_6a.setText(tbObat.getValueAt(tbObat.getSelectedRow(),42).toString());
            vep_4.setText(tbObat.getValueAt(tbObat.getSelectedRow(),43).toString());
            vep_5.setText(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString());
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            catatandokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),45).toString());
            
        }
    }

    private void isRawat() {
        Sequel.cariIsi("select no_rkm_medis from reg_periksa where no_rawat='"+TNoRw.getText()+"' ",TNoRM);
        Sequel.cariIsi("select tb from penilaian_awal_keperawatan_ralan where no_rawat='"+TNoRw.getText()+"' ",TB);
        Sequel.cariIsi("select bb from penilaian_awal_keperawatan_ralan where no_rawat='"+TNoRw.getText()+"' ",BB);   
    }

    private void isPsien() {
        
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','LAKI-LAKI','PEREMPUAN') as jk,reg_periksa.umurdaftar,reg_periksa.sttsumur,reg_periksa.tgl_registrasi,"+
                    "reg_periksa.jam_reg,CASE reg_periksa.status_lanjut WHEN 'Ralan' THEN poliklinik.nm_poli WHEN 'Ranap' THEN CONCAT( kamar_inap.kd_kamar, ' ',bangsal.nm_bangsal ) END AS ruangan,CONCAT( pasien.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) as alamat,CONCAT(pasien.tmp_lahir, ', ',DATE_FORMAT(pasien.tgl_lahir, '%d '),CASE MONTH(pasien.tgl_lahir) WHEN 1 THEN 'JANUARI' WHEN 2 THEN 'FEBRUARI' WHEN 3 THEN 'MARET' WHEN 4 THEN 'APRIL' WHEN 5 THEN 'MEI' WHEN 6 THEN 'JUNI' WHEN 7 THEN 'JULI' WHEN 8 THEN 'AGUSTUS' WHEN 9 THEN 'SEPTEMBER' WHEN 10 THEN 'OKTOBER' WHEN 11 THEN 'NOVEMBER' WHEN 12 THEN 'DESEMBER' END, CONCAT(' ', YEAR(pasien.tgl_lahir))) AS ttl "+
                    "from reg_periksa "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli " +
                    "INNER JOIN kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                    "INNER JOIN kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                    "INNER JOIN kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                    "LEFT JOIN kamar_inap ON kamar_inap.no_rawat = reg_periksa.no_rawat " +
                    "LEFT JOIN kamar ON kamar.kd_kamar = kamar_inap.kd_kamar " +
                    "LEFT JOIN bangsal ON kamar.kd_bangsal = bangsal.kd_bangsal where reg_periksa.no_rawat=? ");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
        
        Sequel.cariIsi("select nm_pasien from pasien where no_rkm_medis='"+TNoRM.getText()+"' ",TPasien);
    }
    
    public void setNoRm(String norwt) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        isRawat();
        isPsien(); 
        isForm();
        tampil();
      
        Sequel.cariIsi("select nama from petugas where nip=?",NmPetugas,akses.getkode());
        Sequel.cariIsi("select nip from petugas where nip=?",KdPetugas,akses.getkode());
    }
    private void isForm(){
        if(ChkInput.isSelected()==true){
            ChkInput.setVisible(false);
            PanelInput.setPreferredSize(new Dimension(WIDTH,365));
            FormInput.setVisible(true);      
            ChkInput.setVisible(true);
        }else if(ChkInput.isSelected()==false){           
            ChkInput.setVisible(false);            
            PanelInput.setPreferredSize(new Dimension(WIDTH,10));
            FormInput.setVisible(false);      
            ChkInput.setVisible(true);
        }
    }
    // CUSTOM ANGGIT
         private void jam(){
         ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam1.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkJam1.isSelected()==false){
                    nilai_jam =Jam.getSelectedIndex();
                    nilai_menit =Menit.getSelectedIndex();
                    nilai_detik =Detik.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam.setSelectedItem(jam);
                Menit.setSelectedItem(menit);
                Detik.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    //
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.gettindakan_ralan());
        BtnHapus.setEnabled(akses.gettindakan_ralan());
        BtnEdit.setEnabled(akses.gettindakan_ralan());
    }
}



