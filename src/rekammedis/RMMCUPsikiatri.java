/*
 * Kontribusi dari Anggit Nurhidayah, RSUP Surakarta
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;

/**
 *
 * @author perpustakaan
 */
public final class RMMCUPsikiatri extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private StringBuilder htmlContent;
    private String finger="";
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMMCUPsikiatri(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tgl.Lahir","J.K.","NIP Dokter","Nama Dokter","Tanggal",
            "Penampilan Umum","Mood","Afek","Pembicaraan Spontan","Pembicaraan Pelan","Pembicaraan Jelas","Pembicaraan Banyak","Pembicaraan Meloncat","Pembicaraan Lambat",
            "Persepsi Halusinasi","Proses Pikir","Pengendalian Impuls","Fungsi Kognitif","Kemampuan Realitas","Kesimpulan","Kode Petugas","Nama Petugas"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        
        tbObat.setModel(tabMode);
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 25; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(150);
            }else if(i==3){
                column.setPreferredWidth(65);
            }else if(i==4){
                column.setPreferredWidth(55);
            }else if(i==5){
                column.setPreferredWidth(80);
            }else if(i==6){
                column.setPreferredWidth(150);
            }else if(i==7){
                column.setPreferredWidth(115);
            }else if(i==8 || i==9 || i==10 ){
                column.setPreferredWidth(100);
            }else if(i==11){
                column.setPreferredWidth(160);
            }else if(i==12){
                column.setPreferredWidth(80);
            }else if(i==13){
                column.setPreferredWidth(150);
            }else if(i==14){
                column.setPreferredWidth(150);
            }else if(i==15){
                column.setPreferredWidth(150);
            }else if(i==16){
                column.setPreferredWidth(150);
            }else if(i==17){
                column.setPreferredWidth(150);
            }else if(i==18){
                column.setPreferredWidth(150);
            }else if(i==19){
                column.setPreferredWidth(150);
            }else if(i==20){
                column.setPreferredWidth(150);
            }else if(i==21){
                column.setPreferredWidth(150);
            }else if(i==22){
                column.setPreferredWidth(250);
            }else if(i==23){
                column.setPreferredWidth(150);
            }else if(i==24){
                column.setPreferredWidth(150);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){
                    KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                    KdDokter.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });

        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                    KdPetugas.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnCetakMCUPsikiatri = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdDokter = new widget.TextBox();
        NmDokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        jLabel11 = new widget.Label();
        jLabel94 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        label11 = new widget.Label();
        TglPemeriksaan = new widget.Tanggal();
        label15 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnPetugas = new widget.Button();
        cb3f = new widget.ComboBox();
        jLabel96 = new widget.Label();
        cb1 = new widget.ComboBox();
        cb2a = new widget.ComboBox();
        jLabel97 = new widget.Label();
        jLabel98 = new widget.Label();
        jLabel100 = new widget.Label();
        cb4 = new widget.ComboBox();
        cb3a = new widget.ComboBox();
        cb3b = new widget.ComboBox();
        cb3c = new widget.ComboBox();
        cb3d = new widget.ComboBox();
        cb3e = new widget.ComboBox();
        jLabel101 = new widget.Label();
        cb2b = new widget.ComboBox();
        jLabel102 = new widget.Label();
        cb5 = new widget.ComboBox();
        jLabel103 = new widget.Label();
        cb6 = new widget.ComboBox();
        jLabel104 = new widget.Label();
        cb7 = new widget.ComboBox();
        jLabel105 = new widget.Label();
        cb8 = new widget.ComboBox();
        jScrollPane5 = new javax.swing.JScrollPane();
        kesimpulan = new javax.swing.JTextArea();
        jLabel106 = new widget.Label();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnCetakMCUPsikiatri.setBackground(new java.awt.Color(255, 255, 254));
        MnCetakMCUPsikiatri.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakMCUPsikiatri.setForeground(new java.awt.Color(50, 50, 50));
        MnCetakMCUPsikiatri.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetakMCUPsikiatri.setText("Surat Keterangan Kesehatan Mata");
        MnCetakMCUPsikiatri.setActionCommand("Surat MCU Psikiatri");
        MnCetakMCUPsikiatri.setName("MnCetakMCUPsikiatri"); // NOI18N
        MnCetakMCUPsikiatri.setPreferredSize(new java.awt.Dimension(220, 26));
        MnCetakMCUPsikiatri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakMCUPsikiatriActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakMCUPsikiatri);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Medical Checkup Psikiatri ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(1020, 700));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 70, 70, 23);

        KdDokter.setEditable(false);
        KdDokter.setName("KdDokter"); // NOI18N
        KdDokter.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokterKeyPressed(evt);
            }
        });
        FormInput.add(KdDokter);
        KdDokter.setBounds(74, 40, 90, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        NmDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmDokter);
        NmDokter.setBounds(166, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(348, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jLabel94.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel94.setText("b) Afek (luas, terbatas, tumpul, mendatar) ");
        jLabel94.setName("jLabel94"); // NOI18N
        FormInput.add(jLabel94);
        jLabel94.setBounds(20, 210, 330, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 100, 880, 1);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(380, 40, 52, 23);

        TglPemeriksaan.setForeground(new java.awt.Color(50, 70, 50));
        TglPemeriksaan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "25-07-2024 13:48:59" }));
        TglPemeriksaan.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglPemeriksaan.setName("TglPemeriksaan"); // NOI18N
        TglPemeriksaan.setOpaque(false);
        TglPemeriksaan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglPemeriksaanKeyPressed(evt);
            }
        });
        FormInput.add(TglPemeriksaan);
        TglPemeriksaan.setBounds(436, 40, 130, 23);

        label15.setText("Dokter :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label15);
        label15.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(74, 70, 90, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(166, 70, 180, 23);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas.setMnemonic('2');
        BtnPetugas.setToolTipText("Alt+2");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        FormInput.add(BtnPetugas);
        BtnPetugas.setBounds(348, 70, 28, 23);

        cb3f.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Lambat", "Cepat" }));
        cb3f.setName("cb3f"); // NOI18N
        cb3f.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb3fKeyPressed(evt);
            }
        });
        FormInput.add(cb3f);
        cb3f.setBounds(300, 310, 120, 23);

        jLabel96.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel96.setText("1. Penampilan umum ditunjukan melalui sikap, perilaku, dan psikomotor ");
        jLabel96.setName("jLabel96"); // NOI18N
        FormInput.add(jLabel96);
        jLabel96.setBounds(10, 120, 350, 23);

        cb1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Kooperatif", "Normatif" }));
        cb1.setName("cb1"); // NOI18N
        cb1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb1KeyPressed(evt);
            }
        });
        FormInput.add(cb1);
        cb1.setBounds(370, 120, 150, 23);

        cb2a.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Normal", "Sedih", "Senang Berlebihan", "Labil", "Iritabel", "Lainnya" }));
        cb2a.setName("cb2a"); // NOI18N
        cb2a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb2aKeyPressed(evt);
            }
        });
        FormInput.add(cb2a);
        cb2a.setBounds(370, 180, 150, 23);

        jLabel97.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel97.setText("4. Persepsi : halusinasi visual/auditorik (penglihatan/pendengaran) ");
        jLabel97.setName("jLabel97"); // NOI18N
        FormInput.add(jLabel97);
        jLabel97.setBounds(10, 350, 340, 30);

        jLabel98.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel98.setText("a) Mood (eutim/normal, sedih, senang berlebihan, labil, iritabel, dll) ");
        jLabel98.setName("jLabel98"); // NOI18N
        FormInput.add(jLabel98);
        jLabel98.setBounds(20, 180, 330, 23);

        jLabel100.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel100.setText("2. Mood / afek (suasana perasaan / ekspresi wajah) ");
        jLabel100.setName("jLabel100"); // NOI18N
        FormInput.add(jLabel100);
        jLabel100.setBounds(10, 150, 260, 23);

        cb4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ada Gangguan Persepsi", "Tidak Ada Gangguan Persepsi" }));
        cb4.setName("cb4"); // NOI18N
        cb4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb4KeyPressed(evt);
            }
        });
        FormInput.add(cb4);
        cb4.setBounds(370, 350, 240, 23);

        cb3a.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Spontan", "Tidak Spontan" }));
        cb3a.setName("cb3a"); // NOI18N
        cb3a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb3aKeyPressed(evt);
            }
        });
        FormInput.add(cb3a);
        cb3a.setBounds(20, 270, 120, 23);

        cb3b.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pelan", "Keras" }));
        cb3b.setName("cb3b"); // NOI18N
        cb3b.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb3bKeyPressed(evt);
            }
        });
        FormInput.add(cb3b);
        cb3b.setBounds(160, 270, 120, 23);

        cb3c.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Jelas", "Tidak Jelas" }));
        cb3c.setName("cb3c"); // NOI18N
        cb3c.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb3cKeyPressed(evt);
            }
        });
        FormInput.add(cb3c);
        cb3c.setBounds(300, 270, 120, 23);

        cb3d.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Banyak", "Sedikit" }));
        cb3d.setName("cb3d"); // NOI18N
        cb3d.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb3dKeyPressed(evt);
            }
        });
        FormInput.add(cb3d);
        cb3d.setBounds(20, 310, 120, 23);

        cb3e.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Meloncat-loncat", "Tidak" }));
        cb3e.setName("cb3e"); // NOI18N
        cb3e.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb3eKeyPressed(evt);
            }
        });
        FormInput.add(cb3e);
        cb3e.setBounds(160, 310, 120, 23);

        jLabel101.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel101.setText("3. Pembicaraan: spontan/tidak, pelan/keras, jelas/tidak, banyak/sedikit, meloncat-loncat/tidak, lambat/cepat dan sebagainya.");
        jLabel101.setName("jLabel101"); // NOI18N
        FormInput.add(jLabel101);
        jLabel101.setBounds(10, 240, 610, 30);

        cb2b.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Afek Luas", "Afek Terbatas", "Afek Tumpul", "Afek Mendatar" }));
        cb2b.setName("cb2b"); // NOI18N
        cb2b.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb2bKeyPressed(evt);
            }
        });
        FormInput.add(cb2b);
        cb2b.setBounds(370, 210, 150, 23);

        jLabel102.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel102.setText("5. Proses dan isi pikir : waham, ide meloncat-loncat dan sebagainya");
        jLabel102.setName("jLabel102"); // NOI18N
        FormInput.add(jLabel102);
        jLabel102.setBounds(10, 380, 340, 30);

        cb5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ada Gangguan Proses dan Isi Pikir", "Tidak Ada Gangguan Proses dan Isi Pikir" }));
        cb5.setName("cb5"); // NOI18N
        cb5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb5KeyPressed(evt);
            }
        });
        FormInput.add(cb5);
        cb5.setBounds(370, 380, 240, 23);

        jLabel103.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel103.setText("6. Pengendalian impuls : verbal / motorik ");
        jLabel103.setName("jLabel103"); // NOI18N
        FormInput.add(jLabel103);
        jLabel103.setBounds(10, 410, 340, 30);

        cb6.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Baik", "Tidak Baik" }));
        cb6.setName("cb6"); // NOI18N
        cb6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb6KeyPressed(evt);
            }
        });
        FormInput.add(cb6);
        cb6.setBounds(370, 410, 240, 23);

        jLabel104.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel104.setText("7. Fungsi kognitif : kesadaran, memori, konsentrasi, visuospatial");
        jLabel104.setName("jLabel104"); // NOI18N
        FormInput.add(jLabel104);
        jLabel104.setBounds(10, 440, 340, 30);

        cb7.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Kesadaran Jernih Konsentrasi Baik", "Kesadaran Tidak Jernih Konsentrasi Tidak Baik" }));
        cb7.setName("cb7"); // NOI18N
        cb7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb7KeyPressed(evt);
            }
        });
        FormInput.add(cb7);
        cb7.setBounds(370, 440, 240, 23);

        jLabel105.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel105.setText("Kesimpulan :");
        jLabel105.setName("jLabel105"); // NOI18N
        FormInput.add(jLabel105);
        jLabel105.setBounds(20, 510, 70, 30);

        cb8.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Terganggu", "Terganggu" }));
        cb8.setName("cb8"); // NOI18N
        cb8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb8KeyPressed(evt);
            }
        });
        FormInput.add(cb8);
        cb8.setBounds(370, 470, 240, 23);

        jScrollPane5.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane5.setName("jScrollPane5"); // NOI18N

        kesimpulan.setColumns(20);
        kesimpulan.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        kesimpulan.setLineWrap(true);
        kesimpulan.setRows(5);
        kesimpulan.setName("kesimpulan"); // NOI18N
        jScrollPane5.setViewportView(kesimpulan);

        FormInput.add(jScrollPane5);
        jScrollPane5.setBounds(90, 520, 520, 100);

        jLabel106.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel106.setText("8. Kemampuan dalam menilai realitas (Reality Testing Ability) :");
        jLabel106.setName("jLabel106"); // NOI18N
        FormInput.add(jLabel106);
        jLabel106.setBounds(10, 470, 350, 30);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Pemeriksaan", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "25-07-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "25-07-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Pemeriksaan", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{            
            Valid.pindah(evt,TCari,BtnDokter);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnPetugas,"Petugas");
        }else if(kesimpulan.getText().trim().equals("")){
            Valid.textKosong(kesimpulan,"Kesimpulan");
        }else{
            if(Sequel.menyimpantf("penilaian_mcu_psikiatri","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",19,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),
                KdPetugas.getText(),
                cb1.getSelectedItem().toString(),
                cb2a.getSelectedItem().toString(),
                cb2b.getSelectedItem().toString(),
                cb3a.getSelectedItem().toString(),
                cb3b.getSelectedItem().toString(),
                cb3c.getSelectedItem().toString(),
                cb3d.getSelectedItem().toString(),
                cb3e.getSelectedItem().toString(),
                cb3f.getSelectedItem().toString(),
                cb4.getSelectedItem().toString(),
                cb5.getSelectedItem().toString(),
                cb6.getSelectedItem().toString(),
                cb7.getSelectedItem().toString(),
                cb8.getSelectedItem().toString(),
                kesimpulan.getText(),
                })==true){
                    JOptionPane.showMessageDialog(rootPane,"Data berhasil disimpan!");
                    emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,cb1,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()) || akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh dokter/petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }  
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRw.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(KdDokter.getText().trim().equals("")){
            Valid.textKosong(KdDokter,"Dokter");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnPetugas,"Petugas");
        }else if(kesimpulan.getText().trim().equals("")){
            Valid.textKosong(kesimpulan,"Kesimpulan");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()) || akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString())){
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh dokter/petugas yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
//        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        if(tabMode.getRowCount()==0){
//            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//            BtnBatal.requestFocus();
//        }else if(tabMode.getRowCount()!=0){
//            try{
//                if(TCari.getText().trim().equals("")){
//                    ps=koneksi.prepareStatement(
//                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_medis_ralan.tanggal,"+
//                        "penilaian_medis_ralan.kd_dokter,penilaian_medis_ralan.anamnesis,penilaian_medis_ralan.hubungan,penilaian_medis_ralan.keluhan_utama,penilaian_medis_ralan.rps,penilaian_medis_ralan.rpk,penilaian_medis_ralan.rpd,penilaian_medis_ralan.rpo,penilaian_medis_ralan.alergi,"+
//                        "penilaian_medis_ralan.keadaan,penilaian_medis_ralan.gcs,penilaian_medis_ralan.kesadaran,penilaian_medis_ralan.td,penilaian_medis_ralan.nadi,penilaian_medis_ralan.rr,penilaian_medis_ralan.suhu,penilaian_medis_ralan.spo,penilaian_medis_ralan.bb,penilaian_medis_ralan.tb,"+
//                        "penilaian_medis_ralan.kepala,penilaian_medis_ralan.gigi,penilaian_medis_ralan.tht,penilaian_medis_ralan.thoraks,penilaian_medis_ralan.abdomen,penilaian_medis_ralan.ekstremitas,penilaian_medis_ralan.genital,penilaian_medis_ralan.kulit,"+
//                        "penilaian_medis_ralan.ket_fisik,penilaian_medis_ralan.ket_lokalis,penilaian_medis_ralan.penunjang,penilaian_medis_ralan.diagnosis,penilaian_medis_ralan.tata,penilaian_medis_ralan.konsulrujuk,dokter.nm_dokter "+
//                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                        "inner join penilaian_medis_ralan on reg_periksa.no_rawat=penilaian_medis_ralan.no_rawat "+
//                        "inner join dokter on penilaian_medis_ralan.kd_dokter=dokter.kd_dokter where "+
//                        "penilaian_medis_ralan.tanggal between ? and ? order by penilaian_medis_ralan.tanggal");
//                }else{
//                    ps=koneksi.prepareStatement(
//                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_medis_ralan.tanggal,"+
//                        "penilaian_medis_ralan.kd_dokter,penilaian_medis_ralan.anamnesis,penilaian_medis_ralan.hubungan,penilaian_medis_ralan.keluhan_utama,penilaian_medis_ralan.rps,penilaian_medis_ralan.rpk,penilaian_medis_ralan.rpd,penilaian_medis_ralan.rpo,penilaian_medis_ralan.alergi,"+
//                        "penilaian_medis_ralan.keadaan,penilaian_medis_ralan.gcs,penilaian_medis_ralan.kesadaran,penilaian_medis_ralan.td,penilaian_medis_ralan.nadi,penilaian_medis_ralan.rr,penilaian_medis_ralan.suhu,penilaian_medis_ralan.spo,penilaian_medis_ralan.bb,penilaian_medis_ralan.tb,"+
//                        "penilaian_medis_ralan.kepala,penilaian_medis_ralan.gigi,penilaian_medis_ralan.tht,penilaian_medis_ralan.thoraks,penilaian_medis_ralan.abdomen,penilaian_medis_ralan.ekstremitas,penilaian_medis_ralan.genital,penilaian_medis_ralan.kulit,"+
//                        "penilaian_medis_ralan.ket_fisik,penilaian_medis_ralan.ket_lokalis,penilaian_medis_ralan.penunjang,penilaian_medis_ralan.diagnosis,penilaian_medis_ralan.tata,penilaian_medis_ralan.konsulrujuk,dokter.nm_dokter "+
//                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                        "inner join penilaian_medis_ralan on reg_periksa.no_rawat=penilaian_medis_ralan.no_rawat "+
//                        "inner join dokter on penilaian_medis_ralan.kd_dokter=dokter.kd_dokter where "+
//                        "penilaian_medis_ralan.tanggal between ? and ? and (reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
//                        "penilaian_medis_ralan.kd_dokter like ? or dokter.nm_dokter like ?) order by penilaian_medis_ralan.tanggal");
//                }
//
//                try {
//                    if(TCari.getText().trim().equals("")){
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                    }else{
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(3,"%"+TCari.getText()+"%");
//                        ps.setString(4,"%"+TCari.getText()+"%");
//                        ps.setString(5,"%"+TCari.getText()+"%");
//                        ps.setString(6,"%"+TCari.getText()+"%");
//                        ps.setString(7,"%"+TCari.getText()+"%");
//                    }  
//                    rs=ps.executeQuery();
//                    htmlContent = new StringBuilder();
//                    htmlContent.append(                             
//                        "<tr class='isi'>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='105px'><b>No.Rawat</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='70px'><b>No.RM</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Nama Pasien</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='65px'><b>Tgl.Lahir</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='55px'><b>J.K.</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>NIP</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Nama Dokter</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='115px'><b>Tanggal</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Anamnesis</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='100px'><b>Hubungan</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Keluhan Utama</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Sekarang</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Dahulu</b></td>"+
//			    "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Keluarga</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penggunaan Obat</b></td>"+   //CUSTOM MUHSIN PENGGUNAKAN JADI PENGGUNAAN
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='120px'><b>Riwayat Alergi</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='90px'><b>Keadaan Umum</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='50px'><b>GCS</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kesadaran</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='60px'><b>TD(mmHg)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='75px'><b>Nadi(x/menit)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='67px'><b>RR(x/menit)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>Suhu</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>SpO2</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>BB(Kg)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>TB(cm)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kepala</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Gigi & Mulut</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>THT</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Thoraks</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Abdomen</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Genital & Anus</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Ekstremitas</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kulit</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Ket.Pemeriksaan Fisik</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='200px'><b>Ket.Status Lokalis</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='170px'><b>Pemeriksaa Penunjang</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Diagnosis/Asesmen</b></td>"+
//			    "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Tatalaksana</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Konsul/Rujuk</b></td>"+
//                        "</tr>"
//                    );
//                    while(rs.next()){
//                        htmlContent.append(
//                            "<tr class='isi'>"+
//                               "<td valign='top'>"+rs.getString("no_rawat")+"</td>"+
//                               "<td valign='top'>"+rs.getString("no_rkm_medis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nm_pasien")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tgl_lahir")+"</td>"+
//                               "<td valign='top'>"+rs.getString("jk")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kd_dokter")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nm_dokter")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tanggal")+"</td>"+
//                               "<td valign='top'>"+rs.getString("anamnesis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("hubungan")+"</td>"+
//                               "<td valign='top'>"+rs.getString("keluhan_utama")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rps")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpd")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpk")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpo")+"</td>"+
//                               "<td valign='top'>"+rs.getString("alergi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("keadaan")+"</td>"+
//                               "<td valign='top'>"+rs.getString("gcs")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kesadaran")+"</td>"+
//                               "<td valign='top'>"+rs.getString("td")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nadi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rr")+"</td>"+
//                               "<td valign='top'>"+rs.getString("suhu")+"</td>"+
//                               "<td valign='top'>"+rs.getString("spo")+"</td>"+
//                               "<td valign='top'>"+rs.getString("bb")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tb")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kepala")+"</td>"+
//                               "<td valign='top'>"+rs.getString("gigi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tht")+"</td>"+
//                               "<td valign='top'>"+rs.getString("thoraks")+"</td>"+
//                               "<td valign='top'>"+rs.getString("abdomen")+"</td>"+
//                               "<td valign='top'>"+rs.getString("genital")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ekstremitas")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kulit")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ket_fisik")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ket_lokalis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("penunjang")+"</td>"+
//                               "<td valign='top'>"+rs.getString("diagnosis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tata")+"</td>"+
//                               "<td valign='top'>"+rs.getString("konsulrujuk")+"</td>"+
//                            "</tr>");
//                    }
//                    LoadHTML.setText(
//                        "<html>"+
//                          "<table width='4400px' border='0' align='center' cellpadding='1px' cellspacing='0' class='tbl_form'>"+
//                           htmlContent.toString()+
//                          "</table>"+
//                        "</html>"
//                    );
//
//                    File g = new File("file2.css");            
//                    BufferedWriter bg = new BufferedWriter(new FileWriter(g));
//                    bg.write(
//                        ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
//                        ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
//                        ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
//                        ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
//                        ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
//                        ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
//                    );
//                    bg.close();
//
//                    File f = new File("DataPenilaianAwalMedisRalan.html");            
//                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));            
//                    bw.write(LoadHTML.getText().replaceAll("<head>","<head>"+
//                                "<link href=\"file2.css\" rel=\"stylesheet\" type=\"text/css\" />"+
//                                "<table width='4400px' border='0' align='center' cellpadding='3px' cellspacing='0' class='tbl_form'>"+
//                                    "<tr class='isi2'>"+
//                                        "<td valign='top' align='center'>"+
//                                            "<font size='4' face='Tahoma'>"+akses.getnamars()+"</font><br>"+
//                                            akses.getalamatrs()+", "+akses.getkabupatenrs()+", "+akses.getpropinsirs()+"<br>"+
//                                            akses.getkontakrs()+", E-mail : "+akses.getemailrs()+"<br><br>"+
//                                            "<font size='2' face='Tahoma'>DATA PENILAIAN AWAL MEDIS RAWAT JALAN<br><br></font>"+        
//                                        "</td>"+
//                                   "</tr>"+
//                                "</table>")
//                    );
//                    bw.close();                         
//                    Desktop.getDesktop().browse(f.toURI());
//                } catch (Exception e) {
//                    System.out.println("Notif : "+e);
//                } finally{
//                    if(rs!=null){
//                        rs.close();
//                    }
//                    if(ps!=null){
//                        ps.close();
//                    }
//                }
//
//            }catch(Exception e){
//                System.out.println("Notifikasi : "+e);
//            }
//        }
//        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void KdDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokterKeyPressed
        
    }//GEN-LAST:event_KdDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setAlwaysOnTop(false);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        //Valid.pindah(evt,Monitoring,BtnSimpan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void TglPemeriksaanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglPemeriksaanKeyPressed
        Valid.pindah(evt,BtnDokter,BtnPetugas);
    }//GEN-LAST:event_TglPemeriksaanKeyPressed

    private void MnCetakMCUPsikiatriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakMCUPsikiatriActionPerformed
      if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),5).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString()));
            Valid.MyReportqry("rptCetakMCUPsikiatri.jasper","report","::[ Formulir Insiden Keselamatan Pasien ]::",
                "select pms.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk,CONCAT( p.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) AS alamat, " +
                "p.tgl_lahir,pms.tanggal,pms.penampilan_umum,pms.mood,pms.afek,pms.pembicaraan_spontan,pms.pembicaraan_pelan,pms.pembicaraan_jelas,pms.pembicaraan_banyak, " +
                "pms.pembicaraan_meloncat,pms.pembicaraan_lambat,pms.persepsi_halusinasi,pms.proses_pikir,pms.pengendalian_impuls,pms.fungsi_kognitif,pms.kemampuan_realitas, "+
                "pms.kd_dokter,d.nm_dokter, pms.kd_petugas, p2.nama,pms.kesimpulan "+
                "from reg_periksa " +
                "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                "inner join penilaian_mcu_psikiatri pms on reg_periksa.no_rawat = pms.no_rawat " +
                "inner join dokter d on pms.kd_dokter = d.kd_dokter " +
                "inner join petugas p2 on p2.nip = pms.kd_petugas " +
                "INNER JOIN kelurahan ON kelurahan.kd_kel = p.kd_kel " +
                "INNER JOIN kecamatan ON kecamatan.kd_kec = p.kd_kec " +
                "INNER JOIN kabupaten ON kabupaten.kd_kab = p.kd_kab " +
                "where pms.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' ",param);
        }    
    }//GEN-LAST:event_MnCetakMCUPsikiatriActionPerformed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdPetugasKeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed
        // TODO add your handling code here:
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);        
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void cb3fKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb3fKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb3fKeyPressed

    private void cb1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb1KeyPressed

    private void cb2aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb2aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb2aKeyPressed

    private void cb4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb4KeyPressed

    private void cb3aKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb3aKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb3aKeyPressed

    private void cb3bKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb3bKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb3bKeyPressed

    private void cb3cKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb3cKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb3cKeyPressed

    private void cb3dKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb3dKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb3dKeyPressed

    private void cb3eKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb3eKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb3eKeyPressed

    private void cb2bKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb2bKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb2bKeyPressed

    private void cb5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb5KeyPressed

    private void cb6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb6KeyPressed

    private void cb7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb7KeyPressed

    private void cb8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb8KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb8KeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMMCUPsikiatri dialog = new RMMCUPsikiatri(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPetugas;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.PanelBiasa FormInput;
    private widget.TextBox Jk;
    private widget.TextBox KdDokter;
    private widget.TextBox KdPetugas;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private javax.swing.JMenuItem MnCetakMCUPsikiatri;
    private widget.TextBox NmDokter;
    private widget.TextBox NmPetugas;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.TextBox TglLahir;
    private widget.Tanggal TglPemeriksaan;
    private widget.ComboBox cb1;
    private widget.ComboBox cb2a;
    private widget.ComboBox cb2b;
    private widget.ComboBox cb3a;
    private widget.ComboBox cb3b;
    private widget.ComboBox cb3c;
    private widget.ComboBox cb3d;
    private widget.ComboBox cb3e;
    private widget.ComboBox cb3f;
    private widget.ComboBox cb4;
    private widget.ComboBox cb5;
    private widget.ComboBox cb6;
    private widget.ComboBox cb7;
    private widget.ComboBox cb8;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel100;
    private widget.Label jLabel101;
    private widget.Label jLabel102;
    private widget.Label jLabel103;
    private widget.Label jLabel104;
    private widget.Label jLabel105;
    private widget.Label jLabel106;
    private widget.Label jLabel11;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private widget.Label jLabel94;
    private widget.Label jLabel96;
    private widget.Label jLabel97;
    private widget.Label jLabel98;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea kesimpulan;
    private widget.Label label11;
    private widget.Label label14;
    private widget.Label label15;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().trim().equals("")){
                ps=koneksi.prepareStatement(
                        "select pms.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk, " +
                        "p.tgl_lahir,pms.tanggal,pms.penampilan_umum,pms.mood,pms.afek,pms.pembicaraan_spontan,pms.pembicaraan_pelan,pms.pembicaraan_jelas,pms.pembicaraan_banyak, " +
                        "pms.pembicaraan_meloncat,pms.pembicaraan_lambat,pms.persepsi_halusinasi,pms.proses_pikir,pms.pengendalian_impuls,pms.fungsi_kognitif,pms.kemampuan_realitas, "+
                        "pms.kd_dokter,d.nm_dokter, p2.nip, p2.nama,pms.kesimpulan "+
                        "from reg_periksa " +
                        "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_mcu_psikiatri pms on reg_periksa.no_rawat = pms.no_rawat " +
                        "inner join dokter d on pms.kd_dokter = d.kd_dokter " +
                        "inner join petugas p2 on p2.nip = pms.kd_petugas " +
                        "where pms.tanggal between ? and ? " +
                        "order by pms.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                        "select pms.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk, " +
                        "p.tgl_lahir,pms.tanggal,pms.penampilan_umum,pms.mood,pms.afek,pms.pembicaraan_spontan,pms.pembicaraan_pelan,pms.pembicaraan_jelas,pms.pembicaraan_banyak, " +
                        "pms.pembicaraan_meloncat,pms.pembicaraan_lambat,pms.persepsi_halusinasi,pms.proses_pikir,pms.pengendalian_impuls,pms.fungsi_kognitif,pms.kemampuan_realitas, "+
                        "pms.kd_dokter,d.nm_dokter,p2.nip, p2.nama,pms.kesimpulan  "+
                        "from reg_periksa " +
                        "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_mcu_psikiatri pms on reg_periksa.no_rawat = pms.no_rawat " +
                        "inner join dokter d on pms.kd_dokter = d.kd_dokter " +
                        "inner join petugas p2 on p2.nip = pms.kd_petugas " +
                        "where pms.tanggal between ? and ? and (reg_periksa.no_rawat like ? or p.no_rkm_medis like ? or p.nm_pasien like ? or pms.kd_dokter like ? or d.nm_dokter like ?) " +
                        "order by pms.tanggal");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("tgl_lahir"),rs.getString("jk"),rs.getString("pms.kd_dokter"),rs.getString("d.nm_dokter"),rs.getString("tanggal"),
                        rs.getString("penampilan_umum"),rs.getString("mood"),rs.getString("afek"),
                        rs.getString("pembicaraan_spontan"),rs.getString("pembicaraan_pelan"),rs.getString("pembicaraan_jelas"),rs.getString("pembicaraan_banyak"),rs.getString("pembicaraan_meloncat"),rs.getString("pembicaraan_lambat"),rs.getString("persepsi_halusinasi"),rs.getString("proses_pikir"),rs.getString("pengendalian_impuls"),rs.getString("fungsi_kognitif"),rs.getString("kemampuan_realitas"),
                        rs.getString("pms.kesimpulan"),rs.getString("p2.nip"),rs.getString("p2.nama")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TabRawat.setSelectedIndex(0);
        TglPemeriksaan.setDate(new Date());
        cb1.setSelectedIndex(0);
        cb2a.setSelectedIndex(0);
        cb2b.setSelectedIndex(0);
        cb3a.setSelectedIndex(0);
        cb3b.setSelectedIndex(0);
        cb3c.setSelectedIndex(0);
        cb3d.setSelectedIndex(0);
        cb3e.setSelectedIndex(0);
        cb3f.setSelectedIndex(0);
        cb4.setSelectedIndex(0);
        cb5.setSelectedIndex(0);
        cb6.setSelectedIndex(0);
        cb7.setSelectedIndex(0);
        cb8.setSelectedIndex(0);
        kesimpulan.setText("");
      
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            Valid.SetTgl2(TglPemeriksaan,tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            cb1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            cb2a.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            cb2b.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            cb3a.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            cb3b.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            cb3c.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            cb3d.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            cb3e.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            cb3f.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            cb4.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            cb5.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            cb6.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            cb7.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            cb8.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            kesimpulan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
 
    public void setNoRm(String norwt,Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2); 
        Sequel.cariIsi("select kd_dokter from reg_periksa where no_rawat=?", KdDokter,norwt);
        Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?", NmDokter,KdDokter.getText());
        KdPetugas.setText(akses.getkode());
        Sequel.cariIsi("select nama from petugas where nip=?", NmPetugas,KdPetugas.getText());
        isRawat(); 
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_mcu());
        BtnHapus.setEnabled(akses.getpenilaian_mcu());
        BtnEdit.setEnabled(akses.getpenilaian_mcu());        
    }
    
    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from penilaian_mcu_gigi where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            tampil();
            TabRawat.setSelectedIndex(1);
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("penilaian_mcu_psikiatri","no_rawat=?","no_rawat=?,tanggal=?,kd_dokter=?,kd_petugas=?,penampilan_umum=?,mood=?,afek=?,pembicaraan_spontan=?,pembicaraan_pelan=?,pembicaraan_jelas=?,pembicaraan_banyak=?,pembicaraan_meloncat=?,pembicaraan_lambat=?,persepsi_halusinasi=?,proses_pikir=?,pengendalian_impuls=?,fungsi_kognitif=?,kemampuan_realitas=?,kesimpulan=?",20,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),
                KdPetugas.getText(),
                cb1.getSelectedItem().toString(),
                cb2a.getSelectedItem().toString(),
                cb2b.getSelectedItem().toString(),
                cb3a.getSelectedItem().toString(),
                cb3b.getSelectedItem().toString(),
                cb3c.getSelectedItem().toString(),
                cb3d.getSelectedItem().toString(),
                cb3e.getSelectedItem().toString(),
                cb3f.getSelectedItem().toString(),
                cb4.getSelectedItem().toString(),
                cb5.getSelectedItem().toString(),
                cb6.getSelectedItem().toString(),
                cb7.getSelectedItem().toString(),
                cb8.getSelectedItem().toString(),
                kesimpulan.getText(),
                TNoRw.getText()
            })==true){
               tampil();
               emptTeks();
               TabRawat.setSelectedIndex(1);
        }
    }
}
