//modifikasi dari source Mas Ikhsan, RS Karina Medika Purwakarta
package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;


/**
 *
 * @author perpustakaan
 */
public final class RMMCU extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private String finger=""; 
    private StringBuilder htmlContent;
    //START CUSTOM
    private String text_ekg="",text_saranlab="",text_kesanlab="",text_dokterlab="",
            text_rps="",text_rpk="",text_rpd="",text_alergi="",text_td="",text_hr="",
            text_rr="",text_suhu="",text_tb="",text_bb="",text_radiologi="",text_dokterrad="",
            print_ekg="",print_dokterrad="",print_dokterlab="",kamar="",namakamar="",uji_kebugaran="",dokter_uji_kebugaran="";
    private int count_dokter=0,count_dokterrad,print_count_dokterrad,print_count_dokter;
    private PreparedStatement ps2,ps3,ps4,pspermintaan;
    private ResultSet rs2,rs3,rs4,rspermintaan;
    //END CUSTOM
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMMCU(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","J.K.","Tgl.Lahir","Tanggal","Informasi","Riwayat Peyakit Sekarang","Riwayat Penyakit Keluarga", 
            "Riwayat Penyakit Dahulu","Alergi Makan & Obat","Keadaan Umum","Kesadaran","T.D.", "Nadi", "R.R.","T.B.","B.B.","Suhu","Submandibula", 
            "Axilla","Supraklavikula","Leher","Inguinal","Oedema","Frontalis","Maxilaris","Palpebra","Sklera","Cornea","Buta Warna",
            "Konjungtiva","Lensa","Pupil","Lubang Telinga","Daun Telinga","Selaput Pendengaran","Proc.Mastoideus","Septum Nasi","Lubang Hidung",
            "Bibir","Caries","Lidah","Faring","Tonsil","Kelenjar Limfe","Kelenjar Gondok","Gerakan Dada","Vocal Femitus","Perkusi Dada",
            "Bunyi Napas","Bunyi Tambahan","Ictus Cordis","Bunyi Jantung","Batas","Inspeksi","Palpasi","Hepar","Perkusi Abdomen","Auskultasi", 
            "Limpa","Costovertebral","Kondisi Kulit","Extrimitas Atas","Keterangan Extrimitas Atas","Extrimitas Bawah","Keterangan Extrimitas Bawah",
            "Pemeriksaan Laboratorium", "Rongsen Thorax","EKG","Spirometri","Audiometri","Treadmill","Lain-lain", "Merokok", "Alkohol", 
            "Kesimpulan", "Anjuran","Kode Dokter","Nama Dokter Penanggung Jawab"
            //START CUSTOM
            ,"Lingkar Perut","Kelainan Kepala","Kelainan Mata","Kelainan Telinga","Kelainan Hidung","Kelainan Leher",
            "Kelainan Dada","Kelainan Abdomen","Kelainan Ekstrimitas"
            //END CUSTOM
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //START CUSTOM || 80->89
        for (i = 0; i < 89; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(65);
            }else if(i==2){
                column.setPreferredWidth(160);
            }else if(i==3){
                column.setPreferredWidth(50);
            }else if(i==4){
                column.setPreferredWidth(60);
            }else if(i==5){
                column.setPreferredWidth(120);
            }else if(i==6){
                column.setPreferredWidth(80);
            }else if(i==7){
                column.setPreferredWidth(200);
            }else if(i==8){
                column.setPreferredWidth(200);
            }else if(i==9){
                column.setPreferredWidth(200);
            }else if(i==10){
                column.setPreferredWidth(150);
            }else if(i==11){
                column.setPreferredWidth(85);
            }else if(i==12){
                column.setPreferredWidth(77);
            }else if(i==13){
                column.setPreferredWidth(45);
            }else if(i==14){
                column.setPreferredWidth(30);
            }else if(i==15){
                column.setPreferredWidth(30);
            }else if(i==16){
                column.setPreferredWidth(30);
            }else if(i==17){
                column.setPreferredWidth(30);
            }else if(i==18){
                column.setPreferredWidth(32);
            }else if(i==19){
                column.setPreferredWidth(87);
            }else if(i==20){
                column.setPreferredWidth(87);
            }else if(i==21){
                column.setPreferredWidth(87);
            }else if(i==22){
                column.setPreferredWidth(87);
            }else if(i==23){
                column.setPreferredWidth(87);
            }else if(i==24){
                column.setPreferredWidth(57);
            }else if(i==25){
                column.setPreferredWidth(57);
            }else if(i==26){
                column.setPreferredWidth(57);
            }else if(i==27){
                column.setPreferredWidth(72);
            }else if(i==28){
                column.setPreferredWidth(72);
            }else if(i==29){
                column.setPreferredWidth(72);
            }else if(i==30){
                column.setPreferredWidth(100);
            }else if(i==31){
                column.setPreferredWidth(68);
            }else if(i==32){
                column.setPreferredWidth(57);
            }else if(i==33){
                column.setPreferredWidth(52);
            }else if(i==34){
                column.setPreferredWidth(82);
            }else if(i==35){
                column.setPreferredWidth(73);
            }else if(i==36){
                column.setPreferredWidth(112);
            }else if(i==37){
                column.setPreferredWidth(90);
            }else if(i==38){
                column.setPreferredWidth(70);
            }else if(i==39){
                column.setPreferredWidth(80);
            }else if(i==40){
                column.setPreferredWidth(50);
            }else if(i==41){
                column.setPreferredWidth(60);
            }else if(i==42){
                column.setPreferredWidth(45);
            }else if(i==43){
                column.setPreferredWidth(54);
            }else if(i==44){
                column.setPreferredWidth(45);
            }else if(i==45){
                column.setPreferredWidth(90);
            }else if(i==46){
                column.setPreferredWidth(90);
            }else if(i==47){
                column.setPreferredWidth(80);
            }else if(i==48){
                column.setPreferredWidth(80);
            }else if(i==49){
                column.setPreferredWidth(72);
            }else if(i==50){
                column.setPreferredWidth(69);
            }else if(i==51){
                column.setPreferredWidth(89);
            }else if(i==52){
                column.setPreferredWidth(72);
            }else if(i==53){
                column.setPreferredWidth(79);
            }else if(i==54){
                column.setPreferredWidth(50);
            }else if(i==55){
                column.setPreferredWidth(50);
            }else if(i==56){
                column.setPreferredWidth(131);
            }else if(i==57){
                column.setPreferredWidth(90);
            }else if(i==58){
                column.setPreferredWidth(92);
            }else if(i==59){
                column.setPreferredWidth(117);
            }else if(i==60){
                column.setPreferredWidth(87);
            }else if(i==61){
                column.setPreferredWidth(80);
            }else if(i==62){
                column.setPreferredWidth(70);
            }else if(i==63){
                column.setPreferredWidth(85);
            }else if(i==64){
                column.setPreferredWidth(143);
            }else if(i==65){
                column.setPreferredWidth(93);
            }else if(i==66){
                column.setPreferredWidth(153);
            }else if(i==67){
                column.setPreferredWidth(200);
            }else if(i==68){
                column.setPreferredWidth(200);
            }else if(i==69){
                column.setPreferredWidth(200);
            }else if(i==70){
                column.setPreferredWidth(200);
            }else if(i==71){
                column.setPreferredWidth(200);
            }else if(i==72){
                column.setPreferredWidth(200);
            }else if(i==73){
                column.setPreferredWidth(200);
            }else if(i==74){
                column.setPreferredWidth(120);
            }else if(i==75){
                column.setPreferredWidth(120);
            }else if(i==76){
                column.setPreferredWidth(250);
            }else if(i==77){
                column.setPreferredWidth(250);
            }else if(i==78){
                column.setPreferredWidth(90);
            }else if(i==79){
                column.setPreferredWidth(170);
            }else{
                column.setPreferredWidth(100);
            }
        }
        //END CUSTOM
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        RiwayatPenyakitSekarang.setDocument(new batasInput((int)2000).getKata(RiwayatPenyakitSekarang));
        RiwayatPenyakitKeluarga.setDocument(new batasInput((int)1000).getKata(RiwayatPenyakitKeluarga));
        RiwayatPenyakitDahulu.setDocument(new batasInput((int)1000).getKata(RiwayatPenyakitDahulu));
        RiwayatAlergiMakanan.setDocument(new batasInput((int)150).getKata(RiwayatAlergiMakanan));
        TD.setDocument(new batasInput((byte)8).getKata(TD));
        Nadi.setDocument(new batasInput((byte)5).getKata(Nadi));
        RR.setDocument(new batasInput((byte)5).getKata(RR));
        TinggiBadan.setDocument(new batasInput((byte)5).getKata(TinggiBadan));
        BeratBadan.setDocument(new batasInput((byte)5).getKata(BeratBadan));
        Suhu.setDocument(new batasInput((byte)5).getKata(Suhu));
        KetExtremitasAtas.setDocument(new batasInput((byte)50).getKata(KetExtremitasAtas));
        KetExtremitasBawah.setDocument(new batasInput((byte)50).getKata(KetExtremitasBawah));
        PemeriksaanLaboratorium.setDocument(new batasInput((int)1200).getKata(PemeriksaanLaboratorium));    //CUSTOM || 1000 -> 1200
        //RongsenThorax.setDocument(new batasInput((int)5000).getKata(RongsenThorax));  //CUSTOM || int 1000 -> BYTE 5000  
        EKG.setDocument(new batasInput((int)1200).getKata(EKG));    //CUSTOM || 1000 -> 1200
        Spirometri.setDocument(new batasInput((int)1000).getKata(Spirometri));
        Audiometri.setDocument(new batasInput((int)1000).getKata(Audiometri));
        Treadmill.setDocument(new batasInput((int)1000).getKata(Treadmill));
        Lainlain.setDocument(new batasInput((int)1000).getKata(Lainlain));
        Merokok.setDocument(new batasInput((int)100).getKata(Merokok));
        Alkohol.setDocument(new batasInput((int)100).getKata(Alkohol));
        Kesimpulan.setDocument(new batasInput((int)1000).getKata(Kesimpulan));
        Anjuran.setDocument(new batasInput((int)1000).getKata(Anjuran));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        LingkarPerut.setDocument(new batasInput((byte)5).getKata(LingkarPerut));    //CUSTOM KHANZA
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){ 
                    KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());   
                }              
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnPenilaianMCU = new javax.swing.JMenuItem();
        MnPenilaianMCU2 = new javax.swing.JMenuItem();
        MnPenilaianMCU3 = new javax.swing.JMenuItem();
        MnPenilaianMCU4 = new javax.swing.JMenuItem();
        MnPenilaianMCU4B = new javax.swing.JMenuItem();
        MnPenilaianMCU5 = new javax.swing.JMenuItem();
        MnPenilaianMCU5B = new javax.swing.JMenuItem();
        MnPenilaianMCU6 = new javax.swing.JMenuItem();
        MnPenilaianMCU6B = new javax.swing.JMenuItem();
        MnPenilaianMCU6BUMS = new javax.swing.JMenuItem();
        MnPenilaianMCU6C = new javax.swing.JMenuItem();
        MnPenilaianMCU7 = new javax.swing.JMenuItem();
        MnPenilaianMCUBPAFK = new javax.swing.JMenuItem();
        MnPenilaianMCUNuansa = new javax.swing.JMenuItem();
        MnPenilaianMCUJepang = new javax.swing.JMenuItem();
        MnPenilaianMCUJepangB = new javax.swing.JMenuItem();
        MnPenilaianMCULN = new javax.swing.JMenuItem();
        MnPenilaianMCUPolkesta = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnVisus = new widget.Button();
        BtnPenunjang = new widget.Button();
        BtnTemplateNormal = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdDokter = new widget.TextBox();
        NmDokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        label11 = new widget.Label();
        jLabel11 = new widget.Label();
        jLabel12 = new widget.Label();
        BeratBadan = new widget.TextBox();
        jLabel15 = new widget.Label();
        jLabel16 = new widget.Label();
        Nadi = new widget.TextBox();
        jLabel17 = new widget.Label();
        jLabel18 = new widget.Label();
        Suhu = new widget.TextBox();
        jLabel22 = new widget.Label();
        TD = new widget.TextBox();
        jLabel20 = new widget.Label();
        jLabel23 = new widget.Label();
        jLabel25 = new widget.Label();
        RR = new widget.TextBox();
        jLabel26 = new widget.Label();
        jLabel36 = new widget.Label();
        Informasi = new widget.ComboBox();
        jLabel53 = new widget.Label();
        TglAsuhan = new widget.Tanggal();
        jLabel28 = new widget.Label();
        TinggiBadan = new widget.TextBox();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel9 = new widget.Label();
        jLabel30 = new widget.Label();
        jLabel31 = new widget.Label();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel54 = new widget.Label();
        jLabel29 = new widget.Label();
        jLabel32 = new widget.Label();
        jLabel33 = new widget.Label();
        jLabel34 = new widget.Label();
        jLabel35 = new widget.Label();
        jLabel37 = new widget.Label();
        jLabel38 = new widget.Label();
        jLabel55 = new widget.Label();
        jLabel50 = new widget.Label();
        jLabel56 = new widget.Label();
        jLabel51 = new widget.Label();
        RiwayatAlergiMakanan = new widget.TextBox();
        jLabel57 = new widget.Label();
        KeadaanUmum = new widget.ComboBox();
        jLabel52 = new widget.Label();
        jLabel41 = new widget.Label();
        jLabel47 = new widget.Label();
        jLabel48 = new widget.Label();
        jLabel49 = new widget.Label();
        jLabel58 = new widget.Label();
        jLabel43 = new widget.Label();
        jLabel59 = new widget.Label();
        jLabel60 = new widget.Label();
        scrollPane10 = new widget.ScrollPane();
        PemeriksaanLaboratorium = new widget.TextArea();
        jLabel61 = new widget.Label();
        jLabel62 = new widget.Label();
        scrollPane11 = new widget.ScrollPane();
        RongsenThorax = new widget.TextArea();
        scrollPane12 = new widget.ScrollPane();
        EKG = new widget.TextArea();
        jLabel101 = new widget.Label();
        scrollPane14 = new widget.ScrollPane();
        Kesimpulan = new widget.TextArea();
        jLabel102 = new widget.Label();
        scrollPane15 = new widget.ScrollPane();
        Anjuran = new widget.TextArea();
        jLabel103 = new widget.Label();
        jLabel104 = new widget.Label();
        jLabel45 = new widget.Label();
        Submandibula = new widget.ComboBox();
        Kesadaran = new widget.ComboBox();
        Leher = new widget.ComboBox();
        Axila = new widget.ComboBox();
        Inguinal = new widget.ComboBox();
        Supraklavikula = new widget.ComboBox();
        Oedema = new widget.ComboBox();
        NyeriTekanSinusFrontalis = new widget.ComboBox();
        NyeriTekananSinusMaxilaris = new widget.ComboBox();
        Palpebra = new widget.ComboBox();
        Sklera = new widget.ComboBox();
        TestButaWarna = new widget.ComboBox();
        Konjungtiva = new widget.ComboBox();
        Lensa = new widget.ComboBox();
        Cornea = new widget.ComboBox();
        Pupil = new widget.ComboBox();
        LubangTelinga = new widget.ComboBox();
        jLabel105 = new widget.Label();
        jLabel63 = new widget.Label();
        DaunTelinga = new widget.ComboBox();
        jLabel64 = new widget.Label();
        SelaputPendengaran = new widget.ComboBox();
        jLabel65 = new widget.Label();
        NyeriMastoideus = new widget.ComboBox();
        jLabel46 = new widget.Label();
        jLabel66 = new widget.Label();
        SeptumNasi = new widget.ComboBox();
        jLabel67 = new widget.Label();
        LubangHidung = new widget.ComboBox();
        jLabel68 = new widget.Label();
        jLabel69 = new widget.Label();
        jLabel70 = new widget.Label();
        jLabel71 = new widget.Label();
        jLabel72 = new widget.Label();
        jLabel73 = new widget.Label();
        Bibir = new widget.ComboBox();
        Caries = new widget.ComboBox();
        Lidah = new widget.ComboBox();
        Faring = new widget.ComboBox();
        Tonsil = new widget.ComboBox();
        jLabel74 = new widget.Label();
        jLabel75 = new widget.Label();
        jLabel76 = new widget.Label();
        KelenjarLimfe = new widget.ComboBox();
        KelenjarGondok = new widget.ComboBox();
        jLabel77 = new widget.Label();
        jLabel78 = new widget.Label();
        jLabel79 = new widget.Label();
        jLabel80 = new widget.Label();
        jLabel81 = new widget.Label();
        jLabel82 = new widget.Label();
        jLabel83 = new widget.Label();
        jLabel84 = new widget.Label();
        GerakanDada = new widget.ComboBox();
        BunyiNapas = new widget.ComboBox();
        VocalFremitus = new widget.ComboBox();
        BunyiTambahan = new widget.ComboBox();
        PerkusiDada = new widget.ComboBox();
        jLabel44 = new widget.Label();
        jLabel85 = new widget.Label();
        jLabel86 = new widget.Label();
        jLabel87 = new widget.Label();
        jLabel88 = new widget.Label();
        jLabel89 = new widget.Label();
        jLabel90 = new widget.Label();
        jLabel91 = new widget.Label();
        jLabel92 = new widget.Label();
        jLabel93 = new widget.Label();
        jLabel94 = new widget.Label();
        jLabel95 = new widget.Label();
        jLabel96 = new widget.Label();
        jLabel97 = new widget.Label();
        jLabel98 = new widget.Label();
        jLabel99 = new widget.Label();
        jLabel100 = new widget.Label();
        IctusCordis = new widget.ComboBox();
        BunyiJantung = new widget.ComboBox();
        Batas = new widget.ComboBox();
        Inspeksi = new widget.ComboBox();
        Palpasi = new widget.ComboBox();
        PerkusiAbdomen = new widget.ComboBox();
        Auskultasi = new widget.ComboBox();
        Hepar = new widget.ComboBox();
        Limpa = new widget.ComboBox();
        NyeriKtok = new widget.ComboBox();
        ExtremitasAtas = new widget.ComboBox();
        ExtremitasBawah = new widget.ComboBox();
        KetExtremitasAtas = new widget.TextBox();
        KetExtremitasBawah = new widget.TextBox();
        Kulit = new widget.ComboBox();
        scrollPane2 = new widget.ScrollPane();
        RiwayatPenyakitSekarang = new widget.TextArea();
        scrollPane3 = new widget.ScrollPane();
        RiwayatPenyakitKeluarga = new widget.TextArea();
        scrollPane4 = new widget.ScrollPane();
        RiwayatPenyakitDahulu = new widget.TextArea();
        jLabel24 = new widget.Label();
        jLabel13 = new widget.Label();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        Merokok = new widget.TextBox();
        Alkohol = new widget.TextBox();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jSeparator10 = new javax.swing.JSeparator();
        jSeparator11 = new javax.swing.JSeparator();
        jSeparator12 = new javax.swing.JSeparator();
        jSeparator13 = new javax.swing.JSeparator();
        jSeparator14 = new javax.swing.JSeparator();
        jLabel106 = new widget.Label();
        jSeparator15 = new javax.swing.JSeparator();
        jLabel107 = new widget.Label();
        scrollPane13 = new widget.ScrollPane();
        Spirometri = new widget.TextArea();
        jSeparator16 = new javax.swing.JSeparator();
        jLabel108 = new widget.Label();
        scrollPane16 = new widget.ScrollPane();
        Audiometri = new widget.TextArea();
        jSeparator17 = new javax.swing.JSeparator();
        jLabel109 = new widget.Label();
        scrollPane17 = new widget.ScrollPane();
        Treadmill = new widget.TextArea();
        jSeparator18 = new javax.swing.JSeparator();
        jLabel110 = new widget.Label();
        scrollPane18 = new widget.ScrollPane();
        Lainlain = new widget.TextArea();
        jLabel27 = new widget.Label();
        LingkarPerut = new widget.TextBox();
        jLabel40 = new widget.Label();
        jLabel42 = new widget.Label();
        jSeparator19 = new javax.swing.JSeparator();
        jLabel111 = new widget.Label();
        jLabel112 = new widget.Label();
        jLabel113 = new widget.Label();
        KelainanKepala = new widget.ComboBox();
        KelainanMata = new widget.ComboBox();
        jLabel114 = new widget.Label();
        KelainanTelinga = new widget.ComboBox();
        jLabel115 = new widget.Label();
        KelainanHidung = new widget.ComboBox();
        jLabel116 = new widget.Label();
        jLabel117 = new widget.Label();
        jLabel118 = new widget.Label();
        jLabel119 = new widget.Label();
        KelainanLeher = new widget.ComboBox();
        KelainanDada = new widget.ComboBox();
        KelainanAbdomen = new widget.ComboBox();
        KelainanEkstrimitas = new widget.ComboBox();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnPenilaianMCU.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU.setText("Laporan Penilaian MCU");
        MnPenilaianMCU.setName("MnPenilaianMCU"); // NOI18N
        MnPenilaianMCU.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCUActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU);

        MnPenilaianMCU2.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU2.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU2.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU2.setText("Laporan Penilaian MCU 2");
        MnPenilaianMCU2.setName("MnPenilaianMCU2"); // NOI18N
        MnPenilaianMCU2.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU2);

        MnPenilaianMCU3.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU3.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU3.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU3.setText("Laporan Penilaian MCU 3");
        MnPenilaianMCU3.setName("MnPenilaianMCU3"); // NOI18N
        MnPenilaianMCU3.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU3ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU3);

        MnPenilaianMCU4.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU4.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU4.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU4.setText("Laporan Penilaian MCU 4");
        MnPenilaianMCU4.setName("MnPenilaianMCU4"); // NOI18N
        MnPenilaianMCU4.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU4ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU4);

        MnPenilaianMCU4B.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU4B.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU4B.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU4B.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU4B.setText("Laporan Penilaian MCU 4 B");
        MnPenilaianMCU4B.setName("MnPenilaianMCU4B"); // NOI18N
        MnPenilaianMCU4B.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU4B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU4BActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU4B);

        MnPenilaianMCU5.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU5.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU5.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU5.setText("Laporan Penilaian MCU 5");
        MnPenilaianMCU5.setName("MnPenilaianMCU5"); // NOI18N
        MnPenilaianMCU5.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU5ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU5);

        MnPenilaianMCU5B.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU5B.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU5B.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU5B.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU5B.setText("Laporan Penilaian MCU 5 LN");
        MnPenilaianMCU5B.setName("MnPenilaianMCU5B"); // NOI18N
        MnPenilaianMCU5B.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU5B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU5BActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU5B);

        MnPenilaianMCU6.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU6.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU6.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU6.setText("Laporan Penilaian MCU 6");
        MnPenilaianMCU6.setName("MnPenilaianMCU6"); // NOI18N
        MnPenilaianMCU6.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU6ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU6);

        MnPenilaianMCU6B.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU6B.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU6B.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU6B.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU6B.setText("Laporan Penilaian MCU 6 B");
        MnPenilaianMCU6B.setName("MnPenilaianMCU6B"); // NOI18N
        MnPenilaianMCU6B.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU6B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU6BActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU6B);

        MnPenilaianMCU6BUMS.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU6BUMS.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU6BUMS.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU6BUMS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU6BUMS.setText("Laporan Penilaian MCU UMS");
        MnPenilaianMCU6BUMS.setName("MnPenilaianMCU6BUMS"); // NOI18N
        MnPenilaianMCU6BUMS.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU6BUMS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU6BUMSActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU6BUMS);

        MnPenilaianMCU6C.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU6C.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU6C.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU6C.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU6C.setText("Laporan Penilaian MCU 6 C");
        MnPenilaianMCU6C.setName("MnPenilaianMCU6C"); // NOI18N
        MnPenilaianMCU6C.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU6C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU6CActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU6C);

        MnPenilaianMCU7.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU7.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU7.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU7.setText("Laporan Penilaian MCU 7");
        MnPenilaianMCU7.setName("MnPenilaianMCU7"); // NOI18N
        MnPenilaianMCU7.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCU7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCU7ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU7);

        MnPenilaianMCUBPAFK.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCUBPAFK.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCUBPAFK.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCUBPAFK.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCUBPAFK.setText("Laporan Penilaian MCU BPAFK");
        MnPenilaianMCUBPAFK.setName("MnPenilaianMCUBPAFK"); // NOI18N
        MnPenilaianMCUBPAFK.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCUBPAFK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCUBPAFKActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCUBPAFK);

        MnPenilaianMCUNuansa.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCUNuansa.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCUNuansa.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCUNuansa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCUNuansa.setText("Laporan Penilaian MCU Nuansa");
        MnPenilaianMCUNuansa.setName("MnPenilaianMCUNuansa"); // NOI18N
        MnPenilaianMCUNuansa.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCUNuansa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCUNuansaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCUNuansa);

        MnPenilaianMCUJepang.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCUJepang.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCUJepang.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCUJepang.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCUJepang.setText("Laporan Penilaian MCU Jepang");
        MnPenilaianMCUJepang.setName("MnPenilaianMCUJepang"); // NOI18N
        MnPenilaianMCUJepang.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCUJepang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCUJepangActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCUJepang);

        MnPenilaianMCUJepangB.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCUJepangB.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCUJepangB.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCUJepangB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCUJepangB.setText("Laporan Penilaian MCU Jepang 2");
        MnPenilaianMCUJepangB.setName("MnPenilaianMCUJepangB"); // NOI18N
        MnPenilaianMCUJepangB.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCUJepangB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCUJepangBActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCUJepangB);

        MnPenilaianMCULN.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCULN.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCULN.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCULN.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCULN.setText("Laporan Penilaian MCU LN");
        MnPenilaianMCULN.setName("MnPenilaianMCULN"); // NOI18N
        MnPenilaianMCULN.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCULN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCULNActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCULN);

        MnPenilaianMCUPolkesta.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCUPolkesta.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCUPolkesta.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCUPolkesta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCUPolkesta.setText("Laporan Penilaian MCU Polkesta");
        MnPenilaianMCUPolkesta.setName("MnPenilaianMCUPolkesta"); // NOI18N
        MnPenilaianMCUPolkesta.setPreferredSize(new java.awt.Dimension(180, 26));
        MnPenilaianMCUPolkesta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCUPolkestaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCUPolkesta);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Penilaian Medical Check Up (MCU) ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnVisus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Doctor.png"))); // NOI18N
        BtnVisus.setMnemonic('T');
        BtnVisus.setText("Mata-THT");
        BtnVisus.setToolTipText("Alt+T");
        BtnVisus.setName("BtnVisus"); // NOI18N
        BtnVisus.setPreferredSize(new java.awt.Dimension(105, 30));
        BtnVisus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVisusActionPerformed(evt);
            }
        });
        BtnVisus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnVisusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnVisus);

        BtnPenunjang.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/editcopy.png"))); // NOI18N
        BtnPenunjang.setMnemonic('T');
        BtnPenunjang.setText("Penunjang");
        BtnPenunjang.setToolTipText("Alt+T");
        BtnPenunjang.setName("BtnPenunjang"); // NOI18N
        BtnPenunjang.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPenunjang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPenunjangActionPerformed(evt);
            }
        });
        BtnPenunjang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPenunjangKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPenunjang);

        BtnTemplateNormal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/editcopy.png"))); // NOI18N
        BtnTemplateNormal.setMnemonic('T');
        BtnTemplateNormal.setToolTipText("Alt+T");
        BtnTemplateNormal.setLabel("Normal");
        BtnTemplateNormal.setName("BtnTemplateNormal"); // NOI18N
        BtnTemplateNormal.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnTemplateNormal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTemplateNormalActionPerformed(evt);
            }
        });
        BtnTemplateNormal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnTemplateNormalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnTemplateNormal);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 2200));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Dokter P.J. :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 40, 70, 23);

        KdDokter.setEditable(false);
        KdDokter.setName("KdDokter"); // NOI18N
        KdDokter.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput.add(KdDokter);
        KdDokter.setBounds(74, 40, 100, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        NmDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmDokter);
        NmDokter.setBounds(176, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(358, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(395, 40, 57, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jLabel12.setText("Berat Badan :");
        jLabel12.setName("jLabel12"); // NOI18N
        FormInput.add(jLabel12);
        jLabel12.setBounds(390, 250, 70, 23);

        BeratBadan.setFocusTraversalPolicyProvider(true);
        BeratBadan.setName("BeratBadan"); // NOI18N
        BeratBadan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BeratBadanKeyPressed(evt);
            }
        });
        FormInput.add(BeratBadan);
        BeratBadan.setBounds(470, 250, 50, 23);

        jLabel15.setText("Kesadaran :");
        jLabel15.setName("jLabel15"); // NOI18N
        FormInput.add(jLabel15);
        jLabel15.setBounds(272, 220, 80, 23);

        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel16.setText("x/menit");
        jLabel16.setName("jLabel16"); // NOI18N
        FormInput.add(jLabel16);
        jLabel16.setBounds(817, 220, 50, 23);

        Nadi.setFocusTraversalPolicyProvider(true);
        Nadi.setName("Nadi"); // NOI18N
        Nadi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NadiKeyPressed(evt);
            }
        });
        FormInput.add(Nadi);
        Nadi.setBounds(764, 220, 50, 23);

        jLabel17.setText("Nadi :");
        jLabel17.setName("jLabel17"); // NOI18N
        FormInput.add(jLabel17);
        jLabel17.setBounds(700, 220, 60, 23);

        jLabel18.setText("Lingkar Perut :");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(710, 250, 70, 23);

        Suhu.setFocusTraversalPolicyProvider(true);
        Suhu.setName("Suhu"); // NOI18N
        Suhu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SuhuKeyPressed(evt);
            }
        });
        FormInput.add(Suhu);
        Suhu.setBounds(620, 250, 50, 23);

        jLabel22.setText("TD :");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(532, 220, 30, 23);

        TD.setFocusTraversalPolicyProvider(true);
        TD.setName("TD"); // NOI18N
        TD.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TDKeyPressed(evt);
            }
        });
        FormInput.add(TD);
        TD.setBounds(566, 220, 65, 23);

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setText("°C");
        jLabel20.setName("jLabel20"); // NOI18N
        FormInput.add(jLabel20);
        jLabel20.setBounds(675, 250, 20, 23);

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel23.setText("mmHg");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(634, 220, 50, 23);

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel25.setText("x/menit");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(155, 250, 50, 23);

        RR.setFocusTraversalPolicyProvider(true);
        RR.setName("RR"); // NOI18N
        RR.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RRKeyPressed(evt);
            }
        });
        FormInput.add(RR);
        RR.setBounds(100, 250, 50, 23);

        jLabel26.setText("RR :");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(0, 250, 90, 23);

        jLabel36.setText("Informasi didapat dari :");
        jLabel36.setName("jLabel36"); // NOI18N
        FormInput.add(jLabel36);
        jLabel36.setBounds(592, 40, 130, 23);

        Informasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Autoanamnesis", "Alloanamnesis" }));
        Informasi.setName("Informasi"); // NOI18N
        Informasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                InformasiKeyPressed(evt);
            }
        });
        FormInput.add(Informasi);
        Informasi.setBounds(726, 40, 128, 23);

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel53.setText("A. ANAMNESA SINGKAT");
        jLabel53.setName("jLabel53"); // NOI18N
        FormInput.add(jLabel53);
        jLabel53.setBounds(10, 70, 180, 23);

        TglAsuhan.setForeground(new java.awt.Color(50, 70, 50));
        TglAsuhan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "24-01-2025 14:01:54" }));
        TglAsuhan.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglAsuhan.setName("TglAsuhan"); // NOI18N
        TglAsuhan.setOpaque(false);
        TglAsuhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglAsuhanKeyPressed(evt);
            }
        });
        FormInput.add(TglAsuhan);
        TglAsuhan.setBounds(456, 40, 130, 23);

        jLabel28.setText("Tinggi Badan :");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(210, 250, 80, 23);

        TinggiBadan.setFocusTraversalPolicyProvider(true);
        TinggiBadan.setName("TinggiBadan"); // NOI18N
        TinggiBadan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TinggiBadanKeyPressed(evt);
            }
        });
        FormInput.add(TinggiBadan);
        TinggiBadan.setBounds(300, 250, 50, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 70, 880, 1);

        jLabel9.setText("Sklera :");
        jLabel9.setName("jLabel9"); // NOI18N
        FormInput.add(jLabel9);
        jLabel9.setBounds(270, 450, 57, 23);

        jLabel30.setText("Palpebra  :");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(100, 450, 60, 23);

        jLabel31.setText("Konjungtiva :");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(100, 480, 70, 23);

        jSeparator2.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator2.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator2.setName("jSeparator2"); // NOI18N
        FormInput.add(jSeparator2);
        jSeparator2.setBounds(0, 200, 880, 1);

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("B. PEMERIKSAAN FISIK");
        jLabel54.setName("jLabel54"); // NOI18N
        FormInput.add(jLabel54);
        jLabel54.setBounds(10, 200, 180, 23);

        jLabel29.setText("Submandibula :");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(0, 300, 170, 23);

        jLabel32.setText("Leher :");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(0, 330, 170, 23);

        jLabel33.setText("Supraklavikula :");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(630, 300, 90, 23);

        jLabel34.setText("Kelenjar Limfe :");
        jLabel34.setName("jLabel34"); // NOI18N
        FormInput.add(jLabel34);
        jLabel34.setBounds(0, 280, 121, 23);

        jLabel35.setText("Axilla :");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(371, 300, 60, 23);

        jLabel37.setText("Inguinal :");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(371, 330, 60, 23);

        jLabel38.setText("Oedema :");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(100, 400, 50, 23);

        jLabel55.setText("Riwayat Penyakit Sekarang :");
        jLabel55.setName("jLabel55"); // NOI18N
        FormInput.add(jLabel55);
        jLabel55.setBounds(0, 90, 182, 23);

        jLabel50.setText("Riwayat Penyakit Keluarga :");
        jLabel50.setName("jLabel50"); // NOI18N
        FormInput.add(jLabel50);
        jLabel50.setBounds(295, 90, 160, 23);

        jLabel56.setText("Riwayat Penyakit Dahulu :");
        jLabel56.setName("jLabel56"); // NOI18N
        FormInput.add(jLabel56);
        jLabel56.setBounds(571, 90, 150, 23);

        jLabel51.setText("Riwayat Alergi Makanan & Obat :");
        jLabel51.setName("jLabel51"); // NOI18N
        FormInput.add(jLabel51);
        jLabel51.setBounds(0, 170, 205, 23);

        RiwayatAlergiMakanan.setFocusTraversalPolicyProvider(true);
        RiwayatAlergiMakanan.setName("RiwayatAlergiMakanan"); // NOI18N
        RiwayatAlergiMakanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatAlergiMakananKeyPressed(evt);
            }
        });
        FormInput.add(RiwayatAlergiMakanan);
        RiwayatAlergiMakanan.setBounds(209, 170, 645, 23);

        jLabel57.setText("Keadaan Umum :");
        jLabel57.setName("jLabel57"); // NOI18N
        FormInput.add(jLabel57);
        jLabel57.setBounds(0, 220, 127, 23);

        KeadaanUmum.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Baik", "Tidak Baik" }));
        KeadaanUmum.setName("KeadaanUmum"); // NOI18N
        KeadaanUmum.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeadaanUmumKeyPressed(evt);
            }
        });
        FormInput.add(KeadaanUmum);
        KeadaanUmum.setBounds(131, 220, 100, 23);

        jLabel52.setText("Nyeri Tekanan Sinus Maxilaris :");
        jLabel52.setName("jLabel52"); // NOI18N
        FormInput.add(jLabel52);
        jLabel52.setBounds(580, 400, 150, 23);

        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel41.setText("2. Mata");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(70, 430, 150, 23);

        jLabel47.setText("Cornea :");
        jLabel47.setName("jLabel47"); // NOI18N
        FormInput.add(jLabel47);
        jLabel47.setBounds(450, 450, 50, 23);

        jLabel48.setText("Lensa :");
        jLabel48.setName("jLabel48"); // NOI18N
        FormInput.add(jLabel48);
        jLabel48.setBounds(270, 480, 57, 23);

        jLabel49.setText("Pupil :");
        jLabel49.setName("jLabel49"); // NOI18N
        FormInput.add(jLabel49);
        jLabel49.setBounds(440, 480, 50, 23);

        jLabel58.setText("Buta Warna :");
        jLabel58.setName("jLabel58"); // NOI18N
        FormInput.add(jLabel58);
        jLabel58.setBounds(630, 450, 80, 23);

        jLabel43.setText("Kulit :");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(429, 940, 60, 23);

        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel59.setText("C. PEMERIKSAAN LABORATORIUM (TERLAMPIR)");
        jLabel59.setName("jLabel59"); // NOI18N
        FormInput.add(jLabel59);
        jLabel59.setBounds(10, 1340, 280, 23);

        jLabel60.setText("Proc. Mastoideus :");
        jLabel60.setName("jLabel60"); // NOI18N
        FormInput.add(jLabel60);
        jLabel60.setBounds(652, 530, 100, 23);

        scrollPane10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane10.setName("scrollPane10"); // NOI18N

        PemeriksaanLaboratorium.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        PemeriksaanLaboratorium.setColumns(20);
        PemeriksaanLaboratorium.setRows(5);
        PemeriksaanLaboratorium.setName("PemeriksaanLaboratorium"); // NOI18N
        PemeriksaanLaboratorium.setPreferredSize(new java.awt.Dimension(182, 52));
        PemeriksaanLaboratorium.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PemeriksaanLaboratoriumKeyPressed(evt);
            }
        });
        scrollPane10.setViewportView(PemeriksaanLaboratorium);

        FormInput.add(scrollPane10);
        scrollPane10.setBounds(44, 1360, 810, 53);

        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel61.setText("E. EKG");
        jLabel61.setName("jLabel61"); // NOI18N
        FormInput.add(jLabel61);
        jLabel61.setBounds(10, 1500, 182, 23);

        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel62.setText("D. RONTGEN");
        jLabel62.setName("jLabel62"); // NOI18N
        FormInput.add(jLabel62);
        jLabel62.setBounds(10, 1420, 182, 23);

        scrollPane11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane11.setName("scrollPane11"); // NOI18N

        RongsenThorax.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        RongsenThorax.setColumns(20);
        RongsenThorax.setRows(5);
        RongsenThorax.setName("RongsenThorax"); // NOI18N
        RongsenThorax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RongsenThoraxKeyPressed(evt);
            }
        });
        scrollPane11.setViewportView(RongsenThorax);

        FormInput.add(scrollPane11);
        scrollPane11.setBounds(44, 1440, 810, 53);

        scrollPane12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane12.setName("scrollPane12"); // NOI18N
        scrollPane12.setPreferredSize(new java.awt.Dimension(199, 52));

        EKG.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EKG.setColumns(20);
        EKG.setRows(5);
        EKG.setName("EKG"); // NOI18N
        EKG.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EKGKeyPressed(evt);
            }
        });
        scrollPane12.setViewportView(EKG);

        FormInput.add(scrollPane12);
        scrollPane12.setBounds(44, 1520, 810, 53);

        jLabel101.setText("Merokok :");
        jLabel101.setName("jLabel101"); // NOI18N
        FormInput.add(jLabel101);
        jLabel101.setBounds(0, 1920, 91, 23);

        scrollPane14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane14.setName("scrollPane14"); // NOI18N

        Kesimpulan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Kesimpulan.setColumns(20);
        Kesimpulan.setRows(5);
        Kesimpulan.setName("Kesimpulan"); // NOI18N
        Kesimpulan.setPreferredSize(new java.awt.Dimension(102, 52));
        Kesimpulan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KesimpulanKeyPressed(evt);
            }
        });
        scrollPane14.setViewportView(Kesimpulan);

        FormInput.add(scrollPane14);
        scrollPane14.setBounds(44, 1970, 810, 53);

        jLabel102.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel102.setText("K. KESIMPULAN");
        jLabel102.setName("jLabel102"); // NOI18N
        FormInput.add(jLabel102);
        jLabel102.setBounds(10, 1950, 190, 23);

        scrollPane15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane15.setName("scrollPane15"); // NOI18N

        Anjuran.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Anjuran.setColumns(20);
        Anjuran.setRows(5);
        Anjuran.setName("Anjuran"); // NOI18N
        Anjuran.setPreferredSize(new java.awt.Dimension(102, 52));
        Anjuran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AnjuranKeyPressed(evt);
            }
        });
        scrollPane15.setViewportView(Anjuran);

        FormInput.add(scrollPane15);
        scrollPane15.setBounds(44, 2050, 810, 53);

        jLabel103.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel103.setText("L. ANJURAN");
        jLabel103.setName("jLabel103"); // NOI18N
        FormInput.add(jLabel103);
        jLabel103.setBounds(10, 2030, 330, 23);

        jLabel104.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel104.setText("J. RIWAYAT MEROKOK DAN KONSUMSI ALKOHOL");
        jLabel104.setName("jLabel104"); // NOI18N
        FormInput.add(jLabel104);
        jLabel104.setBounds(10, 1900, 340, 23);

        jLabel45.setText("Kepala :");
        jLabel45.setName("jLabel45"); // NOI18N
        FormInput.add(jLabel45);
        jLabel45.setBounds(0, 360, 84, 23);

        Submandibula.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Submandibula.setName("Submandibula"); // NOI18N
        Submandibula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SubmandibulaKeyPressed(evt);
            }
        });
        FormInput.add(Submandibula);
        Submandibula.setBounds(174, 300, 130, 23);

        Kesadaran.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Composmentis", "Apatis", "Somnolen" }));
        Kesadaran.setName("Kesadaran"); // NOI18N
        Kesadaran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KesadaranKeyPressed(evt);
            }
        });
        FormInput.add(Kesadaran);
        Kesadaran.setBounds(356, 220, 125, 23);

        Leher.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Leher.setName("Leher"); // NOI18N
        Leher.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LeherKeyPressed(evt);
            }
        });
        FormInput.add(Leher);
        Leher.setBounds(174, 330, 130, 23);

        Axila.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Axila.setName("Axila"); // NOI18N
        Axila.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AxilaKeyPressed(evt);
            }
        });
        FormInput.add(Axila);
        Axila.setBounds(435, 300, 130, 23);

        Inguinal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Inguinal.setName("Inguinal"); // NOI18N
        Inguinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                InguinalKeyPressed(evt);
            }
        });
        FormInput.add(Inguinal);
        Inguinal.setBounds(435, 330, 130, 23);

        Supraklavikula.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Supraklavikula.setName("Supraklavikula"); // NOI18N
        Supraklavikula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SupraklavikulaKeyPressed(evt);
            }
        });
        FormInput.add(Supraklavikula);
        Supraklavikula.setBounds(724, 300, 130, 23);

        Oedema.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        Oedema.setName("Oedema"); // NOI18N
        Oedema.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                OedemaKeyPressed(evt);
            }
        });
        FormInput.add(Oedema);
        Oedema.setBounds(160, 400, 73, 23);

        NyeriTekanSinusFrontalis.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        NyeriTekanSinusFrontalis.setName("NyeriTekanSinusFrontalis"); // NOI18N
        NyeriTekanSinusFrontalis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NyeriTekanSinusFrontalisKeyPressed(evt);
            }
        });
        FormInput.add(NyeriTekanSinusFrontalis);
        NyeriTekanSinusFrontalis.setBounds(440, 400, 73, 23);

        NyeriTekananSinusMaxilaris.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        NyeriTekananSinusMaxilaris.setName("NyeriTekananSinusMaxilaris"); // NOI18N
        NyeriTekananSinusMaxilaris.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NyeriTekananSinusMaxilarisKeyPressed(evt);
            }
        });
        FormInput.add(NyeriTekananSinusMaxilaris);
        NyeriTekananSinusMaxilaris.setBounds(740, 400, 73, 23);

        Palpebra.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Oedem", "Ptosis" }));
        Palpebra.setName("Palpebra"); // NOI18N
        Palpebra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PalpebraKeyPressed(evt);
            }
        });
        FormInput.add(Palpebra);
        Palpebra.setBounds(160, 450, 100, 23);

        Sklera.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Ikterik" }));
        Sklera.setName("Sklera"); // NOI18N
        Sklera.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SkleraKeyPressed(evt);
            }
        });
        FormInput.add(Sklera);
        Sklera.setBounds(330, 450, 100, 23);

        TestButaWarna.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Buta Warna Partial", "Buta Warna Total" }));
        TestButaWarna.setName("TestButaWarna"); // NOI18N
        TestButaWarna.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TestButaWarnaKeyPressed(evt);
            }
        });
        FormInput.add(TestButaWarna);
        TestButaWarna.setBounds(720, 450, 145, 23);

        Konjungtiva.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Anemis", "Hiperemis" }));
        Konjungtiva.setName("Konjungtiva"); // NOI18N
        Konjungtiva.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KonjungtivaKeyPressed(evt);
            }
        });
        FormInput.add(Konjungtiva);
        Konjungtiva.setBounds(170, 480, 100, 23);

        Lensa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Jernih", "Keruh", "Kacamata" }));
        Lensa.setName("Lensa"); // NOI18N
        Lensa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LensaKeyPressed(evt);
            }
        });
        FormInput.add(Lensa);
        Lensa.setBounds(330, 480, 100, 23);

        Cornea.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tidak Normal" }));
        Cornea.setName("Cornea"); // NOI18N
        Cornea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CorneaKeyPressed(evt);
            }
        });
        FormInput.add(Cornea);
        Cornea.setBounds(500, 450, 115, 23);

        Pupil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Isokor", "Anisokor" }));
        Pupil.setName("Pupil"); // NOI18N
        Pupil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PupilKeyPressed(evt);
            }
        });
        FormInput.add(Pupil);
        Pupil.setBounds(490, 480, 115, 23);

        LubangTelinga.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lapang", "Sempit", "Serumen Prop" }));
        LubangTelinga.setName("LubangTelinga"); // NOI18N
        LubangTelinga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LubangTelingaKeyPressed(evt);
            }
        });
        FormInput.add(LubangTelinga);
        LubangTelinga.setBounds(140, 530, 118, 23);

        jLabel105.setText("Alkohol :");
        jLabel105.setName("jLabel105"); // NOI18N
        FormInput.add(jLabel105);
        jLabel105.setBounds(456, 1920, 50, 23);

        jLabel63.setText("Lubang Hidung :");
        jLabel63.setName("jLabel63"); // NOI18N
        FormInput.add(jLabel63);
        jLabel63.setBounds(350, 580, 90, 23);

        DaunTelinga.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tidak Normal" }));
        DaunTelinga.setName("DaunTelinga"); // NOI18N
        DaunTelinga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DaunTelingaKeyPressed(evt);
            }
        });
        FormInput.add(DaunTelinga);
        DaunTelinga.setBounds(306, 530, 114, 23);

        jLabel64.setText("Daun :");
        jLabel64.setName("jLabel64"); // NOI18N
        FormInput.add(jLabel64);
        jLabel64.setBounds(259, 530, 43, 23);

        SelaputPendengaran.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Intak", "Tidak Intak" }));
        SelaputPendengaran.setName("SelaputPendengaran"); // NOI18N
        SelaputPendengaran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelaputPendengaranKeyPressed(evt);
            }
        });
        FormInput.add(SelaputPendengaran);
        SelaputPendengaran.setBounds(548, 530, 105, 23);

        jLabel65.setText("Selaput Pendengaran :");
        jLabel65.setName("jLabel65"); // NOI18N
        FormInput.add(jLabel65);
        jLabel65.setBounds(420, 530, 124, 23);

        NyeriMastoideus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        NyeriMastoideus.setName("NyeriMastoideus"); // NOI18N
        NyeriMastoideus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NyeriMastoideusKeyPressed(evt);
            }
        });
        FormInput.add(NyeriMastoideus);
        NyeriMastoideus.setBounds(756, 530, 98, 23);

        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel46.setText("3. Telinga");
        jLabel46.setName("jLabel46"); // NOI18N
        FormInput.add(jLabel46);
        jLabel46.setBounds(70, 510, 150, 23);

        jLabel66.setText("Lubang :");
        jLabel66.setName("jLabel66"); // NOI18N
        FormInput.add(jLabel66);
        jLabel66.setBounds(0, 530, 136, 23);

        SeptumNasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Deviasi" }));
        SeptumNasi.setName("SeptumNasi"); // NOI18N
        SeptumNasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SeptumNasiKeyPressed(evt);
            }
        });
        FormInput.add(SeptumNasi);
        SeptumNasi.setBounds(166, 580, 100, 23);

        jLabel67.setText("Tonsil :");
        jLabel67.setName("jLabel67"); // NOI18N
        FormInput.add(jLabel67);
        jLabel67.setBounds(710, 630, 60, 23);

        LubangHidung.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lapang", "Rhinore", "Epistaksis" }));
        LubangHidung.setName("LubangHidung"); // NOI18N
        LubangHidung.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LubangHidungKeyPressed(evt);
            }
        });
        FormInput.add(LubangHidung);
        LubangHidung.setBounds(444, 580, 100, 23);

        jLabel68.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel68.setText("4. Hidung");
        jLabel68.setName("jLabel68"); // NOI18N
        FormInput.add(jLabel68);
        jLabel68.setBounds(70, 560, 150, 23);

        jLabel69.setText("Septum Nasi :");
        jLabel69.setName("jLabel69"); // NOI18N
        FormInput.add(jLabel69);
        jLabel69.setBounds(0, 580, 162, 23);

        jLabel70.setText("Bibir :");
        jLabel70.setName("jLabel70"); // NOI18N
        FormInput.add(jLabel70);
        jLabel70.setBounds(0, 630, 123, 23);

        jLabel71.setText("Caries  :");
        jLabel71.setName("jLabel71"); // NOI18N
        FormInput.add(jLabel71);
        jLabel71.setBounds(220, 630, 63, 23);

        jLabel72.setText("Lidah  :");
        jLabel72.setName("jLabel72"); // NOI18N
        FormInput.add(jLabel72);
        jLabel72.setBounds(398, 630, 50, 23);

        jLabel73.setText("Kelenjar Gondok :");
        jLabel73.setName("jLabel73"); // NOI18N
        FormInput.add(jLabel73);
        jLabel73.setBounds(381, 680, 120, 23);

        Bibir.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lembab", "Kering" }));
        Bibir.setName("Bibir"); // NOI18N
        Bibir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BibirKeyPressed(evt);
            }
        });
        FormInput.add(Bibir);
        Bibir.setBounds(127, 630, 88, 23);

        Caries.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        Caries.setName("Caries"); // NOI18N
        Caries.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CariesKeyPressed(evt);
            }
        });
        FormInput.add(Caries);
        Caries.setBounds(287, 630, 98, 23);

        Lidah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Bersih", "Kotor", "Tremor" }));
        Lidah.setName("Lidah"); // NOI18N
        Lidah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LidahKeyPressed(evt);
            }
        });
        FormInput.add(Lidah);
        Lidah.setBounds(452, 630, 85, 23);

        Faring.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Hiperemis" }));
        Faring.setName("Faring"); // NOI18N
        Faring.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                FaringKeyPressed(evt);
            }
        });
        FormInput.add(Faring);
        Faring.setBounds(610, 630, 100, 23);

        Tonsil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "T1-T1", "T2-T2", "T3-T3", "T4-T4", "T0-T0" }));
        Tonsil.setName("Tonsil"); // NOI18N
        Tonsil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TonsilKeyPressed(evt);
            }
        });
        FormInput.add(Tonsil);
        Tonsil.setBounds(774, 630, 80, 23);

        jLabel74.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel74.setText("5. Mulut");
        jLabel74.setName("jLabel74"); // NOI18N
        FormInput.add(jLabel74);
        jLabel74.setBounds(70, 610, 150, 23);

        jLabel75.setText("Faring  :");
        jLabel75.setName("jLabel75"); // NOI18N
        FormInput.add(jLabel75);
        jLabel75.setBounds(546, 630, 60, 23);

        jLabel76.setText("Bunyi Tambahan :");
        jLabel76.setName("jLabel76"); // NOI18N
        FormInput.add(jLabel76);
        jLabel76.setBounds(381, 780, 110, 23);

        KelenjarLimfe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        KelenjarLimfe.setName("KelenjarLimfe"); // NOI18N
        KelenjarLimfe.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelenjarLimfeKeyPressed(evt);
            }
        });
        FormInput.add(KelenjarLimfe);
        KelenjarLimfe.setBounds(175, 680, 140, 23);

        KelenjarGondok.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        KelenjarGondok.setName("KelenjarGondok"); // NOI18N
        KelenjarGondok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelenjarGondokKeyPressed(evt);
            }
        });
        FormInput.add(KelenjarGondok);
        KelenjarGondok.setBounds(505, 680, 140, 23);

        jLabel77.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel77.setText("1. Muka");
        jLabel77.setName("jLabel77"); // NOI18N
        FormInput.add(jLabel77);
        jLabel77.setBounds(70, 380, 150, 23);

        jLabel78.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel78.setText("6. Leher");
        jLabel78.setName("jLabel78"); // NOI18N
        FormInput.add(jLabel78);
        jLabel78.setBounds(70, 660, 150, 23);

        jLabel79.setText("Dada :");
        jLabel79.setName("jLabel79"); // NOI18N
        FormInput.add(jLabel79);
        jLabel79.setBounds(0, 710, 76, 23);

        jLabel80.setText("Kelenjar Limfe :");
        jLabel80.setName("jLabel80"); // NOI18N
        FormInput.add(jLabel80);
        jLabel80.setBounds(0, 680, 171, 23);

        jLabel81.setText("Bunyi Jantung :");
        jLabel81.setName("jLabel81"); // NOI18N
        FormInput.add(jLabel81);
        jLabel81.setBounds(381, 830, 110, 23);

        jLabel82.setText("Vocal Fremitus :");
        jLabel82.setName("jLabel82"); // NOI18N
        FormInput.add(jLabel82);
        jLabel82.setBounds(381, 750, 110, 23);

        jLabel83.setText("Perkusi :");
        jLabel83.setName("jLabel83"); // NOI18N
        FormInput.add(jLabel83);
        jLabel83.setBounds(652, 750, 100, 23);

        jLabel84.setText("Bunyi Napas :");
        jLabel84.setName("jLabel84"); // NOI18N
        FormInput.add(jLabel84);
        jLabel84.setBounds(0, 780, 169, 23);

        GerakanDada.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Simetris", "Tidak Simetris" }));
        GerakanDada.setName("GerakanDada"); // NOI18N
        GerakanDada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                GerakanDadaKeyPressed(evt);
            }
        });
        FormInput.add(GerakanDada);
        GerakanDada.setBounds(173, 750, 129, 23);

        BunyiNapas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Vesikuler", "Bronkhial", "Trakeal" }));
        BunyiNapas.setName("BunyiNapas"); // NOI18N
        BunyiNapas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BunyiNapasKeyPressed(evt);
            }
        });
        FormInput.add(BunyiNapas);
        BunyiNapas.setBounds(173, 780, 129, 23);

        VocalFremitus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Sama", "Tidak Sama" }));
        VocalFremitus.setName("VocalFremitus"); // NOI18N
        VocalFremitus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                VocalFremitusKeyPressed(evt);
            }
        });
        FormInput.add(VocalFremitus);
        VocalFremitus.setBounds(495, 750, 116, 23);

        BunyiTambahan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Wheezing", "Tronkhi" }));
        BunyiTambahan.setName("BunyiTambahan"); // NOI18N
        BunyiTambahan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BunyiTambahanKeyPressed(evt);
            }
        });
        FormInput.add(BunyiTambahan);
        BunyiTambahan.setBounds(495, 780, 116, 23);

        PerkusiDada.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Sonor", "Pekak" }));
        PerkusiDada.setName("PerkusiDada"); // NOI18N
        PerkusiDada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PerkusiDadaKeyPressed(evt);
            }
        });
        FormInput.add(PerkusiDada);
        PerkusiDada.setBounds(756, 750, 98, 23);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel44.setText("1. Paru");
        jLabel44.setName("jLabel44"); // NOI18N
        FormInput.add(jLabel44);
        jLabel44.setBounds(70, 730, 100, 23);

        jLabel85.setText("Gerakan Dada :");
        jLabel85.setName("jLabel85"); // NOI18N
        FormInput.add(jLabel85);
        jLabel85.setBounds(0, 750, 169, 23);

        jLabel86.setText("Hepar :");
        jLabel86.setName("jLabel86"); // NOI18N
        FormInput.add(jLabel86);
        jLabel86.setBounds(630, 880, 90, 23);

        jLabel87.setText("Batas :");
        jLabel87.setName("jLabel87"); // NOI18N
        FormInput.add(jLabel87);
        jLabel87.setBounds(652, 830, 100, 23);

        jLabel88.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel88.setText("2. Jantung");
        jLabel88.setName("jLabel88"); // NOI18N
        FormInput.add(jLabel88);
        jLabel88.setBounds(70, 810, 100, 23);

        jLabel89.setText("Ictus Cordis :");
        jLabel89.setName("jLabel89"); // NOI18N
        FormInput.add(jLabel89);
        jLabel89.setBounds(0, 830, 159, 23);

        jLabel90.setText("Inspeksi :");
        jLabel90.setName("jLabel90"); // NOI18N
        FormInput.add(jLabel90);
        jLabel90.setBounds(0, 880, 141, 23);

        jLabel91.setText("Palpasi :");
        jLabel91.setName("jLabel91"); // NOI18N
        FormInput.add(jLabel91);
        jLabel91.setBounds(330, 880, 90, 23);

        jLabel92.setText("Limpa :");
        jLabel92.setName("jLabel92"); // NOI18N
        FormInput.add(jLabel92);
        jLabel92.setBounds(630, 910, 90, 23);

        jLabel93.setText("Ekstremitas Bawah :");
        jLabel93.setName("jLabel93"); // NOI18N
        FormInput.add(jLabel93);
        jLabel93.setBounds(465, 1010, 110, 23);

        jLabel94.setText("Perkusi :");
        jLabel94.setName("jLabel94"); // NOI18N
        FormInput.add(jLabel94);
        jLabel94.setBounds(0, 910, 141, 23);

        jLabel95.setText("Abdomen :");
        jLabel95.setName("jLabel95"); // NOI18N
        FormInput.add(jLabel95);
        jLabel95.setBounds(0, 860, 97, 23);

        jLabel96.setText("Auskultasi :");
        jLabel96.setName("jLabel96"); // NOI18N
        FormInput.add(jLabel96);
        jLabel96.setBounds(330, 910, 90, 23);

        jLabel97.setText("Nyeri Ketok Costovertebral Angle/CVA :");
        jLabel97.setName("jLabel97"); // NOI18N
        FormInput.add(jLabel97);
        jLabel97.setBounds(0, 960, 287, 23);

        jLabel98.setText("Punggung :");
        jLabel98.setName("jLabel98"); // NOI18N
        FormInput.add(jLabel98);
        jLabel98.setBounds(0, 940, 99, 23);

        jLabel99.setText("Kelainan");
        jLabel99.setName("jLabel99"); // NOI18N
        FormInput.add(jLabel99);
        jLabel99.setBounds(0, 1040, 120, 23);

        jLabel100.setText("Anggota Gerak :");
        jLabel100.setName("jLabel100"); // NOI18N
        FormInput.add(jLabel100);
        jLabel100.setBounds(0, 990, 123, 23);

        IctusCordis.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Terlihat", "Terlihat", "Teraba", "Tidak Teraba" }));
        IctusCordis.setName("IctusCordis"); // NOI18N
        IctusCordis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                IctusCordisKeyPressed(evt);
            }
        });
        FormInput.add(IctusCordis);
        IctusCordis.setBounds(163, 830, 139, 23);

        BunyiJantung.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Reguler", "Irreguler", "Korotkoff I, II", "Gallop", "Lain-lain" }));
        BunyiJantung.setName("BunyiJantung"); // NOI18N
        BunyiJantung.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BunyiJantungKeyPressed(evt);
            }
        });
        FormInput.add(BunyiJantung);
        BunyiJantung.setBounds(495, 830, 116, 23);

        Batas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Melebar" }));
        Batas.setName("Batas"); // NOI18N
        Batas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BatasKeyPressed(evt);
            }
        });
        FormInput.add(Batas);
        Batas.setBounds(756, 830, 98, 23);

        Inspeksi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Datar", "Cembung" }));
        Inspeksi.setName("Inspeksi"); // NOI18N
        Inspeksi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                InspeksiKeyPressed(evt);
            }
        });
        FormInput.add(Inspeksi);
        Inspeksi.setBounds(145, 880, 130, 23);

        Palpasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Supel", "Tegang (Defans Muscular)" }));
        Palpasi.setName("Palpasi"); // NOI18N
        Palpasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PalpasiKeyPressed(evt);
            }
        });
        FormInput.add(Palpasi);
        Palpasi.setBounds(424, 880, 187, 23);

        PerkusiAbdomen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Timpani", "Hipertimpani" }));
        PerkusiAbdomen.setName("PerkusiAbdomen"); // NOI18N
        PerkusiAbdomen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PerkusiAbdomenKeyPressed(evt);
            }
        });
        FormInput.add(PerkusiAbdomen);
        PerkusiAbdomen.setBounds(145, 910, 130, 23);

        Auskultasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Meningkat (>4x/menit)" }));
        Auskultasi.setName("Auskultasi"); // NOI18N
        Auskultasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AuskultasiKeyPressed(evt);
            }
        });
        FormInput.add(Auskultasi);
        Auskultasi.setBounds(424, 910, 187, 23);

        Hepar.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Hepar.setName("Hepar"); // NOI18N
        Hepar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HeparKeyPressed(evt);
            }
        });
        FormInput.add(Hepar);
        Hepar.setBounds(724, 880, 130, 23);

        Limpa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Limpa.setName("Limpa"); // NOI18N
        Limpa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LimpaKeyPressed(evt);
            }
        });
        FormInput.add(Limpa);
        Limpa.setBounds(724, 910, 130, 23);

        NyeriKtok.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        NyeriKtok.setName("NyeriKtok"); // NOI18N
        NyeriKtok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NyeriKtokKeyPressed(evt);
            }
        });
        FormInput.add(NyeriKtok);
        NyeriKtok.setBounds(291, 960, 110, 23);

        ExtremitasAtas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tidak Normal" }));
        ExtremitasAtas.setName("ExtremitasAtas"); // NOI18N
        ExtremitasAtas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ExtremitasAtasKeyPressed(evt);
            }
        });
        FormInput.add(ExtremitasAtas);
        ExtremitasAtas.setBounds(187, 1010, 117, 23);

        ExtremitasBawah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tidak Normal" }));
        ExtremitasBawah.setName("ExtremitasBawah"); // NOI18N
        ExtremitasBawah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ExtremitasBawahKeyPressed(evt);
            }
        });
        FormInput.add(ExtremitasBawah);
        ExtremitasBawah.setBounds(579, 1010, 117, 23);

        KetExtremitasAtas.setName("KetExtremitasAtas"); // NOI18N
        KetExtremitasAtas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetExtremitasAtasKeyPressed(evt);
            }
        });
        FormInput.add(KetExtremitasAtas);
        KetExtremitasAtas.setBounds(307, 1010, 155, 23);

        KetExtremitasBawah.setName("KetExtremitasBawah"); // NOI18N
        KetExtremitasBawah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetExtremitasBawahKeyPressed(evt);
            }
        });
        FormInput.add(KetExtremitasBawah);
        KetExtremitasBawah.setBounds(699, 1010, 155, 23);

        Kulit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tato", "Penyakit Kulit" }));
        Kulit.setName("Kulit"); // NOI18N
        Kulit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KulitKeyPressed(evt);
            }
        });
        FormInput.add(Kulit);
        Kulit.setBounds(553, 960, 118, 23);

        scrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane2.setName("scrollPane2"); // NOI18N

        RiwayatPenyakitSekarang.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        RiwayatPenyakitSekarang.setColumns(20);
        RiwayatPenyakitSekarang.setRows(5);
        RiwayatPenyakitSekarang.setName("RiwayatPenyakitSekarang"); // NOI18N
        RiwayatPenyakitSekarang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatPenyakitSekarangKeyPressed(evt);
            }
        });
        scrollPane2.setViewportView(RiwayatPenyakitSekarang);

        FormInput.add(scrollPane2);
        scrollPane2.setBounds(44, 115, 260, 48);

        scrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane3.setName("scrollPane3"); // NOI18N

        RiwayatPenyakitKeluarga.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        RiwayatPenyakitKeluarga.setColumns(20);
        RiwayatPenyakitKeluarga.setRows(5);
        RiwayatPenyakitKeluarga.setName("RiwayatPenyakitKeluarga"); // NOI18N
        RiwayatPenyakitKeluarga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatPenyakitKeluargaKeyPressed(evt);
            }
        });
        scrollPane3.setViewportView(RiwayatPenyakitKeluarga);

        FormInput.add(scrollPane3);
        scrollPane3.setBounds(319, 115, 260, 48);

        scrollPane4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane4.setName("scrollPane4"); // NOI18N

        RiwayatPenyakitDahulu.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        RiwayatPenyakitDahulu.setColumns(20);
        RiwayatPenyakitDahulu.setRows(5);
        RiwayatPenyakitDahulu.setName("RiwayatPenyakitDahulu"); // NOI18N
        RiwayatPenyakitDahulu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatPenyakitDahuluKeyPressed(evt);
            }
        });
        scrollPane4.setViewportView(RiwayatPenyakitDahulu);

        FormInput.add(scrollPane4);
        scrollPane4.setBounds(594, 115, 260, 48);

        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel24.setText("cm");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(355, 250, 30, 23);

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel13.setText("Kg");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput.add(jLabel13);
        jLabel13.setBounds(525, 250, 20, 23);

        jSeparator3.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator3.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator3.setName("jSeparator3"); // NOI18N
        FormInput.add(jSeparator3);
        jSeparator3.setBounds(0, 1040, 880, 1);

        jSeparator4.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator4.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator4.setName("jSeparator4"); // NOI18N
        FormInput.add(jSeparator4);
        jSeparator4.setBounds(0, 1420, 880, 1);

        jSeparator5.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator5.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator5.setName("jSeparator5"); // NOI18N
        FormInput.add(jSeparator5);
        jSeparator5.setBounds(0, 1500, 880, 1);

        jSeparator6.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator6.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator6.setName("jSeparator6"); // NOI18N
        FormInput.add(jSeparator6);
        jSeparator6.setBounds(0, 1900, 880, 1);

        Merokok.setName("Merokok"); // NOI18N
        Merokok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MerokokKeyPressed(evt);
            }
        });
        FormInput.add(Merokok);
        Merokok.setBounds(95, 1920, 344, 23);

        Alkohol.setName("Alkohol"); // NOI18N
        Alkohol.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AlkoholKeyPressed(evt);
            }
        });
        FormInput.add(Alkohol);
        Alkohol.setBounds(510, 1920, 344, 23);

        jSeparator7.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator7.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator7.setName("jSeparator7"); // NOI18N
        FormInput.add(jSeparator7);
        jSeparator7.setBounds(0, 1950, 880, 1);

        jSeparator8.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator8.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator8.setName("jSeparator8"); // NOI18N
        FormInput.add(jSeparator8);
        jSeparator8.setBounds(0, 2030, 880, 1);

        jSeparator9.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator9.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator9.setName("jSeparator9"); // NOI18N
        FormInput.add(jSeparator9);
        jSeparator9.setBounds(44, 280, 836, 1);

        jSeparator10.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator10.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator10.setName("jSeparator10"); // NOI18N
        FormInput.add(jSeparator10);
        jSeparator10.setBounds(44, 360, 836, 1);

        jSeparator11.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator11.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator11.setName("jSeparator11"); // NOI18N
        FormInput.add(jSeparator11);
        jSeparator11.setBounds(44, 710, 836, 1);

        jSeparator12.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator12.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator12.setName("jSeparator12"); // NOI18N
        FormInput.add(jSeparator12);
        jSeparator12.setBounds(44, 860, 836, 1);

        jSeparator13.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator13.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator13.setName("jSeparator13"); // NOI18N
        FormInput.add(jSeparator13);
        jSeparator13.setBounds(44, 940, 836, 1);

        jSeparator14.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator14.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator14.setName("jSeparator14"); // NOI18N
        FormInput.add(jSeparator14);
        jSeparator14.setBounds(44, 990, 836, 1);

        jLabel106.setText("Kondisi Kulit :");
        jLabel106.setName("jLabel106"); // NOI18N
        FormInput.add(jLabel106);
        jLabel106.setBounds(459, 960, 90, 23);

        jSeparator15.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator15.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator15.setName("jSeparator15"); // NOI18N
        FormInput.add(jSeparator15);
        jSeparator15.setBounds(0, 1580, 880, 1);

        jLabel107.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel107.setText("F. SPIROMETRI");
        jLabel107.setName("jLabel107"); // NOI18N
        FormInput.add(jLabel107);
        jLabel107.setBounds(10, 1580, 182, 23);

        scrollPane13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane13.setName("scrollPane13"); // NOI18N
        scrollPane13.setPreferredSize(new java.awt.Dimension(199, 52));

        Spirometri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Spirometri.setColumns(20);
        Spirometri.setRows(5);
        Spirometri.setName("Spirometri"); // NOI18N
        Spirometri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SpirometriKeyPressed(evt);
            }
        });
        scrollPane13.setViewportView(Spirometri);

        FormInput.add(scrollPane13);
        scrollPane13.setBounds(44, 1600, 810, 53);

        jSeparator16.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator16.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator16.setName("jSeparator16"); // NOI18N
        FormInput.add(jSeparator16);
        jSeparator16.setBounds(0, 1660, 880, 1);

        jLabel108.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel108.setText("G. AUDIOMETRI");
        jLabel108.setName("jLabel108"); // NOI18N
        FormInput.add(jLabel108);
        jLabel108.setBounds(10, 1660, 182, 23);

        scrollPane16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane16.setName("scrollPane16"); // NOI18N
        scrollPane16.setPreferredSize(new java.awt.Dimension(199, 52));

        Audiometri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Audiometri.setColumns(20);
        Audiometri.setRows(5);
        Audiometri.setName("Audiometri"); // NOI18N
        Audiometri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AudiometriKeyPressed(evt);
            }
        });
        scrollPane16.setViewportView(Audiometri);

        FormInput.add(scrollPane16);
        scrollPane16.setBounds(44, 1680, 810, 53);

        jSeparator17.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator17.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator17.setName("jSeparator17"); // NOI18N
        FormInput.add(jSeparator17);
        jSeparator17.setBounds(0, 1740, 880, 1);

        jLabel109.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel109.setText("H. TREADMILL");
        jLabel109.setName("jLabel109"); // NOI18N
        FormInput.add(jLabel109);
        jLabel109.setBounds(10, 1740, 182, 23);

        scrollPane17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane17.setName("scrollPane17"); // NOI18N
        scrollPane17.setPreferredSize(new java.awt.Dimension(199, 52));

        Treadmill.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Treadmill.setColumns(20);
        Treadmill.setRows(5);
        Treadmill.setName("Treadmill"); // NOI18N
        Treadmill.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TreadmillKeyPressed(evt);
            }
        });
        scrollPane17.setViewportView(Treadmill);

        FormInput.add(scrollPane17);
        scrollPane17.setBounds(44, 1760, 810, 53);

        jSeparator18.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator18.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator18.setName("jSeparator18"); // NOI18N
        FormInput.add(jSeparator18);
        jSeparator18.setBounds(0, 1820, 880, 1);

        jLabel110.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel110.setText("I. LAIN-LAIN");
        jLabel110.setName("jLabel110"); // NOI18N
        FormInput.add(jLabel110);
        jLabel110.setBounds(10, 1820, 182, 23);

        scrollPane18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane18.setName("scrollPane18"); // NOI18N
        scrollPane18.setPreferredSize(new java.awt.Dimension(199, 52));

        Lainlain.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Lainlain.setColumns(20);
        Lainlain.setRows(5);
        Lainlain.setName("Lainlain"); // NOI18N
        Lainlain.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LainlainKeyPressed(evt);
            }
        });
        scrollPane18.setViewportView(Lainlain);

        FormInput.add(scrollPane18);
        scrollPane18.setBounds(44, 1840, 810, 53);

        jLabel27.setText("Suhu :");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(570, 250, 40, 23);

        LingkarPerut.setFocusTraversalPolicyProvider(true);
        LingkarPerut.setName("LingkarPerut"); // NOI18N
        LingkarPerut.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LingkarPerutKeyPressed(evt);
            }
        });
        FormInput.add(LingkarPerut);
        LingkarPerut.setBounds(790, 250, 50, 23);

        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel40.setText("cm");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(850, 250, 20, 23);

        jLabel42.setText("Nyeri Tekan Sinus Frontalis :");
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(290, 400, 140, 23);

        jSeparator19.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator19.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator19.setName("jSeparator19"); // NOI18N
        FormInput.add(jSeparator19);
        jSeparator19.setBounds(0, 1340, 880, 1);

        jLabel111.setText("Ekstremitas Atas :");
        jLabel111.setName("jLabel111"); // NOI18N
        FormInput.add(jLabel111);
        jLabel111.setBounds(0, 1010, 183, 23);

        jLabel112.setText("Kelainan Kepala :");
        jLabel112.setName("jLabel112"); // NOI18N
        FormInput.add(jLabel112);
        jLabel112.setBounds(0, 1070, 183, 23);

        jLabel113.setText("Kelainan Mata :");
        jLabel113.setName("jLabel113"); // NOI18N
        FormInput.add(jLabel113);
        jLabel113.setBounds(0, 1100, 183, 23);

        KelainanKepala.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada Kelainan", "Ada Kelainan" }));
        KelainanKepala.setName("KelainanKepala"); // NOI18N
        KelainanKepala.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelainanKepalaKeyPressed(evt);
            }
        });
        FormInput.add(KelainanKepala);
        KelainanKepala.setBounds(187, 1070, 117, 23);

        KelainanMata.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada Kelainan", "Ada Kelainan" }));
        KelainanMata.setName("KelainanMata"); // NOI18N
        KelainanMata.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelainanMataKeyPressed(evt);
            }
        });
        FormInput.add(KelainanMata);
        KelainanMata.setBounds(187, 1100, 117, 23);

        jLabel114.setText("Kelainan Telinga :");
        jLabel114.setName("jLabel114"); // NOI18N
        FormInput.add(jLabel114);
        jLabel114.setBounds(0, 1130, 183, 23);

        KelainanTelinga.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada Kelainan", "Ada Kelainan" }));
        KelainanTelinga.setName("KelainanTelinga"); // NOI18N
        KelainanTelinga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelainanTelingaKeyPressed(evt);
            }
        });
        FormInput.add(KelainanTelinga);
        KelainanTelinga.setBounds(187, 1130, 117, 23);

        jLabel115.setText("Kelainan Hidung :");
        jLabel115.setName("jLabel115"); // NOI18N
        FormInput.add(jLabel115);
        jLabel115.setBounds(0, 1160, 183, 23);

        KelainanHidung.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada Kelainan", "Ada Kelainan" }));
        KelainanHidung.setName("KelainanHidung"); // NOI18N
        KelainanHidung.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelainanHidungKeyPressed(evt);
            }
        });
        FormInput.add(KelainanHidung);
        KelainanHidung.setBounds(187, 1160, 117, 23);

        jLabel116.setText("Kelainan Leher :");
        jLabel116.setName("jLabel116"); // NOI18N
        FormInput.add(jLabel116);
        jLabel116.setBounds(0, 1190, 183, 23);

        jLabel117.setText("Kelainan Dada :");
        jLabel117.setName("jLabel117"); // NOI18N
        FormInput.add(jLabel117);
        jLabel117.setBounds(0, 1220, 183, 23);

        jLabel118.setText("Kelainan Abdomen :");
        jLabel118.setName("jLabel118"); // NOI18N
        FormInput.add(jLabel118);
        jLabel118.setBounds(0, 1250, 183, 23);

        jLabel119.setText("Kelainan Ekstrimitas :");
        jLabel119.setName("jLabel119"); // NOI18N
        FormInput.add(jLabel119);
        jLabel119.setBounds(0, 1280, 183, 23);

        KelainanLeher.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada Kelainan", "Ada Kelainan" }));
        KelainanLeher.setName("KelainanLeher"); // NOI18N
        KelainanLeher.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelainanLeherKeyPressed(evt);
            }
        });
        FormInput.add(KelainanLeher);
        KelainanLeher.setBounds(187, 1190, 117, 23);

        KelainanDada.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada Kelainan", "Ada Kelainan" }));
        KelainanDada.setName("KelainanDada"); // NOI18N
        KelainanDada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelainanDadaKeyPressed(evt);
            }
        });
        FormInput.add(KelainanDada);
        KelainanDada.setBounds(187, 1220, 117, 23);

        KelainanAbdomen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada Kelainan", "Ada Kelainan" }));
        KelainanAbdomen.setName("KelainanAbdomen"); // NOI18N
        KelainanAbdomen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelainanAbdomenKeyPressed(evt);
            }
        });
        FormInput.add(KelainanAbdomen);
        KelainanAbdomen.setBounds(187, 1250, 117, 23);

        KelainanEkstrimitas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada Kelainan", "Ada Kelainan" }));
        KelainanEkstrimitas.setName("KelainanEkstrimitas"); // NOI18N
        KelainanEkstrimitas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KelainanEkstrimitasKeyPressed(evt);
            }
        });
        FormInput.add(KelainanEkstrimitas);
        KelainanEkstrimitas.setBounds(187, 1280, 117, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Penilaian", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Regis :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "24-01-2025" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "24-01-2025" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Penilaian", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{            
            Valid.pindah(evt,TCari,BtnDokter);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else{
           if(Sequel.menyimpantf("penilaian_mcu","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",84,new String[]{
                    TNoRw.getText(),Valid.SetTgl(TglAsuhan.getSelectedItem()+"")+" "+TglAsuhan.getSelectedItem().toString().substring(11,19),KdDokter.getText(),Informasi.getSelectedItem().toString(),RiwayatPenyakitSekarang.getText(),RiwayatPenyakitKeluarga.getText(), 
                    RiwayatPenyakitDahulu.getText(),RiwayatAlergiMakanan.getText(),KeadaanUmum.getSelectedItem().toString(),Kesadaran.getSelectedItem().toString(),TD.getText(),Nadi.getText(),RR.getText(),TinggiBadan.getText(),BeratBadan.getText(),Suhu.getText(),
                    Submandibula.getSelectedItem().toString(),Axila.getSelectedItem().toString(),Supraklavikula.getSelectedItem().toString(),Leher.getSelectedItem().toString(),Inguinal.getSelectedItem().toString(),Oedema.getSelectedItem().toString(),
                    NyeriTekanSinusFrontalis.getSelectedItem().toString(),NyeriTekananSinusMaxilaris.getSelectedItem().toString(),Palpebra.getSelectedItem().toString(),Sklera.getSelectedItem().toString(),Cornea.getSelectedItem().toString(),
                    TestButaWarna.getSelectedItem().toString(),Konjungtiva.getSelectedItem().toString(),Lensa.getSelectedItem().toString(),Pupil.getSelectedItem().toString(),LubangTelinga.getSelectedItem().toString(),DaunTelinga.getSelectedItem().toString(), 
                    SelaputPendengaran.getSelectedItem().toString(),NyeriMastoideus.getSelectedItem().toString(),SeptumNasi.getSelectedItem().toString(),LubangHidung.getSelectedItem().toString(),Bibir.getSelectedItem().toString(),Caries.getSelectedItem().toString(), 
                    Lidah.getSelectedItem().toString(),Faring.getSelectedItem().toString(),Tonsil.getSelectedItem().toString(),KelenjarLimfe.getSelectedItem().toString(),KelenjarGondok.getSelectedItem().toString(),GerakanDada.getSelectedItem().toString(), 
                    VocalFremitus.getSelectedItem().toString(),PerkusiDada.getSelectedItem().toString(),BunyiNapas.getSelectedItem().toString(),BunyiTambahan.getSelectedItem().toString(),IctusCordis.getSelectedItem().toString(), 
                    BunyiJantung.getSelectedItem().toString(),Batas.getSelectedItem().toString(),Inspeksi.getSelectedItem().toString(),Palpasi.getSelectedItem().toString(),Hepar.getSelectedItem().toString(),PerkusiAbdomen.getSelectedItem().toString(),
                    Auskultasi.getSelectedItem().toString(),Limpa.getSelectedItem().toString(),NyeriKtok.getSelectedItem().toString(),Kulit.getSelectedItem().toString(),ExtremitasAtas.getSelectedItem().toString(),KetExtremitasAtas.getText(), 
                    ExtremitasBawah.getSelectedItem().toString(),KetExtremitasBawah.getText(),PemeriksaanLaboratorium.getText(),RongsenThorax.getText(),EKG.getText(),Spirometri.getText(),Audiometri.getText(),Treadmill.getText(),Lainlain.getText(),
                    Merokok.getText(),Alkohol.getText(),Kesimpulan.getText(),Anjuran.getText()
                    //START CUSTOM
                    ,LingkarPerut.getText(),KelainanKepala.getSelectedItem().toString(),KelainanMata.getSelectedItem().toString(),
                    KelainanTelinga.getSelectedItem().toString(),KelainanHidung.getSelectedItem().toString(),KelainanLeher.getSelectedItem().toString(),
                    KelainanDada.getSelectedItem().toString(),KelainanAbdomen.getSelectedItem().toString(),KelainanEkstrimitas.getSelectedItem().toString()
                   //END CUSTOM
                })==true){
                    emptTeks();
            }
        }
    
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,Anjuran,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),78).toString())){   //CUSTOM
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh dokter yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),78).toString())){
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh dokter yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }   
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
         this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
            BtnBatal.requestFocus();
        }else if(tabMode.getRowCount()!=0){
            try{
                Map<String, Object> param = new HashMap<>();
                param.put("namars",akses.getnamars());
                param.put("alamatrs",akses.getalamatrs());
                param.put("kotars",akses.getkabupatenrs());
                param.put("propinsirs",akses.getpropinsirs());
                param.put("kontakrs",akses.getkontakrs());
                param.put("emailrs",akses.getemailrs());
                //start CUSTOM
                param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
                param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));                
                
                if(Valid.SetTgl(DTPCari1.getSelectedItem()+"").equals(Valid.SetTgl(DTPCari2.getSelectedItem()+""))){
                    param.put("tanggal_pemeriksaan","Tanggal Pemeriksaan : "+DTPCari1.getSelectedItem() );
                }else{
                    param.put("tanggal_pemeriksaan","Tanggal Pemeriksaan : "+DTPCari1.getSelectedItem()+" s.d "+DTPCari2.getSelectedItem());
                }
                //UJI KEBUGARAN
                if(Sequel.cariInteger("select count(no_rawat) from penilaian_ujikebugaran where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                    uji_kebugaran=Sequel.cariIsi("SELECT CONCAT('Berdasarkan hasil uji kebugaran yang dilakukan dengan menggunakan protokol Six Minute Walking Test, maka dapat disimpulkan hasil kebugaran: ', hasil_kesimpulan, '\n\nRekomendasi: ', rekomendasi, '\n\nCatatan dokter: ', catatan) as uji_kebugaran " + 
                                    "FROM penilaian_ujikebugaran " + 
                                    "WHERE no_rawat='" + tbObat.getValueAt(tbObat.getSelectedRow(), 0).toString() + "'");
                    dokter_uji_kebugaran=Sequel.cariIsi("select nm_dokter from penilaian_ujikebugaran inner join dokter d on penilaian_ujikebugaran.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());  
                }else{
                    if(Sequel.cariInteger("select count(no_rawat) from penilaian_ujikebugaran_ymca where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                        uji_kebugaran=Sequel.cariIsi("SELECT CONCAT('Berdasarkan hasil uji kebugaran yang dilakukan dengan menggunakan protokol YMCA Test, maka dapat disimpulkan hasil kebugaran: ', hasil_kesimpulan, '\n\nRekomendasi: ', rekomendasi, '\n\nCatatan dokter: ', catatan) as uji_kebugaran " + 
                                        "FROM penilaian_ujikebugaran_ymca " + 
                                        "WHERE no_rawat='" + tbObat.getValueAt(tbObat.getSelectedRow(), 0).toString() + "'");
                        dokter_uji_kebugaran=Sequel.cariIsi("select nm_dokter from penilaian_ujikebugaran_ymca inner join dokter d on penilaian_ujikebugaran_ymca.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());  
                    }else{
                        uji_kebugaran="";
                        dokter_uji_kebugaran="";                    
                    }   
                }
                param.put("uji_kebugaran",uji_kebugaran);                
//                Valid.MyReportqry("rptCetakPenilaianAwalMCUNEW.jasper","report","::[ Data Registrasi Periksa ]::",
//                    "select p.no_rawat,rp.no_rkm_medis,p2.nm_pasien,rp.umurdaftar,rp.sttsumur,if(p2.jk='L','Laki-Laki','Perempuan') as jeniskelamin,p2.jk,p2.alamat,p.ekg," +
//                    "p.laborat,p.radiologi,p.kesimpulan,p.anjuran,p.lainlain as visus,p.treadmill,p.audiometri " +
//                    "from penilaian_mcu p " +
//                    "inner join reg_periksa rp on p.no_rawat = rp.no_rawat " +
//                    "inner join pasien p2 on rp.no_rkm_medis = p2.no_rkm_medis "+   
//                    "where rp.tgl_registrasi between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' "+
//                    "and (p.no_rawat like '%"+TCari.getText()+"%' or rp.no_rkm_medis like '%"+TCari.getText()+"%' or p2.nm_pasien like '%"+TCari.getText()+"%' or p.rps like '%"+TCari.getText()+"%' or p.kesimpulan like '%"+TCari.getText()+"%') order by p2.nm_pasien asc ",param);
                Valid.MyReportqry("rptCetakPenilaianAwalMCUNEW.jasper","report","::[ Data Registrasi Periksa ]::",
                    "select p.no_rawat,rp.no_rkm_medis,p2.nm_pasien,rp.umurdaftar,rp.sttsumur,if(p2.jk='L','Laki-Laki','Perempuan') as jeniskelamin,p2.jk,p2.alamat,p.ekg," +
                    "p.laborat,p.radiologi,p.kesimpulan,p.anjuran,p.lainlain as visus,p.treadmill,p.audiometri " +
                    ",p3.tb,p3.bb,p.lingkar_perut,p3.td,"+
                    "IF(p3.tb > 0, ROUND((p3.bb / (POW(p3.tb / 100, 2))), 2), NULL) AS imt,"+
                    "CASE WHEN ROUND((p3.bb / (POW(p3.tb / 100, 2))), 2) < 17 THEN 'Kurus Berat'\n" +
                    "WHEN ROUND((p3.bb / (POW(p3.tb / 100, 2))), 2) BETWEEN 17 AND 18.4 THEN 'Kurus Ringan'\n" +
                    "WHEN ROUND((p3.bb / (POW(p3.tb / 100, 2))), 2) BETWEEN 18.5 AND 25.0 THEN 'Normal'\n" +
                    "WHEN ROUND((p3.bb / (POW(p3.tb / 100, 2))), 2) BETWEEN 25.1 AND 27.0 THEN 'Gemuk Ringan'\n" +
                    "WHEN ROUND((p3.bb / (POW(p3.tb / 100, 2))), 2) > 27 THEN 'Gemuk Berat'\n" +
                    "ELSE 'Tidak Diketahui'\n" +
                    "END AS klasifikasi_imt,"+       
                    "(SELECT nilai FROM detail_periksa_lab WHERE detail_periksa_lab.no_rawat = p.no_rawat and detail_periksa_lab.kd_jenis_prw='J001993' LIMIT 1) AS glukosa,"+
                    "(SELECT nilai FROM detail_periksa_lab WHERE detail_periksa_lab.no_rawat = p.no_rawat and detail_periksa_lab.kd_jenis_prw='J001994' LIMIT 1) AS kolesterol "+        
                    "from penilaian_mcu p " +
                    "inner join reg_periksa rp on p.no_rawat = rp.no_rawat " +
                    "inner join pasien p2 on rp.no_rkm_medis = p2.no_rkm_medis "+   
                    "LEFT JOIN penilaian_awal_keperawatan_ralan p3 ON rp.no_rawat = p3.no_rawat "+        
                    "where rp.tgl_registrasi between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' "+
                    "and (p.no_rawat like '%"+TCari.getText()+"%' or rp.no_rkm_medis like '%"+TCari.getText()+"%' or p2.nm_pasien like '%"+TCari.getText()+"%' or p.rps like '%"+TCari.getText()+"%' or p.kesimpulan like '%"+TCari.getText()+"%') order by p2.nm_pasien asc ",param);
            }catch(Exception e){
                System.out.println("Notifikasi : "+e);
            }
        }
        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setAlwaysOnTop(false);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        Valid.pindah(evt,Anjuran,Informasi);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void BeratBadanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BeratBadanKeyPressed
        Valid.pindah(evt,TinggiBadan,Suhu);
    }//GEN-LAST:event_BeratBadanKeyPressed

    private void NadiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NadiKeyPressed
        Valid.pindah(evt,TD,RR);
    }//GEN-LAST:event_NadiKeyPressed

    private void SuhuKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SuhuKeyPressed
        Valid.pindah(evt,BeratBadan,Submandibula);
    }//GEN-LAST:event_SuhuKeyPressed

    private void TDKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TDKeyPressed
        Valid.pindah(evt,Kesadaran,Nadi);
    }//GEN-LAST:event_TDKeyPressed

    private void RRKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RRKeyPressed
        Valid.pindah(evt,Nadi,TinggiBadan);
    }//GEN-LAST:event_RRKeyPressed

    private void InformasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_InformasiKeyPressed
        Valid.pindah(evt,TglAsuhan,RiwayatPenyakitSekarang);
    }//GEN-LAST:event_InformasiKeyPressed

    private void TglAsuhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglAsuhanKeyPressed
        Valid.pindah2(evt,KdDokter,Informasi);
    }//GEN-LAST:event_TglAsuhanKeyPressed

    private void TinggiBadanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TinggiBadanKeyPressed
        Valid.pindah(evt,RR,BeratBadan);
    }//GEN-LAST:event_TinggiBadanKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        
    }//GEN-LAST:event_formWindowOpened

    private void RiwayatAlergiMakananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatAlergiMakananKeyPressed
        Valid.pindah(evt,RiwayatPenyakitDahulu,KeadaanUmum);
    }//GEN-LAST:event_RiwayatAlergiMakananKeyPressed

    private void KeadaanUmumKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeadaanUmumKeyPressed
        Valid.pindah(evt,RiwayatAlergiMakanan,Kesadaran);
    }//GEN-LAST:event_KeadaanUmumKeyPressed

    private void PemeriksaanLaboratoriumKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PemeriksaanLaboratoriumKeyPressed
        Valid.pindah2(evt,KetExtremitasBawah,RongsenThorax);
    }//GEN-LAST:event_PemeriksaanLaboratoriumKeyPressed

    private void RongsenThoraxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RongsenThoraxKeyPressed
        Valid.pindah2(evt,PemeriksaanLaboratorium,EKG);
    }//GEN-LAST:event_RongsenThoraxKeyPressed

    private void EKGKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EKGKeyPressed
        Valid.pindah2(evt,RongsenThorax,Spirometri);
    }//GEN-LAST:event_EKGKeyPressed

    private void KesimpulanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KesimpulanKeyPressed
        Valid.pindah2(evt,Alkohol,Anjuran);
    }//GEN-LAST:event_KesimpulanKeyPressed

    private void AnjuranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AnjuranKeyPressed
        Valid.pindah2(evt,Kesimpulan,BtnSimpan);
    }//GEN-LAST:event_AnjuranKeyPressed

    private void MnPenilaianMCUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCUActionPerformed
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU.jasper","report","::[ Laporan Penilaian Awal MCU 1 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }
    }//GEN-LAST:event_MnPenilaianMCUActionPerformed

    private void RiwayatPenyakitSekarangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatPenyakitSekarangKeyPressed
        Valid.pindah2(evt,Informasi,RiwayatPenyakitKeluarga);
    }//GEN-LAST:event_RiwayatPenyakitSekarangKeyPressed

    private void RiwayatPenyakitKeluargaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatPenyakitKeluargaKeyPressed
        Valid.pindah2(evt,RiwayatPenyakitSekarang,RiwayatPenyakitDahulu);
    }//GEN-LAST:event_RiwayatPenyakitKeluargaKeyPressed

    private void RiwayatPenyakitDahuluKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatPenyakitDahuluKeyPressed
        Valid.pindah2(evt,RiwayatPenyakitKeluarga,RiwayatAlergiMakanan);
    }//GEN-LAST:event_RiwayatPenyakitDahuluKeyPressed

    private void MerokokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MerokokKeyPressed
        Valid.pindah(evt,Lainlain,Alkohol);
    }//GEN-LAST:event_MerokokKeyPressed

    private void AlkoholKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AlkoholKeyPressed
        Valid.pindah2(evt,Merokok,Kesimpulan);
    }//GEN-LAST:event_AlkoholKeyPressed

    private void KesadaranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KesadaranKeyPressed
        Valid.pindah(evt,KeadaanUmum,TD);
    }//GEN-LAST:event_KesadaranKeyPressed

    private void SubmandibulaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SubmandibulaKeyPressed
        Valid.pindah(evt,Suhu,Axila);
    }//GEN-LAST:event_SubmandibulaKeyPressed

    private void AxilaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AxilaKeyPressed
        Valid.pindah(evt,Submandibula,Supraklavikula);
    }//GEN-LAST:event_AxilaKeyPressed

    private void SupraklavikulaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SupraklavikulaKeyPressed
        Valid.pindah(evt,Axila,Leher);
    }//GEN-LAST:event_SupraklavikulaKeyPressed

    private void LeherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LeherKeyPressed
        Valid.pindah(evt,Supraklavikula,Inguinal);
    }//GEN-LAST:event_LeherKeyPressed

    private void InguinalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_InguinalKeyPressed
        Valid.pindah(evt,Leher,Oedema);
    }//GEN-LAST:event_InguinalKeyPressed

    private void OedemaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_OedemaKeyPressed
        Valid.pindah(evt,Inguinal,NyeriTekanSinusFrontalis);
    }//GEN-LAST:event_OedemaKeyPressed

    private void NyeriTekanSinusFrontalisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NyeriTekanSinusFrontalisKeyPressed
        Valid.pindah(evt,Oedema,NyeriTekananSinusMaxilaris);
    }//GEN-LAST:event_NyeriTekanSinusFrontalisKeyPressed

    private void NyeriTekananSinusMaxilarisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NyeriTekananSinusMaxilarisKeyPressed
        Valid.pindah(evt,NyeriTekanSinusFrontalis,Palpebra);
    }//GEN-LAST:event_NyeriTekananSinusMaxilarisKeyPressed

    private void PalpebraKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PalpebraKeyPressed
        Valid.pindah(evt,NyeriTekananSinusMaxilaris,Sklera);
    }//GEN-LAST:event_PalpebraKeyPressed

    private void SkleraKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SkleraKeyPressed
        Valid.pindah(evt,Palpebra,Cornea);
    }//GEN-LAST:event_SkleraKeyPressed

    private void CorneaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CorneaKeyPressed
        Valid.pindah(evt,Sklera,TestButaWarna);
    }//GEN-LAST:event_CorneaKeyPressed

    private void TestButaWarnaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TestButaWarnaKeyPressed
        Valid.pindah(evt,Cornea,Konjungtiva);
    }//GEN-LAST:event_TestButaWarnaKeyPressed

    private void KonjungtivaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KonjungtivaKeyPressed
        Valid.pindah(evt,TestButaWarna,Lensa);
    }//GEN-LAST:event_KonjungtivaKeyPressed

    private void LensaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LensaKeyPressed
        Valid.pindah(evt,Konjungtiva,Pupil);
    }//GEN-LAST:event_LensaKeyPressed

    private void PupilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PupilKeyPressed
        Valid.pindah(evt,Lensa,LubangTelinga);
    }//GEN-LAST:event_PupilKeyPressed

    private void LubangTelingaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LubangTelingaKeyPressed
        Valid.pindah(evt,Pupil,DaunTelinga);
    }//GEN-LAST:event_LubangTelingaKeyPressed

    private void DaunTelingaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DaunTelingaKeyPressed
        Valid.pindah(evt,LubangTelinga,SelaputPendengaran);
    }//GEN-LAST:event_DaunTelingaKeyPressed

    private void SelaputPendengaranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SelaputPendengaranKeyPressed
        Valid.pindah(evt,DaunTelinga,NyeriMastoideus);
    }//GEN-LAST:event_SelaputPendengaranKeyPressed

    private void NyeriMastoideusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NyeriMastoideusKeyPressed
        Valid.pindah(evt,SelaputPendengaran,SeptumNasi);
    }//GEN-LAST:event_NyeriMastoideusKeyPressed

    private void SeptumNasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SeptumNasiKeyPressed
        Valid.pindah(evt,NyeriMastoideus,LubangHidung);
    }//GEN-LAST:event_SeptumNasiKeyPressed

    private void LubangHidungKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LubangHidungKeyPressed
        Valid.pindah(evt,SeptumNasi,Bibir);
    }//GEN-LAST:event_LubangHidungKeyPressed

    private void BibirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BibirKeyPressed
        Valid.pindah(evt,LubangHidung,Caries);
    }//GEN-LAST:event_BibirKeyPressed

    private void CariesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CariesKeyPressed
        Valid.pindah(evt,Bibir,Lidah);
    }//GEN-LAST:event_CariesKeyPressed

    private void LidahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LidahKeyPressed
        Valid.pindah(evt,Caries,Faring);
    }//GEN-LAST:event_LidahKeyPressed

    private void FaringKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_FaringKeyPressed
        Valid.pindah(evt,Lidah,Tonsil);
    }//GEN-LAST:event_FaringKeyPressed

    private void TonsilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TonsilKeyPressed
        Valid.pindah(evt,Faring,KelenjarLimfe);
    }//GEN-LAST:event_TonsilKeyPressed

    private void KelenjarLimfeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelenjarLimfeKeyPressed
        Valid.pindah(evt,Tonsil,KelenjarGondok);
    }//GEN-LAST:event_KelenjarLimfeKeyPressed

    private void KelenjarGondokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelenjarGondokKeyPressed
        Valid.pindah(evt,KelenjarLimfe,GerakanDada);
    }//GEN-LAST:event_KelenjarGondokKeyPressed

    private void GerakanDadaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_GerakanDadaKeyPressed
        Valid.pindah(evt,KelenjarGondok,VocalFremitus);
    }//GEN-LAST:event_GerakanDadaKeyPressed

    private void VocalFremitusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_VocalFremitusKeyPressed
        Valid.pindah(evt,GerakanDada,PerkusiDada);
    }//GEN-LAST:event_VocalFremitusKeyPressed

    private void PerkusiDadaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PerkusiDadaKeyPressed
        Valid.pindah(evt,VocalFremitus,BunyiNapas);
    }//GEN-LAST:event_PerkusiDadaKeyPressed

    private void BunyiNapasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BunyiNapasKeyPressed
        Valid.pindah(evt,PerkusiDada,BunyiTambahan);
    }//GEN-LAST:event_BunyiNapasKeyPressed

    private void BunyiTambahanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BunyiTambahanKeyPressed
        Valid.pindah(evt,BunyiNapas,IctusCordis);
    }//GEN-LAST:event_BunyiTambahanKeyPressed

    private void IctusCordisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IctusCordisKeyPressed
        Valid.pindah(evt,BunyiTambahan,BunyiJantung);
    }//GEN-LAST:event_IctusCordisKeyPressed

    private void BunyiJantungKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BunyiJantungKeyPressed
        Valid.pindah(evt,IctusCordis,Batas);
    }//GEN-LAST:event_BunyiJantungKeyPressed

    private void BatasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BatasKeyPressed
        Valid.pindah(evt,BunyiJantung,Inspeksi);
    }//GEN-LAST:event_BatasKeyPressed

    private void InspeksiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_InspeksiKeyPressed
        Valid.pindah(evt,Batas,Palpasi);
    }//GEN-LAST:event_InspeksiKeyPressed

    private void PalpasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PalpasiKeyPressed
        Valid.pindah(evt,Inspeksi,Hepar);
    }//GEN-LAST:event_PalpasiKeyPressed

    private void HeparKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HeparKeyPressed
        Valid.pindah(evt,Palpasi,PerkusiAbdomen);
    }//GEN-LAST:event_HeparKeyPressed

    private void PerkusiAbdomenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PerkusiAbdomenKeyPressed
        Valid.pindah(evt,Hepar,Auskultasi);
    }//GEN-LAST:event_PerkusiAbdomenKeyPressed

    private void AuskultasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AuskultasiKeyPressed
        Valid.pindah(evt,PerkusiAbdomen,Limpa);
    }//GEN-LAST:event_AuskultasiKeyPressed

    private void LimpaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LimpaKeyPressed
        Valid.pindah(evt,Auskultasi,NyeriKtok);
    }//GEN-LAST:event_LimpaKeyPressed

    private void NyeriKtokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NyeriKtokKeyPressed
        Valid.pindah(evt,Limpa,Kulit);
    }//GEN-LAST:event_NyeriKtokKeyPressed

    private void KulitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KulitKeyPressed
        Valid.pindah(evt,NyeriKtok,ExtremitasAtas);
    }//GEN-LAST:event_KulitKeyPressed

    private void ExtremitasAtasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ExtremitasAtasKeyPressed
        Valid.pindah(evt,Kulit,KetExtremitasAtas);
    }//GEN-LAST:event_ExtremitasAtasKeyPressed

    private void KetExtremitasAtasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetExtremitasAtasKeyPressed
        Valid.pindah(evt,ExtremitasAtas,ExtremitasBawah);
    }//GEN-LAST:event_KetExtremitasAtasKeyPressed

    private void ExtremitasBawahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ExtremitasBawahKeyPressed
        Valid.pindah(evt,KetExtremitasAtas,KetExtremitasBawah);
    }//GEN-LAST:event_ExtremitasBawahKeyPressed

    private void KetExtremitasBawahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetExtremitasBawahKeyPressed
        Valid.pindah(evt,ExtremitasBawah,PemeriksaanLaboratorium);
    }//GEN-LAST:event_KetExtremitasBawahKeyPressed

    private void SpirometriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SpirometriKeyPressed
        Valid.pindah2(evt,EKG,Audiometri);
    }//GEN-LAST:event_SpirometriKeyPressed

    private void AudiometriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AudiometriKeyPressed
        Valid.pindah2(evt,Spirometri,Treadmill);
    }//GEN-LAST:event_AudiometriKeyPressed

    private void TreadmillKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TreadmillKeyPressed
        Valid.pindah2(evt,Audiometri,Lainlain);
    }//GEN-LAST:event_TreadmillKeyPressed

    private void LainlainKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LainlainKeyPressed
        Valid.pindah2(evt,Treadmill,Merokok);
    }//GEN-LAST:event_LainlainKeyPressed

    private void LingkarPerutKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LingkarPerutKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_LingkarPerutKeyPressed

    private void KelainanKepalaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelainanKepalaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KelainanKepalaKeyPressed

    private void KelainanMataKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelainanMataKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KelainanMataKeyPressed

    private void KelainanTelingaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelainanTelingaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KelainanTelingaKeyPressed

    private void KelainanHidungKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelainanHidungKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KelainanHidungKeyPressed

    private void KelainanLeherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelainanLeherKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KelainanLeherKeyPressed

    private void KelainanDadaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelainanDadaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KelainanDadaKeyPressed

    private void KelainanAbdomenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelainanAbdomenKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KelainanAbdomenKeyPressed

    private void KelainanEkstrimitasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KelainanEkstrimitasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KelainanEkstrimitasKeyPressed

    private void MnPenilaianMCU2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU2ActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU2.jasper","report","::[ Laporan Penilaian Awal MCU 2 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }        
    }//GEN-LAST:event_MnPenilaianMCU2ActionPerformed

    private void BtnVisusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVisusActionPerformed
        // TODO add your handling code here:
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else{
            if(TabRawat.getSelectedIndex()==0){
                Lainlain.setText("Visus Dasar Mata Kanan: "+Sequel.cariIsi("select visus_dasar_kanan from penilaian_mcu_mata where no_rawat=?",TNoRw.getText())
                        +"\nVisus Dasar Mata Kiri: "+Sequel.cariIsi("select visus_dasar_kiri from penilaian_mcu_mata where no_rawat=?",TNoRw.getText())
                        +"\nVisus Koreksi Mata Kanan: "+Sequel.cariIsi("select visus_koreksi_kanan from penilaian_mcu_mata where no_rawat=?",TNoRw.getText())
                        +"\nVisus Koreksi Mata Kiri: "+Sequel.cariIsi("select visus_koreksi_kiri from penilaian_mcu_mata where no_rawat=?",TNoRw.getText())
                        +"\nPemeriksaan Mata Bagian Luar: "+Sequel.cariIsi("select pemeriksaan_mata_bagian_luar from penilaian_mcu_mata where no_rawat=?",TNoRw.getText())
                        +"\nTes Buta Warna: "+Sequel.cariIsi("select tes_buta_warna from penilaian_mcu_mata where no_rawat=?",TNoRw.getText())
                        +"\nKeterangan: "+Sequel.cariIsi("select keterangan from penilaian_mcu_mata where no_rawat=?",TNoRw.getText())
                );
                Audiometri.setText("Keluhan: "+Sequel.cariIsi("select keluhan from penilaian_mcu_tht where no_rawat=?",TNoRw.getText())
                        +"\nKesan: "+Sequel.cariIsi("select kesan from penilaian_mcu_tht where no_rawat=?",TNoRw.getText())
                        +"\nSaran: "+Sequel.cariIsi("select saran from penilaian_mcu_tht where no_rawat=?",TNoRw.getText()));
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan kembali ke tab pengisian data terlebih dahulu..!!");
            }   
        }
    }//GEN-LAST:event_BtnVisusActionPerformed

    private void BtnVisusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnVisusKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnVisusKeyPressed

    private void MnPenilaianMCU3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU3ActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU3.jasper","report","::[ Laporan Penilaian Awal MCU 3 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }               
    }//GEN-LAST:event_MnPenilaianMCU3ActionPerformed

    private void MnPenilaianMCU4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU4ActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU4.jasper","report","::[ Laporan Penilaian Awal MCU 4 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }   
    }//GEN-LAST:event_MnPenilaianMCU4ActionPerformed

    private void MnPenilaianMCU5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU5ActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU5.jasper","report","::[ Laporan Penilaian Awal MCU 5 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter, "+
                "penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher, "+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas,ifnull(penilaian_mcu_mata.visus_dasar_kanan,'') as visus_dasar_kanan,ifnull(penilaian_mcu_mata.visus_dasar_kiri,'') as visus_dasar_kiri ,ifnull(penilaian_mcu_mata.tes_buta_warna,'') as tes_buta_warna,penilaian_mcu.anjuran "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "left join penilaian_mcu_mata on reg_periksa.no_rawat=penilaian_mcu_mata.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }          
    }//GEN-LAST:event_MnPenilaianMCU5ActionPerformed

    private void BtnPenunjangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPenunjangActionPerformed
        // TODO add your handling code here:
        //LAB
        //PEMERIKSAAN
        try {       
            text_saranlab="";
            text_kesanlab="";
            rs = koneksi.prepareStatement("select saran,kesan from saran_kesan_lab where no_rawat ='"+TNoRw.getText()+"'").executeQuery();
            while(rs.next()) {
                text_saranlab=text_saranlab+rs.getString("saran")+";\n";
                text_kesanlab=text_kesanlab+rs.getString("kesan")+";\n";
            }
        } catch (Exception e) {
            System.out.println("Notifikasi sarankesan: " + e);
        }         
        //DOKTER LAB
        try {       
            text_dokterlab="";
            count_dokter=0;
            rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+TNoRw.getText()+"'").executeQuery();
            while(rs.next()) {
                if(!text_dokterlab.contains(rs.getString("nm_dokter"))){
                    count_dokter++;
                    if(count_dokter>1){
                        text_dokterlab="; "+rs.getString("nm_dokter");
                    }else{
                        text_dokterlab=rs.getString("nm_dokter");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Notifikasi dokterlab: " + e);
        }
        if(!text_dokterlab.equals("")){
            PemeriksaanLaboratorium.setText("Kesan: "+text_kesanlab+"\nSaran: "+text_saranlab);
        }
        
        //RADIOLOGI
        try {       
            text_radiologi="";
            rs = koneksi.prepareStatement("select hasil from hasil_radiologi where no_rawat='"+TNoRw.getText()+"'").executeQuery();
            while(rs.next()) {
                text_radiologi=text_radiologi+rs.getString("hasil")+";\n";
            }
        } catch (Exception e) {
            System.out.println("Notifikasi radiologi: " + e);
        }
        //DOKTER RADIOLOGI
        try {       
            text_dokterrad="";
            count_dokterrad=0;
            rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+TNoRw.getText()+"'").executeQuery();
            while(rs.next()) {
                if(!text_dokterrad.contains(rs.getString("nm_dokter"))){
                    count_dokterrad++;
                    if(count_dokterrad>1){
                        text_dokterrad="; "+rs.getString("nm_dokter");
                    }else{
                        text_dokterrad=rs.getString("nm_dokter");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Notifikasi dokterrad: " + e);
        }
        if(!text_dokterrad.equals("")){
            RongsenThorax.setText("Hasil:\n"+text_radiologi);
        }
    }//GEN-LAST:event_BtnPenunjangActionPerformed

    private void BtnPenunjangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPenunjangKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPenunjangKeyPressed

    private void MnPenilaianMCU6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU6ActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));  
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU6.jasper","report","::[ Laporan Penilaian Awal MCU 6 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }        
    }//GEN-LAST:event_MnPenilaianMCU6ActionPerformed

    private void MnPenilaianMCU7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU7ActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));  
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU7.jasper","report","::[ Laporan Penilaian Awal MCU 7 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas,"+                  
                "ifnull(penilaian_mcu_gigi.caries_gigi,'Tidak Dilakukan')as caries_gigi,ifnull(penilaian_mcu_gigi.gigi_hilang,'Tidak Dilakukan')as gigi_hilang,"+
                "ifnull(penilaian_mcu_gigi.keterangan,'Tidak Dilakukan')as pemeriksaan_gigi,ifnull(penilaian_mcu_gigi.karang_gigi,'Tidak Dilakukan')as karang_gigi "+
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter "+
                "left join penilaian_mcu_gigi on reg_periksa.no_rawat=penilaian_mcu_gigi.no_rawat "+        
                "where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }          
    }//GEN-LAST:event_MnPenilaianMCU7ActionPerformed

    private void MnPenilaianMCUBPAFKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCUBPAFKActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));  
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCUBPAFK.jasper","report","::[ Laporan Penilaian Awal MCU BPAFK ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas,"+                  
                "ifnull(penilaian_mcu_gigi.caries_gigi,'Tidak Dilakukan')as caries_gigi,ifnull(penilaian_mcu_gigi.gigi_hilang,'Tidak Dilakukan')as gigi_hilang,"+
                "ifnull(penilaian_mcu_gigi.keterangan,'Tidak Dilakukan')as pemeriksaan_gigi,ifnull(penilaian_mcu_gigi.karang_gigi,'Tidak Dilakukan')as karang_gigi "+
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter "+
                "left join penilaian_mcu_gigi on reg_periksa.no_rawat=penilaian_mcu_gigi.no_rawat "+        
                "where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }            
    }//GEN-LAST:event_MnPenilaianMCUBPAFKActionPerformed

    private void MnPenilaianMCUNuansaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCUNuansaActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCUNuansa.jasper","report","::[ Laporan Penilaian Awal MCU Nuansa ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas,"+                  
                "skoring_tb_anak.indurasi,skoring_tb_anak.uji_tuberkulin "+
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter "+
                "left join skoring_tb_anak on penilaian_mcu.no_rawat = skoring_tb_anak.no_rawat "+        
                "where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }         
    }//GEN-LAST:event_MnPenilaianMCUNuansaActionPerformed

    private void MnPenilaianMCUJepangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCUJepangActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }
            param.put("dokter_ekg",print_ekg);
            //VISUS MATA
            try {     
                rs = koneksi.prepareStatement("select visus_dasar_kanan,visus_dasar_kiri,visus_koreksi_kanan,visus_koreksi_kiri,tes_buta_warna,keterangan from penilaian_mcu_mata where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    param.put("vision_dasar_kanan",rs.getString("visus_dasar_kanan"));
                    param.put("vision_dasar_kiri",rs.getString("visus_dasar_kiri"));
                    param.put("vision_koreksi_kanan",rs.getString("visus_koreksi_kanan"));
                    param.put("vision_koreksi_kiri",rs.getString("visus_koreksi_kiri"));
                    param.put("vision_buta_warna",rs.getString("tes_buta_warna"));
                    param.put("vision_keterangan",rs.getString("keterangan"));
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print VISUS MATA: " + e);
            }              
            //HASIL MCU DIPARSING KE PARAMETER
            try {       
                rs = koneksi.prepareStatement("select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,penilaian_mcu.anjuran,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas,"+                  
                "skoring_tb_anak.indurasi,skoring_tb_anak.uji_tuberkulin "+
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter "+
                "left join skoring_tb_anak on penilaian_mcu.no_rawat = skoring_tb_anak.no_rawat "+        
                "where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    param.put("no_rawat",rs.getString("no_rawat"));
                    param.put("no_rkm_medis",rs.getString("no_rkm_medis"));
                    param.put("nm_pasien",rs.getString("nm_pasien"));
                    param.put("jk",rs.getString("jk"));
                    param.put("tgl_lahir",rs.getDate("tgl_lahir"));
                    param.put("alamat",rs.getString("alamat"));
                    param.put("keadaan",rs.getString("keadaan"));
                    param.put("kesadaran",rs.getString("kesadaran"));
                    param.put("td",rs.getString("td"));
                    param.put("nadi",rs.getString("nadi"));
                    param.put("tb",rs.getString("tb"));
                    param.put("bb",rs.getString("bb"));
                    param.put("rr",rs.getString("rr"));
                    param.put("suhu",rs.getString("suhu"));
                    param.put("radiologi",rs.getString("radiologi"));
                    param.put("laborat",rs.getString("laborat"));
                    param.put("kesimpulan",rs.getString("kesimpulan"));
                    param.put("anjuran",rs.getString("anjuran")); 
                    param.put("tanggal",rs.getDate("tanggal"));
                    param.put("nm_dokter",rs.getString("nm_dokter"));
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print hasil mcu: " + e);
            }
            //MENGAMBIL HASIL LAB
            try {   
                ps4=koneksi.prepareStatement(
                    "select periksa_lab.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,pasien.jk,pasien.umur,petugas.nama,DATE_FORMAT(periksa_lab.tgl_periksa,'%d-%m-%Y') as tgl_periksa,periksa_lab.jam,periksa_lab.nip,"+
                    "periksa_lab.dokter_perujuk,periksa_lab.kd_dokter,concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab) as alamat,dokter.nm_dokter,DATE_FORMAT(pasien.tgl_lahir,'%d-%m-%Y') as lahir "+
                    " from periksa_lab inner join reg_periksa inner join pasien inner join petugas  inner join dokter inner join kelurahan inner join kecamatan inner join kabupaten "+
                    "on periksa_lab.no_rawat=reg_periksa.no_rawat and reg_periksa.no_rkm_medis=pasien.no_rkm_medis and periksa_lab.nip=petugas.nip and periksa_lab.kd_dokter=dokter.kd_dokter "+
                    "and pasien.kd_kel=kelurahan.kd_kel and pasien.kd_kec=kecamatan.kd_kec and pasien.kd_kab=kabupaten.kd_kab where periksa_lab.kategori='PK' "+
                    "and periksa_lab.no_rawat=? group by concat(periksa_lab.no_rawat,periksa_lab.tgl_periksa,periksa_lab.jam)");
                try {
                    ps4.setString(1,TNoRw.getText());
                    rs=ps4.executeQuery();
                    while(rs.next()){
                        kamar=Sequel.cariIsi("select ifnull(kd_kamar,'') from kamar_inap where no_rawat='"+rs.getString("no_rawat")+"' order by tgl_masuk desc limit 1");
                        if(!kamar.equals("")){
                            namakamar=kamar+", "+Sequel.cariIsi("select nm_bangsal from bangsal inner join kamar on bangsal.kd_bangsal=kamar.kd_bangsal "+
                                    " where kamar.kd_kamar='"+kamar+"' ");            
                            kamar="Kamar";
                        }else if(kamar.equals("")){
                            kamar="Poli";
                            namakamar=Sequel.cariIsi("select nm_poli from poliklinik inner join reg_periksa on poliklinik.kd_poli=reg_periksa.kd_poli "+
                                    "where reg_periksa.no_rawat='"+rs.getString("no_rawat")+"'");
                        }
                        param.put("noperiksa",rs.getString("no_rawat"));
                        param.put("jam",rs.getString("jam"));
                        finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",rs.getString("kd_dokter"));
                        finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",rs.getString("nip"));
                        Sequel.queryu("truncate table temporary_lab");

                        ps2=koneksi.prepareStatement(
                            "select jns_perawatan_lab.kd_jenis_prw,jns_perawatan_lab.nm_perawatan,periksa_lab.biaya from periksa_lab inner join jns_perawatan_lab "+
                            "on periksa_lab.kd_jenis_prw=jns_perawatan_lab.kd_jenis_prw where periksa_lab.kategori='PK' and periksa_lab.no_rawat=? and periksa_lab.tgl_periksa=? "+
                            "and periksa_lab.jam=?");
                        try {
                            ps2.setString(1,rs.getString("no_rawat"));
                            ps2.setString(2,Valid.SetTgl(rs.getString("tgl_periksa")));
                            ps2.setString(3,rs.getString("jam"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                //START CUSTOM TAMBAH NO RAWAT DAN JAM HASIL UNTUK CHECKING DALAM CETAK MODEL 1
                                Sequel.menyimpan("temporary_lab","'0','"+rs2.getString("nm_perawatan")+"','','','','','"+rs.getString("no_rawat")+"','"+rs.getString("jam")+"','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''","Data User"); 
                                //END CUSTOM
                                ps3=koneksi.prepareStatement(
                                    "select template_laboratorium.Pemeriksaan, detail_periksa_lab.nilai,template_laboratorium.satuan,detail_periksa_lab.nilai_rujukan,detail_periksa_lab.biaya_item,"+
                                    "detail_periksa_lab.keterangan,detail_periksa_lab.kd_jenis_prw from detail_periksa_lab inner join template_laboratorium on detail_periksa_lab.id_template=template_laboratorium.id_template "+
                                    "where detail_periksa_lab.no_rawat=? and detail_periksa_lab.kd_jenis_prw=? and detail_periksa_lab.tgl_periksa=? and detail_periksa_lab.jam=? order by template_laboratorium.urut");
                                try {
                                    ps3.setString(1,rs.getString("no_rawat"));
                                    ps3.setString(2,rs2.getString("kd_jenis_prw"));
                                    ps3.setString(3,Valid.SetTgl(rs.getString("tgl_periksa")));
                                    ps3.setString(4,rs.getString("jam"));
                                    rs3=ps3.executeQuery();
                                    while(rs3.next()){
                                        //START CUSTOM TAMBAH NO RAWAT DAN JAM HASIL UNTUK CHECKING DALAM CETAK MODEL 1
                                        Sequel.menyimpan("temporary_lab","'0','  "+rs3.getString("Pemeriksaan")+"','"+rs3.getString("nilai").replaceAll("'","`")+"','"+rs3.getString("satuan")
                                                +"','"+rs3.getString("nilai_rujukan")+"','"+rs3.getString("keterangan")+"','"+rs.getString("no_rawat")+"','"+rs.getString("jam")+"','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''","Data User");
                                        //END CUSTOM
                                    }
                                } catch (Exception e) {
                                    System.out.println("Notif ps3 : "+e);
                                } finally{
                                    if(rs3!=null){
                                        rs3.close();
                                    }
                                    if(ps3!=null){
                                        ps3.close();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notif ps2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        pspermintaan=koneksi.prepareStatement(
                                "select noorder,DATE_FORMAT(tgl_permintaan,'%d-%m-%Y') as tgl_permintaan,jam_permintaan from permintaan_lab where "+
                                "no_rawat=? and tgl_hasil=? and jam_hasil=?");
                        try {
                            pspermintaan.setString(1,rs.getString("no_rawat"));
                            pspermintaan.setString(2,Valid.SetTgl(rs.getString("tgl_periksa")));
                            pspermintaan.setString(3,rs.getString("jam"));
                            rspermintaan=pspermintaan.executeQuery();
                            //nothing
                        } catch (Exception e) {
                            System.out.println("Notif : "+e);
                        } finally{
                            if(rspermintaan!=null){
                                rspermintaan.close();
                            }
                            if(pspermintaan!=null){
                                pspermintaan.close();
                            }
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Notif ps4 : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps4!=null){
                        ps4.close();
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            }            
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCUJepang.jasper","report","::[ Laporan Penilaian Awal MCU Magang Jepang ]::",
                "select no, temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8, temp9, temp10, temp11, temp12, temp13, temp14, temp15, temp16 from temporary_lab order by no asc",param);
        } 
    }//GEN-LAST:event_MnPenilaianMCUJepangActionPerformed

    private void MnPenilaianMCULNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCULNActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }
            param.put("dokter_ekg",print_ekg);
            //VISUS MATA
            try {     
                rs = koneksi.prepareStatement("select visus_dasar_kanan,visus_dasar_kiri,visus_koreksi_kanan,visus_koreksi_kiri,tes_buta_warna,keterangan from penilaian_mcu_mata where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    param.put("vision_dasar_kanan",rs.getString("visus_dasar_kanan"));
                    param.put("vision_dasar_kiri",rs.getString("visus_dasar_kiri"));
                    param.put("vision_koreksi_kanan",rs.getString("visus_koreksi_kanan"));
                    param.put("vision_koreksi_kiri",rs.getString("visus_koreksi_kiri"));
                    param.put("vision_buta_warna",rs.getString("tes_buta_warna"));
                    param.put("vision_keterangan",rs.getString("keterangan"));
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print VISUS MATA: " + e);
            }              
            //HASIL MCU DIPARSING KE PARAMETER
            try {       
                rs = koneksi.prepareStatement("select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas,"+                  
                "skoring_tb_anak.indurasi,skoring_tb_anak.uji_tuberkulin "+
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter "+
                "left join skoring_tb_anak on penilaian_mcu.no_rawat = skoring_tb_anak.no_rawat "+        
                "where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    param.put("no_rawat",rs.getString("no_rawat"));
                    param.put("no_rkm_medis",rs.getString("no_rkm_medis"));
                    param.put("nm_pasien",rs.getString("nm_pasien"));
                    param.put("jk",rs.getString("jk"));
                    param.put("tgl_lahir",rs.getDate("tgl_lahir"));
                    param.put("alamat",rs.getString("alamat"));
                    param.put("keadaan",rs.getString("keadaan"));
                    param.put("kesadaran",rs.getString("kesadaran"));
                    param.put("td",rs.getString("td"));
                    param.put("nadi",rs.getString("nadi"));
                    param.put("tb",rs.getString("tb"));
                    param.put("bb",rs.getString("bb"));
                    param.put("rr",rs.getString("rr"));
                    param.put("suhu",rs.getString("suhu"));
                    param.put("radiologi",rs.getString("radiologi"));
                    param.put("laborat",rs.getString("laborat"));
                    param.put("kesimpulan",rs.getString("kesimpulan"));
                    param.put("anjuran",rs.getString("anjuran"));
                    param.put("tanggal",rs.getDate("tanggal"));
                    param.put("nm_dokter",rs.getString("nm_dokter"));
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print hasil mcu: " + e);
            }
            //MENGAMBIL HASIL LAB
            try {   
                ps4=koneksi.prepareStatement(
                    "select periksa_lab.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,pasien.jk,pasien.umur,petugas.nama,DATE_FORMAT(periksa_lab.tgl_periksa,'%d-%m-%Y') as tgl_periksa,periksa_lab.jam,periksa_lab.nip,"+
                    "periksa_lab.dokter_perujuk,periksa_lab.kd_dokter,concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab) as alamat,dokter.nm_dokter,DATE_FORMAT(pasien.tgl_lahir,'%d-%m-%Y') as lahir "+
                    " from periksa_lab inner join reg_periksa inner join pasien inner join petugas  inner join dokter inner join kelurahan inner join kecamatan inner join kabupaten "+
                    "on periksa_lab.no_rawat=reg_periksa.no_rawat and reg_periksa.no_rkm_medis=pasien.no_rkm_medis and periksa_lab.nip=petugas.nip and periksa_lab.kd_dokter=dokter.kd_dokter "+
                    "and pasien.kd_kel=kelurahan.kd_kel and pasien.kd_kec=kecamatan.kd_kec and pasien.kd_kab=kabupaten.kd_kab where periksa_lab.kategori='PK' "+
                    "and periksa_lab.no_rawat=? group by concat(periksa_lab.no_rawat,periksa_lab.tgl_periksa,periksa_lab.jam)");
                try {
                    ps4.setString(1,TNoRw.getText());
                    rs=ps4.executeQuery();
                    while(rs.next()){
                        kamar=Sequel.cariIsi("select ifnull(kd_kamar,'') from kamar_inap where no_rawat='"+rs.getString("no_rawat")+"' order by tgl_masuk desc limit 1");
                        if(!kamar.equals("")){
                            namakamar=kamar+", "+Sequel.cariIsi("select nm_bangsal from bangsal inner join kamar on bangsal.kd_bangsal=kamar.kd_bangsal "+
                                    " where kamar.kd_kamar='"+kamar+"' ");            
                            kamar="Kamar";
                        }else if(kamar.equals("")){
                            kamar="Poli";
                            namakamar=Sequel.cariIsi("select nm_poli from poliklinik inner join reg_periksa on poliklinik.kd_poli=reg_periksa.kd_poli "+
                                    "where reg_periksa.no_rawat='"+rs.getString("no_rawat")+"'");
                        }
                        param.put("noperiksa",rs.getString("no_rawat"));
                        param.put("jam",rs.getString("jam"));
                        finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",rs.getString("kd_dokter"));
                        finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",rs.getString("nip"));
                        Sequel.queryu("truncate table temporary_lab");

                        ps2=koneksi.prepareStatement(
                            "select jns_perawatan_lab.kd_jenis_prw,jns_perawatan_lab.nm_perawatan,periksa_lab.biaya from periksa_lab inner join jns_perawatan_lab "+
                            "on periksa_lab.kd_jenis_prw=jns_perawatan_lab.kd_jenis_prw where periksa_lab.kategori='PK' and periksa_lab.no_rawat=? and periksa_lab.tgl_periksa=? "+
                            "and periksa_lab.jam=?");
                        try {
                            ps2.setString(1,rs.getString("no_rawat"));
                            ps2.setString(2,Valid.SetTgl(rs.getString("tgl_periksa")));
                            ps2.setString(3,rs.getString("jam"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                //START CUSTOM TAMBAH NO RAWAT DAN JAM HASIL UNTUK CHECKING DALAM CETAK MODEL 1
                                Sequel.menyimpan("temporary_lab","'0','"+rs2.getString("nm_perawatan")+"','','','','','"+rs.getString("no_rawat")+"','"+rs.getString("jam")+"','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''","Data User"); 
                                //END CUSTOM
                                ps3=koneksi.prepareStatement(
                                    "select template_laboratorium.Pemeriksaan, detail_periksa_lab.nilai,template_laboratorium.satuan,detail_periksa_lab.nilai_rujukan,detail_periksa_lab.biaya_item,"+
                                    "detail_periksa_lab.keterangan,detail_periksa_lab.kd_jenis_prw from detail_periksa_lab inner join template_laboratorium on detail_periksa_lab.id_template=template_laboratorium.id_template "+
                                    "where detail_periksa_lab.no_rawat=? and detail_periksa_lab.kd_jenis_prw=? and detail_periksa_lab.tgl_periksa=? and detail_periksa_lab.jam=? order by template_laboratorium.urut");
                                try {
                                    ps3.setString(1,rs.getString("no_rawat"));
                                    ps3.setString(2,rs2.getString("kd_jenis_prw"));
                                    ps3.setString(3,Valid.SetTgl(rs.getString("tgl_periksa")));
                                    ps3.setString(4,rs.getString("jam"));
                                    rs3=ps3.executeQuery();
                                    while(rs3.next()){
                                        //START CUSTOM TAMBAH NO RAWAT DAN JAM HASIL UNTUK CHECKING DALAM CETAK MODEL 1
                                        Sequel.menyimpan("temporary_lab","'0','  "+rs3.getString("Pemeriksaan")+"','"+rs3.getString("nilai").replaceAll("'","`")+"','"+rs3.getString("satuan")
                                                +"','"+rs3.getString("nilai_rujukan")+"','"+rs3.getString("keterangan")+"','"+rs.getString("no_rawat")+"','"+rs.getString("jam")+"','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''","Data User");
                                        //END CUSTOM
                                    }
                                } catch (Exception e) {
                                    System.out.println("Notif ps3 : "+e);
                                } finally{
                                    if(rs3!=null){
                                        rs3.close();
                                    }
                                    if(ps3!=null){
                                        ps3.close();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notif ps2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        pspermintaan=koneksi.prepareStatement(
                                "select noorder,DATE_FORMAT(tgl_permintaan,'%d-%m-%Y') as tgl_permintaan,jam_permintaan from permintaan_lab where "+
                                "no_rawat=? and tgl_hasil=? and jam_hasil=?");
                        try {
                            pspermintaan.setString(1,rs.getString("no_rawat"));
                            pspermintaan.setString(2,Valid.SetTgl(rs.getString("tgl_periksa")));
                            pspermintaan.setString(3,rs.getString("jam"));
                            rspermintaan=pspermintaan.executeQuery();
                            //nothing
                        } catch (Exception e) {
                            System.out.println("Notif : "+e);
                        } finally{
                            if(rspermintaan!=null){
                                rspermintaan.close();
                            }
                            if(pspermintaan!=null){
                                pspermintaan.close();
                            }
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Notif ps4 : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps4!=null){
                        ps4.close();
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            }            
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCULN.jasper","report","::[ Laporan Penilaian Awal MCU Luar Negeri ]::",
                "select no, temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8, temp9, temp10, temp11, temp12, temp13, temp14, temp15, temp16 from temporary_lab order by no asc",param);
        }         
    }//GEN-LAST:event_MnPenilaianMCULNActionPerformed

    private void BtnTemplateNormalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTemplateNormalActionPerformed

        RongsenThorax.setText("Heart and Lungs Normal ");
        PemeriksaanLaboratorium.setText("Laboratory Results Normal");
        Kesimpulan.setText("Fit to work");
        Anjuran.setText("Healthy and Balanced Lifestyle");
    }//GEN-LAST:event_BtnTemplateNormalActionPerformed

    private void BtnTemplateNormalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnTemplateNormalKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnTemplateNormalKeyPressed

    private void MnPenilaianMCU5BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU5BActionPerformed
      // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU5B.jasper","report","::[ Laporan Penilaian Awal MCU 5 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter, "+
                "penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher, "+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas,ifnull(penilaian_mcu_mata.visus_dasar_kanan,'') as visus_dasar_kanan,ifnull(penilaian_mcu_mata.visus_dasar_kiri,'') as visus_dasar_kiri ,ifnull(penilaian_mcu_mata.tes_buta_warna,'') as tes_buta_warna,penilaian_mcu.anjuran "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "left join penilaian_mcu_mata on reg_periksa.no_rawat=penilaian_mcu_mata.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }          
    }//GEN-LAST:event_MnPenilaianMCU5BActionPerformed

    private void MnPenilaianMCUJepangBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCUJepangBActionPerformed
       // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }
            param.put("dokter_ekg",print_ekg);
            //VISUS MATA
            try {     
                rs = koneksi.prepareStatement("select visus_dasar_kanan,visus_dasar_kiri,visus_koreksi_kanan,visus_koreksi_kiri,tes_buta_warna,keterangan from penilaian_mcu_mata where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    param.put("vision_dasar_kanan",rs.getString("visus_dasar_kanan"));
                    param.put("vision_dasar_kiri",rs.getString("visus_dasar_kiri"));
                    param.put("vision_koreksi_kanan",rs.getString("visus_koreksi_kanan"));
                    param.put("vision_koreksi_kiri",rs.getString("visus_koreksi_kiri"));
                    param.put("vision_buta_warna",rs.getString("tes_buta_warna"));
                    param.put("vision_keterangan",rs.getString("keterangan"));
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print VISUS MATA: " + e);
            }              
            //HASIL MCU DIPARSING KE PARAMETER
            try {       
                rs = koneksi.prepareStatement("select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,penilaian_mcu.anjuran,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas,"+                  
                "skoring_tb_anak.indurasi,skoring_tb_anak.uji_tuberkulin "+
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter "+
                "left join skoring_tb_anak on penilaian_mcu.no_rawat = skoring_tb_anak.no_rawat "+        
                "where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    param.put("no_rawat",rs.getString("no_rawat"));
                    param.put("no_rkm_medis",rs.getString("no_rkm_medis"));
                    param.put("nm_pasien",rs.getString("nm_pasien"));
                    param.put("jk",rs.getString("jk"));
                    param.put("tgl_lahir",rs.getDate("tgl_lahir"));
                    param.put("alamat",rs.getString("alamat"));
                    param.put("keadaan",rs.getString("keadaan"));
                    param.put("kesadaran",rs.getString("kesadaran"));
                    param.put("td",rs.getString("td"));
                    param.put("nadi",rs.getString("nadi"));
                    param.put("tb",rs.getString("tb"));
                    param.put("bb",rs.getString("bb"));
                    param.put("rr",rs.getString("rr"));
                    param.put("suhu",rs.getString("suhu"));
                    param.put("radiologi",rs.getString("radiologi"));
                    param.put("laborat",rs.getString("laborat"));
                    param.put("kesimpulan",rs.getString("kesimpulan"));
                    param.put("anjuran",rs.getString("anjuran")); 
                    param.put("tanggal",rs.getDate("tanggal"));
                    param.put("nm_dokter",rs.getString("nm_dokter"));
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print hasil mcu: " + e);
            }
            //MENGAMBIL HASIL LAB
            try {   
                ps4=koneksi.prepareStatement(
                    "select periksa_lab.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,pasien.jk,pasien.umur,petugas.nama,DATE_FORMAT(periksa_lab.tgl_periksa,'%d-%m-%Y') as tgl_periksa,periksa_lab.jam,periksa_lab.nip,"+
                    "periksa_lab.dokter_perujuk,periksa_lab.kd_dokter,concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab) as alamat,dokter.nm_dokter,DATE_FORMAT(pasien.tgl_lahir,'%d-%m-%Y') as lahir "+
                    " from periksa_lab inner join reg_periksa inner join pasien inner join petugas  inner join dokter inner join kelurahan inner join kecamatan inner join kabupaten "+
                    "on periksa_lab.no_rawat=reg_periksa.no_rawat and reg_periksa.no_rkm_medis=pasien.no_rkm_medis and periksa_lab.nip=petugas.nip and periksa_lab.kd_dokter=dokter.kd_dokter "+
                    "and pasien.kd_kel=kelurahan.kd_kel and pasien.kd_kec=kecamatan.kd_kec and pasien.kd_kab=kabupaten.kd_kab where periksa_lab.kategori='PK' "+
                    "and periksa_lab.no_rawat=? group by concat(periksa_lab.no_rawat,periksa_lab.tgl_periksa,periksa_lab.jam)");
                try {
                    ps4.setString(1,TNoRw.getText());
                    rs=ps4.executeQuery();
                    while(rs.next()){
                        kamar=Sequel.cariIsi("select ifnull(kd_kamar,'') from kamar_inap where no_rawat='"+rs.getString("no_rawat")+"' order by tgl_masuk desc limit 1");
                        if(!kamar.equals("")){
                            namakamar=kamar+", "+Sequel.cariIsi("select nm_bangsal from bangsal inner join kamar on bangsal.kd_bangsal=kamar.kd_bangsal "+
                                    " where kamar.kd_kamar='"+kamar+"' ");            
                            kamar="Kamar";
                        }else if(kamar.equals("")){
                            kamar="Poli";
                            namakamar=Sequel.cariIsi("select nm_poli from poliklinik inner join reg_periksa on poliklinik.kd_poli=reg_periksa.kd_poli "+
                                    "where reg_periksa.no_rawat='"+rs.getString("no_rawat")+"'");
                        }
                        param.put("noperiksa",rs.getString("no_rawat"));
                        param.put("jam",rs.getString("jam"));
                        finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",rs.getString("kd_dokter"));
                        finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",rs.getString("nip"));
                        Sequel.queryu("truncate table temporary_lab");

                        ps2=koneksi.prepareStatement(
                            "select jns_perawatan_lab.kd_jenis_prw,jns_perawatan_lab.nm_perawatan,periksa_lab.biaya from periksa_lab inner join jns_perawatan_lab "+
                            "on periksa_lab.kd_jenis_prw=jns_perawatan_lab.kd_jenis_prw where periksa_lab.kategori='PK' and periksa_lab.no_rawat=? and periksa_lab.tgl_periksa=? "+
                            "and periksa_lab.jam=?");
                        try {
                            ps2.setString(1,rs.getString("no_rawat"));
                            ps2.setString(2,Valid.SetTgl(rs.getString("tgl_periksa")));
                            ps2.setString(3,rs.getString("jam"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                //START CUSTOM TAMBAH NO RAWAT DAN JAM HASIL UNTUK CHECKING DALAM CETAK MODEL 1
                                Sequel.menyimpan("temporary_lab","'0','"+rs2.getString("nm_perawatan")+"','','','','','"+rs.getString("no_rawat")+"','"+rs.getString("jam")+"','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''","Data User"); 
                                //END CUSTOM
                                ps3=koneksi.prepareStatement(
                                    "select template_laboratorium.Pemeriksaan, detail_periksa_lab.nilai,template_laboratorium.satuan,detail_periksa_lab.nilai_rujukan,detail_periksa_lab.biaya_item,"+
                                    "detail_periksa_lab.keterangan,detail_periksa_lab.kd_jenis_prw from detail_periksa_lab inner join template_laboratorium on detail_periksa_lab.id_template=template_laboratorium.id_template "+
                                    "where detail_periksa_lab.no_rawat=? and detail_periksa_lab.kd_jenis_prw=? and detail_periksa_lab.tgl_periksa=? and detail_periksa_lab.jam=? order by template_laboratorium.urut");
                                try {
                                    ps3.setString(1,rs.getString("no_rawat"));
                                    ps3.setString(2,rs2.getString("kd_jenis_prw"));
                                    ps3.setString(3,Valid.SetTgl(rs.getString("tgl_periksa")));
                                    ps3.setString(4,rs.getString("jam"));
                                    rs3=ps3.executeQuery();
                                    while(rs3.next()){
                                        //START CUSTOM TAMBAH NO RAWAT DAN JAM HASIL UNTUK CHECKING DALAM CETAK MODEL 1
                                        Sequel.menyimpan("temporary_lab","'0','  "+rs3.getString("Pemeriksaan")+"','"+rs3.getString("nilai").replaceAll("'","`")+"','"+rs3.getString("satuan")
                                                +"','"+rs3.getString("nilai_rujukan")+"','"+rs3.getString("keterangan")+"','"+rs.getString("no_rawat")+"','"+rs.getString("jam")+"','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''","Data User");
                                        //END CUSTOM
                                    }
                                } catch (Exception e) {
                                    System.out.println("Notif ps3 : "+e);
                                } finally{
                                    if(rs3!=null){
                                        rs3.close();
                                    }
                                    if(ps3!=null){
                                        ps3.close();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notif ps2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        pspermintaan=koneksi.prepareStatement(
                                "select noorder,DATE_FORMAT(tgl_permintaan,'%d-%m-%Y') as tgl_permintaan,jam_permintaan from permintaan_lab where "+
                                "no_rawat=? and tgl_hasil=? and jam_hasil=?");
                        try {
                            pspermintaan.setString(1,rs.getString("no_rawat"));
                            pspermintaan.setString(2,Valid.SetTgl(rs.getString("tgl_periksa")));
                            pspermintaan.setString(3,rs.getString("jam"));
                            rspermintaan=pspermintaan.executeQuery();
                            //nothing
                        } catch (Exception e) {
                            System.out.println("Notif : "+e);
                        } finally{
                            if(rspermintaan!=null){
                                rspermintaan.close();
                            }
                            if(pspermintaan!=null){
                                pspermintaan.close();
                            }
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Notif ps4 : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps4!=null){
                        ps4.close();
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            }            
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCUJepangB.jasper","report","::[ Laporan Penilaian Awal MCU Magang Jepang ]::",
                "select no, temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8, temp9, temp10, temp11, temp12, temp13, temp14, temp15, temp16 from temporary_lab order by no asc",param);
        } 
    }//GEN-LAST:event_MnPenilaianMCUJepangBActionPerformed

    private void MnPenilaianMCUPolkestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCUPolkestaActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));  
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            param.put("lingkar_perut",Sequel.cariIsi("select lingkar_perut from penilaian_awal_keperawatan_ralan_tambahan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCUPolkesta.jasper","report","::[ Laporan Penilaian Awal MCU Polkesta ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }         
    }//GEN-LAST:event_MnPenilaianMCUPolkestaActionPerformed

    private void MnPenilaianMCU6BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU6BActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));  
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //UJI KEBUGARAN
            if(Sequel.cariInteger("select count(no_rawat) from penilaian_ujikebugaran where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                uji_kebugaran=Sequel.cariIsi("SELECT CONCAT('Berdasarkan hasil uji kebugaran yang dilakukan dengan menggunakan protokol Six Minute Walking Test, maka dapat disimpulkan hasil kebugaran: ', hasil_kesimpulan, '\n\nRekomendasi: ', rekomendasi, '\n\nCatatan dokter: ', catatan) as uji_kebugaran " + 
                                "FROM penilaian_ujikebugaran " + 
                                "WHERE no_rawat='" + tbObat.getValueAt(tbObat.getSelectedRow(), 0).toString() + "'");
                dokter_uji_kebugaran=Sequel.cariIsi("select nm_dokter from penilaian_ujikebugaran inner join dokter d on penilaian_ujikebugaran.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());  
            }else{
                if(Sequel.cariInteger("select count(no_rawat) from penilaian_ujikebugaran_ymca where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                    uji_kebugaran=Sequel.cariIsi("SELECT CONCAT('Berdasarkan hasil uji kebugaran yang dilakukan dengan menggunakan protokol YMCA Test, maka dapat disimpulkan hasil kebugaran: ', hasil_kesimpulan, '\n\nRekomendasi: ', rekomendasi, '\n\nCatatan dokter: ', catatan) as uji_kebugaran " + 
                                    "FROM penilaian_ujikebugaran_ymca " + 
                                    "WHERE no_rawat='" + tbObat.getValueAt(tbObat.getSelectedRow(), 0).toString() + "'");
                    dokter_uji_kebugaran=Sequel.cariIsi("select nm_dokter from penilaian_ujikebugaran_ymca inner join dokter d on penilaian_ujikebugaran_ymca.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());  
                }else{
                    uji_kebugaran="";
                    dokter_uji_kebugaran="";                    
                }   
            }
            param.put("uji_kebugaran",uji_kebugaran);
            param.put("dokter_uji_kebugaran",dokter_uji_kebugaran);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU6B.jasper","report","::[ Laporan Penilaian Awal MCU 6 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }           
    }//GEN-LAST:event_MnPenilaianMCU6BActionPerformed

    private void MnPenilaianMCU6CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU6CActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));  
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //UJI KEBUGARAN
            if(Sequel.cariInteger("select count(no_rawat) from penilaian_ujikebugaran where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                uji_kebugaran=Sequel.cariIsi("SELECT CONCAT('Berdasarkan hasil uji kebugaran yang dilakukan dengan menggunakan protokol Six Minute Walking Test, maka dapat disimpulkan hasil kebugaran: ', hasil_kesimpulan, '\n\nRekomendasi: ', rekomendasi, '\n\nCatatan dokter: ', catatan) as uji_kebugaran " + 
                                "FROM penilaian_ujikebugaran " + 
                                "WHERE no_rawat='" + tbObat.getValueAt(tbObat.getSelectedRow(), 0).toString() + "'");
                dokter_uji_kebugaran=Sequel.cariIsi("select nm_dokter from penilaian_ujikebugaran inner join dokter d on penilaian_ujikebugaran.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());  
            }else{
                if(Sequel.cariInteger("select count(no_rawat) from penilaian_ujikebugaran_ymca where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                    uji_kebugaran=Sequel.cariIsi("SELECT CONCAT('Berdasarkan hasil uji kebugaran yang dilakukan dengan menggunakan protokol YMCA Test, maka dapat disimpulkan hasil kebugaran: ', hasil_kesimpulan, '\n\nRekomendasi: ', rekomendasi, '\n\nCatatan dokter: ', catatan) as uji_kebugaran " + 
                                    "FROM penilaian_ujikebugaran_ymca " + 
                                    "WHERE no_rawat='" + tbObat.getValueAt(tbObat.getSelectedRow(), 0).toString() + "'");
                    dokter_uji_kebugaran=Sequel.cariIsi("select nm_dokter from penilaian_ujikebugaran_ymca inner join dokter d on penilaian_ujikebugaran_ymca.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());  
                }else{
                    uji_kebugaran="";
                    dokter_uji_kebugaran="";                    
                }   
            }
            param.put("uji_kebugaran",uji_kebugaran);
            param.put("dokter_uji_kebugaran",dokter_uji_kebugaran);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU6C.jasper","report","::[ Laporan Penilaian Awal MCU 6 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }        
    }//GEN-LAST:event_MnPenilaianMCU6CActionPerformed

    private void MnPenilaianMCU4BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU4BActionPerformed
        // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU4B.jasper","report","::[ Laporan Penilaian Awal MCU 4 ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }         
    }//GEN-LAST:event_MnPenilaianMCU4BActionPerformed

    private void MnPenilaianMCU6BUMSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCU6BUMSActionPerformed
         // TODO add your handling code here:
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),75).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),74).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()));
            //START CUSTOM
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));  
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("bmi",Sequel.cariIsi("select bmi from penilaian_awal_keperawatan_ralan where no_rawat=?", tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            //DOKTER RADIOLOGI
            try {       
                print_dokterrad="";
                print_count_dokterrad=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterrad.contains(rs.getString("nm_dokter"))){
                        print_count_dokterrad++;
                        if(print_count_dokterrad>1){
                            print_dokterrad="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterrad=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterrad: " + e);
            }            
            param.put("dokter_rontgen",print_dokterrad);
            //DOKTER LAB
            try {       
                print_dokterlab="";
                print_count_dokter=0;
                rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'").executeQuery();
                while(rs.next()) {
                    if(!print_dokterlab.contains(rs.getString("nm_dokter"))){
                        print_count_dokter++;
                        if(print_count_dokter>1){
                            print_dokterlab="; "+rs.getString("nm_dokter");
                        }else{
                            print_dokterlab=rs.getString("nm_dokter");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print dokterlab: " + e);
            }            
            param.put("dokter_lab",print_dokterlab);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //DOKTER EKG
            print_ekg="";
            if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                print_ekg=Sequel.cariIsi("select nm_dokter from penilaian_ekg inner join dokter d on penilaian_ekg.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                       
            }         
            param.put("dokter_ekg",print_ekg);
            //UJI KEBUGARAN
            if(Sequel.cariInteger("select count(no_rawat) from penilaian_ujikebugaran where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                uji_kebugaran=Sequel.cariIsi("SELECT CONCAT('Berdasarkan hasil uji kebugaran yang dilakukan dengan menggunakan protokol Six Minute Walking Test, maka dapat disimpulkan hasil kebugaran: ', hasil_kesimpulan, '\n\nRekomendasi: ', rekomendasi, '\n\nCatatan dokter: ', catatan) as uji_kebugaran " + 
                                "FROM penilaian_ujikebugaran " + 
                                "WHERE no_rawat='" + tbObat.getValueAt(tbObat.getSelectedRow(), 0).toString() + "'");
                dokter_uji_kebugaran=Sequel.cariIsi("select nm_dokter from penilaian_ujikebugaran inner join dokter d on penilaian_ujikebugaran.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());  
            }else{
                if(Sequel.cariInteger("select count(no_rawat) from penilaian_ujikebugaran_ymca where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'") > 0){
                    uji_kebugaran=Sequel.cariIsi("SELECT CONCAT('Berdasarkan hasil uji kebugaran yang dilakukan dengan menggunakan protokol YMCA Test, maka dapat disimpulkan hasil kebugaran: ', hasil_kesimpulan, '\n\nRekomendasi: ', rekomendasi, '\n\nCatatan dokter: ', catatan) as uji_kebugaran " + 
                                    "FROM penilaian_ujikebugaran_ymca " + 
                                    "WHERE no_rawat='" + tbObat.getValueAt(tbObat.getSelectedRow(), 0).toString() + "'");
                    dokter_uji_kebugaran=Sequel.cariIsi("select nm_dokter from penilaian_ujikebugaran_ymca inner join dokter d on penilaian_ujikebugaran_ymca.kd_dokter = d.kd_dokter where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());  
                }else{
                    uji_kebugaran="";
                    dokter_uji_kebugaran="";                    
                }   
            }
            param.put("uji_kebugaran",uji_kebugaran);
            param.put("dokter_uji_kebugaran",dokter_uji_kebugaran);
            //END CUSTOM
            Valid.MyReportqry("rptCetakPenilaianAwalMCU6BUMS.jasper","report","::[ Laporan Penilaian Awal MCU UMS ]::",
                "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.stts_nikah,penilaian_mcu.tanggal,"+
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+                  
                "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+
                "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }           
    }//GEN-LAST:event_MnPenilaianMCU6BUMSActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMMCU dialog = new RMMCU(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.TextBox Alkohol;
    private widget.TextArea Anjuran;
    private widget.TextArea Audiometri;
    private widget.ComboBox Auskultasi;
    private widget.ComboBox Axila;
    private widget.ComboBox Batas;
    private widget.TextBox BeratBadan;
    private widget.ComboBox Bibir;
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPenunjang;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Button BtnTemplateNormal;
    private widget.Button BtnVisus;
    private widget.ComboBox BunyiJantung;
    private widget.ComboBox BunyiNapas;
    private widget.ComboBox BunyiTambahan;
    private widget.ComboBox Caries;
    private widget.ComboBox Cornea;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox DaunTelinga;
    private widget.TextArea EKG;
    private widget.ComboBox ExtremitasAtas;
    private widget.ComboBox ExtremitasBawah;
    private widget.ComboBox Faring;
    private widget.PanelBiasa FormInput;
    private widget.ComboBox GerakanDada;
    private widget.ComboBox Hepar;
    private widget.ComboBox IctusCordis;
    private widget.ComboBox Informasi;
    private widget.ComboBox Inguinal;
    private widget.ComboBox Inspeksi;
    private widget.TextBox Jk;
    private widget.TextBox KdDokter;
    private widget.ComboBox KeadaanUmum;
    private widget.ComboBox KelainanAbdomen;
    private widget.ComboBox KelainanDada;
    private widget.ComboBox KelainanEkstrimitas;
    private widget.ComboBox KelainanHidung;
    private widget.ComboBox KelainanKepala;
    private widget.ComboBox KelainanLeher;
    private widget.ComboBox KelainanMata;
    private widget.ComboBox KelainanTelinga;
    private widget.ComboBox KelenjarGondok;
    private widget.ComboBox KelenjarLimfe;
    private widget.ComboBox Kesadaran;
    private widget.TextArea Kesimpulan;
    private widget.TextBox KetExtremitasAtas;
    private widget.TextBox KetExtremitasBawah;
    private widget.ComboBox Konjungtiva;
    private widget.ComboBox Kulit;
    private widget.Label LCount;
    private widget.TextArea Lainlain;
    private widget.ComboBox Leher;
    private widget.ComboBox Lensa;
    private widget.ComboBox Lidah;
    private widget.ComboBox Limpa;
    private widget.TextBox LingkarPerut;
    private widget.editorpane LoadHTML;
    private widget.ComboBox LubangHidung;
    private widget.ComboBox LubangTelinga;
    private widget.TextBox Merokok;
    private javax.swing.JMenuItem MnPenilaianMCU;
    private javax.swing.JMenuItem MnPenilaianMCU2;
    private javax.swing.JMenuItem MnPenilaianMCU3;
    private javax.swing.JMenuItem MnPenilaianMCU4;
    private javax.swing.JMenuItem MnPenilaianMCU4B;
    private javax.swing.JMenuItem MnPenilaianMCU5;
    private javax.swing.JMenuItem MnPenilaianMCU5B;
    private javax.swing.JMenuItem MnPenilaianMCU6;
    private javax.swing.JMenuItem MnPenilaianMCU6B;
    private javax.swing.JMenuItem MnPenilaianMCU6BUMS;
    private javax.swing.JMenuItem MnPenilaianMCU6C;
    private javax.swing.JMenuItem MnPenilaianMCU7;
    private javax.swing.JMenuItem MnPenilaianMCUBPAFK;
    private javax.swing.JMenuItem MnPenilaianMCUJepang;
    private javax.swing.JMenuItem MnPenilaianMCUJepangB;
    private javax.swing.JMenuItem MnPenilaianMCULN;
    private javax.swing.JMenuItem MnPenilaianMCUNuansa;
    private javax.swing.JMenuItem MnPenilaianMCUPolkesta;
    private widget.TextBox Nadi;
    private widget.TextBox NmDokter;
    private widget.ComboBox NyeriKtok;
    private widget.ComboBox NyeriMastoideus;
    private widget.ComboBox NyeriTekanSinusFrontalis;
    private widget.ComboBox NyeriTekananSinusMaxilaris;
    private widget.ComboBox Oedema;
    private widget.ComboBox Palpasi;
    private widget.ComboBox Palpebra;
    private widget.TextArea PemeriksaanLaboratorium;
    private widget.ComboBox PerkusiAbdomen;
    private widget.ComboBox PerkusiDada;
    private widget.ComboBox Pupil;
    private widget.TextBox RR;
    private widget.TextBox RiwayatAlergiMakanan;
    private widget.TextArea RiwayatPenyakitDahulu;
    private widget.TextArea RiwayatPenyakitKeluarga;
    private widget.TextArea RiwayatPenyakitSekarang;
    private widget.TextArea RongsenThorax;
    private widget.ScrollPane Scroll;
    private widget.ComboBox SelaputPendengaran;
    private widget.ComboBox SeptumNasi;
    private widget.ComboBox Sklera;
    private widget.TextArea Spirometri;
    private widget.ComboBox Submandibula;
    private widget.TextBox Suhu;
    private widget.ComboBox Supraklavikula;
    private widget.TextBox TCari;
    private widget.TextBox TD;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.ComboBox TestButaWarna;
    private widget.Tanggal TglAsuhan;
    private widget.TextBox TglLahir;
    private widget.TextBox TinggiBadan;
    private widget.ComboBox Tonsil;
    private widget.TextArea Treadmill;
    private widget.ComboBox VocalFremitus;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel100;
    private widget.Label jLabel101;
    private widget.Label jLabel102;
    private widget.Label jLabel103;
    private widget.Label jLabel104;
    private widget.Label jLabel105;
    private widget.Label jLabel106;
    private widget.Label jLabel107;
    private widget.Label jLabel108;
    private widget.Label jLabel109;
    private widget.Label jLabel11;
    private widget.Label jLabel110;
    private widget.Label jLabel111;
    private widget.Label jLabel112;
    private widget.Label jLabel113;
    private widget.Label jLabel114;
    private widget.Label jLabel115;
    private widget.Label jLabel116;
    private widget.Label jLabel117;
    private widget.Label jLabel118;
    private widget.Label jLabel119;
    private widget.Label jLabel12;
    private widget.Label jLabel13;
    private widget.Label jLabel15;
    private widget.Label jLabel16;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel29;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel48;
    private widget.Label jLabel49;
    private widget.Label jLabel50;
    private widget.Label jLabel51;
    private widget.Label jLabel52;
    private widget.Label jLabel53;
    private widget.Label jLabel54;
    private widget.Label jLabel55;
    private widget.Label jLabel56;
    private widget.Label jLabel57;
    private widget.Label jLabel58;
    private widget.Label jLabel59;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel61;
    private widget.Label jLabel62;
    private widget.Label jLabel63;
    private widget.Label jLabel64;
    private widget.Label jLabel65;
    private widget.Label jLabel66;
    private widget.Label jLabel67;
    private widget.Label jLabel68;
    private widget.Label jLabel69;
    private widget.Label jLabel7;
    private widget.Label jLabel70;
    private widget.Label jLabel71;
    private widget.Label jLabel72;
    private widget.Label jLabel73;
    private widget.Label jLabel74;
    private widget.Label jLabel75;
    private widget.Label jLabel76;
    private widget.Label jLabel77;
    private widget.Label jLabel78;
    private widget.Label jLabel79;
    private widget.Label jLabel8;
    private widget.Label jLabel80;
    private widget.Label jLabel81;
    private widget.Label jLabel82;
    private widget.Label jLabel83;
    private widget.Label jLabel84;
    private widget.Label jLabel85;
    private widget.Label jLabel86;
    private widget.Label jLabel87;
    private widget.Label jLabel88;
    private widget.Label jLabel89;
    private widget.Label jLabel9;
    private widget.Label jLabel90;
    private widget.Label jLabel91;
    private widget.Label jLabel92;
    private widget.Label jLabel93;
    private widget.Label jLabel94;
    private widget.Label jLabel95;
    private widget.Label jLabel96;
    private widget.Label jLabel97;
    private widget.Label jLabel98;
    private widget.Label jLabel99;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator17;
    private javax.swing.JSeparator jSeparator18;
    private javax.swing.JSeparator jSeparator19;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private widget.Label label11;
    private widget.Label label14;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane10;
    private widget.ScrollPane scrollPane11;
    private widget.ScrollPane scrollPane12;
    private widget.ScrollPane scrollPane13;
    private widget.ScrollPane scrollPane14;
    private widget.ScrollPane scrollPane15;
    private widget.ScrollPane scrollPane16;
    private widget.ScrollPane scrollPane17;
    private widget.ScrollPane scrollPane18;
    private widget.ScrollPane scrollPane2;
    private widget.ScrollPane scrollPane3;
    private widget.ScrollPane scrollPane4;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    private void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            //START CUSTOM
            if(TCari.getText().equals("")){
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_mcu.tanggal,"+
                        "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                        "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                        "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                        "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                        "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                        "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                        "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                        "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                        "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                        "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                        "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                        ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                        "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+        
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                        "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where "+
                        "reg_periksa.tgl_registrasi between ? and ? order by reg_periksa.tgl_registrasi,reg_periksa.no_rawat");
            }else{
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_mcu.tanggal,"+
                        "penilaian_mcu.informasi,penilaian_mcu.rps,penilaian_mcu.rpk,penilaian_mcu.rpd,penilaian_mcu.alergi,penilaian_mcu.keadaan,penilaian_mcu.kesadaran,penilaian_mcu.td,"+
                        "penilaian_mcu.nadi,penilaian_mcu.rr,penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.suhu,penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,"+
                        "penilaian_mcu.leher,penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinus_frontalis,penilaian_mcu.sinus_maxilaris,penilaian_mcu.palpebra,penilaian_mcu.sklera,"+
                        "penilaian_mcu.cornea,penilaian_mcu.buta_warna,penilaian_mcu.konjungtiva,penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,"+
                        "penilaian_mcu.selaput_pendengaran,penilaian_mcu.proc_mastoideus,penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                        "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,penilaian_mcu.gerakan_dada,"+
                        "penilaian_mcu.vocal_femitus,penilaian_mcu.perkusi_dada,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,penilaian_mcu.ictus_cordis,penilaian_mcu.bunyi_jantung,"+
                        "penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.hepar,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.limpa,"+
                        "penilaian_mcu.costovertebral,penilaian_mcu.kondisi_kulit,penilaian_mcu.ekstrimitas_atas,penilaian_mcu.ekstrimitas_atas_ket,penilaian_mcu.ekstrimitas_bawah,"+
                        "penilaian_mcu.ekstrimitas_bawah_ket,penilaian_mcu.laborat,penilaian_mcu.radiologi,penilaian_mcu.ekg,penilaian_mcu.spirometri,penilaian_mcu.audiometri,"+
                        "penilaian_mcu.treadmill,penilaian_mcu.lainlain,penilaian_mcu.merokok,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.kd_dokter,dokter.nm_dokter "+
                        ",penilaian_mcu.lingkar_perut,penilaian_mcu.kelainan_kepala,penilaian_mcu.kelainan_mata,penilaian_mcu.kelainan_telinga,penilaian_mcu.kelainan_hidung,penilaian_mcu.kelainan_leher,"+
                        "penilaian_mcu.kelainan_dada,penilaian_mcu.kelainan_abdomen,penilaian_mcu.kelainan_ekstrimitas "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                        "inner join dokter on penilaian_mcu.kd_dokter=dokter.kd_dokter where reg_periksa.tgl_registrasi between ? and ? and "+
                        "(reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
                        "penilaian_mcu.kd_dokter like ? or dokter.nm_dokter like ? or penilaian_mcu.rps like ? or penilaian_mcu.kesimpulan like ?) order by reg_periksa.tgl_registrasi,reg_periksa.no_rawat");
            }
            //END CUSTOM    
            try {
                if(TCari.getText().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    //START CUSTOM
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    //END CUSTOM
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("jk"),rs.getString("tgl_lahir"),
                        rs.getString("tanggal"),rs.getString("informasi"),rs.getString("rps"),rs.getString("rpk"),rs.getString("rpd"),rs.getString("alergi"),
                        rs.getString("keadaan"),rs.getString("kesadaran"),rs.getString("td"),rs.getString("nadi"),rs.getString("rr"),rs.getString("tb"),
                        rs.getString("bb"),rs.getString("suhu"),rs.getString("submandibula"),rs.getString("axilla"),rs.getString("supraklavikula"),
                        rs.getString("leher"),rs.getString("inguinal"),rs.getString("oedema"),rs.getString("sinus_frontalis"),rs.getString("sinus_maxilaris"),
                        rs.getString("palpebra"),rs.getString("sklera"),rs.getString("cornea"),rs.getString("buta_warna"),rs.getString("konjungtiva"),
                        rs.getString("lensa"),rs.getString("pupil"),rs.getString("lubang_telinga"),rs.getString("daun_telinga"),rs.getString("selaput_pendengaran"),
                        rs.getString("proc_mastoideus"),rs.getString("septum_nasi"),rs.getString("lubang_hidung"),rs.getString("bibir"),rs.getString("caries"),
                        rs.getString("lidah"),rs.getString("faring"),rs.getString("tonsil"),rs.getString("kelenjar_limfe"),rs.getString("kelenjar_gondok"),
                        rs.getString("gerakan_dada"),rs.getString("vocal_femitus"),rs.getString("perkusi_dada"),rs.getString("bunyi_napas"),
                        rs.getString("bunyi_tambahan"),rs.getString("ictus_cordis"),rs.getString("bunyi_jantung"),rs.getString("batas"),rs.getString("inspeksi"),
                        rs.getString("palpasi"),rs.getString("hepar"),rs.getString("perkusi_abdomen"),rs.getString("auskultasi"),rs.getString("limpa"),
                        rs.getString("costovertebral"),rs.getString("kondisi_kulit"),rs.getString("ekstrimitas_atas"),rs.getString("ekstrimitas_atas_ket"),
                        rs.getString("ekstrimitas_bawah"),rs.getString("ekstrimitas_bawah_ket"),rs.getString("laborat"),rs.getString("radiologi"),
                        rs.getString("ekg"),rs.getString("spirometri"),rs.getString("audiometri"),rs.getString("treadmill"),rs.getString("lainlain"),
                        rs.getString("merokok"),rs.getString("alkohol"),rs.getString("kesimpulan"),rs.getString("anjuran"),rs.getString("kd_dokter"),rs.getString("nm_dokter")
                        //START CUSTOM
                        ,rs.getString("lingkar_perut"),rs.getString("kelainan_kepala"),rs.getString("kelainan_mata"),rs.getString("kelainan_telinga"),rs.getString("kelainan_hidung"),
                        rs.getString("kelainan_leher"),rs.getString("kelainan_dada"),rs.getString("kelainan_abdomen"),rs.getString("kelainan_ekstrimitas")
                        //END CUSTOM    
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TglAsuhan.setDate(new Date());
        Informasi.setSelectedIndex(0);
        RiwayatPenyakitSekarang.setText("");
        RiwayatPenyakitKeluarga.setText("");
        RiwayatPenyakitDahulu.setText("");
        RiwayatAlergiMakanan.setText("");
        KeadaanUmum.setSelectedIndex(0);
        Kesadaran.setSelectedIndex(0);
        TD.setText("");
        Nadi.setText("");
        RR.setText("");
        TinggiBadan.setText("");
        BeratBadan.setText("");
        Suhu.setText("");
        Submandibula.setSelectedIndex(0);
        Axila.setSelectedIndex(0);
        Supraklavikula.setSelectedIndex(0);
        Leher.setSelectedIndex(0);
        Inguinal.setSelectedIndex(0);
        Oedema.setSelectedIndex(0);
        NyeriTekanSinusFrontalis.setSelectedIndex(0);
        NyeriTekananSinusMaxilaris.setSelectedIndex(0);
        Palpebra.setSelectedIndex(0);
        Sklera.setSelectedIndex(0);
        Cornea.setSelectedIndex(0);
        TestButaWarna.setSelectedIndex(0);
        Konjungtiva.setSelectedIndex(0);
        Lensa.setSelectedIndex(0);
        Pupil.setSelectedIndex(0);
        LubangTelinga.setSelectedIndex(0);
        DaunTelinga.setSelectedIndex(0);
        SelaputPendengaran.setSelectedIndex(0);
        NyeriMastoideus.setSelectedIndex(0);
        SeptumNasi.setSelectedIndex(0);
        LubangHidung.setSelectedIndex(0);
        Bibir.setSelectedIndex(0);
        Caries.setSelectedIndex(0);
        Lidah.setSelectedIndex(0);
        Faring.setSelectedIndex(0);
        Tonsil.setSelectedIndex(0);
        KelenjarLimfe.setSelectedIndex(0);
        KelenjarGondok.setSelectedIndex(0);
        GerakanDada.setSelectedIndex(0);
        VocalFremitus.setSelectedIndex(0);
        PerkusiDada.setSelectedIndex(0);
        BunyiNapas.setSelectedIndex(0);
        BunyiTambahan.setSelectedIndex(0);
        IctusCordis.setSelectedIndex(0);
        BunyiJantung.setSelectedIndex(0);
        Batas.setSelectedIndex(0);
        Inspeksi.setSelectedIndex(0);
        Palpasi.setSelectedIndex(0);
        Hepar.setSelectedIndex(0);
        PerkusiAbdomen.setSelectedIndex(0);
        Auskultasi.setSelectedIndex(0);
        Limpa.setSelectedIndex(0);
        NyeriKtok.setSelectedIndex(0);
        Kulit.setSelectedIndex(0);
        ExtremitasAtas.setSelectedIndex(0);
        KetExtremitasAtas.setText("");
        ExtremitasBawah.setSelectedIndex(0);
        KetExtremitasBawah.setText("");
        PemeriksaanLaboratorium.setText("");
        RongsenThorax.setText("");
        EKG.setText("");
        Spirometri.setText("");
        Audiometri.setText("");
        Treadmill.setText("");
        Lainlain.setText("");
        Merokok.setText("");
        Alkohol.setText("");
        Kesimpulan.setText("");
        Anjuran.setText("");
        //START CUSTOM
        LingkarPerut.setText("");
        KelainanKepala.setSelectedIndex(0);
        KelainanMata.setSelectedIndex(0);
        KelainanTelinga.setSelectedIndex(0);
        KelainanHidung.setSelectedIndex(0);
        KelainanLeher.setSelectedIndex(0);
        KelainanDada.setSelectedIndex(0);
        KelainanAbdomen.setSelectedIndex(0);
        KelainanEkstrimitas.setSelectedIndex(0);
        //END CUSTOM
        
        TabRawat.setSelectedIndex(0);
        Informasi.requestFocus();
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString()); 
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()); 
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString()); 
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            Informasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            RiwayatPenyakitSekarang.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            RiwayatPenyakitKeluarga.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            RiwayatPenyakitDahulu.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            RiwayatAlergiMakanan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            KeadaanUmum.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            Kesadaran.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            TD.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            Nadi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            RR.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            TinggiBadan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            BeratBadan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            Suhu.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            Submandibula.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            Axila.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            Supraklavikula.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            Leher.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            Inguinal.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            Oedema.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            NyeriTekanSinusFrontalis.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            NyeriTekananSinusMaxilaris.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            Palpebra.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),27).toString());
            Sklera.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            Cornea.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            TestButaWarna.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
            Konjungtiva.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            Lensa.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());
            Pupil.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString());
            LubangTelinga.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());
            DaunTelinga.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),35).toString());
            SelaputPendengaran.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),36).toString());
            NyeriMastoideus.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString());
            SeptumNasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),38).toString());
            LubangHidung.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),39).toString());
            Bibir.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),40).toString());
            Caries.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),41).toString());
            Lidah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),42).toString());
            Faring.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),43).toString());
            Tonsil.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString());
            KelenjarLimfe.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),45).toString());
            KelenjarGondok.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),46).toString());
            GerakanDada.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),47).toString());
            VocalFremitus.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),48).toString());
            PerkusiDada.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),49).toString());
            BunyiNapas.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),50).toString());
            BunyiTambahan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),51).toString());
            IctusCordis.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),52).toString());
            BunyiJantung.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),53).toString());
            Batas.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),54).toString());
            Inspeksi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),55).toString());
            Palpasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),56).toString());
            Hepar.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),57).toString());
            PerkusiAbdomen.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),58).toString());
            Auskultasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),59).toString());
            Limpa.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),60).toString());
            NyeriKtok.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),61).toString());
            Kulit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),62).toString());
            ExtremitasAtas.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),63).toString());
            KetExtremitasAtas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),64).toString());
            ExtremitasBawah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),65).toString());
            KetExtremitasBawah.setText(tbObat.getValueAt(tbObat.getSelectedRow(),66).toString());
            PemeriksaanLaboratorium.setText(tbObat.getValueAt(tbObat.getSelectedRow(),67).toString());
            RongsenThorax.setText(tbObat.getValueAt(tbObat.getSelectedRow(),68).toString());
            EKG.setText(tbObat.getValueAt(tbObat.getSelectedRow(),69).toString());
            Spirometri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),70).toString());
            Audiometri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),71).toString());
            Treadmill.setText(tbObat.getValueAt(tbObat.getSelectedRow(),72).toString());
            Lainlain.setText(tbObat.getValueAt(tbObat.getSelectedRow(),73).toString());
            Merokok.setText(tbObat.getValueAt(tbObat.getSelectedRow(),74).toString());
            Alkohol.setText(tbObat.getValueAt(tbObat.getSelectedRow(),75).toString());
            Kesimpulan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),76).toString());
            Anjuran.setText(tbObat.getValueAt(tbObat.getSelectedRow(),77).toString());
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),78).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),79).toString());
            Valid.SetTgl2(TglAsuhan,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            //START CUSTOM
            LingkarPerut.setText(tbObat.getValueAt(tbObat.getSelectedRow(),80).toString());
            KelainanKepala.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),81).toString());
            KelainanMata.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),82).toString());
            KelainanTelinga.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),83).toString());
            KelainanHidung.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),84).toString());
            KelainanLeher.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),85).toString());
            KelainanDada.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),86).toString());
            KelainanAbdomen.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),87).toString());
            KelainanEkstrimitas.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),88).toString());
            //END CUSTOM
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    public void setNoRm(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);    
        isRawat(); 
        //START CUSTOM
        //EKG
        text_ekg="";
        if(Sequel.cariIntegerCount("select count(no_rawat) from penilaian_ekg where no_rawat='"+norwt+"'") > 0){
            text_ekg="Kesan: "+Sequel.cariIsi("select kesan from penilaian_ekg where no_rawat=?",norwt)+
                    "\nAnjuran: "+Sequel.cariIsi("select anjuran from penilaian_ekg where no_rawat=?",norwt);            
        }

        if(!text_ekg.equals("")){
            EKG.setText(text_ekg);
        }
        //LAB
        //PEMERIKSAAN
        try {       
            text_saranlab="";
            text_kesanlab="";
            rs = koneksi.prepareStatement("select saran,kesan from saran_kesan_lab where no_rawat ='"+norwt+"'").executeQuery();
            while(rs.next()) {
                text_saranlab=text_saranlab+rs.getString("saran")+";\n";
                text_kesanlab=text_kesanlab+rs.getString("kesan")+";\n";
            }
        } catch (Exception e) {
            System.out.println("Notifikasi sarankesan: " + e);
        }         
        //DOKTER LAB
        try {       
            text_dokterlab="";
            count_dokter=0;
            rs = koneksi.prepareStatement("select nm_dokter from periksa_lab inner join dokter on periksa_lab.kd_dokter=dokter.kd_dokter where periksa_lab.no_rawat='"+norwt+"'").executeQuery();
            while(rs.next()) {
                if(!text_dokterlab.contains(rs.getString("nm_dokter"))){
                    count_dokter++;
                    if(count_dokter>1){
                        text_dokterlab="; "+rs.getString("nm_dokter");
                    }else{
                        text_dokterlab=rs.getString("nm_dokter");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Notifikasi dokterlab: " + e);
        }
        if(!text_dokterlab.equals("")){
            PemeriksaanLaboratorium.setText("Kesan: "+text_kesanlab+"\nSaran: "+text_saranlab);
        }
        //RIWAYAT PENYAKIT
        try {       
            text_rps="";
            text_rpk="";
            text_rpd="";
            text_alergi="";
            text_td="";
            text_hr="";
            text_rr="";
            text_suhu="";
            text_tb="";
            text_bb="";            
            rs = koneksi.prepareStatement("select keluhan_utama,rpd,rpk,rpo,alergi,td,nadi,rr,suhu,tb,bb from penilaian_awal_keperawatan_ralan where no_rawat='"+norwt+"'").executeQuery();
            while(rs.next()) {
                text_rps=rs.getString("keluhan_utama");
                text_rpk=rs.getString("rpk");
                text_rpd=rs.getString("rpd");
                text_alergi="Riwayat Obat: "+rs.getString("rpo")+"; Alergi: "+rs.getString("alergi");
                text_td=rs.getString("td");
                text_hr=rs.getString("nadi");
                text_rr=rs.getString("rr");
                text_suhu=rs.getString("suhu");
                text_tb=rs.getString("tb");
                text_bb=rs.getString("bb");
            }
        } catch (Exception e) {
            System.out.println("Notifikasi riwayatpenyakit & vitalsign: " + e);
        }          
        RiwayatPenyakitSekarang.setText(text_rps);
        RiwayatPenyakitKeluarga.setText(text_rpk);
        RiwayatPenyakitDahulu.setText(text_rpd);
        RiwayatAlergiMakanan.setText(text_alergi);
        TD.setText(text_td);
        Nadi.setText(text_hr);
        RR.setText(text_rr);
        TinggiBadan.setText(text_tb);
        BeratBadan.setText(text_bb);
        Suhu.setText(text_suhu);
        //RADIOLOGI
        try {       
            text_radiologi="";
            rs = koneksi.prepareStatement("select hasil from hasil_radiologi where no_rawat='"+norwt+"'").executeQuery();
            while(rs.next()) {
                text_radiologi=text_radiologi+rs.getString("hasil")+";\n";
            }
        } catch (Exception e) {
            System.out.println("Notifikasi radiologi: " + e);
        }
        //DOKTER RADIOLOGI
        try {       
            text_dokterrad="";
            count_dokterrad=0;
            rs = koneksi.prepareStatement("select nm_dokter from periksa_radiologi inner join dokter on periksa_radiologi.kd_dokter=dokter.kd_dokter where periksa_radiologi.no_rawat='"+norwt+"'").executeQuery();
            while(rs.next()) {
                if(!text_dokterrad.contains(rs.getString("nm_dokter"))){
                    count_dokterrad++;
                    if(count_dokterrad>1){
                        text_dokterrad="; "+rs.getString("nm_dokter");
                    }else{
                        text_dokterrad=rs.getString("nm_dokter");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Notifikasi dokterrad: " + e);
        }
        if(!text_dokterrad.equals("")){
            RongsenThorax.setText("Hasil:\n"+text_radiologi);
        }        
        
        Sequel.cariIsi("select kd_dokter from reg_periksa where no_rawat=?", KdDokter,norwt);
        Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?", NmDokter,KdDokter.getText());
        //DEFAULT TREADMILL
        Treadmill.setText("- Hasil Uji Latih Jantung dengan Treadmill\n"
                            +"- Tingkat Kesegaran Jasmani\n"
                            +"- Fungsional Kelas\n"
                            +"- Kapasitas Aerobik");
        //DEFAULT KESIMPULAN
        Kesimpulan.setText("FIT UNTUK BEKERJA");
        //DEFAULT ANJURAN
        Anjuran.setText("POLA HIDUP SEHAT & SEIMBANG");
        LingkarPerut.setText(Sequel.cariIsi("select lingkar_perut from penilaian_awal_keperawatan_ralan_tambahan where no_rawat=?",norwt));
        //END CUSTOM
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_mcu());
        BtnHapus.setEnabled(akses.getpenilaian_mcu());
        BtnEdit.setEnabled(akses.getpenilaian_mcu());
        if(akses.getjml2()>=1){
            KdDokter.setEditable(false);
            BtnDokter.setEnabled(false);
            //START CUSTOM
//            KdDokter.setText(akses.getkode());
//            Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?", NmDokter,KdDokter.getText());
//            if(NmDokter.getText().equals("")){
//                KdDokter.setText("");
//                JOptionPane.showMessageDialog(null,"User login bukan dokter...!!");
//            }
            //END CUSTOM
        }            
    }

    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }
    
    private void hapus() {
        if(Sequel.queryu2tf("delete from penilaian_mcu where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            tampil();
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }
    //START CUSTOM
    private void ganti() {
        if(Sequel.mengedittf("penilaian_mcu","no_rawat=?","no_rawat=?,tanggal=?,kd_dokter=?,informasi=?,rps=?,rpk=?,rpd=?,alergi=?,keadaan=?,kesadaran=?,td=?,nadi=?,rr=?,tb=?,bb=?,suhu=?,submandibula=?,axilla=?,supraklavikula=?,leher=?,inguinal=?,oedema=?,sinus_frontalis=?,"+
                "sinus_maxilaris=?,palpebra=?,sklera=?,cornea=?,buta_warna=?,konjungtiva=?,lensa=?,pupil=?,lubang_telinga=?,daun_telinga=?,selaput_pendengaran=?,proc_mastoideus=?,septum_nasi=?,lubang_hidung=?,bibir=?,caries=?,lidah=?,faring=?,tonsil=?,kelenjar_limfe=?,"+
                "kelenjar_gondok=?,gerakan_dada=?,vocal_femitus=?,perkusi_dada=?,bunyi_napas=?,bunyi_tambahan=?,ictus_cordis=?,bunyi_jantung=?,batas=?,inspeksi=?,palpasi=?,hepar=?,perkusi_abdomen=?,auskultasi=?,limpa=?,costovertebral=?,kondisi_kulit=?,ekstrimitas_atas=?,"+
                "ekstrimitas_atas_ket=?,ekstrimitas_bawah=?,ekstrimitas_bawah_ket=?,laborat=?,radiologi=?,ekg=?,spirometri=?,audiometri=?,treadmill=?,lainlain=?,merokok=?,alkohol=?,kesimpulan=?,anjuran=?"+ 
                ",lingkar_perut=?,kelainan_kepala=?,kelainan_mata=?,kelainan_telinga=?,kelainan_hidung=?,kelainan_leher=?,kelainan_dada=?,kelainan_abdomen=?,kelainan_ekstrimitas=?",85,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglAsuhan.getSelectedItem()+"")+" "+TglAsuhan.getSelectedItem().toString().substring(11,19),KdDokter.getText(),Informasi.getSelectedItem().toString(),RiwayatPenyakitSekarang.getText(),RiwayatPenyakitKeluarga.getText(), 
                RiwayatPenyakitDahulu.getText(),RiwayatAlergiMakanan.getText(),KeadaanUmum.getSelectedItem().toString(),Kesadaran.getSelectedItem().toString(),TD.getText(),Nadi.getText(),RR.getText(),TinggiBadan.getText(),BeratBadan.getText(),Suhu.getText(),
                Submandibula.getSelectedItem().toString(),Axila.getSelectedItem().toString(),Supraklavikula.getSelectedItem().toString(),Leher.getSelectedItem().toString(),Inguinal.getSelectedItem().toString(),Oedema.getSelectedItem().toString(),
                NyeriTekanSinusFrontalis.getSelectedItem().toString(),NyeriTekananSinusMaxilaris.getSelectedItem().toString(),Palpebra.getSelectedItem().toString(),Sklera.getSelectedItem().toString(),Cornea.getSelectedItem().toString(),
                TestButaWarna.getSelectedItem().toString(),Konjungtiva.getSelectedItem().toString(),Lensa.getSelectedItem().toString(),Pupil.getSelectedItem().toString(),LubangTelinga.getSelectedItem().toString(),DaunTelinga.getSelectedItem().toString(), 
                SelaputPendengaran.getSelectedItem().toString(),NyeriMastoideus.getSelectedItem().toString(),SeptumNasi.getSelectedItem().toString(),LubangHidung.getSelectedItem().toString(),Bibir.getSelectedItem().toString(),Caries.getSelectedItem().toString(), 
                Lidah.getSelectedItem().toString(),Faring.getSelectedItem().toString(),Tonsil.getSelectedItem().toString(),KelenjarLimfe.getSelectedItem().toString(),KelenjarGondok.getSelectedItem().toString(),GerakanDada.getSelectedItem().toString(), 
                VocalFremitus.getSelectedItem().toString(),PerkusiDada.getSelectedItem().toString(),BunyiNapas.getSelectedItem().toString(),BunyiTambahan.getSelectedItem().toString(),IctusCordis.getSelectedItem().toString(), 
                BunyiJantung.getSelectedItem().toString(),Batas.getSelectedItem().toString(),Inspeksi.getSelectedItem().toString(),Palpasi.getSelectedItem().toString(),Hepar.getSelectedItem().toString(),PerkusiAbdomen.getSelectedItem().toString(),
                Auskultasi.getSelectedItem().toString(),Limpa.getSelectedItem().toString(),NyeriKtok.getSelectedItem().toString(),Kulit.getSelectedItem().toString(),ExtremitasAtas.getSelectedItem().toString(),KetExtremitasAtas.getText(), 
                ExtremitasBawah.getSelectedItem().toString(),KetExtremitasBawah.getText(),PemeriksaanLaboratorium.getText(),RongsenThorax.getText(),EKG.getText(),Spirometri.getText(),Audiometri.getText(),Treadmill.getText(),Lainlain.getText(),
                Merokok.getText(),Alkohol.getText(),Kesimpulan.getText(),Anjuran.getText(),LingkarPerut.getText(),KelainanKepala.getSelectedItem().toString(),KelainanMata.getSelectedItem().toString(),
                KelainanTelinga.getSelectedItem().toString(),KelainanHidung.getSelectedItem().toString(),KelainanLeher.getSelectedItem().toString(),KelainanDada.getSelectedItem().toString(),
                KelainanAbdomen.getSelectedItem().toString(),KelainanEkstrimitas.getSelectedItem().toString(),
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
             })==true){
                tampil();
                emptTeks();
                TabRawat.setSelectedIndex(1);
        }
    }
    //END CUSTOM
}
