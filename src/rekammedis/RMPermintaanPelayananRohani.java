/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DlgRujuk.java
 *
 * Created on 31 Mei 10, 20:19:56
 */

package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariPetugas;
import keuangan.DlgKamar;


/**
 *
 * @author perpustakaan
 */
public final class RMPermintaanPelayananRohani extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;    
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private String tgl,finger="",kodedokter="",namadokter="",kodepetugas="",namapetugas="";
    private String TANGGALMUNDUR="yes";
    private DlgKamar kamar=new DlgKamar(null,false);
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMPermintaanPelayananRohani(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(628,674);
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","Tanggal","Jam","No.R.M.","Nama Pasien","Nama Penanggung Jawab","Tgl. Lahir Penanggung Jawab","Umur Penanggung Jawab","Alamat Penanggung Jawab","Permintaan Penampingan Agama","Untuk","Ruangan","NIP","Petugas","Petugas Rohani"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        tbObat.setPreferredScrollableViewportSize(new Dimension(500,300));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 15; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(65); 
            }else if(i==2){
                column.setPreferredWidth(65);
            }else if(i==3){
                column.setPreferredWidth(160); 
            }else if(i==4){
                column.setPreferredWidth(120); 
            }else if(i==5){
                column.setPreferredWidth(120);
            }else if(i==6){
                column.setPreferredWidth(150);
            }else if(i==7){
                column.setPreferredWidth(100);
            }else if(i==8){
                column.setPreferredWidth(100); 
            }else if(i==9){
                column.setPreferredWidth(120); 
            }else if(i==10){
                column.setPreferredWidth(120);
            }else if(i==11){
                column.setPreferredWidth(120); 
            }else if(i==12){
                column.setPreferredWidth(150);
            }else if(i==13){
                column.setPreferredWidth(150);
            }else if(i==14){
                column.setPreferredWidth(150);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        KdPetugas.setDocument(new batasInput((byte)20).getKata(KdPetugas));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
    
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){                   
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                }  
                KdPetugas.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        kamar.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(kamar.getTable().getSelectedRow()!= -1){   
                    nama_pj.setText(kamar.getTable().getValueAt(kamar.getTable().getSelectedRow(),4).toString());
                }  
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        kamar.getTable().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_SPACE){
                    kamar.dispose();
                }
            }
            @Override
            public void keyReleased(KeyEvent e) {}
        }); 
        
        
        ChkInput.setSelected(false);
        isForm();
        
        jam();
        
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnPermintaanPelayananRohani = new javax.swing.JMenuItem();
        JK = new widget.TextBox();
        Umur = new widget.TextBox();
        TanggalRegistrasi = new widget.TextBox();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        jPanel3 = new javax.swing.JPanel();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        BtnKeluar = new widget.Button();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        PanelInput = new javax.swing.JPanel();
        ChkInput = new widget.CekBox();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        jLabel4 = new widget.Label();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        Tanggal = new widget.Tanggal();
        TNoRM = new widget.TextBox();
        jLabel16 = new widget.Label();
        Jam = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Detik = new widget.ComboBox();
        ChkKejadian = new widget.CekBox();
        jLabel18 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        btnPetugas = new widget.Button();
        jLabel25 = new widget.Label();
        cbUntuk = new widget.ComboBox();
        nama_pj = new widget.TextBox();
        jLabel28 = new widget.Label();
        jLabel29 = new widget.Label();
        jLabel30 = new widget.Label();
        scrollPane2 = new widget.ScrollPane();
        alamat = new widget.TextArea();
        jLabel31 = new widget.Label();
        umur = new widget.TextBox();
        jLabel32 = new widget.Label();
        C1 = new widget.CekBox();
        jLabel33 = new widget.Label();
        jLabel34 = new widget.Label();
        C2 = new widget.CekBox();
        jLabel35 = new widget.Label();
        C3 = new widget.CekBox();
        jLabel36 = new widget.Label();
        C4 = new widget.CekBox();
        jLabel37 = new widget.Label();
        C5 = new widget.CekBox();
        jLabel38 = new widget.Label();
        C7 = new widget.CekBox();
        NoRMPasien = new widget.TextBox();
        jLabel39 = new widget.Label();
        jLabel40 = new widget.Label();
        jLabel42 = new widget.Label();
        agama_lainnya = new widget.TextBox();
        Namapasien = new widget.TextBox();
        jLabel43 = new widget.Label();
        C6 = new widget.CekBox();
        TglLahir = new widget.Tanggal();
        jLabel41 = new widget.Label();
        RuangPasien = new widget.TextBox();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel26 = new widget.Label();
        nama_petugas_pembimbing = new widget.TextBox();

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnPermintaanPelayananRohani.setBackground(new java.awt.Color(255, 255, 254));
        MnPermintaanPelayananRohani.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPermintaanPelayananRohani.setForeground(new java.awt.Color(50, 50, 50));
        MnPermintaanPelayananRohani.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPermintaanPelayananRohani.setText("Formulir Permintaan Pelayanan Rohani");
        MnPermintaanPelayananRohani.setName("MnPermintaanPelayananRohani"); // NOI18N
        MnPermintaanPelayananRohani.setPreferredSize(new java.awt.Dimension(240, 26));
        MnPermintaanPelayananRohani.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPermintaanPelayananRohaniActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPermintaanPelayananRohani);

        JK.setHighlighter(null);
        JK.setName("JK"); // NOI18N

        Umur.setHighlighter(null);
        Umur.setName("Umur"); // NOI18N

        TanggalRegistrasi.setHighlighter(null);
        TanggalRegistrasi.setName("TanggalRegistrasi"); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Formulir Permintaan Pelayanan Rohani ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(44, 100));
        jPanel3.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass8.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass8.add(LCount);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        jPanel3.add(panelGlass8, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tanggal :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(310, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnAll);

        jPanel3.add(panelGlass9, java.awt.BorderLayout.PAGE_START);

        internalFrame1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        PanelInput.setToolTipText("");
        PanelInput.setName("PanelInput"); // NOI18N
        PanelInput.setOpaque(false);
        PanelInput.setPreferredSize(new java.awt.Dimension(192, 306));
        PanelInput.setLayout(new java.awt.BorderLayout(1, 1));

        ChkInput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setMnemonic('I');
        ChkInput.setText(".: Input Data");
        ChkInput.setToolTipText("Alt+I");
        ChkInput.setBorderPainted(true);
        ChkInput.setBorderPaintedFlat(true);
        ChkInput.setFocusable(false);
        ChkInput.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ChkInput.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ChkInput.setName("ChkInput"); // NOI18N
        ChkInput.setPreferredSize(new java.awt.Dimension(192, 20));
        ChkInput.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkInputActionPerformed(evt);
            }
        });
        PanelInput.add(ChkInput, java.awt.BorderLayout.PAGE_END);

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(250, 255, 245));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(100, 630));
        FormInput.setLayout(null);

        jLabel4.setText("No.Rawat :");
        jLabel4.setName("jLabel4"); // NOI18N
        FormInput.add(jLabel4);
        jLabel4.setBounds(0, 10, 75, 23);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(79, 10, 141, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        TPasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TPasienKeyPressed(evt);
            }
        });
        FormInput.add(TPasien);
        TPasien.setBounds(336, 10, 450, 23);

        Tanggal.setForeground(new java.awt.Color(50, 70, 50));
        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TanggalKeyPressed(evt);
            }
        });
        FormInput.add(Tanggal);
        Tanggal.setBounds(80, 40, 90, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        TNoRM.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRMKeyPressed(evt);
            }
        });
        FormInput.add(TNoRM);
        TNoRM.setBounds(222, 10, 112, 23);

        jLabel16.setText("Tanggal :");
        jLabel16.setName("jLabel16"); // NOI18N
        jLabel16.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel16);
        jLabel16.setBounds(0, 40, 75, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setName("Jam"); // NOI18N
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        FormInput.add(Jam);
        Jam.setBounds(173, 40, 62, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        FormInput.add(Menit);
        Menit.setBounds(238, 40, 62, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        FormInput.add(Detik);
        Detik.setBounds(303, 40, 62, 23);

        ChkKejadian.setBorder(null);
        ChkKejadian.setSelected(true);
        ChkKejadian.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkKejadian.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkKejadian.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkKejadian.setName("ChkKejadian"); // NOI18N
        FormInput.add(ChkKejadian);
        ChkKejadian.setBounds(370, 40, 23, 23);

        jLabel18.setText("Petugas :");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(390, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setHighlighter(null);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(470, 40, 94, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(570, 40, 180, 23);

        btnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnPetugas.setMnemonic('2');
        btnPetugas.setToolTipText("ALt+2");
        btnPetugas.setName("btnPetugas"); // NOI18N
        btnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPetugasActionPerformed(evt);
            }
        });
        btnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPetugasKeyPressed(evt);
            }
        });
        FormInput.add(btnPetugas);
        btnPetugas.setBounds(758, 40, 28, 23);

        jLabel25.setText("Nama PJ / Keluarga :");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(0, 120, 110, 23);

        cbUntuk.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Saya", "Suami", "Istri", "Ayah", "Ibu", "Anak", "Keluarga" }));
        cbUntuk.setName("cbUntuk"); // NOI18N
        cbUntuk.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbUntukKeyPressed(evt);
            }
        });
        FormInput.add(cbUntuk);
        cbUntuk.setBounds(100, 430, 130, 23);

        nama_pj.setHighlighter(null);
        nama_pj.setName("nama_pj"); // NOI18N
        FormInput.add(nama_pj);
        nama_pj.setBounds(120, 120, 280, 23);

        jLabel28.setText("Untuk :");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(20, 430, 70, 23);

        jLabel29.setText("Islam");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(30, 270, 60, 23);

        jLabel30.setText("Umur :");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(220, 150, 50, 23);

        scrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane2.setName("scrollPane2"); // NOI18N

        alamat.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        alamat.setColumns(20);
        alamat.setRows(5);
        alamat.setName("alamat"); // NOI18N
        alamat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                alamatKeyPressed(evt);
            }
        });
        scrollPane2.setViewportView(alamat);

        FormInput.add(scrollPane2);
        scrollPane2.setBounds(120, 180, 280, 50);

        jLabel31.setText("Tanggal Lahir :");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(10, 150, 100, 23);

        umur.setHighlighter(null);
        umur.setName("umur"); // NOI18N
        FormInput.add(umur);
        umur.setBounds(280, 150, 120, 23);

        jLabel32.setText("Yang bertanda tangan dibawah ini saya :");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(20, 90, 210, 23);

        C1.setBorder(null);
        buttonGroup2.add(C1);
        C1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        C1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        C1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        C1.setName("C1"); // NOI18N
        FormInput.add(C1);
        C1.setBounds(100, 270, 23, 23);

        jLabel33.setText("Alamat :");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(10, 190, 100, 23);

        jLabel34.setText("Katolik");
        jLabel34.setName("jLabel34"); // NOI18N
        FormInput.add(jLabel34);
        jLabel34.setBounds(30, 290, 60, 23);

        C2.setBorder(null);
        buttonGroup2.add(C2);
        C2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        C2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        C2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        C2.setName("C2"); // NOI18N
        FormInput.add(C2);
        C2.setBounds(100, 290, 23, 23);

        jLabel35.setText("Kristen Protestan");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(0, 310, 100, 23);

        C3.setBorder(null);
        buttonGroup2.add(C3);
        C3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        C3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        C3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        C3.setName("C3"); // NOI18N
        FormInput.add(C3);
        C3.setBounds(100, 310, 23, 23);

        jLabel36.setText("Hindu");
        jLabel36.setName("jLabel36"); // NOI18N
        FormInput.add(jLabel36);
        jLabel36.setBounds(0, 330, 90, 23);

        C4.setBorder(null);
        buttonGroup2.add(C4);
        C4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        C4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        C4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        C4.setName("C4"); // NOI18N
        FormInput.add(C4);
        C4.setBounds(100, 330, 23, 23);

        jLabel37.setText("Budha");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(30, 350, 60, 23);

        C5.setBorder(null);
        buttonGroup2.add(C5);
        C5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        C5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        C5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        C5.setName("C5"); // NOI18N
        FormInput.add(C5);
        C5.setBounds(100, 350, 23, 23);

        jLabel38.setText("Khonghucu");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(10, 370, 80, 23);

        C7.setBorder(null);
        buttonGroup2.add(C7);
        C7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        C7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        C7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        C7.setName("C7"); // NOI18N
        FormInput.add(C7);
        C7.setBounds(100, 390, 23, 23);

        NoRMPasien.setEditable(false);
        NoRMPasien.setHighlighter(null);
        NoRMPasien.setName("NoRMPasien"); // NOI18N
        FormInput.add(NoRMPasien);
        NoRMPasien.setBounds(100, 490, 280, 23);

        jLabel39.setText("Lainnya ");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(40, 390, 50, 23);

        jLabel40.setText("No. RM :");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(30, 490, 60, 23);

        jLabel42.setText("Nama Pasien :");
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(10, 460, 80, 23);

        agama_lainnya.setHighlighter(null);
        agama_lainnya.setName("agama_lainnya"); // NOI18N
        FormInput.add(agama_lainnya);
        agama_lainnya.setBounds(130, 390, 130, 23);

        Namapasien.setEditable(false);
        Namapasien.setHighlighter(null);
        Namapasien.setName("Namapasien"); // NOI18N
        FormInput.add(Namapasien);
        Namapasien.setBounds(100, 460, 280, 23);

        jLabel43.setText("dengan ini mengajukan permintaan pelayanan rohaniawan agama :");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(30, 240, 330, 23);

        C6.setBorder(null);
        buttonGroup2.add(C6);
        C6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        C6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        C6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        C6.setName("C6"); // NOI18N
        FormInput.add(C6);
        C6.setBounds(100, 370, 23, 23);

        TglLahir.setForeground(new java.awt.Color(50, 70, 50));
        TglLahir.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024" }));
        TglLahir.setDisplayFormat("dd-MM-yyyy");
        TglLahir.setName("TglLahir"); // NOI18N
        TglLahir.setOpaque(false);
        TglLahir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglLahirKeyPressed(evt);
            }
        });
        FormInput.add(TglLahir);
        TglLahir.setBounds(120, 150, 100, 23);

        jLabel41.setText("Ruang :");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(30, 520, 60, 23);

        RuangPasien.setHighlighter(null);
        RuangPasien.setName("RuangPasien"); // NOI18N
        FormInput.add(RuangPasien);
        RuangPasien.setBounds(100, 520, 280, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 81, 880, 3);

        jSeparator2.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator2.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator2.setName("jSeparator2"); // NOI18N
        FormInput.add(jSeparator2);
        jSeparator2.setBounds(0, 561, 880, 3);

        jLabel26.setText("Nama Petugas Pembimbing Rohani :");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(40, 580, 180, 23);

        nama_petugas_pembimbing.setHighlighter(null);
        nama_petugas_pembimbing.setName("nama_petugas_pembimbing"); // NOI18N
        FormInput.add(nama_petugas_pembimbing);
        nama_petugas_pembimbing.setBounds(230, 580, 280, 23);

        scrollInput.setViewportView(FormInput);

        PanelInput.add(scrollInput, java.awt.BorderLayout.CENTER);

        internalFrame1.add(PanelInput, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);
        internalFrame1.getAccessibleContext().setAccessibleName("::[ Permintaan Pelayanan Gizi ]::");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{            
            Valid.pindah(evt,TCari,Tanggal);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void TPasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TPasienKeyPressed
        Valid.pindah(evt,TCari,BtnSimpan);
}//GEN-LAST:event_TPasienKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(KdPetugas.getText().trim().equals("")||NmPetugas.getText().trim().equals("")){
            Valid.textKosong(KdPetugas,"Petugas");
        }else{
            simpan();
           
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
          
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
        ChkInput.setSelected(true);
        isForm(); 
        
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(KdPetugas.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }   
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(KdPetugas.getText().trim().equals("")||NmPetugas.getText().trim().equals("")){
            Valid.textKosong(KdPetugas,"Petugas");
        }else{ 
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                    emptTeks();
                }else{
                    // CUSTOM ANGGIT
                    // DI BUKA UNTUK SEMUA PETUGAS MENGEDIT PETUGAS ROHANIAWANNYA
                    ganti();
                    emptTeks();
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            } 
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        petugas.dispose();
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        if(tbObat.getRowCount()!=0){
            Map<String, Object> param = new HashMap<>();
            String tanggalDari = DTPCari1.getSelectedItem().toString();
            String tanggalSampai = DTPCari2.getSelectedItem().toString();
            param.put("sampai",tanggalDari);
            param.put("dengan",tanggalSampai);
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());          
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan")); 
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            
            Valid.MyReportqry("rptDataPermintaanRohani.jasper","report","::[ Formulir Permintaan Pelayanan Rohani ]::",
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.tgl_lahir,if(pasien.jk='L','Laki-laki','Perempuan') as jk,CONCAT( pasien.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) AS alamat, "+
                    "permintaan_pelayananrohani.*,petugas.nip,petugas.nama,CASE reg_periksa.status_lanjut WHEN 'Ralan' THEN d2.nm_dokter WHEN 'Ranap' THEN d1.nm_dokter END AS dpjp "+
                    "from permintaan_pelayananrohani inner join reg_periksa on permintaan_pelayananrohani.no_rawat=reg_periksa.no_rawat "+
                    "left join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "left join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli " +
                    "left join petugas on permintaan_pelayananrohani.kd_petugas=petugas.nip "+ 
                    "left join dpjp_ranap on dpjp_ranap.no_rawat = reg_periksa.no_rawat "+
                    "left join dokter as d1 on d1.kd_dokter = dpjp_ranap.kd_dokter "+
                    "left join dokter as d2 on d2.kd_dokter = reg_periksa.kd_dokter "+
                    "INNER JOIN kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                    "INNER JOIN kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                    "INNER JOIN kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                    "where permintaan_pelayananrohani.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+ "' and '" +Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' ",param);         
        } else{
             JOptionPane.showMessageDialog(rootPane,"Maaf Data Tidak Ditemukan..");
        }
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            tampil();
            TCari.setText("");
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void TanggalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TanggalKeyPressed
        Valid.pindah2(evt,TCari,Jam);
}//GEN-LAST:event_TanggalKeyPressed

    private void TNoRMKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRMKeyPressed
        // Valid.pindah(evt, TNm, BtnSimpan);
}//GEN-LAST:event_TNoRMKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void ChkInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkInputActionPerformed
        isForm();
    }//GEN-LAST:event_ChkInputActionPerformed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
        Valid.pindah(evt,Tanggal,Menit);
    }//GEN-LAST:event_JamKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        Valid.pindah(evt,Jam,Detik);
    }//GEN-LAST:event_MenitKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        Valid.pindah(evt,Menit,btnPetugas);
    }//GEN-LAST:event_DetikKeyPressed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            NmPetugas.setText(petugas.tampil3(KdPetugas.getText()));
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            Detik.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
            btnPetugasActionPerformed(null);
        }
    }//GEN-LAST:event_KdPetugasKeyPressed

    private void btnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPetugasActionPerformed
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_btnPetugasActionPerformed

    private void btnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPetugasKeyPressed
       
    }//GEN-LAST:event_btnPetugasKeyPressed

    private void MnPermintaanPelayananRohaniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPermintaanPelayananRohaniActionPerformed
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());          
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan")); 
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan")); 
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),13).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),12).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString())); 
            Valid.MyReportqry("rptCetakPermintaanRohani.jasper","report","::[ Formulir Permintaan Pelayanan Rohani ]::",
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.tgl_lahir,if(pasien.jk='L','Laki-laki','Perempuan') as jk,CONCAT( pasien.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) AS alamat, "+
                    "permintaan_pelayananrohani.*,petugas.nip,petugas.nama,CASE reg_periksa.status_lanjut WHEN 'Ralan' THEN d2.nm_dokter WHEN 'Ranap' THEN d1.nm_dokter END AS dpjp "+
                    "from permintaan_pelayananrohani inner join reg_periksa on permintaan_pelayananrohani.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli " +
                    "inner join petugas on permintaan_pelayananrohani.kd_petugas=petugas.nip "+ 
                    "left join dpjp_ranap on dpjp_ranap.no_rawat = reg_periksa.no_rawat "+
                    "left join dokter as d1 on d1.kd_dokter = dpjp_ranap.kd_dokter "+
                    "left join dokter as d2 on d2.kd_dokter = reg_periksa.kd_dokter "+
                    "INNER JOIN kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                    "INNER JOIN kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                    "INNER JOIN kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                    "where permintaan_pelayananrohani.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and permintaan_pelayananrohani.tanggal='"+tbObat.getValueAt(tbObat.getSelectedRow(),1).toString()+"' and permintaan_pelayananrohani.jam='"+tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()+"' ",param);
        }
    }//GEN-LAST:event_MnPermintaanPelayananRohaniActionPerformed

    private void cbUntukKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbUntukKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbUntukKeyPressed

    private void alamatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_alamatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_alamatKeyPressed

    private void TglLahirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglLahirKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglLahirKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMPermintaanPelayananRohani dialog = new RMPermintaanPelayananRohani(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.CekBox C1;
    private widget.CekBox C2;
    private widget.CekBox C3;
    private widget.CekBox C4;
    private widget.CekBox C5;
    private widget.CekBox C6;
    private widget.CekBox C7;
    private widget.CekBox ChkInput;
    private widget.CekBox ChkKejadian;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox Detik;
    private widget.PanelBiasa FormInput;
    private widget.TextBox JK;
    private widget.ComboBox Jam;
    private widget.TextBox KdPetugas;
    private widget.Label LCount;
    private widget.ComboBox Menit;
    private javax.swing.JMenuItem MnPermintaanPelayananRohani;
    private widget.TextBox Namapasien;
    private widget.TextBox NmPetugas;
    private widget.TextBox NoRMPasien;
    private javax.swing.JPanel PanelInput;
    private widget.TextBox RuangPasien;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.Tanggal Tanggal;
    private widget.TextBox TanggalRegistrasi;
    private widget.Tanggal TglLahir;
    private widget.TextBox Umur;
    private widget.TextBox agama_lainnya;
    private widget.TextArea alamat;
    private widget.Button btnPetugas;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private widget.ComboBox cbUntuk;
    private widget.InternalFrame internalFrame1;
    private widget.Label jLabel16;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel28;
    private widget.Label jLabel29;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel4;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private widget.TextBox nama_petugas_pembimbing;
    private widget.TextBox nama_pj;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane2;
    private widget.Table tbObat;
    private widget.TextBox umur;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().toString().trim().equals("")){
                ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,"+
                    "permintaan_pelayananrohani.*,petugas.nama "+
                    "from permintaan_pelayananrohani inner join reg_periksa on permintaan_pelayananrohani.no_rawat = reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis = pasien.no_rkm_medis "+
                    "inner join petugas on permintaan_pelayananrohani.kd_petugas=petugas.nip where "+
                    "permintaan_pelayananrohani.tanggal between ? and ? order by permintaan_pelayananrohani.tanggal ");
            }else{
                ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,"+
                    "permintaan_pelayananrohani.*, petugas.nama "+
                    "from permintaan_pelayananrohani inner join reg_periksa on permintaan_pelayananrohani.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join petugas on permintaan_pelayananrohani.kd_petugas=petugas.nip where "+
                    "permintaan_pelayananrohani.tanggal between ? and ? and "+
                    "(reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
                    "permintaan_pelayananrohani.kd_petugas like ? or petugas.nama like ? or permintaan_pelayananrohani.nm_petugas_rohani like ? ) "+
                    "order by permintaan_pelayananrohani.tanggal ");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem().toString()+""));
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem().toString()+""));
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem().toString()+""));
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem().toString()+""));
                    ps.setString(3,"%"+TCari.getText().trim()+"%");
                    ps.setString(4,"%"+TCari.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText().trim()+"%");
                    ps.setString(6,"%"+TCari.getText().trim()+"%");
                    ps.setString(7,"%"+TCari.getText().trim()+"%");
                    ps.setString(8,"%"+TCari.getText().trim()+"%");
                   
                }
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("nama_pj"),
                        rs.getString("tgl_lahir_pj"),
                        rs.getString("umur_pj"),
                        rs.getString("alamat_pj"),
                        rs.getString("agama_diminta_pj"),
                        rs.getString("untuk"),
                        rs.getString("ruang"),
                        rs.getString("kd_petugas"),
                        rs.getString("nama"),
                        rs.getString("nm_petugas_rohani")
                        
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {  
        nama_pj.setText("");
        umur.setText("");
        alamat.setText("");
        cbUntuk.setSelectedIndex(0);
        KdPetugas.setText("");
        NmPetugas.setText("");
        Tanggal.setDate(new Date());
        agama_lainnya.setText("");
        C1.setSelected(false);
        C2.setSelected(false);
        C3.setSelected(false);
        C4.setSelected(false);
        C5.setSelected(false);
        C6.setSelected(false);
        C7.setSelected(false);
        nama_petugas_pembimbing.setText("");
        ChkKejadian.setSelected(true);
    } 

    private void getData() {
    if(tbObat.getSelectedRow()!= -1){
            ChkKejadian.setSelected(false);
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            Valid.SetTgl(Tanggal,tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            Jam.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString().substring(0,2));
            Menit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString().substring(3,5));
            Detik.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString().substring(6,8));
            NoRMPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());
            Namapasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());          
            nama_pj.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            Valid.SetTgl(TglLahir,tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            umur.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            alamat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            cbUntuk.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString()); 
            RuangPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            nama_petugas_pembimbing.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            
            if (tbObat.getValueAt(tbObat.getSelectedRow(),9).toString().equals("Islam")) {
                C1.setSelected(true);
              } else if (tbObat.getValueAt(tbObat.getSelectedRow(),9).toString().equals("Katolik")) {
                C2.setSelected(true);
              } else if (tbObat.getValueAt(tbObat.getSelectedRow(),9).toString().equals("Kristen Protestan")) {
                C3.setSelected(true);
              } else if (tbObat.getValueAt(tbObat.getSelectedRow(),9).toString().equals("Hindu")) {
                C4.setSelected(true);
              } else if (tbObat.getValueAt(tbObat.getSelectedRow(),9).toString().equals("Budha")) {
                C5.setSelected(true);
              } else if (tbObat.getValueAt(tbObat.getSelectedRow(),9).toString().equals("Khonghuchu")) {
                C6.setSelected(true);
              } else{
                C7.setSelected(true);
                agama_lainnya.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
              }
        }
    }

    private void isRawat() {
        try {
            // START CUSTOM AMBIL KAMAR INAP TERAKHIR PASIEN
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien,pasien.jk,reg_periksa.umurdaftar,reg_periksa.sttsumur,reg_periksa.tgl_registrasi,"+
                    "reg_periksa.jam_reg,CASE reg_periksa.status_lanjut WHEN 'Ralan' THEN poliklinik.nm_poli WHEN 'Ranap' THEN CONCAT( kamar_inap.kd_kamar, ' ',bangsal.nm_bangsal ) END AS ruangan "+
                    "from reg_periksa "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli " +
                    "LEFT JOIN kamar_inap ON kamar_inap.no_rawat = reg_periksa.no_rawat " +
                    "LEFT JOIN kamar ON kamar.kd_kamar = kamar_inap.kd_kamar " +
                    "LEFT JOIN bangsal ON kamar.kd_bangsal = bangsal.kd_bangsal where reg_periksa.no_rawat=? AND kamar_inap.tgl_masuk = ( SELECT MAX( kamar_inap.tgl_masuk ) FROM kamar_inap WHERE kamar_inap.no_rawat = reg_periksa.no_rawat)");
            // END CUSTOM
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    NoRMPasien.setText(rs.getString("no_rkm_medis"));
                    Namapasien.setText(rs.getString("nm_pasien"));
                    TanggalRegistrasi.setText(rs.getString("tgl_registrasi")+" "+rs.getString("jam_reg"));
                    RuangPasien.setText(rs.getString("ruangan"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    public void setNoRm(String norwt) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        isRawat();              
        ChkInput.setSelected(true);
        isForm();
        tampil();
    }
    public void setNoRm2(String norwt,String norm,String nama,String kamar) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        TNoRM.setText(norm);
        TPasien.setText(nama);
        RuangPasien.setText(kamar);           
        ChkInput.setSelected(true);
        isForm();
        tampil();
    }
    
    private void isForm(){
        if(ChkInput.isSelected()==true){
            if(internalFrame1.getHeight()>478){
                ChkInput.setVisible(false);
                PanelInput.setPreferredSize(new Dimension(WIDTH,306));
                FormInput.setVisible(true);      
                ChkInput.setVisible(true);
            }else{
                ChkInput.setVisible(false);
                PanelInput.setPreferredSize(new Dimension(WIDTH,internalFrame1.getHeight()-172));
                FormInput.setVisible(true);      
                ChkInput.setVisible(true);
            }
        }else if(ChkInput.isSelected()==false){           
            ChkInput.setVisible(false);            
            PanelInput.setPreferredSize(new Dimension(WIDTH,20));
            FormInput.setVisible(false);      
            ChkInput.setVisible(true);
        }
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnHapus.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnPrint.setEnabled(akses.getpenilaian_awal_keperawatan_ralan()); 

        if(akses.getjml2()>=1){
            KdPetugas.setEditable(false);
            btnPetugas.setEnabled(false);
            KdPetugas.setText(akses.getkode());
            NmPetugas.setText(petugas.tampil3(KdPetugas.getText()));
            if(NmPetugas.getText().equals("")){
                KdPetugas.setText("");
                JOptionPane.showMessageDialog(null,"User login bukan petugas...!!");
            }
        }  
        if(TANGGALMUNDUR.equals("no")){
            if(!akses.getkode().equals("Admin Utama")){
                Tanggal.setEditable(false);
                Tanggal.setEnabled(false);
                ChkKejadian.setEnabled(false);
                Jam.setEnabled(false);
                Menit.setEnabled(false);
                Detik.setEnabled(false);
            }
        }
    }

    private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkKejadian.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkKejadian.isSelected()==false){
                    nilai_jam =Jam.getSelectedIndex();
                    nilai_menit =Menit.getSelectedIndex();
                    nilai_detik =Detik.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam.setSelectedItem(jam);
                Menit.setSelectedItem(menit);
                Detik.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }

    private void ganti() {
        String selectedCheckbox = "";
        if (C1.isSelected()) {
          selectedCheckbox = "Islam";
        } else if (C2.isSelected()) {
          selectedCheckbox = "Katolik";
        } else if (C3.isSelected()) {
          selectedCheckbox = "Kristen Protestan";
        } else if (C4.isSelected()) {
          selectedCheckbox = "Hindu";
        } else if (C5.isSelected()) {
          selectedCheckbox = "Budha";
        } else if (C6.isSelected()) {
          selectedCheckbox = "Khonghuchu";
        } else if (C7.isSelected()) {
          selectedCheckbox = agama_lainnya.getText();
        }
        if(Sequel.mengedittf("permintaan_pelayananrohani","no_rawat=? and tanggal=? and jam=?","no_rawat=?,tanggal=?,jam=?,no_rkm_medis=?,nama_pj=?,tgl_lahir_pj=?,umur_pj=?,alamat_pj=?,agama_diminta_pj=?,untuk=?,ruang=?,kd_petugas=?,nm_petugas_rohani=?",16,new String[]{
           TNoRw.getText(),
            Valid.SetTgl(Tanggal.getSelectedItem()+""),
            Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(), 
            NoRMPasien.getText(),
            nama_pj.getText(),
            Valid.SetTgl(TglLahir.getSelectedItem()+""),
            umur.getText(),
            alamat.getText(),
            selectedCheckbox, 
            cbUntuk.getSelectedItem().toString(),
            RuangPasien.getText(),
            KdPetugas.getText(),
            nama_petugas_pembimbing.getText(),
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),
            tbObat.getValueAt(tbObat.getSelectedRow(),1).toString(),
            tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()
        })==true){
            emptTeks();
            tampil();
        }
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from permintaan_pelayananrohani where no_rawat=? and tanggal=? and jam=? ",3,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),
            tbObat.getValueAt(tbObat.getSelectedRow(),1).toString(),
            tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()
        })==true){
            tabMode.removeRow(tbObat.getSelectedRow());
            LCount.setText(""+tabMode.getRowCount());
            emptTeks();
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }
    
    private void simpan() {
        String selectedCheckbox = "";
        if (C1.isSelected()) {
          selectedCheckbox = "Islam";
        } else if (C2.isSelected()) {
          selectedCheckbox = "Katolik";
        } else if (C3.isSelected()) {
          selectedCheckbox = "Kristen Protestan";
        } else if (C4.isSelected()) {
          selectedCheckbox = "Hindu";
        } else if (C5.isSelected()) {
          selectedCheckbox = "Budha";
        } else if (C6.isSelected()) {
          selectedCheckbox = "Khonghuchu";
        } else if (C7.isSelected()) {
          selectedCheckbox = agama_lainnya.getText();
        }
        if(Sequel.menyimpantf("permintaan_pelayananrohani","?,?,?,?,?,?,?,?,?,?,?,?,?","data",13,new String[]{
         TNoRw.getText(),
         Valid.SetTgl(Tanggal.getSelectedItem()+""),
         Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(), 
         NoRMPasien.getText(),
         nama_pj.getText(),
         Valid.SetTgl(TglLahir.getSelectedItem()+""),
         umur.getText(),
         alamat.getText(),
         selectedCheckbox, 
         cbUntuk.getSelectedItem().toString(),
         RuangPasien.getText(),
         KdPetugas.getText(),
         nama_petugas_pembimbing.getText(),
        })==true){
          tampil();
          emptTeks();
        }
    }
}
