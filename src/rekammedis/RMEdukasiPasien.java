/*
 * Kontribusi dari Abdul Wahid, RSUD Cipayung Jakarta Timur
 */


package rekammedis;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariPetugas;


/**
 *
 * @author perpustakaan
 */
public final class RMEdukasiPasien extends javax.swing.JDialog {
    private final DefaultTableModel tabMode,tabModeHambatan,tabModeKebutuhan,tabModeEdukasiDokter,tabModeEdukasiPerawat,tabModeEdukasiGizi,
            tabModeEdukasiFarmasi,tabModeEdukasiNyeri,tabModeEdukasiLainnya;  
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps,ps2;
    private ResultSet rs,rs2;
    private int i=0,jml=0,index=0;    
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private boolean[] pilih;    
    private String[] kode,hambatan,kebutuhan,edukasi;    
    private String finger="",hambatan_belajar="",kebutuhan_edukasi="",
            edukasi_dokter="",edukasi_perawat="",edukasi_gizi="",edukasi_farmasi="",
            edukasi_lainnya="",edukasi_nyeri=""; 
    private File file;
    private FileWriter fileWriter;
    private String iyem;
    private ObjectMapper mapper = new ObjectMapper();
    private JsonNode root;
    private JsonNode response;
    private FileReader myObj;
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMEdukasiPasien(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","J.K.","Tgl. Lahir",
            "Bahasa","Penerjemah","Bahasa Isyarat","Cara Belajar","Tingkat Pendidikan","Hambatan Belajar",
            "Tanggal Hak","Sasaran Hak","Cara Edukasi Hak","Metode Evaluasi Hak",
            "Tanggal Orientasi","Sasaran Orientasi","Cara Edukasi Orientasi","Metode Evaluasi Orientasi",
            "Tanggal Diagnosa","Sasaran Diagnosa","Cara Edukasi Diagnosa","Metode Evaluasi Diagnosa",
            "Tanggal Obat","Sasaran Obat","Cara Edukasi Obat","Metode Evaluasi Obat",
            "Tanggal Alat","Sasaran Alat","Cara Edukasi Alat","Metode Evaluasi Alat",
            "Tanggal Diet","Sasaran Diet","Cara Edukasi Diet","Metode Evaluasi Diet",
            "Tanggal Rehab","Sasaran Rehab","Cara Edukasi Rehab","Metode Evaluasi Rehab",
            "Tanggal Nyeri","Sasaran Nyeri","Cara Edukasi Nyeri","Metode Evaluasi Nyeri",
            "Tanggal Infeksi","Sasaran Infeksi","Cara Edukasi Infeksi","Metode Evaluasi Infeksi",
            "Tanggal Rawat","Sasaran Rawat","Cara Edukasi Rawat","Metode Evaluasi Rawat",
            "Tanggal Edukasi Dokter","Durasi Dokter","Evaluasi Dokter",
            "Tanggal Edukasi Perawat","Durasi Perawat","Evaluasi Perawat",
            "Tanggal Edukasi Gizi","Durasi Gizi","Evaluasi Gizi",
            "Tanggal Edukasi Farmasi","Durasi Farmasi","Evaluasi Farmasi",
            "Tanggal Edukasi Nyeri","Durasi Nyeri","Evaluasi Nyeri",
            "Tanggal Edukasi Lainnya","Durasi Lainnya","Evaluasi Lainnya",
            "NIP","Nama Petugas","Tanggal"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 72; i++) {  
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(65);
            }else if(i==2){
                column.setPreferredWidth(160);
            }else if(i==3){
                column.setPreferredWidth(50);
            }else if(i==4){
                column.setPreferredWidth(80);
            }else {    
                column.setPreferredWidth(150);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        tabModeHambatan=new DefaultTableModel(null,new Object[]{
                "P","KODE","HAMBATAN BELAJAR"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        
        tbHambatanBelajar.setModel(tabModeHambatan);
        tbHambatanBelajar.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbHambatanBelajar.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 3; i++) {
            TableColumn column = tbHambatanBelajar.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(20);
            }else if(i==1){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==2){
                column.setPreferredWidth(350);
            }
        }
        tbHambatanBelajar.setDefaultRenderer(Object.class, new WarnaTable());        

        tabModeEdukasiDokter=new DefaultTableModel(null,new Object[]{
                "P","KODE","EDUKASI DOKTER"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        
        tbEdukasiDokter.setModel(tabModeEdukasiDokter);
        tbEdukasiDokter.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbEdukasiDokter.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 3; i++) {
            TableColumn column = tbEdukasiDokter.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(20);
            }else if(i==1){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==2){
                column.setPreferredWidth(350);
            }
        }
        tbEdukasiDokter.setDefaultRenderer(Object.class, new WarnaTable()); 

        tabModeEdukasiPerawat=new DefaultTableModel(null,new Object[]{
                "P","KODE","EDUKASI PERAWAT"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        
        tbEdukasiPerawat.setModel(tabModeEdukasiPerawat);
        tbEdukasiPerawat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbEdukasiPerawat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 3; i++) {
            TableColumn column = tbEdukasiPerawat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(20);
            }else if(i==1){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==2){
                column.setPreferredWidth(350);
            }
        }
        tbEdukasiPerawat.setDefaultRenderer(Object.class, new WarnaTable()); 

        tabModeEdukasiGizi=new DefaultTableModel(null,new Object[]{
                "P","KODE","EDUKASI AHLI GIZI"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        
        tbEdukasiGizi.setModel(tabModeEdukasiGizi);
        tbEdukasiGizi.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbEdukasiGizi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 3; i++) {
            TableColumn column = tbEdukasiGizi.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(20);
            }else if(i==1){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==2){
                column.setPreferredWidth(350);
            }
        }
        tbEdukasiGizi.setDefaultRenderer(Object.class, new WarnaTable()); 

        tabModeEdukasiFarmasi=new DefaultTableModel(null,new Object[]{
                "P","KODE","EDUKASI FARMASI KLINIK"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        
        tbEdukasiFarmasi.setModel(tabModeEdukasiFarmasi);
        tbEdukasiFarmasi.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbEdukasiFarmasi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 3; i++) {
            TableColumn column = tbEdukasiFarmasi.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(20);
            }else if(i==1){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==2){
                column.setPreferredWidth(350);
            }
        }
        tbEdukasiFarmasi.setDefaultRenderer(Object.class, new WarnaTable()); 

        tabModeEdukasiNyeri=new DefaultTableModel(null,new Object[]{
                "P","KODE","EDUKASI TIM NYERI"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        
        tbEdukasiNyeri.setModel(tabModeEdukasiNyeri);
        tbEdukasiNyeri.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbEdukasiNyeri.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 3; i++) {
            TableColumn column = tbEdukasiNyeri.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(20);
            }else if(i==1){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==2){
                column.setPreferredWidth(350);
            }
        }
        tbEdukasiNyeri.setDefaultRenderer(Object.class, new WarnaTable()); 

        tabModeEdukasiLainnya=new DefaultTableModel(null,new Object[]{
                "P","KODE","EDUKASI PPA LAINNYA"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        
        tbEdukasiLainnya.setModel(tabModeEdukasiLainnya);
        tbEdukasiLainnya.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbEdukasiLainnya.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 3; i++) {
            TableColumn column = tbEdukasiLainnya.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(20);
            }else if(i==1){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==2){
                column.setPreferredWidth(350);
            }
        }
        tbEdukasiLainnya.setDefaultRenderer(Object.class, new WarnaTable()); 

        tabModeKebutuhan=new DefaultTableModel(null,new Object[]{
                "P","KODE","KEBUTUHAN EDUKASI"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        
        tbKebutuhanEdukasi.setModel(tabModeKebutuhan);
        tbKebutuhanEdukasi.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbKebutuhanEdukasi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 3; i++) {
            TableColumn column = tbKebutuhanEdukasi.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(20);
            }else if(i==1){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==2){
                column.setPreferredWidth(350);
            }
        }
        tbKebutuhanEdukasi.setDefaultRenderer(Object.class, new WarnaTable());
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
//        KebutuhanEdukasi.setDocument(new batasInput((int)100).getKata(KebutuhanEdukasi));
        
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){ 
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());   
                }              
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
       
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnCetakEdukasiPasien = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        label11 = new widget.Label();
        jLabel11 = new widget.Label();
        jLabel12 = new widget.Label();
        jLabel22 = new widget.Label();
        jLabel14 = new widget.Label();
        jLabel37 = new widget.Label();
        jLabel53 = new widget.Label();
        jLabel30 = new widget.Label();
        jLabel55 = new widget.Label();
        HakPE = new widget.TextBox();
        TglAsuhan = new widget.Tanggal();
        jLabel56 = new widget.Label();
        jLabel62 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        Bahasa = new widget.TextBox();
        Penerjemah = new widget.ComboBox();
        BahasaIsyarat = new widget.ComboBox();
        CaraBelajar = new widget.ComboBox();
        jLabel13 = new widget.Label();
        Pendidikan = new widget.ComboBox();
        jLabel15 = new widget.Label();
        Hambatan = new widget.ComboBox();
        jLabel78 = new widget.Label();
        jLabel91 = new widget.Label();
        jLabel93 = new widget.Label();
        jLabel98 = new widget.Label();
        jLabel99 = new widget.Label();
        jLabel63 = new widget.Label();
        OrientasiPE = new widget.TextBox();
        jLabel64 = new widget.Label();
        jLabel65 = new widget.Label();
        DiagnosaPE = new widget.TextBox();
        jLabel66 = new widget.Label();
        ObatPE = new widget.TextBox();
        jLabel67 = new widget.Label();
        jLabel68 = new widget.Label();
        AlatPE = new widget.TextBox();
        jLabel69 = new widget.Label();
        DietPE = new widget.TextBox();
        jLabel70 = new widget.Label();
        RehabPE = new widget.TextBox();
        jLabel71 = new widget.Label();
        NyeriPE = new widget.TextBox();
        jLabel72 = new widget.Label();
        jLabel73 = new widget.Label();
        InfeksiPE = new widget.TextBox();
        jLabel74 = new widget.Label();
        RawatPE = new widget.TextBox();
        DurasiDokter = new widget.TextBox();
        jLabel57 = new widget.Label();
        jLabel79 = new widget.Label();
        jLabel80 = new widget.Label();
        jLabel81 = new widget.Label();
        jLabel82 = new widget.Label();
        jLabel83 = new widget.Label();
        TglDokter = new widget.Tanggal();
        scrollPane7 = new widget.ScrollPane();
        EvaluasiDokter = new widget.TextArea();
        jLabel84 = new widget.Label();
        TglPerawat = new widget.Tanggal();
        DurasiPerawat = new widget.TextBox();
        scrollPane9 = new widget.ScrollPane();
        EvaluasiPerawat = new widget.TextArea();
        jLabel85 = new widget.Label();
        jLabel86 = new widget.Label();
        TglGizi = new widget.Tanggal();
        DurasiGizi = new widget.TextBox();
        scrollPane11 = new widget.ScrollPane();
        EvaluasiGizi = new widget.TextArea();
        jLabel87 = new widget.Label();
        DurasiFarmasi = new widget.TextBox();
        TglFarmasi = new widget.Tanggal();
        scrollPane13 = new widget.ScrollPane();
        EvaluasiFarmasi = new widget.TextArea();
        TglNyeri = new widget.Tanggal();
        DurasiNyeri = new widget.TextBox();
        jLabel88 = new widget.Label();
        scrollPane15 = new widget.ScrollPane();
        EvaluasiNyeri = new widget.TextArea();
        jLabel89 = new widget.Label();
        jLabel58 = new widget.Label();
        jLabel59 = new widget.Label();
        jLabel60 = new widget.Label();
        Scroll6 = new widget.ScrollPane();
        tbHambatanBelajar = new widget.Table();
        jSeparator9 = new javax.swing.JSeparator();
        Scroll8 = new widget.ScrollPane();
        tbKebutuhanEdukasi = new widget.Table();
        label12 = new widget.Label();
        TCariHambatan = new widget.TextBox();
        BtnCariHambatan = new widget.Button();
        BtnAllHambatan = new widget.Button();
        BtnTambahHambatan = new widget.Button();
        label13 = new widget.Label();
        TCariKebutuhan = new widget.TextBox();
        BtnCariKebutuhan = new widget.Button();
        BtnAllKebutuhan = new widget.Button();
        BtnTambahKebutuhan = new widget.Button();
        TglHakWP = new widget.Tanggal();
        TglOrientasiWP = new widget.Tanggal();
        TglDiagnosaWP = new widget.Tanggal();
        TglObatWP = new widget.Tanggal();
        TglAlatWP = new widget.Tanggal();
        TglDietWP = new widget.Tanggal();
        TglRehabWP = new widget.Tanggal();
        TglNyeriWP = new widget.Tanggal();
        TglInfeksiWP = new widget.Tanggal();
        TglRawatWP = new widget.Tanggal();
        HakS = new widget.ComboBox();
        OrientasiS = new widget.ComboBox();
        DiagnosaS = new widget.ComboBox();
        ObatS = new widget.ComboBox();
        AlatS = new widget.ComboBox();
        DietS = new widget.ComboBox();
        RehabS = new widget.ComboBox();
        NyeriS = new widget.ComboBox();
        InfeksiS = new widget.ComboBox();
        RawatS = new widget.ComboBox();
        HakCE = new widget.ComboBox();
        OrientasiCE = new widget.ComboBox();
        DiagnosaCE = new widget.ComboBox();
        ObatCE = new widget.ComboBox();
        AlatCE = new widget.ComboBox();
        DietCE = new widget.ComboBox();
        RehabCE = new widget.ComboBox();
        NyeriCE = new widget.ComboBox();
        InfeksiCE = new widget.ComboBox();
        RawatCE = new widget.ComboBox();
        HakME = new widget.ComboBox();
        OrientasiME = new widget.ComboBox();
        DiagnosaME = new widget.ComboBox();
        ObatME = new widget.ComboBox();
        AlatME = new widget.ComboBox();
        DietME = new widget.ComboBox();
        RehabME = new widget.ComboBox();
        NyeriME = new widget.ComboBox();
        InfeksiME = new widget.ComboBox();
        RawatME = new widget.ComboBox();
        jLabel90 = new widget.Label();
        TglLainnya = new widget.Tanggal();
        DurasiLainnya = new widget.TextBox();
        scrollPane17 = new widget.ScrollPane();
        EvaluasiLainnya = new widget.TextArea();
        Scroll10 = new widget.ScrollPane();
        tbEdukasiDokter = new widget.Table();
        label15 = new widget.Label();
        TCariEdukasiDokter = new widget.TextBox();
        BtnCariEdukasiDokter = new widget.Button();
        BtnAllEdukasiDokter = new widget.Button();
        BtnTambahEdukasiDokter = new widget.Button();
        Scroll11 = new widget.ScrollPane();
        tbEdukasiPerawat = new widget.Table();
        label16 = new widget.Label();
        TCariEdukasiPerawat = new widget.TextBox();
        BtnCariEdukasiPerawat = new widget.Button();
        BtnAllEdukasiPerawat = new widget.Button();
        BtnTambahEdukasiPerawat = new widget.Button();
        Scroll12 = new widget.ScrollPane();
        tbEdukasiGizi = new widget.Table();
        label17 = new widget.Label();
        TCariEdukasiGizi = new widget.TextBox();
        BtnCariEdukasiGizi = new widget.Button();
        BtnAllEdukasiGizi = new widget.Button();
        BtnTambahEdukasiGizi = new widget.Button();
        Scroll13 = new widget.ScrollPane();
        tbEdukasiFarmasi = new widget.Table();
        label18 = new widget.Label();
        TCariEdukasiFarmasi = new widget.TextBox();
        BtnCariEdukasiFarmasi = new widget.Button();
        BtnAllEdukasiFarmasi = new widget.Button();
        BtnTambahEdukasiFarmasi = new widget.Button();
        Scroll14 = new widget.ScrollPane();
        tbEdukasiNyeri = new widget.Table();
        label19 = new widget.Label();
        TCariEdukasiNyeri = new widget.TextBox();
        BtnCariEdukasiNyeri = new widget.Button();
        BtnAllEdukasiNyeri = new widget.Button();
        BtnTambahEdukasiNyeri = new widget.Button();
        Scroll15 = new widget.ScrollPane();
        tbEdukasiLainnya = new widget.Table();
        label20 = new widget.Label();
        TCariEdukasiLainnya = new widget.TextBox();
        BtnCariEdukasiLainnya = new widget.Button();
        BtnAllEdukasiLainnya = new widget.Button();
        BtnTambahEdukasiLainnya = new widget.Button();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnCetakEdukasiPasien.setBackground(new java.awt.Color(255, 255, 254));
        MnCetakEdukasiPasien.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakEdukasiPasien.setForeground(new java.awt.Color(50, 50, 50));
        MnCetakEdukasiPasien.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetakEdukasiPasien.setText("Cetak Pengkajian & Edukasi Pasien");
        MnCetakEdukasiPasien.setName("MnCetakEdukasiPasien"); // NOI18N
        MnCetakEdukasiPasien.setPreferredSize(new java.awt.Dimension(250, 26));
        MnCetakEdukasiPasien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakEdukasiPasienActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakEdukasiPasien);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Pengkajian & Perencanaan Edukasi Pasien dan Keluarga ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 1583));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(74, 40, 100, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(176, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(358, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(395, 40, 57, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jLabel12.setText("Perlu penerjemah :");
        jLabel12.setName("jLabel12"); // NOI18N
        FormInput.add(jLabel12);
        jLabel12.setBounds(300, 90, 100, 23);

        jLabel22.setText("Bahasa sehari-hari : ");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(40, 90, 99, 23);

        jLabel14.setText("Bahasa isyarat :");
        jLabel14.setName("jLabel14"); // NOI18N
        FormInput.add(jLabel14);
        jLabel14.setBounds(570, 90, 90, 23);

        jLabel37.setText("Kebutuhan Edukasi :");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(470, 150, 100, 23);

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel53.setText("A. PENGKAJIAN KEBUTUHAN EDUKASI");
        jLabel53.setName("jLabel53"); // NOI18N
        FormInput.add(jLabel53);
        jLabel53.setBounds(10, 70, 230, 23);

        jLabel30.setText("Cara belajar yang disukai :");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(10, 120, 130, 20);

        jLabel55.setText("Metode Evaluasi***");
        jLabel55.setName("jLabel55"); // NOI18N
        FormInput.add(jLabel55);
        jLabel55.setBounds(770, 360, 100, 23);

        HakPE.setEditable(false);
        HakPE.setText("Perawat");
        HakPE.setFocusTraversalPolicyProvider(true);
        HakPE.setName("HakPE"); // NOI18N
        HakPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HakPEActionPerformed(evt);
            }
        });
        HakPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HakPEKeyPressed(evt);
            }
        });
        FormInput.add(HakPE);
        HakPE.setBounds(180, 390, 100, 23);

        TglAsuhan.setForeground(new java.awt.Color(50, 70, 50));
        TglAsuhan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:12" }));
        TglAsuhan.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglAsuhan.setName("TglAsuhan"); // NOI18N
        TglAsuhan.setOpaque(false);
        TglAsuhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglAsuhanKeyPressed(evt);
            }
        });
        FormInput.add(TglAsuhan);
        TglAsuhan.setBounds(456, 40, 130, 23);

        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel56.setText("B. PERENCANAAN PEMBERIAN EDUKASI");
        jLabel56.setName("jLabel56"); // NOI18N
        FormInput.add(jLabel56);
        jLabel56.setBounds(10, 340, 230, 23);

        jLabel62.setText("Hak & kewajiban pasien");
        jLabel62.setName("jLabel62"); // NOI18N
        FormInput.add(jLabel62);
        jLabel62.setBounds(30, 390, 130, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 70, 880, 1);

        Bahasa.setFocusTraversalPolicyProvider(true);
        Bahasa.setName("Bahasa"); // NOI18N
        Bahasa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BahasaKeyPressed(evt);
            }
        });
        FormInput.add(Bahasa);
        Bahasa.setBounds(150, 90, 140, 23);

        Penerjemah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak", "Ya" }));
        Penerjemah.setName("Penerjemah"); // NOI18N
        Penerjemah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PenerjemahActionPerformed(evt);
            }
        });
        Penerjemah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PenerjemahKeyPressed(evt);
            }
        });
        FormInput.add(Penerjemah);
        Penerjemah.setBounds(410, 90, 140, 23);

        BahasaIsyarat.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak", "Ya" }));
        BahasaIsyarat.setName("BahasaIsyarat"); // NOI18N
        BahasaIsyarat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BahasaIsyaratActionPerformed(evt);
            }
        });
        BahasaIsyarat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BahasaIsyaratKeyPressed(evt);
            }
        });
        FormInput.add(BahasaIsyarat);
        BahasaIsyarat.setBounds(670, 90, 140, 23);

        CaraBelajar.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Membaca", "Audio-visual/gambar", "Diskusi", "Demonstrasi" }));
        CaraBelajar.setName("CaraBelajar"); // NOI18N
        CaraBelajar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CaraBelajarActionPerformed(evt);
            }
        });
        CaraBelajar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CaraBelajarKeyPressed(evt);
            }
        });
        FormInput.add(CaraBelajar);
        CaraBelajar.setBounds(150, 120, 140, 23);

        jLabel13.setText("Tingkat Pendidikan :");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput.add(jLabel13);
        jLabel13.setBounds(300, 120, 100, 23);

        Pendidikan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "TK", "SD", "SMP", "SMA", "Akademi", "Sarjana", "Lain-lain" }));
        Pendidikan.setName("Pendidikan"); // NOI18N
        Pendidikan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PendidikanActionPerformed(evt);
            }
        });
        Pendidikan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PendidikanKeyPressed(evt);
            }
        });
        FormInput.add(Pendidikan);
        Pendidikan.setBounds(410, 120, 140, 23);

        jLabel15.setText("Hambatan belajar :");
        jLabel15.setName("jLabel15"); // NOI18N
        FormInput.add(jLabel15);
        jLabel15.setBounds(40, 150, 100, 23);

        Hambatan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada", "Ada" }));
        Hambatan.setName("Hambatan"); // NOI18N
        Hambatan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HambatanActionPerformed(evt);
            }
        });
        Hambatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HambatanKeyPressed(evt);
            }
        });
        FormInput.add(Hambatan);
        Hambatan.setBounds(150, 150, 140, 23);

        jLabel78.setText("Kebutuhan Edukasi");
        jLabel78.setName("jLabel78"); // NOI18N
        FormInput.add(jLabel78);
        jLabel78.setBounds(50, 360, 100, 23);

        jLabel91.setText("Pemberi Edukasi");
        jLabel91.setName("jLabel91"); // NOI18N
        FormInput.add(jLabel91);
        jLabel91.setBounds(190, 360, 80, 23);

        jLabel93.setText("Waktu Pemberian");
        jLabel93.setName("jLabel93"); // NOI18N
        FormInput.add(jLabel93);
        jLabel93.setBounds(310, 360, 90, 23);

        jLabel98.setText("Cara Edukasi (D/Demo/B)**");
        jLabel98.setName("jLabel98"); // NOI18N
        FormInput.add(jLabel98);
        jLabel98.setBounds(580, 360, 140, 23);

        jLabel99.setText("Sasaran (P/K/P&K)*");
        jLabel99.setName("jLabel99"); // NOI18N
        FormInput.add(jLabel99);
        jLabel99.setBounds(430, 360, 110, 23);

        jLabel63.setText("Orientasi ruangan");
        jLabel63.setName("jLabel63"); // NOI18N
        FormInput.add(jLabel63);
        jLabel63.setBounds(30, 420, 130, 23);

        OrientasiPE.setEditable(false);
        OrientasiPE.setText("Perawat");
        OrientasiPE.setFocusTraversalPolicyProvider(true);
        OrientasiPE.setName("OrientasiPE"); // NOI18N
        OrientasiPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OrientasiPEActionPerformed(evt);
            }
        });
        OrientasiPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                OrientasiPEKeyPressed(evt);
            }
        });
        FormInput.add(OrientasiPE);
        OrientasiPE.setBounds(180, 420, 100, 23);

        jLabel64.setText("gejala");
        jLabel64.setName("jLabel64"); // NOI18N
        FormInput.add(jLabel64);
        jLabel64.setBounds(20, 460, 30, 23);

        jLabel65.setText("Diagnosa, penyebab, tanda &");
        jLabel65.setName("jLabel65"); // NOI18N
        FormInput.add(jLabel65);
        jLabel65.setBounds(10, 450, 150, 23);

        DiagnosaPE.setEditable(false);
        DiagnosaPE.setText("Dokter");
        DiagnosaPE.setFocusTraversalPolicyProvider(true);
        DiagnosaPE.setName("DiagnosaPE"); // NOI18N
        DiagnosaPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DiagnosaPEActionPerformed(evt);
            }
        });
        DiagnosaPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DiagnosaPEKeyPressed(evt);
            }
        });
        FormInput.add(DiagnosaPE);
        DiagnosaPE.setBounds(180, 450, 100, 23);

        jLabel66.setText("Obat-obatan yang didapat");
        jLabel66.setName("jLabel66"); // NOI18N
        FormInput.add(jLabel66);
        jLabel66.setBounds(10, 480, 150, 23);

        ObatPE.setEditable(false);
        ObatPE.setText("Farmasi");
        ObatPE.setFocusTraversalPolicyProvider(true);
        ObatPE.setName("ObatPE"); // NOI18N
        ObatPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ObatPEActionPerformed(evt);
            }
        });
        ObatPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ObatPEKeyPressed(evt);
            }
        });
        FormInput.add(ObatPE);
        ObatPE.setBounds(180, 480, 100, 23);

        jLabel67.setText("yang aman & efektif");
        jLabel67.setName("jLabel67"); // NOI18N
        FormInput.add(jLabel67);
        jLabel67.setBounds(20, 520, 100, 23);

        jLabel68.setText("Penggunaan peralatan medis");
        jLabel68.setName("jLabel68"); // NOI18N
        FormInput.add(jLabel68);
        jLabel68.setBounds(10, 510, 150, 23);

        AlatPE.setEditable(false);
        AlatPE.setText("Dokter/Perawat");
        AlatPE.setFocusTraversalPolicyProvider(true);
        AlatPE.setName("AlatPE"); // NOI18N
        AlatPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AlatPEActionPerformed(evt);
            }
        });
        AlatPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AlatPEKeyPressed(evt);
            }
        });
        FormInput.add(AlatPE);
        AlatPE.setBounds(180, 510, 100, 23);

        jLabel69.setText("Diet");
        jLabel69.setName("jLabel69"); // NOI18N
        FormInput.add(jLabel69);
        jLabel69.setBounds(10, 540, 150, 23);

        DietPE.setEditable(false);
        DietPE.setText("Ahli Gizi");
        DietPE.setFocusTraversalPolicyProvider(true);
        DietPE.setName("DietPE"); // NOI18N
        DietPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DietPEActionPerformed(evt);
            }
        });
        DietPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DietPEKeyPressed(evt);
            }
        });
        FormInput.add(DietPE);
        DietPE.setBounds(180, 540, 100, 23);

        jLabel70.setText("Rehabilitasi medik");
        jLabel70.setName("jLabel70"); // NOI18N
        FormInput.add(jLabel70);
        jLabel70.setBounds(10, 570, 150, 23);

        RehabPE.setEditable(false);
        RehabPE.setText("Terapis");
        RehabPE.setFocusTraversalPolicyProvider(true);
        RehabPE.setName("RehabPE"); // NOI18N
        RehabPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RehabPEActionPerformed(evt);
            }
        });
        RehabPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RehabPEKeyPressed(evt);
            }
        });
        FormInput.add(RehabPE);
        RehabPE.setBounds(180, 570, 100, 23);

        jLabel71.setText("Manajemen nyeri");
        jLabel71.setName("jLabel71"); // NOI18N
        FormInput.add(jLabel71);
        jLabel71.setBounds(10, 600, 150, 23);

        NyeriPE.setEditable(false);
        NyeriPE.setText("Tim Nyeri");
        NyeriPE.setFocusTraversalPolicyProvider(true);
        NyeriPE.setName("NyeriPE"); // NOI18N
        NyeriPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NyeriPEActionPerformed(evt);
            }
        });
        NyeriPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NyeriPEKeyPressed(evt);
            }
        });
        FormInput.add(NyeriPE);
        NyeriPE.setBounds(180, 600, 100, 23);

        jLabel72.setText("infeksi");
        jLabel72.setName("jLabel72"); // NOI18N
        FormInput.add(jLabel72);
        jLabel72.setBounds(10, 640, 50, 23);

        jLabel73.setText("Pencegahan & pengendalian");
        jLabel73.setName("jLabel73"); // NOI18N
        FormInput.add(jLabel73);
        jLabel73.setBounds(10, 630, 150, 23);

        InfeksiPE.setEditable(false);
        InfeksiPE.setText("Perawat");
        InfeksiPE.setFocusTraversalPolicyProvider(true);
        InfeksiPE.setName("InfeksiPE"); // NOI18N
        InfeksiPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InfeksiPEActionPerformed(evt);
            }
        });
        InfeksiPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                InfeksiPEKeyPressed(evt);
            }
        });
        FormInput.add(InfeksiPE);
        InfeksiPE.setBounds(180, 630, 100, 23);

        jLabel74.setText("Perawatan di rumah");
        jLabel74.setName("jLabel74"); // NOI18N
        FormInput.add(jLabel74);
        jLabel74.setBounds(10, 660, 150, 23);

        RawatPE.setEditable(false);
        RawatPE.setText("Perawat");
        RawatPE.setFocusTraversalPolicyProvider(true);
        RawatPE.setName("RawatPE"); // NOI18N
        RawatPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RawatPEActionPerformed(evt);
            }
        });
        RawatPE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RawatPEKeyPressed(evt);
            }
        });
        FormInput.add(RawatPE);
        RawatPE.setBounds(180, 660, 100, 23);

        DurasiDokter.setFocusTraversalPolicyProvider(true);
        DurasiDokter.setName("DurasiDokter"); // NOI18N
        DurasiDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DurasiDokterActionPerformed(evt);
            }
        });
        DurasiDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DurasiDokterKeyPressed(evt);
            }
        });
        FormInput.add(DurasiDokter);
        DurasiDokter.setBounds(530, 800, 90, 23);

        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel57.setText("(***) : Mampu menjelaskan atau mampu mendemonstrasikan");
        jLabel57.setName("jLabel57"); // NOI18N
        FormInput.add(jLabel57);
        jLabel57.setBounds(75, 730, 380, 23);

        jLabel79.setText("Respon/Evaluasi");
        jLabel79.setName("jLabel79"); // NOI18N
        FormInput.add(jLabel79);
        jLabel79.setBounds(710, 770, 90, 23);

        jLabel80.setText("1. Dokter");
        jLabel80.setName("jLabel80"); // NOI18N
        FormInput.add(jLabel80);
        jLabel80.setBounds(10, 800, 50, 23);

        jLabel81.setText("Tanggal");
        jLabel81.setName("jLabel81"); // NOI18N
        FormInput.add(jLabel81);
        jLabel81.setBounds(410, 770, 50, 23);

        jLabel82.setText("Durasi");
        jLabel82.setName("jLabel82"); // NOI18N
        FormInput.add(jLabel82);
        jLabel82.setBounds(560, 770, 30, 23);

        jLabel83.setText("Materi Edukasi");
        jLabel83.setName("jLabel83"); // NOI18N
        FormInput.add(jLabel83);
        jLabel83.setBounds(130, 770, 100, 23);

        TglDokter.setForeground(new java.awt.Color(50, 70, 50));
        TglDokter.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglDokter.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglDokter.setName("TglDokter"); // NOI18N
        TglDokter.setOpaque(false);
        TglDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglDokterKeyPressed(evt);
            }
        });
        FormInput.add(TglDokter);
        TglDokter.setBounds(370, 800, 130, 23);

        scrollPane7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane7.setName("scrollPane7"); // NOI18N

        EvaluasiDokter.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EvaluasiDokter.setColumns(20);
        EvaluasiDokter.setRows(5);
        EvaluasiDokter.setName("EvaluasiDokter"); // NOI18N
        EvaluasiDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EvaluasiDokterKeyPressed(evt);
            }
        });
        scrollPane7.setViewportView(EvaluasiDokter);

        FormInput.add(scrollPane7);
        scrollPane7.setBounds(640, 800, 260, 70);

        jLabel84.setText("/ Bidan");
        jLabel84.setName("jLabel84"); // NOI18N
        FormInput.add(jLabel84);
        jLabel84.setBounds(0, 940, 60, 23);

        TglPerawat.setForeground(new java.awt.Color(50, 70, 50));
        TglPerawat.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglPerawat.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglPerawat.setName("TglPerawat"); // NOI18N
        TglPerawat.setOpaque(false);
        TglPerawat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglPerawatKeyPressed(evt);
            }
        });
        FormInput.add(TglPerawat);
        TglPerawat.setBounds(370, 930, 130, 23);

        DurasiPerawat.setFocusTraversalPolicyProvider(true);
        DurasiPerawat.setName("DurasiPerawat"); // NOI18N
        DurasiPerawat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DurasiPerawatActionPerformed(evt);
            }
        });
        DurasiPerawat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DurasiPerawatKeyPressed(evt);
            }
        });
        FormInput.add(DurasiPerawat);
        DurasiPerawat.setBounds(530, 930, 90, 23);

        scrollPane9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane9.setName("scrollPane9"); // NOI18N

        EvaluasiPerawat.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EvaluasiPerawat.setColumns(20);
        EvaluasiPerawat.setRows(5);
        EvaluasiPerawat.setName("EvaluasiPerawat"); // NOI18N
        EvaluasiPerawat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EvaluasiPerawatKeyPressed(evt);
            }
        });
        scrollPane9.setViewportView(EvaluasiPerawat);

        FormInput.add(scrollPane9);
        scrollPane9.setBounds(640, 930, 260, 70);

        jLabel85.setText("2. Perawat");
        jLabel85.setName("jLabel85"); // NOI18N
        FormInput.add(jLabel85);
        jLabel85.setBounds(10, 930, 60, 23);

        jLabel86.setText("3. Ahli Gizi");
        jLabel86.setName("jLabel86"); // NOI18N
        FormInput.add(jLabel86);
        jLabel86.setBounds(20, 1060, 50, 23);

        TglGizi.setForeground(new java.awt.Color(50, 70, 50));
        TglGizi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglGizi.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglGizi.setName("TglGizi"); // NOI18N
        TglGizi.setOpaque(false);
        TglGizi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglGiziKeyPressed(evt);
            }
        });
        FormInput.add(TglGizi);
        TglGizi.setBounds(370, 1060, 130, 23);

        DurasiGizi.setFocusTraversalPolicyProvider(true);
        DurasiGizi.setName("DurasiGizi"); // NOI18N
        DurasiGizi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DurasiGiziActionPerformed(evt);
            }
        });
        DurasiGizi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DurasiGiziKeyPressed(evt);
            }
        });
        FormInput.add(DurasiGizi);
        DurasiGizi.setBounds(530, 1060, 90, 23);

        scrollPane11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane11.setName("scrollPane11"); // NOI18N

        EvaluasiGizi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EvaluasiGizi.setColumns(20);
        EvaluasiGizi.setRows(5);
        EvaluasiGizi.setName("EvaluasiGizi"); // NOI18N
        EvaluasiGizi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EvaluasiGiziKeyPressed(evt);
            }
        });
        scrollPane11.setViewportView(EvaluasiGizi);

        FormInput.add(scrollPane11);
        scrollPane11.setBounds(640, 1060, 260, 70);

        jLabel87.setText("Klinik");
        jLabel87.setName("jLabel87"); // NOI18N
        FormInput.add(jLabel87);
        jLabel87.setBounds(10, 1200, 50, 23);

        DurasiFarmasi.setFocusTraversalPolicyProvider(true);
        DurasiFarmasi.setName("DurasiFarmasi"); // NOI18N
        DurasiFarmasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DurasiFarmasiActionPerformed(evt);
            }
        });
        DurasiFarmasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DurasiFarmasiKeyPressed(evt);
            }
        });
        FormInput.add(DurasiFarmasi);
        DurasiFarmasi.setBounds(530, 1190, 90, 23);

        TglFarmasi.setForeground(new java.awt.Color(50, 70, 50));
        TglFarmasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglFarmasi.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglFarmasi.setName("TglFarmasi"); // NOI18N
        TglFarmasi.setOpaque(false);
        TglFarmasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglFarmasiKeyPressed(evt);
            }
        });
        FormInput.add(TglFarmasi);
        TglFarmasi.setBounds(370, 1190, 130, 23);

        scrollPane13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane13.setName("scrollPane13"); // NOI18N

        EvaluasiFarmasi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EvaluasiFarmasi.setColumns(20);
        EvaluasiFarmasi.setRows(5);
        EvaluasiFarmasi.setName("EvaluasiFarmasi"); // NOI18N
        EvaluasiFarmasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EvaluasiFarmasiKeyPressed(evt);
            }
        });
        scrollPane13.setViewportView(EvaluasiFarmasi);

        FormInput.add(scrollPane13);
        scrollPane13.setBounds(640, 1190, 260, 70);

        TglNyeri.setForeground(new java.awt.Color(50, 70, 50));
        TglNyeri.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglNyeri.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglNyeri.setName("TglNyeri"); // NOI18N
        TglNyeri.setOpaque(false);
        TglNyeri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglNyeriKeyPressed(evt);
            }
        });
        FormInput.add(TglNyeri);
        TglNyeri.setBounds(370, 1320, 130, 23);

        DurasiNyeri.setFocusTraversalPolicyProvider(true);
        DurasiNyeri.setName("DurasiNyeri"); // NOI18N
        DurasiNyeri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DurasiNyeriActionPerformed(evt);
            }
        });
        DurasiNyeri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DurasiNyeriKeyPressed(evt);
            }
        });
        FormInput.add(DurasiNyeri);
        DurasiNyeri.setBounds(530, 1320, 90, 23);

        jLabel88.setText("5. Tim Nyeri");
        jLabel88.setName("jLabel88"); // NOI18N
        FormInput.add(jLabel88);
        jLabel88.setBounds(10, 1320, 60, 23);

        scrollPane15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane15.setName("scrollPane15"); // NOI18N

        EvaluasiNyeri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EvaluasiNyeri.setColumns(20);
        EvaluasiNyeri.setRows(5);
        EvaluasiNyeri.setName("EvaluasiNyeri"); // NOI18N
        EvaluasiNyeri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EvaluasiNyeriKeyPressed(evt);
            }
        });
        scrollPane15.setViewportView(EvaluasiNyeri);

        FormInput.add(scrollPane15);
        scrollPane15.setBounds(640, 1320, 260, 70);

        jLabel89.setText("4. Farmasi");
        jLabel89.setName("jLabel89"); // NOI18N
        FormInput.add(jLabel89);
        jLabel89.setBounds(10, 1190, 60, 23);

        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel58.setText("CATATAN EDUKASI PASIEN TERPADU");
        jLabel58.setName("jLabel58"); // NOI18N
        FormInput.add(jLabel58);
        jLabel58.setBounds(440, 750, 230, 23);

        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel59.setText("Keterangan : (*) P/K/P&K : Pasien, Keluarga Pasien/Pasien & Keluarga Pasien");
        jLabel59.setName("jLabel59"); // NOI18N
        FormInput.add(jLabel59);
        jLabel59.setBounds(10, 690, 380, 23);

        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel60.setText("(**) D/Demo/B : Diskusi/Demonstrasi/Brosur");
        jLabel60.setName("jLabel60"); // NOI18N
        FormInput.add(jLabel60);
        jLabel60.setBounds(75, 710, 380, 23);

        Scroll6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll6.setName("Scroll6"); // NOI18N
        Scroll6.setOpaque(true);

        tbHambatanBelajar.setName("tbHambatanBelajar"); // NOI18N
        tbHambatanBelajar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbHambatanBelajarMouseClicked(evt);
            }
        });
        tbHambatanBelajar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbHambatanBelajarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbHambatanBelajarKeyReleased(evt);
            }
        });
        Scroll6.setViewportView(tbHambatanBelajar);

        FormInput.add(Scroll6);
        Scroll6.setBounds(40, 180, 400, 120);

        jSeparator9.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator9.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator9.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator9.setName("jSeparator9"); // NOI18N
        FormInput.add(jSeparator9);
        jSeparator9.setBounds(450, 180, 1, 150);

        Scroll8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll8.setName("Scroll8"); // NOI18N
        Scroll8.setOpaque(true);

        tbKebutuhanEdukasi.setName("tbKebutuhanEdukasi"); // NOI18N
        tbKebutuhanEdukasi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbKebutuhanEdukasiMouseClicked(evt);
            }
        });
        tbKebutuhanEdukasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbKebutuhanEdukasiKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbKebutuhanEdukasiKeyReleased(evt);
            }
        });
        Scroll8.setViewportView(tbKebutuhanEdukasi);

        FormInput.add(Scroll8);
        Scroll8.setBounds(460, 180, 400, 120);

        label12.setText("Key Word :");
        label12.setName("label12"); // NOI18N
        label12.setPreferredSize(new java.awt.Dimension(60, 23));
        FormInput.add(label12);
        label12.setBounds(33, 310, 60, 23);

        TCariHambatan.setToolTipText("Alt+C");
        TCariHambatan.setName("TCariHambatan"); // NOI18N
        TCariHambatan.setPreferredSize(new java.awt.Dimension(140, 23));
        TCariHambatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariHambatanKeyPressed(evt);
            }
        });
        FormInput.add(TCariHambatan);
        TCariHambatan.setBounds(97, 310, 215, 23);

        BtnCariHambatan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCariHambatan.setMnemonic('1');
        BtnCariHambatan.setToolTipText("Alt+1");
        BtnCariHambatan.setName("BtnCariHambatan"); // NOI18N
        BtnCariHambatan.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariHambatan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariHambatanActionPerformed(evt);
            }
        });
        BtnCariHambatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariHambatanKeyPressed(evt);
            }
        });
        FormInput.add(BtnCariHambatan);
        BtnCariHambatan.setBounds(316, 310, 28, 23);

        BtnAllHambatan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAllHambatan.setMnemonic('2');
        BtnAllHambatan.setToolTipText("2Alt+2");
        BtnAllHambatan.setName("BtnAllHambatan"); // NOI18N
        BtnAllHambatan.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAllHambatan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllHambatanActionPerformed(evt);
            }
        });
        BtnAllHambatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllHambatanKeyPressed(evt);
            }
        });
        FormInput.add(BtnAllHambatan);
        BtnAllHambatan.setBounds(348, 310, 28, 23);

        BtnTambahHambatan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambahHambatan.setMnemonic('3');
        BtnTambahHambatan.setToolTipText("Alt+3");
        BtnTambahHambatan.setName("BtnTambahHambatan"); // NOI18N
        BtnTambahHambatan.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnTambahHambatan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahHambatanActionPerformed(evt);
            }
        });
        FormInput.add(BtnTambahHambatan);
        BtnTambahHambatan.setBounds(380, 310, 28, 23);

        label13.setText("Key Word :");
        label13.setName("label13"); // NOI18N
        label13.setPreferredSize(new java.awt.Dimension(60, 23));
        FormInput.add(label13);
        label13.setBounds(460, 310, 60, 23);

        TCariKebutuhan.setToolTipText("Alt+C");
        TCariKebutuhan.setName("TCariKebutuhan"); // NOI18N
        TCariKebutuhan.setPreferredSize(new java.awt.Dimension(140, 23));
        TCariKebutuhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKebutuhanKeyPressed(evt);
            }
        });
        FormInput.add(TCariKebutuhan);
        TCariKebutuhan.setBounds(530, 310, 215, 23);

        BtnCariKebutuhan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCariKebutuhan.setMnemonic('1');
        BtnCariKebutuhan.setToolTipText("Alt+1");
        BtnCariKebutuhan.setName("BtnCariKebutuhan"); // NOI18N
        BtnCariKebutuhan.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariKebutuhan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariKebutuhanActionPerformed(evt);
            }
        });
        BtnCariKebutuhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKebutuhanKeyPressed(evt);
            }
        });
        FormInput.add(BtnCariKebutuhan);
        BtnCariKebutuhan.setBounds(750, 310, 28, 23);

        BtnAllKebutuhan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAllKebutuhan.setMnemonic('2');
        BtnAllKebutuhan.setToolTipText("2Alt+2");
        BtnAllKebutuhan.setName("BtnAllKebutuhan"); // NOI18N
        BtnAllKebutuhan.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAllKebutuhan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllKebutuhanActionPerformed(evt);
            }
        });
        BtnAllKebutuhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKebutuhanKeyPressed(evt);
            }
        });
        FormInput.add(BtnAllKebutuhan);
        BtnAllKebutuhan.setBounds(780, 310, 28, 23);

        BtnTambahKebutuhan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambahKebutuhan.setMnemonic('3');
        BtnTambahKebutuhan.setToolTipText("Alt+3");
        BtnTambahKebutuhan.setName("BtnTambahKebutuhan"); // NOI18N
        BtnTambahKebutuhan.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnTambahKebutuhan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahKebutuhanActionPerformed(evt);
            }
        });
        FormInput.add(BtnTambahKebutuhan);
        BtnTambahKebutuhan.setBounds(810, 310, 28, 23);

        TglHakWP.setForeground(new java.awt.Color(50, 70, 50));
        TglHakWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglHakWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglHakWP.setName("TglHakWP"); // NOI18N
        TglHakWP.setOpaque(false);
        TglHakWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglHakWPKeyPressed(evt);
            }
        });
        FormInput.add(TglHakWP);
        TglHakWP.setBounds(290, 390, 130, 23);

        TglOrientasiWP.setForeground(new java.awt.Color(50, 70, 50));
        TglOrientasiWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglOrientasiWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglOrientasiWP.setName("TglOrientasiWP"); // NOI18N
        TglOrientasiWP.setOpaque(false);
        TglOrientasiWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglOrientasiWPKeyPressed(evt);
            }
        });
        FormInput.add(TglOrientasiWP);
        TglOrientasiWP.setBounds(290, 420, 130, 23);

        TglDiagnosaWP.setForeground(new java.awt.Color(50, 70, 50));
        TglDiagnosaWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglDiagnosaWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglDiagnosaWP.setName("TglDiagnosaWP"); // NOI18N
        TglDiagnosaWP.setOpaque(false);
        TglDiagnosaWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglDiagnosaWPKeyPressed(evt);
            }
        });
        FormInput.add(TglDiagnosaWP);
        TglDiagnosaWP.setBounds(290, 450, 130, 23);

        TglObatWP.setForeground(new java.awt.Color(50, 70, 50));
        TglObatWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglObatWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglObatWP.setName("TglObatWP"); // NOI18N
        TglObatWP.setOpaque(false);
        TglObatWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglObatWPKeyPressed(evt);
            }
        });
        FormInput.add(TglObatWP);
        TglObatWP.setBounds(290, 480, 130, 23);

        TglAlatWP.setForeground(new java.awt.Color(50, 70, 50));
        TglAlatWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglAlatWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglAlatWP.setName("TglAlatWP"); // NOI18N
        TglAlatWP.setOpaque(false);
        TglAlatWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglAlatWPKeyPressed(evt);
            }
        });
        FormInput.add(TglAlatWP);
        TglAlatWP.setBounds(290, 510, 130, 23);

        TglDietWP.setForeground(new java.awt.Color(50, 70, 50));
        TglDietWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglDietWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglDietWP.setName("TglDietWP"); // NOI18N
        TglDietWP.setOpaque(false);
        TglDietWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglDietWPKeyPressed(evt);
            }
        });
        FormInput.add(TglDietWP);
        TglDietWP.setBounds(290, 540, 130, 23);

        TglRehabWP.setForeground(new java.awt.Color(50, 70, 50));
        TglRehabWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglRehabWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglRehabWP.setName("TglRehabWP"); // NOI18N
        TglRehabWP.setOpaque(false);
        TglRehabWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglRehabWPKeyPressed(evt);
            }
        });
        FormInput.add(TglRehabWP);
        TglRehabWP.setBounds(290, 570, 130, 23);

        TglNyeriWP.setForeground(new java.awt.Color(50, 70, 50));
        TglNyeriWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglNyeriWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglNyeriWP.setName("TglNyeriWP"); // NOI18N
        TglNyeriWP.setOpaque(false);
        TglNyeriWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglNyeriWPKeyPressed(evt);
            }
        });
        FormInput.add(TglNyeriWP);
        TglNyeriWP.setBounds(290, 600, 130, 23);

        TglInfeksiWP.setForeground(new java.awt.Color(50, 70, 50));
        TglInfeksiWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglInfeksiWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglInfeksiWP.setName("TglInfeksiWP"); // NOI18N
        TglInfeksiWP.setOpaque(false);
        TglInfeksiWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglInfeksiWPKeyPressed(evt);
            }
        });
        FormInput.add(TglInfeksiWP);
        TglInfeksiWP.setBounds(290, 630, 130, 23);

        TglRawatWP.setForeground(new java.awt.Color(50, 70, 50));
        TglRawatWP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglRawatWP.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglRawatWP.setName("TglRawatWP"); // NOI18N
        TglRawatWP.setOpaque(false);
        TglRawatWP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglRawatWPKeyPressed(evt);
            }
        });
        FormInput.add(TglRawatWP);
        TglRawatWP.setBounds(290, 660, 130, 23);

        HakS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        HakS.setName("HakS"); // NOI18N
        HakS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HakSActionPerformed(evt);
            }
        });
        HakS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HakSKeyPressed(evt);
            }
        });
        FormInput.add(HakS);
        HakS.setBounds(430, 390, 130, 23);

        OrientasiS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        OrientasiS.setName("OrientasiS"); // NOI18N
        OrientasiS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OrientasiSActionPerformed(evt);
            }
        });
        OrientasiS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                OrientasiSKeyPressed(evt);
            }
        });
        FormInput.add(OrientasiS);
        OrientasiS.setBounds(430, 420, 130, 23);

        DiagnosaS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        DiagnosaS.setName("DiagnosaS"); // NOI18N
        DiagnosaS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DiagnosaSActionPerformed(evt);
            }
        });
        DiagnosaS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DiagnosaSKeyPressed(evt);
            }
        });
        FormInput.add(DiagnosaS);
        DiagnosaS.setBounds(430, 450, 130, 23);

        ObatS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        ObatS.setName("ObatS"); // NOI18N
        ObatS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ObatSActionPerformed(evt);
            }
        });
        ObatS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ObatSKeyPressed(evt);
            }
        });
        FormInput.add(ObatS);
        ObatS.setBounds(430, 480, 130, 23);

        AlatS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        AlatS.setName("AlatS"); // NOI18N
        AlatS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AlatSActionPerformed(evt);
            }
        });
        AlatS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AlatSKeyPressed(evt);
            }
        });
        FormInput.add(AlatS);
        AlatS.setBounds(430, 510, 130, 23);

        DietS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        DietS.setName("DietS"); // NOI18N
        DietS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DietSActionPerformed(evt);
            }
        });
        DietS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DietSKeyPressed(evt);
            }
        });
        FormInput.add(DietS);
        DietS.setBounds(430, 540, 130, 23);

        RehabS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        RehabS.setName("RehabS"); // NOI18N
        RehabS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RehabSActionPerformed(evt);
            }
        });
        RehabS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RehabSKeyPressed(evt);
            }
        });
        FormInput.add(RehabS);
        RehabS.setBounds(430, 570, 130, 23);

        NyeriS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        NyeriS.setName("NyeriS"); // NOI18N
        NyeriS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NyeriSActionPerformed(evt);
            }
        });
        NyeriS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NyeriSKeyPressed(evt);
            }
        });
        FormInput.add(NyeriS);
        NyeriS.setBounds(430, 600, 130, 23);

        InfeksiS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        InfeksiS.setName("InfeksiS"); // NOI18N
        InfeksiS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InfeksiSActionPerformed(evt);
            }
        });
        InfeksiS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                InfeksiSKeyPressed(evt);
            }
        });
        FormInput.add(InfeksiS);
        InfeksiS.setBounds(430, 630, 130, 23);

        RawatS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Pasien", "Keluarga Pasien", "Pasien & Keluarga Pasien" }));
        RawatS.setName("RawatS"); // NOI18N
        RawatS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RawatSActionPerformed(evt);
            }
        });
        RawatS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RawatSKeyPressed(evt);
            }
        });
        FormInput.add(RawatS);
        RawatS.setBounds(430, 660, 130, 23);

        HakCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        HakCE.setName("HakCE"); // NOI18N
        HakCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HakCEActionPerformed(evt);
            }
        });
        HakCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HakCEKeyPressed(evt);
            }
        });
        FormInput.add(HakCE);
        HakCE.setBounds(580, 390, 150, 23);

        OrientasiCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        OrientasiCE.setName("OrientasiCE"); // NOI18N
        OrientasiCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OrientasiCEActionPerformed(evt);
            }
        });
        OrientasiCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                OrientasiCEKeyPressed(evt);
            }
        });
        FormInput.add(OrientasiCE);
        OrientasiCE.setBounds(580, 420, 150, 23);

        DiagnosaCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        DiagnosaCE.setName("DiagnosaCE"); // NOI18N
        DiagnosaCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DiagnosaCEActionPerformed(evt);
            }
        });
        DiagnosaCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DiagnosaCEKeyPressed(evt);
            }
        });
        FormInput.add(DiagnosaCE);
        DiagnosaCE.setBounds(580, 450, 150, 23);

        ObatCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        ObatCE.setName("ObatCE"); // NOI18N
        ObatCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ObatCEActionPerformed(evt);
            }
        });
        ObatCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ObatCEKeyPressed(evt);
            }
        });
        FormInput.add(ObatCE);
        ObatCE.setBounds(580, 480, 150, 23);

        AlatCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        AlatCE.setName("AlatCE"); // NOI18N
        AlatCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AlatCEActionPerformed(evt);
            }
        });
        AlatCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AlatCEKeyPressed(evt);
            }
        });
        FormInput.add(AlatCE);
        AlatCE.setBounds(580, 510, 150, 23);

        DietCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        DietCE.setName("DietCE"); // NOI18N
        DietCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DietCEActionPerformed(evt);
            }
        });
        DietCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DietCEKeyPressed(evt);
            }
        });
        FormInput.add(DietCE);
        DietCE.setBounds(580, 540, 150, 23);

        RehabCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        RehabCE.setName("RehabCE"); // NOI18N
        RehabCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RehabCEActionPerformed(evt);
            }
        });
        RehabCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RehabCEKeyPressed(evt);
            }
        });
        FormInput.add(RehabCE);
        RehabCE.setBounds(580, 570, 150, 23);

        NyeriCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        NyeriCE.setName("NyeriCE"); // NOI18N
        NyeriCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NyeriCEActionPerformed(evt);
            }
        });
        NyeriCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NyeriCEKeyPressed(evt);
            }
        });
        FormInput.add(NyeriCE);
        NyeriCE.setBounds(580, 600, 150, 23);

        InfeksiCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        InfeksiCE.setName("InfeksiCE"); // NOI18N
        InfeksiCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InfeksiCEActionPerformed(evt);
            }
        });
        InfeksiCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                InfeksiCEKeyPressed(evt);
            }
        });
        FormInput.add(InfeksiCE);
        InfeksiCE.setBounds(580, 630, 150, 23);

        RawatCE.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Diskusi", "Demonstrasi", "Brosur" }));
        RawatCE.setName("RawatCE"); // NOI18N
        RawatCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RawatCEActionPerformed(evt);
            }
        });
        RawatCE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RawatCEKeyPressed(evt);
            }
        });
        FormInput.add(RawatCE);
        RawatCE.setBounds(580, 660, 150, 23);

        HakME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        HakME.setName("HakME"); // NOI18N
        HakME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HakMEActionPerformed(evt);
            }
        });
        HakME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HakMEKeyPressed(evt);
            }
        });
        FormInput.add(HakME);
        HakME.setBounds(760, 390, 150, 23);

        OrientasiME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        OrientasiME.setName("OrientasiME"); // NOI18N
        OrientasiME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OrientasiMEActionPerformed(evt);
            }
        });
        OrientasiME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                OrientasiMEKeyPressed(evt);
            }
        });
        FormInput.add(OrientasiME);
        OrientasiME.setBounds(760, 420, 150, 23);

        DiagnosaME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        DiagnosaME.setName("DiagnosaME"); // NOI18N
        DiagnosaME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DiagnosaMEActionPerformed(evt);
            }
        });
        DiagnosaME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DiagnosaMEKeyPressed(evt);
            }
        });
        FormInput.add(DiagnosaME);
        DiagnosaME.setBounds(760, 450, 150, 23);

        ObatME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        ObatME.setName("ObatME"); // NOI18N
        ObatME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ObatMEActionPerformed(evt);
            }
        });
        ObatME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ObatMEKeyPressed(evt);
            }
        });
        FormInput.add(ObatME);
        ObatME.setBounds(760, 480, 150, 23);

        AlatME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        AlatME.setName("AlatME"); // NOI18N
        AlatME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AlatMEActionPerformed(evt);
            }
        });
        AlatME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AlatMEKeyPressed(evt);
            }
        });
        FormInput.add(AlatME);
        AlatME.setBounds(760, 510, 150, 23);

        DietME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        DietME.setName("DietME"); // NOI18N
        DietME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DietMEActionPerformed(evt);
            }
        });
        DietME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DietMEKeyPressed(evt);
            }
        });
        FormInput.add(DietME);
        DietME.setBounds(760, 540, 150, 23);

        RehabME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        RehabME.setName("RehabME"); // NOI18N
        RehabME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RehabMEActionPerformed(evt);
            }
        });
        RehabME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RehabMEKeyPressed(evt);
            }
        });
        FormInput.add(RehabME);
        RehabME.setBounds(760, 570, 150, 23);

        NyeriME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        NyeriME.setName("NyeriME"); // NOI18N
        NyeriME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NyeriMEActionPerformed(evt);
            }
        });
        NyeriME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NyeriMEKeyPressed(evt);
            }
        });
        FormInput.add(NyeriME);
        NyeriME.setBounds(760, 600, 150, 23);

        InfeksiME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        InfeksiME.setName("InfeksiME"); // NOI18N
        InfeksiME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InfeksiMEActionPerformed(evt);
            }
        });
        InfeksiME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                InfeksiMEKeyPressed(evt);
            }
        });
        FormInput.add(InfeksiME);
        InfeksiME.setBounds(760, 630, 150, 23);

        RawatME.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Mampu menjelaskan", "Mampu mendemonstrasikan" }));
        RawatME.setName("RawatME"); // NOI18N
        RawatME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RawatMEActionPerformed(evt);
            }
        });
        RawatME.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RawatMEKeyPressed(evt);
            }
        });
        FormInput.add(RawatME);
        RawatME.setBounds(760, 660, 150, 23);

        jLabel90.setText("6. PPA Lainnya");
        jLabel90.setName("jLabel90"); // NOI18N
        FormInput.add(jLabel90);
        jLabel90.setBounds(0, 1450, 80, 23);

        TglLainnya.setForeground(new java.awt.Color(50, 70, 50));
        TglLainnya.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024 12:05:13" }));
        TglLainnya.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglLainnya.setName("TglLainnya"); // NOI18N
        TglLainnya.setOpaque(false);
        TglLainnya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglLainnyaKeyPressed(evt);
            }
        });
        FormInput.add(TglLainnya);
        TglLainnya.setBounds(370, 1450, 130, 23);

        DurasiLainnya.setFocusTraversalPolicyProvider(true);
        DurasiLainnya.setName("DurasiLainnya"); // NOI18N
        DurasiLainnya.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DurasiLainnyaActionPerformed(evt);
            }
        });
        DurasiLainnya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DurasiLainnyaKeyPressed(evt);
            }
        });
        FormInput.add(DurasiLainnya);
        DurasiLainnya.setBounds(530, 1450, 90, 23);

        scrollPane17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane17.setName("scrollPane17"); // NOI18N

        EvaluasiLainnya.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EvaluasiLainnya.setColumns(20);
        EvaluasiLainnya.setRows(5);
        EvaluasiLainnya.setName("EvaluasiLainnya"); // NOI18N
        EvaluasiLainnya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EvaluasiLainnyaKeyPressed(evt);
            }
        });
        scrollPane17.setViewportView(EvaluasiLainnya);

        FormInput.add(scrollPane17);
        scrollPane17.setBounds(640, 1450, 260, 70);

        Scroll10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll10.setName("Scroll10"); // NOI18N
        Scroll10.setOpaque(true);

        tbEdukasiDokter.setName("tbEdukasiDokter"); // NOI18N
        tbEdukasiDokter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbEdukasiDokterMouseClicked(evt);
            }
        });
        tbEdukasiDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbEdukasiDokterKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbEdukasiDokterKeyReleased(evt);
            }
        });
        Scroll10.setViewportView(tbEdukasiDokter);

        FormInput.add(Scroll10);
        Scroll10.setBounds(90, 800, 260, 90);

        label15.setText("Key Word :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(60, 23));
        FormInput.add(label15);
        label15.setBounds(33, 900, 60, 23);

        TCariEdukasiDokter.setToolTipText("Alt+C");
        TCariEdukasiDokter.setName("TCariEdukasiDokter"); // NOI18N
        TCariEdukasiDokter.setPreferredSize(new java.awt.Dimension(140, 23));
        TCariEdukasiDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariEdukasiDokterKeyPressed(evt);
            }
        });
        FormInput.add(TCariEdukasiDokter);
        TCariEdukasiDokter.setBounds(97, 900, 160, 23);

        BtnCariEdukasiDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCariEdukasiDokter.setMnemonic('1');
        BtnCariEdukasiDokter.setToolTipText("Alt+1");
        BtnCariEdukasiDokter.setName("BtnCariEdukasiDokter"); // NOI18N
        BtnCariEdukasiDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariEdukasiDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariEdukasiDokterActionPerformed(evt);
            }
        });
        BtnCariEdukasiDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariEdukasiDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnCariEdukasiDokter);
        BtnCariEdukasiDokter.setBounds(260, 900, 28, 23);

        BtnAllEdukasiDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAllEdukasiDokter.setMnemonic('2');
        BtnAllEdukasiDokter.setToolTipText("2Alt+2");
        BtnAllEdukasiDokter.setName("BtnAllEdukasiDokter"); // NOI18N
        BtnAllEdukasiDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAllEdukasiDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllEdukasiDokterActionPerformed(evt);
            }
        });
        BtnAllEdukasiDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllEdukasiDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnAllEdukasiDokter);
        BtnAllEdukasiDokter.setBounds(290, 900, 28, 23);

        BtnTambahEdukasiDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambahEdukasiDokter.setMnemonic('3');
        BtnTambahEdukasiDokter.setToolTipText("Alt+3");
        BtnTambahEdukasiDokter.setName("BtnTambahEdukasiDokter"); // NOI18N
        BtnTambahEdukasiDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnTambahEdukasiDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahEdukasiDokterActionPerformed(evt);
            }
        });
        FormInput.add(BtnTambahEdukasiDokter);
        BtnTambahEdukasiDokter.setBounds(320, 900, 28, 23);

        Scroll11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll11.setName("Scroll11"); // NOI18N
        Scroll11.setOpaque(true);

        tbEdukasiPerawat.setName("tbEdukasiPerawat"); // NOI18N
        tbEdukasiPerawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbEdukasiPerawatMouseClicked(evt);
            }
        });
        tbEdukasiPerawat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbEdukasiPerawatKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbEdukasiPerawatKeyReleased(evt);
            }
        });
        Scroll11.setViewportView(tbEdukasiPerawat);

        FormInput.add(Scroll11);
        Scroll11.setBounds(90, 930, 260, 90);

        label16.setText("Key Word :");
        label16.setName("label16"); // NOI18N
        label16.setPreferredSize(new java.awt.Dimension(60, 23));
        FormInput.add(label16);
        label16.setBounds(33, 1030, 60, 23);

        TCariEdukasiPerawat.setToolTipText("Alt+C");
        TCariEdukasiPerawat.setName("TCariEdukasiPerawat"); // NOI18N
        TCariEdukasiPerawat.setPreferredSize(new java.awt.Dimension(140, 23));
        TCariEdukasiPerawat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariEdukasiPerawatKeyPressed(evt);
            }
        });
        FormInput.add(TCariEdukasiPerawat);
        TCariEdukasiPerawat.setBounds(97, 1030, 160, 23);

        BtnCariEdukasiPerawat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCariEdukasiPerawat.setMnemonic('1');
        BtnCariEdukasiPerawat.setToolTipText("Alt+1");
        BtnCariEdukasiPerawat.setName("BtnCariEdukasiPerawat"); // NOI18N
        BtnCariEdukasiPerawat.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariEdukasiPerawat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariEdukasiPerawatActionPerformed(evt);
            }
        });
        BtnCariEdukasiPerawat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariEdukasiPerawatKeyPressed(evt);
            }
        });
        FormInput.add(BtnCariEdukasiPerawat);
        BtnCariEdukasiPerawat.setBounds(260, 1030, 28, 23);

        BtnAllEdukasiPerawat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAllEdukasiPerawat.setMnemonic('2');
        BtnAllEdukasiPerawat.setToolTipText("2Alt+2");
        BtnAllEdukasiPerawat.setName("BtnAllEdukasiPerawat"); // NOI18N
        BtnAllEdukasiPerawat.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAllEdukasiPerawat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllEdukasiPerawatActionPerformed(evt);
            }
        });
        BtnAllEdukasiPerawat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllEdukasiPerawatKeyPressed(evt);
            }
        });
        FormInput.add(BtnAllEdukasiPerawat);
        BtnAllEdukasiPerawat.setBounds(290, 1030, 28, 23);

        BtnTambahEdukasiPerawat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambahEdukasiPerawat.setMnemonic('3');
        BtnTambahEdukasiPerawat.setToolTipText("Alt+3");
        BtnTambahEdukasiPerawat.setName("BtnTambahEdukasiPerawat"); // NOI18N
        BtnTambahEdukasiPerawat.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnTambahEdukasiPerawat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahEdukasiPerawatActionPerformed(evt);
            }
        });
        FormInput.add(BtnTambahEdukasiPerawat);
        BtnTambahEdukasiPerawat.setBounds(320, 1030, 28, 23);

        Scroll12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll12.setName("Scroll12"); // NOI18N
        Scroll12.setOpaque(true);

        tbEdukasiGizi.setName("tbEdukasiGizi"); // NOI18N
        tbEdukasiGizi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbEdukasiGiziMouseClicked(evt);
            }
        });
        tbEdukasiGizi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbEdukasiGiziKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbEdukasiGiziKeyReleased(evt);
            }
        });
        Scroll12.setViewportView(tbEdukasiGizi);

        FormInput.add(Scroll12);
        Scroll12.setBounds(90, 1060, 260, 90);

        label17.setText("Key Word :");
        label17.setName("label17"); // NOI18N
        label17.setPreferredSize(new java.awt.Dimension(60, 23));
        FormInput.add(label17);
        label17.setBounds(33, 1160, 60, 23);

        TCariEdukasiGizi.setToolTipText("Alt+C");
        TCariEdukasiGizi.setName("TCariEdukasiGizi"); // NOI18N
        TCariEdukasiGizi.setPreferredSize(new java.awt.Dimension(140, 23));
        TCariEdukasiGizi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariEdukasiGiziKeyPressed(evt);
            }
        });
        FormInput.add(TCariEdukasiGizi);
        TCariEdukasiGizi.setBounds(97, 1160, 160, 23);

        BtnCariEdukasiGizi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCariEdukasiGizi.setMnemonic('1');
        BtnCariEdukasiGizi.setToolTipText("Alt+1");
        BtnCariEdukasiGizi.setName("BtnCariEdukasiGizi"); // NOI18N
        BtnCariEdukasiGizi.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariEdukasiGizi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariEdukasiGiziActionPerformed(evt);
            }
        });
        BtnCariEdukasiGizi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariEdukasiGiziKeyPressed(evt);
            }
        });
        FormInput.add(BtnCariEdukasiGizi);
        BtnCariEdukasiGizi.setBounds(260, 1160, 28, 23);

        BtnAllEdukasiGizi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAllEdukasiGizi.setMnemonic('2');
        BtnAllEdukasiGizi.setToolTipText("2Alt+2");
        BtnAllEdukasiGizi.setName("BtnAllEdukasiGizi"); // NOI18N
        BtnAllEdukasiGizi.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAllEdukasiGizi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllEdukasiGiziActionPerformed(evt);
            }
        });
        BtnAllEdukasiGizi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllEdukasiGiziKeyPressed(evt);
            }
        });
        FormInput.add(BtnAllEdukasiGizi);
        BtnAllEdukasiGizi.setBounds(290, 1160, 28, 23);

        BtnTambahEdukasiGizi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambahEdukasiGizi.setMnemonic('3');
        BtnTambahEdukasiGizi.setToolTipText("Alt+3");
        BtnTambahEdukasiGizi.setName("BtnTambahEdukasiGizi"); // NOI18N
        BtnTambahEdukasiGizi.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnTambahEdukasiGizi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahEdukasiGiziActionPerformed(evt);
            }
        });
        FormInput.add(BtnTambahEdukasiGizi);
        BtnTambahEdukasiGizi.setBounds(320, 1160, 28, 23);

        Scroll13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll13.setName("Scroll13"); // NOI18N
        Scroll13.setOpaque(true);

        tbEdukasiFarmasi.setName("tbEdukasiFarmasi"); // NOI18N
        tbEdukasiFarmasi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbEdukasiFarmasiMouseClicked(evt);
            }
        });
        tbEdukasiFarmasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbEdukasiFarmasiKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbEdukasiFarmasiKeyReleased(evt);
            }
        });
        Scroll13.setViewportView(tbEdukasiFarmasi);

        FormInput.add(Scroll13);
        Scroll13.setBounds(90, 1190, 260, 90);

        label18.setText("Key Word :");
        label18.setName("label18"); // NOI18N
        label18.setPreferredSize(new java.awt.Dimension(60, 23));
        FormInput.add(label18);
        label18.setBounds(33, 1290, 60, 23);

        TCariEdukasiFarmasi.setToolTipText("Alt+C");
        TCariEdukasiFarmasi.setName("TCariEdukasiFarmasi"); // NOI18N
        TCariEdukasiFarmasi.setPreferredSize(new java.awt.Dimension(140, 23));
        TCariEdukasiFarmasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariEdukasiFarmasiKeyPressed(evt);
            }
        });
        FormInput.add(TCariEdukasiFarmasi);
        TCariEdukasiFarmasi.setBounds(97, 1290, 160, 23);

        BtnCariEdukasiFarmasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCariEdukasiFarmasi.setMnemonic('1');
        BtnCariEdukasiFarmasi.setToolTipText("Alt+1");
        BtnCariEdukasiFarmasi.setName("BtnCariEdukasiFarmasi"); // NOI18N
        BtnCariEdukasiFarmasi.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariEdukasiFarmasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariEdukasiFarmasiActionPerformed(evt);
            }
        });
        BtnCariEdukasiFarmasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariEdukasiFarmasiKeyPressed(evt);
            }
        });
        FormInput.add(BtnCariEdukasiFarmasi);
        BtnCariEdukasiFarmasi.setBounds(260, 1290, 28, 23);

        BtnAllEdukasiFarmasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAllEdukasiFarmasi.setMnemonic('2');
        BtnAllEdukasiFarmasi.setToolTipText("2Alt+2");
        BtnAllEdukasiFarmasi.setName("BtnAllEdukasiFarmasi"); // NOI18N
        BtnAllEdukasiFarmasi.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAllEdukasiFarmasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllEdukasiFarmasiActionPerformed(evt);
            }
        });
        BtnAllEdukasiFarmasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllEdukasiFarmasiKeyPressed(evt);
            }
        });
        FormInput.add(BtnAllEdukasiFarmasi);
        BtnAllEdukasiFarmasi.setBounds(290, 1290, 28, 23);

        BtnTambahEdukasiFarmasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambahEdukasiFarmasi.setMnemonic('3');
        BtnTambahEdukasiFarmasi.setToolTipText("Alt+3");
        BtnTambahEdukasiFarmasi.setName("BtnTambahEdukasiFarmasi"); // NOI18N
        BtnTambahEdukasiFarmasi.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnTambahEdukasiFarmasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahEdukasiFarmasiActionPerformed(evt);
            }
        });
        FormInput.add(BtnTambahEdukasiFarmasi);
        BtnTambahEdukasiFarmasi.setBounds(320, 1290, 28, 23);

        Scroll14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll14.setName("Scroll14"); // NOI18N
        Scroll14.setOpaque(true);

        tbEdukasiNyeri.setName("tbEdukasiNyeri"); // NOI18N
        tbEdukasiNyeri.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbEdukasiNyeriMouseClicked(evt);
            }
        });
        tbEdukasiNyeri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbEdukasiNyeriKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbEdukasiNyeriKeyReleased(evt);
            }
        });
        Scroll14.setViewportView(tbEdukasiNyeri);

        FormInput.add(Scroll14);
        Scroll14.setBounds(90, 1320, 260, 90);

        label19.setText("Key Word :");
        label19.setName("label19"); // NOI18N
        label19.setPreferredSize(new java.awt.Dimension(60, 23));
        FormInput.add(label19);
        label19.setBounds(33, 1420, 60, 23);

        TCariEdukasiNyeri.setToolTipText("Alt+C");
        TCariEdukasiNyeri.setName("TCariEdukasiNyeri"); // NOI18N
        TCariEdukasiNyeri.setPreferredSize(new java.awt.Dimension(140, 23));
        TCariEdukasiNyeri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariEdukasiNyeriKeyPressed(evt);
            }
        });
        FormInput.add(TCariEdukasiNyeri);
        TCariEdukasiNyeri.setBounds(97, 1420, 160, 23);

        BtnCariEdukasiNyeri.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCariEdukasiNyeri.setMnemonic('1');
        BtnCariEdukasiNyeri.setToolTipText("Alt+1");
        BtnCariEdukasiNyeri.setName("BtnCariEdukasiNyeri"); // NOI18N
        BtnCariEdukasiNyeri.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariEdukasiNyeri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariEdukasiNyeriActionPerformed(evt);
            }
        });
        BtnCariEdukasiNyeri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariEdukasiNyeriKeyPressed(evt);
            }
        });
        FormInput.add(BtnCariEdukasiNyeri);
        BtnCariEdukasiNyeri.setBounds(260, 1420, 28, 23);

        BtnAllEdukasiNyeri.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAllEdukasiNyeri.setMnemonic('2');
        BtnAllEdukasiNyeri.setToolTipText("2Alt+2");
        BtnAllEdukasiNyeri.setName("BtnAllEdukasiNyeri"); // NOI18N
        BtnAllEdukasiNyeri.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAllEdukasiNyeri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllEdukasiNyeriActionPerformed(evt);
            }
        });
        BtnAllEdukasiNyeri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllEdukasiNyeriKeyPressed(evt);
            }
        });
        FormInput.add(BtnAllEdukasiNyeri);
        BtnAllEdukasiNyeri.setBounds(290, 1420, 28, 23);

        BtnTambahEdukasiNyeri.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambahEdukasiNyeri.setMnemonic('3');
        BtnTambahEdukasiNyeri.setToolTipText("Alt+3");
        BtnTambahEdukasiNyeri.setName("BtnTambahEdukasiNyeri"); // NOI18N
        BtnTambahEdukasiNyeri.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnTambahEdukasiNyeri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahEdukasiNyeriActionPerformed(evt);
            }
        });
        FormInput.add(BtnTambahEdukasiNyeri);
        BtnTambahEdukasiNyeri.setBounds(320, 1420, 28, 23);

        Scroll15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll15.setName("Scroll15"); // NOI18N
        Scroll15.setOpaque(true);

        tbEdukasiLainnya.setName("tbEdukasiLainnya"); // NOI18N
        tbEdukasiLainnya.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbEdukasiLainnyaMouseClicked(evt);
            }
        });
        tbEdukasiLainnya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbEdukasiLainnyaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbEdukasiLainnyaKeyReleased(evt);
            }
        });
        Scroll15.setViewportView(tbEdukasiLainnya);

        FormInput.add(Scroll15);
        Scroll15.setBounds(90, 1450, 260, 90);

        label20.setText("Key Word :");
        label20.setName("label20"); // NOI18N
        label20.setPreferredSize(new java.awt.Dimension(60, 23));
        FormInput.add(label20);
        label20.setBounds(33, 1550, 60, 23);

        TCariEdukasiLainnya.setToolTipText("Alt+C");
        TCariEdukasiLainnya.setName("TCariEdukasiLainnya"); // NOI18N
        TCariEdukasiLainnya.setPreferredSize(new java.awt.Dimension(140, 23));
        TCariEdukasiLainnya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariEdukasiLainnyaKeyPressed(evt);
            }
        });
        FormInput.add(TCariEdukasiLainnya);
        TCariEdukasiLainnya.setBounds(97, 1550, 160, 23);

        BtnCariEdukasiLainnya.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCariEdukasiLainnya.setMnemonic('1');
        BtnCariEdukasiLainnya.setToolTipText("Alt+1");
        BtnCariEdukasiLainnya.setName("BtnCariEdukasiLainnya"); // NOI18N
        BtnCariEdukasiLainnya.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariEdukasiLainnya.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariEdukasiLainnyaActionPerformed(evt);
            }
        });
        BtnCariEdukasiLainnya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariEdukasiLainnyaKeyPressed(evt);
            }
        });
        FormInput.add(BtnCariEdukasiLainnya);
        BtnCariEdukasiLainnya.setBounds(260, 1550, 28, 23);

        BtnAllEdukasiLainnya.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAllEdukasiLainnya.setMnemonic('2');
        BtnAllEdukasiLainnya.setToolTipText("2Alt+2");
        BtnAllEdukasiLainnya.setName("BtnAllEdukasiLainnya"); // NOI18N
        BtnAllEdukasiLainnya.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAllEdukasiLainnya.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllEdukasiLainnyaActionPerformed(evt);
            }
        });
        BtnAllEdukasiLainnya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllEdukasiLainnyaKeyPressed(evt);
            }
        });
        FormInput.add(BtnAllEdukasiLainnya);
        BtnAllEdukasiLainnya.setBounds(290, 1550, 28, 23);

        BtnTambahEdukasiLainnya.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambahEdukasiLainnya.setMnemonic('3');
        BtnTambahEdukasiLainnya.setToolTipText("Alt+3");
        BtnTambahEdukasiLainnya.setName("BtnTambahEdukasiLainnya"); // NOI18N
        BtnTambahEdukasiLainnya.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnTambahEdukasiLainnya.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahEdukasiLainnyaActionPerformed(evt);
            }
        });
        FormInput.add(BtnTambahEdukasiLainnya);
        BtnTambahEdukasiLainnya.setBounds(320, 1550, 28, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Edukasi", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Edukasi", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmPetugas.getText().trim().equals("")){    
            Valid.textKosong(BtnDokter,"Petugas");
        }else if(Bahasa.getText().equals("") || Penerjemah.getSelectedItem().toString().equals("-") || BahasaIsyarat.getSelectedItem().toString().equals("-") || 
                CaraBelajar.getSelectedItem().toString().equals("-") || Pendidikan.getSelectedItem().toString().equals("-") || Hambatan.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Bahasa, "Pengkajian Kebutuhan Edukasi");
        }else{ 
            if(Sequel.menyimpantf("edukasi_pasien","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",67,new String[]{    
                TNoRw.getText(),Valid.SetTgl(TglAsuhan.getSelectedItem()+"")+" "+TglAsuhan.getSelectedItem().toString().substring(11,19),Bahasa.getText(),Penerjemah.getSelectedItem().toString(),
                BahasaIsyarat.getSelectedItem().toString(),CaraBelajar.getSelectedItem().toString(),Pendidikan.getSelectedItem().toString(),Hambatan.getSelectedItem().toString(),
                Valid.SetTgl(TglHakWP.getSelectedItem()+"")+" "+TglHakWP.getSelectedItem().toString().substring(11,19),HakS.getSelectedItem().toString(),HakCE.getSelectedItem().toString(),HakME.getSelectedItem().toString(),
                Valid.SetTgl(TglOrientasiWP.getSelectedItem()+"")+" "+TglOrientasiWP.getSelectedItem().toString().substring(11,19),OrientasiS.getSelectedItem().toString(),OrientasiCE.getSelectedItem().toString(),OrientasiME.getSelectedItem().toString(),
                Valid.SetTgl(TglDiagnosaWP.getSelectedItem()+"")+" "+TglDiagnosaWP.getSelectedItem().toString().substring(11,19),DiagnosaS.getSelectedItem().toString(),DiagnosaCE.getSelectedItem().toString(),DiagnosaME.getSelectedItem().toString(),
                Valid.SetTgl(TglObatWP.getSelectedItem()+"")+" "+TglObatWP.getSelectedItem().toString().substring(11,19),ObatS.getSelectedItem().toString(),ObatCE.getSelectedItem().toString(),ObatME.getSelectedItem().toString(),
                Valid.SetTgl(TglAlatWP.getSelectedItem()+"")+" "+TglAlatWP.getSelectedItem().toString().substring(11,19),AlatS.getSelectedItem().toString(),AlatCE.getSelectedItem().toString(),AlatME.getSelectedItem().toString(),
                Valid.SetTgl(TglDietWP.getSelectedItem()+"")+" "+TglDietWP.getSelectedItem().toString().substring(11,19),DietS.getSelectedItem().toString(),DietCE.getSelectedItem().toString(),DietME.getSelectedItem().toString(),
                Valid.SetTgl(TglRehabWP.getSelectedItem()+"")+" "+TglRehabWP.getSelectedItem().toString().substring(11,19),RehabS.getSelectedItem().toString(),RehabCE.getSelectedItem().toString(),RehabME.getSelectedItem().toString(),
                Valid.SetTgl(TglNyeriWP.getSelectedItem()+"")+" "+TglNyeriWP.getSelectedItem().toString().substring(11,19),NyeriS.getSelectedItem().toString(),NyeriCE.getSelectedItem().toString(),NyeriME.getSelectedItem().toString(),
                Valid.SetTgl(TglInfeksiWP.getSelectedItem()+"")+" "+TglInfeksiWP.getSelectedItem().toString().substring(11,19),InfeksiS.getSelectedItem().toString(),InfeksiCE.getSelectedItem().toString(),InfeksiME.getSelectedItem().toString(),
                Valid.SetTgl(TglRawatWP.getSelectedItem()+"")+" "+TglRawatWP.getSelectedItem().toString().substring(11,19),RawatS.getSelectedItem().toString(),RawatCE.getSelectedItem().toString(),RawatME.getSelectedItem().toString(),
                Valid.SetTgl(TglDokter.getSelectedItem()+"")+" "+TglDokter.getSelectedItem().toString().substring(11,19),DurasiDokter.getText(),EvaluasiDokter.getText(),
                Valid.SetTgl(TglPerawat.getSelectedItem()+"")+" "+TglPerawat.getSelectedItem().toString().substring(11,19),DurasiPerawat.getText(),EvaluasiPerawat.getText(),
                Valid.SetTgl(TglGizi.getSelectedItem()+"")+" "+TglGizi.getSelectedItem().toString().substring(11,19),DurasiGizi.getText(),EvaluasiGizi.getText(),
                Valid.SetTgl(TglFarmasi.getSelectedItem()+"")+" "+TglFarmasi.getSelectedItem().toString().substring(11,19),DurasiFarmasi.getText(),EvaluasiFarmasi.getText(),
                Valid.SetTgl(TglNyeri.getSelectedItem()+"")+" "+TglNyeri.getSelectedItem().toString().substring(11,19),DurasiNyeri.getText(),EvaluasiNyeri.getText(),
                Valid.SetTgl(TglLainnya.getSelectedItem()+"")+" "+TglLainnya.getSelectedItem().toString().substring(11,19),DurasiLainnya.getText(),EvaluasiLainnya.getText(),
                KdPetugas.getText()    
                })==true){
                    //HAMBATAN BELAJAR
                    for (i = 0; i < tbHambatanBelajar.getRowCount(); i++) {
                        if(tbHambatanBelajar.getValueAt(i,0).toString().equals("true")){
                            Sequel.menyimpan2("hambatan_belajar","?,?",2,new String[]{TNoRw.getText(),tbHambatanBelajar.getValueAt(i,1).toString()});
                        }
                    }
                    //KEBUTUHAN EDUKASI
                    for (i = 0; i < tbKebutuhanEdukasi.getRowCount(); i++) {
                        if(tbKebutuhanEdukasi.getValueAt(i,0).toString().equals("true")){
                            Sequel.menyimpan2("kebutuhan_edukasi","?,?",2,new String[]{TNoRw.getText(),tbKebutuhanEdukasi.getValueAt(i,1).toString()});
                        }
                    }
                    //EDUKASI DOKTER
                    for (i = 0; i < tbEdukasiDokter.getRowCount(); i++) {
                        if(tbEdukasiDokter.getValueAt(i,0).toString().equals("true")){
                            Sequel.menyimpan2("edukasi_dokter","?,?",2,new String[]{TNoRw.getText(),tbEdukasiDokter.getValueAt(i,1).toString()});
                        }
                    }
                    //EDUKASI PERAWAT
                    for (i = 0; i < tbEdukasiPerawat.getRowCount(); i++) {
                        if(tbEdukasiPerawat.getValueAt(i,0).toString().equals("true")){
                            Sequel.menyimpan2("edukasi_perawat","?,?",2,new String[]{TNoRw.getText(),tbEdukasiPerawat.getValueAt(i,1).toString()});
                        }
                    }       
                    //EDUKASI GIZI
                    for (i = 0; i < tbEdukasiGizi.getRowCount(); i++) {
                        if(tbEdukasiGizi.getValueAt(i,0).toString().equals("true")){
                            Sequel.menyimpan2("edukasi_gizi","?,?",2,new String[]{TNoRw.getText(),tbEdukasiGizi.getValueAt(i,1).toString()});
                        }
                    }                    
                    //EDUKASI FARMASI
                    for (i = 0; i < tbEdukasiFarmasi.getRowCount(); i++) {
                        if(tbEdukasiFarmasi.getValueAt(i,0).toString().equals("true")){
                            Sequel.menyimpan2("edukasi_farmasi","?,?",2,new String[]{TNoRw.getText(),tbEdukasiFarmasi.getValueAt(i,1).toString()});
                        }
                    }
                    //EDUKASI NYERI                    
                    for (i = 0; i < tbEdukasiNyeri.getRowCount(); i++) {
                        if(tbEdukasiNyeri.getValueAt(i,0).toString().equals("true")){
                            Sequel.menyimpan2("edukasi_nyeri","?,?",2,new String[]{TNoRw.getText(),tbEdukasiNyeri.getValueAt(i,1).toString()});
                        }
                    }                    
                    //EDUKASI LAINNYA
                    for (i = 0; i < tbEdukasiLainnya.getRowCount(); i++) {
                        if(tbEdukasiLainnya.getValueAt(i,0).toString().equals("true")){
                            Sequel.menyimpan2("edukasi_lainnya","?,?",2,new String[]{TNoRw.getText(),tbEdukasiLainnya.getValueAt(i,1).toString()});
                        }
                    }                    
                    
                    //EMPT TEKS
                    emptTeks();
            }           
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,RawatME,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(KdPetugas.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),69).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmPetugas.getText().trim().equals("")){    
            Valid.textKosong(BtnDokter,"Petugas");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(KdPetugas.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),69).toString())){
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh petugas yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }   
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
//        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        if(tabMode.getRowCount()==0){
//            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//            BtnBatal.requestFocus();
//        }else if(tabMode.getRowCount()!=0){
//            try{
//                if(TCari.getText().equals("")){
//                    ps=koneksi.prepareStatement(
//                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat,penilaian_awal_keperawatan_ralan.tanggal,"+
//                            "penilaian_awal_keperawatan_ralan.informasi,penilaian_awal_keperawatan_ralan.td,penilaian_awal_keperawatan_ralan.nadi,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,"+
//                            "penilaian_awal_keperawatan_ralan.spo2,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.gcs,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,penilaian_awal_keperawatan_ralan.bmi,penilaian_awal_keperawatan_ralan.keluhan_utama,"+    //CUSTOM MUHSIN -> NADI DOUBLE, GANTI 1 JADI SPO2
//                            "penilaian_awal_keperawatan_ralan.rpd,penilaian_awal_keperawatan_ralan.rpk,penilaian_awal_keperawatan_ralan.rpo,penilaian_awal_keperawatan_ralan.alergi,penilaian_awal_keperawatan_ralan.alat_bantu,penilaian_awal_keperawatan_ralan.ket_bantu,penilaian_awal_keperawatan_ralan.prothesa,"+
//                            "penilaian_awal_keperawatan_ralan.ket_pro,penilaian_awal_keperawatan_ralan.adl,penilaian_awal_keperawatan_ralan.status_psiko,penilaian_awal_keperawatan_ralan.ket_psiko,penilaian_awal_keperawatan_ralan.hub_keluarga,penilaian_awal_keperawatan_ralan.tinggal_dengan,"+
//                            "penilaian_awal_keperawatan_ralan.ket_tinggal,penilaian_awal_keperawatan_ralan.ekonomi,penilaian_awal_keperawatan_ralan.edukasi,penilaian_awal_keperawatan_ralan.ket_edukasi,penilaian_awal_keperawatan_ralan.berjalan_a,penilaian_awal_keperawatan_ralan.berjalan_b,"+
//                            "penilaian_awal_keperawatan_ralan.berjalan_c,penilaian_awal_keperawatan_ralan.hasil,penilaian_awal_keperawatan_ralan.lapor,penilaian_awal_keperawatan_ralan.ket_lapor,penilaian_awal_keperawatan_ralan.sg1,penilaian_awal_keperawatan_ralan.nilai1,penilaian_awal_keperawatan_ralan.sg2,penilaian_awal_keperawatan_ralan.nilai2,"+
//                            "penilaian_awal_keperawatan_ralan.total_hasil,penilaian_awal_keperawatan_ralan.nyeri,penilaian_awal_keperawatan_ralan.provokes,penilaian_awal_keperawatan_ralan.ket_provokes,penilaian_awal_keperawatan_ralan.quality,penilaian_awal_keperawatan_ralan.ket_quality,penilaian_awal_keperawatan_ralan.lokasi,penilaian_awal_keperawatan_ralan.menyebar,"+
//                            "penilaian_awal_keperawatan_ralan.skala_nyeri,penilaian_awal_keperawatan_ralan.durasi,penilaian_awal_keperawatan_ralan.nyeri_hilang,penilaian_awal_keperawatan_ralan.ket_nyeri,penilaian_awal_keperawatan_ralan.pada_dokter,penilaian_awal_keperawatan_ralan.ket_dokter,penilaian_awal_keperawatan_ralan.rencana,"+
//                            "penilaian_awal_keperawatan_ralan.nip,petugas.nama,penilaian_awal_keperawatan_ralan.budaya,penilaian_awal_keperawatan_ralan.ket_budaya "+
//                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                            "inner join penilaian_awal_keperawatan_ralan on reg_periksa.no_rawat=penilaian_awal_keperawatan_ralan.no_rawat "+
//                            "inner join petugas on penilaian_awal_keperawatan_ralan.nip=petugas.nip "+
//                            "inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
//                            "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik where "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? order by penilaian_awal_keperawatan_ralan.tanggal");
//                }else{
//                    ps=koneksi.prepareStatement(
//                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat,penilaian_awal_keperawatan_ralan.tanggal,"+
//                            "penilaian_awal_keperawatan_ralan.informasi,penilaian_awal_keperawatan_ralan.td,penilaian_awal_keperawatan_ralan.nadi,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,"+
//                            "penilaian_awal_keperawatan_ralan.spo2,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.gcs,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,penilaian_awal_keperawatan_ralan.bmi,penilaian_awal_keperawatan_ralan.keluhan_utama,"+    //CUSTOM MUHSIN -> NADI DOUBLE, GANTI 1 JADI SPO2
//                            "penilaian_awal_keperawatan_ralan.rpd,penilaian_awal_keperawatan_ralan.rpk,penilaian_awal_keperawatan_ralan.rpo,penilaian_awal_keperawatan_ralan.alergi,penilaian_awal_keperawatan_ralan.alat_bantu,penilaian_awal_keperawatan_ralan.ket_bantu,penilaian_awal_keperawatan_ralan.prothesa,"+
//                            "penilaian_awal_keperawatan_ralan.ket_pro,penilaian_awal_keperawatan_ralan.adl,penilaian_awal_keperawatan_ralan.status_psiko,penilaian_awal_keperawatan_ralan.ket_psiko,penilaian_awal_keperawatan_ralan.hub_keluarga,penilaian_awal_keperawatan_ralan.tinggal_dengan,"+
//                            "penilaian_awal_keperawatan_ralan.ket_tinggal,penilaian_awal_keperawatan_ralan.ekonomi,penilaian_awal_keperawatan_ralan.edukasi,penilaian_awal_keperawatan_ralan.ket_edukasi,penilaian_awal_keperawatan_ralan.berjalan_a,penilaian_awal_keperawatan_ralan.berjalan_b,"+
//                            "penilaian_awal_keperawatan_ralan.berjalan_c,penilaian_awal_keperawatan_ralan.hasil,penilaian_awal_keperawatan_ralan.lapor,penilaian_awal_keperawatan_ralan.ket_lapor,penilaian_awal_keperawatan_ralan.sg1,penilaian_awal_keperawatan_ralan.nilai1,penilaian_awal_keperawatan_ralan.sg2,penilaian_awal_keperawatan_ralan.nilai2,"+
//                            "penilaian_awal_keperawatan_ralan.total_hasil,penilaian_awal_keperawatan_ralan.nyeri,penilaian_awal_keperawatan_ralan.provokes,penilaian_awal_keperawatan_ralan.ket_provokes,penilaian_awal_keperawatan_ralan.quality,penilaian_awal_keperawatan_ralan.ket_quality,penilaian_awal_keperawatan_ralan.lokasi,penilaian_awal_keperawatan_ralan.menyebar,"+
//                            "penilaian_awal_keperawatan_ralan.skala_nyeri,penilaian_awal_keperawatan_ralan.durasi,penilaian_awal_keperawatan_ralan.nyeri_hilang,penilaian_awal_keperawatan_ralan.ket_nyeri,penilaian_awal_keperawatan_ralan.pada_dokter,penilaian_awal_keperawatan_ralan.ket_dokter,penilaian_awal_keperawatan_ralan.rencana,"+
//                            "penilaian_awal_keperawatan_ralan.nip,petugas.nama,penilaian_awal_keperawatan_ralan.budaya,penilaian_awal_keperawatan_ralan.ket_budaya "+
//                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                            "inner join penilaian_awal_keperawatan_ralan on reg_periksa.no_rawat=penilaian_awal_keperawatan_ralan.no_rawat "+
//                            "inner join petugas on penilaian_awal_keperawatan_ralan.nip=petugas.nip "+
//                            "inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
//                            "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik where "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and reg_periksa.no_rawat like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and pasien.no_rkm_medis like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and pasien.nm_pasien like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and penilaian_awal_keperawatan_ralan.nip like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and petugas.nama like ? order by penilaian_awal_keperawatan_ralan.tanggal");
//                }
//
//                try {
//                    if(TCari.getText().equals("")){
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                    }else{
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(3,"%"+TCari.getText()+"%");
//                        ps.setString(4,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(5,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(6,"%"+TCari.getText()+"%");
//                        ps.setString(7,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(8,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(9,"%"+TCari.getText()+"%");
//                        ps.setString(10,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(11,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(12,"%"+TCari.getText()+"%");
//                        ps.setString(13,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(14,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(15,"%"+TCari.getText()+"%");
//                    }   
//                    rs=ps.executeQuery();
//                    htmlContent = new StringBuilder();
//                    htmlContent.append(                             
//                        "<tr class='isi'>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='9%'><b>PASIEN & PETUGAS</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='5%'><b>I. KEADAAN UMUM</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='5%'><b>II. STATUS NUTRISI</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='13%'><b>III. RIWAYAT KESEHATAN</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='8%'><b>IV. FUNGSIONAL</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='16%'><b>V. RIWAYAT PSIKO-SOSIAL SPIRITUAL DAN BUDAYA</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='16%'><b>VI. PENILAIAN RESIKO JATUH</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='11%'><b>VII. SKRINING GIZI</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='11%'><b>VIII. PENILAIAN TINGKAT NYERI</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='6%'><b>MASALAH & RENCANA KEPERAWATAN</b></td>"+
//                        "</tr>"
//                    );
//                    while(rs.next()){
//                        masalahkeperawatan="";
//                        ps2=koneksi.prepareStatement(
//                            "select master_masalah_keperawatan.kode_masalah,master_masalah_keperawatan.nama_masalah from master_masalah_keperawatan "+
//                            "inner join penilaian_awal_keperawatan_ralan_masalah on penilaian_awal_keperawatan_ralan_masalah.kode_masalah=master_masalah_keperawatan.kode_masalah "+
//                            "where penilaian_awal_keperawatan_ralan_masalah.no_rawat=? order by kode_masalah");
//                        try {
//                            ps2.setString(1,rs.getString("no_rawat"));
//                            rs2=ps2.executeQuery();
//                            while(rs2.next()){
//                                masalahkeperawatan=rs2.getString("nama_masalah")+", "+masalahkeperawatan;
//                            }
//                        } catch (Exception e) {
//                            System.out.println("Notif : "+e);
//                        } finally{
//                            if(rs2!=null){
//                                rs2.close();
//                            }
//                            if(ps2!=null){
//                                ps2.close();
//                            }
//                        }
//                        htmlContent.append(
//                            "<tr class='isi'>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>No.Rawat</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("no_rawat")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>No.R.M.</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("no_rkm_medis")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Nama Pasien</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nm_pasien")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>J.K.</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("jk")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Agama</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("agama")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Bahasa</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nama_bahasa")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Tgl.Lahir</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nama_cacat")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Cacat Fisik</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("tgl_lahir")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Petugas</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nip")+" "+rs.getString("nama")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Tgl.Asuhan</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("tanggal")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Informasi</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("informasi")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>TD</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("td")+"mmHg</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>Nadi</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("nadi")+"x/menit</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>RR</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("rr")+"x/menit</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>Suhu</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("suhu")+"°C</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>GCS</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("gcs")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>BB</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("bb")+"Kg</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>TB</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("tb")+"cm</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>BMI</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("bmi")+"Kg/m²</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Keluhan Utama</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("keluhan_utama")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>RPD</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("rpd")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>RPK</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("rpk")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>RPO</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("rpo")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Alergi</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("alergi")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Alat Bantu</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("alat_bantu")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Alat Bantu</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_bantu")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Prothesa</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("prothesa")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Prothesa</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_pro")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>ADL</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("adl")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Status Psikologis</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("status_psiko")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Psikologi</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_psiko")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Hubungan pasien dengan anggota keluarga</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("hub_keluarga")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Tinggal dengan</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("tinggal_dengan")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Tinggal</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_tinggal")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ekonomi</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ekonomi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Kepercayaan / Budaya / Nilai-nilai khusus yang perlu diperhatikan</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("budaya")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Budaya</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_budaya")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Edukasi diberikan kepada </td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("edukasi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Edukasi</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_edukasi")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Tidak seimbang/sempoyongan/limbung</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("berjalan_a")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Jalan dengan menggunakan alat bantu (kruk, tripot, kursi roda, orang lain)</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("berjalan_b")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Menopang saat akan duduk, tampak memegang pinggiran kursi atau meja/benda lain sebagai penopang</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("berjalan_c")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Hasil</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("hasil")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Dilaporan ke dokter?</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("lapor")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Jam Lapor</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_lapor")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Apakah ada penurunan berat badanyang tidak diinginkan selama enam bulan terakhir?</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("sg1")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Apakah nafsu makan berkurang karena tidak nafsu makan?</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("sg2")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Nilai 1</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("nilai1")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Nilai 2</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("nilai2")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Total Skor</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("total_hasil")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Tingkat Nyeri</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("nyeri")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Provokes</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("provokes")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Provokes</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_provokes")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Kualitas</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("quality")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Kualitas</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_quality")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Lokas</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("lokasi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Menyebar</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("menyebar")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Skala Nyeri</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("skala_nyeri")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Durasi</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("durasi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Nyeri Hilang</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("nyeri_hilang")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Hilang Nyeri</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_nyeri")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Lapor Ke Dokter</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("pada_dokter")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Jam Lapor</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_dokter")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "Masalah Keperawatan : "+masalahkeperawatan+"<br><br>"+
//                                    "Rencana Keperawatan : "+rs.getString("rencana")+
//                                "</td>"+
//                            "</tr>"
//                        );
//                    }
//                    LoadHTML.setText(
//                        "<html>"+
//                          "<table width='1800px' border='0' align='center' cellpadding='1px' cellspacing='0' class='tbl_form'>"+
//                           htmlContent.toString()+
//                          "</table>"+
//                        "</html>"
//                    );
//
//                    File g = new File("file2.css");            
//                    BufferedWriter bg = new BufferedWriter(new FileWriter(g));
//                    bg.write(
//                        ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
//                        ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
//                        ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
//                        ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
//                        ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
//                        ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
//                    );
//                    bg.close();
//
//                    File f = new File("DataPenilaianAwalKeperawatanRalan.html");            
//                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));            
//                    bw.write(LoadHTML.getText().replaceAll("<head>","<head>"+
//                                "<link href=\"file2.css\" rel=\"stylesheet\" type=\"text/css\" />"+
//                                "<table width='1800px' border='0' align='center' cellpadding='3px' cellspacing='0' class='tbl_form'>"+
//                                    "<tr class='isi2'>"+
//                                        "<td valign='top' align='center'>"+
//                                            "<font size='4' face='Tahoma'>"+akses.getnamars()+"</font><br>"+
//                                            akses.getalamatrs()+", "+akses.getkabupatenrs()+", "+akses.getpropinsirs()+"<br>"+
//                                            akses.getkontakrs()+", E-mail : "+akses.getemailrs()+"<br><br>"+
//                                            "<font size='2' face='Tahoma'>DATA PENILAIAN AWAL KEPERAWATAN RAWAT JALAN<br><br></font>"+        
//                                        "</td>"+
//                                   "</tr>"+
//                                "</table>")
//                    );
//                    bw.close();                         
//                    Desktop.getDesktop().browse(f.toURI());
//                } catch (Exception e) {
//                    System.out.println("Notif : "+e);
//                } finally{
//                    if(rs!=null){
//                        rs.close();
//                    }
//                    if(ps!=null){
//                        ps.close();
//                    }
//                }
//
//            }catch(Exception e){
//                System.out.println("Notifikasi btnprint: "+e);
//            }
//        }
//        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if(evt.getClickCount()==2){
                getData();
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        try {
            if(Valid.daysOld("./cache/hambatanbelajar.iyem")<8){
                tampilHambatan2();
            }else{
                tampilHambatan();
            }
        } catch (Exception e) {
        }
        try {
            if(Valid.daysOld("./cache/kebutuhanedukasi.iyem")<8){
                tampilKebutuhan2();
            }else{
                tampilKebutuhan();
            }
        } catch (Exception e) {
        }
        try {
            if(Valid.daysOld("./cache/edukasidokter.iyem")<8){
                tampilEdukasiDokter2();
            }else{
                tampilEdukasiDokter();
            }
        } catch (Exception e) {
        }
        try {
            if(Valid.daysOld("./cache/edukasiperawat.iyem")<8){
                tampilEdukasiPerawat2();
            }else{
                tampilEdukasiPerawat();
            }
        } catch (Exception e) {
        }
        try {
            if(Valid.daysOld("./cache/edukasigizi.iyem")<8){
                tampilEdukasiGizi2();
            }else{
                tampilEdukasiGizi();
            }
        } catch (Exception e) {
        }
        try {
            if(Valid.daysOld("./cache/edukasifarmasi.iyem")<8){
                tampilEdukasiFarmasi2();
            }else{
                tampilEdukasiFarmasi();
            }
        } catch (Exception e) {
        }        
        try {
            if(Valid.daysOld("./cache/edukasinyeri.iyem")<8){
                tampilEdukasiNyeri2();
            }else{
                tampilEdukasiNyeri();
            }
        } catch (Exception e) {
        }        
        try {
            if(Valid.daysOld("./cache/edukasilainnya.iyem")<8){
                tampilEdukasiLainnya2();
            }else{
                tampilEdukasiLainnya();
            }
        } catch (Exception e) {
        }        
    }//GEN-LAST:event_formWindowOpened

    private void HambatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HambatanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HambatanKeyPressed

    private void HambatanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HambatanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_HambatanActionPerformed

    private void PendidikanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PendidikanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PendidikanKeyPressed

    private void PendidikanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PendidikanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PendidikanActionPerformed

    private void CaraBelajarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CaraBelajarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CaraBelajarKeyPressed

    private void CaraBelajarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CaraBelajarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CaraBelajarActionPerformed

    private void BahasaIsyaratKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BahasaIsyaratKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BahasaIsyaratKeyPressed

    private void BahasaIsyaratActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BahasaIsyaratActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BahasaIsyaratActionPerformed

    private void PenerjemahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PenerjemahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PenerjemahKeyPressed

    private void PenerjemahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PenerjemahActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PenerjemahActionPerformed

    private void BahasaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BahasaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BahasaKeyPressed

    private void TglAsuhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglAsuhanKeyPressed
        //        Valid.pindah(evt,KetWaktuTunggu,Informasi);
    }//GEN-LAST:event_TglAsuhanKeyPressed

    private void HakPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HakPEKeyPressed
        //        Valid.pindah(evt,StatusPsiko,HubunganKeluarga);
    }//GEN-LAST:event_HakPEKeyPressed

    private void HakPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HakPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_HakPEActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        //        Valid.pindah(evt,KetWaktuTunggu,Informasi);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed

    }//GEN-LAST:event_KdPetugasKeyPressed

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{
            Valid.pindah(evt,TCari,BtnDokter);
        }
    }//GEN-LAST:event_TNoRwKeyPressed

    private void OrientasiPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OrientasiPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_OrientasiPEActionPerformed

    private void OrientasiPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_OrientasiPEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_OrientasiPEKeyPressed

    private void DiagnosaPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DiagnosaPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiagnosaPEActionPerformed

    private void DiagnosaPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DiagnosaPEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiagnosaPEKeyPressed

    private void ObatPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ObatPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatPEActionPerformed

    private void ObatPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ObatPEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatPEKeyPressed

    private void AlatPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AlatPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlatPEActionPerformed

    private void AlatPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AlatPEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlatPEKeyPressed

    private void DietPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DietPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DietPEActionPerformed

    private void DietPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DietPEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DietPEKeyPressed

    private void RehabPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RehabPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RehabPEActionPerformed

    private void RehabPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RehabPEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RehabPEKeyPressed

    private void NyeriPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NyeriPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NyeriPEActionPerformed

    private void NyeriPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NyeriPEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_NyeriPEKeyPressed

    private void InfeksiPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InfeksiPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_InfeksiPEActionPerformed

    private void InfeksiPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_InfeksiPEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_InfeksiPEKeyPressed

    private void RawatPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RawatPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RawatPEActionPerformed

    private void RawatPEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RawatPEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RawatPEKeyPressed

    private void DurasiDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DurasiDokterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiDokterActionPerformed

    private void DurasiDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DurasiDokterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiDokterKeyPressed

    private void TglDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglDokterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglDokterKeyPressed

    private void EvaluasiDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EvaluasiDokterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_EvaluasiDokterKeyPressed

    private void TglPerawatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglPerawatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglPerawatKeyPressed

    private void DurasiPerawatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DurasiPerawatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiPerawatActionPerformed

    private void DurasiPerawatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DurasiPerawatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiPerawatKeyPressed

    private void EvaluasiPerawatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EvaluasiPerawatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_EvaluasiPerawatKeyPressed

    private void TglGiziKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglGiziKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglGiziKeyPressed

    private void DurasiGiziActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DurasiGiziActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiGiziActionPerformed

    private void DurasiGiziKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DurasiGiziKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiGiziKeyPressed

    private void EvaluasiGiziKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EvaluasiGiziKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_EvaluasiGiziKeyPressed

    private void DurasiFarmasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DurasiFarmasiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiFarmasiActionPerformed

    private void DurasiFarmasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DurasiFarmasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiFarmasiKeyPressed

    private void TglFarmasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglFarmasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglFarmasiKeyPressed

    private void EvaluasiFarmasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EvaluasiFarmasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_EvaluasiFarmasiKeyPressed

    private void TglNyeriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglNyeriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglNyeriKeyPressed

    private void DurasiNyeriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DurasiNyeriActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiNyeriActionPerformed

    private void DurasiNyeriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DurasiNyeriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiNyeriKeyPressed

    private void EvaluasiNyeriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EvaluasiNyeriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_EvaluasiNyeriKeyPressed

    private void tbHambatanBelajarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbHambatanBelajarMouseClicked
//        if(tabModeHambatan.getRowCount()!=0){
//            try {
//                tampilHambatan2();
//            } catch (java.lang.NullPointerException e) {
//            }
//        }
    }//GEN-LAST:event_tbHambatanBelajarMouseClicked

    private void tbHambatanBelajarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbHambatanBelajarKeyPressed
//        if(tabModeHambatan.getRowCount()!=0){
//            if(evt.getKeyCode()==KeyEvent.VK_SHIFT){
//                TCariHambatan.setText("");
//                TCariHambatan.requestFocus();
//            }
//        }
    }//GEN-LAST:event_tbHambatanBelajarKeyPressed

    private void tbHambatanBelajarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbHambatanBelajarKeyReleased
//        if(tabModeHambatan.getRowCount()!=0){
//            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
//                try {
//                    tampilHambatan2();
//                } catch (java.lang.NullPointerException e) {
//                }
//            }
//        }
    }//GEN-LAST:event_tbHambatanBelajarKeyReleased

    private void tbKebutuhanEdukasiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbKebutuhanEdukasiMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbKebutuhanEdukasiMouseClicked

    private void tbKebutuhanEdukasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbKebutuhanEdukasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbKebutuhanEdukasiKeyPressed

    private void tbKebutuhanEdukasiKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbKebutuhanEdukasiKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tbKebutuhanEdukasiKeyReleased

    private void TCariHambatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariHambatanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilHambatan2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            Hambatan.requestFocus();
        }
    }//GEN-LAST:event_TCariHambatanKeyPressed

    private void BtnCariHambatanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariHambatanActionPerformed
        tampilHambatan2();
    }//GEN-LAST:event_BtnCariHambatanActionPerformed

    private void BtnCariHambatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariHambatanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilHambatan2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            Hambatan.requestFocus();
        }
    }//GEN-LAST:event_BtnCariHambatanKeyPressed

    private void BtnAllHambatanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllHambatanActionPerformed
        TCariHambatan.setText("");
        tampilHambatan();
    }//GEN-LAST:event_BtnAllHambatanActionPerformed

    private void BtnAllHambatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllHambatanKeyPressed
//        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
//            BtnAllMasalahActionPerformed(null);
//        }else{
//            Valid.pindah(evt, BtnCariHambatan, TCariHambatan);
//        }
    }//GEN-LAST:event_BtnAllHambatanKeyPressed

    private void BtnTambahHambatanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahHambatanActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        MasterHambatanBelajar form=new MasterHambatanBelajar(null,false);
        form.isCek();
        form.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        form.setLocationRelativeTo(internalFrame1);
        form.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_BtnTambahHambatanActionPerformed

    private void TCariKebutuhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKebutuhanKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilKebutuhan2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
//            TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            TCariHambatan.requestFocus();
        }        
    }//GEN-LAST:event_TCariKebutuhanKeyPressed

    private void BtnCariKebutuhanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariKebutuhanActionPerformed
        // TODO add your handling code here:
        tampilKebutuhan2();
    }//GEN-LAST:event_BtnCariKebutuhanActionPerformed

    private void BtnCariKebutuhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKebutuhanKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilKebutuhan2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
//            TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            TCariHambatan.requestFocus();
        }        
    }//GEN-LAST:event_BtnCariKebutuhanKeyPressed

    private void BtnAllKebutuhanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllKebutuhanActionPerformed
        // TODO add your handling code here:
        TCariKebutuhan.setText("");
        tampilKebutuhan();
    }//GEN-LAST:event_BtnAllKebutuhanActionPerformed

    private void BtnAllKebutuhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKebutuhanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAllKebutuhanKeyPressed

    private void BtnTambahKebutuhanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahKebutuhanActionPerformed
        // TODO add your handling code here:
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        MasterKebutuhanEdukasi form=new MasterKebutuhanEdukasi(null,false);
        form.isCek();
        form.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        form.setLocationRelativeTo(internalFrame1);
        form.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());        
    }//GEN-LAST:event_BtnTambahKebutuhanActionPerformed

    private void TglHakWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglHakWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglHakWPKeyPressed

    private void TglOrientasiWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglOrientasiWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglOrientasiWPKeyPressed

    private void TglDiagnosaWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglDiagnosaWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglDiagnosaWPKeyPressed

    private void TglObatWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglObatWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglObatWPKeyPressed

    private void TglAlatWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglAlatWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglAlatWPKeyPressed

    private void TglDietWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglDietWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglDietWPKeyPressed

    private void TglRehabWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglRehabWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglRehabWPKeyPressed

    private void TglNyeriWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglNyeriWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglNyeriWPKeyPressed

    private void TglInfeksiWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglInfeksiWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglInfeksiWPKeyPressed

    private void TglRawatWPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglRawatWPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglRawatWPKeyPressed

    private void HakSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HakSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_HakSActionPerformed

    private void HakSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HakSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HakSKeyPressed

    private void OrientasiSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OrientasiSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_OrientasiSActionPerformed

    private void OrientasiSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_OrientasiSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_OrientasiSKeyPressed

    private void DiagnosaSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DiagnosaSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiagnosaSActionPerformed

    private void DiagnosaSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DiagnosaSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiagnosaSKeyPressed

    private void ObatSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ObatSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatSActionPerformed

    private void ObatSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ObatSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatSKeyPressed

    private void AlatSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AlatSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlatSActionPerformed

    private void AlatSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AlatSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlatSKeyPressed

    private void DietSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DietSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DietSActionPerformed

    private void DietSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DietSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DietSKeyPressed

    private void RehabSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RehabSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RehabSActionPerformed

    private void RehabSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RehabSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RehabSKeyPressed

    private void NyeriSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NyeriSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NyeriSActionPerformed

    private void NyeriSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NyeriSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_NyeriSKeyPressed

    private void InfeksiSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InfeksiSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_InfeksiSActionPerformed

    private void InfeksiSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_InfeksiSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_InfeksiSKeyPressed

    private void RawatSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RawatSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RawatSActionPerformed

    private void RawatSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RawatSKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RawatSKeyPressed

    private void HakCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HakCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_HakCEActionPerformed

    private void HakCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HakCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HakCEKeyPressed

    private void OrientasiCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OrientasiCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_OrientasiCEActionPerformed

    private void OrientasiCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_OrientasiCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_OrientasiCEKeyPressed

    private void DiagnosaCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DiagnosaCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiagnosaCEActionPerformed

    private void DiagnosaCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DiagnosaCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiagnosaCEKeyPressed

    private void ObatCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ObatCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatCEActionPerformed

    private void ObatCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ObatCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatCEKeyPressed

    private void AlatCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AlatCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlatCEActionPerformed

    private void AlatCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AlatCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlatCEKeyPressed

    private void DietCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DietCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DietCEActionPerformed

    private void DietCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DietCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DietCEKeyPressed

    private void RehabCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RehabCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RehabCEActionPerformed

    private void RehabCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RehabCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RehabCEKeyPressed

    private void NyeriCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NyeriCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NyeriCEActionPerformed

    private void NyeriCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NyeriCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_NyeriCEKeyPressed

    private void InfeksiCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InfeksiCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_InfeksiCEActionPerformed

    private void InfeksiCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_InfeksiCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_InfeksiCEKeyPressed

    private void RawatCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RawatCEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RawatCEActionPerformed

    private void RawatCEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RawatCEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RawatCEKeyPressed

    private void HakMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HakMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_HakMEActionPerformed

    private void HakMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HakMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HakMEKeyPressed

    private void OrientasiMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OrientasiMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_OrientasiMEActionPerformed

    private void OrientasiMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_OrientasiMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_OrientasiMEKeyPressed

    private void DiagnosaMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DiagnosaMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiagnosaMEActionPerformed

    private void DiagnosaMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DiagnosaMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiagnosaMEKeyPressed

    private void ObatMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ObatMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatMEActionPerformed

    private void ObatMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ObatMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatMEKeyPressed

    private void AlatMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AlatMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlatMEActionPerformed

    private void AlatMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AlatMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlatMEKeyPressed

    private void DietMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DietMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DietMEActionPerformed

    private void DietMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DietMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DietMEKeyPressed

    private void RehabMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RehabMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RehabMEActionPerformed

    private void RehabMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RehabMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RehabMEKeyPressed

    private void NyeriMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NyeriMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NyeriMEActionPerformed

    private void NyeriMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NyeriMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_NyeriMEKeyPressed

    private void InfeksiMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InfeksiMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_InfeksiMEActionPerformed

    private void InfeksiMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_InfeksiMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_InfeksiMEKeyPressed

    private void RawatMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RawatMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RawatMEActionPerformed

    private void RawatMEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RawatMEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RawatMEKeyPressed

    private void TglLainnyaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglLainnyaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglLainnyaKeyPressed

    private void DurasiLainnyaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DurasiLainnyaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiLainnyaActionPerformed

    private void DurasiLainnyaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DurasiLainnyaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DurasiLainnyaKeyPressed

    private void EvaluasiLainnyaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EvaluasiLainnyaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_EvaluasiLainnyaKeyPressed

    private void tbEdukasiDokterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbEdukasiDokterMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiDokterMouseClicked

    private void tbEdukasiDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiDokterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiDokterKeyPressed

    private void tbEdukasiDokterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiDokterKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiDokterKeyReleased

    private void TCariEdukasiDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariEdukasiDokterKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiDokter2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }        
    }//GEN-LAST:event_TCariEdukasiDokterKeyPressed

    private void BtnCariEdukasiDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariEdukasiDokterActionPerformed
        // TODO add your handling code here:
        tampilEdukasiDokter2();
    }//GEN-LAST:event_BtnCariEdukasiDokterActionPerformed

    private void BtnCariEdukasiDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariEdukasiDokterKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiDokter2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }
    }//GEN-LAST:event_BtnCariEdukasiDokterKeyPressed

    private void BtnAllEdukasiDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllEdukasiDokterActionPerformed
        // TODO add your handling code here:
        TCariEdukasiDokter.setText("");
        tampilEdukasiDokter();
    }//GEN-LAST:event_BtnAllEdukasiDokterActionPerformed

    private void BtnAllEdukasiDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllEdukasiDokterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAllEdukasiDokterKeyPressed

    private void BtnTambahEdukasiDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahEdukasiDokterActionPerformed
        // TODO add your handling code here:
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        MasterEdukasiDokter form=new MasterEdukasiDokter(null,false);
        form.isCek();
        form.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        form.setLocationRelativeTo(internalFrame1);
        form.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());        
    }//GEN-LAST:event_BtnTambahEdukasiDokterActionPerformed

    private void tbEdukasiPerawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbEdukasiPerawatMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiPerawatMouseClicked

    private void tbEdukasiPerawatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiPerawatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiPerawatKeyPressed

    private void tbEdukasiPerawatKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiPerawatKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiPerawatKeyReleased

    private void TCariEdukasiPerawatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariEdukasiPerawatKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiPerawat2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }         
    }//GEN-LAST:event_TCariEdukasiPerawatKeyPressed

    private void BtnCariEdukasiPerawatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariEdukasiPerawatActionPerformed
        // TODO add your handling code here:
        tampilEdukasiPerawat2();
    }//GEN-LAST:event_BtnCariEdukasiPerawatActionPerformed

    private void BtnCariEdukasiPerawatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariEdukasiPerawatKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiPerawat2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }        
    }//GEN-LAST:event_BtnCariEdukasiPerawatKeyPressed

    private void BtnAllEdukasiPerawatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllEdukasiPerawatActionPerformed
        // TODO add your handling code here:
        TCariEdukasiPerawat.setText("");
        tampilEdukasiPerawat();        
    }//GEN-LAST:event_BtnAllEdukasiPerawatActionPerformed

    private void BtnAllEdukasiPerawatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllEdukasiPerawatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAllEdukasiPerawatKeyPressed

    private void BtnTambahEdukasiPerawatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahEdukasiPerawatActionPerformed
        // TODO add your handling code here:
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        MasterEdukasiPerawat form=new MasterEdukasiPerawat(null,false);
        form.isCek();
        form.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        form.setLocationRelativeTo(internalFrame1);
        form.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());          
    }//GEN-LAST:event_BtnTambahEdukasiPerawatActionPerformed

    private void tbEdukasiGiziMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbEdukasiGiziMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiGiziMouseClicked

    private void tbEdukasiGiziKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiGiziKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiGiziKeyPressed

    private void tbEdukasiGiziKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiGiziKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiGiziKeyReleased

    private void TCariEdukasiGiziKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariEdukasiGiziKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiGizi2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }         
    }//GEN-LAST:event_TCariEdukasiGiziKeyPressed

    private void BtnCariEdukasiGiziActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariEdukasiGiziActionPerformed
        // TODO add your handling code here:
        tampilEdukasiGizi2();
    }//GEN-LAST:event_BtnCariEdukasiGiziActionPerformed

    private void BtnCariEdukasiGiziKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariEdukasiGiziKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiGizi2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }        
    }//GEN-LAST:event_BtnCariEdukasiGiziKeyPressed

    private void BtnAllEdukasiGiziActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllEdukasiGiziActionPerformed
        // TODO add your handling code here:
        TCariEdukasiGizi.setText("");
        tampilEdukasiGizi();         
    }//GEN-LAST:event_BtnAllEdukasiGiziActionPerformed

    private void BtnAllEdukasiGiziKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllEdukasiGiziKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAllEdukasiGiziKeyPressed

    private void BtnTambahEdukasiGiziActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahEdukasiGiziActionPerformed
        // TODO add your handling code here:
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        MasterEdukasiGizi form=new MasterEdukasiGizi(null,false);
        form.isCek();
        form.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        form.setLocationRelativeTo(internalFrame1);
        form.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());          
    }//GEN-LAST:event_BtnTambahEdukasiGiziActionPerformed

    private void tbEdukasiFarmasiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbEdukasiFarmasiMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiFarmasiMouseClicked

    private void tbEdukasiFarmasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiFarmasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiFarmasiKeyPressed

    private void tbEdukasiFarmasiKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiFarmasiKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiFarmasiKeyReleased

    private void TCariEdukasiFarmasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariEdukasiFarmasiKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiFarmasi2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }         
    }//GEN-LAST:event_TCariEdukasiFarmasiKeyPressed

    private void BtnCariEdukasiFarmasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariEdukasiFarmasiActionPerformed
        // TODO add your handling code here:
        tampilEdukasiFarmasi2();        
    }//GEN-LAST:event_BtnCariEdukasiFarmasiActionPerformed

    private void BtnCariEdukasiFarmasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariEdukasiFarmasiKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiFarmasi2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }        
    }//GEN-LAST:event_BtnCariEdukasiFarmasiKeyPressed

    private void BtnAllEdukasiFarmasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllEdukasiFarmasiActionPerformed
        // TODO add your handling code here:
        TCariEdukasiFarmasi.setText("");
        tampilEdukasiFarmasi();         
    }//GEN-LAST:event_BtnAllEdukasiFarmasiActionPerformed

    private void BtnAllEdukasiFarmasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllEdukasiFarmasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAllEdukasiFarmasiKeyPressed

    private void BtnTambahEdukasiFarmasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahEdukasiFarmasiActionPerformed
        // TODO add your handling code here:
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        MasterEdukasiFarmasi form=new MasterEdukasiFarmasi(null,false);
        form.isCek();
        form.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        form.setLocationRelativeTo(internalFrame1);
        form.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());          
    }//GEN-LAST:event_BtnTambahEdukasiFarmasiActionPerformed

    private void tbEdukasiNyeriMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbEdukasiNyeriMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiNyeriMouseClicked

    private void tbEdukasiNyeriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiNyeriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiNyeriKeyPressed

    private void tbEdukasiNyeriKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiNyeriKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiNyeriKeyReleased

    private void TCariEdukasiNyeriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariEdukasiNyeriKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiNyeri2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }         
    }//GEN-LAST:event_TCariEdukasiNyeriKeyPressed

    private void BtnCariEdukasiNyeriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariEdukasiNyeriActionPerformed
        // TODO add your handling code here:
        tampilEdukasiNyeri2();        
    }//GEN-LAST:event_BtnCariEdukasiNyeriActionPerformed

    private void BtnCariEdukasiNyeriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariEdukasiNyeriKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiNyeri2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }
    }//GEN-LAST:event_BtnCariEdukasiNyeriKeyPressed

    private void BtnAllEdukasiNyeriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllEdukasiNyeriActionPerformed
        // TODO add your handling code here:
        TCariEdukasiNyeri.setText("");
        tampilEdukasiNyeri();         
    }//GEN-LAST:event_BtnAllEdukasiNyeriActionPerformed

    private void BtnAllEdukasiNyeriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllEdukasiNyeriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAllEdukasiNyeriKeyPressed

    private void BtnTambahEdukasiNyeriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahEdukasiNyeriActionPerformed
        // TODO add your handling code here:
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        MasterEdukasiNyeri form=new MasterEdukasiNyeri(null,false);
        form.isCek();
        form.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        form.setLocationRelativeTo(internalFrame1);
        form.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());          
    }//GEN-LAST:event_BtnTambahEdukasiNyeriActionPerformed

    private void tbEdukasiLainnyaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbEdukasiLainnyaMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiLainnyaMouseClicked

    private void tbEdukasiLainnyaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiLainnyaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiLainnyaKeyPressed

    private void tbEdukasiLainnyaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbEdukasiLainnyaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tbEdukasiLainnyaKeyReleased

    private void TCariEdukasiLainnyaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariEdukasiLainnyaKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiLainnya2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }         
    }//GEN-LAST:event_TCariEdukasiLainnyaKeyPressed

    private void BtnCariEdukasiLainnyaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariEdukasiLainnyaActionPerformed
        // TODO add your handling code here:
        tampilEdukasiLainnya2();
    }//GEN-LAST:event_BtnCariEdukasiLainnyaActionPerformed

    private void BtnCariEdukasiLainnyaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariEdukasiLainnyaKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilEdukasiLainnya2();
        }else if((evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN)||(evt.getKeyCode()==KeyEvent.VK_TAB)){
            //TCariKebutuhan.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //Hambatan.requestFocus();
        }
    }//GEN-LAST:event_BtnCariEdukasiLainnyaKeyPressed

    private void BtnAllEdukasiLainnyaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllEdukasiLainnyaActionPerformed
        // TODO add your handling code here:
        TCariEdukasiLainnya.setText("");
        tampilEdukasiLainnya();         
    }//GEN-LAST:event_BtnAllEdukasiLainnyaActionPerformed

    private void BtnAllEdukasiLainnyaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllEdukasiLainnyaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAllEdukasiLainnyaKeyPressed

    private void BtnTambahEdukasiLainnyaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahEdukasiLainnyaActionPerformed
        // TODO add your handling code here:
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        MasterEdukasiLainnya form=new MasterEdukasiLainnya(null,false);
        form.isCek();
        form.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        form.setLocationRelativeTo(internalFrame1);
        form.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());          
    }//GEN-LAST:event_BtnTambahEdukasiLainnyaActionPerformed

    private void MnCetakEdukasiPasienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakEdukasiPasienActionPerformed
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),69).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),70).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),69).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),71).toString()));
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan")); 
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan")); 
            //HAMBATAN BELAJAR
            try {       
                hambatan_belajar="";
                rs = koneksi.prepareStatement("select master_hambatan_belajar.kode_hambatan,master_hambatan_belajar.nama_hambatan from master_hambatan_belajar "+
                        "inner join hambatan_belajar on hambatan_belajar.kode_hambatan=master_hambatan_belajar.kode_hambatan "+
                        "where hambatan_belajar.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by kode_hambatan").executeQuery();
                while(rs.next()) {
                    if(hambatan_belajar.equals("")){
                        hambatan_belajar=rs.getString("nama_hambatan");
                    }else{
                        hambatan_belajar=hambatan_belajar+";\n"+rs.getString("nama_hambatan");
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print hambatan belajar: " + e);
            }            
            param.put("hambatan_belajar",hambatan_belajar);
            //KEBUTUHAN EDUKASI
            try {       
                kebutuhan_edukasi="";
                rs = koneksi.prepareStatement("select master_kebutuhan_edukasi.kode_kebutuhan,master_kebutuhan_edukasi.nama_kebutuhan from master_kebutuhan_edukasi "+
                        "inner join kebutuhan_edukasi on kebutuhan_edukasi.kode_kebutuhan=master_kebutuhan_edukasi.kode_kebutuhan "+
                        "where kebutuhan_edukasi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by kode_kebutuhan").executeQuery();
                while(rs.next()) {
                    if(kebutuhan_edukasi.equals("")){
                        kebutuhan_edukasi=rs.getString("nama_kebutuhan");
                    }else{
                        kebutuhan_edukasi=kebutuhan_edukasi+";\n"+rs.getString("nama_kebutuhan");
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print kebutuhan edukasi: " + e);
            }            
            param.put("hambatan_edukasi",kebutuhan_edukasi);                 
            //EDUKASI DOKTER
            try {       
                edukasi_dokter="";
                rs = koneksi.prepareStatement("select master_edukasi_dokter.kode_edukasi,master_edukasi_dokter.nama_edukasi from master_edukasi_dokter "+
                        "inner join edukasi_dokter on edukasi_dokter.kode_edukasi=master_edukasi_dokter.kode_edukasi "+
                        "where edukasi_dokter.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by kode_edukasi").executeQuery();
                while(rs.next()) {
                    if(edukasi_dokter.equals("")){
                        edukasi_dokter=rs.getString("nama_edukasi");
                    }else{
                        edukasi_dokter=edukasi_dokter+";\n"+rs.getString("nama_edukasi");
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print edukasi dokter: " + e);
            }            
            param.put("edukasi_dokter",edukasi_dokter);    
            //EDUKASI PERAWAT    
            try {       
                edukasi_perawat="";
                rs = koneksi.prepareStatement("select master_edukasi_perawat.kode_edukasi,master_edukasi_perawat.nama_edukasi from master_edukasi_perawat "+
                        "inner join edukasi_perawat on edukasi_perawat.kode_edukasi=master_edukasi_perawat.kode_edukasi "+
                        "where edukasi_perawat.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by kode_edukasi").executeQuery();
                while(rs.next()) {
                    if(edukasi_perawat.equals("")){
                        edukasi_perawat=rs.getString("nama_edukasi");
                    }else{
                        edukasi_perawat=edukasi_perawat+";\n"+rs.getString("nama_edukasi");
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print edukasi perawat: " + e);
            }            
            param.put("edukasi_perawat",edukasi_perawat);            
            //EDUKASI GIZI
            try {       
                edukasi_gizi="";
                rs = koneksi.prepareStatement("select master_edukasi_gizi.kode_edukasi,master_edukasi_gizi.nama_edukasi from master_edukasi_gizi "+
                        "inner join edukasi_gizi on edukasi_gizi.kode_edukasi=master_edukasi_gizi.kode_edukasi "+
                        "where edukasi_gizi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by kode_edukasi").executeQuery();
                while(rs.next()) {
                    if(edukasi_gizi.equals("")){
                        edukasi_gizi=rs.getString("nama_edukasi");
                    }else{
                        edukasi_gizi=edukasi_gizi+";\n"+rs.getString("nama_edukasi");
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print edukasi gizi: " + e);
            }            
            param.put("edukasi_gizi",edukasi_gizi);
            //EDUKASI FARMASI
            try {       
                edukasi_farmasi="";
                rs = koneksi.prepareStatement("select master_edukasi_farmasi.kode_edukasi,master_edukasi_farmasi.nama_edukasi from master_edukasi_farmasi "+
                        "inner join edukasi_farmasi on edukasi_farmasi.kode_edukasi=master_edukasi_farmasi.kode_edukasi "+
                        "where edukasi_farmasi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by kode_edukasi").executeQuery();
                while(rs.next()) {
                    if(edukasi_farmasi.equals("")){
                        edukasi_farmasi=rs.getString("nama_edukasi");
                    }else{
                        edukasi_farmasi=edukasi_farmasi+";\n"+rs.getString("nama_edukasi");
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print edukasi farmasi: " + e);
            }            
            param.put("edukasi_farmasi",edukasi_farmasi);
            //EDUKASI NYERI 
            try {       
                edukasi_nyeri="";
                rs = koneksi.prepareStatement("select master_edukasi_nyeri.kode_edukasi,master_edukasi_nyeri.nama_edukasi from master_edukasi_nyeri "+
                        "inner join edukasi_nyeri on edukasi_nyeri.kode_edukasi=master_edukasi_nyeri.kode_edukasi "+
                        "where edukasi_nyeri.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by kode_edukasi").executeQuery();
                while(rs.next()) {
                    if(edukasi_nyeri.equals("")){
                        edukasi_nyeri=rs.getString("nama_edukasi");
                    }else{
                        edukasi_nyeri=edukasi_nyeri+";\n"+rs.getString("nama_edukasi");
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print edukasi nyeri: " + e);
            }            
            param.put("edukasi_nyeri",edukasi_nyeri);
            //EDUKASI LAINNYA
            try {       
                edukasi_lainnya="";
                rs = koneksi.prepareStatement("select master_edukasi_lainnya.kode_edukasi,master_edukasi_lainnya.nama_edukasi from master_edukasi_lainnya "+
                        "inner join edukasi_lainnya on edukasi_lainnya.kode_edukasi=master_edukasi_lainnya.kode_edukasi "+
                        "where edukasi_lainnya.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by kode_edukasi").executeQuery();
                while(rs.next()) {
                    if(edukasi_lainnya.equals("")){
                        edukasi_lainnya=rs.getString("nama_edukasi");
                    }else{
                        edukasi_lainnya=edukasi_lainnya+";\n"+rs.getString("nama_edukasi");
                    }
                }
            } catch (Exception e) {
                System.out.println("Notifikasi print edukasi lainnya: " + e);
            }            
            param.put("edukasi_lainnya",edukasi_lainnya);     
            
            Valid.MyReportqry("rptCetakEdukasiPasien.jasper","report","::[ Cetak Pengkajian & Edukasi Pasien ]::",
                "SELECT edukasi_pasien.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,IF( pasien.jk = 'L', 'Laki-Laki', 'Perempuan' ) AS jk,pasien.tgl_lahir,edukasi_pasien.tanggal," +
                "concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab,', ',propinsi.nm_prop) as alamat,"+
                "edukasi_pasien.bahasa,edukasi_pasien.penerjemah,edukasi_pasien.bahasa_isyarat,edukasi_pasien.cara_belajar,edukasi_pasien.tingkat_pendidikan,edukasi_pasien.hambatan_belajar," +
                "edukasi_pasien.tanggal_hak,edukasi_pasien.sasaran_hak,edukasi_pasien.cara_edukasi_hak,edukasi_pasien.metode_evaluasi_hak," +
                "edukasi_pasien.tanggal_orientasi,edukasi_pasien.sasaran_orientasi,edukasi_pasien.cara_edukasi_orientasi,edukasi_pasien.metode_evaluasi_orientasi," +
                "edukasi_pasien.tanggal_diagnosa,edukasi_pasien.sasaran_diagnosa,edukasi_pasien.cara_edukasi_diagnosa,edukasi_pasien.metode_evaluasi_diagnosa," +
                "edukasi_pasien.tanggal_obat,edukasi_pasien.sasaran_obat,edukasi_pasien.cara_edukasi_obat,edukasi_pasien.metode_evaluasi_obat,edukasi_pasien.tanggal_alat," +
                "edukasi_pasien.sasaran_alat,edukasi_pasien.cara_edukasi_alat,edukasi_pasien.metode_evaluasi_alat,edukasi_pasien.tanggal_diet,edukasi_pasien.sasaran_diet," +
                "edukasi_pasien.cara_edukasi_diet,edukasi_pasien.metode_evaluasi_diet,edukasi_pasien.tanggal_rehab,edukasi_pasien.sasaran_rehab,edukasi_pasien.cara_edukasi_rehab," +
                "edukasi_pasien.metode_evaluasi_rehab,edukasi_pasien.tanggal_nyeri,edukasi_pasien.sasaran_nyeri,edukasi_pasien.cara_edukasi_nyeri,edukasi_pasien.metode_evaluasi_nyeri," +
                "edukasi_pasien.tanggal_infeksi,edukasi_pasien.sasaran_infeksi,edukasi_pasien.cara_edukasi_infeksi,edukasi_pasien.metode_evaluasi_infeksi,edukasi_pasien.tanggal_rawat," +
                "edukasi_pasien.sasaran_rawat,edukasi_pasien.cara_edukasi_rawat,edukasi_pasien.metode_evaluasi_rawat,edukasi_pasien.tanggal_edukasi_dokter,edukasi_pasien.durasi_dokter," +
                "edukasi_pasien.evaluasi_dokter,edukasi_pasien.tanggal_edukasi_perawat,edukasi_pasien.durasi_perawat,edukasi_pasien.evaluasi_perawat,edukasi_pasien.tanggal_edukasi_gizi," +
                "edukasi_pasien.durasi_gizi,edukasi_pasien.evaluasi_gizi,edukasi_pasien.tanggal_edukasi_farmasi,edukasi_pasien.durasi_farmasi,edukasi_pasien.evaluasi_farmasi,"+ 
                "edukasi_pasien.tanggal_edukasi_nyeri,edukasi_pasien.durasi_nyeri,edukasi_pasien.evaluasi_nyeri,edukasi_pasien.tanggal_edukasi_lainnya,edukasi_pasien.durasi_lainnya," +
                "edukasi_pasien.evaluasi_lainnya,edukasi_pasien.nip,petugas.nama FROM reg_periksa " +
                "INNER JOIN pasien ON reg_periksa.no_rkm_medis = pasien.no_rkm_medis " +
                "INNER JOIN edukasi_pasien ON reg_periksa.no_rawat = edukasi_pasien.no_rawat " +
                "INNER JOIN petugas ON edukasi_pasien.nip = petugas.nip " +
                "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+
                "inner join propinsi on pasien.kd_prop=propinsi.kd_prop "+        
                "WHERE edukasi_pasien.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' "+        
                "ORDER BY edukasi_pasien.tanggal",param);
        }
    }//GEN-LAST:event_MnCetakEdukasiPasienActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMPenilaianAwalKeperawatanRalan dialog = new RMPenilaianAwalKeperawatanRalan(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.ComboBox AlatCE;
    private widget.ComboBox AlatME;
    private widget.TextBox AlatPE;
    private widget.ComboBox AlatS;
    private widget.TextBox Bahasa;
    private widget.ComboBox BahasaIsyarat;
    private widget.Button BtnAll;
    private widget.Button BtnAllEdukasiDokter;
    private widget.Button BtnAllEdukasiFarmasi;
    private widget.Button BtnAllEdukasiGizi;
    private widget.Button BtnAllEdukasiLainnya;
    private widget.Button BtnAllEdukasiNyeri;
    private widget.Button BtnAllEdukasiPerawat;
    private widget.Button BtnAllHambatan;
    private widget.Button BtnAllKebutuhan;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnCariEdukasiDokter;
    private widget.Button BtnCariEdukasiFarmasi;
    private widget.Button BtnCariEdukasiGizi;
    private widget.Button BtnCariEdukasiLainnya;
    private widget.Button BtnCariEdukasiNyeri;
    private widget.Button BtnCariEdukasiPerawat;
    private widget.Button BtnCariHambatan;
    private widget.Button BtnCariKebutuhan;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Button BtnTambahEdukasiDokter;
    private widget.Button BtnTambahEdukasiFarmasi;
    private widget.Button BtnTambahEdukasiGizi;
    private widget.Button BtnTambahEdukasiLainnya;
    private widget.Button BtnTambahEdukasiNyeri;
    private widget.Button BtnTambahEdukasiPerawat;
    private widget.Button BtnTambahHambatan;
    private widget.Button BtnTambahKebutuhan;
    private widget.ComboBox CaraBelajar;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox DiagnosaCE;
    private widget.ComboBox DiagnosaME;
    private widget.TextBox DiagnosaPE;
    private widget.ComboBox DiagnosaS;
    private widget.ComboBox DietCE;
    private widget.ComboBox DietME;
    private widget.TextBox DietPE;
    private widget.ComboBox DietS;
    private widget.TextBox DurasiDokter;
    private widget.TextBox DurasiFarmasi;
    private widget.TextBox DurasiGizi;
    private widget.TextBox DurasiLainnya;
    private widget.TextBox DurasiNyeri;
    private widget.TextBox DurasiPerawat;
    private widget.TextArea EvaluasiDokter;
    private widget.TextArea EvaluasiFarmasi;
    private widget.TextArea EvaluasiGizi;
    private widget.TextArea EvaluasiLainnya;
    private widget.TextArea EvaluasiNyeri;
    private widget.TextArea EvaluasiPerawat;
    private widget.PanelBiasa FormInput;
    private widget.ComboBox HakCE;
    private widget.ComboBox HakME;
    private widget.TextBox HakPE;
    private widget.ComboBox HakS;
    private widget.ComboBox Hambatan;
    private widget.ComboBox InfeksiCE;
    private widget.ComboBox InfeksiME;
    private widget.TextBox InfeksiPE;
    private widget.ComboBox InfeksiS;
    private widget.TextBox Jk;
    private widget.TextBox KdPetugas;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private javax.swing.JMenuItem MnCetakEdukasiPasien;
    private widget.TextBox NmPetugas;
    private widget.ComboBox NyeriCE;
    private widget.ComboBox NyeriME;
    private widget.TextBox NyeriPE;
    private widget.ComboBox NyeriS;
    private widget.ComboBox ObatCE;
    private widget.ComboBox ObatME;
    private widget.TextBox ObatPE;
    private widget.ComboBox ObatS;
    private widget.ComboBox OrientasiCE;
    private widget.ComboBox OrientasiME;
    private widget.TextBox OrientasiPE;
    private widget.ComboBox OrientasiS;
    private widget.ComboBox Pendidikan;
    private widget.ComboBox Penerjemah;
    private widget.ComboBox RawatCE;
    private widget.ComboBox RawatME;
    private widget.TextBox RawatPE;
    private widget.ComboBox RawatS;
    private widget.ComboBox RehabCE;
    private widget.ComboBox RehabME;
    private widget.TextBox RehabPE;
    private widget.ComboBox RehabS;
    private widget.ScrollPane Scroll;
    private widget.ScrollPane Scroll10;
    private widget.ScrollPane Scroll11;
    private widget.ScrollPane Scroll12;
    private widget.ScrollPane Scroll13;
    private widget.ScrollPane Scroll14;
    private widget.ScrollPane Scroll15;
    private widget.ScrollPane Scroll6;
    private widget.ScrollPane Scroll8;
    private widget.TextBox TCari;
    private widget.TextBox TCariEdukasiDokter;
    private widget.TextBox TCariEdukasiFarmasi;
    private widget.TextBox TCariEdukasiGizi;
    private widget.TextBox TCariEdukasiLainnya;
    private widget.TextBox TCariEdukasiNyeri;
    private widget.TextBox TCariEdukasiPerawat;
    private widget.TextBox TCariHambatan;
    private widget.TextBox TCariKebutuhan;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal TglAlatWP;
    private widget.Tanggal TglAsuhan;
    private widget.Tanggal TglDiagnosaWP;
    private widget.Tanggal TglDietWP;
    private widget.Tanggal TglDokter;
    private widget.Tanggal TglFarmasi;
    private widget.Tanggal TglGizi;
    private widget.Tanggal TglHakWP;
    private widget.Tanggal TglInfeksiWP;
    private widget.TextBox TglLahir;
    private widget.Tanggal TglLainnya;
    private widget.Tanggal TglNyeri;
    private widget.Tanggal TglNyeriWP;
    private widget.Tanggal TglObatWP;
    private widget.Tanggal TglOrientasiWP;
    private widget.Tanggal TglPerawat;
    private widget.Tanggal TglRawatWP;
    private widget.Tanggal TglRehabWP;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel11;
    private widget.Label jLabel12;
    private widget.Label jLabel13;
    private widget.Label jLabel14;
    private widget.Label jLabel15;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel30;
    private widget.Label jLabel37;
    private widget.Label jLabel53;
    private widget.Label jLabel55;
    private widget.Label jLabel56;
    private widget.Label jLabel57;
    private widget.Label jLabel58;
    private widget.Label jLabel59;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel62;
    private widget.Label jLabel63;
    private widget.Label jLabel64;
    private widget.Label jLabel65;
    private widget.Label jLabel66;
    private widget.Label jLabel67;
    private widget.Label jLabel68;
    private widget.Label jLabel69;
    private widget.Label jLabel7;
    private widget.Label jLabel70;
    private widget.Label jLabel71;
    private widget.Label jLabel72;
    private widget.Label jLabel73;
    private widget.Label jLabel74;
    private widget.Label jLabel78;
    private widget.Label jLabel79;
    private widget.Label jLabel8;
    private widget.Label jLabel80;
    private widget.Label jLabel81;
    private widget.Label jLabel82;
    private widget.Label jLabel83;
    private widget.Label jLabel84;
    private widget.Label jLabel85;
    private widget.Label jLabel86;
    private widget.Label jLabel87;
    private widget.Label jLabel88;
    private widget.Label jLabel89;
    private widget.Label jLabel90;
    private widget.Label jLabel91;
    private widget.Label jLabel93;
    private widget.Label jLabel98;
    private widget.Label jLabel99;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator9;
    private widget.Label label11;
    private widget.Label label12;
    private widget.Label label13;
    private widget.Label label14;
    private widget.Label label15;
    private widget.Label label16;
    private widget.Label label17;
    private widget.Label label18;
    private widget.Label label19;
    private widget.Label label20;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane11;
    private widget.ScrollPane scrollPane13;
    private widget.ScrollPane scrollPane15;
    private widget.ScrollPane scrollPane17;
    private widget.ScrollPane scrollPane7;
    private widget.ScrollPane scrollPane9;
    private widget.Table tbEdukasiDokter;
    private widget.Table tbEdukasiFarmasi;
    private widget.Table tbEdukasiGizi;
    private widget.Table tbEdukasiLainnya;
    private widget.Table tbEdukasiNyeri;
    private widget.Table tbEdukasiPerawat;
    private widget.Table tbHambatanBelajar;
    private widget.Table tbKebutuhanEdukasi;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    private void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().equals("")){
                ps=koneksi.prepareStatement(
                        "SELECT edukasi_pasien.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,IF( pasien.jk = 'L', 'Laki-Laki', 'Perempuan' ) AS jk,pasien.tgl_lahir,edukasi_pasien.tanggal," +
                        "edukasi_pasien.bahasa,edukasi_pasien.penerjemah,edukasi_pasien.bahasa_isyarat,edukasi_pasien.cara_belajar,edukasi_pasien.tingkat_pendidikan,edukasi_pasien.hambatan_belajar," +
                        "edukasi_pasien.tanggal_hak,edukasi_pasien.sasaran_hak,edukasi_pasien.cara_edukasi_hak,edukasi_pasien.metode_evaluasi_hak," +
                        "edukasi_pasien.tanggal_orientasi,edukasi_pasien.sasaran_orientasi,edukasi_pasien.cara_edukasi_orientasi,edukasi_pasien.metode_evaluasi_orientasi," +
                        "edukasi_pasien.tanggal_diagnosa,edukasi_pasien.sasaran_diagnosa,edukasi_pasien.cara_edukasi_diagnosa,edukasi_pasien.metode_evaluasi_diagnosa," +
                        "edukasi_pasien.tanggal_obat,edukasi_pasien.sasaran_obat,edukasi_pasien.cara_edukasi_obat,edukasi_pasien.metode_evaluasi_obat,edukasi_pasien.tanggal_alat," +
                        "edukasi_pasien.sasaran_alat,edukasi_pasien.cara_edukasi_alat,edukasi_pasien.metode_evaluasi_alat,edukasi_pasien.tanggal_diet,edukasi_pasien.sasaran_diet," +
                        "edukasi_pasien.cara_edukasi_diet,edukasi_pasien.metode_evaluasi_diet,edukasi_pasien.tanggal_rehab,edukasi_pasien.sasaran_rehab,edukasi_pasien.cara_edukasi_rehab," +
                        "edukasi_pasien.metode_evaluasi_rehab,edukasi_pasien.tanggal_nyeri,edukasi_pasien.sasaran_nyeri,edukasi_pasien.cara_edukasi_nyeri,edukasi_pasien.metode_evaluasi_nyeri," +
                        "edukasi_pasien.tanggal_infeksi,edukasi_pasien.sasaran_infeksi,edukasi_pasien.cara_edukasi_infeksi,edukasi_pasien.metode_evaluasi_infeksi,edukasi_pasien.tanggal_rawat," +
                        "edukasi_pasien.sasaran_rawat,edukasi_pasien.cara_edukasi_rawat,edukasi_pasien.metode_evaluasi_rawat,edukasi_pasien.tanggal_edukasi_dokter,edukasi_pasien.durasi_dokter," +
                        "edukasi_pasien.evaluasi_dokter,edukasi_pasien.tanggal_edukasi_perawat,edukasi_pasien.durasi_perawat,edukasi_pasien.evaluasi_perawat,edukasi_pasien.tanggal_edukasi_gizi," +
                        "edukasi_pasien.durasi_gizi,edukasi_pasien.evaluasi_gizi,edukasi_pasien.tanggal_edukasi_farmasi,edukasi_pasien.durasi_farmasi,edukasi_pasien.evaluasi_farmasi,"+ 
                        "edukasi_pasien.tanggal_edukasi_nyeri,edukasi_pasien.durasi_nyeri,edukasi_pasien.evaluasi_nyeri,edukasi_pasien.tanggal_edukasi_lainnya,edukasi_pasien.durasi_lainnya," +
                        "edukasi_pasien.evaluasi_lainnya,edukasi_pasien.nip,petugas.nama FROM reg_periksa " +
                        "INNER JOIN pasien ON reg_periksa.no_rkm_medis = pasien.no_rkm_medis\n" +
                        "INNER JOIN edukasi_pasien ON reg_periksa.no_rawat = edukasi_pasien.no_rawat\n" +
                        "INNER JOIN petugas ON edukasi_pasien.nip = petugas.nip\n" +
                        "WHERE edukasi_pasien.tanggal BETWEEN ? AND ?" +
                        "ORDER BY edukasi_pasien.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                        "SELECT edukasi_pasien.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,IF( pasien.jk = 'L', 'Laki-Laki', 'Perempuan' ) AS jk,pasien.tgl_lahir,edukasi_pasien.tanggal," +
                        "edukasi_pasien.bahasa,edukasi_pasien.penerjemah,edukasi_pasien.bahasa_isyarat,edukasi_pasien.cara_belajar,edukasi_pasien.tingkat_pendidikan,edukasi_pasien.hambatan_belajar," +
                        "edukasi_pasien.tanggal_hak,edukasi_pasien.sasaran_hak,edukasi_pasien.cara_edukasi_hak,edukasi_pasien.metode_evaluasi_hak," +
                        "edukasi_pasien.tanggal_orientasi,edukasi_pasien.sasaran_orientasi,edukasi_pasien.cara_edukasi_orientasi,edukasi_pasien.metode_evaluasi_orientasi," +
                        "edukasi_pasien.tanggal_diagnosa,edukasi_pasien.sasaran_diagnosa,edukasi_pasien.cara_edukasi_diagnosa,edukasi_pasien.metode_evaluasi_diagnosa," +
                        "edukasi_pasien.tanggal_obat,edukasi_pasien.sasaran_obat,edukasi_pasien.cara_edukasi_obat,edukasi_pasien.metode_evaluasi_obat,edukasi_pasien.tanggal_alat," +
                        "edukasi_pasien.sasaran_alat,edukasi_pasien.cara_edukasi_alat,edukasi_pasien.metode_evaluasi_alat,edukasi_pasien.tanggal_diet,edukasi_pasien.sasaran_diet," +
                        "edukasi_pasien.cara_edukasi_diet,edukasi_pasien.metode_evaluasi_diet,edukasi_pasien.tanggal_rehab,edukasi_pasien.sasaran_rehab,edukasi_pasien.cara_edukasi_rehab," +
                        "edukasi_pasien.metode_evaluasi_rehab,edukasi_pasien.tanggal_nyeri,edukasi_pasien.sasaran_nyeri,edukasi_pasien.cara_edukasi_nyeri,edukasi_pasien.metode_evaluasi_nyeri," +
                        "edukasi_pasien.tanggal_infeksi,edukasi_pasien.sasaran_infeksi,edukasi_pasien.cara_edukasi_infeksi,edukasi_pasien.metode_evaluasi_infeksi,edukasi_pasien.tanggal_rawat," +
                        "edukasi_pasien.sasaran_rawat,edukasi_pasien.cara_edukasi_rawat,edukasi_pasien.metode_evaluasi_rawat,edukasi_pasien.tanggal_edukasi_dokter,edukasi_pasien.durasi_dokter," +
                        "edukasi_pasien.evaluasi_dokter,edukasi_pasien.tanggal_edukasi_perawat,edukasi_pasien.durasi_perawat,edukasi_pasien.evaluasi_perawat,edukasi_pasien.tanggal_edukasi_gizi," +
                        "edukasi_pasien.durasi_gizi,edukasi_pasien.evaluasi_gizi,edukasi_pasien.tanggal_edukasi_farmasi,edukasi_pasien.durasi_farmasi,edukasi_pasien.evaluasi_farmasi,"+ 
                        "edukasi_pasien.tanggal_edukasi_nyeri,edukasi_pasien.durasi_nyeri,edukasi_pasien.evaluasi_nyeri,edukasi_pasien.tanggal_edukasi_lainnya,edukasi_pasien.durasi_lainnya," +
                        "edukasi_pasien.evaluasi_lainnya,edukasi_pasien.nip,petugas.nama FROM reg_periksa " +
                        "INNER JOIN pasien ON reg_periksa.no_rkm_medis = pasien.no_rkm_medis\n" +
                        "INNER JOIN edukasi_pasien ON reg_periksa.no_rawat = edukasi_pasien.no_rawat\n" +
                        "INNER JOIN petugas ON edukasi_pasien.nip = petugas.nip\n" +
                        "WHERE edukasi_pasien.tanggal BETWEEN ? AND ?" +
                        "AND (reg_periksa.no_rawat LIKE ? OR pasien.no_rkm_medis LIKE ?"+ 
                        "OR pasien.nm_pasien LIKE ? OR edukasi_pasien.nip LIKE ? OR petugas.nama LIKE ? ) " +
                        "ORDER BY edukasi_pasien.tanggal");
            }
                
            try {
                if(TCari.getText().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("jk"),rs.getString("tgl_lahir"),
                        rs.getString("bahasa"),rs.getString("penerjemah"),rs.getString("bahasa_isyarat"),rs.getString("cara_belajar"),
                        rs.getString("tingkat_pendidikan"),rs.getString("hambatan_belajar"),
                        rs.getString("tanggal_hak"),rs.getString("sasaran_hak"),rs.getString("cara_edukasi_hak"),rs.getString("metode_evaluasi_hak"),
                        rs.getString("tanggal_orientasi"),rs.getString("sasaran_orientasi"),rs.getString("cara_edukasi_orientasi"),rs.getString("metode_evaluasi_orientasi"),
                        rs.getString("tanggal_diagnosa"),rs.getString("sasaran_diagnosa"),rs.getString("cara_edukasi_diagnosa"),rs.getString("metode_evaluasi_diagnosa"),
                        rs.getString("tanggal_obat"),rs.getString("sasaran_obat"),rs.getString("cara_edukasi_obat"),rs.getString("metode_evaluasi_obat"),
                        rs.getString("tanggal_alat"),rs.getString("sasaran_alat"),rs.getString("cara_edukasi_alat"),rs.getString("metode_evaluasi_alat"),
                        rs.getString("tanggal_diet"),rs.getString("sasaran_diet"),rs.getString("cara_edukasi_diet"),rs.getString("metode_evaluasi_diet"),
                        rs.getString("tanggal_rehab"),rs.getString("sasaran_rehab"),rs.getString("cara_edukasi_rehab"),rs.getString("metode_evaluasi_rehab"),
                        rs.getString("tanggal_nyeri"),rs.getString("sasaran_nyeri"),rs.getString("cara_edukasi_nyeri"),rs.getString("metode_evaluasi_nyeri"),
                        rs.getString("tanggal_infeksi"),rs.getString("sasaran_infeksi"),rs.getString("cara_edukasi_infeksi"),rs.getString("metode_evaluasi_infeksi"),
                        rs.getString("tanggal_rawat"),rs.getString("sasaran_rawat"),rs.getString("cara_edukasi_rawat"),rs.getString("metode_evaluasi_rawat"),
                        rs.getString("tanggal_edukasi_dokter"),rs.getString("durasi_dokter"),rs.getString("evaluasi_dokter"),
                        rs.getString("tanggal_edukasi_perawat"),rs.getString("durasi_perawat"),rs.getString("evaluasi_perawat"),
                        rs.getString("tanggal_edukasi_gizi"),rs.getString("durasi_gizi"),rs.getString("evaluasi_gizi"),
                        rs.getString("tanggal_edukasi_farmasi"),rs.getString("durasi_farmasi"),rs.getString("evaluasi_farmasi"),
                        rs.getString("tanggal_edukasi_nyeri"),rs.getString("durasi_nyeri"),rs.getString("evaluasi_nyeri"),
                        rs.getString("tanggal_edukasi_lainnya"),rs.getString("durasi_lainnya"),rs.getString("evaluasi_lainnya"),
                        rs.getString("nip"),rs.getString("nama"),rs.getString("tanggal")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi tampil(): "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TglAsuhan.setDate(new Date());
        TabRawat.setSelectedIndex(0);
        isRawat();
        Penerjemah.setSelectedIndex(0);
        BahasaIsyarat.setSelectedIndex(0);
        CaraBelajar.setSelectedIndex(0);
        Pendidikan.setSelectedIndex(0);
        Hambatan.setSelectedIndex(0);
        TCariHambatan.setText("");
        tampilHambatan();
        TCariKebutuhan.setText("");
        tampilKebutuhan();
        TglHakWP.setDate(new Date());
        HakS.setSelectedIndex(0);
        HakCE.setSelectedIndex(0);
        HakME.setSelectedIndex(0);
        TglOrientasiWP.setDate(new Date());
        OrientasiS.setSelectedIndex(0);
        OrientasiCE.setSelectedIndex(0);
        OrientasiME.setSelectedIndex(0);
        TglDiagnosaWP.setDate(new Date());
        DiagnosaS.setSelectedIndex(0);
        DiagnosaCE.setSelectedIndex(0);
        DiagnosaME.setSelectedIndex(0);
        TglObatWP.setDate(new Date());
        ObatS.setSelectedIndex(0);
        ObatCE.setSelectedIndex(0);
        ObatME.setSelectedIndex(0);
        TglAlatWP.setDate(new Date());
        AlatS.setSelectedIndex(0);
        AlatCE.setSelectedIndex(0);
        AlatME.setSelectedIndex(0);
        TglDietWP.setDate(new Date());
        DietS.setSelectedIndex(0);
        DietCE.setSelectedIndex(0);
        DietME.setSelectedIndex(0);
        TglRehabWP.setDate(new Date());
        RehabS.setSelectedIndex(0);
        RehabCE.setSelectedIndex(0);
        RehabME.setSelectedIndex(0);
        TglNyeriWP.setDate(new Date());
        NyeriS.setSelectedIndex(0);
        NyeriCE.setSelectedIndex(0);
        NyeriME.setSelectedIndex(0);
        TglInfeksiWP.setDate(new Date());
        InfeksiS.setSelectedIndex(0);
        InfeksiCE.setSelectedIndex(0);
        InfeksiME.setSelectedIndex(0);
        TglRawatWP.setDate(new Date());
        RawatS.setSelectedIndex(0);
        RawatCE.setSelectedIndex(0);
        RawatME.setSelectedIndex(0);
        TCariEdukasiDokter.setText("");
        tampilEdukasiDokter();
        TglDokter.setDate(new Date());
        DurasiDokter.setText("");
        EvaluasiDokter.setText("");
        TCariEdukasiPerawat.setText("");
        tampilEdukasiPerawat();
        TglPerawat.setDate(new Date());
        DurasiPerawat.setText("");
        EvaluasiPerawat.setText("");
        TCariEdukasiGizi.setText("");
        tampilEdukasiGizi();
        TglGizi.setDate(new Date());
        DurasiGizi.setText("");
        EvaluasiGizi.setText("");        
        TCariEdukasiFarmasi.setText("");
        tampilEdukasiFarmasi();
        TglFarmasi.setDate(new Date());
        DurasiFarmasi.setText("");
        EvaluasiFarmasi.setText("");
        TCariEdukasiNyeri.setText("");
        tampilEdukasiNyeri();
        TglNyeri.setDate(new Date());
        DurasiNyeri.setText("");
        EvaluasiNyeri.setText("");
        TCariEdukasiLainnya.setText("");
        tampilEdukasiLainnya();
        TglLainnya.setDate(new Date());
        DurasiLainnya.setText("");
        EvaluasiLainnya.setText("");        
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()); 
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),69).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),70).toString());
            Valid.SetTgl2(TglAsuhan,tbObat.getValueAt(tbObat.getSelectedRow(),71).toString());
            Bahasa.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            Penerjemah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            BahasaIsyarat.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            CaraBelajar.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            Pendidikan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            Hambatan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            Valid.SetTgl2(TglHakWP,tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            HakS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            HakCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            HakME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            Valid.SetTgl2(TglOrientasiWP,tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            OrientasiS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            OrientasiCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            OrientasiME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            Valid.SetTgl2(TglDiagnosaWP,tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            DiagnosaS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            DiagnosaCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            DiagnosaME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());            
            Valid.SetTgl2(TglObatWP,tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            ObatS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            ObatCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            ObatME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            Valid.SetTgl2(TglAlatWP,tbObat.getValueAt(tbObat.getSelectedRow(),27).toString());
            AlatS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            AlatCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            AlatME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());            
            Valid.SetTgl2(TglDietWP,tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            DietS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());
            DietCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString());
            DietME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());
            Valid.SetTgl2(TglRehabWP,tbObat.getValueAt(tbObat.getSelectedRow(),35).toString());
            RehabS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),36).toString());
            RehabCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString());
            RehabME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),38).toString());
            Valid.SetTgl2(TglNyeriWP,tbObat.getValueAt(tbObat.getSelectedRow(),39).toString());
            NyeriS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),40).toString());
            NyeriCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),41).toString());
            NyeriME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),42).toString());
            Valid.SetTgl2(TglInfeksiWP,tbObat.getValueAt(tbObat.getSelectedRow(),43).toString());
            InfeksiS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString());
            InfeksiCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),45).toString());
            InfeksiME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),46).toString());            
            Valid.SetTgl2(TglRawatWP,tbObat.getValueAt(tbObat.getSelectedRow(),47).toString());
            RawatS.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),48).toString());
            RawatCE.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),49).toString());
            RawatME.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),50).toString());
            Valid.SetTgl2(TglDokter,tbObat.getValueAt(tbObat.getSelectedRow(),51).toString());
            DurasiDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),52).toString());
            EvaluasiDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),53).toString());
            Valid.SetTgl2(TglPerawat,tbObat.getValueAt(tbObat.getSelectedRow(),54).toString());
            DurasiPerawat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),55).toString());
            EvaluasiPerawat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),56).toString());            
            Valid.SetTgl2(TglGizi,tbObat.getValueAt(tbObat.getSelectedRow(),57).toString());
            DurasiGizi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),58).toString());
            EvaluasiGizi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),59).toString());
            Valid.SetTgl2(TglFarmasi,tbObat.getValueAt(tbObat.getSelectedRow(),60).toString());
            DurasiFarmasi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),61).toString());
            EvaluasiFarmasi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),62).toString());  
            Valid.SetTgl2(TglNyeri,tbObat.getValueAt(tbObat.getSelectedRow(),63).toString());
            DurasiNyeri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),64).toString());
            EvaluasiNyeri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),65).toString());            
            Valid.SetTgl2(TglLainnya,tbObat.getValueAt(tbObat.getSelectedRow(),66).toString());
            DurasiLainnya.setText(tbObat.getValueAt(tbObat.getSelectedRow(),67).toString());
            EvaluasiLainnya.setText(tbObat.getValueAt(tbObat.getSelectedRow(),68).toString());            
            
            //HAMBATAN BELAJAR
            try {
                Valid.tabelKosong(tabModeHambatan);
                
                ps=koneksi.prepareStatement(
                        "select master_hambatan_belajar.kode_hambatan,master_hambatan_belajar.nama_hambatan from master_hambatan_belajar "+
                        "inner join hambatan_belajar on hambatan_belajar.kode_hambatan=master_hambatan_belajar.kode_hambatan "+
                        "where hambatan_belajar.no_rawat=? order by kode_hambatan");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeHambatan.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif tampil hambatan : "+e);
            }
            
            //KEBUTUHAN BELAJAR
            try {
                Valid.tabelKosong(tabModeKebutuhan);
                
                ps=koneksi.prepareStatement(
                        "select master_kebutuhan_edukasi.kode_kebutuhan,master_kebutuhan_edukasi.nama_kebutuhan from master_kebutuhan_edukasi "+
                        "inner join kebutuhan_edukasi on kebutuhan_edukasi.kode_kebutuhan=master_kebutuhan_edukasi.kode_kebutuhan "+
                        "where kebutuhan_edukasi.no_rawat=? order by kode_kebutuhan");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeKebutuhan.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif tampil kebutuhan : "+e);
            }
            
            //EDUKASI DOKTER
            try {
                Valid.tabelKosong(tabModeEdukasiDokter);
                
                ps=koneksi.prepareStatement(
                        "select master_edukasi_dokter.kode_edukasi,master_edukasi_dokter.nama_edukasi from master_edukasi_dokter "+
                        "inner join edukasi_dokter on edukasi_dokter.kode_edukasi=master_edukasi_dokter.kode_edukasi "+
                        "where edukasi_dokter.no_rawat=? order by kode_edukasi");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeEdukasiDokter.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif tampil edukasi dokter : "+e);
            }            
            
            //EDUKASI PERAWAT
            try {
                Valid.tabelKosong(tabModeEdukasiPerawat);
                
                ps=koneksi.prepareStatement(
                        "select master_edukasi_perawat.kode_edukasi,master_edukasi_perawat.nama_edukasi from master_edukasi_perawat "+
                        "inner join edukasi_perawat on edukasi_perawat.kode_edukasi=master_edukasi_perawat.kode_edukasi "+
                        "where edukasi_perawat.no_rawat=? order by kode_edukasi");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeEdukasiPerawat.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif tampil edukasi perawat : "+e);
            }            
            
            //EDUKASI GIZI
            try {
                Valid.tabelKosong(tabModeEdukasiGizi);
                
                ps=koneksi.prepareStatement(
                        "select master_edukasi_gizi.kode_edukasi,master_edukasi_gizi.nama_edukasi from master_edukasi_gizi "+
                        "inner join edukasi_gizi on edukasi_gizi.kode_edukasi=master_edukasi_gizi.kode_edukasi "+
                        "where edukasi_gizi.no_rawat=? order by kode_edukasi");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeEdukasiGizi.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif tampil edukasi gizi: "+e);
            }
            
            //EDUKASI FARMASI
            try {
                Valid.tabelKosong(tabModeEdukasiFarmasi);
                
                ps=koneksi.prepareStatement(
                        "select master_edukasi_farmasi.kode_edukasi,master_edukasi_farmasi.nama_edukasi from master_edukasi_farmasi "+
                        "inner join edukasi_farmasi on edukasi_farmasi.kode_edukasi=master_edukasi_farmasi.kode_edukasi "+
                        "where edukasi_farmasi.no_rawat=? order by kode_edukasi");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeEdukasiFarmasi.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif tampil edukasi farmasi: "+e);
            }          
            
            //EDUKASI NYERI     
            try {
                Valid.tabelKosong(tabModeEdukasiNyeri);
                
                ps=koneksi.prepareStatement(
                        "select master_edukasi_nyeri.kode_edukasi,master_edukasi_nyeri.nama_edukasi from master_edukasi_nyeri "+
                        "inner join edukasi_nyeri on edukasi_nyeri.kode_edukasi=master_edukasi_nyeri.kode_edukasi "+
                        "where edukasi_nyeri.no_rawat=? order by kode_edukasi");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeEdukasiNyeri.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif tampil edukasi Nyeri: "+e);
            }            
            
            //EDUKASI LAINNYA
            try {
                Valid.tabelKosong(tabModeEdukasiLainnya);
                
                ps=koneksi.prepareStatement(
                        "select master_edukasi_lainnya.kode_edukasi,master_edukasi_lainnya.nama_edukasi from master_edukasi_lainnya "+
                        "inner join edukasi_lainnya on edukasi_lainnya.kode_edukasi=master_edukasi_lainnya.kode_edukasi "+
                        "where edukasi_lainnya.no_rawat=? order by kode_edukasi");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeEdukasiLainnya.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif tampil edukasi lainnya: "+e);
            }                
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,"+
                    "pasien.tgl_lahir,pasien.agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
                    "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                    Bahasa.setText(rs.getString("nama_bahasa"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    public void setNoRm(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);    
        isRawat();
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnHapus.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        if(akses.getjml2()>=1){
            KdPetugas.setEditable(false);
            BtnDokter.setEnabled(false);
            KdPetugas.setText(akses.getkode());
            Sequel.cariIsi("select nama from petugas where nip=?", NmPetugas,KdPetugas.getText());
            if(NmPetugas.getText().equals("")){
                KdPetugas.setText("");
                JOptionPane.showMessageDialog(null,"User login bukan petugas...!!");
            }
        }            
    }

    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }
    
    private void hapus() {
        if(Sequel.queryu2tf("delete from edukasi_pasien where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            //HAMBATAN BELAJAR
            Sequel.meghapus("hambatan_belajar","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            //KEBUTUHAN EDUKASI
            Sequel.meghapus("kebutuhan_edukasi","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            //EDUKASI DOKTER
            Sequel.meghapus("edukasi_dokter","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            //EDUKASI PERAWAT
            Sequel.meghapus("edukasi_perawat","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());      
            //EDUKASI GIZI
            Sequel.meghapus("edukasi_gizi","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                    
            //EDUKASI FARMASI
            Sequel.meghapus("edukasi_farmasi","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            //EDUKASI NYERI                    
            Sequel.meghapus("edukasi_nyeri","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());                   
            //EDUKASI LAINNYA
            Sequel.meghapus("edukasi_lainnya","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            Valid.tabelKosong(tabModeHambatan);
            Valid.tabelKosong(tabModeKebutuhan);
            Valid.tabelKosong(tabModeEdukasiDokter);
            Valid.tabelKosong(tabModeEdukasiPerawat);
            Valid.tabelKosong(tabModeEdukasiGizi);
            Valid.tabelKosong(tabModeEdukasiFarmasi);
            Valid.tabelKosong(tabModeEdukasiNyeri);
            Valid.tabelKosong(tabModeEdukasiLainnya);
            tampil();
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("edukasi_pasien","no_rawat=?","no_rawat=?,tanggal=?,bahasa=?,penerjemah=?,bahasa_isyarat=?,"
                + "cara_belajar=?,tingkat_pendidikan=?,hambatan_belajar=?,tanggal_hak=?,sasaran_hk=?,"
                + "cara_edukasi_hak=?,metode_evaluasi_hak=?,tanggal_orientasi=?,sasaran_orientasi=?,cara_edukasi_orientasi=?,"
                + "metode_evaluasi_orientasi=?,tanggal_diagnosa=?,sasaran_diagnosa=?,cara_edukasi_diagnosa=?,metode_evaluasi_diagnosa=?,"
                + "tanggal_obat=?,sasaran_obat=?,cara_edukasi_obat=?,metode_evaluasi_obat=?,tanggal_alat=?,"
                + "sasaran_alat=?,cara_edukasi_alat=?,metode_evaluasi_alat=?,tanggal_diet=?,sasaran_diet=?,"
                + "cara_edukasi_diet=?,metode_evaluasi_diet=?,tanggal_rehab=?,sasaran_rehab=?,cara_edukasi_rehab=?,"
                + "metode_evaluasi_rehab=?,tanggal_nyeri=?,sasaran_nyeri=?,cara_edukasi_nyeri=?,metode_evaluasi_nyeri=?,"
                + "tanggal_infeksi=?,sasaran_infeksi=?,cara_edukasi_infeksi=?,metode_evaluasi_infeksi=?,tanggal_rawat=?,"
                + "sasaran_rawat=?,cara_edukasi_rawat=?,metode_evaluasi_rawat=?,tanggal_edukasi_dokter=?,durasi_dokter=?,"
                + "evaluasi_dokter=?,tanggal_edukasi_perawat=?,durasi_perawat=?,evaluasi_perawat=?,tanggal_edukasi_gizi=?,"
                + "durasi_gizi=?,evaluasi_gizi=?,tanggal_edukasi_farmasi=?,durasi_farmasi=?,evaluasi_farmasi=?,"
                + "tanggal_edukasi_nyeri=?,durasi_nyeri=?,evaluasi_nyeri=?,tanggal_edukasi_lainnya=?,durasi_lainnya=?,"
                + "evaluasi_lainnya=?,nip=?",68,new String[]{
                    TNoRw.getText(),Valid.SetTgl(TglAsuhan.getSelectedItem()+"")+" "+TglAsuhan.getSelectedItem().toString().substring(11,19),Bahasa.getText(),Penerjemah.getSelectedItem().toString(),BahasaIsyarat.getSelectedItem().toString(),
                    CaraBelajar.getSelectedItem().toString(),Pendidikan.getSelectedItem().toString(),Hambatan.getSelectedItem().toString(),Valid.SetTgl(TglHakWP.getSelectedItem()+"")+" "+TglHakWP.getSelectedItem().toString().substring(11,19),HakS.getSelectedItem().toString(),
                    HakCE.getSelectedItem().toString(),HakME.getSelectedItem().toString(),Valid.SetTgl(TglOrientasiWP.getSelectedItem()+"")+" "+TglOrientasiWP.getSelectedItem().toString().substring(11,19),OrientasiS.getSelectedItem().toString(),OrientasiCE.getSelectedItem().toString(),
                    OrientasiME.getSelectedItem().toString(),Valid.SetTgl(TglDiagnosaWP.getSelectedItem()+"")+" "+TglDiagnosaWP.getSelectedItem().toString().substring(11,19),DiagnosaS.getSelectedItem().toString(),DiagnosaCE.getSelectedItem().toString(),DiagnosaME.getSelectedItem().toString(),
                    Valid.SetTgl(TglObatWP.getSelectedItem()+"")+" "+TglObatWP.getSelectedItem().toString().substring(11,19),ObatS.getSelectedItem().toString(),ObatCE.getSelectedItem().toString(),ObatME.getSelectedItem().toString(),Valid.SetTgl(TglAlatWP.getSelectedItem()+"")+" "+TglAlatWP.getSelectedItem().toString().substring(11,19),
                    AlatS.getSelectedItem().toString(),AlatCE.getSelectedItem().toString(),AlatME.getSelectedItem().toString(),Valid.SetTgl(TglDietWP.getSelectedItem()+"")+" "+TglDietWP.getSelectedItem().toString().substring(11,19),DietS.getSelectedItem().toString(),
                    DietCE.getSelectedItem().toString(),DietME.getSelectedItem().toString(),Valid.SetTgl(TglRehabWP.getSelectedItem()+"")+" "+TglRehabWP.getSelectedItem().toString().substring(11,19),RehabS.getSelectedItem().toString(),RehabCE.getSelectedItem().toString(),
                    RehabME.getSelectedItem().toString(),Valid.SetTgl(TglNyeriWP.getSelectedItem()+"")+" "+TglNyeriWP.getSelectedItem().toString().substring(11,19),NyeriS.getSelectedItem().toString(),NyeriCE.getSelectedItem().toString(),NyeriME.getSelectedItem().toString(),
                    Valid.SetTgl(TglInfeksiWP.getSelectedItem()+"")+" "+TglInfeksiWP.getSelectedItem().toString().substring(11,19),InfeksiS.getSelectedItem().toString(),InfeksiCE.getSelectedItem().toString(),InfeksiME.getSelectedItem().toString(),Valid.SetTgl(TglRawatWP.getSelectedItem()+"")+" "+TglRawatWP.getSelectedItem().toString().substring(11,19),
                    RawatS.getSelectedItem().toString(),RawatCE.getSelectedItem().toString(),RawatME.getSelectedItem().toString(),Valid.SetTgl(TglDokter.getSelectedItem()+"")+" "+TglDokter.getSelectedItem().toString().substring(11,19),DurasiDokter.getText(),
                    EvaluasiDokter.getText(),Valid.SetTgl(TglPerawat.getSelectedItem()+"")+" "+TglPerawat.getSelectedItem().toString().substring(11,19),DurasiPerawat.getText(),EvaluasiDokter.getText(),Valid.SetTgl(TglGizi.getSelectedItem()+"")+" "+TglGizi.getSelectedItem().toString().substring(11,19),
                    DurasiGizi.getText(),EvaluasiGizi.getText(),Valid.SetTgl(TglFarmasi.getSelectedItem()+"")+" "+TglFarmasi.getSelectedItem().toString().substring(11,19),DurasiFarmasi.getText(),EvaluasiFarmasi.getText(),
                    Valid.SetTgl(TglNyeri.getSelectedItem()+"")+" "+TglNyeri.getSelectedItem().toString().substring(11,19),DurasiNyeri.getText(),EvaluasiNyeri.getText(),Valid.SetTgl(TglLainnya.getSelectedItem()+"")+" "+TglLainnya.getSelectedItem().toString().substring(11,19),DurasiLainnya.getText(),
                    EvaluasiLainnya.getText(),KdPetugas.getText(),
                    tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
             })==true){
                //HAMBATAN BELAJAR
                Sequel.meghapus("hambatan_belajar","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                for (i = 0; i < tbHambatanBelajar.getRowCount(); i++) {
                        if(tbHambatanBelajar.getValueAt(i,0).toString().equals("true")){
                            Sequel.menyimpan2("hambatan_belajar","?,?",2,new String[]{TNoRw.getText(),tbHambatanBelajar.getValueAt(i,1).toString()});
                        }
                }
                //KEBUTUHAN EDUKASI
                Sequel.meghapus("kebutuhan_edukasi","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                for (i = 0; i < tbKebutuhanEdukasi.getRowCount(); i++) {
                    if(tbKebutuhanEdukasi.getValueAt(i,0).toString().equals("true")){
                        Sequel.menyimpan2("kebutuhan_edukasi","?,?",2,new String[]{TNoRw.getText(),tbKebutuhanEdukasi.getValueAt(i,1).toString()});
                    }
                }
                //EDUKASI DOKTER
                Sequel.meghapus("edukasi_dokter","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                for (i = 0; i < tbEdukasiDokter.getRowCount(); i++) {
                    if(tbEdukasiDokter.getValueAt(i,0).toString().equals("true")){
                        Sequel.menyimpan2("edukasi_dokter","?,?",2,new String[]{TNoRw.getText(),tbEdukasiDokter.getValueAt(i,1).toString()});
                    }
                }
                //EDUKASI PERAWAT
                Sequel.meghapus("edukasi_perawat","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                for (i = 0; i < tbEdukasiPerawat.getRowCount(); i++) {
                    if(tbEdukasiPerawat.getValueAt(i,0).toString().equals("true")){
                        Sequel.menyimpan2("edukasi_perawat","?,?",2,new String[]{TNoRw.getText(),tbEdukasiPerawat.getValueAt(i,1).toString()});
                    }
                }       
                //EDUKASI GIZI
                Sequel.meghapus("edukasi_gizi","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                for (i = 0; i < tbEdukasiGizi.getRowCount(); i++) {
                    if(tbEdukasiGizi.getValueAt(i,0).toString().equals("true")){
                        Sequel.menyimpan2("edukasi_gizi","?,?",2,new String[]{TNoRw.getText(),tbEdukasiGizi.getValueAt(i,1).toString()});
                    }
                }                    
                //EDUKASI FARMASI
                Sequel.meghapus("edukasi_farmasi","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                for (i = 0; i < tbEdukasiFarmasi.getRowCount(); i++) {
                    if(tbEdukasiFarmasi.getValueAt(i,0).toString().equals("true")){
                        Sequel.menyimpan2("edukasi_farmasi","?,?",2,new String[]{TNoRw.getText(),tbEdukasiFarmasi.getValueAt(i,1).toString()});
                    }
                }
                //EDUKASI NYERI                    
                Sequel.meghapus("edukasi_nyeri","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                for (i = 0; i < tbEdukasiNyeri.getRowCount(); i++) {
                    if(tbEdukasiNyeri.getValueAt(i,0).toString().equals("true")){
                        Sequel.menyimpan2("edukasi_nyeri","?,?",2,new String[]{TNoRw.getText(),tbEdukasiNyeri.getValueAt(i,1).toString()});
                    }
                }                    
                //EDUKASI LAINNYA
                Sequel.meghapus("edukasi_lainnya","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                for (i = 0; i < tbEdukasiLainnya.getRowCount(); i++) {
                    if(tbEdukasiLainnya.getValueAt(i,0).toString().equals("true")){
                        Sequel.menyimpan2("edukasi_lainnya","?,?",2,new String[]{TNoRw.getText(),tbEdukasiLainnya.getValueAt(i,1).toString()});
                    }
                } 
                emptTeks();
                TabRawat.setSelectedIndex(1);
                tampil();
        }
    }
    
    private void tampilHambatan() {
        try{
            Valid.tabelKosong(tabModeHambatan);
            file=new File("./cache/hambatanbelajar.iyem");
            file.createNewFile();
            fileWriter = new FileWriter(file);
            iyem="";
            ps=koneksi.prepareStatement("select * from master_hambatan_belajar order by master_hambatan_belajar.kode_hambatan");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeHambatan.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                    iyem=iyem+"{\"KodeHambatan\":\""+rs.getString(1)+"\",\"NamaHambatan\":\""+rs.getString(2)+"\"},";
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            fileWriter.write("{\"hambatanbelajar\":["+iyem.substring(0,iyem.length()-1)+"]}");
            fileWriter.flush();
            fileWriter.close();
            iyem=null;
        }catch(Exception e){
            System.out.println("Notifikasi hambatan: "+e);
        }
    }    
    
    private void tampilHambatan2() {
        try{
            jml=0;
            for(i=0;i<tbHambatanBelajar.getRowCount();i++){
                if(tbHambatanBelajar.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            kode=null;
            kode=new String[jml];
            hambatan=null;
            hambatan=new String[jml];

            index=0;        
            for(i=0;i<tbHambatanBelajar.getRowCount();i++){
                if(tbHambatanBelajar.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    kode[index]=tbHambatanBelajar.getValueAt(i,1).toString();
                    hambatan[index]=tbHambatanBelajar.getValueAt(i,2).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeHambatan);

            for(i=0;i<jml;i++){
                tabModeHambatan.addRow(new Object[] {
                    pilih[i],kode[i],hambatan[i]
                });
            }
            
            myObj = new FileReader("./cache/hambatanbelajar.iyem");
            root = mapper.readTree(myObj);
            response = root.path("hambatanbelajar");
            if(response.isArray()){
                for(JsonNode list:response){
                    if(list.path("KodeHambatan").asText().toLowerCase().contains(TCariHambatan.getText().toLowerCase())||list.path("NamaHambatan").asText().toLowerCase().contains(TCariHambatan.getText().toLowerCase())){
                        tabModeHambatan.addRow(new Object[]{
                            false,list.path("KodeHambatan").asText(),list.path("NamaHambatan").asText()
                        });                    
                    }
                }
            }
            myObj.close();
        }catch(Exception e){
            System.out.println("Notifikasi hambatan: "+e);
        }
    }
    
    private void tampilKebutuhan() {
        try{
            Valid.tabelKosong(tabModeKebutuhan);
            file=new File("./cache/kebutuhanedukasi.iyem");
            file.createNewFile();
            fileWriter = new FileWriter(file);
            iyem="";
            ps=koneksi.prepareStatement("select * from master_kebutuhan_edukasi order by master_kebutuhan_edukasi.kode_kebutuhan");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeKebutuhan.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                    iyem=iyem+"{\"KodeKebutuhan\":\""+rs.getString(1)+"\",\"NamaKebutuhan\":\""+rs.getString(2)+"\"},";
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            fileWriter.write("{\"kebutuhanedukasi\":["+iyem.substring(0,iyem.length()-1)+"]}");
            fileWriter.flush();
            fileWriter.close();
            iyem=null;
        }catch(Exception e){
            System.out.println("Notifikasi kebutuhan: "+e);
        }
    }
    
    private void tampilKebutuhan2() {
        try{
            jml=0;
            for(i=0;i<tbKebutuhanEdukasi.getRowCount();i++){
                if(tbKebutuhanEdukasi.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            kode=null;
            kode=new String[jml];
            kebutuhan=null;
            kebutuhan=new String[jml];

            index=0;        
            for(i=0;i<tbKebutuhanEdukasi.getRowCount();i++){
                if(tbKebutuhanEdukasi.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    kode[index]=tbKebutuhanEdukasi.getValueAt(i,1).toString();
                    kebutuhan[index]=tbKebutuhanEdukasi.getValueAt(i,2).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeKebutuhan);

            for(i=0;i<jml;i++){
                tabModeKebutuhan.addRow(new Object[] {
                    pilih[i],kode[i],kebutuhan[i]
                });
            }
            
            myObj = new FileReader("./cache/kebutuhanedukasi.iyem");
            root = mapper.readTree(myObj);
            response = root.path("kebutuhanedukasi");
            if(response.isArray()){
                for(JsonNode list:response){
                    if(list.path("KodeKebutuhan").asText().toLowerCase().contains(TCariKebutuhan.getText().toLowerCase())||list.path("NamaKebutuhan").asText().toLowerCase().contains(TCariKebutuhan.getText().toLowerCase())){
                        tabModeKebutuhan.addRow(new Object[]{
                            false,list.path("KodeKebutuhan").asText(),list.path("NamaKebutuhan").asText()
                        });                    
                    }
                }
            }
            myObj.close();
        }catch(Exception e){
            System.out.println("Notifikasi kebutuhan: "+e);
        }
    }
    
    private void tampilEdukasiDokter() {
        try{
            Valid.tabelKosong(tabModeEdukasiDokter);
            file=new File("./cache/edukasidokter.iyem");
            file.createNewFile();
            fileWriter = new FileWriter(file);
            iyem="";
            ps=koneksi.prepareStatement("select * from master_edukasi_dokter order by master_edukasi_dokter.kode_edukasi");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeEdukasiDokter.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                    iyem=iyem+"{\"KodeEdukasi\":\""+rs.getString(1)+"\",\"NamaEdukasi\":\""+rs.getString(2)+"\"},";
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            fileWriter.write("{\"edukasidokter\":["+iyem.substring(0,iyem.length()-1)+"]}");
            fileWriter.flush();
            fileWriter.close();
            iyem=null;
        }catch(Exception e){
            System.out.println("Notifikasi edukasi dokter: "+e);
        }
    }
    
    private void tampilEdukasiDokter2() {
        try{
            jml=0;
            for(i=0;i<tbEdukasiDokter.getRowCount();i++){
                if(tbEdukasiDokter.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            kode=null;
            kode=new String[jml];
            edukasi=null;
            edukasi=new String[jml];

            index=0;        
            for(i=0;i<tbEdukasiDokter.getRowCount();i++){
                if(tbEdukasiDokter.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    kode[index]=tbEdukasiDokter.getValueAt(i,1).toString();
                    edukasi[index]=tbEdukasiDokter.getValueAt(i,2).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeEdukasiDokter);

            for(i=0;i<jml;i++){
                tabModeEdukasiDokter.addRow(new Object[] {
                    pilih[i],kode[i],edukasi[i]
                });
            }
            
            myObj = new FileReader("./cache/edukasidokter.iyem");
            root = mapper.readTree(myObj);
            response = root.path("edukasidokter");
            if(response.isArray()){
                for(JsonNode list:response){
                    if(list.path("KodeEdukasi").asText().toLowerCase().contains(TCariEdukasiDokter.getText().toLowerCase())||list.path("NamaEdukasi").asText().toLowerCase().contains(TCariEdukasiDokter.getText().toLowerCase())){
                        tabModeEdukasiDokter.addRow(new Object[]{
                            false,list.path("KodeEdukasi").asText(),list.path("NamaEdukasi").asText()
                        });                    
                    }
                }
            }
            myObj.close();
        }catch(Exception e){
            System.out.println("Notifikasi edukasi dokter: "+e);
        }
    }    

    private void tampilEdukasiPerawat() {
        try{
            Valid.tabelKosong(tabModeEdukasiPerawat);
            file=new File("./cache/edukasiperawat.iyem");
            file.createNewFile();
            fileWriter = new FileWriter(file);
            iyem="";
            ps=koneksi.prepareStatement("select * from master_edukasi_perawat order by master_edukasi_perawat.kode_edukasi");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeEdukasiPerawat.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                    iyem=iyem+"{\"KodeEdukasi\":\""+rs.getString(1)+"\",\"NamaEdukasi\":\""+rs.getString(2)+"\"},";
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            fileWriter.write("{\"edukasiperawat\":["+iyem.substring(0,iyem.length()-1)+"]}");
            fileWriter.flush();
            fileWriter.close();
            iyem=null;
        }catch(Exception e){
            System.out.println("Notifikasi edukasi perawat: "+e);
        }
    }
    
    private void tampilEdukasiPerawat2() {
        try{
            jml=0;
            for(i=0;i<tbEdukasiPerawat.getRowCount();i++){
                if(tbEdukasiPerawat.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            kode=null;
            kode=new String[jml];
            edukasi=null;
            edukasi=new String[jml];

            index=0;        
            for(i=0;i<tbEdukasiDokter.getRowCount();i++){
                if(tbEdukasiDokter.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    kode[index]=tbEdukasiPerawat.getValueAt(i,1).toString();
                    edukasi[index]=tbEdukasiPerawat.getValueAt(i,2).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeEdukasiPerawat);

            for(i=0;i<jml;i++){
                tabModeEdukasiPerawat.addRow(new Object[] {
                    pilih[i],kode[i],edukasi[i]
                });
            }
            
            myObj = new FileReader("./cache/edukasiperawat.iyem");
            root = mapper.readTree(myObj);
            response = root.path("edukasiperawat");
            if(response.isArray()){
                for(JsonNode list:response){
                    if(list.path("KodeEdukasi").asText().toLowerCase().contains(TCariEdukasiPerawat.getText().toLowerCase())||list.path("NamaEdukasi").asText().toLowerCase().contains(TCariEdukasiPerawat.getText().toLowerCase())){
                        tabModeEdukasiPerawat.addRow(new Object[]{
                            false,list.path("KodeEdukasi").asText(),list.path("NamaEdukasi").asText()
                        });                    
                    }
                }
            }
            myObj.close();
        }catch(Exception e){
            System.out.println("Notifikasi edukasi perawat: "+e);
        }
    }       
    
    private void tampilEdukasiGizi() {
        try{
            Valid.tabelKosong(tabModeEdukasiGizi);
            file=new File("./cache/edukasigizi.iyem");
            file.createNewFile();
            fileWriter = new FileWriter(file);
            iyem="";
            ps=koneksi.prepareStatement("select * from master_edukasi_gizi order by master_edukasi_gizi.kode_edukasi");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeEdukasiGizi.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                    iyem=iyem+"{\"KodeEdukasi\":\""+rs.getString(1)+"\",\"NamaEdukasi\":\""+rs.getString(2)+"\"},";
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            fileWriter.write("{\"edukasigizi\":["+iyem.substring(0,iyem.length()-1)+"]}");
            fileWriter.flush();
            fileWriter.close();
            iyem=null;
        }catch(Exception e){
            System.out.println("Notifikasi edukasi gizi: "+e);
        }
    }
    
    private void tampilEdukasiGizi2() {
        try{
            jml=0;
            for(i=0;i<tbEdukasiGizi.getRowCount();i++){
                if(tbEdukasiGizi.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            kode=null;
            kode=new String[jml];
            edukasi=null;
            edukasi=new String[jml];

            index=0;        
            for(i=0;i<tbEdukasiGizi.getRowCount();i++){
                if(tbEdukasiGizi.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    kode[index]=tbEdukasiGizi.getValueAt(i,1).toString();
                    edukasi[index]=tbEdukasiGizi.getValueAt(i,2).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeEdukasiGizi);

            for(i=0;i<jml;i++){
                tabModeEdukasiGizi.addRow(new Object[] {
                    pilih[i],kode[i],edukasi[i]
                });
            }
            
            myObj = new FileReader("./cache/edukasigizi.iyem");
            root = mapper.readTree(myObj);
            response = root.path("edukasigizi");
            if(response.isArray()){
                for(JsonNode list:response){
                    if(list.path("KodeEdukasi").asText().toLowerCase().contains(TCariEdukasiGizi.getText().toLowerCase())||list.path("NamaEdukasi").asText().toLowerCase().contains(TCariEdukasiGizi.getText().toLowerCase())){
                        tabModeEdukasiGizi.addRow(new Object[]{
                            false,list.path("KodeEdukasi").asText(),list.path("NamaEdukasi").asText()
                        });                    
                    }
                }
            }
            myObj.close();
        }catch(Exception e){
            System.out.println("Notifikasi edukasi gizi: "+e);
        }
    }    
    
    private void tampilEdukasiFarmasi() {
        try{
            Valid.tabelKosong(tabModeEdukasiFarmasi);
            file=new File("./cache/edukasifarmasi.iyem");
            file.createNewFile();
            fileWriter = new FileWriter(file);
            iyem="";
            ps=koneksi.prepareStatement("select * from master_edukasi_farmasi order by master_edukasi_farmasi.kode_edukasi");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeEdukasiFarmasi.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                    iyem=iyem+"{\"KodeEdukasi\":\""+rs.getString(1)+"\",\"NamaEdukasi\":\""+rs.getString(2)+"\"},";
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            fileWriter.write("{\"edukasifarmasi\":["+iyem.substring(0,iyem.length()-1)+"]}");
            fileWriter.flush();
            fileWriter.close();
            iyem=null;
        }catch(Exception e){
            System.out.println("Notifikasi edukasi farmasi: "+e);
        }
    }
    
    private void tampilEdukasiFarmasi2() {
        try{
            jml=0;
            for(i=0;i<tbEdukasiFarmasi.getRowCount();i++){
                if(tbEdukasiFarmasi.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            kode=null;
            kode=new String[jml];
            edukasi=null;
            edukasi=new String[jml];

            index=0;        
            for(i=0;i<tbEdukasiFarmasi.getRowCount();i++){
                if(tbEdukasiFarmasi.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    kode[index]=tbEdukasiFarmasi.getValueAt(i,1).toString();
                    edukasi[index]=tbEdukasiFarmasi.getValueAt(i,2).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeEdukasiFarmasi);

            for(i=0;i<jml;i++){
                tabModeEdukasiFarmasi.addRow(new Object[] {
                    pilih[i],kode[i],edukasi[i]
                });
            }
            
            myObj = new FileReader("./cache/edukasifarmasi.iyem");
            root = mapper.readTree(myObj);
            response = root.path("edukasifarmasi");
            if(response.isArray()){
                for(JsonNode list:response){
                    if(list.path("KodeEdukasi").asText().toLowerCase().contains(TCariEdukasiFarmasi.getText().toLowerCase())||list.path("NamaEdukasi").asText().toLowerCase().contains(TCariEdukasiFarmasi.getText().toLowerCase())){
                        tabModeEdukasiFarmasi.addRow(new Object[]{
                            false,list.path("KodeEdukasi").asText(),list.path("NamaEdukasi").asText()
                        });                    
                    }
                }
            }
            myObj.close();
        }catch(Exception e){
            System.out.println("Notifikasi edukasi farmasi: "+e);
        }
    }   
    
    private void tampilEdukasiNyeri() {
        try{
            Valid.tabelKosong(tabModeEdukasiNyeri);
            file=new File("./cache/edukasinyeri.iyem");
            file.createNewFile();
            fileWriter = new FileWriter(file);
            iyem="";
            ps=koneksi.prepareStatement("select * from master_edukasi_nyeri order by master_edukasi_nyeri.kode_edukasi");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeEdukasiNyeri.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                    iyem=iyem+"{\"KodeEdukasi\":\""+rs.getString(1)+"\",\"NamaEdukasi\":\""+rs.getString(2)+"\"},";
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            fileWriter.write("{\"edukasinyeri\":["+iyem.substring(0,iyem.length()-1)+"]}");
            fileWriter.flush();
            fileWriter.close();
            iyem=null;
        }catch(Exception e){
            System.out.println("Notifikasi edukasi nyeri: "+e);
        }
    }
    
    private void tampilEdukasiNyeri2() {
        try{
            jml=0;
            for(i=0;i<tbEdukasiNyeri.getRowCount();i++){
                if(tbEdukasiNyeri.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            kode=null;
            kode=new String[jml];
            edukasi=null;
            edukasi=new String[jml];

            index=0;        
            for(i=0;i<tbEdukasiNyeri.getRowCount();i++){
                if(tbEdukasiNyeri.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    kode[index]=tbEdukasiNyeri.getValueAt(i,1).toString();
                    edukasi[index]=tbEdukasiNyeri.getValueAt(i,2).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeEdukasiNyeri);

            for(i=0;i<jml;i++){
                tabModeEdukasiNyeri.addRow(new Object[] {
                    pilih[i],kode[i],edukasi[i]
                });
            }
            
            myObj = new FileReader("./cache/edukasinyeri.iyem");
            root = mapper.readTree(myObj);
            response = root.path("edukasinyeri");
            if(response.isArray()){
                for(JsonNode list:response){
                    if(list.path("KodeEdukasi").asText().toLowerCase().contains(TCariEdukasiNyeri.getText().toLowerCase())||list.path("NamaEdukasi").asText().toLowerCase().contains(TCariEdukasiNyeri.getText().toLowerCase())){
                        tabModeEdukasiNyeri.addRow(new Object[]{
                            false,list.path("KodeEdukasi").asText(),list.path("NamaEdukasi").asText()
                        });                    
                    }
                }
            }
            myObj.close();
        }catch(Exception e){
            System.out.println("Notifikasi edukasi nyeri: "+e);
        }
    }      
    
    private void tampilEdukasiLainnya() {
        try{
            Valid.tabelKosong(tabModeEdukasiLainnya);
            file=new File("./cache/edukasilainnya.iyem");
            file.createNewFile();
            fileWriter = new FileWriter(file);
            iyem="";
            ps=koneksi.prepareStatement("select * from master_edukasi_lainnya order by master_edukasi_lainnya.kode_edukasi");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeEdukasiLainnya.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                    iyem=iyem+"{\"KodeEdukasi\":\""+rs.getString(1)+"\",\"NamaEdukasi\":\""+rs.getString(2)+"\"},";
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            fileWriter.write("{\"edukasilainnya\":["+iyem.substring(0,iyem.length()-1)+"]}");
            fileWriter.flush();
            fileWriter.close();
            iyem=null;
        }catch(Exception e){
            System.out.println("Notifikasi edukasi lainnya: "+e);
        }
    }
    
    private void tampilEdukasiLainnya2() {
        try{
            jml=0;
            for(i=0;i<tbEdukasiLainnya.getRowCount();i++){
                if(tbEdukasiLainnya.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            kode=null;
            kode=new String[jml];
            edukasi=null;
            edukasi=new String[jml];

            index=0;        
            for(i=0;i<tbEdukasiLainnya.getRowCount();i++){
                if(tbEdukasiLainnya.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    kode[index]=tbEdukasiLainnya.getValueAt(i,1).toString();
                    edukasi[index]=tbEdukasiLainnya.getValueAt(i,2).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeEdukasiLainnya);

            for(i=0;i<jml;i++){
                tabModeEdukasiLainnya.addRow(new Object[] {
                    pilih[i],kode[i],edukasi[i]
                });
            }
            
            myObj = new FileReader("./cache/edukasilainnya.iyem");
            root = mapper.readTree(myObj);
            response = root.path("edukasilainnya");
            if(response.isArray()){
                for(JsonNode list:response){
                    if(list.path("KodeEdukasi").asText().toLowerCase().contains(TCariEdukasiLainnya.getText().toLowerCase())||list.path("NamaEdukasi").asText().toLowerCase().contains(TCariEdukasiLainnya.getText().toLowerCase())){
                        tabModeEdukasiLainnya.addRow(new Object[]{
                            false,list.path("KodeEdukasi").asText(),list.path("NamaEdukasi").asText()
                        });                    
                    }
                }
            }
            myObj.close();
        }catch(Exception e){
            System.out.println("Notifikasi edukasi lainnya: "+e);
        }
    }       
}
