/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.akses;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;
import simrskhanza.DlgCariListLaporanOK;
import simrskhanza.DlgCariOperatorSignIn;


/**
 *
 * @author perpustakaan
 */
public final class RMTimeOutSebelumInsisi extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0,pilihan=0;    
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariListLaporanOK carilaporanOK=new DlgCariListLaporanOK(null,false);
    private DlgCariOperatorSignIn carioperatorsignin=new DlgCariOperatorSignIn(null,false);
    private String finger="",finger2="";
    private StringBuilder htmlContent;
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMTimeOutSebelumInsisi(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(628,674);

        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tgl.Lahir","J.K.","Tanggal","Jam","Kd Operator 1","Nama Operator 1","Kd Operator 2","Nama Operator 2","Kd Dokter Anestesi","Dokter Anestesi","Kd Dokter Anak","Dokter Anak","Kd Bidan Perina","Bidan Perina","Kd Instrumen","Petugas Instrumen",
            "Konfirmasi Tim","Konfirmasi Pasien","Tindakan","Tempat Insisi","Konfirmasi Antibiotik","Jam Antibiotik","Keterangan Konfirmasi Antibiotik","Antisipasi Operator","Jam Lama Operasi","Menit Lama Operasi","Antisipasi Kehilangan Darah","Ket Antisipasi Kehilangan Darah","ASA","Jenis Anestesi","Antisipasi Anestesi","Antisipasi Keperawatan 1","Keterangan Antisipasi Keperawatan 1","Antisipasi Keperawatan 2","Keterangan Antisipasi Keperawatan 2","Antisipasi Keperawatan 3","Keterangan Antisipasi Keperawatan 3","Antisipasi Keperawatan 4","Keterangan Antisipasi Keperawatan 4",
            "Kode Petugas","Nama Petugas","Kode Asisten 1","Nama Asisten 1","Kode Asisten 2","Nama Asisten 2","Kode Perawat Anestesi","Nama Perawat Anestesi"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 48; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(150);
            }else if(i==3){
                column.setPreferredWidth(65);
            }else if(i==4){
                column.setPreferredWidth(25);
            }else if(i==5){
                column.setPreferredWidth(115);
            }else if(i==6){
                column.setPreferredWidth(90);
            }else if(i==7){
                column.setPreferredWidth(160);
            }else if(i==8){
                column.setPreferredWidth(100);
            }else if(i==9){
                column.setPreferredWidth(160);
            }else if(i==10){
                column.setPreferredWidth(100);
            }else if(i==11){
                column.setPreferredWidth(150);
            }else if(i==12){
                column.setPreferredWidth(88);
            }else if(i==13){
                column.setPreferredWidth(88);
            }else if(i==14){
                column.setPreferredWidth(95);
            }else if(i==15){
                column.setPreferredWidth(130);
            }else if(i==16){
                column.setPreferredWidth(76);
            }else if(i==17){
                column.setPreferredWidth(117);
            }else if(i==18){
                column.setPreferredWidth(110);
            }else if(i==19){
                column.setPreferredWidth(90);
            }else if(i==20){
                column.setPreferredWidth(111);
            }else if(i==21){
                column.setPreferredWidth(135);
            }else if(i==22){
                column.setPreferredWidth(200);
            }else if(i==23){
                column.setPreferredWidth(145);
            }else if(i==24){
                column.setPreferredWidth(145);
            }else if(i==25){
                column.setPreferredWidth(145);
            }else if(i==26){
                column.setPreferredWidth(145);
            }else if(i==27){
                column.setPreferredWidth(145);
            }else if(i==28){
                column.setPreferredWidth(145);
            }else if(i==29){
                column.setPreferredWidth(145);
            }else if(i==30){
                column.setPreferredWidth(145);
            }else if(i==31){
                column.setPreferredWidth(145);
            }else if(i==32){
                column.setPreferredWidth(145);
            }else if(i==33){
                column.setPreferredWidth(145);
            }else if(i==34){
                column.setPreferredWidth(145);
            }else if(i==35){
                column.setPreferredWidth(145);
            }else if(i==36){
                column.setPreferredWidth(145);
            }else if(i==37){
                column.setPreferredWidth(145);
            }else if(i==38){
                column.setPreferredWidth(145);
            }else if(i==39){
                column.setPreferredWidth(145);
            }else if(i==40){
                column.setPreferredWidth(145);
            }else if(i==41){
                column.setPreferredWidth(145);
            }else if(i==42){
                column.setPreferredWidth(145);
            }else if(i==43){
                column.setPreferredWidth(145);
            }else if(i==44){
                column.setPreferredWidth(145);
            }else if(i==45){
                column.setPreferredWidth(145);
            }else if(i==45){
                column.setPreferredWidth(145);
            }else if(i==46){
                column.setPreferredWidth(145);
            }else if(i==47){
                column.setPreferredWidth(145);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());

        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
      
        
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){     
                    if(pilihan==1){
                        KdPetugasOK.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        NmPetugasOK.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        btnPetugasOK.requestFocus();
                    }else if(pilihan==4){
                        KdAsisten1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        NmAsisten1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        btnAsisten1.requestFocus();
                    }else if(pilihan==5){
                        KdAsisten2.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        NmAsisten2.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        btnAsisten2.requestFocus();
                    }else if(pilihan==6){
                        KdPrwAnestesi.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        NmPrwAnestesi.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        btnPerawatAnestesi.requestFocus();
                        btnDokterAnestesi.requestFocus();
                    }else if(pilihan==7){
                        KdPerina.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        NmPerina.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        btnBidanPerina.requestFocus();
                    }else if(pilihan==8){
                        KdInstrumen.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        NmInstrumen.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        btnInstrumen.requestFocus();
                    }
                }   
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){      
                    if(pilihan==1){
                        KodeOperator1.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NamaOperator1.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                        btnOperator1.requestFocus();
                    }else if(pilihan==2){
                        KodeOperator2.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NamaOperator2.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                        btnOperator2.requestFocus();
                    }else if(pilihan==3){
                        KodeDokterAnestesi.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NamaDokterAnestesi.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                        btnDokterAnestesi.requestFocus();
                    }else if(pilihan==4){
                        KdAnak.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NmAnak.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                        btnDokterAnestesi.requestFocus();
                    }else if(pilihan==5){
                      
                    }  
                    
                }  
                    
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        carilaporanOK.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(carilaporanOK.getTable().getSelectedRow()!= -1){
                        KodeOperator1.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),8).toString());
                        NamaOperator1.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),9).toString());
                        KodeOperator2.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),10).toString());
                        NamaOperator2.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),11).toString());
                        KdAnak.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),12).toString());
                        NmAnak.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),13).toString());
                        KodeDokterAnestesi.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),14).toString());
                        NamaDokterAnestesi.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),15).toString());
                        KdPetugasOK.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),34).toString());
                        NmPetugasOK.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),35).toString());
                        KdAsisten1.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),20).toString());
                        NmAsisten1.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),21).toString());
                        KdAsisten2.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),22).toString());
                        NmAsisten2.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),23).toString());
                        KdInstrumen.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),24).toString());
                        NmInstrumen.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),25).toString());
                        KdPrwAnestesi.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),28).toString());
                        NmPrwAnestesi.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),29).toString());
                        KdPerina.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),32).toString());
                        NmPerina.setText(carilaporanOK.getTable().getValueAt(carilaporanOK.getTable().getSelectedRow(),33).toString());
                        cpasien_ket_prosedur.requestFocus();
                   
                }   
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        
        carioperatorsignin.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(carioperatorsignin.getTable().getSelectedRow()!= -1){
                        KodeOperator1.setText(carioperatorsignin.getTable().getValueAt(carioperatorsignin.getTable().getSelectedRow(),6).toString());
                        NamaOperator1.setText(carioperatorsignin.getTable().getValueAt(carioperatorsignin.getTable().getSelectedRow(),7).toString());
                        KodeDokterAnestesi.setText(carioperatorsignin.getTable().getValueAt(carioperatorsignin.getTable().getSelectedRow(),8).toString());
                        NamaDokterAnestesi.setText(carioperatorsignin.getTable().getValueAt(carioperatorsignin.getTable().getSelectedRow(),9).toString());
                        KdPrwAnestesi.setText(carioperatorsignin.getTable().getValueAt(carioperatorsignin.getTable().getSelectedRow(),10).toString());
                        NmPrwAnestesi.setText(carioperatorsignin.getTable().getValueAt(carioperatorsignin.getTable().getSelectedRow(),11).toString());
                        KodeOperator2.setText("-");
                        NamaOperator2.setText("-");
                        KdAsisten1.setText("-");
                        NmAsisten1.setText("-");
                        KdAsisten2.setText("-");
                        NmAsisten2.setText("-");
                        KdAnak.setText("-");
                        NmAnak.setText("-");
                        KdPetugasOK.setText("-");
                        NmPetugasOK.setText("-");
                        KdInstrumen.setText("-");
                        NmInstrumen.setText("-");
                        KdPerina.setText("-");
                        NmPerina.setText("-");
                        KdPerina.requestFocus();
                   
                }   
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        ChkInput.setSelected(false);
        isForm();
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
        jam();
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnTimeOutSebelumInsisi = new javax.swing.JMenuItem();
        LoadHTML = new widget.editorpane();
        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        jPanel3 = new javax.swing.JPanel();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        BtnKeluar = new widget.Button();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        PanelInput = new javax.swing.JPanel();
        ChkInput = new widget.CekBox();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        jLabel4 = new widget.Label();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        jLabel16 = new widget.Label();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        jLabel20 = new widget.Label();
        jLabel23 = new widget.Label();
        KodeOperator1 = new widget.TextBox();
        NamaOperator1 = new widget.TextBox();
        btnOperator1 = new widget.Button();
        btnDokterAnestesi = new widget.Button();
        NamaDokterAnestesi = new widget.TextBox();
        KodeDokterAnestesi = new widget.TextBox();
        jLabel24 = new widget.Label();
        jLabel26 = new widget.Label();
        KdPetugasOK = new widget.TextBox();
        NmPetugasOK = new widget.TextBox();
        btnPetugasOK = new widget.Button();
        jLabel5 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel53 = new widget.Label();
        jLabel54 = new widget.Label();
        ctim = new widget.ComboBox();
        jLabel60 = new widget.Label();
        jLabel25 = new widget.Label();
        KodeOperator2 = new widget.TextBox();
        NamaOperator2 = new widget.TextBox();
        btnOperator2 = new widget.Button();
        csterilitas = new widget.ComboBox();
        jLabel63 = new widget.Label();
        jLabel64 = new widget.Label();
        jLabel65 = new widget.Label();
        jLabel66 = new widget.Label();
        ckehilangandarah_ket = new widget.TextBox();
        jLabel67 = new widget.Label();
        jLabel68 = new widget.Label();
        cimaging_ket = new widget.TextBox();
        ckehilangandarah = new widget.ComboBox();
        jLabel61 = new widget.Label();
        cimplan = new widget.ComboBox();
        hal_penting = new widget.TextBox();
        jLabel62 = new widget.Label();
        jLabel69 = new widget.Label();
        cimaging = new widget.ComboBox();
        jLabel70 = new widget.Label();
        cpasien = new widget.ComboBox();
        hal_kritis = new widget.TextBox();
        jLabel27 = new widget.Label();
        KdAsisten1 = new widget.TextBox();
        NmAsisten1 = new widget.TextBox();
        btnAsisten1 = new widget.Button();
        jLabel28 = new widget.Label();
        KdAsisten2 = new widget.TextBox();
        NmAsisten2 = new widget.TextBox();
        btnAsisten2 = new widget.Button();
        jLabel29 = new widget.Label();
        KdPrwAnestesi = new widget.TextBox();
        NmPrwAnestesi = new widget.TextBox();
        btnPerawatAnestesi = new widget.Button();
        DTPtanggal = new widget.Tanggal();
        Jam1 = new widget.ComboBox();
        Menit1 = new widget.ComboBox();
        ChkJam = new widget.CekBox();
        Detik1 = new widget.ComboBox();
        cpasien_ket_prosedur = new widget.TextBox();
        jLabel30 = new widget.Label();
        KdAnak = new widget.TextBox();
        NmAnak = new widget.TextBox();
        btnAsisten3 = new widget.Button();
        jLabel31 = new widget.Label();
        KdPerina = new widget.TextBox();
        NmPerina = new widget.TextBox();
        btnBidanPerina = new widget.Button();
        csterilitas_ket = new widget.TextBox();
        jLabel72 = new widget.Label();
        cantibiotik = new widget.ComboBox();
        cantibiotik_ket = new widget.TextBox();
        Jam2 = new widget.ComboBox();
        Menit2 = new widget.ComboBox();
        Detik2 = new widget.ComboBox();
        jLabel73 = new widget.Label();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        cmasalah = new widget.ComboBox();
        cmasalah_ket = new widget.TextBox();
        cimplan_ket = new widget.TextBox();
        KdInstrumen = new widget.TextBox();
        NmInstrumen = new widget.TextBox();
        btnInstrumen = new widget.Button();
        jSeparator6 = new javax.swing.JSeparator();
        casa = new widget.ComboBox();
        cpasien_ket_tempatinsisi = new widget.TextBox();
        Jam3 = new widget.ComboBox();
        Menit3 = new widget.ComboBox();
        jLabel74 = new widget.Label();
        jLabel75 = new widget.Label();
        jLabel71 = new widget.Label();
        jLabel76 = new widget.Label();
        cjenisanestesi = new widget.ComboBox();
        jLabel77 = new widget.Label();
        jLabel33 = new widget.Label();
        btnDataOperator = new widget.Button();
        label15 = new widget.Label();

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnTimeOutSebelumInsisi.setBackground(new java.awt.Color(255, 255, 254));
        MnTimeOutSebelumInsisi.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnTimeOutSebelumInsisi.setForeground(new java.awt.Color(50, 50, 50));
        MnTimeOutSebelumInsisi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnTimeOutSebelumInsisi.setText("Formulir Time-Out Sebelum Tindakan Insisi");
        MnTimeOutSebelumInsisi.setName("MnTimeOutSebelumInsisi"); // NOI18N
        MnTimeOutSebelumInsisi.setPreferredSize(new java.awt.Dimension(270, 26));
        MnTimeOutSebelumInsisi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnTimeOutSebelumInsisiActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnTimeOutSebelumInsisi);

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Time-Out Sebelum Tindakan Insisi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(44, 100));
        jPanel3.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass8.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass8.add(LCount);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        jPanel3.add(panelGlass8, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tanggal :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "18-09-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "18-09-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(310, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnAll);

        jPanel3.add(panelGlass9, java.awt.BorderLayout.PAGE_START);

        internalFrame1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        PanelInput.setName("PanelInput"); // NOI18N
        PanelInput.setOpaque(false);
        PanelInput.setPreferredSize(new java.awt.Dimension(192, 406));
        PanelInput.setLayout(new java.awt.BorderLayout(1, 1));

        ChkInput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setMnemonic('I');
        ChkInput.setText(".: Input Data");
        ChkInput.setToolTipText("Alt+I");
        ChkInput.setBorderPainted(true);
        ChkInput.setBorderPaintedFlat(true);
        ChkInput.setFocusable(false);
        ChkInput.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ChkInput.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ChkInput.setName("ChkInput"); // NOI18N
        ChkInput.setPreferredSize(new java.awt.Dimension(192, 20));
        ChkInput.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkInputActionPerformed(evt);
            }
        });
        PanelInput.add(ChkInput, java.awt.BorderLayout.PAGE_END);

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(250, 255, 245));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(100, 980));
        FormInput.setLayout(null);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("No.Rawat");
        jLabel4.setName("jLabel4"); // NOI18N
        FormInput.add(jLabel4);
        jLabel4.setBounds(21, 10, 75, 23);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(90, 10, 130, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        TPasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TPasienKeyPressed(evt);
            }
        });
        FormInput.add(TPasien);
        TPasien.setBounds(336, 10, 285, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        TNoRM.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRMKeyPressed(evt);
            }
        });
        FormInput.add(TNoRM);
        TNoRM.setBounds(222, 10, 112, 23);

        jLabel16.setText("Tanggal :");
        jLabel16.setName("jLabel16"); // NOI18N
        jLabel16.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel16);
        jLabel16.setBounds(20, 220, 60, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(625, 10, 60, 23);

        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(689, 10, 100, 23);

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setText("Dilakukan sebelum insisi, dihadiri minimal oleh perawat, ahli anestesi , operator I &  II");
        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setName("jLabel20"); // NOI18N
        FormInput.add(jLabel20);
        jLabel20.setBounds(20, 260, 500, 23);

        jLabel23.setText("Operator 1 :");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(-10, 40, 91, 23);

        KodeOperator1.setEditable(false);
        KodeOperator1.setHighlighter(null);
        KodeOperator1.setName("KodeOperator1"); // NOI18N
        FormInput.add(KodeOperator1);
        KodeOperator1.setBounds(90, 40, 97, 23);

        NamaOperator1.setEditable(false);
        NamaOperator1.setName("NamaOperator1"); // NOI18N
        FormInput.add(NamaOperator1);
        NamaOperator1.setBounds(190, 40, 175, 23);

        btnOperator1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnOperator1.setMnemonic('2');
        btnOperator1.setToolTipText("ALt+2");
        btnOperator1.setName("btnOperator1"); // NOI18N
        btnOperator1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOperator1ActionPerformed(evt);
            }
        });
        btnOperator1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnOperator1KeyPressed(evt);
            }
        });
        FormInput.add(btnOperator1);
        btnOperator1.setBounds(370, 40, 28, 23);

        btnDokterAnestesi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnDokterAnestesi.setMnemonic('2');
        btnDokterAnestesi.setToolTipText("ALt+2");
        btnDokterAnestesi.setName("btnDokterAnestesi"); // NOI18N
        btnDokterAnestesi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDokterAnestesiActionPerformed(evt);
            }
        });
        btnDokterAnestesi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnDokterAnestesiKeyPressed(evt);
            }
        });
        FormInput.add(btnDokterAnestesi);
        btnDokterAnestesi.setBounds(780, 40, 28, 23);

        NamaDokterAnestesi.setEditable(false);
        NamaDokterAnestesi.setName("NamaDokterAnestesi"); // NOI18N
        FormInput.add(NamaDokterAnestesi);
        NamaDokterAnestesi.setBounds(600, 40, 180, 23);

        KodeDokterAnestesi.setEditable(false);
        KodeDokterAnestesi.setHighlighter(null);
        KodeDokterAnestesi.setName("KodeDokterAnestesi"); // NOI18N
        FormInput.add(KodeDokterAnestesi);
        KodeDokterAnestesi.setBounds(500, 40, 97, 23);

        jLabel24.setText("Dokter Anestesi :");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(410, 40, 83, 23);

        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel26.setText("Perawat Sirkuler :");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(410, 70, 90, 23);

        KdPetugasOK.setEditable(false);
        KdPetugasOK.setHighlighter(null);
        KdPetugasOK.setName("KdPetugasOK"); // NOI18N
        FormInput.add(KdPetugasOK);
        KdPetugasOK.setBounds(500, 70, 100, 23);

        NmPetugasOK.setEditable(false);
        NmPetugasOK.setName("NmPetugasOK"); // NOI18N
        FormInput.add(NmPetugasOK);
        NmPetugasOK.setBounds(600, 70, 180, 23);

        btnPetugasOK.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnPetugasOK.setMnemonic('2');
        btnPetugasOK.setToolTipText("ALt+2");
        btnPetugasOK.setName("btnPetugasOK"); // NOI18N
        btnPetugasOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPetugasOKActionPerformed(evt);
            }
        });
        btnPetugasOK.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPetugasOKKeyPressed(evt);
            }
        });
        FormInput.add(btnPetugasOK);
        btnPetugasOK.setBounds(780, 70, 28, 23);

        jLabel5.setText(":");
        jLabel5.setName("jLabel5"); // NOI18N
        FormInput.add(jLabel5);
        jLabel5.setBounds(0, 10, 75, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 257, 810, 3);

        jSeparator3.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator3.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator3.setName("jSeparator3"); // NOI18N
        FormInput.add(jSeparator3);
        jSeparator3.setBounds(0, 417, 810, 0);

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel53.setText("ANTISIPASI KEJADIAN KRITIS");
        jLabel53.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel53.setName("jLabel53"); // NOI18N
        FormInput.add(jLabel53);
        jLabel53.setBounds(20, 500, 170, 23);

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("Seluruh anggota tim telah menyebutkan nama dan peran masing-masing");
        jLabel54.setName("jLabel54"); // NOI18N
        FormInput.add(jLabel54);
        jLabel54.setBounds(20, 290, 350, 23);

        ctim.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        ctim.setName("ctim"); // NOI18N
        ctim.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ctimKeyPressed(evt);
            }
        });
        FormInput.add(ctim);
        ctim.setBounds(380, 290, 80, 23);

        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel60.setText("Sudahkah sterilitas dipastikan (termasuk hasil indikator) ?");
        jLabel60.setName("jLabel60"); // NOI18N
        FormInput.add(jLabel60);
        jLabel60.setBounds(20, 770, 280, 23);

        jLabel25.setText("Operator 2 :");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(-10, 70, 91, 23);

        KodeOperator2.setEditable(false);
        KodeOperator2.setHighlighter(null);
        KodeOperator2.setName("KodeOperator2"); // NOI18N
        FormInput.add(KodeOperator2);
        KodeOperator2.setBounds(90, 70, 97, 23);

        NamaOperator2.setEditable(false);
        NamaOperator2.setName("NamaOperator2"); // NOI18N
        FormInput.add(NamaOperator2);
        NamaOperator2.setBounds(190, 70, 175, 23);

        btnOperator2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnOperator2.setMnemonic('2');
        btnOperator2.setToolTipText("ALt+2");
        btnOperator2.setName("btnOperator2"); // NOI18N
        btnOperator2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOperator2ActionPerformed(evt);
            }
        });
        btnOperator2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnOperator2KeyPressed(evt);
            }
        });
        FormInput.add(btnOperator2);
        btnOperator2.setBounds(370, 70, 28, 23);

        csterilitas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        csterilitas.setName("csterilitas"); // NOI18N
        csterilitas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                csterilitasKeyPressed(evt);
            }
        });
        FormInput.add(csterilitas);
        csterilitas.setBounds(380, 770, 80, 23);

        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel63.setText("Antibiotik profilaksis telah diberikan dalam 60 menit ?");
        jLabel63.setName("jLabel63"); // NOI18N
        FormInput.add(jLabel63);
        jLabel63.setBounds(20, 420, 350, 23);

        jLabel64.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel64.setText("Tim Keperawatan");
        jLabel64.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel64.setName("jLabel64"); // NOI18N
        FormInput.add(jLabel64);
        jLabel64.setBounds(20, 750, 170, 23);

        jLabel65.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel65.setText("Operator");
        jLabel65.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel65.setName("jLabel65"); // NOI18N
        FormInput.add(jLabel65);
        jLabel65.setBounds(20, 520, 60, 23);

        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel66.setText("Hal kritis atau langkah tak terduga apakah yang mungkin diambil ?");
        jLabel66.setName("jLabel66"); // NOI18N
        FormInput.add(jLabel66);
        jLabel66.setBounds(20, 540, 350, 23);

        ckehilangandarah_ket.setHighlighter(null);
        ckehilangandarah_ket.setName("ckehilangandarah_ket"); // NOI18N
        ckehilangandarah_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ckehilangandarah_ketKeyPressed(evt);
            }
        });
        FormInput.add(ckehilangandarah_ket);
        ckehilangandarah_ket.setBounds(470, 600, 330, 23);

        jLabel67.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel67.setText("Tim Anestesi");
        jLabel67.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel67.setName("jLabel67"); // NOI18N
        FormInput.add(jLabel67);
        jLabel67.setBounds(20, 640, 170, 23);

        jLabel68.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel68.setText("Jenis Anestesi :");
        jLabel68.setName("jLabel68"); // NOI18N
        FormInput.add(jLabel68);
        jLabel68.setBounds(490, 660, 80, 23);

        cimaging_ket.setHighlighter(null);
        cimaging_ket.setName("cimaging_ket"); // NOI18N
        cimaging_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cimaging_ketKeyPressed(evt);
            }
        });
        FormInput.add(cimaging_ket);
        cimaging_ket.setBounds(470, 880, 330, 23);

        ckehilangandarah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        ckehilangandarah.setName("ckehilangandarah"); // NOI18N
        ckehilangandarah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ckehilangandarahKeyPressed(evt);
            }
        });
        FormInput.add(ckehilangandarah);
        ckehilangandarah.setBounds(380, 600, 80, 23);

        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel61.setText("Apakah implan sudah tersedia dan sesuai ?");
        jLabel61.setName("jLabel61"); // NOI18N
        FormInput.add(jLabel61);
        jLabel61.setBounds(20, 830, 310, 23);

        cimplan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        cimplan.setName("cimplan"); // NOI18N
        cimplan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cimplanKeyPressed(evt);
            }
        });
        FormInput.add(cimplan);
        cimplan.setBounds(380, 830, 80, 23);

        hal_penting.setHighlighter(null);
        hal_penting.setName("hal_penting"); // NOI18N
        hal_penting.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                hal_pentingKeyPressed(evt);
            }
        });
        FormInput.add(hal_penting);
        hal_penting.setBounds(380, 700, 420, 23);

        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel62.setText("Adakah masalah atau perhatian khusus mengenai peralatan ?");
        jLabel62.setName("jLabel62"); // NOI18N
        FormInput.add(jLabel62);
        jLabel62.setBounds(20, 800, 310, 23);

        jLabel69.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel69.setText("Hasil pemeriksaan imaging penting ditampilkan ?");
        jLabel69.setName("jLabel69"); // NOI18N
        FormInput.add(jLabel69);
        jLabel69.setBounds(20, 880, 310, 23);

        cimaging.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak Dapat Diterapkan" }));
        cimaging.setName("cimaging"); // NOI18N
        cimaging.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cimagingKeyPressed(evt);
            }
        });
        FormInput.add(cimaging);
        cimaging.setBounds(380, 880, 80, 23);

        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel70.setText("Tempat insisi :");
        jLabel70.setName("jLabel70"); // NOI18N
        FormInput.add(jLabel70);
        jLabel70.setBounds(280, 380, 70, 23);

        cpasien.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        cpasien.setName("cpasien"); // NOI18N
        cpasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cpasienKeyPressed(evt);
            }
        });
        FormInput.add(cpasien);
        cpasien.setBounds(380, 320, 80, 23);

        hal_kritis.setHighlighter(null);
        hal_kritis.setName("hal_kritis"); // NOI18N
        hal_kritis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                hal_kritisKeyPressed(evt);
            }
        });
        FormInput.add(hal_kritis);
        hal_kritis.setBounds(380, 540, 420, 23);

        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel27.setText("Asisten 1 :");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(30, 110, 60, 23);

        KdAsisten1.setEditable(false);
        KdAsisten1.setHighlighter(null);
        KdAsisten1.setName("KdAsisten1"); // NOI18N
        FormInput.add(KdAsisten1);
        KdAsisten1.setBounds(90, 110, 100, 23);

        NmAsisten1.setEditable(false);
        NmAsisten1.setName("NmAsisten1"); // NOI18N
        FormInput.add(NmAsisten1);
        NmAsisten1.setBounds(190, 110, 180, 23);

        btnAsisten1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnAsisten1.setMnemonic('2');
        btnAsisten1.setToolTipText("ALt+2");
        btnAsisten1.setName("btnAsisten1"); // NOI18N
        btnAsisten1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAsisten1ActionPerformed(evt);
            }
        });
        btnAsisten1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAsisten1KeyPressed(evt);
            }
        });
        FormInput.add(btnAsisten1);
        btnAsisten1.setBounds(370, 110, 28, 23);

        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel28.setText("Asisten 2 :");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(30, 140, 60, 23);

        KdAsisten2.setEditable(false);
        KdAsisten2.setHighlighter(null);
        KdAsisten2.setName("KdAsisten2"); // NOI18N
        FormInput.add(KdAsisten2);
        KdAsisten2.setBounds(90, 140, 100, 23);

        NmAsisten2.setEditable(false);
        NmAsisten2.setName("NmAsisten2"); // NOI18N
        FormInput.add(NmAsisten2);
        NmAsisten2.setBounds(190, 140, 180, 23);

        btnAsisten2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnAsisten2.setMnemonic('2');
        btnAsisten2.setToolTipText("ALt+2");
        btnAsisten2.setName("btnAsisten2"); // NOI18N
        btnAsisten2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAsisten2ActionPerformed(evt);
            }
        });
        btnAsisten2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAsisten2KeyPressed(evt);
            }
        });
        FormInput.add(btnAsisten2);
        btnAsisten2.setBounds(370, 140, 28, 23);

        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel29.setText("Perawat Anestesi :");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(400, 110, 110, 23);

        KdPrwAnestesi.setEditable(false);
        KdPrwAnestesi.setHighlighter(null);
        KdPrwAnestesi.setName("KdPrwAnestesi"); // NOI18N
        FormInput.add(KdPrwAnestesi);
        KdPrwAnestesi.setBounds(500, 110, 100, 23);

        NmPrwAnestesi.setEditable(false);
        NmPrwAnestesi.setName("NmPrwAnestesi"); // NOI18N
        FormInput.add(NmPrwAnestesi);
        NmPrwAnestesi.setBounds(600, 110, 180, 23);

        btnPerawatAnestesi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnPerawatAnestesi.setMnemonic('2');
        btnPerawatAnestesi.setToolTipText("ALt+2");
        btnPerawatAnestesi.setName("btnPerawatAnestesi"); // NOI18N
        btnPerawatAnestesi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPerawatAnestesiActionPerformed(evt);
            }
        });
        btnPerawatAnestesi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPerawatAnestesiKeyPressed(evt);
            }
        });
        FormInput.add(btnPerawatAnestesi);
        btnPerawatAnestesi.setBounds(780, 110, 28, 23);

        DTPtanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "18-09-2024" }));
        DTPtanggal.setDisplayFormat("dd-MM-yyyy");
        DTPtanggal.setName("DTPtanggal"); // NOI18N
        DTPtanggal.setOpaque(false);
        DTPtanggal.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput.add(DTPtanggal);
        DTPtanggal.setBounds(90, 220, 100, 23);

        Jam1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam1.setName("Jam1"); // NOI18N
        Jam1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam1ActionPerformed(evt);
            }
        });
        Jam1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam1KeyPressed(evt);
            }
        });
        FormInput.add(Jam1);
        Jam1.setBounds(200, 220, 50, 23);

        Menit1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit1.setName("Menit1"); // NOI18N
        Menit1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit1KeyPressed(evt);
            }
        });
        FormInput.add(Menit1);
        Menit1.setBounds(260, 220, 50, 23);

        ChkJam.setBorder(null);
        ChkJam.setSelected(true);
        ChkJam.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam.setName("ChkJam"); // NOI18N
        FormInput.add(ChkJam);
        ChkJam.setBounds(370, 220, 23, 23);

        Detik1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik1.setName("Detik1"); // NOI18N
        Detik1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik1KeyPressed(evt);
            }
        });
        FormInput.add(Detik1);
        Detik1.setBounds(320, 220, 50, 23);

        cpasien_ket_prosedur.setHighlighter(null);
        cpasien_ket_prosedur.setName("cpasien_ket_prosedur"); // NOI18N
        cpasien_ket_prosedur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cpasien_ket_prosedurKeyPressed(evt);
            }
        });
        FormInput.add(cpasien_ket_prosedur);
        cpasien_ket_prosedur.setBounds(380, 350, 420, 23);

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel30.setText("Dokter Anak :");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(10, 180, 90, 23);

        KdAnak.setEditable(false);
        KdAnak.setHighlighter(null);
        KdAnak.setName("KdAnak"); // NOI18N
        FormInput.add(KdAnak);
        KdAnak.setBounds(90, 180, 100, 23);

        NmAnak.setEditable(false);
        NmAnak.setName("NmAnak"); // NOI18N
        FormInput.add(NmAnak);
        NmAnak.setBounds(190, 180, 180, 23);

        btnAsisten3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnAsisten3.setMnemonic('2');
        btnAsisten3.setToolTipText("ALt+2");
        btnAsisten3.setName("btnAsisten3"); // NOI18N
        btnAsisten3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAsisten3ActionPerformed(evt);
            }
        });
        btnAsisten3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAsisten3KeyPressed(evt);
            }
        });
        FormInput.add(btnAsisten3);
        btnAsisten3.setBounds(370, 180, 28, 23);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("Instrumen :");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(430, 140, 60, 23);

        KdPerina.setEditable(false);
        KdPerina.setHighlighter(null);
        KdPerina.setName("KdPerina"); // NOI18N
        FormInput.add(KdPerina);
        KdPerina.setBounds(500, 180, 100, 23);

        NmPerina.setEditable(false);
        NmPerina.setName("NmPerina"); // NOI18N
        FormInput.add(NmPerina);
        NmPerina.setBounds(600, 180, 180, 23);

        btnBidanPerina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnBidanPerina.setMnemonic('2');
        btnBidanPerina.setToolTipText("ALt+2");
        btnBidanPerina.setName("btnBidanPerina"); // NOI18N
        btnBidanPerina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBidanPerinaActionPerformed(evt);
            }
        });
        btnBidanPerina.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnBidanPerinaKeyPressed(evt);
            }
        });
        FormInput.add(btnBidanPerina);
        btnBidanPerina.setBounds(780, 180, 28, 23);

        csterilitas_ket.setHighlighter(null);
        csterilitas_ket.setName("csterilitas_ket"); // NOI18N
        csterilitas_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                csterilitas_ketKeyPressed(evt);
            }
        });
        FormInput.add(csterilitas_ket);
        csterilitas_ket.setBounds(470, 770, 330, 23);

        jLabel72.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel72.setText("Antisipasi kehilangan darah yang dipersiapkan ?");
        jLabel72.setName("jLabel72"); // NOI18N
        FormInput.add(jLabel72);
        jLabel72.setBounds(20, 600, 350, 23);

        cantibiotik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak Dapat Diterapkan" }));
        cantibiotik.setName("cantibiotik"); // NOI18N
        cantibiotik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cantibiotikKeyPressed(evt);
            }
        });
        FormInput.add(cantibiotik);
        cantibiotik.setBounds(380, 420, 80, 23);

        cantibiotik_ket.setHighlighter(null);
        cantibiotik_ket.setName("cantibiotik_ket"); // NOI18N
        cantibiotik_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cantibiotik_ketKeyPressed(evt);
            }
        });
        FormInput.add(cantibiotik_ket);
        cantibiotik_ket.setBounds(380, 450, 420, 23);

        Jam2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam2.setName("Jam2"); // NOI18N
        Jam2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam2ActionPerformed(evt);
            }
        });
        Jam2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam2KeyPressed(evt);
            }
        });
        FormInput.add(Jam2);
        Jam2.setBounds(470, 420, 50, 23);

        Menit2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit2.setName("Menit2"); // NOI18N
        Menit2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit2KeyPressed(evt);
            }
        });
        FormInput.add(Menit2);
        Menit2.setBounds(530, 420, 50, 23);

        Detik2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik2.setName("Detik2"); // NOI18N
        Detik2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik2KeyPressed(evt);
            }
        });
        FormInput.add(Detik2);
        Detik2.setBounds(590, 420, 50, 23);

        jLabel73.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel73.setText("Menit");
        jLabel73.setName("jLabel73"); // NOI18N
        FormInput.add(jLabel73);
        jLabel73.setBounds(550, 570, 30, 23);

        jSeparator2.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator2.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator2.setName("jSeparator2"); // NOI18N
        FormInput.add(jSeparator2);
        jSeparator2.setBounds(0, 480, 810, 3);

        jSeparator4.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator4.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator4.setName("jSeparator4"); // NOI18N
        FormInput.add(jSeparator4);
        jSeparator4.setBounds(0, 630, 810, 3);

        jSeparator5.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator5.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator5.setName("jSeparator5"); // NOI18N
        FormInput.add(jSeparator5);
        jSeparator5.setBounds(0, 730, 810, 3);

        cmasalah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        cmasalah.setName("cmasalah"); // NOI18N
        cmasalah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmasalahKeyPressed(evt);
            }
        });
        FormInput.add(cmasalah);
        cmasalah.setBounds(380, 800, 80, 23);

        cmasalah_ket.setHighlighter(null);
        cmasalah_ket.setName("cmasalah_ket"); // NOI18N
        cmasalah_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmasalah_ketKeyPressed(evt);
            }
        });
        FormInput.add(cmasalah_ket);
        cmasalah_ket.setBounds(470, 800, 330, 23);

        cimplan_ket.setHighlighter(null);
        cimplan_ket.setName("cimplan_ket"); // NOI18N
        cimplan_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cimplan_ketKeyPressed(evt);
            }
        });
        FormInput.add(cimplan_ket);
        cimplan_ket.setBounds(470, 830, 330, 23);

        KdInstrumen.setEditable(false);
        KdInstrumen.setHighlighter(null);
        KdInstrumen.setName("KdInstrumen"); // NOI18N
        FormInput.add(KdInstrumen);
        KdInstrumen.setBounds(500, 140, 100, 23);

        NmInstrumen.setEditable(false);
        NmInstrumen.setName("NmInstrumen"); // NOI18N
        FormInput.add(NmInstrumen);
        NmInstrumen.setBounds(600, 140, 180, 23);

        btnInstrumen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnInstrumen.setMnemonic('2');
        btnInstrumen.setToolTipText("ALt+2");
        btnInstrumen.setName("btnInstrumen"); // NOI18N
        btnInstrumen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInstrumenActionPerformed(evt);
            }
        });
        btnInstrumen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnInstrumenKeyPressed(evt);
            }
        });
        FormInput.add(btnInstrumen);
        btnInstrumen.setBounds(780, 140, 28, 23);

        jSeparator6.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator6.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator6.setName("jSeparator6"); // NOI18N
        FormInput.add(jSeparator6);
        jSeparator6.setBounds(0, 870, 810, 3);

        casa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "ASA I", "ASA II", "ASA III", "ASA IV", "ASA V", "ASA VI" }));
        casa.setName("casa"); // NOI18N
        casa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                casaKeyPressed(evt);
            }
        });
        FormInput.add(casa);
        casa.setBounds(380, 660, 80, 23);

        cpasien_ket_tempatinsisi.setHighlighter(null);
        cpasien_ket_tempatinsisi.setName("cpasien_ket_tempatinsisi"); // NOI18N
        cpasien_ket_tempatinsisi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cpasien_ket_tempatinsisiKeyPressed(evt);
            }
        });
        FormInput.add(cpasien_ket_tempatinsisi);
        cpasien_ket_tempatinsisi.setBounds(380, 380, 420, 23);

        Jam3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "1", "2", "3", "4", "5" }));
        Jam3.setName("Jam3"); // NOI18N
        Jam3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam3ActionPerformed(evt);
            }
        });
        Jam3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam3KeyPressed(evt);
            }
        });
        FormInput.add(Jam3);
        Jam3.setBounds(380, 570, 50, 23);

        Menit3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit3.setName("Menit3"); // NOI18N
        Menit3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit3KeyPressed(evt);
            }
        });
        FormInput.add(Menit3);
        Menit3.setBounds(490, 570, 50, 23);

        jLabel74.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel74.setText("Berapa estimasi lama operasi ?");
        jLabel74.setName("jLabel74"); // NOI18N
        FormInput.add(jLabel74);
        jLabel74.setBounds(20, 570, 350, 23);

        jLabel75.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel75.setText("Jam");
        jLabel75.setName("jLabel75"); // NOI18N
        FormInput.add(jLabel75);
        jLabel75.setBounds(440, 570, 30, 23);

        jLabel71.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel71.setText("Pastikan nama pasien / nama prosedur / dimana insisi akan dilakukan");
        jLabel71.setName("jLabel71"); // NOI18N
        FormInput.add(jLabel71);
        jLabel71.setBounds(20, 320, 350, 23);

        jLabel76.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel76.setText("Tindakan :");
        jLabel76.setName("jLabel76"); // NOI18N
        FormInput.add(jLabel76);
        jLabel76.setBounds(300, 350, 50, 23);

        cjenisanestesi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "LA", "SAB", "EPIDURAL", "GA ETT", "GA LMA", "GA FACEMASK", "GA TIVA" }));
        cjenisanestesi.setName("cjenisanestesi"); // NOI18N
        cjenisanestesi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cjenisanestesiKeyPressed(evt);
            }
        });
        FormInput.add(cjenisanestesi);
        cjenisanestesi.setBounds(580, 660, 140, 20);

        jLabel77.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel77.setText("Adakah terdapat hal penting mengenai pasien yang perlu diperhatikan ?");
        jLabel77.setName("jLabel77"); // NOI18N
        FormInput.add(jLabel77);
        jLabel77.setBounds(20, 660, 350, 23);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel33.setText("Bidan Perina :");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(420, 180, 80, 23);

        btnDataOperator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnDataOperator.setMnemonic('2');
        btnDataOperator.setToolTipText("Alt+2");
        btnDataOperator.setName("btnDataOperator"); // NOI18N
        btnDataOperator.setPreferredSize(new java.awt.Dimension(28, 23));
        btnDataOperator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDataOperatorActionPerformed(evt);
            }
        });
        FormInput.add(btnDataOperator);
        btnDataOperator.setBounds(530, 220, 28, 23);

        label15.setText("Cari Operator Sign In :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label15);
        label15.setBounds(370, 220, 160, 23);

        scrollInput.setViewportView(FormInput);

        PanelInput.add(scrollInput, java.awt.BorderLayout.CENTER);

        internalFrame1.add(PanelInput, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
            isPsien();
        }else{            
            //Valid.pindah(evt,TCari,Tanggal);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void TPasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TPasienKeyPressed
        Valid.pindah(evt,TCari,BtnSimpan);
}//GEN-LAST:event_TPasienKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Pasien");
        }else if(KodeOperator1.getText().trim().equals("")||NamaOperator1.getText().trim().equals("")){
            Valid.textKosong(btnOperator1,"Dokter Operator 1");
        }else if(KodeOperator2.getText().trim().equals("")||NamaOperator2.getText().trim().equals("")){
            Valid.textKosong(btnOperator2,"Dokter Operator 2");
        }else if(KodeDokterAnestesi.getText().trim().equals("")||NamaDokterAnestesi.getText().trim().equals("")){
            Valid.textKosong(KodeDokterAnestesi,"Dokter Anestesi");
        }else if(KdPetugasOK.getText().trim().equals("")||NmPetugasOK.getText().trim().equals("")){
            Valid.textKosong(KdPetugasOK,"Perawat");
        }else if(KdAnak.getText().trim().equals("")||NmAnak.getText().trim().equals("")){
            Valid.textKosong(KdAnak,"Dokter Anak");
        }else if(KdPerina.getText().trim().equals("")||NmPerina.getText().trim().equals("")){
            Valid.textKosong(KdPerina,"Bidan Perina");
        }else if(KdInstrumen.getText().trim().equals("")||NmInstrumen.getText().trim().equals("")){
            Valid.textKosong(KdInstrumen,"Instrumen");
        }else if(KdAsisten1.getText().trim().equals("")||NmAsisten1.getText().trim().equals("")){
            Valid.textKosong(KdAsisten1,"Asisten 1");
        }else if(KdAsisten2.getText().trim().equals("")||NmAsisten2.getText().trim().equals("")){
            Valid.textKosong(KdAsisten2,"Asisten 2");
        }else if(KdPrwAnestesi.getText().trim().equals("")||NmPrwAnestesi.getText().trim().equals("")){
            Valid.textKosong(KdAsisten1,"Perawat Anestesi");
        }else{
            if(Sequel.menyimpantf("timeout_sebelum_insisi","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Data",36,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(DTPtanggal.getSelectedItem()+""),
                Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                KodeOperator1.getText(),
                KodeOperator2.getText(),
                KodeDokterAnestesi.getText(),
                KdPetugasOK.getText(),
                KdAsisten1.getText(),
                KdAsisten2.getText(),
                KdPrwAnestesi.getText(),
                KdAnak.getText(),
                KdPerina.getText(),
                KdInstrumen.getText(),
                ctim.getSelectedItem().toString(),
                cpasien.getSelectedItem().toString(),
                cpasien_ket_prosedur.getText(),
                cpasien_ket_tempatinsisi.getText(),
                cantibiotik.getSelectedItem().toString(),
                Jam2.getSelectedItem()+":"+Menit2.getSelectedItem()+":"+Detik2.getSelectedItem(),
                cantibiotik_ket.getText(),
                hal_kritis.getText(),
                Jam3.getSelectedItem().toString(),
                Menit3.getSelectedItem().toString(),
                ckehilangandarah.getSelectedItem().toString(),
                ckehilangandarah_ket.getText(),
                casa.getSelectedItem().toString(),
                cjenisanestesi.getSelectedItem().toString(),
                hal_penting.getText(),
                csterilitas.getSelectedItem().toString(),
                csterilitas_ket.getText(),
                cmasalah.getSelectedItem().toString(),
                cmasalah_ket.getText(),
                cimplan.getSelectedItem().toString(),
                cimplan_ket.getText(),
                cimaging.getSelectedItem().toString(),
                cimaging_ket.getText()
            })==true){
                tampil();
                emptTeks();
            } 
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,btnPetugasOK,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        ChkInput.setSelected(true);
        isForm(); 
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else { 
                hapus();
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }   
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(KodeOperator1.getText().trim().equals("")||NamaOperator1.getText().trim().equals("")){
            Valid.textKosong(btnOperator1,"Dokter Bedah");
        }else if(KodeDokterAnestesi.getText().trim().equals("")||NamaDokterAnestesi.getText().trim().equals("")){
            Valid.textKosong(KodeDokterAnestesi,"Dokter Anestesi");
        }else if(KdAnak.getText().trim().equals("")||NmAnak.getText().trim().equals("")){
            Valid.textKosong(KdAnak,"Dokter Anak");
        }else if(KdPerina.getText().trim().equals("")||NmPerina.getText().trim().equals("")){
            Valid.textKosong(KdPerina,"Bidan Perina");
        }else if(KdInstrumen.getText().trim().equals("")||NmInstrumen.getText().trim().equals("")){
            Valid.textKosong(KdInstrumen,"Instrumen");
        }else if(KdAsisten1.getText().trim().equals("")||NmAsisten1.getText().trim().equals("")){
            Valid.textKosong(KdAsisten1,"Asisten 1");
        }else if(KdAsisten2.getText().trim().equals("")||NmAsisten2.getText().trim().equals("")){
            Valid.textKosong(KdAsisten2,"Asisten 2");
        }else if(KdPrwAnestesi.getText().trim().equals("")||NmPrwAnestesi.getText().trim().equals("")){
            Valid.textKosong(KdAsisten1,"Perawat Anestesi");
        }else{  
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else {
                    ganti();
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        petugas.dispose();
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
       
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            tampil();
            TCari.setText("");
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void TNoRMKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRMKeyPressed
        // Valid.pindah(evt, TNm, BtnSimpan);
}//GEN-LAST:event_TNoRMKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void MnTimeOutSebelumInsisiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnTimeOutSebelumInsisiActionPerformed
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),42).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),43).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),42).toString():finger)+"\n"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+" "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()); 
            finger2=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),42).toString());
            param.put("finger2","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),43).toString()+"\nID "+(finger2.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),42).toString():finger2)+"\n"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+" "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()); 
            Valid.MyReportqry("rptFormulirTimeOutSebelumInsisi.jasper","report","::[ Formulir Time-Out Sebelum Tindakan Insisi ]::",
                    "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                    "timeout_sebelum_insisi.*,d1.kd_dokter as kd_operator1,d1.nm_dokter as nm_operator1,d2.kd_dokter as kd_operator2,d2.nm_dokter as nm_operator2, "+
                    "d3.kd_dokter as kd_anestesi,d3.nm_dokter as nm_anestesi,d4.kd_dokter as kd_anak,d4.nm_dokter as nm_anak,d5.nip as kd_perinatologi,d5.nama as nm_perinatologi,d6.nip as kd_instrumen,d6.nama as nm_instrumen,a0.nip as kd_petugas,a0.nama as nm_petugas, "+
                    "a1.nip as kd_asisten1,a1.nama as nm_asisten1,a2.nip as kd_asisten2,a2.nama as nm_asisten2,pa.nip as kd_prw_anestesi,pa.nama as nm_prw_anestesi "+
                    "from timeout_sebelum_insisi inner join reg_periksa on timeout_sebelum_insisi.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter as d1 on d1.kd_dokter=timeout_sebelum_insisi.kd_operator1 "+
                    "inner join dokter as d2 on d2.kd_dokter=timeout_sebelum_insisi.kd_operator2 "+
                    "inner join dokter as d3 on d3.kd_dokter=timeout_sebelum_insisi.kd_anestesi "+
                    "inner join dokter as d4 on d4.kd_dokter=timeout_sebelum_insisi.kd_anak "+
                    "inner join petugas as d5 on d5.nip=timeout_sebelum_insisi.kd_perinatologi "+
                    "inner join petugas as d6 on d6.nip=timeout_sebelum_insisi.kd_instrumen "+
                    "inner join petugas as a0 on a0.nip=timeout_sebelum_insisi.kd_petugas "+
                    "inner join petugas as a1 on a1.nip=timeout_sebelum_insisi.kd_asisten1 "+
                    "inner join petugas as a2 on a2.nip=timeout_sebelum_insisi.kd_asisten2 "+
                    "inner join petugas as pa on pa.nip=timeout_sebelum_insisi.kd_prw_anestesi "+
                    "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                    "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                    "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                    "where timeout_sebelum_insisi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and timeout_sebelum_insisi.tanggal='"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+"' and timeout_sebelum_insisi.jam='"+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"' ",param);
        }
    }//GEN-LAST:event_MnTimeOutSebelumInsisiActionPerformed

    private void ChkInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkInputActionPerformed
        isForm();
    }//GEN-LAST:event_ChkInputActionPerformed

    private void btnOperator1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOperator1ActionPerformed
        pilihan=1;
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_btnOperator1ActionPerformed

    private void btnOperator1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnOperator1KeyPressed
       
    }//GEN-LAST:event_btnOperator1KeyPressed

    private void btnDokterAnestesiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDokterAnestesiActionPerformed
        pilihan=3;
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_btnDokterAnestesiActionPerformed

    private void btnDokterAnestesiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnDokterAnestesiKeyPressed
        Valid.pindah(evt,btnOperator1,ctim);
    }//GEN-LAST:event_btnDokterAnestesiKeyPressed

    private void btnPetugasOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPetugasOKActionPerformed
        pilihan=1;
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_btnPetugasOKActionPerformed

    private void btnPetugasOKKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPetugasOKKeyPressed
      
    }//GEN-LAST:event_btnPetugasOKKeyPressed

    private void ctimKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ctimKeyPressed
  
    }//GEN-LAST:event_ctimKeyPressed

    private void btnOperator2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOperator2ActionPerformed
        pilihan=2;
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);        // TODO add your handling code here:
    }//GEN-LAST:event_btnOperator2ActionPerformed

    private void btnOperator2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnOperator2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnOperator2KeyPressed

    private void csterilitasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_csterilitasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_csterilitasKeyPressed

    private void ckehilangandarah_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ckehilangandarah_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ckehilangandarah_ketKeyPressed

    private void cimaging_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cimaging_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cimaging_ketKeyPressed

    private void ckehilangandarahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ckehilangandarahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ckehilangandarahKeyPressed

    private void cimplanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cimplanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cimplanKeyPressed

    private void hal_pentingKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_hal_pentingKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_hal_pentingKeyPressed

    private void cimagingKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cimagingKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cimagingKeyPressed

    private void cpasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cpasienKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cpasienKeyPressed

    private void hal_kritisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_hal_kritisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_hal_kritisKeyPressed

    private void btnAsisten1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAsisten1ActionPerformed
        pilihan=4;
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_btnAsisten1ActionPerformed

    private void btnAsisten1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAsisten1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAsisten1KeyPressed

    private void btnAsisten2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAsisten2ActionPerformed
        pilihan=5;
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_btnAsisten2ActionPerformed

    private void btnAsisten2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAsisten2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAsisten2KeyPressed

    private void btnPerawatAnestesiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPerawatAnestesiActionPerformed
        pilihan=6;
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_btnPerawatAnestesiActionPerformed

    private void btnPerawatAnestesiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPerawatAnestesiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPerawatAnestesiKeyPressed

    private void Jam1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1ActionPerformed

    private void Jam1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1KeyPressed

    private void Menit1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit1KeyPressed

    private void Detik1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik1KeyPressed

    private void cpasien_ket_prosedurKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cpasien_ket_prosedurKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cpasien_ket_prosedurKeyPressed

    private void btnAsisten3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAsisten3ActionPerformed
        pilihan=4;
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);        // TODO add your handling code here:  
    }//GEN-LAST:event_btnAsisten3ActionPerformed

    private void btnAsisten3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAsisten3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAsisten3KeyPressed

    private void btnBidanPerinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBidanPerinaActionPerformed
        pilihan=7;
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);       
    }//GEN-LAST:event_btnBidanPerinaActionPerformed

    private void btnBidanPerinaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnBidanPerinaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnBidanPerinaKeyPressed

    private void csterilitas_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_csterilitas_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_csterilitas_ketKeyPressed

    private void cantibiotikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantibiotikKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantibiotikKeyPressed

    private void cantibiotik_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantibiotik_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantibiotik_ketKeyPressed

    private void Jam2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam2ActionPerformed

    private void Jam2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam2KeyPressed

    private void Menit2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit2KeyPressed

    private void Detik2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik2KeyPressed

    private void cmasalahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmasalahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmasalahKeyPressed

    private void cmasalah_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmasalah_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmasalah_ketKeyPressed

    private void cimplan_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cimplan_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cimplan_ketKeyPressed

    private void btnInstrumenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInstrumenActionPerformed
        pilihan=8;
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);     
    }//GEN-LAST:event_btnInstrumenActionPerformed

    private void btnInstrumenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnInstrumenKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnInstrumenKeyPressed

    private void casaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_casaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_casaKeyPressed

    private void cpasien_ket_tempatinsisiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cpasien_ket_tempatinsisiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cpasien_ket_tempatinsisiKeyPressed

    private void Jam3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam3ActionPerformed

    private void Jam3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam3KeyPressed

    private void Menit3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit3KeyPressed

    private void cjenisanestesiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cjenisanestesiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cjenisanestesiKeyPressed

    private void btnDataOperatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDataOperatorActionPerformed

        carioperatorsignin.emptTeks();
        carioperatorsignin.setnorawat(TNoRw.getText());
        carioperatorsignin.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        carioperatorsignin.setLocationRelativeTo(internalFrame1);
        carioperatorsignin.setVisible(true);
    }//GEN-LAST:event_btnDataOperatorActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMTimeOutSebelumInsisi dialog = new RMTimeOutSebelumInsisi(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.CekBox ChkInput;
    private widget.CekBox ChkJam;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.Tanggal DTPtanggal;
    private widget.ComboBox Detik1;
    private widget.ComboBox Detik2;
    private widget.PanelBiasa FormInput;
    private widget.ComboBox Jam1;
    private widget.ComboBox Jam2;
    private widget.ComboBox Jam3;
    private widget.TextBox KdAnak;
    private widget.TextBox KdAsisten1;
    private widget.TextBox KdAsisten2;
    private widget.TextBox KdInstrumen;
    private widget.TextBox KdPerina;
    private widget.TextBox KdPetugasOK;
    private widget.TextBox KdPrwAnestesi;
    private widget.TextBox KodeDokterAnestesi;
    private widget.TextBox KodeOperator1;
    private widget.TextBox KodeOperator2;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private widget.ComboBox Menit1;
    private widget.ComboBox Menit2;
    private widget.ComboBox Menit3;
    private javax.swing.JMenuItem MnTimeOutSebelumInsisi;
    private widget.TextBox NamaDokterAnestesi;
    private widget.TextBox NamaOperator1;
    private widget.TextBox NamaOperator2;
    private widget.TextBox NmAnak;
    private widget.TextBox NmAsisten1;
    private widget.TextBox NmAsisten2;
    private widget.TextBox NmInstrumen;
    private widget.TextBox NmPerina;
    private widget.TextBox NmPetugasOK;
    private widget.TextBox NmPrwAnestesi;
    private javax.swing.JPanel PanelInput;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.TextBox TglLahir;
    private widget.Button btnAsisten1;
    private widget.Button btnAsisten2;
    private widget.Button btnAsisten3;
    private widget.Button btnBidanPerina;
    private widget.Button btnDataOperator;
    private widget.Button btnDokterAnestesi;
    private widget.Button btnInstrumen;
    private widget.Button btnOperator1;
    private widget.Button btnOperator2;
    private widget.Button btnPerawatAnestesi;
    private widget.Button btnPetugasOK;
    private widget.ComboBox cantibiotik;
    private widget.TextBox cantibiotik_ket;
    private widget.ComboBox casa;
    private widget.ComboBox cimaging;
    private widget.TextBox cimaging_ket;
    private widget.ComboBox cimplan;
    private widget.TextBox cimplan_ket;
    private widget.ComboBox cjenisanestesi;
    private widget.ComboBox ckehilangandarah;
    private widget.TextBox ckehilangandarah_ket;
    private widget.ComboBox cmasalah;
    private widget.TextBox cmasalah_ket;
    private widget.ComboBox cpasien;
    private widget.TextBox cpasien_ket_prosedur;
    private widget.TextBox cpasien_ket_tempatinsisi;
    private widget.ComboBox csterilitas;
    private widget.TextBox csterilitas_ket;
    private widget.ComboBox ctim;
    private widget.TextBox hal_kritis;
    private widget.TextBox hal_penting;
    private widget.InternalFrame internalFrame1;
    private widget.Label jLabel16;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel29;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel33;
    private widget.Label jLabel4;
    private widget.Label jLabel5;
    private widget.Label jLabel53;
    private widget.Label jLabel54;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel61;
    private widget.Label jLabel62;
    private widget.Label jLabel63;
    private widget.Label jLabel64;
    private widget.Label jLabel65;
    private widget.Label jLabel66;
    private widget.Label jLabel67;
    private widget.Label jLabel68;
    private widget.Label jLabel69;
    private widget.Label jLabel7;
    private widget.Label jLabel70;
    private widget.Label jLabel71;
    private widget.Label jLabel72;
    private widget.Label jLabel73;
    private widget.Label jLabel74;
    private widget.Label jLabel75;
    private widget.Label jLabel76;
    private widget.Label jLabel77;
    private widget.Label jLabel8;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private widget.Label label15;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables
    
    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().trim().equals("")){
                ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,pasien.jk, "+
                    "timeout_sebelum_insisi.*,d1.kd_dokter as kd_operator1,d1.nm_dokter as nm_operator1,d2.kd_dokter as kd_operator2,d2.nm_dokter as nm_operator2, "+
                    "d3.kd_dokter as kd_anestesi,d3.nm_dokter as nm_anestesi,timeout_sebelum_insisi.kd_petugas,petugas.nama, "+
                    "a1.nip as kd_asisten1,a1.nama as nm_asisten1,a2.nip as kd_asisten2,a2.nama as nm_asisten2,pa.nip as kd_prw_anestesi,pa.nama as nm_prw_anestesi,d4.kd_dokter as kd_anak,d4.nm_dokter as nm_anak,d5.nip as kd_perinatologi,d5.nama as nm_perinatologi,d6.nip as kd_instrumen,d6.nama as nm_instrumen "+
                    "from timeout_sebelum_insisi inner join reg_periksa on timeout_sebelum_insisi.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter as d1 on d1.kd_dokter=timeout_sebelum_insisi.kd_operator1 "+
                    "inner join dokter as d2 on d2.kd_dokter=timeout_sebelum_insisi.kd_operator2 "+
                    "inner join dokter as d3 on d3.kd_dokter=timeout_sebelum_insisi.kd_anestesi "+
                    "inner join dokter as d4 on d4.kd_dokter=timeout_sebelum_insisi.kd_anak "+
                    "inner join petugas as d5 on d5.nip=timeout_sebelum_insisi.kd_perinatologi "+
                    "inner join petugas as d6 on d6.nip=timeout_sebelum_insisi.kd_instrumen "+
                    "inner join petugas on petugas.nip=timeout_sebelum_insisi.kd_petugas "+
                    "inner join petugas as a1 on a1.nip=timeout_sebelum_insisi.kd_asisten1 "+
                    "inner join petugas as a2 on a2.nip=timeout_sebelum_insisi.kd_asisten2 "+
                    "inner join petugas as pa on pa.nip=timeout_sebelum_insisi.kd_prw_anestesi "+
                    "where timeout_sebelum_insisi.tanggal between ? and ? order by timeout_sebelum_insisi.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,pasien.jk, "+
                    "timeout_sebelum_insisi.*,d1.kd_dokter as kd_operator1,d1.nm_dokter as nm_operator1,d2.kd_dokter as kd_operator2,d2.nm_dokter as nm_operator2, "+
                    "d3.kd_dokter as kd_anestesi,d3.nm_dokter as nm_anestesi,timeout_sebelum_insisi.kd_petugas,petugas.nama, "+
                    "a1.nip as kd_asisten1,a1.nama as nm_asisten1,a2.nip as kd_asisten2,a2.nama as nm_asisten2,pa.nip as kd_prw_anestesi,pa.nama as nm_prw_anestesi,d4.kd_dokter as kd_anak,d4.nm_dokter as nm_anak,d5.nip as kd_perinatologi,d5.nama as nm_perinatologi,d6.nip as kd_instrumen,d6.nama as nm_instrumen "+
                    "from timeout_sebelum_insisi inner join reg_periksa on timeout_sebelum_insisi.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter as d1 on d1.kd_dokter=timeout_sebelum_insisi.kd_operator1 "+
                    "inner join dokter as d2 on d2.kd_dokter=timeout_sebelum_insisi.kd_operator2 "+
                    "inner join dokter as d3 on d3.kd_dokter=timeout_sebelum_insisi.kd_anestesi "+
                    "inner join dokter as d4 on d4.kd_dokter=timeout_sebelum_insisi.kd_anak "+
                    "inner join petugas as d5 on d5.nip=timeout_sebelum_insisi.kd_perinatologi "+
                    "inner join petugas as d6 on d6.nip=timeout_sebelum_insisi.kd_instrumen "+
                    "inner join petugas on petugas.nip=timeout_sebelum_insisi.kd_petugas "+
                    "inner join petugas as a1 on a1.nip=timeout_sebelum_insisi.kd_asisten1 "+
                    "inner join petugas as a2 on a2.nip=timeout_sebelum_insisi.kd_asisten2 "+
                    "inner join petugas as pa on pa.nip=timeout_sebelum_insisi.kd_prw_anestesi "+
                    "where timeout_sebelum_insisi.tanggal between ? and ? and (reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or "+
                    "pasien.nm_pasien like ? or d1.nm_dokter like ? or d2.nm_dokter like ? or petugas.nama like ?) "+
                    "order by timeout_sebelum_insisi.tanggal ");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                }
                    
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("tgl_lahir"),
                        rs.getString("jk"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("kd_operator1"),
                        rs.getString("nm_operator1"),
                        rs.getString("kd_operator2"),
                        rs.getString("nm_operator2"),
                        rs.getString("kd_anestesi"),
                        rs.getString("nm_anestesi"),
                        rs.getString("kd_anak"),
                        rs.getString("nm_anak"),
                        rs.getString("kd_perinatologi"),
                        rs.getString("nm_perinatologi"),
                        rs.getString("kd_instrumen"),
                        rs.getString("nm_instrumen"),
                        rs.getString("konfirmasi_tim"),
                        rs.getString("konfirmasi_pasien"),
                        rs.getString("ket_konfirmasi_pasien"),
                        rs.getString("ket_konfirmasi_insisi"),
                        rs.getString("konfirmasi_antibiotik"),
                        rs.getString("jam_antibiotik"),
                        rs.getString("ket_antibiotik"),
                        rs.getString("antisipasi_operator"),
                        rs.getString("antisipasi_jamlamaoperasi"),
                        rs.getString("antisipasi_menitlamaoperasi"),
                        rs.getString("antisipasi_kehilangandarah"),
                        rs.getString("ket_antisipasi_kehilangandarah"),
                        rs.getString("asa_antisipasi_anestesi"),
                        rs.getString("antisipasi_anestesi_jenis"),
                        rs.getString("antisipasi_anestesi"),
                        rs.getString("antisipasi_keperawatan_1"),
                        rs.getString("ket_antisipasi_keperawatan_1"),
                        rs.getString("antisipasi_keperawatan_2"),
                        rs.getString("ket_antisipasi_keperawatan_2"),
                        rs.getString("antisipasi_keperawatan_3"),
                        rs.getString("ket_antisipasi_keperawatan_3"),
                        rs.getString("antisipasi_keperawatan_4"),
                        rs.getString("ket_antisipasi_keperawatan_4"),
                        rs.getString("kd_petugas"),
                        rs.getString("nama"),
                        rs.getString("kd_asisten1"),
                        rs.getString("nm_asisten1"),
                        rs.getString("kd_asisten2"),
                        rs.getString("nm_asisten2"),
                        rs.getString("kd_prw_anestesi"),
                        rs.getString("nm_prw_anestesi")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }
    
    public void emptTeks() {
        KodeOperator1.setText("");
        NamaOperator1.setText("");
        KodeOperator2.setText("");
        NamaOperator2.setText("");
        KodeDokterAnestesi.setText("");
        NamaDokterAnestesi.setText("");
        KdAsisten1.setText("");
        NmAsisten1.setText("");
        KdAsisten2.setText("");
        NmAsisten2.setText("");
        KdPrwAnestesi.setText("");
        NmPrwAnestesi.setText("");
        KdPetugasOK.setText("");
        NmPetugasOK.setText("");
        KdAnak.setText("");
        NmAnak.setText("");
        KdPerina.setText("");
        NmPerina.setText("");
        KdInstrumen.setText("");
        NmInstrumen.setText("");
        ctim.setSelectedIndex(0);
        cpasien.setSelectedIndex(0);
        cpasien_ket_prosedur.setText("");
        cantibiotik.setSelectedIndex(0);
        cantibiotik_ket.setText("");
        Jam2.setSelectedIndex(0);
        Menit2.setSelectedIndex(0);
        Detik2.setSelectedIndex(0);
        hal_kritis.setText("");
        Jam3.setSelectedIndex(0);
        Menit3.setSelectedIndex(0);
        ckehilangandarah.setSelectedIndex(0);
        ckehilangandarah_ket.setText("");
        casa.setSelectedIndex(0);
        cjenisanestesi.setSelectedIndex(0);
        hal_penting.setText("");
        csterilitas.setSelectedIndex(0);
        csterilitas_ket.setText("");
        cmasalah.setSelectedIndex(0);
        cmasalah_ket.setText("");
        cimplan.setSelectedIndex(0);
        cimplan_ket.setText("");
        cimaging.setSelectedIndex(0);
        cimaging_ket.setText("");
        DTPtanggal.setDate(new Date());
        ChkJam.setSelected(true);
        cpasien_ket_tempatinsisi.setText("");
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            emptTeks();
            ChkJam.setSelected(false);
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Valid.SetTgl(DTPtanggal,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            Jam1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString().substring(0,2));
            Menit1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString().substring(3,5));
            Detik1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString().substring(6,8));
            KodeOperator1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            NamaOperator1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            KodeOperator2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            NamaOperator2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            KodeDokterAnestesi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            NamaDokterAnestesi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            KdAnak.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            NmAnak.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            KdPerina.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            NmPerina.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            KdInstrumen.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            NmInstrumen.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            ctim.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            cpasien.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            cpasien_ket_prosedur.setText(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            cpasien_ket_tempatinsisi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            cantibiotik.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            Jam2.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString().substring(0,2));
            Menit2.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString().substring(3,5));
            Detik2.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString().substring(6,8));
            cantibiotik_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            hal_kritis.setText(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            Jam3.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),27).toString());
            Menit3.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            ckehilangandarah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            ckehilangandarah_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
            casa.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            cjenisanestesi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());
            hal_penting.setText(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString());
            csterilitas.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());
            csterilitas_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),35).toString());
            cmasalah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),36).toString());
            cmasalah_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString());
            cimplan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),38).toString());
            cimplan_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),39).toString());
            cimaging.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),40).toString());
            cimaging_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),41).toString());
            KdPetugasOK.setText(tbObat.getValueAt(tbObat.getSelectedRow(),42).toString());
            NmPetugasOK.setText(tbObat.getValueAt(tbObat.getSelectedRow(),43).toString());
            KdAsisten1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString());
            NmAsisten1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),45).toString());
            KdAsisten2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),46).toString());
            NmAsisten2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),47).toString());
            KdPrwAnestesi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),48).toString());
            NmPrwAnestesi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),49).toString());
   
        }
    }
    private void isRawat() {
         Sequel.cariIsi("select reg_periksa.no_rkm_medis from reg_periksa where reg_periksa.no_rawat='"+TNoRw.getText()+"' ",TNoRM);
    }

    private void isPsien() {
        Sequel.cariIsi("select pasien.nm_pasien from pasien where pasien.no_rkm_medis='"+TNoRM.getText()+"' ",TPasien);
        Sequel.cariIsi("select DATE_FORMAT(pasien.tgl_lahir,'%d-%m-%Y') from pasien where pasien.no_rkm_medis=? ",TglLahir,TNoRM.getText());
    }
    

    
    public void setNoRm(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        Sequel.cariIsi("select reg_periksa.tgl_registrasi from reg_periksa where reg_periksa.no_rawat='"+norwt+"'", DTPCari1);
        DTPCari2.setDate(tgl2);
        isRawat();
        isPsien();
        ChkInput.setSelected(true);
        isForm();
       
        
    }
    
    private void isForm(){
        if(ChkInput.isSelected()==true){
            if(internalFrame1.getHeight()>508){
                ChkInput.setVisible(false);
                PanelInput.setPreferredSize(new Dimension(WIDTH,336));
                FormInput.setVisible(true);      
                ChkInput.setVisible(true);
            }else{
                ChkInput.setVisible(false);
                PanelInput.setPreferredSize(new Dimension(WIDTH,internalFrame1.getHeight()-172));
                FormInput.setVisible(true);      
                ChkInput.setVisible(true);
            }
        }else if(ChkInput.isSelected()==false){           
            ChkInput.setVisible(false);            
            PanelInput.setPreferredSize(new Dimension(WIDTH,20));
            FormInput.setVisible(false);      
            ChkInput.setVisible(true);
        }
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getoperasi());
        BtnHapus.setEnabled(akses.getoperasi());
        BtnEdit.setEnabled(akses.getoperasi());
        BtnPrint.setEnabled(akses.getoperasi()); 
    }

    private void ganti() {
        Sequel.mengedit("timeout_sebelum_insisi","no_rawat=? and tanggal=? and jam=?","no_rawat=?,tanggal=?,jam=?,kd_operator1=?,kd_operator2=?,kd_anestesi=?,kd_petugas=?,kd_asisten1=?,kd_asisten2=?,kd_prw_anestesi=?,kd_anak=?,kd_perinatologi=?,kd_instrumen=?,konfirmasi_tim=?,konfirmasi_pasien=?,ket_konfirmasi_pasien=?,ket_konfirmasi_insisi=?,konfirmasi_antibiotik=?,jam_antibiotik=?,ket_antibiotik=?,antisipasi_operator=?,antisipasi_jamlamaoperasi=?,antisipasi_menitlamaoperasi=?,antisipasi_kehilangandarah=?,ket_antisipasi_kehilangandarah=?,asa_antisipasi_anestesi=?,antisipasi_anestesi_jenis=?," +
                "antisipasi_anestesi=?,antisipasi_keperawatan_1=?,ket_antisipasi_keperawatan_1=?,antisipasi_keperawatan_2=?,ket_antisipasi_keperawatan_2=?,antisipasi_keperawatan_3=?,ket_antisipasi_keperawatan_3=?,antisipasi_keperawatan_4=?,ket_antisipasi_keperawatan_4=?",39,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(DTPtanggal.getSelectedItem()+""),
                Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                KodeOperator1.getText(),
                KodeOperator2.getText(),
                KodeDokterAnestesi.getText(),
                KdPetugasOK.getText(),
                KdAsisten1.getText(),
                KdAsisten2.getText(),
                KdPrwAnestesi.getText(),
                KdAnak.getText(),
                KdPerina.getText(),
                KdInstrumen.getText(),
                ctim.getSelectedItem().toString(),
                cpasien.getSelectedItem().toString(),
                cpasien_ket_prosedur.getText(),
                cpasien_ket_tempatinsisi.getText(),
                cantibiotik.getSelectedItem().toString(),
                Jam2.getSelectedItem()+":"+Menit2.getSelectedItem()+":"+Detik2.getSelectedItem(),
                cantibiotik_ket.getText(),
                hal_kritis.getText(),
                Jam3.getSelectedItem().toString(),
                Menit3.getSelectedItem().toString(),
                ckehilangandarah.getSelectedItem().toString(),
                ckehilangandarah_ket.getText(),
                casa.getSelectedItem().toString(),
                cjenisanestesi.getSelectedItem().toString(),
                hal_penting.getText(),
                csterilitas.getSelectedItem().toString(),
                csterilitas_ket.getText(),
                cmasalah.getSelectedItem().toString(),
                cmasalah_ket.getText(),
                cimplan.getSelectedItem().toString(),
                cimplan_ket.getText(),
                cimaging.getSelectedItem().toString(),
                cimaging_ket.getText(),
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),
                tbObat.getValueAt(tbObat.getSelectedRow(),5).toString(),
                tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()
        });
            
        if(tabMode.getRowCount()!=0){tampil();}
        emptTeks();
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from timeout_sebelum_insisi where no_rawat=? and tanggal=? and jam=?",3,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),
            tbObat.getValueAt(tbObat.getSelectedRow(),5).toString(),
            tbObat.getValueAt(tbObat.getSelectedRow(),6).toString(),
        })==true){
            tabMode.removeRow(tbObat.getSelectedRow());
            LCount.setText(""+tabMode.getRowCount());
            emptTeks();
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }
    private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkJam.isSelected()==false){
                    nilai_jam =Jam1.getSelectedIndex();
                    nilai_menit =Menit1.getSelectedIndex();
                    nilai_detik =Detik1.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam1.setSelectedItem(jam);
                Menit1.setSelectedItem(menit);
                Detik1.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
}
