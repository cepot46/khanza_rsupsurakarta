/*
 * Kontribusi Anggit Nurhidayah
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariPetugas;


/**
 *
 * @author perpustakaan
 */
public final class RMFormulirKFR extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps,ps_soap;
    private ResultSet rs,rs_soap;
    private int i=0;
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private String finger=""; 
    private StringBuilder htmlContent;
    //START CUSTOM MUHSIN
    private boolean[] pilih_i;    
    private String[] kode_i,intervensi;    
    private String intervensiresikojatuh=""; 
    private int jml=0,index=0,jml_i=0,count_intervensi=0;    
    private final DefaultTableModel tabModeIntervensi,tabModeDetailIntervensi;  //CUSTOM MUHSIN
    //END CUSTOM
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMFormulirKFR(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","J.K.","Tanggal","Tanggal Pelayanan","Jam Pelayanan",
            "Anamnesa","Pemeriksaan Fisik","Diagnosa Medis","Diagnosa Fungsi", 
            "Pemeriksaan Penunjang","Tata Laksana","Anjuran","Evaluasi","Suspek Penyakit","Keterangan","NIP","Petugas"  //CUSTOM ANGGIT
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 17; i++) {  //CUSTOM ANGGIT
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(65);
            }else if(i==2){
                column.setPreferredWidth(160);
            }else if(i==3){
                column.setPreferredWidth(50);
            }else if(i==4){
                column.setPreferredWidth(80);
            }else if(i==5){
                column.setPreferredWidth(120);
            }else if(i==6){
                column.setPreferredWidth(85);
            }else if(i==7){
                column.setPreferredWidth(270);
            }else if(i==8){
                column.setPreferredWidth(184);
            }else if(i==9){
                column.setPreferredWidth(184);
            }else if(i==10){
                column.setPreferredWidth(120);
            }else if(i==11){
                column.setPreferredWidth(120);
            }else if(i==12){
                column.setPreferredWidth(120);
            }else if(i==13){
                column.setPreferredWidth(120);
            }else if(i==14){
                column.setPreferredWidth(120);
            }else if(i==15){
                column.setPreferredWidth(120);
            }else if(i==16){
                column.setPreferredWidth(100);
            }else if(i==17){
                column.setPreferredWidth(100);
            } //END CUSTOM
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        anamnesa.setDocument(new batasInput((int)500).getKata(anamnesa));
        pemeriksaanfisik.setDocument(new batasInput((int)500).getKata(pemeriksaanfisik));
        diagnosamedis.setDocument(new batasInput((int)500).getKata(diagnosamedis));
        diagnosafungsi.setDocument(new batasInput((int)500).getKata(diagnosafungsi));
        penunjang.setDocument(new batasInput((int)500).getKata(penunjang));
        tatalaksana.setDocument(new batasInput((int)500).getKata(tatalaksana));
        anjuran.setDocument(new batasInput((int)500).getKata(anjuran));
         //CUSTOM MUHSIN
        
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){ 
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());   
                }              
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
        
        //START CUSTOM MUHSIN
        tabModeIntervensi=new DefaultTableModel(null,new Object[]{
                "P","KODE","TINDAKAN / INTERVENSI"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbIntervensiResikoJatuh.setModel(tabModeIntervensi);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbIntervensiResikoJatuh.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbIntervensiResikoJatuh.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 3; i++) {
            TableColumn column = tbIntervensiResikoJatuh.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(20);
            }else if(i==1){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==2){
                column.setPreferredWidth(500);
            }
        }
        tbIntervensiResikoJatuh.setDefaultRenderer(Object.class, new WarnaTable());
        
        tabModeDetailIntervensi=new DefaultTableModel(null,new Object[]{
                
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        
        jam();
        jam2();
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnPenilaianLayanannKFR = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnDokter = new widget.Button();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        label11 = new widget.Label();
        jLabel11 = new widget.Label();
        jLabel53 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel54 = new widget.Label();
        jLabel47 = new widget.Label();
        scrollPane3 = new widget.ScrollPane();
        anamnesa = new widget.TextArea();
        jLabel48 = new widget.Label();
        scrollPane5 = new widget.ScrollPane();
        pemeriksaanfisik = new widget.TextArea();
        jLabel49 = new widget.Label();
        jLabel58 = new widget.Label();
        scrollPane6 = new widget.ScrollPane();
        diagnosamedis = new widget.TextArea();
        scrollPane7 = new widget.ScrollPane();
        diagnosafungsi = new widget.TextArea();
        jLabel59 = new widget.Label();
        jLabel60 = new widget.Label();
        scrollPane9 = new widget.ScrollPane();
        tatalaksana = new widget.TextArea();
        scrollPane10 = new widget.ScrollPane();
        penunjang = new widget.TextArea();
        jLabel61 = new widget.Label();
        scrollPane12 = new widget.ScrollPane();
        anjuran = new widget.TextArea();
        jSeparator16 = new javax.swing.JSeparator();
        Scroll8 = new widget.ScrollPane();
        tbIntervensiResikoJatuh = new widget.Table();
        jLabel44 = new widget.Label();
        jLabel45 = new widget.Label();
        jLabel46 = new widget.Label();
        jLabel64 = new widget.Label();
        alamat = new widget.TextBox();
        nm_pasien = new widget.TextBox();
        tgl_lahir = new widget.TextBox();
        nohp = new widget.TextBox();
        scrollPane16 = new widget.ScrollPane();
        evaluasi = new widget.TextArea();
        suspek_penyakit = new widget.ComboBox();
        jLabel12 = new widget.Label();
        scrollPane13 = new widget.ScrollPane();
        keterangan = new widget.TextArea();
        jLabel66 = new widget.Label();
        Tanggal = new widget.Tanggal();
        Jam = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Detik = new widget.ComboBox();
        ChkKejadian = new widget.CekBox();
        Jam1 = new widget.ComboBox();
        Menit1 = new widget.ComboBox();
        Detik1 = new widget.ComboBox();
        TanggalPelayanan = new widget.Tanggal();
        ChkKejadian1 = new widget.CekBox();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnPenilaianLayanannKFR.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianLayanannKFR.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianLayanannKFR.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianLayanannKFR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianLayanannKFR.setText("Formulir Pelayanan KFR");
        MnPenilaianLayanannKFR.setName("MnPenilaianLayanannKFR"); // NOI18N
        MnPenilaianLayanannKFR.setPreferredSize(new java.awt.Dimension(220, 26));
        MnPenilaianLayanannKFR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianLayanannKFRActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianLayanannKFR);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Formulir Layanan Kedokteran Fisik dan Rehabilitasi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 800));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(74, 40, 100, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(176, 40, 250, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(430, 40, 28, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(310, 120, 120, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        label11.setText("Tanggal Pelayanan :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(460, 90, 110, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(270, 120, 30, 23);

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel53.setText("I. DATA PASIEN");
        jLabel53.setName("jLabel53"); // NOI18N
        FormInput.add(jLabel53);
        jLabel53.setBounds(10, 70, 180, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 70, 880, 1);

        jSeparator2.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator2.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator2.setName("jSeparator2"); // NOI18N
        FormInput.add(jSeparator2);
        jSeparator2.setBounds(0, 230, 880, 3);

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("II. DI ISI OLEH DOKTER Sp.KFR");
        jLabel54.setName("jLabel54"); // NOI18N
        FormInput.add(jLabel54);
        jLabel54.setBounds(10, 240, 180, 23);

        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel47.setText("a. Anamnesa :");
        jLabel47.setName("jLabel47"); // NOI18N
        FormInput.add(jLabel47);
        jLabel47.setBounds(30, 270, 182, 23);

        scrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane3.setName("scrollPane3"); // NOI18N

        anamnesa.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        anamnesa.setColumns(20);
        anamnesa.setRows(5);
        anamnesa.setName("anamnesa"); // NOI18N
        anamnesa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                anamnesaKeyPressed(evt);
            }
        });
        scrollPane3.setViewportView(anamnesa);

        FormInput.add(scrollPane3);
        scrollPane3.setBounds(30, 290, 375, 43);

        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel48.setText("b. Pemeriksaan Fisik dan Uji Fungsi : ");
        jLabel48.setName("jLabel48"); // NOI18N
        FormInput.add(jLabel48);
        jLabel48.setBounds(450, 270, 182, 23);

        scrollPane5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane5.setName("scrollPane5"); // NOI18N

        pemeriksaanfisik.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pemeriksaanfisik.setColumns(20);
        pemeriksaanfisik.setRows(5);
        pemeriksaanfisik.setName("pemeriksaanfisik"); // NOI18N
        pemeriksaanfisik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pemeriksaanfisikKeyPressed(evt);
            }
        });
        scrollPane5.setViewportView(pemeriksaanfisik);

        FormInput.add(scrollPane5);
        scrollPane5.setBounds(450, 290, 375, 43);

        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel49.setText("c. Diagnosa medis (ICD-10) :");
        jLabel49.setName("jLabel49"); // NOI18N
        FormInput.add(jLabel49);
        jLabel49.setBounds(30, 340, 182, 23);

        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel58.setText("d. Diagnosa Fungsi (ICD-10) :");
        jLabel58.setName("jLabel58"); // NOI18N
        FormInput.add(jLabel58);
        jLabel58.setBounds(450, 340, 182, 23);

        scrollPane6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane6.setName("scrollPane6"); // NOI18N

        diagnosamedis.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        diagnosamedis.setColumns(20);
        diagnosamedis.setRows(5);
        diagnosamedis.setName("diagnosamedis"); // NOI18N
        diagnosamedis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                diagnosamedisKeyPressed(evt);
            }
        });
        scrollPane6.setViewportView(diagnosamedis);

        FormInput.add(scrollPane6);
        scrollPane6.setBounds(30, 360, 375, 43);

        scrollPane7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane7.setName("scrollPane7"); // NOI18N

        diagnosafungsi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        diagnosafungsi.setColumns(20);
        diagnosafungsi.setRows(5);
        diagnosafungsi.setName("diagnosafungsi"); // NOI18N
        diagnosafungsi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                diagnosafungsiKeyPressed(evt);
            }
        });
        scrollPane7.setViewportView(diagnosafungsi);

        FormInput.add(scrollPane7);
        scrollPane7.setBounds(450, 360, 375, 43);

        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel59.setText("e. Pemeriksaan Penunjang :");
        jLabel59.setName("jLabel59"); // NOI18N
        FormInput.add(jLabel59);
        jLabel59.setBounds(30, 410, 182, 23);

        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel60.setText("f. Tata Laksana KFR (ICD 9 CM) : ");
        jLabel60.setName("jLabel60"); // NOI18N
        FormInput.add(jLabel60);
        jLabel60.setBounds(450, 410, 182, 23);

        scrollPane9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane9.setName("scrollPane9"); // NOI18N

        tatalaksana.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        tatalaksana.setColumns(20);
        tatalaksana.setRows(5);
        tatalaksana.setName("tatalaksana"); // NOI18N
        tatalaksana.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tatalaksanaKeyPressed(evt);
            }
        });
        scrollPane9.setViewportView(tatalaksana);

        FormInput.add(scrollPane9);
        scrollPane9.setBounds(450, 430, 375, 43);

        scrollPane10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane10.setName("scrollPane10"); // NOI18N

        penunjang.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        penunjang.setColumns(20);
        penunjang.setRows(5);
        penunjang.setName("penunjang"); // NOI18N
        penunjang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                penunjangKeyPressed(evt);
            }
        });
        scrollPane10.setViewportView(penunjang);

        FormInput.add(scrollPane10);
        scrollPane10.setBounds(30, 430, 375, 43);

        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel61.setText("g. Anjuran :");
        jLabel61.setName("jLabel61"); // NOI18N
        FormInput.add(jLabel61);
        jLabel61.setBounds(30, 480, 182, 23);

        scrollPane12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane12.setName("scrollPane12"); // NOI18N

        anjuran.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        anjuran.setColumns(20);
        anjuran.setRows(5);
        anjuran.setName("anjuran"); // NOI18N
        anjuran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                anjuranKeyPressed(evt);
            }
        });
        scrollPane12.setViewportView(anjuran);

        FormInput.add(scrollPane12);
        scrollPane12.setBounds(30, 500, 375, 60);

        jSeparator16.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator16.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator16.setName("jSeparator16"); // NOI18N
        FormInput.add(jSeparator16);
        jSeparator16.setBounds(0, 1300, 880, 1);

        Scroll8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll8.setName("Scroll8"); // NOI18N
        Scroll8.setOpaque(true);

        tbIntervensiResikoJatuh.setName("tbIntervensiResikoJatuh"); // NOI18N
        Scroll8.setViewportView(tbIntervensiResikoJatuh);

        FormInput.add(Scroll8);
        Scroll8.setBounds(10, 1520, 560, 170);

        jLabel44.setText("Nama Pasien :");
        jLabel44.setName("jLabel44"); // NOI18N
        FormInput.add(jLabel44);
        jLabel44.setBounds(0, 90, 125, 23);

        jLabel45.setText("Tanggal Lahir :");
        jLabel45.setName("jLabel45"); // NOI18N
        FormInput.add(jLabel45);
        jLabel45.setBounds(0, 120, 125, 23);

        jLabel46.setText("Alamat :");
        jLabel46.setName("jLabel46"); // NOI18N
        FormInput.add(jLabel46);
        jLabel46.setBounds(0, 150, 125, 23);

        jLabel64.setText("No Telp / HP :");
        jLabel64.setName("jLabel64"); // NOI18N
        FormInput.add(jLabel64);
        jLabel64.setBounds(0, 180, 125, 23);

        alamat.setEditable(false);
        alamat.setHighlighter(null);
        alamat.setName("alamat"); // NOI18N
        FormInput.add(alamat);
        alamat.setBounds(130, 150, 300, 23);

        nm_pasien.setEditable(false);
        nm_pasien.setHighlighter(null);
        nm_pasien.setName("nm_pasien"); // NOI18N
        FormInput.add(nm_pasien);
        nm_pasien.setBounds(130, 90, 300, 23);

        tgl_lahir.setEditable(false);
        tgl_lahir.setHighlighter(null);
        tgl_lahir.setName("tgl_lahir"); // NOI18N
        FormInput.add(tgl_lahir);
        tgl_lahir.setBounds(130, 120, 140, 23);

        nohp.setEditable(false);
        nohp.setHighlighter(null);
        nohp.setName("nohp"); // NOI18N
        FormInput.add(nohp);
        nohp.setBounds(130, 180, 300, 23);

        scrollPane16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane16.setName("scrollPane16"); // NOI18N

        evaluasi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        evaluasi.setColumns(20);
        evaluasi.setRows(5);
        evaluasi.setName("evaluasi"); // NOI18N
        evaluasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                evaluasiKeyPressed(evt);
            }
        });
        scrollPane16.setViewportView(evaluasi);

        FormInput.add(scrollPane16);
        scrollPane16.setBounds(450, 500, 375, 60);

        suspek_penyakit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak", "Ya" }));
        suspek_penyakit.setName("suspek_penyakit"); // NOI18N
        suspek_penyakit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                suspek_penyakitKeyPressed(evt);
            }
        });
        FormInput.add(suspek_penyakit);
        suspek_penyakit.setBounds(200, 590, 100, 23);

        jLabel12.setText("i. Suspek Penyakit Akibat Kerja :");
        jLabel12.setName("jLabel12"); // NOI18N
        FormInput.add(jLabel12);
        jLabel12.setBounds(30, 590, 160, 23);

        scrollPane13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane13.setName("scrollPane13"); // NOI18N

        keterangan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        keterangan.setColumns(20);
        keterangan.setRows(5);
        keterangan.setName("keterangan"); // NOI18N
        keterangan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                keteranganKeyPressed(evt);
            }
        });
        scrollPane13.setViewportView(keterangan);

        FormInput.add(scrollPane13);
        scrollPane13.setBounds(30, 620, 375, 60);

        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel66.setText("h. Evaluasi :");
        jLabel66.setName("jLabel66"); // NOI18N
        FormInput.add(jLabel66);
        jLabel66.setBounds(450, 480, 182, 23);

        Tanggal.setForeground(new java.awt.Color(50, 70, 50));
        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "03-01-2025" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TanggalKeyPressed(evt);
            }
        });
        FormInput.add(Tanggal);
        Tanggal.setBounds(580, 10, 90, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setName("Jam"); // NOI18N
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        FormInput.add(Jam);
        Jam.setBounds(580, 40, 62, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        FormInput.add(Menit);
        Menit.setBounds(650, 40, 62, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        FormInput.add(Detik);
        Detik.setBounds(720, 40, 62, 23);

        ChkKejadian.setBorder(null);
        ChkKejadian.setSelected(true);
        ChkKejadian.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkKejadian.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkKejadian.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkKejadian.setName("ChkKejadian"); // NOI18N
        FormInput.add(ChkKejadian);
        ChkKejadian.setBounds(780, 40, 23, 23);

        Jam1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam1.setName("Jam1"); // NOI18N
        Jam1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam1KeyPressed(evt);
            }
        });
        FormInput.add(Jam1);
        Jam1.setBounds(680, 90, 62, 23);

        Menit1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit1.setName("Menit1"); // NOI18N
        Menit1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit1KeyPressed(evt);
            }
        });
        FormInput.add(Menit1);
        Menit1.setBounds(750, 90, 62, 23);

        Detik1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik1.setName("Detik1"); // NOI18N
        Detik1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik1KeyPressed(evt);
            }
        });
        FormInput.add(Detik1);
        Detik1.setBounds(820, 90, 62, 23);

        TanggalPelayanan.setForeground(new java.awt.Color(50, 70, 50));
        TanggalPelayanan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "03-01-2025" }));
        TanggalPelayanan.setDisplayFormat("dd-MM-yyyy");
        TanggalPelayanan.setName("TanggalPelayanan"); // NOI18N
        TanggalPelayanan.setOpaque(false);
        TanggalPelayanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TanggalPelayananKeyPressed(evt);
            }
        });
        FormInput.add(TanggalPelayanan);
        TanggalPelayanan.setBounds(580, 90, 90, 23);

        ChkKejadian1.setBorder(null);
        ChkKejadian1.setSelected(true);
        ChkKejadian1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkKejadian1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkKejadian1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkKejadian1.setName("ChkKejadian1"); // NOI18N
        FormInput.add(ChkKejadian1);
        ChkKejadian1.setBounds(880, 90, 23, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Penilaian", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "03-01-2025" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "03-01-2025" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Penilaian", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Petugas");
        }else if(anamnesa.getText().trim().equals("")){
            Valid.textKosong(anamnesa,"Anamnesa");
        }else if(pemeriksaanfisik.getText().trim().equals("")){
            Valid.textKosong(pemeriksaanfisik,"Pemeriksaan Fisik");
        }else if(diagnosamedis.getText().trim().equals("")){
            Valid.textKosong(diagnosamedis,"Diagnosa Medis");
        }else if(diagnosafungsi.getText().trim().equals("")){
            Valid.textKosong(diagnosafungsi,"Diagnosa Fungsi");
        }else if(penunjang.getText().trim().equals("")){
            Valid.textKosong(penunjang,"Penunjang");
        }else if(tatalaksana.getText().trim().equals("")){
            Valid.textKosong(tatalaksana,"Tata Laksana");
        }else if(anjuran.getText().trim().equals("")){
            Valid.textKosong(anjuran,"Anjuran");
        }else if(suspek_penyakit.getSelectedItem().toString().equals("-")){
            Valid.textKosong(suspek_penyakit,"Keterangan Suspek");
        }else{
           //START CUSTOM ANGGIT
           if(Sequel.menyimpantf("penilaian_layanan_kfr","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",15,new String[]{
               TNoRw.getText(),
               Valid.SetTgl(Tanggal.getSelectedItem()+"")+" "+Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
               Valid.SetTgl(TanggalPelayanan.getSelectedItem()+""),
               Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
               anamnesa.getText(),
               pemeriksaanfisik.getText(),
               diagnosamedis.getText(),
               diagnosafungsi.getText(),
               penunjang.getText(),
               tatalaksana.getText(),
               anjuran.getText(),
               evaluasi.getText(),
               suspek_penyakit.getSelectedItem().toString(),
               keterangan.getText(),
               KdPetugas.getText(),      
               })==true){
               emptTeks();
            }
        }
    
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,BtnSimpan,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(KdPetugas.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
     
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Petugas");   
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(KdPetugas.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString())){
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh petugas yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }   
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
         
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
       
    }//GEN-LAST:event_formWindowOpened

    private void MnPenilaianLayanannKFRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianLayanannKFRActionPerformed
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan")); 
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan")); 
            String nippj=Sequel.cariIsi("select kd_dokterkfr from set_pjkfr");
            String dokter=Sequel.cariIsi("select nm_dokter from dokter where kd_dokter='"+nippj+"'");
            param.put("nipdokter",nippj);
            param.put("dokter",dokter); 
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",nippj);
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+dokter+"\nID "+(finger.equals("")?nippj:finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()));

            Valid.MyReportqry("rptCetakLayananKFR.jasper","report","::[ Laporan Penilaian Layanan Kedokteran Fisik dan Rehabilitasi ]::",
                "select penilaian_layanan_kfr.no_rawat,penilaian_layanan_kfr.tanggal,penilaian_layanan_kfr.tanggal_pelayanan,penilaian_layanan_kfr.jam_pelayanan,penilaian_layanan_kfr.anamnesa,penilaian_layanan_kfr.pemeriksaan_fisik, "+
                "penilaian_layanan_kfr.diagnosa_medis,penilaian_layanan_kfr.diagnosa_fungsi,penilaian_layanan_kfr.pemeriksaan_penunjang,penilaian_layanan_kfr.tata_laksana,penilaian_layanan_kfr.anjuran,penilaian_layanan_kfr.evaluasi, "+
                "penilaian_layanan_kfr.suspek_penyakit,penilaian_layanan_kfr.keterangan,penilaian_layanan_kfr.nip,petugas.nama,pasien.no_rkm_medis,pasien.nm_pasien,pasien.jk,pasien.tgl_lahir,CONCAT( pasien.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) AS alamat "+
                "from penilaian_layanan_kfr inner join reg_periksa on reg_periksa.no_rawat=penilaian_layanan_kfr.no_rawat "+
                "inner join petugas on penilaian_layanan_kfr.nip = petugas.nip "+
                "inner join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                "where penilaian_layanan_kfr.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }
    }//GEN-LAST:event_MnPenilaianLayanannKFRActionPerformed

    private void anjuranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_anjuranKeyPressed
        Valid.pindah2(evt,tatalaksana,diagnosafungsi);
    }//GEN-LAST:event_anjuranKeyPressed

    private void penunjangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_penunjangKeyPressed
        Valid.pindah2(evt,diagnosafungsi,tatalaksana);
    }//GEN-LAST:event_penunjangKeyPressed

    private void tatalaksanaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tatalaksanaKeyPressed
        Valid.pindah2(evt,penunjang,anjuran);
    }//GEN-LAST:event_tatalaksanaKeyPressed

    private void diagnosafungsiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_diagnosafungsiKeyPressed
        Valid.pindah2(evt,diagnosamedis,penunjang);
    }//GEN-LAST:event_diagnosafungsiKeyPressed

    private void diagnosamedisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_diagnosamedisKeyPressed
        Valid.pindah2(evt,pemeriksaanfisik,diagnosafungsi);
    }//GEN-LAST:event_diagnosamedisKeyPressed

    private void pemeriksaanfisikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pemeriksaanfisikKeyPressed
        Valid.pindah2(evt,anamnesa,diagnosamedis);
    }//GEN-LAST:event_pemeriksaanfisikKeyPressed

    private void anamnesaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_anamnesaKeyPressed
        Valid.pindah2(evt,diagnosamedis,pemeriksaanfisik);
    }//GEN-LAST:event_anamnesaKeyPressed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        Valid.pindah(evt,suspek_penyakit,keterangan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{
            Valid.pindah(evt,TCari,BtnDokter);
        }
    }//GEN-LAST:event_TNoRwKeyPressed

    private void evaluasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_evaluasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_evaluasiKeyPressed

    private void suspek_penyakitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_suspek_penyakitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_suspek_penyakitKeyPressed

    private void keteranganKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_keteranganKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_keteranganKeyPressed

    private void TanggalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TanggalKeyPressed
        Valid.pindah2(evt,TCari,Jam);
    }//GEN-LAST:event_TanggalKeyPressed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
        Valid.pindah(evt,Tanggal,Menit);
    }//GEN-LAST:event_JamKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        Valid.pindah(evt,Jam,Detik);
    }//GEN-LAST:event_MenitKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        Valid.pindah(evt,Menit,Jam);
    }//GEN-LAST:event_DetikKeyPressed

    private void Jam1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1KeyPressed

    private void Menit1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit1KeyPressed

    private void Detik1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik1KeyPressed

    private void TanggalPelayananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TanggalPelayananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TanggalPelayananKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMFormulirKFR dialog = new RMFormulirKFR(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.CekBox ChkKejadian;
    private widget.CekBox ChkKejadian1;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox Detik;
    private widget.ComboBox Detik1;
    private widget.PanelBiasa FormInput;
    private widget.ComboBox Jam;
    private widget.ComboBox Jam1;
    private widget.TextBox Jk;
    private widget.TextBox KdPetugas;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private widget.ComboBox Menit;
    private widget.ComboBox Menit1;
    private javax.swing.JMenuItem MnPenilaianLayanannKFR;
    private widget.TextBox NmPetugas;
    private widget.ScrollPane Scroll;
    private widget.ScrollPane Scroll8;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal Tanggal;
    private widget.Tanggal TanggalPelayanan;
    private widget.TextBox alamat;
    private widget.TextArea anamnesa;
    private widget.TextArea anjuran;
    private widget.TextArea diagnosafungsi;
    private widget.TextArea diagnosamedis;
    private widget.TextArea evaluasi;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel11;
    private widget.Label jLabel12;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel48;
    private widget.Label jLabel49;
    private widget.Label jLabel53;
    private widget.Label jLabel54;
    private widget.Label jLabel58;
    private widget.Label jLabel59;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel61;
    private widget.Label jLabel64;
    private widget.Label jLabel66;
    private widget.Label jLabel7;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator2;
    private widget.TextArea keterangan;
    private widget.Label label11;
    private widget.Label label14;
    private widget.TextBox nm_pasien;
    private widget.TextBox nohp;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.TextArea pemeriksaanfisik;
    private widget.TextArea penunjang;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane10;
    private widget.ScrollPane scrollPane12;
    private widget.ScrollPane scrollPane13;
    private widget.ScrollPane scrollPane16;
    private widget.ScrollPane scrollPane3;
    private widget.ScrollPane scrollPane5;
    private widget.ScrollPane scrollPane6;
    private widget.ScrollPane scrollPane7;
    private widget.ScrollPane scrollPane9;
    private widget.ComboBox suspek_penyakit;
    private widget.TextArea tatalaksana;
    private widget.Table tbIntervensiResikoJatuh;
    private widget.Table tbObat;
    private widget.TextBox tgl_lahir;
    // End of variables declaration//GEN-END:variables

    private void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().equals("")){
                ps=koneksi.prepareStatement(
                        "select penilaian_layanan_kfr.no_rawat,penilaian_layanan_kfr.tanggal,penilaian_layanan_kfr.tanggal_pelayanan,penilaian_layanan_kfr.jam_pelayanan,penilaian_layanan_kfr.anamnesa,penilaian_layanan_kfr.pemeriksaan_fisik, "+
                        "penilaian_layanan_kfr.diagnosa_medis,penilaian_layanan_kfr.diagnosa_fungsi,penilaian_layanan_kfr.pemeriksaan_penunjang,penilaian_layanan_kfr.tata_laksana,penilaian_layanan_kfr.anjuran,penilaian_layanan_kfr.evaluasi, "+
                        "penilaian_layanan_kfr.suspek_penyakit,penilaian_layanan_kfr.keterangan,penilaian_layanan_kfr.nip,petugas.nama,pasien.no_rkm_medis,pasien.nm_pasien,pasien.jk "+
                        "from penilaian_layanan_kfr inner join reg_periksa on reg_periksa.no_rawat=penilaian_layanan_kfr.no_rawat "+
                        "left join petugas on penilaian_layanan_kfr.nip=petugas.nip "+
                        "left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                        "where penilaian_layanan_kfr.tanggal between ? and ? order by penilaian_layanan_kfr.no_rawat asc, penilaian_layanan_kfr.tanggal asc ");
            }else{
                ps=koneksi.prepareStatement(
                        "select penilaian_layanan_kfr.no_rawat,penilaian_layanan_kfr.tanggal,penilaian_layanan_kfr.tanggal_pelayanan,penilaian_layanan_kfr.jam_pelayanan,penilaian_layanan_kfr.anamnesa,penilaian_layanan_kfr.pemeriksaan_fisik, "+
                        "penilaian_layanan_kfr.diagnosa_medis,penilaian_layanan_kfr.diagnosa_fungsi,penilaian_layanan_kfr.pemeriksaan_penunjang,penilaian_layanan_kfr.tata_laksana,penilaian_layanan_kfr.anjuran,penilaian_layanan_kfr.evaluasi, "+
                        "penilaian_layanan_kfr.suspek_penyakit,penilaian_layanan_kfr.keterangan,penilaian_layanan_kfr.nip,petugas.nama,pasien.no_rkm_medis,pasien.nm_pasien,pasien.jk "+
                        "from penilaian_layanan_kfr inner join reg_periksa on reg_periksa.no_rawat=penilaian_layanan_kfr.no_rawat "+
                        "left join petugas on penilaian_layanan_kfr.nip=petugas.nip "+
                        "left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                        "where penilaian_layanan_kfr.tanggal between ? and ? and "+
                        "(penilaian_layanan_kfr.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
                        "penilaian_layanan_kfr.nip like ? or petugas.nama like ?) order by penilaian_layanan_kfr.no_rawat asc,penilaian_layanan_kfr.tanggal asc");
            }
                
            try {
                if(TCari.getText().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("penilaian_layanan_kfr.no_rawat"),rs.getString("pasien.no_rkm_medis"),rs.getString("pasien.nm_pasien"),rs.getString("jk"),rs.getString("tanggal"),
                        rs.getString("tanggal_pelayanan"),rs.getString("jam_pelayanan"),rs.getString("anamnesa"),rs.getString("pemeriksaan_fisik"),rs.getString("diagnosa_medis"),
                        rs.getString("diagnosa_fungsi"),rs.getString("pemeriksaan_penunjang"),rs.getString("tata_laksana"),rs.getString("anjuran"),rs.getString("evaluasi"),rs.getString("suspek_penyakit"),
                        rs.getString("keterangan"),rs.getString("nip"),rs.getString("nama")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
               TNoRw.getText();
               Tanggal.setDate(new Date());
               anamnesa.setText("");
               pemeriksaanfisik.setText("");
               diagnosamedis.setText("");
               diagnosafungsi.setText("");
               penunjang.setText("");
               tatalaksana.setText("");
               anjuran.setText("");
               evaluasi.setText("");
               suspek_penyakit.setSelectedItem("Tidak");
               keterangan.setText("");
                   
        TabRawat.setSelectedIndex(0);
      
    } 

    private void getData() {
         if(tbObat.getSelectedRow()!= -1){
             try {
                    ps=koneksi.prepareStatement(
                            "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.no_tlp,concat(pasien.alamat,' ',kel.nm_kel,', ',kec.nm_kec,', ',kab.nm_kab) as alamat,reg_periksa.tgl_registrasi "+
                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis inner join kelurahan kel on pasien.kd_kel = kel.kd_kel inner join kecamatan kec on pasien.kd_kec = kec.kd_kec inner join kabupaten kab on pasien.kd_kab = kab.kd_kab where reg_periksa.no_rawat=?");
                    try {
                        ps.setString(1,TNoRw.getText());
                        rs=ps.executeQuery();
                        if(rs.next()){
                            TNoRM.setText(rs.getString("no_rkm_medis"));
                            TPasien.setText(rs.getString("nm_pasien"));
                            nm_pasien.setText(rs.getString("nm_pasien"));
                            alamat.setText(rs.getString("alamat"));
                            nohp.setText(rs.getString("no_tlp"));
                            DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                            Jk.setText(rs.getString("jk"));
                            tgl_lahir.setText(rs.getString("tgl_lahir"));
                        }
                    } catch (Exception e) {
                        System.out.println("Notif : "+e);
                    } finally{
                        if(rs!=null){
                            rs.close();
                        }
                        if(ps!=null){
                            ps.close();
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                }
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            
            Valid.SetTgl(Tanggal,tbObat.getValueAt(tbObat.getSelectedRow(),4).toString().substring(0,10));
            Jam.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString().substring(11,12));
            Menit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString().substring(14,16));
            Detik.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString().substring(17,19));
            
            Valid.SetTgl(TanggalPelayanan,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            Jam1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString().substring(0,2));
            Menit1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString().substring(3,5));
            Detik1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString().substring(6,8));
            anamnesa.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            pemeriksaanfisik.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            diagnosamedis.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            diagnosafungsi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            penunjang.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            tatalaksana.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            anjuran.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            evaluasi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            suspek_penyakit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),15));
            keterangan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
         }
    }
    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.no_tlp,concat(pasien.alamat,' ',kel.nm_kel,', ',kec.nm_kec,', ',kab.nm_kab) as alamat,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis inner join kelurahan kel on pasien.kd_kel = kel.kd_kel inner join kecamatan kec on pasien.kd_kec = kec.kd_kec inner join kabupaten kab on pasien.kd_kab = kab.kd_kab where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    nm_pasien.setText(rs.getString("nm_pasien"));
                    alamat.setText(rs.getString("alamat"));
                    nohp.setText(rs.getString("no_tlp"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    Jk.setText(rs.getString("jk"));
                    tgl_lahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
        
        // START CUSTOM ANGGIT AMBIL SOAP TERAKHIR DARI DOKTER KFR
        try {
            String nippj=Sequel.cariIsi("select kd_dokterkfr from set_pjkfr");
            ps_soap=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi,pemeriksaan_ralan.no_rawat,pemeriksaan_ralan.tgl_perawatan,pemeriksaan_ralan.jam_rawat,pemeriksaan_ralan.keluhan as S,pemeriksaan_ralan.pemeriksaan as O, pemeriksaan_ralan.rtl as P, pemeriksaan_ralan.penilaian as A, pemeriksaan_ralan.instruksi as I, pemeriksaan_ralan.evaluasi as E "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis left join pemeriksaan_ralan on pemeriksaan_ralan.no_rawat = reg_periksa.no_rawat where reg_periksa.no_rawat=? AND (pemeriksaan_ralan.no_rawat, pemeriksaan_ralan.tgl_perawatan, pemeriksaan_ralan.jam_rawat) IN " +
                    "(SELECT pr.no_rawat, pr.tgl_perawatan, MAX(pr.jam_rawat) AS jam_rawat FROM pemeriksaan_ralan pr where pr.nip ='"+nippj+"' group by pr.no_rawat, pr.tgl_perawatan )" );
            try {
                ps_soap.setString(1,TNoRw.getText());
                rs_soap=ps_soap.executeQuery();
                if(rs_soap.next()){
                 anamnesa.setText(rs_soap.getString("S"));
                 pemeriksaanfisik.setText(rs_soap.getString("O"));
                 diagnosamedis.setText(rs_soap.getString("A"));
                 diagnosafungsi.setText(rs_soap.getString("A"));
                 tatalaksana.setText(rs_soap.getString("P"));
                 anjuran.setText(rs_soap.getString("I"));
                 //evaluasi.setText(rs_soap.getString("E"));
                }
            } catch (Exception e) {
                System.out.println("Notif Soap : "+e);
            } finally{
                if(rs_soap!=null){
                    rs_soap.close();
                }
                if(ps_soap!=null){
                    ps_soap.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif Soap: "+e);
        }
        // END CUSTOM
    }
    
    private void isRawatrujukan() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.no_tlp,concat(pasien.alamat,' ',kel.nm_kel,', ',kec.nm_kec,', ',kab.nm_kab) as alamat,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis inner join kelurahan kel on pasien.kd_kel = kel.kd_kel inner join kecamatan kec on pasien.kd_kec = kec.kd_kec inner join kabupaten kab on pasien.kd_kab = kab.kd_kab where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    nm_pasien.setText(rs.getString("nm_pasien"));
                    alamat.setText(rs.getString("alamat"));
                    nohp.setText(rs.getString("no_tlp"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    Jk.setText(rs.getString("jk"));
                    tgl_lahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
        
        // START CUSTOM ANGGIT AMBIL SOAP TERAKHIR DARI DOKTER KFR
        try {
            String nippj=Sequel.cariIsi("select kd_dokterkfr from set_pjkfr");
            ps_soap=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi,pemeriksaan_ralan.no_rawat,pemeriksaan_ralan.tgl_perawatan,pemeriksaan_ralan.jam_rawat,pemeriksaan_ralan.keluhan as S,pemeriksaan_ralan.pemeriksaan as O, pemeriksaan_ralan.rtl as P, pemeriksaan_ralan.penilaian as A, pemeriksaan_ralan.instruksi as I, pemeriksaan_ralan.evaluasi as E "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis left join pemeriksaan_ralan on pemeriksaan_ralan.no_rawat = reg_periksa.no_rawat where reg_periksa.no_rawat=? AND (pemeriksaan_ralan.no_rawat, pemeriksaan_ralan.tgl_perawatan, pemeriksaan_ralan.jam_rawat) IN " +
                    "(SELECT pr.no_rawat, pr.tgl_perawatan, MAX(pr.jam_rawat) AS jam_rawat FROM pemeriksaan_ralan pr where pr.nip ='"+nippj+"' group by pr.no_rawat, pr.tgl_perawatan )" );
            try {
                ps_soap.setString(1,TNoRw.getText());
                rs_soap=ps_soap.executeQuery();
                if(rs_soap.next()){
                 anamnesa.setText(rs_soap.getString("S"));
                 pemeriksaanfisik.setText(rs_soap.getString("O"));
                 diagnosamedis.setText(rs_soap.getString("A"));
                 tatalaksana.setText(rs_soap.getString("P"));
                 anjuran.setText(rs_soap.getString("I"));
                 evaluasi.setText(rs_soap.getString("E"));
                }
            } catch (Exception e) {
                System.out.println("Notif Soap : "+e);
            } finally{
                if(rs_soap!=null){
                    rs_soap.close();
                }
                if(ps_soap!=null){
                    ps_soap.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif Soap: "+e);
        }
        // END CUSTOM
    }
    
    public void setNoRm(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);    
        isRawat(); 
        anjuran.append(" \n \n - Edukasi  : \n - Goal therapy : \n - Frekwensi kunjungan : \n - Lama rencana therapy : \n ");
        evaluasi.setText(" - Rencana Re-asesmen  : \n - Rencana Tindak Lanjut : \n \t- apabila belum sembuh dan gejala klinis berkurang dilanjutkan terapi paket selanjutnya  \n \t- apabila belum ada perbaikan sama sekali ataupun sudah sembuh, kembali ke dokter perujuk \n ");
    }
    
    public void setNoRmrujukan(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);    
        isRawatrujukan(); 
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_fisioterapi());
        BtnHapus.setEnabled(akses.getpenilaian_fisioterapi());
        BtnEdit.setEnabled(akses.getpenilaian_fisioterapi());
        BtnEdit.setEnabled(akses.getpenilaian_fisioterapi());
        if(akses.getjml2()>=1){
            KdPetugas.setEditable(false);
            BtnDokter.setEnabled(false);
            KdPetugas.setText(akses.getkode());
            Sequel.cariIsi("select nama from petugas where nip=?", NmPetugas,KdPetugas.getText());
            if(NmPetugas.getText().equals("")){
                KdPetugas.setText("");
                JOptionPane.showMessageDialog(null,"User login bukan petugas...!!");
            }
        }            
    }

    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }
    
    private void hapus() {
        if(Sequel.queryu2tf("delete from penilaian_layanan_kfr where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            tampil();
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }
    private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkKejadian.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkKejadian.isSelected()==false){
                    nilai_jam =Jam.getSelectedIndex();
                    nilai_menit =Menit.getSelectedIndex();
                    nilai_detik =Detik.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam.setSelectedItem(jam);
                Menit.setSelectedItem(menit);
                Detik.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    private void ganti() {
        if(Sequel.mengedittf("penilaian_layanan_kfr","no_rawat=?","no_rawat=?,tanggal=?,tanggal_pelayanan=?,jam_pelayanan=?,anamnesa=?,pemeriksaan_fisik=?,diagnosa_medis=?,diagnosa_fungsi=?,pemeriksaan_penunjang=?,tata_laksana=?,anjuran=?,evaluasi=?,suspek_penyakit=?,keterangan=?,nip=?",16,new String[]{
               TNoRw.getText(),
               Valid.SetTgl(Tanggal.getSelectedItem()+"")+" "+Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
               Valid.SetTgl(TanggalPelayanan.getSelectedItem()+""),
               Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
               anamnesa.getText(),
               pemeriksaanfisik.getText(),
               diagnosamedis.getText(),
               diagnosafungsi.getText(),
               penunjang.getText(),
               tatalaksana.getText(),
               anjuran.getText(),
               evaluasi.getText(),
               suspek_penyakit.getSelectedItem().toString(),
               keterangan.getText(),
               KdPetugas.getText(),    
               tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
             })==true){
                tampil();
                emptTeks();
                TabRawat.setSelectedIndex(1);
        }
    }
    
    // JAM 2
    private void jam2(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam1;
            private int nilai_menit1;
            private int nilai_detik1;
            public void actionPerformed(ActionEvent e) {
                String nol_jam1 = "";
                String nol_menit1 = "";
                String nol_detik1 = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkKejadian1.isSelected()==true){
                    nilai_jam1 = now.getHours();
                    nilai_menit1 = now.getMinutes();
                    nilai_detik1 = now.getSeconds();
                }else if(ChkKejadian1.isSelected()==false){
                    nilai_jam1 =Jam1.getSelectedIndex();
                    nilai_menit1 =Menit1.getSelectedIndex();
                    nilai_detik1 =Detik1.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam1 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam1 = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit1 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit1 = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik1 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik1 = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam1 + Integer.toString(nilai_jam1);
                String menit = nol_menit1 + Integer.toString(nilai_menit1);
                String detik = nol_detik1 + Integer.toString(nilai_detik1);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam1.setSelectedItem(jam);
                Menit1.setSelectedItem(menit);
                Detik1.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
}
