/*
 * Kontribusi dari Anggit Nurhidayah, RSUP Surakarta
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;

/**
 *
 * @author perpustakaan
 */
public final class RMPneumoniaSeverityIndex extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariDokter dpjp=new DlgCariDokter(null,false);
    private StringBuilder htmlContent;
    private String finger="",finger_dpjp="";
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMPneumoniaSeverityIndex(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tgl.Lahir","J.K.",
            "NIP Dokter","Nama Dokter","NIP DPJP","Nama DPJP","Tanggal",
            "Skor Usia","Skor Jenis Kelamin","Skor Panti Werda","Skor Keganasan","Skor Penyakit Hati",
            "Skor Penyakit Jantung","Skor Penyakit Serebro","Skor Penyakit Ginjal","Skor Gangguan Kesadaran","Skor Frekuensi Nafas",
            "Skor Sistolik","Skor Suhu Tubuh","Skor Nadi","Skor pH","Skor Ureum",
            "Skor Natrium","Skor Glukosa","Skor Hematokrit","Skor Tekanan O2","Skor Efusi Pleura",
            "Total"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        
        tbObat.setModel(tabMode);
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 31; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(150);
            }else if(i==3){
                column.setPreferredWidth(65);
            }else if(i==4){
                column.setPreferredWidth(55);
            }else if(i==5){
                column.setPreferredWidth(100);
            }else if(i==6){
                column.setPreferredWidth(100);
            }else if(i==7){
                column.setPreferredWidth(100);
            }else if(i==8){
                column.setPreferredWidth(100);
            }else if(i==9){
                column.setPreferredWidth(100);
            }else {
                column.setPreferredWidth(100);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        usia.setDocument(new batasInput((int)3).getOnlyAngka(usia));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){
                    KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                    KdDokter.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        dpjp.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dpjp.getTable().getSelectedRow()!= -1){
                    KdDPJP.setText(dpjp.getTable().getValueAt(dpjp.getTable().getSelectedRow(),0).toString());
                    NmDPJP.setText(dpjp.getTable().getValueAt(dpjp.getTable().getSelectedRow(),1).toString());
                    KdDPJP.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnCetak = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        KdDokter = new widget.TextBox();
        NmDokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        jLabel11 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        label11 = new widget.Label();
        TglPemeriksaan = new widget.Tanggal();
        label15 = new widget.Label();
        total = new widget.TextBox();
        jLabel108 = new widget.Label();
        jLabel111 = new widget.Label();
        label16 = new widget.Label();
        KdDPJP = new widget.TextBox();
        NmDPJP = new widget.TextBox();
        BtnDPJP = new widget.Button();
        jLabel122 = new widget.Label();
        usia = new widget.TextBox();
        jLabel123 = new widget.Label();
        skor_jenis_kelamin = new widget.TextBox();
        jLabel124 = new widget.Label();
        panti_werda = new widget.ComboBox();
        skor_panti_werda = new widget.TextBox();
        jLabel112 = new widget.Label();
        jLabel125 = new widget.Label();
        keganasan = new widget.ComboBox();
        skor_keganasan = new widget.TextBox();
        jLabel126 = new widget.Label();
        penyakit_hati = new widget.ComboBox();
        skor_penyakit_hati = new widget.TextBox();
        jLabel127 = new widget.Label();
        penyakit_jantung = new widget.ComboBox();
        skor_penyakit_jantung = new widget.TextBox();
        jLabel128 = new widget.Label();
        penyakit_serebro = new widget.ComboBox();
        skor_penyakit_serebro = new widget.TextBox();
        jLabel129 = new widget.Label();
        penyakit_ginjal = new widget.ComboBox();
        skor_penyakit_ginjal = new widget.TextBox();
        jLabel113 = new widget.Label();
        jLabel130 = new widget.Label();
        gangguan_kesadaran = new widget.ComboBox();
        skor_gangguan_kesadaran = new widget.TextBox();
        jLabel131 = new widget.Label();
        frekuensi_nafas = new widget.ComboBox();
        skor_frekuensi_nafas = new widget.TextBox();
        jLabel132 = new widget.Label();
        sistolik = new widget.ComboBox();
        skor_sistolik = new widget.TextBox();
        jLabel133 = new widget.Label();
        suhu_tubuh = new widget.ComboBox();
        skor_suhu_tubuh = new widget.TextBox();
        jLabel134 = new widget.Label();
        nadi = new widget.ComboBox();
        skor_nadi = new widget.TextBox();
        jLabel114 = new widget.Label();
        jLabel135 = new widget.Label();
        ph = new widget.ComboBox();
        skor_ph = new widget.TextBox();
        skor_ureum = new widget.TextBox();
        ureum = new widget.ComboBox();
        jLabel136 = new widget.Label();
        skor_natrium = new widget.TextBox();
        natrium = new widget.ComboBox();
        jLabel137 = new widget.Label();
        jLabel138 = new widget.Label();
        glukosa = new widget.ComboBox();
        skor_glukosa = new widget.TextBox();
        jLabel139 = new widget.Label();
        hematokrit = new widget.ComboBox();
        skor_hematokrit = new widget.TextBox();
        jLabel140 = new widget.Label();
        tekanan_o2 = new widget.ComboBox();
        skor_tekanan_o2 = new widget.TextBox();
        jLabel141 = new widget.Label();
        efusi_pleura = new widget.ComboBox();
        skor_efusi_pleura = new widget.TextBox();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnCetak.setBackground(new java.awt.Color(255, 255, 254));
        MnCetak.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetak.setForeground(new java.awt.Color(50, 50, 50));
        MnCetak.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetak.setText("Cetak Pneumonia Severity Index");
        MnCetak.setActionCommand("Cetak Skoring CURB65");
        MnCetak.setName("MnCetak"); // NOI18N
        MnCetak.setPreferredSize(new java.awt.Dimension(220, 26));
        MnCetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetak);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Pneumonia Severity Index (PSI) ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(920, 900));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        KdDokter.setEditable(false);
        KdDokter.setName("KdDokter"); // NOI18N
        KdDokter.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokterKeyPressed(evt);
            }
        });
        FormInput.add(KdDokter);
        KdDokter.setBounds(74, 40, 90, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        NmDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmDokter);
        NmDokter.setBounds(166, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(348, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(640, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 100, 880, 2);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(380, 40, 52, 23);

        TglPemeriksaan.setForeground(new java.awt.Color(50, 70, 50));
        TglPemeriksaan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "06-12-2024 08:34:05" }));
        TglPemeriksaan.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglPemeriksaan.setName("TglPemeriksaan"); // NOI18N
        TglPemeriksaan.setOpaque(false);
        TglPemeriksaan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglPemeriksaanKeyPressed(evt);
            }
        });
        FormInput.add(TglPemeriksaan);
        TglPemeriksaan.setBounds(436, 40, 130, 23);

        label15.setText("Dokter :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label15);
        label15.setBounds(0, 40, 70, 23);

        total.setHighlighter(null);
        total.setName("total"); // NOI18N
        total.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                totalInputMethodTextChanged(evt);
            }
        });
        total.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalActionPerformed(evt);
            }
        });
        FormInput.add(total);
        total.setBounds(450, 820, 80, 23);

        jLabel108.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel108.setText("Total Skor :");
        jLabel108.setName("jLabel108"); // NOI18N
        FormInput.add(jLabel108);
        jLabel108.setBounds(380, 820, 60, 30);

        jLabel111.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel111.setText("FAKTOR DEMOGRAFIK");
        jLabel111.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel111.setName("jLabel111"); // NOI18N
        FormInput.add(jLabel111);
        jLabel111.setBounds(40, 100, 140, 30);

        label16.setText("DPJP :");
        label16.setName("label16"); // NOI18N
        label16.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label16);
        label16.setBounds(0, 70, 70, 23);

        KdDPJP.setEditable(false);
        KdDPJP.setName("KdDPJP"); // NOI18N
        KdDPJP.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDPJP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDPJPKeyPressed(evt);
            }
        });
        FormInput.add(KdDPJP);
        KdDPJP.setBounds(74, 70, 90, 23);

        NmDPJP.setEditable(false);
        NmDPJP.setName("NmDPJP"); // NOI18N
        NmDPJP.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmDPJP);
        NmDPJP.setBounds(166, 70, 180, 23);

        BtnDPJP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDPJP.setMnemonic('2');
        BtnDPJP.setToolTipText("Alt+2");
        BtnDPJP.setName("BtnDPJP"); // NOI18N
        BtnDPJP.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDPJP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDPJPActionPerformed(evt);
            }
        });
        BtnDPJP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDPJPKeyPressed(evt);
            }
        });
        FormInput.add(BtnDPJP);
        BtnDPJP.setBounds(348, 70, 28, 23);

        jLabel122.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel122.setText("Usia");
        jLabel122.setName("jLabel122"); // NOI18N
        FormInput.add(jLabel122);
        jLabel122.setBounds(30, 120, 150, 30);

        usia.setEditable(false);
        usia.setHighlighter(null);
        usia.setName("usia"); // NOI18N
        usia.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                usiaInputMethodTextChanged(evt);
            }
        });
        FormInput.add(usia);
        usia.setBounds(220, 120, 80, 23);

        jLabel123.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel123.setText("Jenis kelamin");
        jLabel123.setName("jLabel123"); // NOI18N
        FormInput.add(jLabel123);
        jLabel123.setBounds(30, 150, 150, 30);

        skor_jenis_kelamin.setEditable(false);
        skor_jenis_kelamin.setHighlighter(null);
        skor_jenis_kelamin.setName("skor_jenis_kelamin"); // NOI18N
        skor_jenis_kelamin.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_jenis_kelaminInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_jenis_kelamin);
        skor_jenis_kelamin.setBounds(220, 150, 80, 23);

        jLabel124.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel124.setText("Penghuni panti werda");
        jLabel124.setName("jLabel124"); // NOI18N
        FormInput.add(jLabel124);
        jLabel124.setBounds(30, 180, 150, 30);

        panti_werda.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        panti_werda.setName("panti_werda"); // NOI18N
        panti_werda.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                panti_werdaItemStateChanged(evt);
            }
        });
        panti_werda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                panti_werdaKeyPressed(evt);
            }
        });
        FormInput.add(panti_werda);
        panti_werda.setBounds(220, 180, 210, 23);

        skor_panti_werda.setEditable(false);
        skor_panti_werda.setHighlighter(null);
        skor_panti_werda.setName("skor_panti_werda"); // NOI18N
        skor_panti_werda.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_panti_werdaInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_panti_werda);
        skor_panti_werda.setBounds(450, 180, 80, 23);

        jLabel112.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel112.setText("PENYAKIT KOMORBID");
        jLabel112.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel112.setName("jLabel112"); // NOI18N
        FormInput.add(jLabel112);
        jLabel112.setBounds(40, 210, 140, 30);

        jLabel125.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel125.setText("Keganasan");
        jLabel125.setName("jLabel125"); // NOI18N
        FormInput.add(jLabel125);
        jLabel125.setBounds(30, 240, 150, 30);

        keganasan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        keganasan.setName("keganasan"); // NOI18N
        keganasan.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                keganasanItemStateChanged(evt);
            }
        });
        keganasan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                keganasanKeyPressed(evt);
            }
        });
        FormInput.add(keganasan);
        keganasan.setBounds(220, 240, 210, 23);

        skor_keganasan.setEditable(false);
        skor_keganasan.setHighlighter(null);
        skor_keganasan.setName("skor_keganasan"); // NOI18N
        skor_keganasan.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_keganasanInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_keganasan);
        skor_keganasan.setBounds(450, 240, 80, 23);

        jLabel126.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel126.setText("Penyakit hati");
        jLabel126.setName("jLabel126"); // NOI18N
        FormInput.add(jLabel126);
        jLabel126.setBounds(30, 270, 150, 30);

        penyakit_hati.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        penyakit_hati.setName("penyakit_hati"); // NOI18N
        penyakit_hati.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                penyakit_hatiItemStateChanged(evt);
            }
        });
        penyakit_hati.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                penyakit_hatiKeyPressed(evt);
            }
        });
        FormInput.add(penyakit_hati);
        penyakit_hati.setBounds(220, 270, 210, 23);

        skor_penyakit_hati.setEditable(false);
        skor_penyakit_hati.setHighlighter(null);
        skor_penyakit_hati.setName("skor_penyakit_hati"); // NOI18N
        skor_penyakit_hati.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_penyakit_hatiInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_penyakit_hati);
        skor_penyakit_hati.setBounds(450, 270, 80, 23);

        jLabel127.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel127.setText("Penyakit jantung kongestif");
        jLabel127.setName("jLabel127"); // NOI18N
        FormInput.add(jLabel127);
        jLabel127.setBounds(30, 300, 150, 30);

        penyakit_jantung.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        penyakit_jantung.setName("penyakit_jantung"); // NOI18N
        penyakit_jantung.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                penyakit_jantungItemStateChanged(evt);
            }
        });
        penyakit_jantung.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                penyakit_jantungKeyPressed(evt);
            }
        });
        FormInput.add(penyakit_jantung);
        penyakit_jantung.setBounds(220, 300, 210, 23);

        skor_penyakit_jantung.setEditable(false);
        skor_penyakit_jantung.setHighlighter(null);
        skor_penyakit_jantung.setName("skor_penyakit_jantung"); // NOI18N
        skor_penyakit_jantung.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_penyakit_jantungInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_penyakit_jantung);
        skor_penyakit_jantung.setBounds(450, 300, 80, 23);

        jLabel128.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel128.setText("Penyakit serebro vaskular");
        jLabel128.setName("jLabel128"); // NOI18N
        FormInput.add(jLabel128);
        jLabel128.setBounds(30, 330, 150, 30);

        penyakit_serebro.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        penyakit_serebro.setName("penyakit_serebro"); // NOI18N
        penyakit_serebro.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                penyakit_serebroItemStateChanged(evt);
            }
        });
        penyakit_serebro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                penyakit_serebroKeyPressed(evt);
            }
        });
        FormInput.add(penyakit_serebro);
        penyakit_serebro.setBounds(220, 330, 210, 23);

        skor_penyakit_serebro.setEditable(false);
        skor_penyakit_serebro.setHighlighter(null);
        skor_penyakit_serebro.setName("skor_penyakit_serebro"); // NOI18N
        skor_penyakit_serebro.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_penyakit_serebroInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_penyakit_serebro);
        skor_penyakit_serebro.setBounds(450, 330, 80, 23);

        jLabel129.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel129.setText("Penyakit ginjal");
        jLabel129.setName("jLabel129"); // NOI18N
        FormInput.add(jLabel129);
        jLabel129.setBounds(30, 360, 150, 30);

        penyakit_ginjal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        penyakit_ginjal.setName("penyakit_ginjal"); // NOI18N
        penyakit_ginjal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                penyakit_ginjalItemStateChanged(evt);
            }
        });
        penyakit_ginjal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                penyakit_ginjalKeyPressed(evt);
            }
        });
        FormInput.add(penyakit_ginjal);
        penyakit_ginjal.setBounds(220, 360, 210, 23);

        skor_penyakit_ginjal.setEditable(false);
        skor_penyakit_ginjal.setHighlighter(null);
        skor_penyakit_ginjal.setName("skor_penyakit_ginjal"); // NOI18N
        skor_penyakit_ginjal.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_penyakit_ginjalInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_penyakit_ginjal);
        skor_penyakit_ginjal.setBounds(450, 360, 80, 23);

        jLabel113.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel113.setText("PEMERIKSAAN FISIK");
        jLabel113.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel113.setName("jLabel113"); // NOI18N
        FormInput.add(jLabel113);
        jLabel113.setBounds(40, 390, 140, 30);

        jLabel130.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel130.setText("Gangguan kesadaran");
        jLabel130.setName("jLabel130"); // NOI18N
        FormInput.add(jLabel130);
        jLabel130.setBounds(30, 420, 150, 30);

        gangguan_kesadaran.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        gangguan_kesadaran.setName("gangguan_kesadaran"); // NOI18N
        gangguan_kesadaran.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                gangguan_kesadaranItemStateChanged(evt);
            }
        });
        gangguan_kesadaran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                gangguan_kesadaranKeyPressed(evt);
            }
        });
        FormInput.add(gangguan_kesadaran);
        gangguan_kesadaran.setBounds(220, 420, 210, 23);

        skor_gangguan_kesadaran.setEditable(false);
        skor_gangguan_kesadaran.setHighlighter(null);
        skor_gangguan_kesadaran.setName("skor_gangguan_kesadaran"); // NOI18N
        skor_gangguan_kesadaran.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_gangguan_kesadaranInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_gangguan_kesadaran);
        skor_gangguan_kesadaran.setBounds(450, 420, 80, 23);

        jLabel131.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel131.setText("Frekuensi nafas > 30 x/menit");
        jLabel131.setName("jLabel131"); // NOI18N
        FormInput.add(jLabel131);
        jLabel131.setBounds(30, 450, 150, 30);

        frekuensi_nafas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        frekuensi_nafas.setName("frekuensi_nafas"); // NOI18N
        frekuensi_nafas.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                frekuensi_nafasItemStateChanged(evt);
            }
        });
        frekuensi_nafas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                frekuensi_nafasKeyPressed(evt);
            }
        });
        FormInput.add(frekuensi_nafas);
        frekuensi_nafas.setBounds(220, 450, 210, 23);

        skor_frekuensi_nafas.setEditable(false);
        skor_frekuensi_nafas.setHighlighter(null);
        skor_frekuensi_nafas.setName("skor_frekuensi_nafas"); // NOI18N
        skor_frekuensi_nafas.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_frekuensi_nafasInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_frekuensi_nafas);
        skor_frekuensi_nafas.setBounds(450, 450, 80, 23);

        jLabel132.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel132.setText("Tekanan darah sistolik < 90 mmHg");
        jLabel132.setName("jLabel132"); // NOI18N
        FormInput.add(jLabel132);
        jLabel132.setBounds(30, 480, 170, 30);

        sistolik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        sistolik.setName("sistolik"); // NOI18N
        sistolik.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sistolikItemStateChanged(evt);
            }
        });
        sistolik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sistolikKeyPressed(evt);
            }
        });
        FormInput.add(sistolik);
        sistolik.setBounds(220, 480, 210, 23);

        skor_sistolik.setEditable(false);
        skor_sistolik.setHighlighter(null);
        skor_sistolik.setName("skor_sistolik"); // NOI18N
        skor_sistolik.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_sistolikInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_sistolik);
        skor_sistolik.setBounds(450, 480, 80, 23);

        jLabel133.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel133.setText("Suhu tubuh < 30 °C atau > 40 °C");
        jLabel133.setName("jLabel133"); // NOI18N
        FormInput.add(jLabel133);
        jLabel133.setBounds(30, 510, 170, 30);

        suhu_tubuh.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        suhu_tubuh.setName("suhu_tubuh"); // NOI18N
        suhu_tubuh.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                suhu_tubuhItemStateChanged(evt);
            }
        });
        suhu_tubuh.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                suhu_tubuhKeyPressed(evt);
            }
        });
        FormInput.add(suhu_tubuh);
        suhu_tubuh.setBounds(220, 510, 210, 23);

        skor_suhu_tubuh.setEditable(false);
        skor_suhu_tubuh.setHighlighter(null);
        skor_suhu_tubuh.setName("skor_suhu_tubuh"); // NOI18N
        skor_suhu_tubuh.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_suhu_tubuhInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_suhu_tubuh);
        skor_suhu_tubuh.setBounds(450, 510, 80, 23);

        jLabel134.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel134.setText("Frekuensi nadi > 125 x/menit");
        jLabel134.setName("jLabel134"); // NOI18N
        FormInput.add(jLabel134);
        jLabel134.setBounds(30, 540, 170, 30);

        nadi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        nadi.setName("nadi"); // NOI18N
        nadi.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nadiItemStateChanged(evt);
            }
        });
        nadi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nadiKeyPressed(evt);
            }
        });
        FormInput.add(nadi);
        nadi.setBounds(220, 540, 210, 23);

        skor_nadi.setEditable(false);
        skor_nadi.setHighlighter(null);
        skor_nadi.setName("skor_nadi"); // NOI18N
        skor_nadi.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_nadiInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_nadi);
        skor_nadi.setBounds(450, 540, 80, 23);

        jLabel114.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel114.setText("HASIL LABORATORIUM");
        jLabel114.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel114.setName("jLabel114"); // NOI18N
        FormInput.add(jLabel114);
        jLabel114.setBounds(40, 570, 140, 30);

        jLabel135.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel135.setText("pH < 7.35");
        jLabel135.setName("jLabel135"); // NOI18N
        FormInput.add(jLabel135);
        jLabel135.setBounds(30, 600, 150, 30);

        ph.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        ph.setName("ph"); // NOI18N
        ph.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                phItemStateChanged(evt);
            }
        });
        ph.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                phKeyPressed(evt);
            }
        });
        FormInput.add(ph);
        ph.setBounds(220, 600, 210, 23);

        skor_ph.setEditable(false);
        skor_ph.setHighlighter(null);
        skor_ph.setName("skor_ph"); // NOI18N
        skor_ph.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_phInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_ph);
        skor_ph.setBounds(450, 600, 80, 23);

        skor_ureum.setEditable(false);
        skor_ureum.setHighlighter(null);
        skor_ureum.setName("skor_ureum"); // NOI18N
        skor_ureum.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_ureumInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_ureum);
        skor_ureum.setBounds(450, 630, 80, 23);

        ureum.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        ureum.setName("ureum"); // NOI18N
        ureum.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ureumItemStateChanged(evt);
            }
        });
        ureum.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ureumKeyPressed(evt);
            }
        });
        FormInput.add(ureum);
        ureum.setBounds(220, 630, 210, 23);

        jLabel136.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel136.setText("Ureum > 64,2 mg/dL");
        jLabel136.setName("jLabel136"); // NOI18N
        FormInput.add(jLabel136);
        jLabel136.setBounds(30, 630, 150, 30);

        skor_natrium.setEditable(false);
        skor_natrium.setHighlighter(null);
        skor_natrium.setName("skor_natrium"); // NOI18N
        skor_natrium.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_natriumInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_natrium);
        skor_natrium.setBounds(450, 660, 80, 23);

        natrium.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        natrium.setName("natrium"); // NOI18N
        natrium.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                natriumItemStateChanged(evt);
            }
        });
        natrium.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                natriumKeyPressed(evt);
            }
        });
        FormInput.add(natrium);
        natrium.setBounds(220, 660, 210, 23);

        jLabel137.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel137.setText("Natrium < 130 mEq/L");
        jLabel137.setName("jLabel137"); // NOI18N
        FormInput.add(jLabel137);
        jLabel137.setBounds(30, 660, 150, 30);

        jLabel138.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel138.setText("Glukosa > 250 mg/dL");
        jLabel138.setName("jLabel138"); // NOI18N
        FormInput.add(jLabel138);
        jLabel138.setBounds(30, 690, 150, 30);

        glukosa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        glukosa.setName("glukosa"); // NOI18N
        glukosa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                glukosaItemStateChanged(evt);
            }
        });
        glukosa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                glukosaKeyPressed(evt);
            }
        });
        FormInput.add(glukosa);
        glukosa.setBounds(220, 690, 210, 23);

        skor_glukosa.setEditable(false);
        skor_glukosa.setHighlighter(null);
        skor_glukosa.setName("skor_glukosa"); // NOI18N
        skor_glukosa.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_glukosaInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_glukosa);
        skor_glukosa.setBounds(450, 690, 80, 23);

        jLabel139.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel139.setText("Hematokrit < 30%");
        jLabel139.setName("jLabel139"); // NOI18N
        FormInput.add(jLabel139);
        jLabel139.setBounds(30, 720, 150, 30);

        hematokrit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        hematokrit.setName("hematokrit"); // NOI18N
        hematokrit.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                hematokritItemStateChanged(evt);
            }
        });
        hematokrit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                hematokritKeyPressed(evt);
            }
        });
        FormInput.add(hematokrit);
        hematokrit.setBounds(220, 720, 210, 23);

        skor_hematokrit.setEditable(false);
        skor_hematokrit.setHighlighter(null);
        skor_hematokrit.setName("skor_hematokrit"); // NOI18N
        skor_hematokrit.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_hematokritInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_hematokrit);
        skor_hematokrit.setBounds(450, 720, 80, 23);

        jLabel140.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel140.setText("Tekanan O₂ darah arteri < 60 mmHg");
        jLabel140.setName("jLabel140"); // NOI18N
        FormInput.add(jLabel140);
        jLabel140.setBounds(30, 750, 180, 30);

        tekanan_o2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        tekanan_o2.setName("tekanan_o2"); // NOI18N
        tekanan_o2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                tekanan_o2ItemStateChanged(evt);
            }
        });
        tekanan_o2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tekanan_o2KeyPressed(evt);
            }
        });
        FormInput.add(tekanan_o2);
        tekanan_o2.setBounds(220, 750, 210, 23);

        skor_tekanan_o2.setEditable(false);
        skor_tekanan_o2.setHighlighter(null);
        skor_tekanan_o2.setName("skor_tekanan_o2"); // NOI18N
        skor_tekanan_o2.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_tekanan_o2InputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_tekanan_o2);
        skor_tekanan_o2.setBounds(450, 750, 80, 23);

        jLabel141.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel141.setText("Efusi pleura");
        jLabel141.setName("jLabel141"); // NOI18N
        FormInput.add(jLabel141);
        jLabel141.setBounds(30, 780, 180, 30);

        efusi_pleura.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        efusi_pleura.setName("efusi_pleura"); // NOI18N
        efusi_pleura.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                efusi_pleuraItemStateChanged(evt);
            }
        });
        efusi_pleura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                efusi_pleuraKeyPressed(evt);
            }
        });
        FormInput.add(efusi_pleura);
        efusi_pleura.setBounds(220, 780, 210, 23);

        skor_efusi_pleura.setEditable(false);
        skor_efusi_pleura.setHighlighter(null);
        skor_efusi_pleura.setName("skor_efusi_pleura"); // NOI18N
        skor_efusi_pleura.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                skor_efusi_pleuraInputMethodTextChanged(evt);
            }
        });
        FormInput.add(skor_efusi_pleura);
        skor_efusi_pleura.setBounds(450, 780, 80, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Penilaian", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "06-12-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "06-12-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Penilaian", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{            
            Valid.pindah(evt,TCari,BtnDokter);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(KdDokter.getText().trim().equals("")){
            Valid.textKosong(KdDokter,"Dokter Umum");
        }else if(KdDPJP.getText().trim().equals("")){
            Valid.textKosong(KdDPJP,"DPJP");
        }else if(usia.getText().equals("")){
            Valid.textKosong(usia,"Usia");
        }else if(panti_werda.getSelectedIndex()==0){
            Valid.textKosong(panti_werda,"Panti werda");
        }else if(keganasan.getSelectedIndex()==0){
            Valid.textKosong(keganasan,"Keganasan");
        }else if(penyakit_hati.getSelectedIndex()==0){
            Valid.textKosong(penyakit_hati,"Penyakit hati");
        }else if(penyakit_jantung.getSelectedIndex()==0){
            Valid.textKosong(penyakit_jantung,"Penyakit jantung kongestif");
        }else if(penyakit_serebro.getSelectedIndex()==0){
            Valid.textKosong(penyakit_serebro,"Penyakit serebro vaskuler");
        }else if(penyakit_ginjal.getSelectedIndex()==0){
            Valid.textKosong(penyakit_ginjal,"Penyakit ginjal");
        }else if(gangguan_kesadaran.getSelectedIndex()==0){
            Valid.textKosong(gangguan_kesadaran,"Gangguan kesadaran");
        }else if(frekuensi_nafas.getSelectedIndex()==0){
            Valid.textKosong(frekuensi_nafas,"Frekuensi nafas");
        }else if(sistolik.getSelectedIndex()==0){
            Valid.textKosong(sistolik,"Tekanan darah sistolik");
        }else if(suhu_tubuh.getSelectedIndex()==0){
            Valid.textKosong(suhu_tubuh,"Suhu_tubuh");
        }else if(nadi.getSelectedIndex()==0){
            Valid.textKosong(nadi,"Frekuensi nadi");
        }else if(ph.getSelectedIndex()==0){
            Valid.textKosong(ph,"pH");
        }else if(ureum.getSelectedIndex()==0){
            Valid.textKosong(ureum,"Ureum");
        }else if(natrium.getSelectedIndex()==0){
            Valid.textKosong(natrium,"Natrium");
        }else if(glukosa.getSelectedIndex()==0){
            Valid.textKosong(glukosa,"Glukosa");
        }else if(hematokrit.getSelectedIndex()==0){
            Valid.textKosong(hematokrit,"Hematokrit");
        }else if(tekanan_o2.getSelectedIndex()==0){
            Valid.textKosong(tekanan_o2,"Tekanan O2 darah arteri");
        }else if(efusi_pleura.getSelectedIndex()==0){
            Valid.textKosong(efusi_pleura,"Efusi pleura");
        }else{
            if(Sequel.menyimpantf("penilaian_pneumonia_severity","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",25,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),
                KdDPJP.getText(),
                usia.getText(),
                skor_jenis_kelamin.getText(),
                skor_panti_werda.getText(),
                skor_keganasan.getText(),
                skor_penyakit_hati.getText(),
                skor_penyakit_jantung.getText(),
                skor_penyakit_serebro.getText(),
                skor_penyakit_ginjal.getText(),
                skor_gangguan_kesadaran.getText(),
                skor_frekuensi_nafas.getText(),
                skor_sistolik.getText(),
                skor_suhu_tubuh.getText(),
                skor_nadi.getText(),
                skor_ph.getText(),
                skor_ureum.getText(),
                skor_natrium.getText(),
                skor_glukosa.getText(),
                skor_hematokrit.getText(),
                skor_tekanan_o2.getText(),
                skor_efusi_pleura.getText(),
                total.getText()
                })==true){
                    JOptionPane.showMessageDialog(rootPane,"Data berhasil disimpan!");
                    emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,total,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh dokter/petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }  
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(KdDokter.getText().trim().equals("")){
            Valid.textKosong(KdDokter,"Dokter Umum");
        }else if(KdDPJP.getText().trim().equals("")){
            Valid.textKosong(KdDPJP,"DPJP");
        }else if(usia.getText().equals("")){
            Valid.textKosong(usia,"Usia");
        }else if(panti_werda.getSelectedIndex()==0){
            Valid.textKosong(panti_werda,"Panti werda");
        }else if(keganasan.getSelectedIndex()==0){
            Valid.textKosong(keganasan,"Keganasan");
        }else if(penyakit_hati.getSelectedIndex()==0){
            Valid.textKosong(penyakit_hati,"Penyakit hati");
        }else if(penyakit_jantung.getSelectedIndex()==0){
            Valid.textKosong(penyakit_jantung,"Penyakit jantung kongestif");
        }else if(penyakit_serebro.getSelectedIndex()==0){
            Valid.textKosong(penyakit_serebro,"Penyakit serebro vaskuler");
        }else if(penyakit_ginjal.getSelectedIndex()==0){
            Valid.textKosong(penyakit_ginjal,"Penyakit ginjal");
        }else if(gangguan_kesadaran.getSelectedIndex()==0){
            Valid.textKosong(gangguan_kesadaran,"Gangguan kesadaran");
        }else if(frekuensi_nafas.getSelectedIndex()==0){
            Valid.textKosong(frekuensi_nafas,"Frekuensi nafas");
        }else if(sistolik.getSelectedIndex()==0){
            Valid.textKosong(sistolik,"Tekanan darah sistolik");
        }else if(suhu_tubuh.getSelectedIndex()==0){
            Valid.textKosong(suhu_tubuh,"Suhu_tubuh");
        }else if(nadi.getSelectedIndex()==0){
            Valid.textKosong(nadi,"Frekuensi nadi");
        }else if(ph.getSelectedIndex()==0){
            Valid.textKosong(ph,"pH");
        }else if(ureum.getSelectedIndex()==0){
            Valid.textKosong(ureum,"Ureum");
        }else if(natrium.getSelectedIndex()==0){
            Valid.textKosong(natrium,"Natrium");
        }else if(glukosa.getSelectedIndex()==0){
            Valid.textKosong(glukosa,"Glukosa");
        }else if(hematokrit.getSelectedIndex()==0){
            Valid.textKosong(hematokrit,"Hematokrit");
        }else if(tekanan_o2.getSelectedIndex()==0){
            Valid.textKosong(tekanan_o2,"Tekanan O2 darah arteri");
        }else if(efusi_pleura.getSelectedIndex()==0){
            Valid.textKosong(efusi_pleura,"Efusi pleura");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    ganti();
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }
        }    
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed

}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void KdDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokterKeyPressed
        
    }//GEN-LAST:event_KdDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setAlwaysOnTop(false);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        //Valid.pindah(evt,Monitoring,BtnSimpan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void TglPemeriksaanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglPemeriksaanKeyPressed
  
    }//GEN-LAST:event_TglPemeriksaanKeyPressed

    private void MnCetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakActionPerformed
      if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            finger_dpjp=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),5).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString()));
            param.put("finger_dpjp","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),8).toString()+"\nID "+(finger_dpjp.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),7).toString():finger_dpjp)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString()));
            Valid.MyReportqry("rptCetakPneumoniaSeverityIndex.jasper","report","::[ Pneumonia Severity Index (PSI) ]::",
                "select psi.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk,CONCAT( p.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) AS alamat,reg_periksa.umurdaftar,reg_periksa.sttsumur, " +
                "p.tgl_lahir,psi.kd_dokter,d.nm_dokter,psi.tanggal,psi.kd_dpjp,dpjp.nm_dokter as nm_dpjp,"+ 
                "psi.skor_usia,psi.skor_jenis_kelamin,psi.skor_panti_werda,psi.skor_keganasan,psi.skor_penyakit_hati,psi.skor_penyakit_jantung,psi.skor_penyakit_serebro,psi.skor_penyakit_ginjal,psi.skor_gangguan_kesadaran,psi.skor_frekuensi_nafas, "+
                "psi.skor_sistolik,psi.skor_suhu_tubuh,psi.skor_nadi,psi.skor_ph,psi.skor_ureum,psi.skor_natrium,skor_glukosa,psi.skor_hematokrit,skor_tekanan_o2,skor_efusi_pleura,psi.total " +
                "from reg_periksa " +
                "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                "inner join penilaian_pneumonia_severity psi on reg_periksa.no_rawat = psi.no_rawat " +
                "inner join dokter d on psi.kd_dokter = d.kd_dokter " +
                "inner join dokter dpjp on psi.kd_dpjp= dpjp.kd_dokter " +        
                "INNER JOIN kelurahan ON kelurahan.kd_kel = p.kd_kel " +
                "INNER JOIN kecamatan ON kecamatan.kd_kec = p.kd_kec " +
                "INNER JOIN kabupaten ON kabupaten.kd_kab = p.kd_kab " +
                "where psi.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and psi.tanggal='"+tbObat.getValueAt(tbObat.getSelectedRow(),9).toString()+"' ",param);
        }    
    }//GEN-LAST:event_MnCetakActionPerformed

    private void totalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_totalActionPerformed

    private void totalInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_totalInputMethodTextChanged
      isjml();
    }//GEN-LAST:event_totalInputMethodTextChanged

    private void KdDPJPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDPJPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdDPJPKeyPressed

    private void BtnDPJPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDPJPActionPerformed
        // TODO add your handling code here:
        dpjp.isCek();
        dpjp.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dpjp.setLocationRelativeTo(internalFrame1);
        dpjp.setAlwaysOnTop(false);
        dpjp.setVisible(true);
    }//GEN-LAST:event_BtnDPJPActionPerformed

    private void BtnDPJPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDPJPKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnDPJPKeyPressed

    private void skor_jenis_kelaminInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_jenis_kelaminInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_jenis_kelaminInputMethodTextChanged

    private void panti_werdaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_panti_werdaItemStateChanged
        // TODO add your handling code here:
        if(panti_werda.getSelectedIndex()==0){
            skor_panti_werda.setText("0");
        }else if(panti_werda.getSelectedIndex()==1){
            skor_panti_werda.setText("10");
        }else if(panti_werda.getSelectedIndex()==2){
            skor_panti_werda.setText("0");
        }        
        isjml();
    }//GEN-LAST:event_panti_werdaItemStateChanged

    private void panti_werdaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_panti_werdaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_panti_werdaKeyPressed

    private void skor_panti_werdaInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_panti_werdaInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_panti_werdaInputMethodTextChanged

    private void keganasanItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_keganasanItemStateChanged
        // TODO add your handling code here:
        if(keganasan.getSelectedIndex()==0){
            skor_keganasan.setText("0");
        }else if(keganasan.getSelectedIndex()==1){
            skor_keganasan.setText("10");
        }else if(keganasan.getSelectedIndex()==2){
            skor_keganasan.setText("0");
        }  
        isjml();
    }//GEN-LAST:event_keganasanItemStateChanged

    private void keganasanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_keganasanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_keganasanKeyPressed

    private void skor_keganasanInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_keganasanInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_keganasanInputMethodTextChanged

    private void penyakit_hatiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_penyakit_hatiItemStateChanged
        // TODO add your handling code here:
        if(penyakit_hati.getSelectedIndex()==0){
            skor_penyakit_hati.setText("0");
        }else if(penyakit_hati.getSelectedIndex()==1){
            skor_penyakit_hati.setText("10");
        }else if(penyakit_hati.getSelectedIndex()==2){
            skor_penyakit_hati.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_penyakit_hatiItemStateChanged

    private void penyakit_hatiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_penyakit_hatiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_penyakit_hatiKeyPressed

    private void skor_penyakit_hatiInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_penyakit_hatiInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_penyakit_hatiInputMethodTextChanged

    private void penyakit_jantungItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_penyakit_jantungItemStateChanged
        // TODO add your handling code here:
        if(penyakit_jantung.getSelectedIndex()==0){
            skor_penyakit_jantung.setText("0");
        }else if(penyakit_jantung.getSelectedIndex()==1){
            skor_penyakit_jantung.setText("10");
        }else if(penyakit_jantung.getSelectedIndex()==2){
            skor_penyakit_jantung.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_penyakit_jantungItemStateChanged

    private void penyakit_jantungKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_penyakit_jantungKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_penyakit_jantungKeyPressed

    private void skor_penyakit_jantungInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_penyakit_jantungInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_penyakit_jantungInputMethodTextChanged

    private void penyakit_serebroItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_penyakit_serebroItemStateChanged
        // TODO add your handling code here:
        if(penyakit_serebro.getSelectedIndex()==0){
            skor_penyakit_serebro.setText("0");
        }else if(penyakit_serebro.getSelectedIndex()==1){
            skor_penyakit_serebro.setText("10");
        }else if(penyakit_serebro.getSelectedIndex()==2){
            skor_penyakit_serebro.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_penyakit_serebroItemStateChanged

    private void penyakit_serebroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_penyakit_serebroKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_penyakit_serebroKeyPressed

    private void skor_penyakit_serebroInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_penyakit_serebroInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_penyakit_serebroInputMethodTextChanged

    private void penyakit_ginjalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_penyakit_ginjalItemStateChanged
        // TODO add your handling code here:
        if(penyakit_ginjal.getSelectedIndex()==0){
            skor_penyakit_ginjal.setText("0");
        }else if(penyakit_ginjal.getSelectedIndex()==1){
            skor_penyakit_ginjal.setText("10");
        }else if(penyakit_ginjal.getSelectedIndex()==2){
            skor_penyakit_ginjal.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_penyakit_ginjalItemStateChanged

    private void penyakit_ginjalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_penyakit_ginjalKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_penyakit_ginjalKeyPressed

    private void skor_penyakit_ginjalInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_penyakit_ginjalInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_penyakit_ginjalInputMethodTextChanged

    private void gangguan_kesadaranItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_gangguan_kesadaranItemStateChanged
        // TODO add your handling code here:
        if(gangguan_kesadaran.getSelectedIndex()==0){
            skor_gangguan_kesadaran.setText("0");
        }else if(gangguan_kesadaran.getSelectedIndex()==1){
            skor_gangguan_kesadaran.setText("10");
        }else if(gangguan_kesadaran.getSelectedIndex()==2){
            skor_gangguan_kesadaran.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_gangguan_kesadaranItemStateChanged

    private void gangguan_kesadaranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_gangguan_kesadaranKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_gangguan_kesadaranKeyPressed

    private void skor_gangguan_kesadaranInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_gangguan_kesadaranInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_gangguan_kesadaranInputMethodTextChanged

    private void frekuensi_nafasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_frekuensi_nafasItemStateChanged
        // TODO add your handling code here:
        if(frekuensi_nafas.getSelectedIndex()==0){
            skor_frekuensi_nafas.setText("0");
        }else if(frekuensi_nafas.getSelectedIndex()==1){
            skor_frekuensi_nafas.setText("10");
        }else if(frekuensi_nafas.getSelectedIndex()==2){
            skor_frekuensi_nafas.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_frekuensi_nafasItemStateChanged

    private void frekuensi_nafasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_frekuensi_nafasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_frekuensi_nafasKeyPressed

    private void skor_frekuensi_nafasInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_frekuensi_nafasInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_frekuensi_nafasInputMethodTextChanged

    private void sistolikItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sistolikItemStateChanged
        // TODO add your handling code here:
        if(sistolik.getSelectedIndex()==0){
            skor_sistolik.setText("0");
        }else if(sistolik.getSelectedIndex()==1){
            skor_sistolik.setText("10");
        }else if(sistolik.getSelectedIndex()==2){
            skor_sistolik.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_sistolikItemStateChanged

    private void sistolikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sistolikKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_sistolikKeyPressed

    private void skor_sistolikInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_sistolikInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_sistolikInputMethodTextChanged

    private void suhu_tubuhItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_suhu_tubuhItemStateChanged
        // TODO add your handling code here:
        if(suhu_tubuh.getSelectedIndex()==0){
            skor_suhu_tubuh.setText("0");
        }else if(suhu_tubuh.getSelectedIndex()==1){
            skor_suhu_tubuh.setText("10");
        }else if(suhu_tubuh.getSelectedIndex()==2){
            skor_suhu_tubuh.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_suhu_tubuhItemStateChanged

    private void suhu_tubuhKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_suhu_tubuhKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_suhu_tubuhKeyPressed

    private void skor_suhu_tubuhInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_suhu_tubuhInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_suhu_tubuhInputMethodTextChanged

    private void nadiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_nadiItemStateChanged
        // TODO add your handling code here:
        if(nadi.getSelectedIndex()==0){
            skor_nadi.setText("0");
        }else if(nadi.getSelectedIndex()==1){
            skor_nadi.setText("10");
        }else if(nadi.getSelectedIndex()==2){
            skor_nadi.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_nadiItemStateChanged

    private void nadiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nadiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_nadiKeyPressed

    private void skor_nadiInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_nadiInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_nadiInputMethodTextChanged

    private void phItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_phItemStateChanged
        // TODO add your handling code here:
        if(ph.getSelectedIndex()==0){
            skor_ph.setText("0");
        }else if(ph.getSelectedIndex()==1){
            skor_ph.setText("10");
        }else if(ph.getSelectedIndex()==2){
            skor_ph.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_phItemStateChanged

    private void phKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_phKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_phKeyPressed

    private void skor_phInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_phInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_phInputMethodTextChanged

    private void skor_ureumInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_ureumInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_ureumInputMethodTextChanged

    private void ureumItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ureumItemStateChanged
        // TODO add your handling code here:
        if(ureum.getSelectedIndex()==0){
            skor_ureum.setText("0");
        }else if(ureum.getSelectedIndex()==1){
            skor_ureum.setText("10");
        }else if(ureum.getSelectedIndex()==2){
            skor_ureum.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_ureumItemStateChanged

    private void ureumKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ureumKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ureumKeyPressed

    private void skor_natriumInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_natriumInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_natriumInputMethodTextChanged

    private void natriumItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_natriumItemStateChanged
        // TODO add your handling code here:
        if(natrium.getSelectedIndex()==0){
            skor_natrium.setText("0");
        }else if(natrium.getSelectedIndex()==1){
            skor_natrium.setText("10");
        }else if(natrium.getSelectedIndex()==2){
            skor_natrium.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_natriumItemStateChanged

    private void natriumKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_natriumKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_natriumKeyPressed

    private void glukosaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_glukosaItemStateChanged
        // TODO add your handling code here:
        if(glukosa.getSelectedIndex()==0){
            skor_glukosa.setText("0");
        }else if(glukosa.getSelectedIndex()==1){
            skor_glukosa.setText("10");
        }else if(glukosa.getSelectedIndex()==2){
            skor_glukosa.setText("0");
        }          
        isjml();
    }//GEN-LAST:event_glukosaItemStateChanged

    private void glukosaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_glukosaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_glukosaKeyPressed

    private void skor_glukosaInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_glukosaInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_glukosaInputMethodTextChanged

    private void hematokritItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_hematokritItemStateChanged
        // TODO add your handling code here:
        if(hematokrit.getSelectedIndex()==0){
            skor_hematokrit.setText("0");
        }else if(hematokrit.getSelectedIndex()==1){
            skor_hematokrit.setText("10");
        }else if(hematokrit.getSelectedIndex()==2){
            skor_hematokrit.setText("0");
        }         
        isjml();
    }//GEN-LAST:event_hematokritItemStateChanged

    private void hematokritKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_hematokritKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_hematokritKeyPressed

    private void skor_hematokritInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_hematokritInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_hematokritInputMethodTextChanged

    private void tekanan_o2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_tekanan_o2ItemStateChanged
        // TODO add your handling code here:
        if(tekanan_o2.getSelectedIndex()==0){
            skor_tekanan_o2.setText("0");
        }else if(tekanan_o2.getSelectedIndex()==1){
            skor_tekanan_o2.setText("10");
        }else if(tekanan_o2.getSelectedIndex()==2){
            skor_tekanan_o2.setText("0");
        }         
        isjml();
    }//GEN-LAST:event_tekanan_o2ItemStateChanged

    private void tekanan_o2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tekanan_o2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tekanan_o2KeyPressed

    private void skor_tekanan_o2InputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_tekanan_o2InputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_tekanan_o2InputMethodTextChanged

    private void efusi_pleuraItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_efusi_pleuraItemStateChanged
        // TODO add your handling code here:
        if(efusi_pleura.getSelectedIndex()==0){
            skor_efusi_pleura.setText("0");
        }else if(efusi_pleura.getSelectedIndex()==1){
            skor_efusi_pleura.setText("10");
        }else if(efusi_pleura.getSelectedIndex()==2){
            skor_efusi_pleura.setText("0");
        }         
        isjml();
    }//GEN-LAST:event_efusi_pleuraItemStateChanged

    private void efusi_pleuraKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_efusi_pleuraKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_efusi_pleuraKeyPressed

    private void skor_efusi_pleuraInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_skor_efusi_pleuraInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_skor_efusi_pleuraInputMethodTextChanged

    private void usiaInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_usiaInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_usiaInputMethodTextChanged

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMPneumoniaSeverityIndex dialog = new RMPneumoniaSeverityIndex(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDPJP;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.PanelBiasa FormInput;
    private widget.TextBox Jk;
    private widget.TextBox KdDPJP;
    private widget.TextBox KdDokter;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private javax.swing.JMenuItem MnCetak;
    private widget.TextBox NmDPJP;
    private widget.TextBox NmDokter;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.TextBox TglLahir;
    private widget.Tanggal TglPemeriksaan;
    private widget.ComboBox efusi_pleura;
    private widget.ComboBox frekuensi_nafas;
    private widget.ComboBox gangguan_kesadaran;
    private widget.ComboBox glukosa;
    private widget.ComboBox hematokrit;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel108;
    private widget.Label jLabel11;
    private widget.Label jLabel111;
    private widget.Label jLabel112;
    private widget.Label jLabel113;
    private widget.Label jLabel114;
    private widget.Label jLabel122;
    private widget.Label jLabel123;
    private widget.Label jLabel124;
    private widget.Label jLabel125;
    private widget.Label jLabel126;
    private widget.Label jLabel127;
    private widget.Label jLabel128;
    private widget.Label jLabel129;
    private widget.Label jLabel130;
    private widget.Label jLabel131;
    private widget.Label jLabel132;
    private widget.Label jLabel133;
    private widget.Label jLabel134;
    private widget.Label jLabel135;
    private widget.Label jLabel136;
    private widget.Label jLabel137;
    private widget.Label jLabel138;
    private widget.Label jLabel139;
    private widget.Label jLabel140;
    private widget.Label jLabel141;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private widget.ComboBox keganasan;
    private widget.Label label11;
    private widget.Label label15;
    private widget.Label label16;
    private widget.ComboBox nadi;
    private widget.ComboBox natrium;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ComboBox panti_werda;
    private widget.ComboBox penyakit_ginjal;
    private widget.ComboBox penyakit_hati;
    private widget.ComboBox penyakit_jantung;
    private widget.ComboBox penyakit_serebro;
    private widget.ComboBox ph;
    private widget.ScrollPane scrollInput;
    private widget.ComboBox sistolik;
    private widget.TextBox skor_efusi_pleura;
    private widget.TextBox skor_frekuensi_nafas;
    private widget.TextBox skor_gangguan_kesadaran;
    private widget.TextBox skor_glukosa;
    private widget.TextBox skor_hematokrit;
    private widget.TextBox skor_jenis_kelamin;
    private widget.TextBox skor_keganasan;
    private widget.TextBox skor_nadi;
    private widget.TextBox skor_natrium;
    private widget.TextBox skor_panti_werda;
    private widget.TextBox skor_penyakit_ginjal;
    private widget.TextBox skor_penyakit_hati;
    private widget.TextBox skor_penyakit_jantung;
    private widget.TextBox skor_penyakit_serebro;
    private widget.TextBox skor_ph;
    private widget.TextBox skor_sistolik;
    private widget.TextBox skor_suhu_tubuh;
    private widget.TextBox skor_tekanan_o2;
    private widget.TextBox skor_ureum;
    private widget.ComboBox suhu_tubuh;
    private widget.Table tbObat;
    private widget.ComboBox tekanan_o2;
    private widget.TextBox total;
    private widget.ComboBox ureum;
    private widget.TextBox usia;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().trim().equals("")){
            ps=koneksi.prepareStatement(
                        "select p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk, " +
                        "p.tgl_lahir,psi.*, " +
                        "d.nm_dokter,dpjp.nm_dokter as nm_dpjp "+
                        "from reg_periksa " +
                        "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_pneumonia_severity psi on reg_periksa.no_rawat = psi.no_rawat " +
                        "inner join dokter d on psi.kd_dokter = d.kd_dokter " +
                        "inner join dokter dpjp on psi.kd_dpjp = dpjp.kd_dokter " +        
                        "where psi.tanggal between ? and ? " +
                        "order by psi.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                        "select p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk, " +
                        "p.tgl_lahir,psi.*, " +
                        "d.nm_dokter,dpjp.nm_dokter as nm_dpjp "+
                        "from reg_periksa " +
                        "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_pneumonia_severity psi on reg_periksa.no_rawat = psi.no_rawat " +
                        "inner join dokter d on psi.kd_dokter = d.kd_dokter " +
                        "inner join dokter dpjp on psi.kd_dpjp = dpjp.kd_dokter " +         
                        "where psi.tanggal between ? and ? and (reg_periksa.no_rawat like ? or p.no_rkm_medis like ? or p.nm_pasien like ? or psi.kd_dokter like ? or d.nm_dokter like ? or psi.kd_dpjp like ? or dpjp.nm_dokter like ?) " +
                        "order by psi.tanggal");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("tgl_lahir"),rs.getString("jk"),
                        rs.getString("kd_dokter"),rs.getString("nm_dokter"),rs.getString("kd_dpjp"),rs.getString("nm_dpjp"),rs.getString("tanggal"),
                        rs.getString("skor_usia"),rs.getString("skor_jenis_kelamin"),rs.getString("skor_panti_werda"),rs.getString("skor_keganasan"),rs.getString("skor_penyakit_hati"),
                        rs.getString("skor_penyakit_jantung"),rs.getString("skor_penyakit_serebro"),rs.getString("skor_penyakit_ginjal"),rs.getString("skor_gangguan_kesadaran"),rs.getString("skor_frekuensi_nafas"),
                        rs.getString("skor_sistolik"),rs.getString("skor_suhu_tubuh"),rs.getString("skor_nadi"),rs.getString("skor_ph"),rs.getString("skor_ureum"),
                        rs.getString("skor_natrium"),rs.getString("skor_glukosa"),rs.getString("skor_hematokrit"),rs.getString("skor_tekanan_o2"),rs.getString("skor_efusi_pleura"),
                        rs.getString("total")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TabRawat.setSelectedIndex(0);
        isRawat();
        TglPemeriksaan.setDate(new Date());
        hitungskorusia();
        panti_werda.setSelectedIndex(0);
        keganasan.setSelectedIndex(0);
        penyakit_hati.setSelectedIndex(0);
        penyakit_jantung.setSelectedIndex(0);
        penyakit_serebro.setSelectedIndex(0);
        penyakit_ginjal.setSelectedIndex(0);
        gangguan_kesadaran.setSelectedIndex(0);
        frekuensi_nafas.setSelectedIndex(0);
        sistolik.setSelectedIndex(0);
        suhu_tubuh.setSelectedIndex(0);
        nadi.setSelectedIndex(0);
        ph.setSelectedIndex(0);
        ureum.setSelectedIndex(0);
        natrium.setSelectedIndex(0);
        glukosa.setSelectedIndex(0);
        hematokrit.setSelectedIndex(0);
        tekanan_o2.setSelectedIndex(0);
        efusi_pleura.setSelectedIndex(0);
        total.setText("");
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){         
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            KdDPJP.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            NmDPJP.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            Valid.SetTgl2(TglPemeriksaan,tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            usia.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            skor_jenis_kelamin.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            skor_panti_werda.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            if(skor_panti_werda.getText().equals("10")){
                panti_werda.setSelectedIndex(1);
            }else if(skor_panti_werda.getText().equals("0")){
                panti_werda.setSelectedIndex(2);
            }else {
                panti_werda.setSelectedIndex(0);
            }     
            skor_keganasan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            if(skor_keganasan.getText().equals("10")){
                keganasan.setSelectedIndex(1);
            }else if(skor_keganasan.getText().equals("0")){
                keganasan.setSelectedIndex(2);
            }else {
                keganasan.setSelectedIndex(0);
            }
            skor_penyakit_hati.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            if(skor_penyakit_hati.getText().equals("10")){
                penyakit_hati.setSelectedIndex(1);
            }else if(skor_penyakit_hati.getText().equals("0")){
                penyakit_hati.setSelectedIndex(2);
            }else {
                penyakit_hati.setSelectedIndex(0);
            }     
            skor_penyakit_jantung.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            if(skor_penyakit_jantung.getText().equals("10")){
                penyakit_jantung.setSelectedIndex(1);
            }else if(skor_penyakit_jantung.getText().equals("0")){
                penyakit_jantung.setSelectedIndex(2);
            }else {
                penyakit_jantung.setSelectedIndex(0);
            }            
            skor_penyakit_serebro.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            if(skor_penyakit_serebro.getText().equals("10")){
                penyakit_serebro.setSelectedIndex(1);
            }else if(skor_penyakit_serebro.getText().equals("0")){
                penyakit_serebro.setSelectedIndex(2);
            }else {
                penyakit_serebro.setSelectedIndex(0);
            }            
            skor_penyakit_ginjal.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            if(skor_penyakit_ginjal.getText().equals("10")){
                penyakit_ginjal.setSelectedIndex(1);
            }else if(skor_penyakit_ginjal.getText().equals("0")){
                penyakit_ginjal.setSelectedIndex(2);
            }else {
                penyakit_ginjal.setSelectedIndex(0);
            }            
            skor_gangguan_kesadaran.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            if(skor_gangguan_kesadaran.getText().equals("10")){
                gangguan_kesadaran.setSelectedIndex(1);
            }else if(skor_gangguan_kesadaran.getText().equals("0")){
                gangguan_kesadaran.setSelectedIndex(2);
            }else {
                gangguan_kesadaran.setSelectedIndex(0);
            }
            skor_frekuensi_nafas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            if(skor_frekuensi_nafas.getText().equals("10")){
                frekuensi_nafas.setSelectedIndex(1);
            }else if(skor_frekuensi_nafas.getText().equals("0")){
                frekuensi_nafas.setSelectedIndex(2);
            }else {
                frekuensi_nafas.setSelectedIndex(0);
            }
            skor_sistolik.setText(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            if(skor_sistolik.getText().equals("10")){
                sistolik.setSelectedIndex(1);
            }else if(skor_sistolik.getText().equals("0")){
                sistolik.setSelectedIndex(2);
            }else {
                sistolik.setSelectedIndex(0);
            }     
            skor_suhu_tubuh.setText(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            if(skor_suhu_tubuh.getText().equals("10")){
                suhu_tubuh.setSelectedIndex(1);
            }else if(skor_suhu_tubuh.getText().equals("0")){
                suhu_tubuh.setSelectedIndex(2);
            }else {
                suhu_tubuh.setSelectedIndex(0);
            }      
            skor_nadi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            if(skor_nadi.getText().equals("10")){
                nadi.setSelectedIndex(1);
            }else if(skor_nadi.getText().equals("0")){
                nadi.setSelectedIndex(2);
            }else {
                nadi.setSelectedIndex(0);
            }     
            skor_ph.setText(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            if(skor_ph.getText().equals("10")){
                ph.setSelectedIndex(1);
            }else if(skor_ph.getText().equals("0")){
                ph.setSelectedIndex(2);
            }else {
                ph.setSelectedIndex(0);
            }  
            skor_ureum.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            if(skor_ureum.getText().equals("10")){
                ureum.setSelectedIndex(1);
            }else if(skor_ureum.getText().equals("0")){
                ureum.setSelectedIndex(2);
            }else {
                ureum.setSelectedIndex(0);
            }         
            skor_natrium.setText(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            if(skor_natrium.getText().equals("10")){
                natrium.setSelectedIndex(1);
            }else if(skor_natrium.getText().equals("0")){
                natrium.setSelectedIndex(2);
            }else {
                natrium.setSelectedIndex(0);
            }         
            skor_glukosa.setText(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            if(skor_glukosa.getText().equals("10")){
                glukosa.setSelectedIndex(1);
            }else if(skor_glukosa.getText().equals("0")){
                glukosa.setSelectedIndex(2);
            }else {
                glukosa.setSelectedIndex(0);
            }           
            skor_hematokrit.setText(tbObat.getValueAt(tbObat.getSelectedRow(),27).toString());
            if(skor_hematokrit.getText().equals("10")){
                hematokrit.setSelectedIndex(1);
            }else if(skor_hematokrit.getText().equals("0")){
                hematokrit.setSelectedIndex(2);
            }else {
                hematokrit.setSelectedIndex(0);
            }            
            skor_tekanan_o2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            if(skor_tekanan_o2.getText().equals("10")){
                tekanan_o2.setSelectedIndex(1);
            }else if(skor_tekanan_o2.getText().equals("0")){
                tekanan_o2.setSelectedIndex(2);
            }else {
                tekanan_o2.setSelectedIndex(0);
            }            
            skor_efusi_pleura.setText(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            if(skor_efusi_pleura.getText().equals("10")){
                efusi_pleura.setSelectedIndex(1);
            }else if(skor_efusi_pleura.getText().equals("0")){
                efusi_pleura.setSelectedIndex(2);
            }else {
                efusi_pleura.setSelectedIndex(0);
            }     
            total.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi,reg_periksa.umurdaftar,reg_periksa.sttsumur "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                    if(rs.getString("sttsumur").equals("Th")){
                        usia.setText(rs.getString("umurdaftar"));
                    }else{
                        usia.setText("0");
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
 
    public void setNoRm(String norwt,Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2); 
        Sequel.cariIsi("select kd_dokter from reg_periksa where no_rawat=?", KdDokter,norwt);
        Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?", NmDokter,KdDokter.getText());
        isRawat(); 
        hitungskorusia();
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.gettindakan_ralan());
        BtnHapus.setEnabled(akses.gettindakan_ralan());
        BtnEdit.setEnabled(akses.gettindakan_ralan());        
    }
    
    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }

    private void hapus() {
        if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()) ||
                akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString()) ||
                akses.getkode().equals("Admin Utama")){
            if(Sequel.queryu2tf("delete from penilaian_pneumonia_severity where no_rawat=? and tanggal=? ",2,new String[]{
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),
                tbObat.getValueAt(tbObat.getSelectedRow(),9).toString().substring(0,19),
            })==true){
                tampil();
                TabRawat.setSelectedIndex(1);
            }else{
                JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
            }            
        }else{
            JOptionPane.showMessageDialog(null,"Maaf, anda tidak memiliki hak akses untuk menghapus!");
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("penilaian_pneumonia_severity",
            "no_rawat=? and tanggal=?",
            "no_rawat=?,tanggal=?,kd_dokter=?,kd_dpjp=?,skor_usia=?,"+ 
            "skor_jenis_kelamin=?,skor_panti_werda=?,skor_keganasan=?,skor_penyakit_hati=?,skor_penyakit_jantung=?,"+ 
            "skor_penyakit_serebro=?,skor_penyakit_ginjal=?,skor_gangguan_kesadaran=?,skor_frekuensi_nafas=?,skor_sistolik=?,"+ 
            "skor_suhu_tubuh=?,skor_nadi=?,skor_ph=?,skor_ureum=?,skor_natrium=?,"+ 
            "skor_glukosa=?,skor_hematokrit=?,skor_tekanan_o2=?,skor_efusi_pleura=?,total=?",        
            27,
            new String[]{
                TNoRw.getText(),Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),KdDokter.getText(),KdDPJP.getText(),usia.getText(),
                skor_jenis_kelamin.getText(),skor_panti_werda.getText(),skor_keganasan.getText(),skor_penyakit_hati.getText(),skor_penyakit_jantung.getText(),
                skor_penyakit_serebro.getText(),skor_penyakit_ginjal.getText(),skor_gangguan_kesadaran.getText(),skor_frekuensi_nafas.getText(),skor_sistolik.getText(),
                skor_suhu_tubuh.getText(),skor_nadi.getText(),skor_ph.getText(),skor_ureum.getText(),skor_natrium.getText(),
                skor_glukosa.getText(),skor_hematokrit.getText(),skor_tekanan_o2.getText(),skor_efusi_pleura.getText(),total.getText(),
                TNoRw.getText(),
                tbObat.getValueAt(tbObat.getSelectedRow(),9).toString().substring(0,19),
            })==true){
                tampil();
                emptTeks();
                TabRawat.setSelectedIndex(1);   
        }
    }
    
    private void isjml(){
        total.setText(Valid.SetAngka(Valid.SetAngka(usia.getText())+
                Valid.SetAngka(skor_jenis_kelamin.getText())+
                Valid.SetAngka(skor_panti_werda.getText())+
                Valid.SetAngka(skor_keganasan.getText())+
                Valid.SetAngka(skor_penyakit_hati.getText())+
                Valid.SetAngka(skor_penyakit_jantung.getText())+
                Valid.SetAngka(skor_penyakit_serebro.getText())+
                Valid.SetAngka(skor_penyakit_ginjal.getText())+
                Valid.SetAngka(skor_gangguan_kesadaran.getText())+
                Valid.SetAngka(skor_frekuensi_nafas.getText())+
                Valid.SetAngka(skor_sistolik.getText())+
                Valid.SetAngka(skor_suhu_tubuh.getText())+
                Valid.SetAngka(skor_nadi.getText())+         
                Valid.SetAngka(skor_ph.getText())+
                Valid.SetAngka(skor_ureum.getText())+
                Valid.SetAngka(skor_natrium.getText())+
                Valid.SetAngka(skor_glukosa.getText())+
                Valid.SetAngka(skor_hematokrit.getText())+
                Valid.SetAngka(skor_tekanan_o2.getText())+
                Valid.SetAngka(skor_efusi_pleura.getText())              
        ));
    }
    
    private void hitungskorusia(){
        if(Jk.getText().equals("Perempuan")){
            skor_jenis_kelamin.setText(Valid.SetAngka(
                    Valid.SetAngka(usia.getText())-10          
                )
            );
        }else{
            skor_jenis_kelamin.setText(Valid.SetAngka(
                    Valid.SetAngka(usia.getText())
                )
            );
        }
        isjml();
    }    
}
