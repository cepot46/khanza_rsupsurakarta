/*
 * Kontribusi dari Abdul Wahid, RSUD Cipayung Jakarta Timur
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.util.Calendar;
import java.util.Date;
import javax.swing.Timer;
import kepegawaian.DlgCariPetugas;


/**
 *
 * @author perpustakaan
 */
public final class RMOKChecklistPersiapanPreOp extends javax.swing.JDialog {
    private final DefaultTableModel tabMode,tabModePotensiKomplain,tabModePenyakitKronik,tabModeTingkatMasukIGD,
            tabModeFinansial,tabModeLayanan,tabModeTampilPotensi,tabModeTampilKasus,tabModeTampilIGD,
            tabModeTampilFinansial,tabModeTampilLayanan;  
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps,ps2;
    private ResultSet rs,rs2;
    private int i=0,jml=0,index=0,jml_i=0,count_intervensi=0;    
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private boolean[] pilih,pilih_potensi,pilih_penyakit,pilih_tingkatigd,pilih_finansial,pilih_layanan;    
    private String[] kode,masalah,kode_potensi,potensi,kode_penyakit,penyakit,kode_tingkatigd,tingkatigd,kode_finansial,finansial,kode_layanan,layanan;    
    private String masalahkeperawatan="",finger="",report_potensi="",report_kasus="",report_igd="",report_finansial="",report_layanan=""; 
    private StringBuilder htmlContent;
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMOKChecklistPersiapanPreOp(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","J.K.","Tgl.Lahir","Tgl.Skrining",
            "Usia","Kognitif","Resiko Bunuh Diri","Potensi Komplain Lain","Kasus Penyakit Kronik Lain",
            "Status Fungsional","Riwayat Alat Medis","Riwayat Mental Sosial","Tingkat Masuk IGD Lain",
            "Finansial Lain","Kontinuitas Pelayanan Lain","Perkiraan Biaya Asuhan Tinggi",
            "NIP","Nama Petugas"   
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 20; i++) {  //CUSTOM MUHSIN
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(65);
            }else if(i==2){
                column.setPreferredWidth(160);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(60);
            }else if(i==5){
                column.setPreferredWidth(120);
            }else if(i==6){
                column.setPreferredWidth(90);
            }else if(i==7){
                column.setPreferredWidth(250);
            }else if(i==8){
                column.setPreferredWidth(100);
            }else if(i==9){
                column.setPreferredWidth(250);
            }else if(i==10){
                column.setPreferredWidth(250);
            }else if(i==11){
                column.setPreferredWidth(100);
            }else if(i==12){
                column.setPreferredWidth(100);
            }else if(i==13){
                column.setPreferredWidth(120);
            }else if(i==14){
                column.setPreferredWidth(250);
            }else if(i==15){
                column.setPreferredWidth(250);
            }else if(i==16){
                column.setPreferredWidth(250);
            }else if(i==17){
                column.setPreferredWidth(200);
            }else if(i==18){
                column.setPreferredWidth(120);
            }else if(i==19){
                column.setPreferredWidth(250);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        tabModePotensiKomplain=new DefaultTableModel(null,new Object[]{
                "P","Kode","Potensi Komplain Tinggi"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
       
        
     

        tabModePenyakitKronik=new DefaultTableModel(null,new Object[]{
                "P","Kode","Kasus"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
       
        
        for (i = 0; i < 3; i++) {
           
        }
      
        
            tabModeTingkatMasukIGD=new DefaultTableModel(null,new Object[]{
                "P","Kode","Keterangan"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
       

       
        
        for (i = 0; i < 3; i++) {
          
        }
       
        
        tabModeFinansial=new DefaultTableModel(null,new Object[]{
                "P","Kode","Keterangan"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
     
        
        for (i = 0; i < 3; i++) {
          
        }
     
        
        tabModeLayanan=new DefaultTableModel(null,new Object[]{
                "P","Kode","Keterangan"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
     
        
        for (i = 0; i < 3; i++) {
        
        }
        
        
        tabModeTampilPotensi=new DefaultTableModel(null,new Object[]{
                "Kode","Potensi komplain tinggi"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbTampilPotensi.setModel(tabModeTampilPotensi);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbTampilPotensi.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbTampilPotensi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 2; i++) {
            TableColumn column = tbTampilPotensi.getColumnModel().getColumn(i);
            if(i==0){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==1){
                column.setPreferredWidth(420);
            }
        }
        tbTampilPotensi.setDefaultRenderer(Object.class, new WarnaTable());
        
        tabModeTampilKasus=new DefaultTableModel(null,new Object[]{
                "Kode","Kasus penyakit kronik, katastropik, stadium terminal"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbTampilKasus.setModel(tabModeTampilKasus);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbTampilKasus.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbTampilKasus.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 2; i++) {
            TableColumn column = tbTampilKasus.getColumnModel().getColumn(i);
            if(i==0){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==1){
                column.setPreferredWidth(420);
            }
        }
        tbTampilKasus.setDefaultRenderer(Object.class, new WarnaTable());        
        
        tabModeTampilIGD=new DefaultTableModel(null,new Object[]{
                "Kode","Tingkat masuk IGD & readmisi tinggi"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbTampilIGD.setModel(tabModeTampilIGD);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbTampilIGD.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbTampilIGD.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 2; i++) {
            TableColumn column = tbTampilIGD.getColumnModel().getColumn(i);
            if(i==0){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==1){
                column.setPreferredWidth(420);
            }
        }
        tbTampilIGD.setDefaultRenderer(Object.class, new WarnaTable());        
        
        tabModeTampilFinansial=new DefaultTableModel(null,new Object[]{
                "Kode","Kemungkinan sistem pembiayaan yang komplek atau adanya masalah finansial"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbTampilFinansial.setModel(tabModeTampilFinansial);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbTampilFinansial.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbTampilFinansial.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 2; i++) {
            TableColumn column = tbTampilFinansial.getColumnModel().getColumn(i);
            if(i==0){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==1){
                column.setPreferredWidth(420);
            }
        }
        tbTampilFinansial.setDefaultRenderer(Object.class, new WarnaTable());        

        tabModeTampilLayanan=new DefaultTableModel(null,new Object[]{
                "Kode","Kasus yang diidentifikasi dengan rencana pemulangannya penting/berisiko atau yang membutuhkan kontinuitas pelayanan"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbTampilLayanan.setModel(tabModeTampilLayanan);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbTampilLayanan.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbTampilLayanan.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 2; i++) {
            TableColumn column = tbTampilLayanan.getColumnModel().getColumn(i);
            if(i==0){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==1){
                column.setPreferredWidth(420);
            }
        }
        tbTampilLayanan.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        KeteranganFinansial.setDocument(new batasInput((int)230).getKata(KeteranganFinansial));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){ 
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());   
                }              
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
        
        
        ChkAccor.setSelected(false);
        isMenu();
        jam();
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnPetugas = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        jLabel11 = new widget.Label();
        jLabel22 = new widget.Label();
        jLabel53 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel35 = new widget.Label();
        KeteranganFinansial = new widget.TextBox();
        jLabel20 = new widget.Label();
        Tanggal = new widget.Tanggal();
        Jam = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Detik = new widget.ComboBox();
        ChkJam1 = new widget.CekBox();
        label199 = new widget.Label();
        A1_R_Y = new widget.CekBox();
        A1_R_T = new widget.CekBox();
        label200 = new widget.Label();
        label201 = new widget.Label();
        label202 = new widget.Label();
        A1_R_Y1 = new widget.CekBox();
        A1_R_T1 = new widget.CekBox();
        label203 = new widget.Label();
        label204 = new widget.Label();
        jLabel23 = new widget.Label();
        jLabel24 = new widget.Label();
        jLabel25 = new widget.Label();
        jLabel26 = new widget.Label();
        jLabel27 = new widget.Label();
        jLabel28 = new widget.Label();
        jLabel29 = new widget.Label();
        jLabel30 = new widget.Label();
        A1_R_Y2 = new widget.CekBox();
        A1_R_T2 = new widget.CekBox();
        A1_R_Y3 = new widget.CekBox();
        A1_R_T3 = new widget.CekBox();
        A1_R_Y4 = new widget.CekBox();
        A1_R_T4 = new widget.CekBox();
        A1_R_Y5 = new widget.CekBox();
        A1_R_T5 = new widget.CekBox();
        A1_R_Y6 = new widget.CekBox();
        A1_R_T6 = new widget.CekBox();
        A1_R_Y7 = new widget.CekBox();
        A1_R_T7 = new widget.CekBox();
        A1_R_Y8 = new widget.CekBox();
        A1_R_Y9 = new widget.CekBox();
        A1_R_Y10 = new widget.CekBox();
        A1_R_T8 = new widget.CekBox();
        A1_R_T9 = new widget.CekBox();
        A1_R_T10 = new widget.CekBox();
        A1_R_Y11 = new widget.CekBox();
        A1_R_T11 = new widget.CekBox();
        A1_R_T12 = new widget.CekBox();
        A1_R_Y12 = new widget.CekBox();
        A1_R_T13 = new widget.CekBox();
        A1_R_Y13 = new widget.CekBox();
        A1_R_T14 = new widget.CekBox();
        A1_R_Y14 = new widget.CekBox();
        A1_R_T15 = new widget.CekBox();
        A1_R_Y15 = new widget.CekBox();
        A1_R_T16 = new widget.CekBox();
        A1_R_Y16 = new widget.CekBox();
        A1_R_T17 = new widget.CekBox();
        A1_R_Y17 = new widget.CekBox();
        KeteranganFinansial1 = new widget.TextBox();
        KeteranganFinansial2 = new widget.TextBox();
        jLabel54 = new widget.Label();
        jLabel31 = new widget.Label();
        label205 = new widget.Label();
        A1_R_Y18 = new widget.CekBox();
        A1_R_T18 = new widget.CekBox();
        label206 = new widget.Label();
        label207 = new widget.Label();
        label208 = new widget.Label();
        A1_R_Y19 = new widget.CekBox();
        A1_R_T19 = new widget.CekBox();
        label209 = new widget.Label();
        label210 = new widget.Label();
        A1_R_Y20 = new widget.CekBox();
        A1_R_T20 = new widget.CekBox();
        A1_R_Y21 = new widget.CekBox();
        A1_R_T21 = new widget.CekBox();
        A1_R_Y22 = new widget.CekBox();
        A1_R_T22 = new widget.CekBox();
        A1_R_Y23 = new widget.CekBox();
        A1_R_T23 = new widget.CekBox();
        jLabel32 = new widget.Label();
        jLabel33 = new widget.Label();
        jLabel38 = new widget.Label();
        jLabel39 = new widget.Label();
        jLabel40 = new widget.Label();
        jLabel41 = new widget.Label();
        jLabel42 = new widget.Label();
        jLabel43 = new widget.Label();
        jLabel44 = new widget.Label();
        jLabel45 = new widget.Label();
        jLabel46 = new widget.Label();
        jLabel47 = new widget.Label();
        jLabel48 = new widget.Label();
        jLabel49 = new widget.Label();
        jLabel50 = new widget.Label();
        jLabel51 = new widget.Label();
        jLabel52 = new widget.Label();
        jLabel55 = new widget.Label();
        jLabel56 = new widget.Label();
        jLabel57 = new widget.Label();
        jLabel58 = new widget.Label();
        jLabel59 = new widget.Label();
        jLabel60 = new widget.Label();
        jLabel61 = new widget.Label();
        jLabel62 = new widget.Label();
        jLabel63 = new widget.Label();
        jLabel64 = new widget.Label();
        jLabel65 = new widget.Label();
        jLabel66 = new widget.Label();
        jLabel67 = new widget.Label();
        jLabel68 = new widget.Label();
        jLabel69 = new widget.Label();
        jLabel70 = new widget.Label();
        jLabel71 = new widget.Label();
        jLabel72 = new widget.Label();
        jLabel73 = new widget.Label();
        jLabel74 = new widget.Label();
        jLabel75 = new widget.Label();
        jLabel76 = new widget.Label();
        KeteranganFinansial3 = new widget.TextBox();
        KeteranganFinansial4 = new widget.TextBox();
        KeteranganFinansial5 = new widget.TextBox();
        KeteranganFinansial6 = new widget.TextBox();
        KeteranganFinansial7 = new widget.TextBox();
        KeteranganFinansial8 = new widget.TextBox();
        A1_R_Y24 = new widget.CekBox();
        A1_R_T24 = new widget.CekBox();
        A1_R_Y25 = new widget.CekBox();
        A1_R_T25 = new widget.CekBox();
        A1_R_T26 = new widget.CekBox();
        A1_R_Y26 = new widget.CekBox();
        A1_R_Y27 = new widget.CekBox();
        A1_R_T27 = new widget.CekBox();
        A1_R_T28 = new widget.CekBox();
        A1_R_Y28 = new widget.CekBox();
        A1_R_T29 = new widget.CekBox();
        A1_R_Y29 = new widget.CekBox();
        A1_R_T30 = new widget.CekBox();
        A1_R_T31 = new widget.CekBox();
        A1_R_Y30 = new widget.CekBox();
        A1_R_Y31 = new widget.CekBox();
        A1_R_T32 = new widget.CekBox();
        A1_R_T33 = new widget.CekBox();
        A1_R_Y32 = new widget.CekBox();
        A1_R_Y33 = new widget.CekBox();
        A1_R_T34 = new widget.CekBox();
        A1_R_T35 = new widget.CekBox();
        A1_R_Y34 = new widget.CekBox();
        A1_R_Y35 = new widget.CekBox();
        A1_R_T36 = new widget.CekBox();
        A1_R_T37 = new widget.CekBox();
        A1_R_Y36 = new widget.CekBox();
        A1_R_Y37 = new widget.CekBox();
        A1_R_T38 = new widget.CekBox();
        A1_R_T39 = new widget.CekBox();
        A1_R_Y38 = new widget.CekBox();
        A1_R_Y39 = new widget.CekBox();
        A1_R_T40 = new widget.CekBox();
        A1_R_T41 = new widget.CekBox();
        A1_R_Y40 = new widget.CekBox();
        A1_R_Y41 = new widget.CekBox();
        A1_R_T42 = new widget.CekBox();
        A1_R_T43 = new widget.CekBox();
        A1_R_Y42 = new widget.CekBox();
        A1_R_Y43 = new widget.CekBox();
        A1_R_T44 = new widget.CekBox();
        A1_R_T45 = new widget.CekBox();
        A1_R_Y44 = new widget.CekBox();
        A1_R_Y45 = new widget.CekBox();
        A1_R_T46 = new widget.CekBox();
        A1_R_T47 = new widget.CekBox();
        A1_R_Y46 = new widget.CekBox();
        A1_R_Y47 = new widget.CekBox();
        A1_R_T48 = new widget.CekBox();
        A1_R_T49 = new widget.CekBox();
        A1_R_Y48 = new widget.CekBox();
        A1_R_Y49 = new widget.CekBox();
        A1_R_T50 = new widget.CekBox();
        A1_R_T51 = new widget.CekBox();
        A1_R_Y50 = new widget.CekBox();
        A1_R_Y51 = new widget.CekBox();
        A1_R_T52 = new widget.CekBox();
        A1_R_T53 = new widget.CekBox();
        A1_R_Y52 = new widget.CekBox();
        A1_R_Y53 = new widget.CekBox();
        label211 = new widget.Label();
        label212 = new widget.Label();
        label213 = new widget.Label();
        label214 = new widget.Label();
        label215 = new widget.Label();
        label216 = new widget.Label();
        A1_R_T54 = new widget.CekBox();
        A1_R_T55 = new widget.CekBox();
        A1_R_Y54 = new widget.CekBox();
        A1_R_Y55 = new widget.CekBox();
        A1_R_T56 = new widget.CekBox();
        A1_R_T57 = new widget.CekBox();
        A1_R_Y56 = new widget.CekBox();
        A1_R_Y57 = new widget.CekBox();
        A1_R_T58 = new widget.CekBox();
        A1_R_T59 = new widget.CekBox();
        A1_R_Y58 = new widget.CekBox();
        A1_R_Y59 = new widget.CekBox();
        A1_R_T60 = new widget.CekBox();
        A1_R_T61 = new widget.CekBox();
        A1_R_Y60 = new widget.CekBox();
        A1_R_Y61 = new widget.CekBox();
        A1_R_T62 = new widget.CekBox();
        A1_R_Y62 = new widget.CekBox();
        A1_R_Y63 = new widget.CekBox();
        A1_R_T63 = new widget.CekBox();
        A1_R_Y64 = new widget.CekBox();
        A1_R_T64 = new widget.CekBox();
        A1_R_Y65 = new widget.CekBox();
        A1_R_T65 = new widget.CekBox();
        A1_R_T66 = new widget.CekBox();
        A1_R_T67 = new widget.CekBox();
        A1_R_Y66 = new widget.CekBox();
        A1_R_Y67 = new widget.CekBox();
        A1_R_T68 = new widget.CekBox();
        A1_R_Y68 = new widget.CekBox();
        A1_R_T69 = new widget.CekBox();
        A1_R_Y69 = new widget.CekBox();
        A1_R_T70 = new widget.CekBox();
        A1_R_Y70 = new widget.CekBox();
        A1_R_T71 = new widget.CekBox();
        A1_R_Y71 = new widget.CekBox();
        A1_R_T72 = new widget.CekBox();
        A1_R_Y72 = new widget.CekBox();
        A1_R_T73 = new widget.CekBox();
        A1_R_Y73 = new widget.CekBox();
        A1_R_T74 = new widget.CekBox();
        A1_R_Y74 = new widget.CekBox();
        A1_R_T75 = new widget.CekBox();
        A1_R_Y75 = new widget.CekBox();
        A1_R_T76 = new widget.CekBox();
        A1_R_Y76 = new widget.CekBox();
        A1_R_Y77 = new widget.CekBox();
        A1_R_T77 = new widget.CekBox();
        A1_R_T78 = new widget.CekBox();
        A1_R_T79 = new widget.CekBox();
        A1_R_Y78 = new widget.CekBox();
        A1_R_Y79 = new widget.CekBox();
        A1_R_T80 = new widget.CekBox();
        A1_R_Y80 = new widget.CekBox();
        A1_R_T81 = new widget.CekBox();
        A1_R_Y81 = new widget.CekBox();
        A1_R_T82 = new widget.CekBox();
        A1_R_Y82 = new widget.CekBox();
        A1_R_T83 = new widget.CekBox();
        A1_R_Y83 = new widget.CekBox();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        label217 = new widget.Label();
        label218 = new widget.Label();
        label219 = new widget.Label();
        label220 = new widget.Label();
        label221 = new widget.Label();
        label222 = new widget.Label();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel83 = new widget.Label();
        KeteranganFinansial9 = new widget.TextBox();
        KeteranganFinansial11 = new widget.TextBox();
        KeteranganFinansial12 = new widget.TextBox();
        KeteranganFinansial13 = new widget.TextBox();
        KeteranganFinansial17 = new widget.TextBox();
        KeteranganFinansial18 = new widget.TextBox();
        KeteranganFinansial19 = new widget.TextBox();
        KeteranganFinansial20 = new widget.TextBox();
        KeteranganFinansial21 = new widget.TextBox();
        KeteranganFinansial22 = new widget.TextBox();
        KeteranganFinansial23 = new widget.TextBox();
        KeteranganFinansial24 = new widget.TextBox();
        KeteranganFinansial25 = new widget.TextBox();
        jLabel84 = new widget.Label();
        jLabel85 = new widget.Label();
        KeteranganFinansial26 = new widget.TextBox();
        KeteranganFinansial27 = new widget.TextBox();
        KeteranganFinansial28 = new widget.TextBox();
        KeteranganFinansial29 = new widget.TextBox();
        KeteranganFinansial30 = new widget.TextBox();
        KeteranganFinansial31 = new widget.TextBox();
        KeteranganFinansial32 = new widget.TextBox();
        KeteranganFinansial33 = new widget.TextBox();
        KeteranganFinansial34 = new widget.TextBox();
        KeteranganFinansial37 = new widget.TextBox();
        KeteranganFinansial39 = new widget.TextBox();
        KeteranganFinansial38 = new widget.TextBox();
        KeteranganFinansial40 = new widget.TextBox();
        KeteranganFinansial41 = new widget.TextBox();
        jLabel86 = new widget.Label();
        Detik1 = new widget.ComboBox();
        Jam1 = new widget.ComboBox();
        Menit1 = new widget.ComboBox();
        jLabel87 = new widget.Label();
        jLabel88 = new widget.Label();
        jenis_anestesi = new widget.ComboBox();
        Tanggal1 = new widget.Tanggal();
        jLabel89 = new widget.Label();
        KeteranganFinansial10 = new widget.TextBox();
        jLabel90 = new widget.Label();
        jLabel91 = new widget.Label();
        Tanggal2 = new widget.Tanggal();
        jLabel92 = new widget.Label();
        KeteranganFinansial42 = new widget.TextBox();
        jLabel93 = new widget.Label();
        Tanggal3 = new widget.Tanggal();
        jLabel94 = new widget.Label();
        KeteranganFinansial43 = new widget.TextBox();
        jLabel95 = new widget.Label();
        jLabel96 = new widget.Label();
        jLabel97 = new widget.Label();
        jLabel98 = new widget.Label();
        jLabel99 = new widget.Label();
        jLabel100 = new widget.Label();
        jLabel101 = new widget.Label();
        KeteranganFinansial44 = new widget.TextBox();
        jLabel102 = new widget.Label();
        jenis_anestesi1 = new widget.ComboBox();
        jenis_anestesi2 = new widget.ComboBox();
        KeteranganFinansial45 = new widget.TextBox();
        KeteranganFinansial46 = new widget.TextBox();
        KeteranganFinansial47 = new widget.TextBox();
        jLabel103 = new widget.Label();
        jLabel104 = new widget.Label();
        jLabel105 = new widget.Label();
        jLabel106 = new widget.Label();
        jLabel107 = new widget.Label();
        jLabel108 = new widget.Label();
        KeteranganFinansial48 = new widget.TextBox();
        KeteranganFinansial49 = new widget.TextBox();
        KeteranganFinansial50 = new widget.TextBox();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        PanelAccor = new widget.PanelBiasa();
        ChkAccor = new widget.CekBox();
        FormMenu = new widget.PanelBiasa();
        jLabel34 = new widget.Label();
        TNoRM1 = new widget.TextBox();
        TPasien1 = new widget.TextBox();
        BtnPrint1 = new widget.Button();
        FormSkriningMPP = new widget.PanelBiasa();
        Scroll7 = new widget.ScrollPane();
        tbTampilPotensi = new widget.Table();
        Scroll9 = new widget.ScrollPane();
        tbTampilKasus = new widget.Table();
        Scroll13 = new widget.ScrollPane();
        tbTampilIGD = new widget.Table();
        Scroll14 = new widget.ScrollPane();
        tbTampilFinansial = new widget.Table();
        Scroll15 = new widget.ScrollPane();
        tbTampilLayanan = new widget.Table();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Checklist Persiapan Pre Operasi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.setPreferredSize(new java.awt.Dimension(923, 1700));
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 1700));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(74, 40, 100, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(176, 40, 180, 23);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas.setMnemonic('2');
        BtnPetugas.setToolTipText("Alt+2");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        FormInput.add(BtnPetugas);
        BtnPetugas.setBounds(358, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel22.setText("9. Lainnya . . .");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(20, 360, 150, 23);

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel53.setText("A. Persiapan Administrasi");
        jLabel53.setName("jLabel53"); // NOI18N
        FormInput.add(jLabel53);
        jLabel53.setBounds(10, 90, 130, 20);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 70, 880, 1);

        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel35.setText("Keterangan :");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(630, 90, 70, 23);

        KeteranganFinansial.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial.setName("KeteranganFinansial"); // NOI18N
        KeteranganFinansial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansialKeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial);
        KeteranganFinansial.setBounds(460, 120, 400, 20);

        jLabel20.setText("Tanggal :");
        jLabel20.setName("jLabel20"); // NOI18N
        jLabel20.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel20);
        jLabel20.setBounds(380, 40, 75, 23);

        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "27-02-2025" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput.add(Tanggal);
        Tanggal.setBounds(460, 40, 110, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setName("Jam"); // NOI18N
        Jam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JamActionPerformed(evt);
            }
        });
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        FormInput.add(Jam);
        Jam.setBounds(580, 40, 50, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        FormInput.add(Menit);
        Menit.setBounds(640, 40, 50, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        FormInput.add(Detik);
        Detik.setBounds(700, 40, 50, 23);

        ChkJam1.setBorder(null);
        ChkJam1.setSelected(true);
        ChkJam1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam1.setName("ChkJam1"); // NOI18N
        FormInput.add(ChkJam1);
        ChkJam1.setBounds(750, 40, 23, 23);

        label199.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label199.setText("  Ruangan");
        label199.setName("label199"); // NOI18N
        label199.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label199);
        label199.setBounds(270, 80, 60, 23);

        A1_R_Y.setBorder(null);
        A1_R_Y.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y.setName("A1_R_Y"); // NOI18N
        FormInput.add(A1_R_Y);
        A1_R_Y.setBounds(260, 120, 23, 23);

        A1_R_T.setBorder(null);
        A1_R_T.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T.setName("A1_R_T"); // NOI18N
        FormInput.add(A1_R_T);
        A1_R_T.setBounds(310, 120, 23, 23);

        label200.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label200.setText("Tdk");
        label200.setName("label200"); // NOI18N
        label200.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label200);
        label200.setBounds(310, 100, 20, 23);

        label201.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label201.setText("  Ya");
        label201.setName("label201"); // NOI18N
        label201.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label201);
        label201.setBounds(260, 100, 30, 23);

        label202.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label202.setText("IBS");
        label202.setName("label202"); // NOI18N
        label202.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label202);
        label202.setBounds(400, 80, 30, 23);

        A1_R_Y1.setBorder(null);
        A1_R_Y1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y1.setName("A1_R_Y1"); // NOI18N
        FormInput.add(A1_R_Y1);
        A1_R_Y1.setBounds(370, 120, 23, 23);

        A1_R_T1.setBorder(null);
        A1_R_T1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T1.setName("A1_R_T1"); // NOI18N
        FormInput.add(A1_R_T1);
        A1_R_T1.setBounds(420, 120, 23, 23);

        label203.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label203.setText("Tdk");
        label203.setName("label203"); // NOI18N
        label203.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label203);
        label203.setBounds(420, 100, 20, 23);

        label204.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label204.setText("  Ya");
        label204.setName("label204"); // NOI18N
        label204.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label204);
        label204.setBounds(370, 100, 30, 23);

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel23.setText("1. Daftar Program ke IBS");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(20, 120, 130, 23);

        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel24.setText("2. Informed Consent Operator");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(20, 150, 150, 23);

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel25.setText("3. Informed Consent Anestesi");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(20, 180, 150, 23);

        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel26.setText("4. Hasil Laboratorium");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(20, 210, 150, 23);

        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel27.setText("5. Hasil Radiologi Rontgen");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(20, 240, 150, 23);

        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel28.setText("6. Hasil CT Scan");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(20, 270, 150, 23);

        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel29.setText("7. Hasil USG");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(20, 300, 150, 23);

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel30.setText("8. Hasil EKG");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(20, 330, 150, 23);

        A1_R_Y2.setBorder(null);
        A1_R_Y2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y2.setName("A1_R_Y2"); // NOI18N
        FormInput.add(A1_R_Y2);
        A1_R_Y2.setBounds(260, 150, 23, 23);

        A1_R_T2.setBorder(null);
        A1_R_T2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T2.setName("A1_R_T2"); // NOI18N
        FormInput.add(A1_R_T2);
        A1_R_T2.setBounds(310, 150, 23, 23);

        A1_R_Y3.setBorder(null);
        A1_R_Y3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y3.setName("A1_R_Y3"); // NOI18N
        FormInput.add(A1_R_Y3);
        A1_R_Y3.setBounds(370, 150, 23, 23);

        A1_R_T3.setBorder(null);
        A1_R_T3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T3.setName("A1_R_T3"); // NOI18N
        FormInput.add(A1_R_T3);
        A1_R_T3.setBounds(420, 150, 23, 23);

        A1_R_Y4.setBorder(null);
        A1_R_Y4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y4.setName("A1_R_Y4"); // NOI18N
        FormInput.add(A1_R_Y4);
        A1_R_Y4.setBounds(260, 180, 23, 23);

        A1_R_T4.setBorder(null);
        A1_R_T4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T4.setName("A1_R_T4"); // NOI18N
        FormInput.add(A1_R_T4);
        A1_R_T4.setBounds(310, 180, 23, 23);

        A1_R_Y5.setBorder(null);
        A1_R_Y5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y5.setName("A1_R_Y5"); // NOI18N
        FormInput.add(A1_R_Y5);
        A1_R_Y5.setBounds(370, 180, 23, 23);

        A1_R_T5.setBorder(null);
        A1_R_T5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T5.setName("A1_R_T5"); // NOI18N
        FormInput.add(A1_R_T5);
        A1_R_T5.setBounds(420, 180, 23, 23);

        A1_R_Y6.setBorder(null);
        A1_R_Y6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y6.setName("A1_R_Y6"); // NOI18N
        FormInput.add(A1_R_Y6);
        A1_R_Y6.setBounds(260, 210, 23, 23);

        A1_R_T6.setBorder(null);
        A1_R_T6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T6.setName("A1_R_T6"); // NOI18N
        FormInput.add(A1_R_T6);
        A1_R_T6.setBounds(310, 210, 23, 23);

        A1_R_Y7.setBorder(null);
        A1_R_Y7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y7.setName("A1_R_Y7"); // NOI18N
        FormInput.add(A1_R_Y7);
        A1_R_Y7.setBounds(370, 210, 23, 23);

        A1_R_T7.setBorder(null);
        A1_R_T7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T7.setName("A1_R_T7"); // NOI18N
        FormInput.add(A1_R_T7);
        A1_R_T7.setBounds(420, 210, 23, 23);

        A1_R_Y8.setBorder(null);
        A1_R_Y8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y8.setName("A1_R_Y8"); // NOI18N
        FormInput.add(A1_R_Y8);
        A1_R_Y8.setBounds(370, 270, 23, 23);

        A1_R_Y9.setBorder(null);
        A1_R_Y9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y9.setName("A1_R_Y9"); // NOI18N
        FormInput.add(A1_R_Y9);
        A1_R_Y9.setBounds(370, 240, 23, 23);

        A1_R_Y10.setBorder(null);
        A1_R_Y10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y10.setName("A1_R_Y10"); // NOI18N
        FormInput.add(A1_R_Y10);
        A1_R_Y10.setBounds(260, 270, 23, 23);

        A1_R_T8.setBorder(null);
        A1_R_T8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T8.setName("A1_R_T8"); // NOI18N
        FormInput.add(A1_R_T8);
        A1_R_T8.setBounds(420, 240, 23, 23);

        A1_R_T9.setBorder(null);
        A1_R_T9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T9.setName("A1_R_T9"); // NOI18N
        FormInput.add(A1_R_T9);
        A1_R_T9.setBounds(310, 240, 23, 23);

        A1_R_T10.setBorder(null);
        A1_R_T10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T10.setName("A1_R_T10"); // NOI18N
        FormInput.add(A1_R_T10);
        A1_R_T10.setBounds(420, 270, 23, 23);

        A1_R_Y11.setBorder(null);
        A1_R_Y11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y11.setName("A1_R_Y11"); // NOI18N
        FormInput.add(A1_R_Y11);
        A1_R_Y11.setBounds(260, 240, 23, 23);

        A1_R_T11.setBorder(null);
        A1_R_T11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T11.setName("A1_R_T11"); // NOI18N
        FormInput.add(A1_R_T11);
        A1_R_T11.setBounds(310, 270, 23, 23);

        A1_R_T12.setBorder(null);
        A1_R_T12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T12.setName("A1_R_T12"); // NOI18N
        FormInput.add(A1_R_T12);
        A1_R_T12.setBounds(310, 300, 23, 23);

        A1_R_Y12.setBorder(null);
        A1_R_Y12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y12.setName("A1_R_Y12"); // NOI18N
        FormInput.add(A1_R_Y12);
        A1_R_Y12.setBounds(260, 300, 23, 23);

        A1_R_T13.setBorder(null);
        A1_R_T13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T13.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T13.setName("A1_R_T13"); // NOI18N
        FormInput.add(A1_R_T13);
        A1_R_T13.setBounds(310, 330, 23, 23);

        A1_R_Y13.setBorder(null);
        A1_R_Y13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y13.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y13.setName("A1_R_Y13"); // NOI18N
        FormInput.add(A1_R_Y13);
        A1_R_Y13.setBounds(260, 330, 23, 23);

        A1_R_T14.setBorder(null);
        A1_R_T14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T14.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T14.setName("A1_R_T14"); // NOI18N
        FormInput.add(A1_R_T14);
        A1_R_T14.setBounds(420, 330, 23, 23);

        A1_R_Y14.setBorder(null);
        A1_R_Y14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y14.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y14.setName("A1_R_Y14"); // NOI18N
        FormInput.add(A1_R_Y14);
        A1_R_Y14.setBounds(370, 330, 23, 23);

        A1_R_T15.setBorder(null);
        A1_R_T15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T15.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T15.setName("A1_R_T15"); // NOI18N
        FormInput.add(A1_R_T15);
        A1_R_T15.setBounds(420, 300, 23, 23);

        A1_R_Y15.setBorder(null);
        A1_R_Y15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y15.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y15.setName("A1_R_Y15"); // NOI18N
        FormInput.add(A1_R_Y15);
        A1_R_Y15.setBounds(370, 300, 23, 23);

        A1_R_T16.setBorder(null);
        A1_R_T16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T16.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T16.setName("A1_R_T16"); // NOI18N
        FormInput.add(A1_R_T16);
        A1_R_T16.setBounds(310, 360, 23, 23);

        A1_R_Y16.setBorder(null);
        A1_R_Y16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y16.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y16.setName("A1_R_Y16"); // NOI18N
        FormInput.add(A1_R_Y16);
        A1_R_Y16.setBounds(260, 360, 23, 23);

        A1_R_T17.setBorder(null);
        A1_R_T17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T17.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T17.setName("A1_R_T17"); // NOI18N
        FormInput.add(A1_R_T17);
        A1_R_T17.setBounds(420, 360, 23, 23);

        A1_R_Y17.setBorder(null);
        A1_R_Y17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y17.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y17.setName("A1_R_Y17"); // NOI18N
        FormInput.add(A1_R_Y17);
        A1_R_Y17.setBounds(370, 360, 23, 23);

        KeteranganFinansial1.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial1.setName("KeteranganFinansial1"); // NOI18N
        KeteranganFinansial1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial1KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial1);
        KeteranganFinansial1.setBounds(460, 150, 400, 20);

        KeteranganFinansial2.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial2.setName("KeteranganFinansial2"); // NOI18N
        KeteranganFinansial2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial2KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial2);
        KeteranganFinansial2.setBounds(460, 180, 400, 20);

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("D. Persiapan Psikis / Mental");
        jLabel54.setName("jLabel54"); // NOI18N
        FormInput.add(jLabel54);
        jLabel54.setBounds(10, 1370, 140, 20);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("c. Fresh Frozen Plasma (FFP)");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(40, 800, 140, 23);

        label205.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label205.setText(" Ruangan");
        label205.setName("label205"); // NOI18N
        label205.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label205);
        label205.setBounds(270, 410, 50, 23);

        A1_R_Y18.setBorder(null);
        A1_R_Y18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y18.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y18.setName("A1_R_Y18"); // NOI18N
        FormInput.add(A1_R_Y18);
        A1_R_Y18.setBounds(260, 450, 23, 23);

        A1_R_T18.setBorder(null);
        A1_R_T18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T18.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T18.setName("A1_R_T18"); // NOI18N
        FormInput.add(A1_R_T18);
        A1_R_T18.setBounds(310, 450, 23, 23);

        label206.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label206.setText("Tdk");
        label206.setName("label206"); // NOI18N
        label206.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label206);
        label206.setBounds(310, 430, 20, 23);

        label207.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label207.setText("  Ya");
        label207.setName("label207"); // NOI18N
        label207.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label207);
        label207.setBounds(260, 430, 30, 23);

        label208.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label208.setText("IBS");
        label208.setName("label208"); // NOI18N
        label208.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label208);
        label208.setBounds(400, 410, 30, 20);

        A1_R_Y19.setBorder(null);
        A1_R_Y19.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y19.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y19.setName("A1_R_Y19"); // NOI18N
        FormInput.add(A1_R_Y19);
        A1_R_Y19.setBounds(370, 450, 23, 23);

        A1_R_T19.setBorder(null);
        A1_R_T19.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T19.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T19.setName("A1_R_T19"); // NOI18N
        FormInput.add(A1_R_T19);
        A1_R_T19.setBounds(420, 450, 23, 23);

        label209.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label209.setText("Tdk");
        label209.setName("label209"); // NOI18N
        label209.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label209);
        label209.setBounds(420, 430, 20, 23);

        label210.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label210.setText("  Ya");
        label210.setName("label210"); // NOI18N
        label210.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label210);
        label210.setBounds(370, 430, 30, 23);

        A1_R_Y20.setBorder(null);
        A1_R_Y20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y20.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y20.setName("A1_R_Y20"); // NOI18N
        FormInput.add(A1_R_Y20);
        A1_R_Y20.setBounds(260, 480, 23, 23);

        A1_R_T20.setBorder(null);
        A1_R_T20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T20.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T20.setName("A1_R_T20"); // NOI18N
        FormInput.add(A1_R_T20);
        A1_R_T20.setBounds(310, 480, 23, 23);

        A1_R_Y21.setBorder(null);
        A1_R_Y21.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y21.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y21.setName("A1_R_Y21"); // NOI18N
        FormInput.add(A1_R_Y21);
        A1_R_Y21.setBounds(370, 480, 23, 23);

        A1_R_T21.setBorder(null);
        A1_R_T21.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T21.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T21.setName("A1_R_T21"); // NOI18N
        FormInput.add(A1_R_T21);
        A1_R_T21.setBounds(420, 480, 23, 23);

        A1_R_Y22.setBorder(null);
        A1_R_Y22.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y22.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y22.setName("A1_R_Y22"); // NOI18N
        FormInput.add(A1_R_Y22);
        A1_R_Y22.setBounds(260, 510, 23, 23);

        A1_R_T22.setBorder(null);
        A1_R_T22.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T22.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T22.setName("A1_R_T22"); // NOI18N
        FormInput.add(A1_R_T22);
        A1_R_T22.setBounds(310, 510, 23, 23);

        A1_R_Y23.setBorder(null);
        A1_R_Y23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y23.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y23.setName("A1_R_Y23"); // NOI18N
        FormInput.add(A1_R_Y23);
        A1_R_Y23.setBounds(370, 510, 23, 23);

        A1_R_T23.setBorder(null);
        A1_R_T23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T23.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T23.setName("A1_R_T23"); // NOI18N
        FormInput.add(A1_R_T23);
        A1_R_T23.setBounds(420, 510, 23, 23);

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel32.setText("1. Antibiotik Profilaksis");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(20, 450, 130, 23);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel33.setText("2. Hasil Skin Test Obat");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(20, 480, 130, 23);

        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel38.setText("3. Cukur Area Operasi");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(20, 510, 130, 23);

        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel39.setText("4. Dilakukan Levement / Huknah");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(20, 540, 170, 23);

        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel40.setText("5. Gigi Palsu");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(20, 570, 170, 23);

        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel41.setText("6. Pasang DC No : ... Kunci : ...");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(20, 600, 170, 23);

        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel42.setText("7. Pasang Infus No : ... di : ...");
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(20, 630, 170, 23);

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel43.setText("8. Pasang NGT No : .. Kedalaman : ");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(20, 660, 170, 23);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel44.setText("9. Perhiasan");
        jLabel44.setName("jLabel44"); // NOI18N
        FormInput.add(jLabel44);
        jLabel44.setBounds(20, 690, 110, 23);

        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel45.setText("c. ......");
        jLabel45.setName("jLabel45"); // NOI18N
        FormInput.add(jLabel45);
        jLabel45.setBounds(40, 1230, 140, 23);

        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel46.setText("a. Whole Blood (WB)");
        jLabel46.setName("jLabel46"); // NOI18N
        FormInput.add(jLabel46);
        jLabel46.setBounds(40, 740, 110, 23);

        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel47.setText("b. Packed Red Cell ( PRC)");
        jLabel47.setName("jLabel47"); // NOI18N
        FormInput.add(jLabel47);
        jLabel47.setBounds(40, 770, 140, 23);

        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel48.setText("10. Persiapan Tranfusi Darat :");
        jLabel48.setName("jLabel48"); // NOI18N
        FormInput.add(jLabel48);
        jLabel48.setBounds(20, 720, 170, 23);

        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel49.setText("11. Premedikasi");
        jLabel49.setName("jLabel49"); // NOI18N
        FormInput.add(jLabel49);
        jLabel49.setBounds(20, 830, 170, 23);

        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel50.setText("12. Puasa mulai jam");
        jLabel50.setName("jLabel50"); // NOI18N
        FormInput.add(jLabel50);
        jLabel50.setBounds(20, 860, 110, 23);

        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel51.setText("13. Riwayat Alergi Obat / Makanan");
        jLabel51.setName("jLabel51"); // NOI18N
        FormInput.add(jLabel51);
        jLabel51.setBounds(20, 890, 190, 23);

        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel52.setText("14. Tandai lokasi operasi (Site Marking)");
        jLabel52.setName("jLabel52"); // NOI18N
        FormInput.add(jLabel52);
        jLabel52.setBounds(20, 920, 200, 23);

        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel55.setText("B. Persiapan Fisik");
        jLabel55.setName("jLabel55"); // NOI18N
        FormInput.add(jLabel55);
        jLabel55.setBounds(10, 420, 130, 20);

        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel56.setText("15. Lainnya . . .");
        jLabel56.setName("jLabel56"); // NOI18N
        FormInput.add(jLabel56);
        jLabel56.setBounds(20, 950, 80, 23);

        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel57.setText("1. ASMA - Obat Asma Pre Op");
        jLabel57.setName("jLabel57"); // NOI18N
        FormInput.add(jLabel57);
        jLabel57.setBounds(20, 1020, 150, 23);

        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel58.setText("2. Diabetes Mellitus - Insulin Pre Op");
        jLabel58.setName("jLabel58"); // NOI18N
        FormInput.add(jLabel58);
        jLabel58.setBounds(20, 1050, 190, 23);

        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel59.setText("3. Hipertensi - Obat Anti Hipertensi Pre Op");
        jLabel59.setName("jLabel59"); // NOI18N
        FormInput.add(jLabel59);
        jLabel59.setBounds(20, 1080, 204, 23);

        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel60.setText("4. Riwayat Penyakit Lain");
        jLabel60.setName("jLabel60"); // NOI18N
        FormInput.add(jLabel60);
        jLabel60.setBounds(20, 1110, 120, 23);

        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel61.setText("4. Riwayat Operasi");
        jLabel61.setName("jLabel61"); // NOI18N
        FormInput.add(jLabel61);
        jLabel61.setBounds(20, 1590, 150, 23);

        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel62.setText("a. ......");
        jLabel62.setName("jLabel62"); // NOI18N
        FormInput.add(jLabel62);
        jLabel62.setBounds(40, 1170, 140, 23);

        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel63.setText("b. ......");
        jLabel63.setName("jLabel63"); // NOI18N
        FormInput.add(jLabel63);
        jLabel63.setBounds(40, 1200, 140, 23);

        jLabel64.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel64.setText("5. Obat yang dibawa ke IBS");
        jLabel64.setName("jLabel64"); // NOI18N
        FormInput.add(jLabel64);
        jLabel64.setBounds(20, 1150, 140, 23);

        jLabel65.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel65.setText("6. Status Pasien Covid");
        jLabel65.setName("jLabel65"); // NOI18N
        FormInput.add(jLabel65);
        jLabel65.setBounds(20, 1260, 140, 23);

        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel66.setText("Swab Terkonfirmasi (+ )");
        jLabel66.setName("jLabel66"); // NOI18N
        FormInput.add(jLabel66);
        jLabel66.setBounds(40, 1280, 120, 23);

        jLabel67.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel67.setText("Suspect");
        jLabel67.setName("jLabel67"); // NOI18N
        FormInput.add(jLabel67);
        jLabel67.setBounds(40, 1310, 50, 23);

        jLabel68.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel68.setText("C. Persiapan Khusus");
        jLabel68.setName("jLabel68"); // NOI18N
        FormInput.add(jLabel68);
        jLabel68.setBounds(10, 990, 130, 20);

        jLabel69.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel69.setText("7. Lainnya . . .");
        jLabel69.setName("jLabel69"); // NOI18N
        FormInput.add(jLabel69);
        jLabel69.setBounds(20, 1340, 80, 23);

        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel70.setText("Kunjungan / Visite Dokter Pre Operasi");
        jLabel70.setName("jLabel70"); // NOI18N
        FormInput.add(jLabel70);
        jLabel70.setBounds(30, 1390, 200, 23);

        jLabel71.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel71.setText("1. Dokter Bedah");
        jLabel71.setName("jLabel71"); // NOI18N
        FormInput.add(jLabel71);
        jLabel71.setBounds(30, 1420, 90, 23);

        jLabel72.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel72.setText("2. Dokter Anestesi");
        jLabel72.setName("jLabel72"); // NOI18N
        FormInput.add(jLabel72);
        jLabel72.setBounds(30, 1450, 90, 23);

        jLabel73.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel73.setText("3. Dokter Konsultasi");
        jLabel73.setName("jLabel73"); // NOI18N
        FormInput.add(jLabel73);
        jLabel73.setBounds(30, 1480, 100, 23);

        jLabel74.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel74.setText("a. Spesialis Paru");
        jLabel74.setName("jLabel74"); // NOI18N
        FormInput.add(jLabel74);
        jLabel74.setBounds(50, 1500, 100, 23);

        jLabel75.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel75.setText("b. Spesialis Penyakit Dalam");
        jLabel75.setName("jLabel75"); // NOI18N
        FormInput.add(jLabel75);
        jLabel75.setBounds(50, 1530, 150, 23);

        jLabel76.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel76.setText("c. Spesialis . . .");
        jLabel76.setName("jLabel76"); // NOI18N
        FormInput.add(jLabel76);
        jLabel76.setBounds(50, 1560, 150, 23);

        KeteranganFinansial3.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial3.setName("KeteranganFinansial3"); // NOI18N
        KeteranganFinansial3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial3KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial3);
        KeteranganFinansial3.setBounds(460, 210, 400, 20);

        KeteranganFinansial4.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial4.setName("KeteranganFinansial4"); // NOI18N
        KeteranganFinansial4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial4KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial4);
        KeteranganFinansial4.setBounds(460, 240, 400, 20);

        KeteranganFinansial5.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial5.setName("KeteranganFinansial5"); // NOI18N
        KeteranganFinansial5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial5KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial5);
        KeteranganFinansial5.setBounds(460, 270, 400, 20);

        KeteranganFinansial6.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial6.setName("KeteranganFinansial6"); // NOI18N
        KeteranganFinansial6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial6KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial6);
        KeteranganFinansial6.setBounds(460, 300, 400, 20);

        KeteranganFinansial7.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial7.setName("KeteranganFinansial7"); // NOI18N
        KeteranganFinansial7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial7KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial7);
        KeteranganFinansial7.setBounds(460, 330, 400, 20);

        KeteranganFinansial8.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial8.setName("KeteranganFinansial8"); // NOI18N
        KeteranganFinansial8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial8KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial8);
        KeteranganFinansial8.setBounds(460, 360, 400, 20);

        A1_R_Y24.setBorder(null);
        A1_R_Y24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y24.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y24.setName("A1_R_Y24"); // NOI18N
        FormInput.add(A1_R_Y24);
        A1_R_Y24.setBounds(260, 540, 23, 23);

        A1_R_T24.setBorder(null);
        A1_R_T24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T24.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T24.setName("A1_R_T24"); // NOI18N
        FormInput.add(A1_R_T24);
        A1_R_T24.setBounds(310, 540, 23, 23);

        A1_R_Y25.setBorder(null);
        A1_R_Y25.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y25.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y25.setName("A1_R_Y25"); // NOI18N
        FormInput.add(A1_R_Y25);
        A1_R_Y25.setBounds(370, 540, 23, 23);

        A1_R_T25.setBorder(null);
        A1_R_T25.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T25.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T25.setName("A1_R_T25"); // NOI18N
        FormInput.add(A1_R_T25);
        A1_R_T25.setBounds(420, 540, 23, 23);

        A1_R_T26.setBorder(null);
        A1_R_T26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T26.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T26.setName("A1_R_T26"); // NOI18N
        FormInput.add(A1_R_T26);
        A1_R_T26.setBounds(310, 570, 23, 23);

        A1_R_Y26.setBorder(null);
        A1_R_Y26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y26.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y26.setName("A1_R_Y26"); // NOI18N
        FormInput.add(A1_R_Y26);
        A1_R_Y26.setBounds(370, 570, 23, 23);

        A1_R_Y27.setBorder(null);
        A1_R_Y27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y27.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y27.setName("A1_R_Y27"); // NOI18N
        FormInput.add(A1_R_Y27);
        A1_R_Y27.setBounds(260, 570, 23, 23);

        A1_R_T27.setBorder(null);
        A1_R_T27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T27.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T27.setName("A1_R_T27"); // NOI18N
        FormInput.add(A1_R_T27);
        A1_R_T27.setBounds(420, 570, 23, 23);

        A1_R_T28.setBorder(null);
        A1_R_T28.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T28.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T28.setName("A1_R_T28"); // NOI18N
        FormInput.add(A1_R_T28);
        A1_R_T28.setBounds(310, 600, 23, 23);

        A1_R_Y28.setBorder(null);
        A1_R_Y28.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y28.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y28.setName("A1_R_Y28"); // NOI18N
        FormInput.add(A1_R_Y28);
        A1_R_Y28.setBounds(260, 600, 23, 23);

        A1_R_T29.setBorder(null);
        A1_R_T29.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T29.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T29.setName("A1_R_T29"); // NOI18N
        FormInput.add(A1_R_T29);
        A1_R_T29.setBounds(420, 600, 23, 23);

        A1_R_Y29.setBorder(null);
        A1_R_Y29.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y29.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y29.setName("A1_R_Y29"); // NOI18N
        FormInput.add(A1_R_Y29);
        A1_R_Y29.setBounds(370, 600, 23, 23);

        A1_R_T30.setBorder(null);
        A1_R_T30.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T30.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T30.setName("A1_R_T30"); // NOI18N
        FormInput.add(A1_R_T30);
        A1_R_T30.setBounds(310, 630, 23, 23);

        A1_R_T31.setBorder(null);
        A1_R_T31.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T31.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T31.setName("A1_R_T31"); // NOI18N
        FormInput.add(A1_R_T31);
        A1_R_T31.setBounds(420, 630, 23, 23);

        A1_R_Y30.setBorder(null);
        A1_R_Y30.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y30.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y30.setName("A1_R_Y30"); // NOI18N
        FormInput.add(A1_R_Y30);
        A1_R_Y30.setBounds(370, 630, 23, 23);

        A1_R_Y31.setBorder(null);
        A1_R_Y31.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y31.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y31.setName("A1_R_Y31"); // NOI18N
        FormInput.add(A1_R_Y31);
        A1_R_Y31.setBounds(260, 630, 23, 23);

        A1_R_T32.setBorder(null);
        A1_R_T32.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T32.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T32.setName("A1_R_T32"); // NOI18N
        FormInput.add(A1_R_T32);
        A1_R_T32.setBounds(310, 660, 23, 23);

        A1_R_T33.setBorder(null);
        A1_R_T33.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T33.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T33.setName("A1_R_T33"); // NOI18N
        FormInput.add(A1_R_T33);
        A1_R_T33.setBounds(420, 660, 23, 23);

        A1_R_Y32.setBorder(null);
        A1_R_Y32.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y32.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y32.setName("A1_R_Y32"); // NOI18N
        FormInput.add(A1_R_Y32);
        A1_R_Y32.setBounds(370, 660, 23, 23);

        A1_R_Y33.setBorder(null);
        A1_R_Y33.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y33.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y33.setName("A1_R_Y33"); // NOI18N
        FormInput.add(A1_R_Y33);
        A1_R_Y33.setBounds(260, 660, 23, 23);

        A1_R_T34.setBorder(null);
        A1_R_T34.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T34.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T34.setName("A1_R_T34"); // NOI18N
        FormInput.add(A1_R_T34);
        A1_R_T34.setBounds(310, 690, 23, 23);

        A1_R_T35.setBorder(null);
        A1_R_T35.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T35.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T35.setName("A1_R_T35"); // NOI18N
        FormInput.add(A1_R_T35);
        A1_R_T35.setBounds(420, 690, 23, 23);

        A1_R_Y34.setBorder(null);
        A1_R_Y34.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y34.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y34.setName("A1_R_Y34"); // NOI18N
        FormInput.add(A1_R_Y34);
        A1_R_Y34.setBounds(370, 690, 23, 23);

        A1_R_Y35.setBorder(null);
        A1_R_Y35.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y35.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y35.setName("A1_R_Y35"); // NOI18N
        FormInput.add(A1_R_Y35);
        A1_R_Y35.setBounds(260, 690, 23, 23);

        A1_R_T36.setBorder(null);
        A1_R_T36.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T36.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T36.setName("A1_R_T36"); // NOI18N
        FormInput.add(A1_R_T36);
        A1_R_T36.setBounds(310, 740, 23, 23);

        A1_R_T37.setBorder(null);
        A1_R_T37.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T37.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T37.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T37.setName("A1_R_T37"); // NOI18N
        FormInput.add(A1_R_T37);
        A1_R_T37.setBounds(420, 740, 23, 23);

        A1_R_Y36.setBorder(null);
        A1_R_Y36.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y36.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y36.setName("A1_R_Y36"); // NOI18N
        FormInput.add(A1_R_Y36);
        A1_R_Y36.setBounds(370, 740, 23, 23);

        A1_R_Y37.setBorder(null);
        A1_R_Y37.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y37.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y37.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y37.setName("A1_R_Y37"); // NOI18N
        FormInput.add(A1_R_Y37);
        A1_R_Y37.setBounds(260, 740, 23, 23);

        A1_R_T38.setBorder(null);
        A1_R_T38.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T38.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T38.setName("A1_R_T38"); // NOI18N
        FormInput.add(A1_R_T38);
        A1_R_T38.setBounds(420, 770, 23, 23);

        A1_R_T39.setBorder(null);
        A1_R_T39.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T39.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T39.setName("A1_R_T39"); // NOI18N
        FormInput.add(A1_R_T39);
        A1_R_T39.setBounds(310, 770, 23, 23);

        A1_R_Y38.setBorder(null);
        A1_R_Y38.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y38.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y38.setName("A1_R_Y38"); // NOI18N
        FormInput.add(A1_R_Y38);
        A1_R_Y38.setBounds(260, 770, 23, 23);

        A1_R_Y39.setBorder(null);
        A1_R_Y39.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y39.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y39.setName("A1_R_Y39"); // NOI18N
        FormInput.add(A1_R_Y39);
        A1_R_Y39.setBounds(370, 770, 23, 23);

        A1_R_T40.setBorder(null);
        A1_R_T40.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T40.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T40.setName("A1_R_T40"); // NOI18N
        FormInput.add(A1_R_T40);
        A1_R_T40.setBounds(420, 800, 23, 23);

        A1_R_T41.setBorder(null);
        A1_R_T41.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T41.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T41.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T41.setName("A1_R_T41"); // NOI18N
        FormInput.add(A1_R_T41);
        A1_R_T41.setBounds(310, 800, 23, 23);

        A1_R_Y40.setBorder(null);
        A1_R_Y40.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y40.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y40.setName("A1_R_Y40"); // NOI18N
        FormInput.add(A1_R_Y40);
        A1_R_Y40.setBounds(260, 800, 23, 23);

        A1_R_Y41.setBorder(null);
        A1_R_Y41.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y41.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y41.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y41.setName("A1_R_Y41"); // NOI18N
        FormInput.add(A1_R_Y41);
        A1_R_Y41.setBounds(370, 800, 23, 23);

        A1_R_T42.setBorder(null);
        A1_R_T42.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T42.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T42.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T42.setName("A1_R_T42"); // NOI18N
        FormInput.add(A1_R_T42);
        A1_R_T42.setBounds(420, 830, 23, 23);

        A1_R_T43.setBorder(null);
        A1_R_T43.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T43.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T43.setName("A1_R_T43"); // NOI18N
        FormInput.add(A1_R_T43);
        A1_R_T43.setBounds(310, 830, 23, 23);

        A1_R_Y42.setBorder(null);
        A1_R_Y42.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y42.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y42.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y42.setName("A1_R_Y42"); // NOI18N
        FormInput.add(A1_R_Y42);
        A1_R_Y42.setBounds(260, 830, 23, 23);

        A1_R_Y43.setBorder(null);
        A1_R_Y43.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y43.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y43.setName("A1_R_Y43"); // NOI18N
        FormInput.add(A1_R_Y43);
        A1_R_Y43.setBounds(370, 830, 23, 23);

        A1_R_T44.setBorder(null);
        A1_R_T44.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T44.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T44.setName("A1_R_T44"); // NOI18N
        FormInput.add(A1_R_T44);
        A1_R_T44.setBounds(420, 860, 23, 23);

        A1_R_T45.setBorder(null);
        A1_R_T45.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T45.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T45.setName("A1_R_T45"); // NOI18N
        FormInput.add(A1_R_T45);
        A1_R_T45.setBounds(310, 860, 23, 23);

        A1_R_Y44.setBorder(null);
        A1_R_Y44.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y44.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y44.setName("A1_R_Y44"); // NOI18N
        FormInput.add(A1_R_Y44);
        A1_R_Y44.setBounds(260, 860, 23, 23);

        A1_R_Y45.setBorder(null);
        A1_R_Y45.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y45.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y45.setName("A1_R_Y45"); // NOI18N
        FormInput.add(A1_R_Y45);
        A1_R_Y45.setBounds(370, 860, 23, 23);

        A1_R_T46.setBorder(null);
        A1_R_T46.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T46.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T46.setName("A1_R_T46"); // NOI18N
        FormInput.add(A1_R_T46);
        A1_R_T46.setBounds(420, 890, 23, 23);

        A1_R_T47.setBorder(null);
        A1_R_T47.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T47.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T47.setName("A1_R_T47"); // NOI18N
        FormInput.add(A1_R_T47);
        A1_R_T47.setBounds(310, 890, 23, 23);

        A1_R_Y46.setBorder(null);
        A1_R_Y46.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y46.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y46.setName("A1_R_Y46"); // NOI18N
        FormInput.add(A1_R_Y46);
        A1_R_Y46.setBounds(260, 890, 23, 23);

        A1_R_Y47.setBorder(null);
        A1_R_Y47.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y47.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y47.setName("A1_R_Y47"); // NOI18N
        FormInput.add(A1_R_Y47);
        A1_R_Y47.setBounds(370, 890, 23, 23);

        A1_R_T48.setBorder(null);
        A1_R_T48.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T48.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T48.setName("A1_R_T48"); // NOI18N
        FormInput.add(A1_R_T48);
        A1_R_T48.setBounds(420, 920, 23, 23);

        A1_R_T49.setBorder(null);
        A1_R_T49.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T49.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T49.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T49.setName("A1_R_T49"); // NOI18N
        FormInput.add(A1_R_T49);
        A1_R_T49.setBounds(310, 920, 23, 23);

        A1_R_Y48.setBorder(null);
        A1_R_Y48.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y48.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y48.setName("A1_R_Y48"); // NOI18N
        FormInput.add(A1_R_Y48);
        A1_R_Y48.setBounds(260, 920, 23, 23);

        A1_R_Y49.setBorder(null);
        A1_R_Y49.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y49.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y49.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y49.setName("A1_R_Y49"); // NOI18N
        FormInput.add(A1_R_Y49);
        A1_R_Y49.setBounds(370, 920, 23, 23);

        A1_R_T50.setBorder(null);
        A1_R_T50.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T50.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T50.setName("A1_R_T50"); // NOI18N
        FormInput.add(A1_R_T50);
        A1_R_T50.setBounds(420, 950, 23, 23);

        A1_R_T51.setBorder(null);
        A1_R_T51.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T51.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T51.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T51.setName("A1_R_T51"); // NOI18N
        FormInput.add(A1_R_T51);
        A1_R_T51.setBounds(310, 950, 23, 23);

        A1_R_Y50.setBorder(null);
        A1_R_Y50.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y50.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y50.setName("A1_R_Y50"); // NOI18N
        FormInput.add(A1_R_Y50);
        A1_R_Y50.setBounds(260, 950, 23, 23);

        A1_R_Y51.setBorder(null);
        A1_R_Y51.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y51.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y51.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y51.setName("A1_R_Y51"); // NOI18N
        FormInput.add(A1_R_Y51);
        A1_R_Y51.setBounds(370, 950, 23, 23);

        A1_R_T52.setBorder(null);
        A1_R_T52.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T52.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T52.setName("A1_R_T52"); // NOI18N
        FormInput.add(A1_R_T52);
        A1_R_T52.setBounds(420, 1020, 23, 23);

        A1_R_T53.setBorder(null);
        A1_R_T53.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T53.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T53.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T53.setName("A1_R_T53"); // NOI18N
        FormInput.add(A1_R_T53);
        A1_R_T53.setBounds(310, 1020, 23, 23);

        A1_R_Y52.setBorder(null);
        A1_R_Y52.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y52.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y52.setName("A1_R_Y52"); // NOI18N
        FormInput.add(A1_R_Y52);
        A1_R_Y52.setBounds(260, 1020, 23, 23);

        A1_R_Y53.setBorder(null);
        A1_R_Y53.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y53.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y53.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y53.setName("A1_R_Y53"); // NOI18N
        FormInput.add(A1_R_Y53);
        A1_R_Y53.setBounds(370, 1020, 23, 23);

        label211.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label211.setText(" Ruangan");
        label211.setName("label211"); // NOI18N
        label211.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label211);
        label211.setBounds(270, 990, 50, 20);

        label212.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label212.setText("Tdk");
        label212.setName("label212"); // NOI18N
        label212.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label212);
        label212.setBounds(310, 1000, 20, 23);

        label213.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label213.setText("  Ya");
        label213.setName("label213"); // NOI18N
        label213.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label213);
        label213.setBounds(260, 1000, 30, 23);

        label214.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label214.setText(" IBS");
        label214.setName("label214"); // NOI18N
        label214.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label214);
        label214.setBounds(390, 990, 30, 20);

        label215.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label215.setText("Tdk");
        label215.setName("label215"); // NOI18N
        label215.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label215);
        label215.setBounds(420, 1000, 20, 23);

        label216.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label216.setText("  Ya");
        label216.setName("label216"); // NOI18N
        label216.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label216);
        label216.setBounds(370, 1000, 30, 23);

        A1_R_T54.setBorder(null);
        A1_R_T54.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T54.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T54.setName("A1_R_T54"); // NOI18N
        FormInput.add(A1_R_T54);
        A1_R_T54.setBounds(420, 1050, 23, 23);

        A1_R_T55.setBorder(null);
        A1_R_T55.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T55.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T55.setName("A1_R_T55"); // NOI18N
        FormInput.add(A1_R_T55);
        A1_R_T55.setBounds(310, 1050, 23, 23);

        A1_R_Y54.setBorder(null);
        A1_R_Y54.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y54.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y54.setName("A1_R_Y54"); // NOI18N
        FormInput.add(A1_R_Y54);
        A1_R_Y54.setBounds(260, 1050, 23, 23);

        A1_R_Y55.setBorder(null);
        A1_R_Y55.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y55.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y55.setName("A1_R_Y55"); // NOI18N
        FormInput.add(A1_R_Y55);
        A1_R_Y55.setBounds(370, 1050, 23, 23);

        A1_R_T56.setBorder(null);
        A1_R_T56.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T56.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T56.setName("A1_R_T56"); // NOI18N
        FormInput.add(A1_R_T56);
        A1_R_T56.setBounds(420, 1080, 23, 23);

        A1_R_T57.setBorder(null);
        A1_R_T57.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T57.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T57.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T57.setName("A1_R_T57"); // NOI18N
        FormInput.add(A1_R_T57);
        A1_R_T57.setBounds(310, 1080, 23, 23);

        A1_R_Y56.setBorder(null);
        A1_R_Y56.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y56.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y56.setName("A1_R_Y56"); // NOI18N
        FormInput.add(A1_R_Y56);
        A1_R_Y56.setBounds(260, 1080, 23, 23);

        A1_R_Y57.setBorder(null);
        A1_R_Y57.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y57.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y57.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y57.setName("A1_R_Y57"); // NOI18N
        FormInput.add(A1_R_Y57);
        A1_R_Y57.setBounds(370, 1080, 23, 23);

        A1_R_T58.setBorder(null);
        A1_R_T58.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T58.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T58.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T58.setName("A1_R_T58"); // NOI18N
        FormInput.add(A1_R_T58);
        A1_R_T58.setBounds(420, 1110, 23, 23);

        A1_R_T59.setBorder(null);
        A1_R_T59.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T59.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T59.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T59.setName("A1_R_T59"); // NOI18N
        FormInput.add(A1_R_T59);
        A1_R_T59.setBounds(310, 1110, 23, 23);

        A1_R_Y58.setBorder(null);
        A1_R_Y58.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y58.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y58.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y58.setName("A1_R_Y58"); // NOI18N
        FormInput.add(A1_R_Y58);
        A1_R_Y58.setBounds(260, 1110, 23, 23);

        A1_R_Y59.setBorder(null);
        A1_R_Y59.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y59.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y59.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y59.setName("A1_R_Y59"); // NOI18N
        FormInput.add(A1_R_Y59);
        A1_R_Y59.setBounds(370, 1110, 23, 23);

        A1_R_T60.setBorder(null);
        A1_R_T60.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T60.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T60.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T60.setName("A1_R_T60"); // NOI18N
        FormInput.add(A1_R_T60);
        A1_R_T60.setBounds(420, 1150, 23, 20);

        A1_R_T61.setBorder(null);
        A1_R_T61.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T61.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T61.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T61.setName("A1_R_T61"); // NOI18N
        FormInput.add(A1_R_T61);
        A1_R_T61.setBounds(310, 1150, 23, 20);

        A1_R_Y60.setBorder(null);
        A1_R_Y60.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y60.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y60.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y60.setName("A1_R_Y60"); // NOI18N
        FormInput.add(A1_R_Y60);
        A1_R_Y60.setBounds(260, 1150, 23, 20);

        A1_R_Y61.setBorder(null);
        A1_R_Y61.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y61.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y61.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y61.setName("A1_R_Y61"); // NOI18N
        FormInput.add(A1_R_Y61);
        A1_R_Y61.setBounds(370, 1150, 23, 20);

        A1_R_T62.setBorder(null);
        A1_R_T62.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T62.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T62.setName("A1_R_T62"); // NOI18N
        FormInput.add(A1_R_T62);
        A1_R_T62.setBounds(310, 1180, 23, 20);

        A1_R_Y62.setBorder(null);
        A1_R_Y62.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y62.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y62.setName("A1_R_Y62"); // NOI18N
        FormInput.add(A1_R_Y62);
        A1_R_Y62.setBounds(370, 1180, 23, 20);

        A1_R_Y63.setBorder(null);
        A1_R_Y63.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y63.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y63.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y63.setName("A1_R_Y63"); // NOI18N
        FormInput.add(A1_R_Y63);
        A1_R_Y63.setBounds(260, 1180, 23, 20);

        A1_R_T63.setBorder(null);
        A1_R_T63.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T63.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T63.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T63.setName("A1_R_T63"); // NOI18N
        FormInput.add(A1_R_T63);
        A1_R_T63.setBounds(420, 1180, 23, 20);

        A1_R_Y64.setBorder(null);
        A1_R_Y64.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y64.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y64.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y64.setName("A1_R_Y64"); // NOI18N
        FormInput.add(A1_R_Y64);
        A1_R_Y64.setBounds(370, 1210, 23, 20);

        A1_R_T64.setBorder(null);
        A1_R_T64.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T64.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T64.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T64.setName("A1_R_T64"); // NOI18N
        FormInput.add(A1_R_T64);
        A1_R_T64.setBounds(420, 1210, 23, 20);

        A1_R_Y65.setBorder(null);
        A1_R_Y65.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y65.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y65.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y65.setName("A1_R_Y65"); // NOI18N
        FormInput.add(A1_R_Y65);
        A1_R_Y65.setBounds(260, 1210, 23, 20);

        A1_R_T65.setBorder(null);
        A1_R_T65.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T65.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T65.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T65.setName("A1_R_T65"); // NOI18N
        FormInput.add(A1_R_T65);
        A1_R_T65.setBounds(310, 1210, 23, 20);

        A1_R_T66.setBorder(null);
        A1_R_T66.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T66.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T66.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T66.setName("A1_R_T66"); // NOI18N
        FormInput.add(A1_R_T66);
        A1_R_T66.setBounds(310, 1240, 23, 20);

        A1_R_T67.setBorder(null);
        A1_R_T67.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T67.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T67.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T67.setName("A1_R_T67"); // NOI18N
        FormInput.add(A1_R_T67);
        A1_R_T67.setBounds(420, 1240, 23, 20);

        A1_R_Y66.setBorder(null);
        A1_R_Y66.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y66.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y66.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y66.setName("A1_R_Y66"); // NOI18N
        FormInput.add(A1_R_Y66);
        A1_R_Y66.setBounds(370, 1240, 23, 20);

        A1_R_Y67.setBorder(null);
        A1_R_Y67.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y67.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y67.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y67.setName("A1_R_Y67"); // NOI18N
        FormInput.add(A1_R_Y67);
        A1_R_Y67.setBounds(260, 1240, 23, 20);

        A1_R_T68.setBorder(null);
        A1_R_T68.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T68.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T68.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T68.setName("A1_R_T68"); // NOI18N
        FormInput.add(A1_R_T68);
        A1_R_T68.setBounds(420, 1280, 23, 23);

        A1_R_Y68.setBorder(null);
        A1_R_Y68.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y68.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y68.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y68.setName("A1_R_Y68"); // NOI18N
        FormInput.add(A1_R_Y68);
        A1_R_Y68.setBounds(260, 1280, 23, 23);

        A1_R_T69.setBorder(null);
        A1_R_T69.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T69.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T69.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T69.setName("A1_R_T69"); // NOI18N
        FormInput.add(A1_R_T69);
        A1_R_T69.setBounds(310, 1280, 23, 23);

        A1_R_Y69.setBorder(null);
        A1_R_Y69.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y69.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y69.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y69.setName("A1_R_Y69"); // NOI18N
        FormInput.add(A1_R_Y69);
        A1_R_Y69.setBounds(370, 1280, 23, 23);

        A1_R_T70.setBorder(null);
        A1_R_T70.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T70.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T70.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T70.setName("A1_R_T70"); // NOI18N
        FormInput.add(A1_R_T70);
        A1_R_T70.setBounds(420, 1310, 23, 23);

        A1_R_Y70.setBorder(null);
        A1_R_Y70.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y70.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y70.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y70.setName("A1_R_Y70"); // NOI18N
        FormInput.add(A1_R_Y70);
        A1_R_Y70.setBounds(260, 1310, 23, 23);

        A1_R_T71.setBorder(null);
        A1_R_T71.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T71.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T71.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T71.setName("A1_R_T71"); // NOI18N
        FormInput.add(A1_R_T71);
        A1_R_T71.setBounds(310, 1310, 23, 23);

        A1_R_Y71.setBorder(null);
        A1_R_Y71.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y71.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y71.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y71.setName("A1_R_Y71"); // NOI18N
        FormInput.add(A1_R_Y71);
        A1_R_Y71.setBounds(370, 1310, 23, 23);

        A1_R_T72.setBorder(null);
        A1_R_T72.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T72.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T72.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T72.setName("A1_R_T72"); // NOI18N
        FormInput.add(A1_R_T72);
        A1_R_T72.setBounds(310, 1340, 23, 23);

        A1_R_Y72.setBorder(null);
        A1_R_Y72.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y72.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y72.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y72.setName("A1_R_Y72"); // NOI18N
        FormInput.add(A1_R_Y72);
        A1_R_Y72.setBounds(370, 1340, 23, 23);

        A1_R_T73.setBorder(null);
        A1_R_T73.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T73.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T73.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T73.setName("A1_R_T73"); // NOI18N
        FormInput.add(A1_R_T73);
        A1_R_T73.setBounds(420, 1340, 23, 23);

        A1_R_Y73.setBorder(null);
        A1_R_Y73.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y73.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y73.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y73.setName("A1_R_Y73"); // NOI18N
        FormInput.add(A1_R_Y73);
        A1_R_Y73.setBounds(260, 1340, 23, 23);

        A1_R_T74.setBorder(null);
        A1_R_T74.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T74.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T74.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T74.setName("A1_R_T74"); // NOI18N
        FormInput.add(A1_R_T74);
        A1_R_T74.setBounds(310, 1420, 23, 23);

        A1_R_Y74.setBorder(null);
        A1_R_Y74.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y74.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y74.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y74.setName("A1_R_Y74"); // NOI18N
        FormInput.add(A1_R_Y74);
        A1_R_Y74.setBounds(370, 1420, 23, 23);

        A1_R_T75.setBorder(null);
        A1_R_T75.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T75.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T75.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T75.setName("A1_R_T75"); // NOI18N
        FormInput.add(A1_R_T75);
        A1_R_T75.setBounds(420, 1420, 23, 23);

        A1_R_Y75.setBorder(null);
        A1_R_Y75.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y75.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y75.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y75.setName("A1_R_Y75"); // NOI18N
        FormInput.add(A1_R_Y75);
        A1_R_Y75.setBounds(260, 1420, 23, 23);

        A1_R_T76.setBorder(null);
        A1_R_T76.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T76.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T76.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T76.setName("A1_R_T76"); // NOI18N
        FormInput.add(A1_R_T76);
        A1_R_T76.setBounds(310, 1450, 23, 23);

        A1_R_Y76.setBorder(null);
        A1_R_Y76.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y76.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y76.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y76.setName("A1_R_Y76"); // NOI18N
        FormInput.add(A1_R_Y76);
        A1_R_Y76.setBounds(260, 1450, 23, 23);

        A1_R_Y77.setBorder(null);
        A1_R_Y77.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y77.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y77.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y77.setName("A1_R_Y77"); // NOI18N
        FormInput.add(A1_R_Y77);
        A1_R_Y77.setBounds(370, 1450, 23, 23);

        A1_R_T77.setBorder(null);
        A1_R_T77.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T77.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T77.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T77.setName("A1_R_T77"); // NOI18N
        FormInput.add(A1_R_T77);
        A1_R_T77.setBounds(420, 1450, 23, 23);

        A1_R_T78.setBorder(null);
        A1_R_T78.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T78.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T78.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T78.setName("A1_R_T78"); // NOI18N
        FormInput.add(A1_R_T78);
        A1_R_T78.setBounds(310, 1500, 23, 23);

        A1_R_T79.setBorder(null);
        A1_R_T79.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T79.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T79.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T79.setName("A1_R_T79"); // NOI18N
        FormInput.add(A1_R_T79);
        A1_R_T79.setBounds(420, 1500, 23, 23);

        A1_R_Y78.setBorder(null);
        A1_R_Y78.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y78.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y78.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y78.setName("A1_R_Y78"); // NOI18N
        FormInput.add(A1_R_Y78);
        A1_R_Y78.setBounds(260, 1500, 23, 23);

        A1_R_Y79.setBorder(null);
        A1_R_Y79.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y79.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y79.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y79.setName("A1_R_Y79"); // NOI18N
        FormInput.add(A1_R_Y79);
        A1_R_Y79.setBounds(370, 1500, 23, 23);

        A1_R_T80.setBorder(null);
        A1_R_T80.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T80.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T80.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T80.setName("A1_R_T80"); // NOI18N
        FormInput.add(A1_R_T80);
        A1_R_T80.setBounds(310, 1530, 23, 23);

        A1_R_Y80.setBorder(null);
        A1_R_Y80.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y80.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y80.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y80.setName("A1_R_Y80"); // NOI18N
        FormInput.add(A1_R_Y80);
        A1_R_Y80.setBounds(370, 1530, 23, 23);

        A1_R_T81.setBorder(null);
        A1_R_T81.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T81.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T81.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T81.setName("A1_R_T81"); // NOI18N
        FormInput.add(A1_R_T81);
        A1_R_T81.setBounds(420, 1530, 23, 23);

        A1_R_Y81.setBorder(null);
        A1_R_Y81.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y81.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y81.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y81.setName("A1_R_Y81"); // NOI18N
        FormInput.add(A1_R_Y81);
        A1_R_Y81.setBounds(260, 1530, 23, 23);

        A1_R_T82.setBorder(null);
        A1_R_T82.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T82.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T82.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T82.setName("A1_R_T82"); // NOI18N
        FormInput.add(A1_R_T82);
        A1_R_T82.setBounds(310, 1560, 23, 23);

        A1_R_Y82.setBorder(null);
        A1_R_Y82.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y82.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y82.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y82.setName("A1_R_Y82"); // NOI18N
        FormInput.add(A1_R_Y82);
        A1_R_Y82.setBounds(370, 1560, 23, 23);

        A1_R_T83.setBorder(null);
        A1_R_T83.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_T83.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_T83.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_T83.setName("A1_R_T83"); // NOI18N
        FormInput.add(A1_R_T83);
        A1_R_T83.setBounds(420, 1560, 23, 23);

        A1_R_Y83.setBorder(null);
        A1_R_Y83.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1_R_Y83.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1_R_Y83.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1_R_Y83.setName("A1_R_Y83"); // NOI18N
        FormInput.add(A1_R_Y83);
        A1_R_Y83.setBounds(260, 1560, 23, 23);

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));
        jPanel1.setName("jPanel1"); // NOI18N
        FormInput.add(jPanel1);
        jPanel1.setBounds(360, 80, 90, 310);

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));
        jPanel2.setName("jPanel2"); // NOI18N
        FormInput.add(jPanel2);
        jPanel2.setBounds(250, 80, 90, 310);

        jPanel3.setBackground(new java.awt.Color(204, 255, 255));
        jPanel3.setName("jPanel3"); // NOI18N
        FormInput.add(jPanel3);
        jPanel3.setBounds(360, 410, 90, 570);

        jPanel4.setBackground(new java.awt.Color(204, 255, 204));
        jPanel4.setName("jPanel4"); // NOI18N
        FormInput.add(jPanel4);
        jPanel4.setBounds(250, 410, 90, 570);

        jPanel5.setBackground(new java.awt.Color(204, 255, 255));
        jPanel5.setName("jPanel5"); // NOI18N
        FormInput.add(jPanel5);
        jPanel5.setBounds(360, 990, 90, 380);

        jPanel6.setBackground(new java.awt.Color(204, 255, 204));
        jPanel6.setName("jPanel6"); // NOI18N
        FormInput.add(jPanel6);
        jPanel6.setBounds(250, 990, 90, 380);

        label217.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label217.setText(" Ruangan");
        label217.setName("label217"); // NOI18N
        label217.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label217);
        label217.setBounds(270, 1380, 50, 20);

        label218.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label218.setText(" IBS");
        label218.setName("label218"); // NOI18N
        label218.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label218);
        label218.setBounds(390, 1380, 30, 20);

        label219.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label219.setText("Tdk");
        label219.setName("label219"); // NOI18N
        label219.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label219);
        label219.setBounds(420, 1390, 20, 30);

        label220.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label220.setText("  Ya");
        label220.setName("label220"); // NOI18N
        label220.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label220);
        label220.setBounds(370, 1390, 30, 30);

        label221.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label221.setText("Tdk");
        label221.setName("label221"); // NOI18N
        label221.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label221);
        label221.setBounds(310, 1390, 20, 30);

        label222.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label222.setText("  Ya");
        label222.setName("label222"); // NOI18N
        label222.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput.add(label222);
        label222.setBounds(260, 1390, 30, 30);

        jPanel7.setBackground(new java.awt.Color(204, 255, 204));
        jPanel7.setName("jPanel7"); // NOI18N
        FormInput.add(jPanel7);
        jPanel7.setBounds(250, 1380, 90, 210);

        jPanel8.setBackground(new java.awt.Color(204, 255, 255));
        jPanel8.setName("jPanel8"); // NOI18N
        FormInput.add(jPanel8);
        jPanel8.setBounds(360, 1380, 90, 210);

        jLabel83.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel83.setText("3. OP :");
        jLabel83.setName("jLabel83"); // NOI18N
        FormInput.add(jLabel83);
        jLabel83.setBounds(480, 1660, 40, 23);

        KeteranganFinansial9.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial9.setName("KeteranganFinansial9"); // NOI18N
        KeteranganFinansial9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial9KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial9);
        KeteranganFinansial9.setBounds(720, 450, 140, 20);

        KeteranganFinansial11.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial11.setName("KeteranganFinansial11"); // NOI18N
        KeteranganFinansial11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial11KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial11);
        KeteranganFinansial11.setBounds(460, 510, 400, 20);

        KeteranganFinansial12.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial12.setName("KeteranganFinansial12"); // NOI18N
        KeteranganFinansial12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial12KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial12);
        KeteranganFinansial12.setBounds(460, 540, 400, 20);

        KeteranganFinansial13.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial13.setName("KeteranganFinansial13"); // NOI18N
        KeteranganFinansial13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial13KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial13);
        KeteranganFinansial13.setBounds(460, 570, 400, 20);

        KeteranganFinansial17.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial17.setName("KeteranganFinansial17"); // NOI18N
        KeteranganFinansial17.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial17KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial17);
        KeteranganFinansial17.setBounds(460, 690, 400, 20);

        KeteranganFinansial18.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial18.setName("KeteranganFinansial18"); // NOI18N
        KeteranganFinansial18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial18KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial18);
        KeteranganFinansial18.setBounds(460, 950, 400, 20);

        KeteranganFinansial19.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial19.setName("KeteranganFinansial19"); // NOI18N
        KeteranganFinansial19.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial19KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial19);
        KeteranganFinansial19.setBounds(460, 920, 400, 20);

        KeteranganFinansial20.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial20.setName("KeteranganFinansial20"); // NOI18N
        KeteranganFinansial20.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial20KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial20);
        KeteranganFinansial20.setBounds(460, 890, 400, 20);

        KeteranganFinansial21.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial21.setName("KeteranganFinansial21"); // NOI18N
        KeteranganFinansial21.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial21KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial21);
        KeteranganFinansial21.setBounds(460, 860, 400, 20);

        KeteranganFinansial22.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial22.setName("KeteranganFinansial22"); // NOI18N
        KeteranganFinansial22.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial22KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial22);
        KeteranganFinansial22.setBounds(730, 830, 130, 20);

        KeteranganFinansial23.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial23.setName("KeteranganFinansial23"); // NOI18N
        KeteranganFinansial23.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial23KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial23);
        KeteranganFinansial23.setBounds(500, 800, 100, 20);

        KeteranganFinansial24.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial24.setName("KeteranganFinansial24"); // NOI18N
        KeteranganFinansial24.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial24KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial24);
        KeteranganFinansial24.setBounds(500, 770, 100, 20);

        KeteranganFinansial25.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial25.setName("KeteranganFinansial25"); // NOI18N
        KeteranganFinansial25.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial25KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial25);
        KeteranganFinansial25.setBounds(500, 740, 100, 20);

        jLabel84.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel84.setText("Tanggal Pasang :");
        jLabel84.setName("jLabel84"); // NOI18N
        FormInput.add(jLabel84);
        jLabel84.setBounds(460, 600, 90, 23);

        jLabel85.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel85.setText("Keterangan :");
        jLabel85.setName("jLabel85"); // NOI18N
        FormInput.add(jLabel85);
        jLabel85.setBounds(630, 710, 70, 23);

        KeteranganFinansial26.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial26.setName("KeteranganFinansial26"); // NOI18N
        KeteranganFinansial26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial26KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial26);
        KeteranganFinansial26.setBounds(460, 1240, 400, 20);

        KeteranganFinansial27.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial27.setName("KeteranganFinansial27"); // NOI18N
        KeteranganFinansial27.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial27KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial27);
        KeteranganFinansial27.setBounds(460, 1210, 400, 20);

        KeteranganFinansial28.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial28.setName("KeteranganFinansial28"); // NOI18N
        KeteranganFinansial28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial28KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial28);
        KeteranganFinansial28.setBounds(460, 1180, 400, 20);

        KeteranganFinansial29.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial29.setName("KeteranganFinansial29"); // NOI18N
        KeteranganFinansial29.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial29KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial29);
        KeteranganFinansial29.setBounds(460, 1150, 400, 20);

        KeteranganFinansial30.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial30.setName("KeteranganFinansial30"); // NOI18N
        KeteranganFinansial30.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial30KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial30);
        KeteranganFinansial30.setBounds(460, 1110, 400, 20);

        KeteranganFinansial31.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial31.setName("KeteranganFinansial31"); // NOI18N
        KeteranganFinansial31.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial31KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial31);
        KeteranganFinansial31.setBounds(460, 1080, 400, 20);

        KeteranganFinansial32.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial32.setName("KeteranganFinansial32"); // NOI18N
        KeteranganFinansial32.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial32KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial32);
        KeteranganFinansial32.setBounds(460, 1050, 400, 20);

        KeteranganFinansial33.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial33.setName("KeteranganFinansial33"); // NOI18N
        KeteranganFinansial33.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial33KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial33);
        KeteranganFinansial33.setBounds(460, 1020, 400, 20);

        KeteranganFinansial34.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial34.setName("KeteranganFinansial34"); // NOI18N
        KeteranganFinansial34.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial34KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial34);
        KeteranganFinansial34.setBounds(460, 1340, 400, 20);

        KeteranganFinansial37.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial37.setName("KeteranganFinansial37"); // NOI18N
        KeteranganFinansial37.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial37KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial37);
        KeteranganFinansial37.setBounds(460, 1450, 400, 20);

        KeteranganFinansial39.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial39.setName("KeteranganFinansial39"); // NOI18N
        KeteranganFinansial39.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial39KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial39);
        KeteranganFinansial39.setBounds(460, 1420, 400, 20);

        KeteranganFinansial38.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial38.setName("KeteranganFinansial38"); // NOI18N
        KeteranganFinansial38.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial38KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial38);
        KeteranganFinansial38.setBounds(520, 1660, 230, 20);

        KeteranganFinansial40.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial40.setName("KeteranganFinansial40"); // NOI18N
        KeteranganFinansial40.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial40KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial40);
        KeteranganFinansial40.setBounds(460, 1500, 400, 20);

        KeteranganFinansial41.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial41.setName("KeteranganFinansial41"); // NOI18N
        KeteranganFinansial41.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial41KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial41);
        KeteranganFinansial41.setBounds(460, 1530, 400, 20);

        jLabel86.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel86.setText("Keterangan :");
        jLabel86.setName("jLabel86"); // NOI18N
        FormInput.add(jLabel86);
        jLabel86.setBounds(620, 990, 70, 23);

        Detik1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik1.setName("Detik1"); // NOI18N
        Detik1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik1KeyPressed(evt);
            }
        });
        FormInput.add(Detik1);
        Detik1.setBounds(620, 450, 50, 23);

        Jam1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam1.setName("Jam1"); // NOI18N
        Jam1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam1ActionPerformed(evt);
            }
        });
        Jam1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam1KeyPressed(evt);
            }
        });
        FormInput.add(Jam1);
        Jam1.setBounds(500, 450, 50, 23);

        Menit1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit1.setName("Menit1"); // NOI18N
        Menit1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit1KeyPressed(evt);
            }
        });
        FormInput.add(Menit1);
        Menit1.setBounds(560, 450, 50, 23);

        jLabel87.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel87.setText("Keterangan :");
        jLabel87.setName("jLabel87"); // NOI18N
        FormInput.add(jLabel87);
        jLabel87.setBounds(630, 420, 70, 23);

        jLabel88.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel88.setText("Kunci :");
        jLabel88.setName("jLabel88"); // NOI18N
        FormInput.add(jLabel88);
        jLabel88.setBounds(690, 600, 40, 23);

        jenis_anestesi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "APD Level III", "APD Level II", "APD Level I" }));
        jenis_anestesi.setName("jenis_anestesi"); // NOI18N
        jenis_anestesi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jenis_anestesiActionPerformed(evt);
            }
        });
        jenis_anestesi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jenis_anestesiKeyPressed(evt);
            }
        });
        FormInput.add(jenis_anestesi);
        jenis_anestesi.setBounds(460, 1310, 210, 20);

        Tanggal1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "27-02-2025" }));
        Tanggal1.setDisplayFormat("dd-MM-yyyy");
        Tanggal1.setName("Tanggal1"); // NOI18N
        Tanggal1.setOpaque(false);
        Tanggal1.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput.add(Tanggal1);
        Tanggal1.setBounds(550, 600, 110, 23);

        jLabel89.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel89.setText("Jam :");
        jLabel89.setName("jLabel89"); // NOI18N
        FormInput.add(jLabel89);
        jLabel89.setBounds(460, 450, 30, 23);

        KeteranganFinansial10.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial10.setName("KeteranganFinansial10"); // NOI18N
        KeteranganFinansial10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial10KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial10);
        KeteranganFinansial10.setBounds(730, 600, 130, 20);

        jLabel90.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel90.setText("Dosis :");
        jLabel90.setName("jLabel90"); // NOI18N
        FormInput.add(jLabel90);
        jLabel90.setBounds(680, 450, 40, 23);

        jLabel91.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel91.setText("Tanggal Pasang :");
        jLabel91.setName("jLabel91"); // NOI18N
        FormInput.add(jLabel91);
        jLabel91.setBounds(460, 630, 90, 23);

        Tanggal2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "27-02-2025" }));
        Tanggal2.setDisplayFormat("dd-MM-yyyy");
        Tanggal2.setName("Tanggal2"); // NOI18N
        Tanggal2.setOpaque(false);
        Tanggal2.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput.add(Tanggal2);
        Tanggal2.setBounds(550, 630, 110, 23);

        jLabel92.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel92.setText("di :");
        jLabel92.setName("jLabel92"); // NOI18N
        FormInput.add(jLabel92);
        jLabel92.setBounds(710, 630, 20, 23);

        KeteranganFinansial42.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial42.setName("KeteranganFinansial42"); // NOI18N
        KeteranganFinansial42.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial42KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial42);
        KeteranganFinansial42.setBounds(730, 630, 130, 20);

        jLabel93.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel93.setText("Kedalaman :");
        jLabel93.setName("jLabel93"); // NOI18N
        FormInput.add(jLabel93);
        jLabel93.setBounds(670, 660, 60, 23);

        Tanggal3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "27-02-2025" }));
        Tanggal3.setDisplayFormat("dd-MM-yyyy");
        Tanggal3.setName("Tanggal3"); // NOI18N
        Tanggal3.setOpaque(false);
        Tanggal3.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput.add(Tanggal3);
        Tanggal3.setBounds(550, 660, 110, 23);

        jLabel94.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel94.setText("Tanggal Pasang :");
        jLabel94.setName("jLabel94"); // NOI18N
        FormInput.add(jLabel94);
        jLabel94.setBounds(460, 660, 90, 23);

        KeteranganFinansial43.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial43.setName("KeteranganFinansial43"); // NOI18N
        KeteranganFinansial43.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial43KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial43);
        KeteranganFinansial43.setBounds(730, 660, 130, 20);

        jLabel95.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel95.setText("Obat :");
        jLabel95.setName("jLabel95"); // NOI18N
        FormInput.add(jLabel95);
        jLabel95.setBounds(460, 830, 40, 23);

        jLabel96.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel96.setText("Sedia :");
        jLabel96.setName("jLabel96"); // NOI18N
        FormInput.add(jLabel96);
        jLabel96.setBounds(460, 770, 40, 23);

        jLabel97.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel97.setText("Sedia :");
        jLabel97.setName("jLabel97"); // NOI18N
        FormInput.add(jLabel97);
        jLabel97.setBounds(460, 740, 40, 23);

        jLabel98.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel98.setText("Kolf");
        jLabel98.setName("jLabel98"); // NOI18N
        FormInput.add(jLabel98);
        jLabel98.setBounds(610, 740, 20, 23);

        jLabel99.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel99.setText("Kolf");
        jLabel99.setName("jLabel99"); // NOI18N
        FormInput.add(jLabel99);
        jLabel99.setBounds(610, 770, 20, 23);

        jLabel100.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel100.setText("Rute :");
        jLabel100.setName("jLabel100"); // NOI18N
        FormInput.add(jLabel100);
        jLabel100.setBounds(690, 830, 30, 23);

        jLabel101.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel101.setText("Sedia :");
        jLabel101.setName("jLabel101"); // NOI18N
        FormInput.add(jLabel101);
        jLabel101.setBounds(460, 800, 40, 23);

        KeteranganFinansial44.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial44.setName("KeteranganFinansial44"); // NOI18N
        KeteranganFinansial44.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial44KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial44);
        KeteranganFinansial44.setBounds(500, 830, 160, 20);

        jLabel102.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel102.setText("Kolf");
        jLabel102.setName("jLabel102"); // NOI18N
        FormInput.add(jLabel102);
        jLabel102.setBounds(610, 800, 20, 23);

        jenis_anestesi1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Positif", "Negatif" }));
        jenis_anestesi1.setName("jenis_anestesi1"); // NOI18N
        jenis_anestesi1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jenis_anestesi1ActionPerformed(evt);
            }
        });
        jenis_anestesi1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jenis_anestesi1KeyPressed(evt);
            }
        });
        FormInput.add(jenis_anestesi1);
        jenis_anestesi1.setBounds(460, 480, 210, 23);

        jenis_anestesi2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "APD Level III", "APD Level II", "APD Level I" }));
        jenis_anestesi2.setName("jenis_anestesi2"); // NOI18N
        jenis_anestesi2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jenis_anestesi2ActionPerformed(evt);
            }
        });
        jenis_anestesi2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jenis_anestesi2KeyPressed(evt);
            }
        });
        FormInput.add(jenis_anestesi2);
        jenis_anestesi2.setBounds(460, 1280, 210, 20);

        KeteranganFinansial45.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial45.setName("KeteranganFinansial45"); // NOI18N
        KeteranganFinansial45.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial45KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial45);
        KeteranganFinansial45.setBounds(460, 1560, 400, 20);

        KeteranganFinansial46.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial46.setName("KeteranganFinansial46"); // NOI18N
        KeteranganFinansial46.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial46KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial46);
        KeteranganFinansial46.setBounds(520, 1600, 230, 20);

        KeteranganFinansial47.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial47.setName("KeteranganFinansial47"); // NOI18N
        KeteranganFinansial47.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial47KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial47);
        KeteranganFinansial47.setBounds(520, 1630, 230, 20);

        jLabel103.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel103.setText("Keterangan :");
        jLabel103.setName("jLabel103"); // NOI18N
        FormInput.add(jLabel103);
        jLabel103.setBounds(630, 1380, 70, 23);

        jLabel104.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel104.setText("1. OP :");
        jLabel104.setName("jLabel104"); // NOI18N
        FormInput.add(jLabel104);
        jLabel104.setBounds(480, 1600, 40, 23);

        jLabel105.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel105.setText("2. OP :");
        jLabel105.setName("jLabel105"); // NOI18N
        FormInput.add(jLabel105);
        jLabel105.setBounds(480, 1630, 40, 23);

        jLabel106.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel106.setText("Th :");
        jLabel106.setName("jLabel106"); // NOI18N
        FormInput.add(jLabel106);
        jLabel106.setBounds(760, 1660, 20, 23);

        jLabel107.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel107.setText("Th :");
        jLabel107.setName("jLabel107"); // NOI18N
        FormInput.add(jLabel107);
        jLabel107.setBounds(760, 1600, 20, 23);

        jLabel108.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel108.setText("Th :");
        jLabel108.setName("jLabel108"); // NOI18N
        FormInput.add(jLabel108);
        jLabel108.setBounds(760, 1630, 20, 23);

        KeteranganFinansial48.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial48.setName("KeteranganFinansial48"); // NOI18N
        KeteranganFinansial48.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial48KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial48);
        KeteranganFinansial48.setBounds(780, 1600, 80, 20);

        KeteranganFinansial49.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial49.setName("KeteranganFinansial49"); // NOI18N
        KeteranganFinansial49.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial49KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial49);
        KeteranganFinansial49.setBounds(780, 1630, 80, 20);

        KeteranganFinansial50.setFocusTraversalPolicyProvider(true);
        KeteranganFinansial50.setName("KeteranganFinansial50"); // NOI18N
        KeteranganFinansial50.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganFinansial50KeyPressed(evt);
            }
        });
        FormInput.add(KeteranganFinansial50);
        KeteranganFinansial50.setBounds(780, 1660, 80, 20);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Penilaian", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "27-02-2025" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "27-02-2025" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        PanelAccor.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor.setName("PanelAccor"); // NOI18N
        PanelAccor.setPreferredSize(new java.awt.Dimension(470, 43));
        PanelAccor.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setSelected(true);
        ChkAccor.setFocusable(false);
        ChkAccor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor.setName("ChkAccor"); // NOI18N
        ChkAccor.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccorActionPerformed(evt);
            }
        });
        PanelAccor.add(ChkAccor, java.awt.BorderLayout.WEST);

        FormMenu.setBackground(new java.awt.Color(255, 255, 255));
        FormMenu.setBorder(null);
        FormMenu.setName("FormMenu"); // NOI18N
        FormMenu.setPreferredSize(new java.awt.Dimension(115, 43));
        FormMenu.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        jLabel34.setText("Pasien :");
        jLabel34.setName("jLabel34"); // NOI18N
        jLabel34.setPreferredSize(new java.awt.Dimension(55, 23));
        FormMenu.add(jLabel34);

        TNoRM1.setEditable(false);
        TNoRM1.setHighlighter(null);
        TNoRM1.setName("TNoRM1"); // NOI18N
        TNoRM1.setPreferredSize(new java.awt.Dimension(100, 23));
        FormMenu.add(TNoRM1);

        TPasien1.setEditable(false);
        TPasien1.setBackground(new java.awt.Color(245, 250, 240));
        TPasien1.setHighlighter(null);
        TPasien1.setName("TPasien1"); // NOI18N
        TPasien1.setPreferredSize(new java.awt.Dimension(250, 23));
        FormMenu.add(TPasien1);

        BtnPrint1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/item (copy).png"))); // NOI18N
        BtnPrint1.setMnemonic('T');
        BtnPrint1.setToolTipText("Alt+T");
        BtnPrint1.setName("BtnPrint1"); // NOI18N
        BtnPrint1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPrint1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrint1ActionPerformed(evt);
            }
        });
        FormMenu.add(BtnPrint1);

        PanelAccor.add(FormMenu, java.awt.BorderLayout.NORTH);

        FormSkriningMPP.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 254)));
        FormSkriningMPP.setName("FormSkriningMPP"); // NOI18N
        FormSkriningMPP.setLayout(new java.awt.GridLayout(5, 0, 1, 1));

        Scroll7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 254)));
        Scroll7.setName("Scroll7"); // NOI18N
        Scroll7.setOpaque(true);
        Scroll7.setPreferredSize(new java.awt.Dimension(452, 202));

        tbTampilPotensi.setName("tbTampilPotensi"); // NOI18N
        Scroll7.setViewportView(tbTampilPotensi);

        FormSkriningMPP.add(Scroll7);

        Scroll9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll9.setName("Scroll9"); // NOI18N
        Scroll9.setOpaque(true);
        Scroll9.setPreferredSize(new java.awt.Dimension(452, 202));

        tbTampilKasus.setName("tbTampilKasus"); // NOI18N
        Scroll9.setViewportView(tbTampilKasus);

        FormSkriningMPP.add(Scroll9);

        Scroll13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll13.setName("Scroll13"); // NOI18N
        Scroll13.setOpaque(true);
        Scroll13.setPreferredSize(new java.awt.Dimension(452, 202));

        tbTampilIGD.setName("tbTampilIGD"); // NOI18N
        Scroll13.setViewportView(tbTampilIGD);

        FormSkriningMPP.add(Scroll13);

        Scroll14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll14.setName("Scroll14"); // NOI18N
        Scroll14.setOpaque(true);
        Scroll14.setPreferredSize(new java.awt.Dimension(452, 202));

        tbTampilFinansial.setName("tbTampilFinansial"); // NOI18N
        Scroll14.setViewportView(tbTampilFinansial);

        FormSkriningMPP.add(Scroll14);

        Scroll15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll15.setName("Scroll15"); // NOI18N
        Scroll15.setOpaque(true);
        Scroll15.setPreferredSize(new java.awt.Dimension(452, 202));

        tbTampilLayanan.setName("tbTampilLayanan"); // NOI18N
        Scroll15.setViewportView(tbTampilLayanan);

        FormSkriningMPP.add(Scroll15);

        PanelAccor.add(FormSkriningMPP, java.awt.BorderLayout.CENTER);

        internalFrame3.add(PanelAccor, java.awt.BorderLayout.EAST);

        TabRawat.addTab("Data Penilaian", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmPetugas.getText().trim().equals("")){    
            Valid.textKosong(BtnPetugas,"Petugas");
        }else{  
            if(Sequel.menyimpantf("checklist_skrining_mpp","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No. Rawat",15,new String[]{
                TNoRw.getText(),Valid.SetTgl(Tanggal.getSelectedItem()+"")+" "+Tanggal.getSelectedItem().toString().substring(11,19),
              
                KdPetugas.getText()
            })==true){
                //potensi komplain
              
                //mengkosongkan field isian
                emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
           
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(KdPetugas.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmPetugas.getText().trim().equals("")){    
            Valid.textKosong(BtnPetugas,"Petugas");
        }else{  
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(KdPetugas.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString())){
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh petugas yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }   
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
//        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        if(tabMode.getRowCount()==0){
//            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//            BtnBatal.requestFocus();
//        }else if(tabMode.getRowCount()!=0){
//            try{
//                if(TCari.getText().equals("")){
//                    ps=koneksi.prepareStatement(
//                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat,penilaian_awal_keperawatan_ralan.tanggal,"+
//                            "penilaian_awal_keperawatan_ralan.informasi,penilaian_awal_keperawatan_ralan.td,penilaian_awal_keperawatan_ralan.nadi,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,"+
//                            "penilaian_awal_keperawatan_ralan.spo2,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.gcs,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,penilaian_awal_keperawatan_ralan.bmi,penilaian_awal_keperawatan_ralan.keluhan_utama,"+    //CUSTOM MUHSIN -> NADI DOUBLE, GANTI 1 JADI SPO2
//                            "penilaian_awal_keperawatan_ralan.rpd,penilaian_awal_keperawatan_ralan.rpk,penilaian_awal_keperawatan_ralan.rpo,penilaian_awal_keperawatan_ralan.alergi,penilaian_awal_keperawatan_ralan.alat_bantu,penilaian_awal_keperawatan_ralan.ket_bantu,penilaian_awal_keperawatan_ralan.prothesa,"+
//                            "penilaian_awal_keperawatan_ralan.ket_pro,penilaian_awal_keperawatan_ralan.adl,penilaian_awal_keperawatan_ralan.status_psiko,penilaian_awal_keperawatan_ralan.ket_psiko,penilaian_awal_keperawatan_ralan.hub_keluarga,penilaian_awal_keperawatan_ralan.tinggal_dengan,"+
//                            "penilaian_awal_keperawatan_ralan.ket_tinggal,penilaian_awal_keperawatan_ralan.ekonomi,penilaian_awal_keperawatan_ralan.edukasi,penilaian_awal_keperawatan_ralan.ket_edukasi,penilaian_awal_keperawatan_ralan.berjalan_a,penilaian_awal_keperawatan_ralan.berjalan_b,"+
//                            "penilaian_awal_keperawatan_ralan.berjalan_c,penilaian_awal_keperawatan_ralan.hasil,penilaian_awal_keperawatan_ralan.lapor,penilaian_awal_keperawatan_ralan.ket_lapor,penilaian_awal_keperawatan_ralan.sg1,penilaian_awal_keperawatan_ralan.nilai1,penilaian_awal_keperawatan_ralan.sg2,penilaian_awal_keperawatan_ralan.nilai2,"+
//                            "penilaian_awal_keperawatan_ralan.total_hasil,penilaian_awal_keperawatan_ralan.nyeri,penilaian_awal_keperawatan_ralan.provokes,penilaian_awal_keperawatan_ralan.ket_provokes,penilaian_awal_keperawatan_ralan.quality,penilaian_awal_keperawatan_ralan.ket_quality,penilaian_awal_keperawatan_ralan.lokasi,penilaian_awal_keperawatan_ralan.menyebar,"+
//                            "penilaian_awal_keperawatan_ralan.skala_nyeri,penilaian_awal_keperawatan_ralan.durasi,penilaian_awal_keperawatan_ralan.nyeri_hilang,penilaian_awal_keperawatan_ralan.ket_nyeri,penilaian_awal_keperawatan_ralan.pada_dokter,penilaian_awal_keperawatan_ralan.ket_dokter,penilaian_awal_keperawatan_ralan.rencana,"+
//                            "penilaian_awal_keperawatan_ralan.nip,petugas.nama,penilaian_awal_keperawatan_ralan.budaya,penilaian_awal_keperawatan_ralan.ket_budaya "+
//                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                            "inner join penilaian_awal_keperawatan_ralan on reg_periksa.no_rawat=penilaian_awal_keperawatan_ralan.no_rawat "+
//                            "inner join petugas on penilaian_awal_keperawatan_ralan.nip=petugas.nip "+
//                            "inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
//                            "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik where "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? order by penilaian_awal_keperawatan_ralan.tanggal");
//                }else{
//                    ps=koneksi.prepareStatement(
//                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat,penilaian_awal_keperawatan_ralan.tanggal,"+
//                            "penilaian_awal_keperawatan_ralan.informasi,penilaian_awal_keperawatan_ralan.td,penilaian_awal_keperawatan_ralan.nadi,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,"+
//                            "penilaian_awal_keperawatan_ralan.spo2,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.gcs,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,penilaian_awal_keperawatan_ralan.bmi,penilaian_awal_keperawatan_ralan.keluhan_utama,"+    //CUSTOM MUHSIN -> NADI DOUBLE, GANTI 1 JADI SPO2
//                            "penilaian_awal_keperawatan_ralan.rpd,penilaian_awal_keperawatan_ralan.rpk,penilaian_awal_keperawatan_ralan.rpo,penilaian_awal_keperawatan_ralan.alergi,penilaian_awal_keperawatan_ralan.alat_bantu,penilaian_awal_keperawatan_ralan.ket_bantu,penilaian_awal_keperawatan_ralan.prothesa,"+
//                            "penilaian_awal_keperawatan_ralan.ket_pro,penilaian_awal_keperawatan_ralan.adl,penilaian_awal_keperawatan_ralan.status_psiko,penilaian_awal_keperawatan_ralan.ket_psiko,penilaian_awal_keperawatan_ralan.hub_keluarga,penilaian_awal_keperawatan_ralan.tinggal_dengan,"+
//                            "penilaian_awal_keperawatan_ralan.ket_tinggal,penilaian_awal_keperawatan_ralan.ekonomi,penilaian_awal_keperawatan_ralan.edukasi,penilaian_awal_keperawatan_ralan.ket_edukasi,penilaian_awal_keperawatan_ralan.berjalan_a,penilaian_awal_keperawatan_ralan.berjalan_b,"+
//                            "penilaian_awal_keperawatan_ralan.berjalan_c,penilaian_awal_keperawatan_ralan.hasil,penilaian_awal_keperawatan_ralan.lapor,penilaian_awal_keperawatan_ralan.ket_lapor,penilaian_awal_keperawatan_ralan.sg1,penilaian_awal_keperawatan_ralan.nilai1,penilaian_awal_keperawatan_ralan.sg2,penilaian_awal_keperawatan_ralan.nilai2,"+
//                            "penilaian_awal_keperawatan_ralan.total_hasil,penilaian_awal_keperawatan_ralan.nyeri,penilaian_awal_keperawatan_ralan.provokes,penilaian_awal_keperawatan_ralan.ket_provokes,penilaian_awal_keperawatan_ralan.quality,penilaian_awal_keperawatan_ralan.ket_quality,penilaian_awal_keperawatan_ralan.lokasi,penilaian_awal_keperawatan_ralan.menyebar,"+
//                            "penilaian_awal_keperawatan_ralan.skala_nyeri,penilaian_awal_keperawatan_ralan.durasi,penilaian_awal_keperawatan_ralan.nyeri_hilang,penilaian_awal_keperawatan_ralan.ket_nyeri,penilaian_awal_keperawatan_ralan.pada_dokter,penilaian_awal_keperawatan_ralan.ket_dokter,penilaian_awal_keperawatan_ralan.rencana,"+
//                            "penilaian_awal_keperawatan_ralan.nip,petugas.nama,penilaian_awal_keperawatan_ralan.budaya,penilaian_awal_keperawatan_ralan.ket_budaya "+
//                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                            "inner join penilaian_awal_keperawatan_ralan on reg_periksa.no_rawat=penilaian_awal_keperawatan_ralan.no_rawat "+
//                            "inner join petugas on penilaian_awal_keperawatan_ralan.nip=petugas.nip "+
//                            "inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
//                            "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik where "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and reg_periksa.no_rawat like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and pasien.no_rkm_medis like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and pasien.nm_pasien like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and penilaian_awal_keperawatan_ralan.nip like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and petugas.nama like ? order by penilaian_awal_keperawatan_ralan.tanggal");
//                }
//
//                try {
//                    if(TCari.getText().equals("")){
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                    }else{
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(3,"%"+TCari.getText()+"%");
//                        ps.setString(4,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(5,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(6,"%"+TCari.getText()+"%");
//                        ps.setString(7,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(8,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(9,"%"+TCari.getText()+"%");
//                        ps.setString(10,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(11,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(12,"%"+TCari.getText()+"%");
//                        ps.setString(13,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(14,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(15,"%"+TCari.getText()+"%");
//                    }   
//                    rs=ps.executeQuery();
//                    htmlContent = new StringBuilder();
//                    htmlContent.append(                             
//                        "<tr class='isi'>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='9%'><b>PASIEN & PETUGAS</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='5%'><b>I. KEADAAN UMUM</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='5%'><b>II. STATUS NUTRISI</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='13%'><b>III. RIWAYAT KESEHATAN</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='8%'><b>IV. FUNGSIONAL</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='16%'><b>V. RIWAYAT PSIKO-SOSIAL SPIRITUAL DAN BUDAYA</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='16%'><b>VI. PENILAIAN RESIKO JATUH</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='11%'><b>VII. SKRINING GIZI</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='11%'><b>VIII. PENILAIAN TINGKAT NYERI</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='6%'><b>MASALAH & RENCANA KEPERAWATAN</b></td>"+
//                        "</tr>"
//                    );
//                    while(rs.next()){
//                        masalahkeperawatan="";
//                        ps2=koneksi.prepareStatement(
//                            "select master_masalah_keperawatan.kode_masalah,master_masalah_keperawatan.nama_masalah from master_masalah_keperawatan "+
//                            "inner join penilaian_awal_keperawatan_ralan_masalah on penilaian_awal_keperawatan_ralan_masalah.kode_masalah=master_masalah_keperawatan.kode_masalah "+
//                            "where penilaian_awal_keperawatan_ralan_masalah.no_rawat=? order by kode_masalah");
//                        try {
//                            ps2.setString(1,rs.getString("no_rawat"));
//                            rs2=ps2.executeQuery();
//                            while(rs2.next()){
//                                masalahkeperawatan=rs2.getString("nama_masalah")+", "+masalahkeperawatan;
//                            }
//                        } catch (Exception e) {
//                            System.out.println("Notif : "+e);
//                        } finally{
//                            if(rs2!=null){
//                                rs2.close();
//                            }
//                            if(ps2!=null){
//                                ps2.close();
//                            }
//                        }
//                        htmlContent.append(
//                            "<tr class='isi'>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>No.Rawat</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("no_rawat")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>No.R.M.</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("no_rkm_medis")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Nama Pasien</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nm_pasien")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>J.K.</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("jk")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Agama</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("agama")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Bahasa</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nama_bahasa")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Tgl.Lahir</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nama_cacat")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Cacat Fisik</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("tgl_lahir")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Petugas</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nip")+" "+rs.getString("nama")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Tgl.Asuhan</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("tanggal")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Informasi</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("informasi")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>TD</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("td")+"mmHg</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>Nadi</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("nadi")+"x/menit</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>RR</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("rr")+"x/menit</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>Suhu</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("suhu")+"°C</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>GCS</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("gcs")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>BB</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("bb")+"Kg</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>TB</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("tb")+"cm</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>BMI</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("bmi")+"Kg/m²</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Keluhan Utama</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("keluhan_utama")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>RPD</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("rpd")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>RPK</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("rpk")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>RPO</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("rpo")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Alergi</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("alergi")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Alat Bantu</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("alat_bantu")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Alat Bantu</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_bantu")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Prothesa</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("prothesa")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Prothesa</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_pro")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>ADL</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("adl")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Status Psikologis</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("status_psiko")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Psikologi</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_psiko")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Hubungan pasien dengan anggota keluarga</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("hub_keluarga")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Tinggal dengan</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("tinggal_dengan")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Tinggal</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_tinggal")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ekonomi</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ekonomi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Kepercayaan / Budaya / Nilai-nilai khusus yang perlu diperhatikan</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("budaya")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Budaya</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_budaya")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Edukasi diberikan kepada </td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("edukasi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Edukasi</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_edukasi")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Tidak seimbang/sempoyongan/limbung</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("berjalan_a")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Jalan dengan menggunakan alat bantu (kruk, tripot, kursi roda, orang lain)</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("berjalan_b")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Menopang saat akan duduk, tampak memegang pinggiran kursi atau meja/benda lain sebagai penopang</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("berjalan_c")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Hasil</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("hasil")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Dilaporan ke dokter?</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("lapor")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Jam Lapor</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_lapor")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Apakah ada penurunan berat badanyang tidak diinginkan selama enam bulan terakhir?</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("sg1")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Apakah nafsu makan berkurang karena tidak nafsu makan?</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("sg2")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Nilai 1</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("nilai1")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Nilai 2</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("nilai2")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Total Skor</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("total_hasil")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Tingkat Nyeri</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("nyeri")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Provokes</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("provokes")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Provokes</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_provokes")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Kualitas</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("quality")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Kualitas</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_quality")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Lokas</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("lokasi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Menyebar</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("menyebar")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Skala Nyeri</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("skala_nyeri")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Durasi</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("durasi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Nyeri Hilang</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("nyeri_hilang")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Hilang Nyeri</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_nyeri")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Lapor Ke Dokter</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("pada_dokter")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Jam Lapor</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_dokter")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "Masalah Keperawatan : "+masalahkeperawatan+"<br><br>"+
//                                    "Rencana Keperawatan : "+rs.getString("rencana")+
//                                "</td>"+
//                            "</tr>"
//                        );
//                    }
//                    LoadHTML.setText(
//                        "<html>"+
//                          "<table width='1800px' border='0' align='center' cellpadding='1px' cellspacing='0' class='tbl_form'>"+
//                           htmlContent.toString()+
//                          "</table>"+
//                        "</html>"
//                    );
//
//                    File g = new File("file2.css");            
//                    BufferedWriter bg = new BufferedWriter(new FileWriter(g));
//                    bg.write(
//                        ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
//                        ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
//                        ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
//                        ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
//                        ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
//                        ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
//                    );
//                    bg.close();
//
//                    File f = new File("DataPenilaianAwalKeperawatanRalan.html");            
//                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));            
//                    bw.write(LoadHTML.getText().replaceAll("<head>","<head>"+
//                                "<link href=\"file2.css\" rel=\"stylesheet\" type=\"text/css\" />"+
//                                "<table width='1800px' border='0' align='center' cellpadding='3px' cellspacing='0' class='tbl_form'>"+
//                                    "<tr class='isi2'>"+
//                                        "<td valign='top' align='center'>"+
//                                            "<font size='4' face='Tahoma'>"+akses.getnamars()+"</font><br>"+
//                                            akses.getalamatrs()+", "+akses.getkabupatenrs()+", "+akses.getpropinsirs()+"<br>"+
//                                            akses.getkontakrs()+", E-mail : "+akses.getemailrs()+"<br><br>"+
//                                            "<font size='2' face='Tahoma'>DATA PENILAIAN AWAL KEPERAWATAN RAWAT JALAN<br><br></font>"+        
//                                        "</td>"+
//                                   "</tr>"+
//                                "</table>")
//                    );
//                    bw.close();                         
//                    Desktop.getDesktop().browse(f.toURI());
//                } catch (Exception e) {
//                    System.out.println("Notif : "+e);
//                } finally{
//                    if(rs!=null){
//                        rs.close();
//                    }
//                    if(ps!=null){
//                        ps.close();
//                    }
//                }
//
//            }catch(Exception e){
//                System.out.println("Notifikasi : "+e);
//            }
//        }
//        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                ChkAccor.setSelected(true);
                isMenu();
                getMasalah();
            } catch (java.lang.NullPointerException e) {
            }
            if(evt.getClickCount()==2){
                getData();
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    ChkAccor.setSelected(true);
                    isMenu();
                    getMasalah();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        tampilPotensi();
        tampilPenyakitKronik();
        tampilTingkatMasukIGD();
        tampilFinansial();
        tampilLayanan();
    }//GEN-LAST:event_formWindowOpened

    private void ChkAccorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccorActionPerformed
        if(tbObat.getSelectedRow()!= -1){
            isMenu();
        }else{
            ChkAccor.setSelected(false);
            JOptionPane.showMessageDialog(null,"Maaf, silahkan pilih data yang mau ditampilkan...!!!!");
        }
    }//GEN-LAST:event_ChkAccorActionPerformed

    private void BtnPrint1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrint1ActionPerformed
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();    
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());          
            param.put("logo",Sequel.cariGambar("select logo from setting")); 
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),19).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),18).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString())); 
            param.put("alamat",Sequel.cariIsi("select concat(pasien.alamat,', ',k.nm_kel,', ',k2.nm_kec,', ',k3.nm_kab) as alamat\n" +
                "from pasien\n" +
                "inner join kelurahan k on pasien.kd_kel = k.kd_kel\n" +
                "inner join kecamatan k2 on pasien.kd_kec = k2.kd_kec\n" +
                "inner join kabupaten k3 on pasien.kd_kab = k3.kd_kab\n" +
                "where no_rkm_medis=?",Sequel.cariIsi("select pasien.no_rkm_medis from pasien inner join reg_periksa rp on pasien.no_rkm_medis = rp.no_rkm_medis where rp.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'")));
            param.put("nm_pasien",TPasien.getText());             
            param.put("tgl_lahir",Sequel.cariIsi("select DATE_FORMAT(pasien.tgl_lahir,'%d-%m-%Y') from pasien where pasien.no_rkm_medis=?",TNoRM.getText()));             
            param.put("no_rm",TNoRM.getText());
            param.put("ruang_rawat",Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                "where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            param.put("dpjp",Sequel.cariIsi("select dokter.nm_dokter from dpjp_ranap inner join dokter " +
                "on dpjp_ranap.kd_dokter=dokter.kd_dokter where dpjp_ranap.no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            param.put("ppjp",Sequel.cariIsi("select p.png_jawab from reg_periksa inner join penjab p on reg_periksa.kd_pj = p.kd_pj " +
                "where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            param.put("jkel",Sequel.cariIsi("select if(pasien.jk='L','Laki-Laki','Perempuan') from pasien where pasien.no_rkm_medis=?",TNoRM.getText()));
            param.put("norawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());  
            param.put("tanggal",Sequel.cariIsi("select DATE_FORMAT(csm.tanggal,'%d-%m-%Y %h:%m:%s') from checklist_skrining_mpp csm where no_rawat=?",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()));
            param.put("petugas",tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());  

            try {
                report_potensi="";
                ps=koneksi.prepareStatement(
                    "select m.nama_potensi from checklist_skrining_mpp_potensikomplain "+
                    "inner join master_potensi_komplain_tinggi m on checklist_skrining_mpp_potensikomplain.kode = m.kode_potensi "+
                    "where no_rawat=?");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        report_potensi=rs.getString("nama_potensi")+", "+report_potensi;
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            param.put("potensi",report_potensi+tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            
            try {
                report_kasus="";
                ps=koneksi.prepareStatement(
                    "select m.nama_penyakit from checklist_skrining_mpp_penyakitkronik c "+
                    "inner join master_kasus_penyakit_kronik m on c.kode = m.kode_penyakit "+
                    "where no_rawat=?");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        report_kasus=rs.getString("nama_penyakit")+", "+report_kasus;
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            param.put("kasus",report_kasus+tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());            

            try {
                report_igd="";
                ps=koneksi.prepareStatement(
                    "select m.nama_keterangan from checklist_skrining_mpp_tingkatmasukigd c "+
                    "inner join master_tingkat_masuk_igd m on c.kode = m.kode_keterangan "+
                    "where no_rawat=?");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        report_igd=rs.getString("nama_keterangan")+", "+report_igd;
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            param.put("igd",report_igd+tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            
            try {
                report_finansial="";
                ps=koneksi.prepareStatement(
                    "select m.nama_keterangan from checklist_skrining_mpp_finansial c "+
                    "inner join master_finansial m on c.kode = m.kode_keterangan "+
                    "where no_rawat=?");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        report_finansial=rs.getString("nama_keterangan")+", "+report_finansial;
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            param.put("finansial",report_finansial+tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            
            try {
                report_layanan="";
                ps=koneksi.prepareStatement(
                    "select m.nama_keterangan from checklist_skrining_mpp_layanan c "+
                    "inner join master_layanan m on c.kode = m.kode_keterangan "+
                    "where no_rawat=?");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        report_layanan=rs.getString("nama_keterangan")+", "+report_layanan;
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            param.put("layanan",report_layanan+tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());            
            
            Valid.MyReportqry("rptChecklistSkriningMPP.jasper","report","::[ Checklist Skrining MPP ]::",
                        "select c.usia,c.kognitif,c.resiko_bunuh_diri,c.status_fungsional, " +
                        "c.riwayat_alat_medis,c.riwayat_mental_sosial,c.penjab " +
                        "from checklist_skrining_mpp c " +
                        "where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }else{
            JOptionPane.showMessageDialog(null,"Maaf, silahkan pilih data terlebih dahulu..!!!!");
        }  
    }//GEN-LAST:event_BtnPrint1ActionPerformed

    private void KetWaktuTungguActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KetWaktuTungguActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetWaktuTungguActionPerformed

    private void KetWaktuTungguKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetWaktuTungguKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetWaktuTungguKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DetikKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MenitKeyPressed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamKeyPressed

    private void JamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JamActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamActionPerformed

    private void KeteranganFinansialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansialKeyPressed
        // TODO add your handling code here:
     
    }//GEN-LAST:event_KeteranganFinansialKeyPressed

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
        // Valid.pindah(evt,KetWaktuTunggu,Informasi);
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed

    }//GEN-LAST:event_KdPetugasKeyPressed

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{
            Valid.pindah(evt,TCari,BtnPetugas);
        }
    }//GEN-LAST:event_TNoRwKeyPressed

    private void KeteranganFinansial1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial1KeyPressed

    private void KeteranganFinansial2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial2KeyPressed

    private void KeteranganFinansial3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial3KeyPressed

    private void KeteranganFinansial4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial4KeyPressed

    private void KeteranganFinansial5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial5KeyPressed

    private void KeteranganFinansial6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial6KeyPressed

    private void KeteranganFinansial7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial7KeyPressed

    private void KeteranganFinansial8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial8KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial8KeyPressed

    private void KeteranganFinansial9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial9KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial9KeyPressed

    private void KeteranganFinansial11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial11KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial11KeyPressed

    private void KeteranganFinansial12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial12KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial12KeyPressed

    private void KeteranganFinansial13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial13KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial13KeyPressed

    private void KeteranganFinansial17KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial17KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial17KeyPressed

    private void KeteranganFinansial18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial18KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial18KeyPressed

    private void KeteranganFinansial19KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial19KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial19KeyPressed

    private void KeteranganFinansial20KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial20KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial20KeyPressed

    private void KeteranganFinansial21KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial21KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial21KeyPressed

    private void KeteranganFinansial22KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial22KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial22KeyPressed

    private void KeteranganFinansial23KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial23KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial23KeyPressed

    private void KeteranganFinansial24KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial24KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial24KeyPressed

    private void KeteranganFinansial25KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial25KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial25KeyPressed

    private void KeteranganFinansial27KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial27KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial27KeyPressed

    private void KeteranganFinansial28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial28KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial28KeyPressed

    private void KeteranganFinansial29KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial29KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial29KeyPressed

    private void KeteranganFinansial30KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial30KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial30KeyPressed

    private void KeteranganFinansial31KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial31KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial31KeyPressed

    private void KeteranganFinansial32KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial32KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial32KeyPressed

    private void KeteranganFinansial33KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial33KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial33KeyPressed

    private void KeteranganFinansial26KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial26KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial26KeyPressed

    private void KeteranganFinansial34KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial34KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial34KeyPressed

    private void KeteranganFinansial37KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial37KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial37KeyPressed

    private void KeteranganFinansial39KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial39KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial39KeyPressed

    private void KeteranganFinansial38KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial38KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial38KeyPressed

    private void KeteranganFinansial40KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial40KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial40KeyPressed

    private void KeteranganFinansial41KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial41KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial41KeyPressed

    private void Detik1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik1KeyPressed

    private void Jam1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1ActionPerformed

    private void Jam1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1KeyPressed

    private void Menit1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit1KeyPressed

    private void jenis_anestesiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jenis_anestesiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenis_anestesiActionPerformed

    private void jenis_anestesiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jenis_anestesiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenis_anestesiKeyPressed

    private void KeteranganFinansial10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial10KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial10KeyPressed

    private void KeteranganFinansial42KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial42KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial42KeyPressed

    private void KeteranganFinansial43KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial43KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial43KeyPressed

    private void KeteranganFinansial44KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial44KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial44KeyPressed

    private void jenis_anestesi1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jenis_anestesi1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenis_anestesi1ActionPerformed

    private void jenis_anestesi1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jenis_anestesi1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenis_anestesi1KeyPressed

    private void jenis_anestesi2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jenis_anestesi2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenis_anestesi2ActionPerformed

    private void jenis_anestesi2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jenis_anestesi2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenis_anestesi2KeyPressed

    private void KeteranganFinansial45KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial45KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial45KeyPressed

    private void KeteranganFinansial46KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial46KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial46KeyPressed

    private void KeteranganFinansial47KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial47KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial47KeyPressed

    private void KeteranganFinansial48KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial48KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial48KeyPressed

    private void KeteranganFinansial49KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial49KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial49KeyPressed

    private void KeteranganFinansial50KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganFinansial50KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeteranganFinansial50KeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMPenilaianAwalKeperawatanRalan dialog = new RMPenilaianAwalKeperawatanRalan(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.CekBox A1_R_T;
    private widget.CekBox A1_R_T1;
    private widget.CekBox A1_R_T10;
    private widget.CekBox A1_R_T11;
    private widget.CekBox A1_R_T12;
    private widget.CekBox A1_R_T13;
    private widget.CekBox A1_R_T14;
    private widget.CekBox A1_R_T15;
    private widget.CekBox A1_R_T16;
    private widget.CekBox A1_R_T17;
    private widget.CekBox A1_R_T18;
    private widget.CekBox A1_R_T19;
    private widget.CekBox A1_R_T2;
    private widget.CekBox A1_R_T20;
    private widget.CekBox A1_R_T21;
    private widget.CekBox A1_R_T22;
    private widget.CekBox A1_R_T23;
    private widget.CekBox A1_R_T24;
    private widget.CekBox A1_R_T25;
    private widget.CekBox A1_R_T26;
    private widget.CekBox A1_R_T27;
    private widget.CekBox A1_R_T28;
    private widget.CekBox A1_R_T29;
    private widget.CekBox A1_R_T3;
    private widget.CekBox A1_R_T30;
    private widget.CekBox A1_R_T31;
    private widget.CekBox A1_R_T32;
    private widget.CekBox A1_R_T33;
    private widget.CekBox A1_R_T34;
    private widget.CekBox A1_R_T35;
    private widget.CekBox A1_R_T36;
    private widget.CekBox A1_R_T37;
    private widget.CekBox A1_R_T38;
    private widget.CekBox A1_R_T39;
    private widget.CekBox A1_R_T4;
    private widget.CekBox A1_R_T40;
    private widget.CekBox A1_R_T41;
    private widget.CekBox A1_R_T42;
    private widget.CekBox A1_R_T43;
    private widget.CekBox A1_R_T44;
    private widget.CekBox A1_R_T45;
    private widget.CekBox A1_R_T46;
    private widget.CekBox A1_R_T47;
    private widget.CekBox A1_R_T48;
    private widget.CekBox A1_R_T49;
    private widget.CekBox A1_R_T5;
    private widget.CekBox A1_R_T50;
    private widget.CekBox A1_R_T51;
    private widget.CekBox A1_R_T52;
    private widget.CekBox A1_R_T53;
    private widget.CekBox A1_R_T54;
    private widget.CekBox A1_R_T55;
    private widget.CekBox A1_R_T56;
    private widget.CekBox A1_R_T57;
    private widget.CekBox A1_R_T58;
    private widget.CekBox A1_R_T59;
    private widget.CekBox A1_R_T6;
    private widget.CekBox A1_R_T60;
    private widget.CekBox A1_R_T61;
    private widget.CekBox A1_R_T62;
    private widget.CekBox A1_R_T63;
    private widget.CekBox A1_R_T64;
    private widget.CekBox A1_R_T65;
    private widget.CekBox A1_R_T66;
    private widget.CekBox A1_R_T67;
    private widget.CekBox A1_R_T68;
    private widget.CekBox A1_R_T69;
    private widget.CekBox A1_R_T7;
    private widget.CekBox A1_R_T70;
    private widget.CekBox A1_R_T71;
    private widget.CekBox A1_R_T72;
    private widget.CekBox A1_R_T73;
    private widget.CekBox A1_R_T74;
    private widget.CekBox A1_R_T75;
    private widget.CekBox A1_R_T76;
    private widget.CekBox A1_R_T77;
    private widget.CekBox A1_R_T78;
    private widget.CekBox A1_R_T79;
    private widget.CekBox A1_R_T8;
    private widget.CekBox A1_R_T80;
    private widget.CekBox A1_R_T81;
    private widget.CekBox A1_R_T82;
    private widget.CekBox A1_R_T83;
    private widget.CekBox A1_R_T9;
    private widget.CekBox A1_R_Y;
    private widget.CekBox A1_R_Y1;
    private widget.CekBox A1_R_Y10;
    private widget.CekBox A1_R_Y11;
    private widget.CekBox A1_R_Y12;
    private widget.CekBox A1_R_Y13;
    private widget.CekBox A1_R_Y14;
    private widget.CekBox A1_R_Y15;
    private widget.CekBox A1_R_Y16;
    private widget.CekBox A1_R_Y17;
    private widget.CekBox A1_R_Y18;
    private widget.CekBox A1_R_Y19;
    private widget.CekBox A1_R_Y2;
    private widget.CekBox A1_R_Y20;
    private widget.CekBox A1_R_Y21;
    private widget.CekBox A1_R_Y22;
    private widget.CekBox A1_R_Y23;
    private widget.CekBox A1_R_Y24;
    private widget.CekBox A1_R_Y25;
    private widget.CekBox A1_R_Y26;
    private widget.CekBox A1_R_Y27;
    private widget.CekBox A1_R_Y28;
    private widget.CekBox A1_R_Y29;
    private widget.CekBox A1_R_Y3;
    private widget.CekBox A1_R_Y30;
    private widget.CekBox A1_R_Y31;
    private widget.CekBox A1_R_Y32;
    private widget.CekBox A1_R_Y33;
    private widget.CekBox A1_R_Y34;
    private widget.CekBox A1_R_Y35;
    private widget.CekBox A1_R_Y36;
    private widget.CekBox A1_R_Y37;
    private widget.CekBox A1_R_Y38;
    private widget.CekBox A1_R_Y39;
    private widget.CekBox A1_R_Y4;
    private widget.CekBox A1_R_Y40;
    private widget.CekBox A1_R_Y41;
    private widget.CekBox A1_R_Y42;
    private widget.CekBox A1_R_Y43;
    private widget.CekBox A1_R_Y44;
    private widget.CekBox A1_R_Y45;
    private widget.CekBox A1_R_Y46;
    private widget.CekBox A1_R_Y47;
    private widget.CekBox A1_R_Y48;
    private widget.CekBox A1_R_Y49;
    private widget.CekBox A1_R_Y5;
    private widget.CekBox A1_R_Y50;
    private widget.CekBox A1_R_Y51;
    private widget.CekBox A1_R_Y52;
    private widget.CekBox A1_R_Y53;
    private widget.CekBox A1_R_Y54;
    private widget.CekBox A1_R_Y55;
    private widget.CekBox A1_R_Y56;
    private widget.CekBox A1_R_Y57;
    private widget.CekBox A1_R_Y58;
    private widget.CekBox A1_R_Y59;
    private widget.CekBox A1_R_Y6;
    private widget.CekBox A1_R_Y60;
    private widget.CekBox A1_R_Y61;
    private widget.CekBox A1_R_Y62;
    private widget.CekBox A1_R_Y63;
    private widget.CekBox A1_R_Y64;
    private widget.CekBox A1_R_Y65;
    private widget.CekBox A1_R_Y66;
    private widget.CekBox A1_R_Y67;
    private widget.CekBox A1_R_Y68;
    private widget.CekBox A1_R_Y69;
    private widget.CekBox A1_R_Y7;
    private widget.CekBox A1_R_Y70;
    private widget.CekBox A1_R_Y71;
    private widget.CekBox A1_R_Y72;
    private widget.CekBox A1_R_Y73;
    private widget.CekBox A1_R_Y74;
    private widget.CekBox A1_R_Y75;
    private widget.CekBox A1_R_Y76;
    private widget.CekBox A1_R_Y77;
    private widget.CekBox A1_R_Y78;
    private widget.CekBox A1_R_Y79;
    private widget.CekBox A1_R_Y8;
    private widget.CekBox A1_R_Y80;
    private widget.CekBox A1_R_Y81;
    private widget.CekBox A1_R_Y82;
    private widget.CekBox A1_R_Y83;
    private widget.CekBox A1_R_Y9;
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPetugas;
    private widget.Button BtnPrint;
    private widget.Button BtnPrint1;
    private widget.Button BtnSimpan;
    private widget.CekBox ChkAccor;
    private widget.CekBox ChkJam1;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox Detik;
    private widget.ComboBox Detik1;
    private widget.PanelBiasa FormInput;
    private widget.PanelBiasa FormMenu;
    private widget.PanelBiasa FormSkriningMPP;
    private widget.ComboBox Jam;
    private widget.ComboBox Jam1;
    private widget.TextBox Jk;
    private widget.TextBox KdPetugas;
    private widget.TextBox KeteranganFinansial;
    private widget.TextBox KeteranganFinansial1;
    private widget.TextBox KeteranganFinansial10;
    private widget.TextBox KeteranganFinansial11;
    private widget.TextBox KeteranganFinansial12;
    private widget.TextBox KeteranganFinansial13;
    private widget.TextBox KeteranganFinansial17;
    private widget.TextBox KeteranganFinansial18;
    private widget.TextBox KeteranganFinansial19;
    private widget.TextBox KeteranganFinansial2;
    private widget.TextBox KeteranganFinansial20;
    private widget.TextBox KeteranganFinansial21;
    private widget.TextBox KeteranganFinansial22;
    private widget.TextBox KeteranganFinansial23;
    private widget.TextBox KeteranganFinansial24;
    private widget.TextBox KeteranganFinansial25;
    private widget.TextBox KeteranganFinansial26;
    private widget.TextBox KeteranganFinansial27;
    private widget.TextBox KeteranganFinansial28;
    private widget.TextBox KeteranganFinansial29;
    private widget.TextBox KeteranganFinansial3;
    private widget.TextBox KeteranganFinansial30;
    private widget.TextBox KeteranganFinansial31;
    private widget.TextBox KeteranganFinansial32;
    private widget.TextBox KeteranganFinansial33;
    private widget.TextBox KeteranganFinansial34;
    private widget.TextBox KeteranganFinansial37;
    private widget.TextBox KeteranganFinansial38;
    private widget.TextBox KeteranganFinansial39;
    private widget.TextBox KeteranganFinansial4;
    private widget.TextBox KeteranganFinansial40;
    private widget.TextBox KeteranganFinansial41;
    private widget.TextBox KeteranganFinansial42;
    private widget.TextBox KeteranganFinansial43;
    private widget.TextBox KeteranganFinansial44;
    private widget.TextBox KeteranganFinansial45;
    private widget.TextBox KeteranganFinansial46;
    private widget.TextBox KeteranganFinansial47;
    private widget.TextBox KeteranganFinansial48;
    private widget.TextBox KeteranganFinansial49;
    private widget.TextBox KeteranganFinansial5;
    private widget.TextBox KeteranganFinansial50;
    private widget.TextBox KeteranganFinansial6;
    private widget.TextBox KeteranganFinansial7;
    private widget.TextBox KeteranganFinansial8;
    private widget.TextBox KeteranganFinansial9;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private widget.ComboBox Menit;
    private widget.ComboBox Menit1;
    private widget.TextBox NmPetugas;
    private widget.PanelBiasa PanelAccor;
    private widget.ScrollPane Scroll;
    private widget.ScrollPane Scroll13;
    private widget.ScrollPane Scroll14;
    private widget.ScrollPane Scroll15;
    private widget.ScrollPane Scroll7;
    private widget.ScrollPane Scroll9;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRM1;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.TextBox TPasien1;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal Tanggal;
    private widget.Tanggal Tanggal1;
    private widget.Tanggal Tanggal2;
    private widget.Tanggal Tanggal3;
    private widget.TextBox TglLahir;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel100;
    private widget.Label jLabel101;
    private widget.Label jLabel102;
    private widget.Label jLabel103;
    private widget.Label jLabel104;
    private widget.Label jLabel105;
    private widget.Label jLabel106;
    private widget.Label jLabel107;
    private widget.Label jLabel108;
    private widget.Label jLabel11;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel29;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel48;
    private widget.Label jLabel49;
    private widget.Label jLabel50;
    private widget.Label jLabel51;
    private widget.Label jLabel52;
    private widget.Label jLabel53;
    private widget.Label jLabel54;
    private widget.Label jLabel55;
    private widget.Label jLabel56;
    private widget.Label jLabel57;
    private widget.Label jLabel58;
    private widget.Label jLabel59;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel61;
    private widget.Label jLabel62;
    private widget.Label jLabel63;
    private widget.Label jLabel64;
    private widget.Label jLabel65;
    private widget.Label jLabel66;
    private widget.Label jLabel67;
    private widget.Label jLabel68;
    private widget.Label jLabel69;
    private widget.Label jLabel7;
    private widget.Label jLabel70;
    private widget.Label jLabel71;
    private widget.Label jLabel72;
    private widget.Label jLabel73;
    private widget.Label jLabel74;
    private widget.Label jLabel75;
    private widget.Label jLabel76;
    private widget.Label jLabel8;
    private widget.Label jLabel83;
    private widget.Label jLabel84;
    private widget.Label jLabel85;
    private widget.Label jLabel86;
    private widget.Label jLabel87;
    private widget.Label jLabel88;
    private widget.Label jLabel89;
    private widget.Label jLabel90;
    private widget.Label jLabel91;
    private widget.Label jLabel92;
    private widget.Label jLabel93;
    private widget.Label jLabel94;
    private widget.Label jLabel95;
    private widget.Label jLabel96;
    private widget.Label jLabel97;
    private widget.Label jLabel98;
    private widget.Label jLabel99;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JSeparator jSeparator1;
    private widget.ComboBox jenis_anestesi;
    private widget.ComboBox jenis_anestesi1;
    private widget.ComboBox jenis_anestesi2;
    private widget.Label label14;
    private widget.Label label199;
    private widget.Label label200;
    private widget.Label label201;
    private widget.Label label202;
    private widget.Label label203;
    private widget.Label label204;
    private widget.Label label205;
    private widget.Label label206;
    private widget.Label label207;
    private widget.Label label208;
    private widget.Label label209;
    private widget.Label label210;
    private widget.Label label211;
    private widget.Label label212;
    private widget.Label label213;
    private widget.Label label214;
    private widget.Label label215;
    private widget.Label label216;
    private widget.Label label217;
    private widget.Label label218;
    private widget.Label label219;
    private widget.Label label220;
    private widget.Label label221;
    private widget.Label label222;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.Table tbObat;
    private widget.Table tbTampilFinansial;
    private widget.Table tbTampilIGD;
    private widget.Table tbTampilKasus;
    private widget.Table tbTampilLayanan;
    private widget.Table tbTampilPotensi;
    // End of variables declaration//GEN-END:variables

    private void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().equals("")){
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,reg_periksa.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk," +
                        "DATE_FORMAT(p.tgl_lahir,'%d-%m-%Y') AS tgl_lahir,csm.tanggal,csm.usia,csm.kognitif,csm.resiko_bunuh_diri," +
                        "csm.potensi_lain,csm.kasus_lain,csm.status_fungsional,csm.riwayat_alat_medis,csm.riwayat_mental_sosial," +
                        "csm.tingkat_masuk_igd_lain,csm.keterangan_finansial,csm.keterangan_layanan,csm.penjab," +
                        "csm.nip,p2.nama " +
                        "from reg_periksa inner join checklist_skrining_mpp csm on reg_periksa.no_rawat = csm.no_rawat " +
                        "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join petugas p2 on csm.nip = p2.nip "+
                        "where csm.tanggal between ? and ? order by csm.tanggal,reg_periksa.no_rawat");
            }else{
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,reg_periksa.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk," +
                        "DATE_FORMAT(p.tgl_lahir,'%d-%m-%Y') AS tgl_lahir,csm.tanggal,csm.usia,csm.kognitif,csm.resiko_bunuh_diri," +
                        "csm.potensi_lain,csm.kasus_lain,csm.status_fungsional,csm.riwayat_alat_medis,csm.riwayat_mental_sosial," +
                        "csm.tingkat_masuk_igd_lain,csm.keterangan_finansial,csm.keterangan_layanan,csm.penjab," +
                        "csm.nip,p2.nama " +
                        "from reg_periksa inner join checklist_skrining_mpp csm on reg_periksa.no_rawat = csm.no_rawat " +
                        "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join petugas p2 on csm.nip = p2.nip "+
                        "where csm.tanggal between ? and ? and reg_periksa.no_rawat like ? or "+
                        "csm.tanggal between ? and ? and p.no_rkm_medis like ? or "+
                        "csm.tanggal between ? and ? and p.nm_pasien like ? or "+
                        "csm.tanggal between ? and ? and csm.nip like ? or "+
                        "csm.tanggal between ? and ? and p2.nama like ? order by csm.tanggal,reg_periksa.no_rawat");
            }
                
            try {
                if(TCari.getText().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(5,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(8,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(11,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(12,"%"+TCari.getText()+"%");
                    ps.setString(13,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(14,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(15,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("jk"),rs.getString("tgl_lahir"),
                        rs.getString("tanggal"),rs.getString("usia"),rs.getString("kognitif"),rs.getString("resiko_bunuh_diri"),
                        rs.getString("potensi_lain"),rs.getString("kasus_lain"),rs.getString("status_fungsional"),
                        rs.getString("riwayat_alat_medis"),rs.getString("riwayat_mental_sosial"),rs.getString("tingkat_masuk_igd_lain"),
                        rs.getString("keterangan_finansial"),rs.getString("keterangan_layanan"),
                        rs.getString("penjab"),rs.getString("nip"),rs.getString("nama")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
       
        for (i = 0; i < tabModePotensiKomplain.getRowCount(); i++) {
            tabModePotensiKomplain.setValueAt(false,i,0);
        }
        for (i = 0; i < tabModePenyakitKronik.getRowCount(); i++) {
            tabModePenyakitKronik.setValueAt(false,i,0);
        }
        for (i = 0; i < tabModeTingkatMasukIGD.getRowCount(); i++) {
            tabModeTingkatMasukIGD.setValueAt(false,i,0);
        }
        for (i = 0; i < tabModeFinansial.getRowCount(); i++) {
            tabModeFinansial.setValueAt(false,i,0);
        }
        for (i = 0; i < tabModeLayanan.getRowCount(); i++) {
            tabModeLayanan.setValueAt(false,i,0);
        }        
        TabRawat.setSelectedIndex(0);
      
        KeteranganFinansial.setText("");
       
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()); 
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString()); 
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            Valid.SetTgl2(Tanggal,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
           
            KeteranganFinansial.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
           
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            //FOR
            //potensi komplain tinggi
            try {
                Valid.tabelKosong(tabModePotensiKomplain);
                
                ps=koneksi.prepareStatement(
                        "select kode_potensi,nama_potensi from master_potensi_komplain_tinggi "+
                        "order by kode_potensi");
                try {
                    rs=ps.executeQuery();
                    while(rs.next()){
                        //CEK TRUE/FALSE
                        if(Sequel.cariIsi("select kode from checklist_skrining_mpp_potensikomplain where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and kode='"+rs.getString("kode_potensi")+"'").equals("")){
                            tabModePotensiKomplain.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                        }else{
                            tabModePotensiKomplain.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            //kasus penyakit kronik
            try {
                Valid.tabelKosong(tabModePenyakitKronik);
                
                ps=koneksi.prepareStatement(
                        "select kode_penyakit,nama_penyakit from master_kasus_penyakit_kronik "+
                        "order by kode_penyakit");
                try {
                    rs=ps.executeQuery();
                    while(rs.next()){
                        //CEK TRUE/FALSE
                        if(Sequel.cariIsi("select kode from checklist_skrining_mpp_penyakitkronik where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and kode='"+rs.getString("kode_penyakit")+"'").equals("")){
                            tabModePenyakitKronik.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                        }else{
                            tabModePenyakitKronik.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            //TINGKAT MASUK IGD
            try {
                Valid.tabelKosong(tabModeTingkatMasukIGD);
                
                ps=koneksi.prepareStatement(
                        "select kode_keterangan,nama_keterangan from master_tingkat_masuk_igd "+
                        "order by kode_keterangan");
                try {
                    rs=ps.executeQuery();
                    while(rs.next()){
                        //CEK TRUE/FALSE
                        if(Sequel.cariIsi("select kode from checklist_skrining_mpp_tingkatmasukigd where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and kode='"+rs.getString("kode_keterangan")+"'").equals("")){
                            tabModeTingkatMasukIGD.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                        }else{
                            tabModeTingkatMasukIGD.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                        }    
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            //FINANSIAL
            try {
                Valid.tabelKosong(tabModeFinansial);
                
                ps=koneksi.prepareStatement(
                        "select kode_keterangan,nama_keterangan from master_finansial "+
                        "order by kode_keterangan");
                try {
                    rs=ps.executeQuery();
                    while(rs.next()){
                        //CEK TRUE/FALSE
                        if(Sequel.cariIsi("select kode from checklist_skrining_mpp_finansial where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and kode='"+rs.getString("kode_keterangan")+"'").equals("")){
                            tabModeFinansial.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                        }else{
                            tabModeFinansial.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            //layanan
            try {
                Valid.tabelKosong(tabModeLayanan);
                
                ps=koneksi.prepareStatement(
                        "select kode_keterangan,nama_keterangan from master_layanan "+
                        "order by kode_keterangan");
                try {
                    rs=ps.executeQuery();
                    while(rs.next()){
                        //CEK TRUE/FALSE
                        if(Sequel.cariIsi("select kode from checklist_skrining_mpp_layanan where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and kode='"+rs.getString("kode_keterangan")+"'").equals("")){
                            tabModeLayanan.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                        }else{
                            tabModeLayanan.addRow(new Object[]{true,rs.getString(1),rs.getString(2)});
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }            
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,"+
                    "pasien.tgl_lahir,pasien.agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
                    "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    public void setNoRm(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);    
        isRawat();
    }
    public void setNoRm2(String NoRawat,String NoRkmMedis,String Nama,Date Tanggal){
        TNoRw.setText(NoRawat);
        TCari.setText(NoRawat);
        DTPCari2.setDate(Tanggal); 
        isRawat();
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnHapus.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        if(akses.getjml2()>=1){
            KdPetugas.setEditable(false);
            BtnPetugas.setEnabled(false);
            KdPetugas.setText(akses.getkode());
            Sequel.cariIsi("select nama from petugas where nip=?", NmPetugas,KdPetugas.getText());
            if(NmPetugas.getText().equals("")){
                KdPetugas.setText("");
                JOptionPane.showMessageDialog(null,"User login bukan petugas...!!");
            }
        }            
    }

    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }
    
    private void isMenu(){
        if(ChkAccor.isSelected()==true){
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(470,HEIGHT));
            FormMenu.setVisible(true);  
            FormSkriningMPP.setVisible(true);  
            ChkAccor.setVisible(true);
        }else if(ChkAccor.isSelected()==false){   
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(15,HEIGHT));
            FormMenu.setVisible(false);  
            FormSkriningMPP.setVisible(false);   
            ChkAccor.setVisible(true);
        }
    }

    private void getMasalah() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRM1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()); 
            //tampil potensi di panel
            try {
                Valid.tabelKosong(tabModeTampilPotensi);
                ps=koneksi.prepareStatement(
                        "select master_potensi_komplain_tinggi.kode_potensi,master_potensi_komplain_tinggi.nama_potensi from master_potensi_komplain_tinggi "+
                        "inner join checklist_skrining_mpp_potensikomplain c on c.kode=master_potensi_komplain_tinggi.kode_potensi "+
                        "where c.no_rawat=? order by kode_potensi");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeTampilPotensi.addRow(new Object[]{rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            //tampil kasus penyakit kronik di panel
            try {
                Valid.tabelKosong(tabModeTampilKasus);
                ps=koneksi.prepareStatement(
                        "select master_kasus_penyakit_kronik.kode_penyakit,master_kasus_penyakit_kronik.nama_penyakit from master_kasus_penyakit_kronik "+
                        "inner join checklist_skrining_mpp_penyakitkronik c on c.kode=master_kasus_penyakit_kronik.kode_penyakit "+
                        "where c.no_rawat=? order by kode_penyakit");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeTampilKasus.addRow(new Object[]{rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }            
            //tampil tingkat masuk igd di panel
            try {
                Valid.tabelKosong(tabModeTampilIGD);
                ps=koneksi.prepareStatement(
                        "select master_tingkat_masuk_igd.kode_keterangan,master_tingkat_masuk_igd.nama_keterangan from master_tingkat_masuk_igd "+
                        "inner join checklist_skrining_mpp_tingkatmasukigd c on c.kode=master_tingkat_masuk_igd.kode_keterangan "+
                        "where c.no_rawat=? order by kode_keterangan");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeTampilIGD.addRow(new Object[]{rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            //tampil finansial di panel
            try {
                Valid.tabelKosong(tabModeTampilFinansial);
                ps=koneksi.prepareStatement(
                        "select master_finansial.kode_keterangan,master_finansial.nama_keterangan from master_finansial "+
                        "inner join checklist_skrining_mpp_finansial c on c.kode=master_finansial.kode_keterangan "+
                        "where c.no_rawat=? order by kode_keterangan");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeTampilFinansial.addRow(new Object[]{rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
            //tampil layanan di panel
            try {
                Valid.tabelKosong(tabModeTampilLayanan);
                ps=koneksi.prepareStatement(
                        "select master_layanan.kode_keterangan,master_layanan.nama_keterangan from master_layanan "+
                        "inner join checklist_skrining_mpp_layanan c on c.kode=master_layanan.kode_keterangan "+
                        "where c.no_rawat=? order by kode_keterangan");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabModeTampilLayanan.addRow(new Object[]{rs.getString(1),rs.getString(2)});
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
        }
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from checklist_skrining_mpp where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            TNoRM1.setText("");
            TPasien1.setText("");
            Sequel.meghapus("checklist_skrining_mpp_potensikomplain","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            Sequel.meghapus("checklist_skrining_mpp_penyakitkronik","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            Sequel.meghapus("checklist_skrining_mpp_tingkatmasukigd","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            Sequel.meghapus("checklist_skrining_mpp_finansial","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());             
            Sequel.meghapus("checklist_skrining_mpp_layanan","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());             
            Valid.tabelKosong(tabModeTampilPotensi);
            Valid.tabelKosong(tabModeTampilKasus);
            Valid.tabelKosong(tabModeTampilIGD);
            Valid.tabelKosong(tabModeTampilFinansial);
            Valid.tabelKosong(tabModeTampilLayanan);
            ChkAccor.setSelected(false);
            isMenu();
            tampil();
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("checklist_skrining_mpp", "no_rawat=?", 
                "no_rawat=?,tanggal=?,usia=?,kognitif=?,resiko_bunuh_diri=?,status_fungsional=?,riwayat_alat_medis=?,riwayat_mental_sosial=?,penjab=?,potensi_lain=?,kasus_lain=?,tingkat_masuk_igd_lain=?,keterangan_finansial=?,keterangan_layanan=?,nip=?", 
                16, new String[]{
                    TNoRw.getText(),Valid.SetTgl(Tanggal.getSelectedItem()+"")+" "+Tanggal.getSelectedItem().toString().substring(11,19),
                  
                    KeteranganFinansial.getText(),KdPetugas.getText(),tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            //potensi komplain
                Sequel.meghapus("checklist_skrining_mpp_potensikomplain","no_rawat",tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                        
             
               
                JOptionPane.showMessageDialog(null,"Berhasil mengedit");
        }
    }
    
    private void tampilPotensi() {
        try{
            jml=0;
           
            pilih_potensi=null;
            pilih_potensi=new boolean[jml]; 
            kode_potensi=null;
            kode_potensi=new String[jml];
            potensi=null;
            potensi=new String[jml];

            index=0;        
           

            Valid.tabelKosong(tabModePotensiKomplain);

            for(i=0;i<jml;i++){
                tabModePotensiKomplain.addRow(new Object[] {
                    pilih_potensi[i],kode_potensi[i],potensi[i]
                });
            }
            ps=koneksi.prepareStatement("select * from master_potensi_komplain_tinggi order by kode_potensi");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModePotensiKomplain.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
    
    private void tampilPenyakitKronik() {
        try{
            jml=0;
          

            pilih_penyakit=null;
            pilih_penyakit=new boolean[jml]; 
            kode_penyakit=null;
            kode_penyakit=new String[jml];
            penyakit=null;
            penyakit=new String[jml];

            index=0;        
          

            Valid.tabelKosong(tabModePenyakitKronik);

            for(i=0;i<jml;i++){
                tabModePenyakitKronik.addRow(new Object[] {
                    pilih_penyakit[i],kode_penyakit[i],penyakit[i]
                });
            }
            ps=koneksi.prepareStatement("select * from master_kasus_penyakit_kronik order by kode_penyakit");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModePenyakitKronik.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
    
    private void tampilTingkatMasukIGD() {
        try{
            jml=0;
          

            pilih_tingkatigd=null;
            pilih_tingkatigd=new boolean[jml]; 
            kode_tingkatigd=null;
            kode_tingkatigd=new String[jml];
            tingkatigd=null;
            tingkatigd=new String[jml];

            index=0;        
           

            Valid.tabelKosong(tabModeTingkatMasukIGD);

            for(i=0;i<jml;i++){
                tabModeTingkatMasukIGD.addRow(new Object[] {
                    pilih_tingkatigd[i],kode_tingkatigd[i],tingkatigd[i]
                });
            }
            ps=koneksi.prepareStatement("select * from master_tingkat_masuk_igd order by kode_keterangan");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeTingkatMasukIGD.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
    
    private void tampilFinansial() {
        try{
            jml=0;
            

            pilih_finansial=null;
            pilih_finansial=new boolean[jml]; 
            kode_finansial=null;
            kode_finansial=new String[jml];
            finansial=null;
            finansial=new String[jml];

            index=0;        
           

            Valid.tabelKosong(tabModeFinansial);

            for(i=0;i<jml;i++){
                tabModeFinansial.addRow(new Object[] {
                    pilih_finansial[i],kode_finansial[i],finansial[i]
                });
            }
            ps=koneksi.prepareStatement("select * from master_finansial order by kode_keterangan");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeFinansial.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
    
    private void tampilLayanan() {
        try{
            jml=0;
           

            pilih_layanan=null;
            pilih_layanan=new boolean[jml]; 
            kode_layanan=null;
            kode_layanan=new String[jml];
            layanan=null;
            layanan=new String[jml];

            index=0;        
          

            Valid.tabelKosong(tabModeLayanan);

            for(i=0;i<jml;i++){
                tabModeLayanan.addRow(new Object[] {
                    pilih_layanan[i],kode_layanan[i],layanan[i]
                });
            }
            ps=koneksi.prepareStatement("select * from master_layanan order by kode_keterangan");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeLayanan.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }    

 private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam1.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkJam1.isSelected()==false){
                    nilai_jam =Jam.getSelectedIndex();
                    nilai_menit =Menit.getSelectedIndex();
                    nilai_detik =Detik.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam.setSelectedItem(jam);
                Menit.setSelectedItem(menit);
                Detik.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
}