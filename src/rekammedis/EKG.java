/*
 * Kontribusi dari Abdul Wahid, RSUD Cipayung Jakarta Timur
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;

/**
 *
 * @author perpustakaan
 */
public final class EKG extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private StringBuilder htmlContent;
    private String finger="";
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public EKG(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tgl.Lahir","J.K.","NIP Dokter","Nama Dokter","Tanggal",
            "Check Up","Chest Pain","Keluhan","Hipertensi","Pulmonary Disease","Arrhytmia","Obesitas",
            "Synus Rhytme","Synus Tachycardia","Synus Arrhytmia","Synus Bradycardia","Low Voltage",
            "AF/AFF","CVT(PAT)","VT/VF","RBBB","LBBB",
            "LVH","RVH","LAH","RAH","A-V Blok",
            "QRS Rate","P-P Rate","QRS Axis","P-R Interval","Q-t Interval",
            "SVES/VES","Delta Wave","Q Wave","r Premordial","ST depresed",
            "ST elevation","T Flat","Kesan","Anjuran","NIP Petugas",
            "Nama Petugas"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        
        tbObat.setModel(tabMode);
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 46; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(150);
            }else if(i==3){
                column.setPreferredWidth(65);
            }else if(i==4){
                column.setPreferredWidth(55);
            }else if(i==5){
                column.setPreferredWidth(80);
            }else if(i==6){
                column.setPreferredWidth(150);
            }else if(i==7){
                column.setPreferredWidth(115);
            }else if(i==8 || i==9 || i==10 || i==11 || i==12 || i==13 || i==14){
                column.setPreferredWidth(100);
            }else if(i==15 || i==16 || i==17 || i==18 || i==19 || i==20 || i==21){
                column.setPreferredWidth(100);
            }else if(i==21 || i==22 || i==23 || i==24 || i==25 || i==26 || i==27 || i==28 || i==29){
                column.setPreferredWidth(100);
            }else if(i==30 || i==31 || i==32 || i==33 || i==34 || i==35){
                column.setPreferredWidth(100);
            }else if(i==36 || i==37 || i==38 || i==39 || i==40 || i==41){
                column.setPreferredWidth(100);
            }else if(i==42 || i==43 || i==44){
                column.setPreferredWidth(100);
            }else if(i==45){
                column.setPreferredWidth(100);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        Keluhan.setDocument(new batasInput((int)250).getKata(Keluhan));
        QRSRate.setDocument(new batasInput((int)5).getKata(QRSRate));
        PPRate.setDocument(new batasInput((int)5).getKata(PPRate));
        QRSAxis.setDocument(new batasInput((int)5).getKata(QRSAxis));
        PRInterval.setDocument(new batasInput((int)5).getKata(PRInterval));
        QtInterval.setDocument(new batasInput((int)5).getKata(QtInterval));
        SVESVES.setDocument(new batasInput((int)5).getKata(SVESVES));
        DeltaWave.setDocument(new batasInput((int)250).getKata(DeltaWave));
        QWave.setDocument(new batasInput((int)250).getKata(QWave));
        rPremordial.setDocument(new batasInput((int)250).getKata(rPremordial));
        STdepresed.setDocument(new batasInput((int)250).getKata(STdepresed));
        STelevation.setDocument(new batasInput((int)250).getKata(STelevation));
        TFlat.setDocument(new batasInput((int)250).getKata(TFlat));
        Kesan.setDocument(new batasInput((int)500).getKata(Kesan));
        Anjuran.setDocument(new batasInput((int)500).getKata(Anjuran));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){
                    KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                    KdDokter.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });

        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                    KdPetugas.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnSuratKeterangan = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdDokter = new widget.TextBox();
        NmDokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        jLabel9 = new widget.Label();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        jLabel11 = new widget.Label();
        scrollPane5 = new widget.ScrollPane();
        Keluhan = new widget.TextArea();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator12 = new javax.swing.JSeparator();
        label11 = new widget.Label();
        TglPemeriksaan = new widget.Tanggal();
        scrollPane6 = new widget.ScrollPane();
        DeltaWave = new widget.TextArea();
        label15 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnPetugas = new widget.Button();
        CheckUp = new javax.swing.JCheckBox();
        ChestPain = new javax.swing.JCheckBox();
        Hipertensi = new javax.swing.JCheckBox();
        PulmonaryDisease = new javax.swing.JCheckBox();
        Arrhytmia = new javax.swing.JCheckBox();
        Obesitas = new javax.swing.JCheckBox();
        SynusRhytme = new javax.swing.JCheckBox();
        SynusTachycardia = new javax.swing.JCheckBox();
        SynusArrhytmia = new javax.swing.JCheckBox();
        SynusBradycardia = new javax.swing.JCheckBox();
        LowVoltage = new javax.swing.JCheckBox();
        AFAFF = new javax.swing.JCheckBox();
        CVTPAT = new javax.swing.JCheckBox();
        VTVF = new javax.swing.JCheckBox();
        RBBB = new javax.swing.JCheckBox();
        LBBB = new javax.swing.JCheckBox();
        LVH = new javax.swing.JCheckBox();
        RVH = new javax.swing.JCheckBox();
        LAH = new javax.swing.JCheckBox();
        RAH = new javax.swing.JCheckBox();
        AVBlok = new javax.swing.JCheckBox();
        jLabel12 = new widget.Label();
        jLabel13 = new widget.Label();
        jLabel14 = new widget.Label();
        jLabel15 = new widget.Label();
        jLabel16 = new widget.Label();
        jLabel17 = new widget.Label();
        QRSRate = new widget.TextBox();
        jLabel18 = new widget.Label();
        PPRate = new widget.TextBox();
        jLabel20 = new widget.Label();
        QRSAxis = new widget.TextBox();
        jLabel22 = new widget.Label();
        PRInterval = new widget.TextBox();
        jLabel23 = new widget.Label();
        QtInterval = new widget.TextBox();
        jLabel24 = new widget.Label();
        SVESVES = new widget.TextBox();
        jLabel25 = new widget.Label();
        jLabel26 = new widget.Label();
        jLabel27 = new widget.Label();
        scrollPane7 = new widget.ScrollPane();
        QWave = new widget.TextArea();
        jLabel28 = new widget.Label();
        scrollPane8 = new widget.ScrollPane();
        rPremordial = new widget.TextArea();
        jLabel29 = new widget.Label();
        scrollPane9 = new widget.ScrollPane();
        STdepresed = new widget.TextArea();
        jLabel30 = new widget.Label();
        scrollPane10 = new widget.ScrollPane();
        TFlat = new widget.TextArea();
        jLabel31 = new widget.Label();
        scrollPane11 = new widget.ScrollPane();
        STelevation = new widget.TextArea();
        jSeparator13 = new javax.swing.JSeparator();
        jLabel32 = new widget.Label();
        scrollPane12 = new widget.ScrollPane();
        Anjuran = new widget.TextArea();
        jLabel33 = new widget.Label();
        scrollPane13 = new widget.ScrollPane();
        Kesan = new widget.TextArea();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnSuratKeterangan.setBackground(new java.awt.Color(255, 255, 254));
        MnSuratKeterangan.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnSuratKeterangan.setForeground(new java.awt.Color(50, 50, 50));
        MnSuratKeterangan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnSuratKeterangan.setText("Hasil Pemeriksaan EKG");
        MnSuratKeterangan.setName("MnSuratKeterangan"); // NOI18N
        MnSuratKeterangan.setPreferredSize(new java.awt.Dimension(220, 26));
        MnSuratKeterangan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnSuratKeteranganActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnSuratKeterangan);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Pemeriksaan EKG ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 1000));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 70, 70, 23);

        KdDokter.setEditable(false);
        KdDokter.setName("KdDokter"); // NOI18N
        KdDokter.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokterKeyPressed(evt);
            }
        });
        FormInput.add(KdDokter);
        KdDokter.setBounds(74, 40, 90, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        NmDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmDokter);
        NmDokter.setBounds(166, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(348, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        jLabel9.setText("P-P Rate :");
        jLabel9.setName("jLabel9"); // NOI18N
        FormInput.add(jLabel9);
        jLabel9.setBounds(330, 290, 110, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        scrollPane5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane5.setName("scrollPane5"); // NOI18N

        Keluhan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Keluhan.setColumns(20);
        Keluhan.setRows(5);
        Keluhan.setName("Keluhan"); // NOI18N
        Keluhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeluhanKeyPressed(evt);
            }
        });
        scrollPane5.setViewportView(Keluhan);

        FormInput.add(scrollPane5);
        scrollPane5.setBounds(160, 180, 300, 60);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 100, 880, 1);

        jSeparator12.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator12.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator12.setName("jSeparator12"); // NOI18N
        FormInput.add(jSeparator12);
        jSeparator12.setBounds(0, 250, 880, 1);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(380, 40, 52, 23);

        TglPemeriksaan.setForeground(new java.awt.Color(50, 70, 50));
        TglPemeriksaan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "15-06-2023 07:45:57" }));
        TglPemeriksaan.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglPemeriksaan.setName("TglPemeriksaan"); // NOI18N
        TglPemeriksaan.setOpaque(false);
        TglPemeriksaan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglPemeriksaanKeyPressed(evt);
            }
        });
        FormInput.add(TglPemeriksaan);
        TglPemeriksaan.setBounds(436, 40, 130, 23);

        scrollPane6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane6.setName("scrollPane6"); // NOI18N

        DeltaWave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        DeltaWave.setColumns(20);
        DeltaWave.setRows(5);
        DeltaWave.setName("DeltaWave"); // NOI18N
        DeltaWave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DeltaWaveKeyPressed(evt);
            }
        });
        scrollPane6.setViewportView(DeltaWave);

        FormInput.add(scrollPane6);
        scrollPane6.setBounds(450, 440, 260, 60);

        label15.setText("Dokter :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label15);
        label15.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(74, 70, 90, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(166, 70, 180, 23);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas.setMnemonic('2');
        BtnPetugas.setToolTipText("Alt+2");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        FormInput.add(BtnPetugas);
        BtnPetugas.setBounds(348, 70, 28, 23);

        CheckUp.setFont(new java.awt.Font("Tahoma", 0, 11));
        CheckUp.setText("Check Up");
        CheckUp.setName("CheckUp"); // NOI18N
        CheckUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckUpActionPerformed(evt);
            }
        });
        FormInput.add(CheckUp);
        CheckUp.setBounds(70, 110, 81, 23);

        ChestPain.setFont(new java.awt.Font("Tahoma", 0, 11));
        ChestPain.setText("Chest Pain");
        ChestPain.setName("ChestPain"); // NOI18N
        ChestPain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChestPainActionPerformed(evt);
            }
        });
        FormInput.add(ChestPain);
        ChestPain.setBounds(70, 140, 81, 23);

        Hipertensi.setFont(new java.awt.Font("Tahoma", 0, 11));
        Hipertensi.setText("Hipertensi");
        Hipertensi.setName("Hipertensi"); // NOI18N
        Hipertensi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HipertensiActionPerformed(evt);
            }
        });
        FormInput.add(Hipertensi);
        Hipertensi.setBounds(250, 110, 140, 23);

        PulmonaryDisease.setFont(new java.awt.Font("Tahoma", 0, 11));
        PulmonaryDisease.setText("Pulmonary Disease");
        PulmonaryDisease.setName("PulmonaryDisease"); // NOI18N
        PulmonaryDisease.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PulmonaryDiseaseActionPerformed(evt);
            }
        });
        FormInput.add(PulmonaryDisease);
        PulmonaryDisease.setBounds(250, 140, 140, 23);

        Arrhytmia.setFont(new java.awt.Font("Tahoma", 0, 11));
        Arrhytmia.setText("Arrhytmia");
        Arrhytmia.setName("Arrhytmia"); // NOI18N
        Arrhytmia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ArrhytmiaActionPerformed(evt);
            }
        });
        FormInput.add(Arrhytmia);
        Arrhytmia.setBounds(450, 110, 80, 23);

        Obesitas.setFont(new java.awt.Font("Tahoma", 0, 11));
        Obesitas.setText("Obesitas");
        Obesitas.setName("Obesitas"); // NOI18N
        Obesitas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ObesitasActionPerformed(evt);
            }
        });
        FormInput.add(Obesitas);
        Obesitas.setBounds(450, 140, 80, 23);

        SynusRhytme.setFont(new java.awt.Font("Tahoma", 0, 11));
        SynusRhytme.setText("Synus Rhytme");
        SynusRhytme.setName("SynusRhytme"); // NOI18N
        SynusRhytme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SynusRhytmeActionPerformed(evt);
            }
        });
        FormInput.add(SynusRhytme);
        SynusRhytme.setBounds(70, 260, 120, 23);

        SynusTachycardia.setFont(new java.awt.Font("Tahoma", 0, 11));
        SynusTachycardia.setText("Synus Tachycardia");
        SynusTachycardia.setName("SynusTachycardia"); // NOI18N
        SynusTachycardia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SynusTachycardiaActionPerformed(evt);
            }
        });
        FormInput.add(SynusTachycardia);
        SynusTachycardia.setBounds(70, 290, 120, 23);

        SynusArrhytmia.setFont(new java.awt.Font("Tahoma", 0, 11));
        SynusArrhytmia.setText("Synus Arrhytmia");
        SynusArrhytmia.setName("SynusArrhytmia"); // NOI18N
        SynusArrhytmia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SynusArrhytmiaActionPerformed(evt);
            }
        });
        FormInput.add(SynusArrhytmia);
        SynusArrhytmia.setBounds(70, 320, 120, 23);

        SynusBradycardia.setFont(new java.awt.Font("Tahoma", 0, 11));
        SynusBradycardia.setText("Synus Bradycardia");
        SynusBradycardia.setName("SynusBradycardia"); // NOI18N
        SynusBradycardia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SynusBradycardiaActionPerformed(evt);
            }
        });
        FormInput.add(SynusBradycardia);
        SynusBradycardia.setBounds(70, 350, 120, 23);

        LowVoltage.setFont(new java.awt.Font("Tahoma", 0, 11));
        LowVoltage.setText("Low Voltage");
        LowVoltage.setName("LowVoltage"); // NOI18N
        LowVoltage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LowVoltageActionPerformed(evt);
            }
        });
        FormInput.add(LowVoltage);
        LowVoltage.setBounds(70, 380, 120, 23);

        AFAFF.setFont(new java.awt.Font("Tahoma", 0, 11));
        AFAFF.setText("AF/AFF");
        AFAFF.setName("AFAFF"); // NOI18N
        AFAFF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AFAFFActionPerformed(evt);
            }
        });
        FormInput.add(AFAFF);
        AFAFF.setBounds(70, 410, 120, 23);

        CVTPAT.setFont(new java.awt.Font("Tahoma", 0, 11));
        CVTPAT.setText("CVT (PAT)");
        CVTPAT.setName("CVTPAT"); // NOI18N
        CVTPAT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CVTPATActionPerformed(evt);
            }
        });
        FormInput.add(CVTPAT);
        CVTPAT.setBounds(70, 440, 120, 23);

        VTVF.setFont(new java.awt.Font("Tahoma", 0, 11));
        VTVF.setText("VT / VF");
        VTVF.setName("VTVF"); // NOI18N
        VTVF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VTVFActionPerformed(evt);
            }
        });
        FormInput.add(VTVF);
        VTVF.setBounds(70, 470, 120, 23);

        RBBB.setFont(new java.awt.Font("Tahoma", 0, 11));
        RBBB.setText("RBBB Complete / incomplete");
        RBBB.setName("RBBB"); // NOI18N
        RBBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RBBBActionPerformed(evt);
            }
        });
        FormInput.add(RBBB);
        RBBB.setBounds(70, 500, 170, 23);

        LBBB.setFont(new java.awt.Font("Tahoma", 0, 11));
        LBBB.setText("LBBB Complete / incomplete");
        LBBB.setName("LBBB"); // NOI18N
        LBBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LBBBActionPerformed(evt);
            }
        });
        FormInput.add(LBBB);
        LBBB.setBounds(70, 530, 170, 23);

        LVH.setFont(new java.awt.Font("Tahoma", 0, 11));
        LVH.setText("LVH");
        LVH.setName("LVH"); // NOI18N
        LVH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LVHActionPerformed(evt);
            }
        });
        FormInput.add(LVH);
        LVH.setBounds(70, 560, 120, 23);

        RVH.setFont(new java.awt.Font("Tahoma", 0, 11));
        RVH.setText("RVH");
        RVH.setName("RVH"); // NOI18N
        RVH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RVHActionPerformed(evt);
            }
        });
        FormInput.add(RVH);
        RVH.setBounds(70, 590, 120, 23);

        LAH.setFont(new java.awt.Font("Tahoma", 0, 11));
        LAH.setText("LAH");
        LAH.setName("LAH"); // NOI18N
        LAH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LAHActionPerformed(evt);
            }
        });
        FormInput.add(LAH);
        LAH.setBounds(70, 620, 120, 23);

        RAH.setFont(new java.awt.Font("Tahoma", 0, 11));
        RAH.setText("RAH");
        RAH.setName("RAH"); // NOI18N
        RAH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RAHActionPerformed(evt);
            }
        });
        FormInput.add(RAH);
        RAH.setBounds(70, 650, 120, 23);

        AVBlok.setFont(new java.awt.Font("Tahoma", 0, 11));
        AVBlok.setText("First / second / third degree A-V Blok");
        AVBlok.setName("AVBlok"); // NOI18N
        AVBlok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AVBlokActionPerformed(evt);
            }
        });
        FormInput.add(AVBlok);
        AVBlok.setBounds(70, 680, 210, 23);

        jLabel12.setText("Keluhan/ Gejala Lain :");
        jLabel12.setName("jLabel12"); // NOI18N
        FormInput.add(jLabel12);
        jLabel12.setBounds(40, 170, 110, 23);

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel13.setText("/Menit");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput.add(jLabel13);
        jLabel13.setBounds(590, 260, 40, 23);

        jLabel14.setText("QRS Rate :");
        jLabel14.setName("jLabel14"); // NOI18N
        FormInput.add(jLabel14);
        jLabel14.setBounds(330, 320, 110, 23);

        jLabel15.setText("P-R Interval :");
        jLabel15.setName("jLabel15"); // NOI18N
        FormInput.add(jLabel15);
        jLabel15.setBounds(330, 350, 110, 23);

        jLabel16.setText("Q-t Interval :");
        jLabel16.setName("jLabel16"); // NOI18N
        FormInput.add(jLabel16);
        jLabel16.setBounds(330, 380, 110, 23);

        jLabel17.setText("SVES/VES :");
        jLabel17.setName("jLabel17"); // NOI18N
        FormInput.add(jLabel17);
        jLabel17.setBounds(330, 410, 110, 23);

        QRSRate.setFocusTraversalPolicyProvider(true);
        QRSRate.setName("QRSRate"); // NOI18N
        QRSRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                QRSRateActionPerformed(evt);
            }
        });
        QRSRate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                QRSRateKeyPressed(evt);
            }
        });
        FormInput.add(QRSRate);
        QRSRate.setBounds(450, 260, 130, 23);

        jLabel18.setText("QRS Rate :");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(330, 260, 110, 23);

        PPRate.setFocusTraversalPolicyProvider(true);
        PPRate.setName("PPRate"); // NOI18N
        PPRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PPRateActionPerformed(evt);
            }
        });
        PPRate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PPRateKeyPressed(evt);
            }
        });
        FormInput.add(PPRate);
        PPRate.setBounds(450, 290, 130, 23);

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setText("/Menit");
        jLabel20.setName("jLabel20"); // NOI18N
        FormInput.add(jLabel20);
        jLabel20.setBounds(590, 290, 40, 23);

        QRSAxis.setFocusTraversalPolicyProvider(true);
        QRSAxis.setName("QRSAxis"); // NOI18N
        QRSAxis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                QRSAxisActionPerformed(evt);
            }
        });
        QRSAxis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                QRSAxisKeyPressed(evt);
            }
        });
        FormInput.add(QRSAxis);
        QRSAxis.setBounds(450, 320, 130, 23);

        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel22.setText("/derajat");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(590, 320, 40, 23);

        PRInterval.setFocusTraversalPolicyProvider(true);
        PRInterval.setName("PRInterval"); // NOI18N
        PRInterval.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PRIntervalActionPerformed(evt);
            }
        });
        PRInterval.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PRIntervalKeyPressed(evt);
            }
        });
        FormInput.add(PRInterval);
        PRInterval.setBounds(450, 350, 130, 23);

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel23.setText("/Detik");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(590, 350, 40, 23);

        QtInterval.setFocusTraversalPolicyProvider(true);
        QtInterval.setName("QtInterval"); // NOI18N
        QtInterval.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                QtIntervalActionPerformed(evt);
            }
        });
        QtInterval.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                QtIntervalKeyPressed(evt);
            }
        });
        FormInput.add(QtInterval);
        QtInterval.setBounds(450, 380, 130, 23);

        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel24.setText("/Detik");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(590, 380, 40, 23);

        SVESVES.setFocusTraversalPolicyProvider(true);
        SVESVES.setName("SVESVES"); // NOI18N
        SVESVES.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SVESVESActionPerformed(evt);
            }
        });
        SVESVES.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SVESVESKeyPressed(evt);
            }
        });
        FormInput.add(SVESVES);
        SVESVES.setBounds(450, 410, 130, 23);

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel25.setText("/Detik");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(590, 410, 40, 23);

        jLabel26.setText("Delta Wave / U wave di lead");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(300, 440, 140, 23);

        jLabel27.setText("Q Wave di lead");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(300, 510, 140, 23);

        scrollPane7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane7.setName("scrollPane7"); // NOI18N

        QWave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        QWave.setColumns(20);
        QWave.setRows(5);
        QWave.setName("QWave"); // NOI18N
        QWave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                QWaveKeyPressed(evt);
            }
        });
        scrollPane7.setViewportView(QWave);

        FormInput.add(scrollPane7);
        scrollPane7.setBounds(450, 510, 260, 60);

        jLabel28.setText("r Premordial di lead");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(300, 580, 140, 23);

        scrollPane8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane8.setName("scrollPane8"); // NOI18N

        rPremordial.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        rPremordial.setColumns(20);
        rPremordial.setRows(5);
        rPremordial.setName("rPremordial"); // NOI18N
        rPremordial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rPremordialKeyPressed(evt);
            }
        });
        scrollPane8.setViewportView(rPremordial);

        FormInput.add(scrollPane8);
        scrollPane8.setBounds(450, 580, 260, 60);

        jLabel29.setText("ST depresed di lead");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(300, 650, 140, 23);

        scrollPane9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane9.setName("scrollPane9"); // NOI18N

        STdepresed.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        STdepresed.setColumns(20);
        STdepresed.setRows(5);
        STdepresed.setName("STdepresed"); // NOI18N
        STdepresed.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                STdepresedKeyPressed(evt);
            }
        });
        scrollPane9.setViewportView(STdepresed);

        FormInput.add(scrollPane9);
        scrollPane9.setBounds(450, 650, 260, 60);

        jLabel30.setText("ST elevation di lead");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(300, 720, 140, 23);

        scrollPane10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane10.setName("scrollPane10"); // NOI18N

        TFlat.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        TFlat.setColumns(20);
        TFlat.setRows(5);
        TFlat.setName("TFlat"); // NOI18N
        TFlat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TFlatKeyPressed(evt);
            }
        });
        scrollPane10.setViewportView(TFlat);

        FormInput.add(scrollPane10);
        scrollPane10.setBounds(450, 790, 260, 60);

        jLabel31.setText("Anjuran");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(380, 880, 50, 23);

        scrollPane11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane11.setName("scrollPane11"); // NOI18N

        STelevation.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        STelevation.setColumns(20);
        STelevation.setRows(5);
        STelevation.setName("STelevation"); // NOI18N
        STelevation.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                STelevationKeyPressed(evt);
            }
        });
        scrollPane11.setViewportView(STelevation);

        FormInput.add(scrollPane11);
        scrollPane11.setBounds(450, 720, 260, 60);

        jSeparator13.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator13.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator13.setName("jSeparator13"); // NOI18N
        FormInput.add(jSeparator13);
        jSeparator13.setBounds(0, 860, 880, 1);

        jLabel32.setText("T Flat/T inverted di lead");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(300, 790, 140, 23);

        scrollPane12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane12.setName("scrollPane12"); // NOI18N

        Anjuran.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Anjuran.setColumns(20);
        Anjuran.setRows(5);
        Anjuran.setName("Anjuran"); // NOI18N
        Anjuran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AnjuranKeyPressed(evt);
            }
        });
        scrollPane12.setViewportView(Anjuran);

        FormInput.add(scrollPane12);
        scrollPane12.setBounds(450, 880, 260, 60);

        jLabel33.setText("Kesan");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(40, 880, 30, 23);

        scrollPane13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane13.setName("scrollPane13"); // NOI18N

        Kesan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Kesan.setColumns(20);
        Kesan.setRows(5);
        Kesan.setName("Kesan"); // NOI18N
        Kesan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KesanKeyPressed(evt);
            }
        });
        scrollPane13.setViewportView(Kesan);

        FormInput.add(scrollPane13);
        scrollPane13.setBounds(80, 880, 260, 60);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Pemeriksaan", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "15-06-2023" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "15-06-2023" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Pemeriksaan", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{            
            Valid.pindah(evt,TCari,BtnDokter);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnPetugas,"Petugas");
        }else{
            if(Sequel.menyimpantf("penilaian_ekg","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",40,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),KdPetugas.getText(),String.valueOf(CheckUp.isSelected()),String.valueOf(ChestPain.isSelected()),
                Keluhan.getText(),String.valueOf(Hipertensi.isSelected()),String.valueOf(PulmonaryDisease.isSelected()),
                String.valueOf(Arrhytmia.isSelected()),String.valueOf(Obesitas.isSelected()),String.valueOf(SynusRhytme.isSelected()),
                String.valueOf(SynusTachycardia.isSelected()),String.valueOf(SynusArrhytmia.isSelected()),String.valueOf(SynusBradycardia.isSelected()),
                String.valueOf(LowVoltage.isSelected()),String.valueOf(AFAFF.isSelected()),String.valueOf(CVTPAT.isSelected()),String.valueOf(VTVF.isSelected()),
                String.valueOf(RBBB.isSelected()),String.valueOf(LBBB.isSelected()),String.valueOf(LVH.isSelected()),String.valueOf(RVH.isSelected()),
                String.valueOf(LAH.isSelected()),String.valueOf(RAH.isSelected()),String.valueOf(AVBlok.isSelected()),
                QRSRate.getText(),PPRate.getText(),QRSAxis.getText(),PRInterval.getText(),QtInterval.getText(),SVESVES.getText(),
                DeltaWave.getText(),QWave.getText(),rPremordial.getText(),STdepresed.getText(),STelevation.getText(),TFlat.getText(),
                Kesan.getText(),Anjuran.getText()
                })==true){
                    JOptionPane.showMessageDialog(rootPane,"Data berhasil disimpan!");
                    emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,Keluhan,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()) || akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh dokter/petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }              
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnPetugas,"Petugas");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()) || akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString())){
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh dokter/petugas yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
//        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        if(tabMode.getRowCount()==0){
//            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//            BtnBatal.requestFocus();
//        }else if(tabMode.getRowCount()!=0){
//            try{
//                if(TCari.getText().trim().equals("")){
//                    ps=koneksi.prepareStatement(
//                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_medis_ralan.tanggal,"+
//                        "penilaian_medis_ralan.kd_dokter,penilaian_medis_ralan.anamnesis,penilaian_medis_ralan.hubungan,penilaian_medis_ralan.keluhan_utama,penilaian_medis_ralan.rps,penilaian_medis_ralan.rpk,penilaian_medis_ralan.rpd,penilaian_medis_ralan.rpo,penilaian_medis_ralan.alergi,"+
//                        "penilaian_medis_ralan.keadaan,penilaian_medis_ralan.gcs,penilaian_medis_ralan.kesadaran,penilaian_medis_ralan.td,penilaian_medis_ralan.nadi,penilaian_medis_ralan.rr,penilaian_medis_ralan.suhu,penilaian_medis_ralan.spo,penilaian_medis_ralan.bb,penilaian_medis_ralan.tb,"+
//                        "penilaian_medis_ralan.kepala,penilaian_medis_ralan.gigi,penilaian_medis_ralan.tht,penilaian_medis_ralan.thoraks,penilaian_medis_ralan.abdomen,penilaian_medis_ralan.ekstremitas,penilaian_medis_ralan.genital,penilaian_medis_ralan.kulit,"+
//                        "penilaian_medis_ralan.ket_fisik,penilaian_medis_ralan.ket_lokalis,penilaian_medis_ralan.penunjang,penilaian_medis_ralan.diagnosis,penilaian_medis_ralan.tata,penilaian_medis_ralan.konsulrujuk,dokter.nm_dokter "+
//                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                        "inner join penilaian_medis_ralan on reg_periksa.no_rawat=penilaian_medis_ralan.no_rawat "+
//                        "inner join dokter on penilaian_medis_ralan.kd_dokter=dokter.kd_dokter where "+
//                        "penilaian_medis_ralan.tanggal between ? and ? order by penilaian_medis_ralan.tanggal");
//                }else{
//                    ps=koneksi.prepareStatement(
//                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_medis_ralan.tanggal,"+
//                        "penilaian_medis_ralan.kd_dokter,penilaian_medis_ralan.anamnesis,penilaian_medis_ralan.hubungan,penilaian_medis_ralan.keluhan_utama,penilaian_medis_ralan.rps,penilaian_medis_ralan.rpk,penilaian_medis_ralan.rpd,penilaian_medis_ralan.rpo,penilaian_medis_ralan.alergi,"+
//                        "penilaian_medis_ralan.keadaan,penilaian_medis_ralan.gcs,penilaian_medis_ralan.kesadaran,penilaian_medis_ralan.td,penilaian_medis_ralan.nadi,penilaian_medis_ralan.rr,penilaian_medis_ralan.suhu,penilaian_medis_ralan.spo,penilaian_medis_ralan.bb,penilaian_medis_ralan.tb,"+
//                        "penilaian_medis_ralan.kepala,penilaian_medis_ralan.gigi,penilaian_medis_ralan.tht,penilaian_medis_ralan.thoraks,penilaian_medis_ralan.abdomen,penilaian_medis_ralan.ekstremitas,penilaian_medis_ralan.genital,penilaian_medis_ralan.kulit,"+
//                        "penilaian_medis_ralan.ket_fisik,penilaian_medis_ralan.ket_lokalis,penilaian_medis_ralan.penunjang,penilaian_medis_ralan.diagnosis,penilaian_medis_ralan.tata,penilaian_medis_ralan.konsulrujuk,dokter.nm_dokter "+
//                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                        "inner join penilaian_medis_ralan on reg_periksa.no_rawat=penilaian_medis_ralan.no_rawat "+
//                        "inner join dokter on penilaian_medis_ralan.kd_dokter=dokter.kd_dokter where "+
//                        "penilaian_medis_ralan.tanggal between ? and ? and (reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
//                        "penilaian_medis_ralan.kd_dokter like ? or dokter.nm_dokter like ?) order by penilaian_medis_ralan.tanggal");
//                }
//
//                try {
//                    if(TCari.getText().trim().equals("")){
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                    }else{
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(3,"%"+TCari.getText()+"%");
//                        ps.setString(4,"%"+TCari.getText()+"%");
//                        ps.setString(5,"%"+TCari.getText()+"%");
//                        ps.setString(6,"%"+TCari.getText()+"%");
//                        ps.setString(7,"%"+TCari.getText()+"%");
//                    }  
//                    rs=ps.executeQuery();
//                    htmlContent = new StringBuilder();
//                    htmlContent.append(                             
//                        "<tr class='isi'>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='105px'><b>No.Rawat</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='70px'><b>No.RM</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Nama Pasien</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='65px'><b>Tgl.Lahir</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='55px'><b>J.K.</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>NIP</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Nama Dokter</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='115px'><b>Tanggal</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Anamnesis</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='100px'><b>Hubungan</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Keluhan Utama</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Sekarang</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Dahulu</b></td>"+
//			    "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Keluarga</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penggunaan Obat</b></td>"+   //CUSTOM MUHSIN PENGGUNAKAN JADI PENGGUNAAN
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='120px'><b>Riwayat Alergi</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='90px'><b>Keadaan Umum</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='50px'><b>GCS</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kesadaran</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='60px'><b>TD(mmHg)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='75px'><b>Nadi(x/menit)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='67px'><b>RR(x/menit)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>Suhu</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>SpO2</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>BB(Kg)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>TB(cm)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kepala</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Gigi & Mulut</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>THT</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Thoraks</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Abdomen</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Genital & Anus</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Ekstremitas</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kulit</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Ket.Pemeriksaan Fisik</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='200px'><b>Ket.Status Lokalis</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='170px'><b>Pemeriksaa Penunjang</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Diagnosis/Asesmen</b></td>"+
//			    "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Tatalaksana</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Konsul/Rujuk</b></td>"+
//                        "</tr>"
//                    );
//                    while(rs.next()){
//                        htmlContent.append(
//                            "<tr class='isi'>"+
//                               "<td valign='top'>"+rs.getString("no_rawat")+"</td>"+
//                               "<td valign='top'>"+rs.getString("no_rkm_medis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nm_pasien")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tgl_lahir")+"</td>"+
//                               "<td valign='top'>"+rs.getString("jk")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kd_dokter")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nm_dokter")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tanggal")+"</td>"+
//                               "<td valign='top'>"+rs.getString("anamnesis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("hubungan")+"</td>"+
//                               "<td valign='top'>"+rs.getString("keluhan_utama")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rps")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpd")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpk")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpo")+"</td>"+
//                               "<td valign='top'>"+rs.getString("alergi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("keadaan")+"</td>"+
//                               "<td valign='top'>"+rs.getString("gcs")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kesadaran")+"</td>"+
//                               "<td valign='top'>"+rs.getString("td")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nadi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rr")+"</td>"+
//                               "<td valign='top'>"+rs.getString("suhu")+"</td>"+
//                               "<td valign='top'>"+rs.getString("spo")+"</td>"+
//                               "<td valign='top'>"+rs.getString("bb")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tb")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kepala")+"</td>"+
//                               "<td valign='top'>"+rs.getString("gigi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tht")+"</td>"+
//                               "<td valign='top'>"+rs.getString("thoraks")+"</td>"+
//                               "<td valign='top'>"+rs.getString("abdomen")+"</td>"+
//                               "<td valign='top'>"+rs.getString("genital")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ekstremitas")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kulit")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ket_fisik")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ket_lokalis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("penunjang")+"</td>"+
//                               "<td valign='top'>"+rs.getString("diagnosis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tata")+"</td>"+
//                               "<td valign='top'>"+rs.getString("konsulrujuk")+"</td>"+
//                            "</tr>");
//                    }
//                    LoadHTML.setText(
//                        "<html>"+
//                          "<table width='4400px' border='0' align='center' cellpadding='1px' cellspacing='0' class='tbl_form'>"+
//                           htmlContent.toString()+
//                          "</table>"+
//                        "</html>"
//                    );
//
//                    File g = new File("file2.css");            
//                    BufferedWriter bg = new BufferedWriter(new FileWriter(g));
//                    bg.write(
//                        ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
//                        ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
//                        ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
//                        ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
//                        ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
//                        ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
//                    );
//                    bg.close();
//
//                    File f = new File("DataPenilaianAwalMedisRalan.html");            
//                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));            
//                    bw.write(LoadHTML.getText().replaceAll("<head>","<head>"+
//                                "<link href=\"file2.css\" rel=\"stylesheet\" type=\"text/css\" />"+
//                                "<table width='4400px' border='0' align='center' cellpadding='3px' cellspacing='0' class='tbl_form'>"+
//                                    "<tr class='isi2'>"+
//                                        "<td valign='top' align='center'>"+
//                                            "<font size='4' face='Tahoma'>"+akses.getnamars()+"</font><br>"+
//                                            akses.getalamatrs()+", "+akses.getkabupatenrs()+", "+akses.getpropinsirs()+"<br>"+
//                                            akses.getkontakrs()+", E-mail : "+akses.getemailrs()+"<br><br>"+
//                                            "<font size='2' face='Tahoma'>DATA PENILAIAN AWAL MEDIS RAWAT JALAN<br><br></font>"+        
//                                        "</td>"+
//                                   "</tr>"+
//                                "</table>")
//                    );
//                    bw.close();                         
//                    Desktop.getDesktop().browse(f.toURI());
//                } catch (Exception e) {
//                    System.out.println("Notif : "+e);
//                } finally{
//                    if(rs!=null){
//                        rs.close();
//                    }
//                    if(ps!=null){
//                        ps.close();
//                    }
//                }
//
//            }catch(Exception e){
//                System.out.println("Notifikasi : "+e);
//            }
//        }
//        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void KdDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokterKeyPressed
        
    }//GEN-LAST:event_KdDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setAlwaysOnTop(false);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        //Valid.pindah(evt,Monitoring,BtnSimpan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void KeluhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeluhanKeyPressed
//        Valid.pindah2(evt,Obesitas,Obesitas);
    }//GEN-LAST:event_KeluhanKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void TglPemeriksaanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglPemeriksaanKeyPressed
        Valid.pindah(evt,BtnDokter,BtnPetugas);
    }//GEN-LAST:event_TglPemeriksaanKeyPressed

    private void MnSuratKeteranganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnSuratKeteranganActionPerformed
        if(tbObat.getSelectedRow()>-1){
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());          
            param.put("logo",Sequel.cariGambar("select logo from setting")); 
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),5).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString())); 
            param.put("alamat",Sequel.cariIsi("select concat(pasien.alamat,', ',k.nm_kel,', ',k2.nm_kec,', ',k3.nm_kab) as alamat\n" +
                                "from pasien\n" +
                                "inner join kelurahan k on pasien.kd_kel = k.kd_kel\n" +
                                "inner join kecamatan k2 on pasien.kd_kec = k2.kd_kec\n" +
                                "inner join kabupaten k3 on pasien.kd_kab = k3.kd_kab\n" +
                                "where no_rkm_medis=?",tbObat.getValueAt(tbObat.getSelectedRow(),1).toString()));            
            
            Valid.MyReportqry("rptCetakPenilaianEKG.jasper","report","::[ Hasil Pemeriksaan EKG ]::",
                "select pe.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk," +
                "date_format(p.tgl_lahir,'%d-%m-%Y') as tgl_lahir,date_format(pe.tanggal,'%d-%m-%Y') as tanggal," +
                "pe.checkup,pe.chestpain,pe.keluhan,pe.hipertensi,pe.pulmonarydisease,pe.arrhytmia,pe.obesitas," +
                "pe.synusrhytme,pe.synustachycardia,pe.synusarrhytmia,pe.synusbradycardia," +
                "pe.lowvoltage,pe.afaff,pe.cvtpat,pe.vtvf,pe.rbbb,pe.lbbb,pe.lvh,pe.rvh,pe.lah,pe.rah,pe.avblok," +
                "pe.qrsrate,pe.pprate,pe.qrsaxis,pe.printerval,pe.qtinterval,pe.svesves," +
                "pe.deltawave,pe.qwave,pe.rpremordial,pe.stdepresed,pe.stelevation,pe.tflat," +
                "pe.kesan,pe.anjuran,pe.kd_dokter,d.nm_dokter,pe.kd_petugas,p2.nama,reg_periksa.umurdaftar,reg_periksa.sttsumur " +
                "from reg_periksa inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                "inner join penilaian_ekg pe on reg_periksa.no_rawat = pe.no_rawat " +
                "inner join dokter d on pe.kd_dokter = d.kd_dokter " +
                "inner join petugas p2 on pe.kd_petugas = p2.nip "+
                "where pe.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
            this.setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_MnSuratKeteranganActionPerformed

    private void DeltaWaveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DeltaWaveKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DeltaWaveKeyPressed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdPetugasKeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed
        // TODO add your handling code here:
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);        
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void CheckUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckUpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CheckUpActionPerformed

    private void ChestPainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChestPainActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ChestPainActionPerformed

    private void HipertensiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HipertensiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_HipertensiActionPerformed

    private void PulmonaryDiseaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PulmonaryDiseaseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PulmonaryDiseaseActionPerformed

    private void ArrhytmiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ArrhytmiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ArrhytmiaActionPerformed

    private void ObesitasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ObesitasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObesitasActionPerformed

    private void SynusRhytmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SynusRhytmeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SynusRhytmeActionPerformed

    private void SynusTachycardiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SynusTachycardiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SynusTachycardiaActionPerformed

    private void SynusArrhytmiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SynusArrhytmiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SynusArrhytmiaActionPerformed

    private void SynusBradycardiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SynusBradycardiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SynusBradycardiaActionPerformed

    private void LowVoltageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LowVoltageActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_LowVoltageActionPerformed

    private void AFAFFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AFAFFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AFAFFActionPerformed

    private void CVTPATActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CVTPATActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CVTPATActionPerformed

    private void VTVFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VTVFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_VTVFActionPerformed

    private void RBBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RBBBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RBBBActionPerformed

    private void LBBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LBBBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_LBBBActionPerformed

    private void LVHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LVHActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_LVHActionPerformed

    private void RVHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RVHActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RVHActionPerformed

    private void LAHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LAHActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_LAHActionPerformed

    private void RAHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RAHActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RAHActionPerformed

    private void AVBlokActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AVBlokActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AVBlokActionPerformed

    private void QRSRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_QRSRateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_QRSRateActionPerformed

    private void QRSRateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_QRSRateKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_QRSRateKeyPressed

    private void PPRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PPRateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PPRateActionPerformed

    private void PPRateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PPRateKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PPRateKeyPressed

    private void QRSAxisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_QRSAxisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_QRSAxisActionPerformed

    private void QRSAxisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_QRSAxisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_QRSAxisKeyPressed

    private void PRIntervalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PRIntervalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PRIntervalActionPerformed

    private void PRIntervalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PRIntervalKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PRIntervalKeyPressed

    private void QtIntervalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_QtIntervalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_QtIntervalActionPerformed

    private void QtIntervalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_QtIntervalKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_QtIntervalKeyPressed

    private void SVESVESActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SVESVESActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SVESVESActionPerformed

    private void SVESVESKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SVESVESKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_SVESVESKeyPressed

    private void QWaveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_QWaveKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_QWaveKeyPressed

    private void rPremordialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rPremordialKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_rPremordialKeyPressed

    private void STdepresedKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_STdepresedKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_STdepresedKeyPressed

    private void TFlatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TFlatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TFlatKeyPressed

    private void STelevationKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_STelevationKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_STelevationKeyPressed

    private void AnjuranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AnjuranKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AnjuranKeyPressed

    private void KesanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KesanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KesanKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            EKG dialog = new EKG(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox AFAFF;
    private javax.swing.JCheckBox AVBlok;
    private widget.TextArea Anjuran;
    private javax.swing.JCheckBox Arrhytmia;
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPetugas;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private javax.swing.JCheckBox CVTPAT;
    private javax.swing.JCheckBox CheckUp;
    private javax.swing.JCheckBox ChestPain;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.TextArea DeltaWave;
    private widget.PanelBiasa FormInput;
    private javax.swing.JCheckBox Hipertensi;
    private widget.TextBox Jk;
    private widget.TextBox KdDokter;
    private widget.TextBox KdPetugas;
    private widget.TextArea Keluhan;
    private widget.TextArea Kesan;
    private javax.swing.JCheckBox LAH;
    private javax.swing.JCheckBox LBBB;
    private widget.Label LCount;
    private javax.swing.JCheckBox LVH;
    private widget.editorpane LoadHTML;
    private javax.swing.JCheckBox LowVoltage;
    private javax.swing.JMenuItem MnSuratKeterangan;
    private widget.TextBox NmDokter;
    private widget.TextBox NmPetugas;
    private javax.swing.JCheckBox Obesitas;
    private widget.TextBox PPRate;
    private widget.TextBox PRInterval;
    private javax.swing.JCheckBox PulmonaryDisease;
    private widget.TextBox QRSAxis;
    private widget.TextBox QRSRate;
    private widget.TextArea QWave;
    private widget.TextBox QtInterval;
    private javax.swing.JCheckBox RAH;
    private javax.swing.JCheckBox RBBB;
    private javax.swing.JCheckBox RVH;
    private widget.TextArea STdepresed;
    private widget.TextArea STelevation;
    private widget.TextBox SVESVES;
    private widget.ScrollPane Scroll;
    private javax.swing.JCheckBox SynusArrhytmia;
    private javax.swing.JCheckBox SynusBradycardia;
    private javax.swing.JCheckBox SynusRhytme;
    private javax.swing.JCheckBox SynusTachycardia;
    private widget.TextBox TCari;
    private widget.TextArea TFlat;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.TextBox TglLahir;
    private widget.Tanggal TglPemeriksaan;
    private javax.swing.JCheckBox VTVF;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel11;
    private widget.Label jLabel12;
    private widget.Label jLabel13;
    private widget.Label jLabel14;
    private widget.Label jLabel15;
    private widget.Label jLabel16;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel29;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private widget.Label jLabel9;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private widget.Label label11;
    private widget.Label label14;
    private widget.Label label15;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.TextArea rPremordial;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane10;
    private widget.ScrollPane scrollPane11;
    private widget.ScrollPane scrollPane12;
    private widget.ScrollPane scrollPane13;
    private widget.ScrollPane scrollPane5;
    private widget.ScrollPane scrollPane6;
    private widget.ScrollPane scrollPane7;
    private widget.ScrollPane scrollPane8;
    private widget.ScrollPane scrollPane9;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().trim().equals("")){
                ps=koneksi.prepareStatement(
                        "select pe.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk," +
                        "p.tgl_lahir,date_format(pe.tanggal,'%d-%m-%Y') as tanggal," +
                        "pe.checkup,pe.chestpain,pe.keluhan,pe.hipertensi,pe.pulmonarydisease,pe.arrhytmia,pe.obesitas," +
                        "pe.synusrhytme,pe.synustachycardia,pe.synusarrhytmia,pe.synusbradycardia," +
                        "pe.lowvoltage,pe.afaff,pe.cvtpat,pe.vtvf,pe.rbbb,pe.lbbb,pe.lvh,pe.rvh,pe.lah,pe.rah,pe.avblok," +
                        "pe.qrsrate,pe.pprate,pe.qrsaxis,pe.printerval,pe.qtinterval,pe.svesves," +
                        "pe.deltawave,pe.qwave,pe.rpremordial,pe.stdepresed,pe.stelevation,pe.tflat," +
                        "pe.kesan,pe.anjuran,pe.kd_dokter,d.nm_dokter,pe.kd_petugas,p2.nama " +
                        "from reg_periksa inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_ekg pe on reg_periksa.no_rawat = pe.no_rawat " +
                        "inner join dokter d on pe.kd_dokter = d.kd_dokter " +
                        "inner join petugas p2 on pe.kd_petugas = p2.nip " +
                        "where pe.tanggal between ? and ? " +
                        "order by pe.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                        "select pe.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk," +
                        "p.tgl_lahir,date_format(pe.tanggal,'%d-%m-%Y') as tanggal," +
                        "pe.checkup,pe.chestpain,pe.keluhan,pe.hipertensi,pe.pulmonarydisease,pe.arrhytmia,pe.obesitas," +
                        "pe.synusrhytme,pe.synustachycardia,pe.synusarrhytmia,pe.synusbradycardia," +
                        "pe.lowvoltage,pe.afaff,pe.cvtpat,pe.vtvf,pe.rbbb,pe.lbbb,pe.lvh,pe.rvh,pe.lah,pe.rah,pe.avblok," +
                        "pe.qrsrate,pe.pprate,pe.qrsaxis,pe.printerval,pe.qtinterval,pe.svesves," +
                        "pe.deltawave,pe.qwave,pe.rpremordial,pe.stdepresed,pe.stelevation,pe.tflat," +
                        "pe.kesan,pe.anjuran,pe.kd_dokter,d.nm_dokter,pe.kd_petugas,p2.nama " +
                        "from reg_periksa inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_ekg pe on reg_periksa.no_rawat = pe.no_rawat " +
                        "inner join dokter d on pe.kd_dokter = d.kd_dokter " +
                        "inner join petugas p2 on pe.kd_petugas = p2.nip " +
                        "where pe.tanggal between ? and ? and reg_periksa.no_rawat like ? or p.no_rkm_medis like ? or p.nm_pasien like ? or pe.kd_dokter like ? or d.nm_dokter like ? " +
                        "order by pe.tanggal");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("tgl_lahir"),rs.getString("jk"),rs.getString("kd_dokter"),rs.getString("nm_dokter"),rs.getString("tanggal"),
                        rs.getString("checkup"),rs.getString("chestpain"),rs.getString("keluhan"),rs.getString("hipertensi"),rs.getString("pulmonarydisease"),rs.getString("arrhytmia"),rs.getString("obesitas"),
                        rs.getString("synusrhytme"),rs.getString("synustachycardia"),rs.getString("synusarrhytmia"),rs.getString("synusbradycardia"),
                        rs.getString("lowvoltage"),rs.getString("afaff"),rs.getString("cvtpat"),rs.getString("vtvf"),rs.getString("rbbb"),rs.getString("lbbb"),
                        rs.getString("lvh"),rs.getString("rvh"),rs.getString("lah"),rs.getString("rah"),rs.getString("avblok"),
                        rs.getString("qrsrate"),rs.getString("pprate"),rs.getString("qrsaxis"),rs.getString("printerval"),rs.getString("qtinterval"),
                        rs.getString("svesves"),rs.getString("deltawave"),rs.getString("qwave"),rs.getString("rpremordial"),rs.getString("stdepresed"),
                        rs.getString("stelevation"),rs.getString("tflat"),rs.getString("kesan"),rs.getString("anjuran"),
                        rs.getString("kd_petugas"),rs.getString("nama")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TabRawat.setSelectedIndex(0);
        TglPemeriksaan.setDate(new Date());
        CheckUp.setSelected(false);
        ChestPain.setSelected(false);
        Hipertensi.setSelected(false);
        PulmonaryDisease.setSelected(false);
        Arrhytmia.setSelected(false);
        Obesitas.setSelected(false);
        Keluhan.setText("");
        SynusRhytme.setSelected(false);
        SynusTachycardia.setSelected(false);
        SynusArrhytmia.setSelected(false);
        SynusBradycardia.setSelected(false);
        LowVoltage.setSelected(false);
        AFAFF.setSelected(false);
        CVTPAT.setSelected(false);
        VTVF.setSelected(false);
        RBBB.setSelected(false);
        LBBB.setSelected(false);
        LVH.setSelected(false);
        RVH.setSelected(false);
        LAH.setSelected(false);
        RAH.setSelected(false);
        AVBlok.setSelected(false);
        QRSRate.setText("");
        PPRate.setText("");
        QRSAxis.setText("");
        PRInterval.setText("");
        QtInterval.setText("");
        SVESVES.setText("");
        DeltaWave.setText("");
        QWave.setText("");
        rPremordial.setText("");
        STdepresed.setText("");
        STelevation.setText("");
        TFlat.setText("");
        Kesan.setText("");
        Anjuran.setText("");
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            Valid.SetTgl2(TglPemeriksaan,tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            CheckUp.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString()));
            ChestPain.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString()));
            Keluhan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            Hipertensi.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString()));
            PulmonaryDisease.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString()));
            Arrhytmia.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString()));
            Obesitas.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString()));
            SynusRhytme.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString()));
            SynusTachycardia.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString()));
            SynusArrhytmia.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString()));
            SynusBradycardia.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString()));
            LowVoltage.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString()));
            AFAFF.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString()));
            CVTPAT.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString()));
            VTVF.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString()));
            RBBB.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString()));
            LBBB.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString()));
            LVH.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString()));
            RVH.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString()));
            LAH.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),27).toString()));
            RAH.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString()));
            AVBlok.setSelected(Boolean.valueOf(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString()));
            QRSRate.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
            PPRate.setText(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            QRSAxis.setText(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());
            PRInterval.setText(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString());
            QtInterval.setText(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());
            SVESVES.setText(tbObat.getValueAt(tbObat.getSelectedRow(),35).toString());
            DeltaWave.setText(tbObat.getValueAt(tbObat.getSelectedRow(),36).toString());
            QWave.setText(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString());
            rPremordial.setText(tbObat.getValueAt(tbObat.getSelectedRow(),38).toString());
            STdepresed.setText(tbObat.getValueAt(tbObat.getSelectedRow(),39).toString());
            STelevation.setText(tbObat.getValueAt(tbObat.getSelectedRow(),40).toString());
            TFlat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),41).toString());
            Kesan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),42).toString());
            Anjuran.setText(tbObat.getValueAt(tbObat.getSelectedRow(),43).toString());
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),45).toString());
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
 
    public void setNoRm(String norwt,Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);   
        //SET DOKTER BERDASAR AKSES AKUN & PETUGAS (-)
        KdDokter.setText(akses.getkode());
        Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?", NmDokter,KdDokter.getText());
        KdPetugas.setText("-");
        Sequel.cariIsi("select nama from petugas where nip=?", NmPetugas,KdPetugas.getText());
        isRawat(); 
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_awal_medis_ralan());
        BtnHapus.setEnabled(akses.getpenilaian_awal_medis_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_medis_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_medis_ralan());         
    }
    
    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from penilaian_ekg where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            tampil();
            TabRawat.setSelectedIndex(1);
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("penilaian_ekg","no_rawat=?","no_rawat=?,tanggal=?,kd_dokter=?,kd_petugas=?,checkup=?,chestpain=?,keluhan=?,hipertensi=?,pulmonarydisease=?,arrhytmia=?,obesitas=?,"
                + "synusrhytme=?,synustachycardia=?,synusarrhytmia=?,synusbradycardia=?,lowvoltage=?,afaff=?,cvtpat=?,vtvf=?,rbbb=?,lbbb=?,"
                + "lvh=?,rvh=?,lah=?,rah=?,avblok=?,qrsrate=?,pprate=?,qrsaxis=?,printerval=?,qtinterval=?,svesves=?,deltawave=?,qwave=?,rpremordial=?,"
                + "stdepresed=?,stelevation=?,tflat=?,kesan=?,anjuran=?",41,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),KdPetugas.getText(),String.valueOf(CheckUp.isSelected()),String.valueOf(ChestPain.isSelected()),
                Keluhan.getText(),String.valueOf(Hipertensi.isSelected()),String.valueOf(PulmonaryDisease.isSelected()),
                String.valueOf(Arrhytmia.isSelected()),String.valueOf(Obesitas.isSelected()),String.valueOf(SynusRhytme.isSelected()),
                String.valueOf(SynusTachycardia.isSelected()),String.valueOf(SynusArrhytmia.isSelected()),String.valueOf(SynusBradycardia.isSelected()),
                String.valueOf(LowVoltage.isSelected()),String.valueOf(AFAFF.isSelected()),String.valueOf(CVTPAT.isSelected()),String.valueOf(VTVF.isSelected()),
                String.valueOf(RBBB.isSelected()),String.valueOf(LBBB.isSelected()),String.valueOf(LVH.isSelected()),String.valueOf(RVH.isSelected()),
                String.valueOf(LAH.isSelected()),String.valueOf(RAH.isSelected()),String.valueOf(AVBlok.isSelected()),
                QRSRate.getText(),PPRate.getText(),QRSAxis.getText(),PRInterval.getText(),QtInterval.getText(),SVESVES.getText(),
                DeltaWave.getText(),QWave.getText(),rPremordial.getText(),STdepresed.getText(),STelevation.getText(),TFlat.getText(),
                Kesan.getText(),Anjuran.getText(),tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
            })==true){
               tampil();
               emptTeks();
               TabRawat.setSelectedIndex(1);
        }
    }
    
}
