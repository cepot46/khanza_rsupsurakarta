/*
 * Kontribusi dari Abdul Wahid, RSUD Cipayung Jakarta Timur
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariPetugas;


/**
 *
 * @author perpustakaan
 */
public final class TesAlergi extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;  
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps,ps2;
    private ResultSet rs,rs2;
    private int i=0;   
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private boolean[] pilih;    
    private String finger="";
    private StringBuilder htmlContent;
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public TesAlergi(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","J.K.","House Dust","Bulu Anjing","Kapuk","Bulu Ayam","Cumi-cumi","Kakap","Udang","Kerang","Ikan Tawar","Tongkol",
            "Pindang","Bandeng","Kepiting","Vetsin","Daging Sapi","Daging Ayam","Daging Kambing","Kuning Telur",
            "Putih Telur","Kacang Mete","Kacang Hijau","Kacang Tanah","Kedelai","Terigu","Gandum","Cokelat","Susu","Kopi",
            "Wortel","Tomat","Nanas","Teh","Nama Petugas","Kode Petugas","Waktu Tes","Kontrol Positif","Kontrol Negatif","Kesimpulan","Jagung"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 43; i++) {  //CUSTOM MUHSIN
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(65);
            }else if(i==2){
                column.setPreferredWidth(160);
            }else if(i==3){
                column.setPreferredWidth(50);
            }else if(i==4){
                column.setPreferredWidth(80);
            }else if(i==5){
                column.setPreferredWidth(80);
            }else if(i==6){
                column.setPreferredWidth(80);
            }else if(i==7){
                column.setPreferredWidth(80);
            }else if(i==8){
                column.setPreferredWidth(80);
            }else if(i==9){
                column.setPreferredWidth(80);
            }else if(i==10){
                column.setPreferredWidth(80);
            }else if(i==11){
                column.setPreferredWidth(80);
            }else if(i==12){
                column.setPreferredWidth(80);
            }else if(i==13){
                column.setPreferredWidth(80);
            }else if(i==14){
                column.setPreferredWidth(80);
            }else if(i==15){
                column.setPreferredWidth(80);   //BANDENG
            }else if(i==16){
                column.setPreferredWidth(80);
            }else if(i==17){
                column.setPreferredWidth(80);
            }else if(i==18){
                column.setPreferredWidth(80);
            }else if(i==19){
                column.setPreferredWidth(80);
            }else if(i==20){
                column.setPreferredWidth(80);
            }else if(i==21){
                column.setPreferredWidth(80);
            }else if(i==22){
                column.setPreferredWidth(80);
            }else if(i==23){
                column.setPreferredWidth(80);
            }else if(i==24){
                column.setPreferredWidth(80);
            }else if(i==25){
                column.setPreferredWidth(80);
            }else if(i==26){
                column.setPreferredWidth(80);
            }else if(i==27){
                column.setPreferredWidth(80);
            }else if(i==28){
                column.setPreferredWidth(80);
            }else if(i==29){
                column.setPreferredWidth(80);
            }else if(i==30){
                column.setPreferredWidth(80);   //SUSU
            }else if(i==31){
                column.setPreferredWidth(80);
            }else if(i==32){
                column.setPreferredWidth(80);
            }else if(i==33){
                column.setPreferredWidth(80);
            }else if(i==34){
                column.setPreferredWidth(80);
            }else if(i==35){
                column.setPreferredWidth(80);   //TEH
            }else if(i==36){
                column.setPreferredWidth(150);
            }else if(i==37){
                column.setPreferredWidth(150);
            }else if(i==38){
                column.setPreferredWidth(150);
            }else if(i==39){
                column.setPreferredWidth(80);
            }else if(i==40){
                column.setPreferredWidth(80);
            }else if(i==41){
                column.setPreferredWidth(200);
            }else{
                column.setPreferredWidth(80);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());

        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        housedust.setDocument(new batasInput((byte)8).getKata(housedust));
        buluanjing.setDocument(new batasInput((byte)3).getKata(buluanjing));    
        kapuk.setDocument(new batasInput((byte)3).getKata(kapuk));   
        buluayam.setDocument(new batasInput((byte)3).getKata(buluayam));
        cumicumi.setDocument(new batasInput((byte)3).getKata(cumicumi));    
        kakap.setDocument(new batasInput((byte)3).getKata(kakap));    
        udang.setDocument(new batasInput((byte)3).getKata(udang));
        kerang.setDocument(new batasInput((byte)3).getKata(kerang));
        ikantawar.setDocument(new batasInput((byte)3).getKata(ikantawar));
        tongkol.setDocument(new batasInput((byte)3).getKata(tongkol));
        pindang.setDocument(new batasInput((byte)3).getKata(pindang));
        bandeng.setDocument(new batasInput((byte)3).getKata(bandeng));
        kepiting.setDocument(new batasInput((byte)3).getKata(kepiting));
        vetsin.setDocument(new batasInput((byte)3).getKata(vetsin));
        dagingsapi.setDocument(new batasInput((byte)3).getKata(dagingsapi));
        dagingayam.setDocument(new batasInput((byte)3).getKata(dagingayam));
        dagingkambing.setDocument(new batasInput((byte)3).getKata(dagingkambing));
        kuningtelur.setDocument(new batasInput((byte)3).getKata(kuningtelur));
        putihtelur.setDocument(new batasInput((byte)3).getKata(putihtelur));
        kacangmete.setDocument(new batasInput((byte)3).getKata(kacangmete));
        kacanghijau.setDocument(new batasInput((byte)3).getKata(kacanghijau));
        kacangtanah.setDocument(new batasInput((byte)3).getKata(kacangtanah));
        kedelai.setDocument(new batasInput((byte)3).getKata(kedelai));
        terigu.setDocument(new batasInput((byte)3).getKata(terigu));
        gandum.setDocument(new batasInput((byte)3).getKata(gandum));
        cokelat.setDocument(new batasInput((byte)3).getKata(cokelat));
        susu.setDocument(new batasInput((byte)3).getKata(susu));
        jagung.setDocument(new batasInput((byte)3).getKata(jagung));
//        kopi.setDocument(new batasInput((byte)3).getKata(kopi));
//        wortel.setDocument(new batasInput((byte)3).getKata(wortel));
//        tomat.setDocument(new batasInput((byte)3).getKata(tomat));
//        nanas.setDocument(new batasInput((byte)3).getKata(nanas));
//        teh.setDocument(new batasInput((byte)3).getKata(teh));
        kontrolpositif.setDocument(new batasInput((byte)3).getKata(kontrolpositif));
        kontrolnegatif.setDocument(new batasInput((byte)3).getKata(kontrolpositif));
        Kesimpulan.setDocument(new batasInput((int)200).getKata(Kesimpulan));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){ 
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());   
                }              
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
        
        
        ChkAccor.setSelected(false);
        isMenu();
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        label11 = new widget.Label();
        jLabel11 = new widget.Label();
        jLabel12 = new widget.Label();
        cumicumi = new widget.TextBox();
        jLabel13 = new widget.Label();
        kakap = new widget.TextBox();
        jLabel15 = new widget.Label();
        jLabel16 = new widget.Label();
        buluanjing = new widget.TextBox();
        jLabel17 = new widget.Label();
        jLabel18 = new widget.Label();
        buluayam = new widget.TextBox();
        jLabel22 = new widget.Label();
        housedust = new widget.TextBox();
        jLabel20 = new widget.Label();
        jLabel23 = new widget.Label();
        jLabel24 = new widget.Label();
        jLabel25 = new widget.Label();
        kapuk = new widget.TextBox();
        jLabel26 = new widget.Label();
        jLabel14 = new widget.Label();
        udang = new widget.TextBox();
        jLabel27 = new widget.Label();
        jLabel53 = new widget.Label();
        TglAsuhan = new widget.Tanggal();
        jLabel93 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        Scroll6 = new widget.ScrollPane();
        tbMasalahKeperawatan = new widget.Table();
        jLabel33 = new widget.Label();
        kerang = new widget.TextBox();
        Scroll8 = new widget.ScrollPane();
        tbIntervensiResikoJatuh = new widget.Table();
        jLabel28 = new widget.Label();
        jLabel30 = new widget.Label();
        ikantawar = new widget.TextBox();
        jLabel31 = new widget.Label();
        jLabel32 = new widget.Label();
        tongkol = new widget.TextBox();
        jLabel35 = new widget.Label();
        jLabel37 = new widget.Label();
        pindang = new widget.TextBox();
        jLabel38 = new widget.Label();
        jLabel39 = new widget.Label();
        bandeng = new widget.TextBox();
        jLabel40 = new widget.Label();
        jLabel41 = new widget.Label();
        kepiting = new widget.TextBox();
        jLabel42 = new widget.Label();
        jLabel43 = new widget.Label();
        vetsin = new widget.TextBox();
        jLabel44 = new widget.Label();
        jLabel45 = new widget.Label();
        dagingsapi = new widget.TextBox();
        jLabel46 = new widget.Label();
        jLabel47 = new widget.Label();
        dagingayam = new widget.TextBox();
        jLabel48 = new widget.Label();
        jLabel49 = new widget.Label();
        dagingkambing = new widget.TextBox();
        jLabel50 = new widget.Label();
        jLabel51 = new widget.Label();
        putihtelur = new widget.TextBox();
        jLabel52 = new widget.Label();
        jLabel54 = new widget.Label();
        jLabel55 = new widget.Label();
        kuningtelur = new widget.TextBox();
        jLabel56 = new widget.Label();
        kacangmete = new widget.TextBox();
        jLabel57 = new widget.Label();
        terigu = new widget.TextBox();
        kacanghijau = new widget.TextBox();
        kedelai = new widget.TextBox();
        jLabel58 = new widget.Label();
        jLabel59 = new widget.Label();
        jLabel60 = new widget.Label();
        kacangtanah = new widget.TextBox();
        jLabel61 = new widget.Label();
        jLabel62 = new widget.Label();
        jLabel63 = new widget.Label();
        jLabel64 = new widget.Label();
        jLabel65 = new widget.Label();
        jLabel66 = new widget.Label();
        jLabel67 = new widget.Label();
        cokelat = new widget.TextBox();
        jLabel68 = new widget.Label();
        gandum = new widget.TextBox();
        susu = new widget.TextBox();
        jLabel69 = new widget.Label();
        jLabel70 = new widget.Label();
        jLabel71 = new widget.Label();
        jLabel72 = new widget.Label();
        jLabel73 = new widget.Label();
        jLabel78 = new widget.Label();
        jLabel80 = new widget.Label();
        kontrolpositif = new widget.TextBox();
        jLabel84 = new widget.Label();
        kontrolnegatif = new widget.TextBox();
        jLabel85 = new widget.Label();
        jLabel86 = new widget.Label();
        scrollPane1 = new widget.ScrollPane();
        Kesimpulan = new widget.TextArea();
        jagung = new widget.TextBox();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        PanelAccor = new widget.PanelBiasa();
        ChkAccor = new widget.CekBox();
        FormMenu = new widget.PanelBiasa();
        jLabel34 = new widget.Label();
        TNoRM1 = new widget.TextBox();
        TPasien1 = new widget.TextBox();
        BtnPrint1 = new widget.Button();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Tes Alergi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50)));
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 11));
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 550));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(74, 40, 100, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(176, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(358, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(395, 40, 57, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jLabel12.setText("1. Cumi-cumi :");
        jLabel12.setName("jLabel12"); // NOI18N
        FormInput.add(jLabel12);
        jLabel12.setBounds(40, 140, 70, 23);

        cumicumi.setFocusTraversalPolicyProvider(true);
        cumicumi.setName("cumicumi"); // NOI18N
        cumicumi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cumicumiKeyPressed(evt);
            }
        });
        FormInput.add(cumicumi);
        cumicumi.setBounds(120, 140, 60, 23);

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel13.setText("mm");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput.add(jLabel13);
        jLabel13.setBounds(190, 140, 30, 23);

        kakap.setFocusTraversalPolicyProvider(true);
        kakap.setName("kakap"); // NOI18N
        kakap.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kakapKeyPressed(evt);
            }
        });
        FormInput.add(kakap);
        kakap.setBounds(310, 140, 60, 23);

        jLabel15.setText("2. Kakap :");
        jLabel15.setName("jLabel15"); // NOI18N
        FormInput.add(jLabel15);
        jLabel15.setBounds(250, 140, 60, 23);

        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel16.setText("mm");
        jLabel16.setName("jLabel16"); // NOI18N
        FormInput.add(jLabel16);
        jLabel16.setBounds(380, 90, 20, 23);

        buluanjing.setFocusTraversalPolicyProvider(true);
        buluanjing.setName("buluanjing"); // NOI18N
        buluanjing.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                buluanjingKeyPressed(evt);
            }
        });
        FormInput.add(buluanjing);
        buluanjing.setBounds(310, 90, 60, 23);

        jLabel17.setText("2. Bulu Anjing :");
        jLabel17.setName("jLabel17"); // NOI18N
        FormInput.add(jLabel17);
        jLabel17.setBounds(230, 90, 80, 23);

        jLabel18.setText("4. Bulu Ayam :");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(610, 90, 70, 23);

        buluayam.setFocusTraversalPolicyProvider(true);
        buluayam.setName("buluayam"); // NOI18N
        buluayam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                buluayamKeyPressed(evt);
            }
        });
        FormInput.add(buluayam);
        buluayam.setBounds(680, 90, 60, 23);

        jLabel22.setText("1. House Dust :");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(20, 90, 90, 23);

        housedust.setFocusTraversalPolicyProvider(true);
        housedust.setName("housedust"); // NOI18N
        housedust.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                housedustKeyPressed(evt);
            }
        });
        FormInput.add(housedust);
        housedust.setBounds(120, 90, 60, 23);

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setText("mm");
        jLabel20.setName("jLabel20"); // NOI18N
        FormInput.add(jLabel20);
        jLabel20.setBounds(750, 140, 30, 23);

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel23.setText("mm");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(190, 90, 20, 23);

        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel24.setText("mm");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(380, 140, 30, 23);

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel25.setText("mm");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(560, 90, 20, 23);

        kapuk.setFocusTraversalPolicyProvider(true);
        kapuk.setName("kapuk"); // NOI18N
        kapuk.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kapukKeyPressed(evt);
            }
        });
        FormInput.add(kapuk);
        kapuk.setBounds(490, 90, 60, 23);

        jLabel26.setText("3. Kapuk :");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(440, 90, 50, 23);

        jLabel14.setText("3. Udang :");
        jLabel14.setName("jLabel14"); // NOI18N
        FormInput.add(jLabel14);
        jLabel14.setBounds(430, 140, 60, 23);

        udang.setFocusTraversalPolicyProvider(true);
        udang.setName("udang"); // NOI18N
        udang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                udangKeyPressed(evt);
            }
        });
        FormInput.add(udang);
        udang.setBounds(490, 140, 60, 23);

        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel27.setText("mm");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(560, 140, 20, 23);

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel53.setText("I. ALERGEN HIRUP/INHALAN");
        jLabel53.setName("jLabel53"); // NOI18N
        FormInput.add(jLabel53);
        jLabel53.setBounds(10, 70, 180, 23);

        TglAsuhan.setForeground(new java.awt.Color(50, 70, 50));
        TglAsuhan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "19-07-2022 13:26:14" }));
        TglAsuhan.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglAsuhan.setName("TglAsuhan"); // NOI18N
        TglAsuhan.setOpaque(false);
        TglAsuhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglAsuhanKeyPressed(evt);
            }
        });
        FormInput.add(TglAsuhan);
        TglAsuhan.setBounds(456, 40, 130, 23);

        jLabel93.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel93.setText("II. ALERGEN MAKANAN");
        jLabel93.setName("jLabel93"); // NOI18N
        FormInput.add(jLabel93);
        jLabel93.setBounds(10, 120, 180, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 70, 880, 1);

        jSeparator2.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator2.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator2.setName("jSeparator2"); // NOI18N
        FormInput.add(jSeparator2);
        jSeparator2.setBounds(0, 120, 880, 1);

        Scroll6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll6.setName("Scroll6"); // NOI18N
        Scroll6.setOpaque(true);

        tbMasalahKeperawatan.setName("tbMasalahKeperawatan"); // NOI18N
        Scroll6.setViewportView(tbMasalahKeperawatan);

        FormInput.add(Scroll6);
        Scroll6.setBounds(10, 1210, 400, 133);

        jLabel33.setText("4. Kerang :");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(620, 140, 60, 23);

        kerang.setFocusTraversalPolicyProvider(true);
        kerang.setName("kerang"); // NOI18N
        kerang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kerangKeyPressed(evt);
            }
        });
        FormInput.add(kerang);
        kerang.setBounds(680, 140, 60, 23);

        Scroll8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll8.setName("Scroll8"); // NOI18N
        Scroll8.setOpaque(true);

        tbIntervensiResikoJatuh.setName("tbIntervensiResikoJatuh"); // NOI18N
        Scroll8.setViewportView(tbIntervensiResikoJatuh);

        FormInput.add(Scroll8);
        Scroll8.setBounds(10, 700, 560, 170);

        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel28.setText("mm");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(750, 90, 30, 23);

        jLabel30.setText("5. Ikan Tawar :");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(30, 180, 80, 23);

        ikantawar.setFocusTraversalPolicyProvider(true);
        ikantawar.setName("ikantawar"); // NOI18N
        ikantawar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ikantawarKeyPressed(evt);
            }
        });
        FormInput.add(ikantawar);
        ikantawar.setBounds(120, 180, 60, 23);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("mm");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(190, 180, 30, 23);

        jLabel32.setText("6. Tongkol :");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(250, 180, 60, 23);

        tongkol.setFocusTraversalPolicyProvider(true);
        tongkol.setName("tongkol"); // NOI18N
        tongkol.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tongkolKeyPressed(evt);
            }
        });
        FormInput.add(tongkol);
        tongkol.setBounds(310, 180, 60, 23);

        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel35.setText("mm");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(380, 180, 30, 23);

        jLabel37.setText("7. Pindang :");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(420, 180, 70, 23);

        pindang.setFocusTraversalPolicyProvider(true);
        pindang.setName("pindang"); // NOI18N
        pindang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pindangKeyPressed(evt);
            }
        });
        FormInput.add(pindang);
        pindang.setBounds(490, 180, 60, 23);

        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel38.setText("mm");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(560, 180, 20, 23);

        jLabel39.setText("8. Bandeng :");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(610, 180, 70, 23);

        bandeng.setFocusTraversalPolicyProvider(true);
        bandeng.setName("bandeng"); // NOI18N
        bandeng.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bandengKeyPressed(evt);
            }
        });
        FormInput.add(bandeng);
        bandeng.setBounds(680, 180, 60, 23);

        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel40.setText("mm");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(750, 180, 30, 23);

        jLabel41.setText("9. Kepiting :");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(40, 220, 70, 23);

        kepiting.setFocusTraversalPolicyProvider(true);
        kepiting.setName("kepiting"); // NOI18N
        kepiting.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kepitingKeyPressed(evt);
            }
        });
        FormInput.add(kepiting);
        kepiting.setBounds(120, 220, 60, 23);

        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel42.setText("mm");
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(190, 220, 30, 23);

        jLabel43.setText("10. Vetsin :");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(250, 220, 60, 23);

        vetsin.setFocusTraversalPolicyProvider(true);
        vetsin.setName("vetsin"); // NOI18N
        vetsin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                vetsinKeyPressed(evt);
            }
        });
        FormInput.add(vetsin);
        vetsin.setBounds(310, 220, 60, 23);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel44.setText("mm");
        jLabel44.setName("jLabel44"); // NOI18N
        FormInput.add(jLabel44);
        jLabel44.setBounds(380, 220, 30, 23);

        jLabel45.setText("11. Daging Sapi :");
        jLabel45.setName("jLabel45"); // NOI18N
        FormInput.add(jLabel45);
        jLabel45.setBounds(400, 220, 90, 23);

        dagingsapi.setFocusTraversalPolicyProvider(true);
        dagingsapi.setName("dagingsapi"); // NOI18N
        dagingsapi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dagingsapiKeyPressed(evt);
            }
        });
        FormInput.add(dagingsapi);
        dagingsapi.setBounds(490, 220, 60, 23);

        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel46.setText("mm");
        jLabel46.setName("jLabel46"); // NOI18N
        FormInput.add(jLabel46);
        jLabel46.setBounds(560, 220, 20, 23);

        jLabel47.setText("12. Daging Ayam :");
        jLabel47.setName("jLabel47"); // NOI18N
        FormInput.add(jLabel47);
        jLabel47.setBounds(590, 220, 90, 23);

        dagingayam.setFocusTraversalPolicyProvider(true);
        dagingayam.setName("dagingayam"); // NOI18N
        dagingayam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dagingayamKeyPressed(evt);
            }
        });
        FormInput.add(dagingayam);
        dagingayam.setBounds(680, 220, 60, 23);

        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel48.setText("mm");
        jLabel48.setName("jLabel48"); // NOI18N
        FormInput.add(jLabel48);
        jLabel48.setBounds(750, 220, 30, 23);

        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel49.setText("mm");
        jLabel49.setName("jLabel49"); // NOI18N
        FormInput.add(jLabel49);
        jLabel49.setBounds(380, 260, 20, 23);

        dagingkambing.setFocusTraversalPolicyProvider(true);
        dagingkambing.setName("dagingkambing"); // NOI18N
        dagingkambing.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dagingkambingKeyPressed(evt);
            }
        });
        FormInput.add(dagingkambing);
        dagingkambing.setBounds(120, 260, 60, 23);

        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel50.setText("mm");
        jLabel50.setName("jLabel50"); // NOI18N
        FormInput.add(jLabel50);
        jLabel50.setBounds(560, 260, 20, 23);

        jLabel51.setText("13. Daging Kambing :");
        jLabel51.setName("jLabel51"); // NOI18N
        FormInput.add(jLabel51);
        jLabel51.setBounds(0, 260, 110, 23);

        putihtelur.setFocusTraversalPolicyProvider(true);
        putihtelur.setName("putihtelur"); // NOI18N
        putihtelur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                putihtelurKeyPressed(evt);
            }
        });
        FormInput.add(putihtelur);
        putihtelur.setBounds(490, 260, 60, 23);

        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel52.setText("mm");
        jLabel52.setName("jLabel52"); // NOI18N
        FormInput.add(jLabel52);
        jLabel52.setBounds(750, 260, 30, 23);

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("mm");
        jLabel54.setName("jLabel54"); // NOI18N
        FormInput.add(jLabel54);
        jLabel54.setBounds(190, 260, 30, 23);

        jLabel55.setText("14. Kuning Telur:");
        jLabel55.setName("jLabel55"); // NOI18N
        FormInput.add(jLabel55);
        jLabel55.setBounds(220, 260, 90, 23);

        kuningtelur.setFocusTraversalPolicyProvider(true);
        kuningtelur.setName("kuningtelur"); // NOI18N
        kuningtelur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kuningtelurKeyPressed(evt);
            }
        });
        FormInput.add(kuningtelur);
        kuningtelur.setBounds(310, 260, 60, 23);

        jLabel56.setText("15. Putih Telur:");
        jLabel56.setName("jLabel56"); // NOI18N
        FormInput.add(jLabel56);
        jLabel56.setBounds(400, 260, 90, 23);

        kacangmete.setFocusTraversalPolicyProvider(true);
        kacangmete.setName("kacangmete"); // NOI18N
        kacangmete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kacangmeteKeyPressed(evt);
            }
        });
        FormInput.add(kacangmete);
        kacangmete.setBounds(680, 260, 60, 23);

        jLabel57.setText("16. Kacang Mete :");
        jLabel57.setName("jLabel57"); // NOI18N
        FormInput.add(jLabel57);
        jLabel57.setBounds(580, 260, 100, 23);

        terigu.setFocusTraversalPolicyProvider(true);
        terigu.setName("terigu"); // NOI18N
        terigu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                teriguKeyPressed(evt);
            }
        });
        FormInput.add(terigu);
        terigu.setBounds(680, 300, 60, 23);

        kacanghijau.setFocusTraversalPolicyProvider(true);
        kacanghijau.setName("kacanghijau"); // NOI18N
        kacanghijau.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kacanghijauKeyPressed(evt);
            }
        });
        FormInput.add(kacanghijau);
        kacanghijau.setBounds(120, 300, 60, 23);

        kedelai.setFocusTraversalPolicyProvider(true);
        kedelai.setName("kedelai"); // NOI18N
        kedelai.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kedelaiKeyPressed(evt);
            }
        });
        FormInput.add(kedelai);
        kedelai.setBounds(490, 300, 60, 23);

        jLabel58.setText("17. Kacang Hijau :");
        jLabel58.setName("jLabel58"); // NOI18N
        FormInput.add(jLabel58);
        jLabel58.setBounds(10, 300, 100, 23);

        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel59.setText("mm");
        jLabel59.setName("jLabel59"); // NOI18N
        FormInput.add(jLabel59);
        jLabel59.setBounds(560, 300, 30, 23);

        jLabel60.setText("18. Kacang Tanah :");
        jLabel60.setName("jLabel60"); // NOI18N
        FormInput.add(jLabel60);
        jLabel60.setBounds(210, 300, 100, 23);

        kacangtanah.setFocusTraversalPolicyProvider(true);
        kacangtanah.setName("kacangtanah"); // NOI18N
        kacangtanah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kacangtanahKeyPressed(evt);
            }
        });
        FormInput.add(kacangtanah);
        kacangtanah.setBounds(310, 300, 60, 23);

        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel61.setText("mm");
        jLabel61.setName("jLabel61"); // NOI18N
        FormInput.add(jLabel61);
        jLabel61.setBounds(750, 300, 30, 23);

        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel62.setText("mm");
        jLabel62.setName("jLabel62"); // NOI18N
        FormInput.add(jLabel62);
        jLabel62.setBounds(380, 300, 20, 23);

        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel63.setText("mm");
        jLabel63.setName("jLabel63"); // NOI18N
        FormInput.add(jLabel63);
        jLabel63.setBounds(190, 300, 30, 23);

        jLabel64.setText("19. Kedelai :");
        jLabel64.setName("jLabel64"); // NOI18N
        FormInput.add(jLabel64);
        jLabel64.setBounds(430, 300, 60, 23);

        jLabel65.setText("20. Terigu :");
        jLabel65.setName("jLabel65"); // NOI18N
        FormInput.add(jLabel65);
        jLabel65.setBounds(620, 300, 60, 23);

        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel66.setText("mm");
        jLabel66.setName("jLabel66"); // NOI18N
        FormInput.add(jLabel66);
        jLabel66.setBounds(380, 340, 30, 23);

        jLabel67.setText("23. Susu :");
        jLabel67.setName("jLabel67"); // NOI18N
        FormInput.add(jLabel67);
        jLabel67.setBounds(440, 340, 50, 23);

        cokelat.setFocusTraversalPolicyProvider(true);
        cokelat.setName("cokelat"); // NOI18N
        cokelat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cokelatKeyPressed(evt);
            }
        });
        FormInput.add(cokelat);
        cokelat.setBounds(310, 340, 60, 23);

        jLabel68.setText("22. Cokelat :");
        jLabel68.setName("jLabel68"); // NOI18N
        FormInput.add(jLabel68);
        jLabel68.setBounds(240, 340, 70, 23);

        gandum.setFocusTraversalPolicyProvider(true);
        gandum.setName("gandum"); // NOI18N
        gandum.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                gandumKeyPressed(evt);
            }
        });
        FormInput.add(gandum);
        gandum.setBounds(120, 340, 60, 23);

        susu.setFocusTraversalPolicyProvider(true);
        susu.setName("susu"); // NOI18N
        susu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                susuKeyPressed(evt);
            }
        });
        FormInput.add(susu);
        susu.setBounds(490, 340, 60, 23);

        jLabel69.setText("21. Gandum :");
        jLabel69.setName("jLabel69"); // NOI18N
        FormInput.add(jLabel69);
        jLabel69.setBounds(40, 340, 70, 23);

        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel70.setText("mm");
        jLabel70.setName("jLabel70"); // NOI18N
        FormInput.add(jLabel70);
        jLabel70.setBounds(560, 340, 30, 23);

        jLabel71.setText("24. Jagung :");
        jLabel71.setName("jLabel71"); // NOI18N
        FormInput.add(jLabel71);
        jLabel71.setBounds(610, 340, 70, 23);

        jLabel72.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel72.setText("mm");
        jLabel72.setName("jLabel72"); // NOI18N
        FormInput.add(jLabel72);
        jLabel72.setBounds(750, 340, 30, 23);

        jLabel73.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel73.setText("mm");
        jLabel73.setName("jLabel73"); // NOI18N
        FormInput.add(jLabel73);
        jLabel73.setBounds(190, 340, 30, 23);

        jLabel78.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel78.setText("mm");
        jLabel78.setName("jLabel78"); // NOI18N
        FormInput.add(jLabel78);
        jLabel78.setBounds(190, 440, 30, 23);

        jLabel80.setText("Kesimpulan :");
        jLabel80.setName("jLabel80"); // NOI18N
        FormInput.add(jLabel80);
        jLabel80.setBounds(30, 490, 80, 23);

        kontrolpositif.setFocusTraversalPolicyProvider(true);
        kontrolpositif.setName("kontrolpositif"); // NOI18N
        kontrolpositif.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kontrolpositifKeyPressed(evt);
            }
        });
        FormInput.add(kontrolpositif);
        kontrolpositif.setBounds(120, 440, 60, 23);

        jLabel84.setText("Kontrol Negatif :");
        jLabel84.setName("jLabel84"); // NOI18N
        FormInput.add(jLabel84);
        jLabel84.setBounds(300, 440, 80, 23);

        kontrolnegatif.setFocusTraversalPolicyProvider(true);
        kontrolnegatif.setName("kontrolnegatif"); // NOI18N
        kontrolnegatif.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kontrolnegatifKeyPressed(evt);
            }
        });
        FormInput.add(kontrolnegatif);
        kontrolnegatif.setBounds(390, 440, 60, 23);

        jLabel85.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel85.setText("mm");
        jLabel85.setName("jLabel85"); // NOI18N
        FormInput.add(jLabel85);
        jLabel85.setBounds(460, 440, 30, 23);

        jLabel86.setText("Kontrol Positif :");
        jLabel86.setName("jLabel86"); // NOI18N
        FormInput.add(jLabel86);
        jLabel86.setBounds(30, 440, 80, 23);

        scrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane1.setName("scrollPane1"); // NOI18N

        Kesimpulan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Kesimpulan.setColumns(20);
        Kesimpulan.setRows(5);
        Kesimpulan.setName("Kesimpulan"); // NOI18N
        Kesimpulan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KesimpulanKeyPressed(evt);
            }
        });
        scrollPane1.setViewportView(Kesimpulan);

        FormInput.add(scrollPane1);
        scrollPane1.setBounds(120, 490, 260, 43);

        jagung.setFocusTraversalPolicyProvider(true);
        jagung.setName("jagung"); // NOI18N
        jagung.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jagungKeyPressed(evt);
            }
        });
        FormInput.add(jagung);
        jagung.setBounds(680, 340, 60, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Penilaian", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "19-07-2022" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "19-07-2022" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        PanelAccor.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor.setName("PanelAccor"); // NOI18N
        PanelAccor.setPreferredSize(new java.awt.Dimension(470, 43));
        PanelAccor.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setSelected(true);
        ChkAccor.setFocusable(false);
        ChkAccor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor.setName("ChkAccor"); // NOI18N
        ChkAccor.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccorActionPerformed(evt);
            }
        });
        PanelAccor.add(ChkAccor, java.awt.BorderLayout.WEST);

        FormMenu.setBackground(new java.awt.Color(255, 255, 255));
        FormMenu.setBorder(null);
        FormMenu.setName("FormMenu"); // NOI18N
        FormMenu.setPreferredSize(new java.awt.Dimension(115, 43));
        FormMenu.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        jLabel34.setText("Pasien :");
        jLabel34.setName("jLabel34"); // NOI18N
        jLabel34.setPreferredSize(new java.awt.Dimension(55, 23));
        FormMenu.add(jLabel34);

        TNoRM1.setEditable(false);
        TNoRM1.setHighlighter(null);
        TNoRM1.setName("TNoRM1"); // NOI18N
        TNoRM1.setPreferredSize(new java.awt.Dimension(100, 23));
        FormMenu.add(TNoRM1);

        TPasien1.setEditable(false);
        TPasien1.setBackground(new java.awt.Color(245, 250, 240));
        TPasien1.setHighlighter(null);
        TPasien1.setName("TPasien1"); // NOI18N
        TPasien1.setPreferredSize(new java.awt.Dimension(250, 23));
        FormMenu.add(TPasien1);

        BtnPrint1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/item (copy).png"))); // NOI18N
        BtnPrint1.setMnemonic('T');
        BtnPrint1.setToolTipText("Alt+T");
        BtnPrint1.setName("BtnPrint1"); // NOI18N
        BtnPrint1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPrint1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrint1ActionPerformed(evt);
            }
        });
        FormMenu.add(BtnPrint1);

        PanelAccor.add(FormMenu, java.awt.BorderLayout.NORTH);

        internalFrame3.add(PanelAccor, java.awt.BorderLayout.EAST);

        TabRawat.addTab("Data Penilaian", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmPetugas.getText().trim().equals("")){    //START CUSTOM MUHSIN
            Valid.textKosong(BtnDokter,"Petugas");
        }else if(housedust.getText().trim().equals("")){
            Valid.textKosong(housedust,"House Dust");
        }else if(buluanjing.getText().trim().equals("")){
            Valid.textKosong(buluanjing,"Bulu Anjing");
        }else if(kapuk.getText().trim().equals("")){
            Valid.textKosong(kapuk,"Kapuk");
        }else if(buluayam.getText().trim().equals("")){
            Valid.textKosong(buluayam,"Bulu Ayam");
        }else if(cumicumi.getText().trim().equals("")){
            Valid.textKosong(cumicumi,"Cumi-cumi");
        }else if(kakap.getText().trim().equals("")){
            Valid.textKosong(kakap,"Kakap");
        }else if(udang.getText().trim().equals("")){
            Valid.textKosong(udang,"Udang");
        }else if(kerang.getText().trim().equals("")){ 
            Valid.textKosong(kerang,"Kerang");
        }else if(ikantawar.getText().trim().equals("")){
            Valid.textKosong(ikantawar,"Ikan Tawar");
        }else if(tongkol.getText().trim().equals("")){
            Valid.textKosong(tongkol,"Tongkol");
        }else if(pindang.getText().trim().equals("")){
            Valid.textKosong(pindang,"Pindang");
        }else if(bandeng.getText().trim().equals("")){
            Valid.textKosong(bandeng,"Bandeng");
        }else if(kepiting.getText().trim().equals("")){
            Valid.textKosong(kepiting,"Kepiting");
        }else if(vetsin.getText().trim().equals("")){   
            Valid.textKosong(vetsin,"Vetsin");
        }else if(dagingsapi.getText().trim().equals("")){
            Valid.textKosong(dagingsapi,"Daging Sapi");
        }else if(dagingayam.getText().trim().equals("")){
            Valid.textKosong(dagingayam,"Daging Ayam");
        }else if(dagingkambing.getText().trim().equals("")){
            Valid.textKosong(dagingkambing,"Daging Kambing");
        }else if(kuningtelur.getText().trim().equals("")){
            Valid.textKosong(kuningtelur,"Kuning Telur");
        }else if(putihtelur.getText().trim().equals("")){
            Valid.textKosong(putihtelur,"Putih Telur");
        }else if(kacangmete.getText().trim().equals("")){
            Valid.textKosong(kacangmete,"Kacang Mete");   
        }else if(kacanghijau.getText().trim().equals("")){
            Valid.textKosong(kacanghijau,"Kacang Hijau");
        }else if(kacangtanah.getText().trim().equals("")){
            Valid.textKosong(kacangtanah,"Kacang Tanah");
        }else if(kedelai.getText().trim().equals("")){
            Valid.textKosong(kedelai,"Kedelai");
        }else if(terigu.getText().trim().equals("")){
            Valid.textKosong(terigu,"Terigu");
        }else if(gandum.getText().trim().equals("")){
            Valid.textKosong(gandum,"Gandum");
        }else if(cokelat.getText().trim().equals("")){
            Valid.textKosong(cokelat,"Cokelat");
        }else if(susu.getText().trim().equals("")){
            Valid.textKosong(susu,"Susu");
        }else if(jagung.getText().trim().equals("")){
            Valid.textKosong(jagung,"Jagung");
        }else if(kontrolpositif.getText().trim().equals("")){
            Valid.textKosong(kontrolpositif,"Kontrol Positif");
        }else if(kontrolnegatif.getText().trim().equals("")){
            Valid.textKosong(kontrolnegatif,"Kontrol Negatif");
        }else if(Kesimpulan.getText().trim().equals("")){
            Valid.textKosong(Kesimpulan,"Kesimpulan");
        }else{  //END CUSTOM
            if(Sequel.menyimpantf("tes_alergi","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",39,new String[]{    //CUSTOM MUHSIN 
                    TNoRw.getText(),Valid.SetTgl(TglAsuhan.getSelectedItem()+"")+" "+TglAsuhan.getSelectedItem().toString().substring(11,19),housedust.getText(),buluanjing.getText(),kapuk.getText(),buluayam.getText(),
                    cumicumi.getText(),kakap.getText(),udang.getText(),kerang.getText(),ikantawar.getText(),tongkol.getText(),pindang.getText(),bandeng.getText(),kepiting.getText(),vetsin.getText(),dagingsapi.getText(),dagingayam.getText(), 
                    dagingkambing.getText(),kuningtelur.getText(),putihtelur.getText(),kacangmete.getText(),kacanghijau.getText(),kacangtanah.getText(), 
                    kedelai.getText(),terigu.getText(),gandum.getText(),cokelat.getText(),susu.getText(),
                    "-","-","-","-","-",KdPetugas.getText(),kontrolpositif.getText(),kontrolnegatif.getText(),Kesimpulan.getText(),
                    jagung.getText()
                })==true){
                    emptTeks();
            }
        }
    
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,Kesimpulan,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(KdPetugas.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmPetugas.getText().trim().equals("")){    //START CUSTOM MUHSIN
            Valid.textKosong(BtnDokter,"Petugas");
        }else if(housedust.getText().trim().equals("")){
            Valid.textKosong(housedust,"House Dust");
        }else if(buluanjing.getText().trim().equals("")){
            Valid.textKosong(buluanjing,"Bulu Anjing");
        }else if(kapuk.getText().trim().equals("")){
            Valid.textKosong(kapuk,"Kapuk");
        }else if(buluayam.getText().trim().equals("")){
            Valid.textKosong(buluayam,"Bulu Ayam");
        }else if(cumicumi.getText().trim().equals("")){
            Valid.textKosong(cumicumi,"Cumi-cumi");
        }else if(kakap.getText().trim().equals("")){
            Valid.textKosong(kakap,"Kakap");
        }else if(udang.getText().trim().equals("")){
            Valid.textKosong(udang,"Udang");
        }else if(kerang.getText().trim().equals("")){ 
            Valid.textKosong(kerang,"Kerang");
        }else if(ikantawar.getText().trim().equals("")){
            Valid.textKosong(ikantawar,"Ikan Tawar");
        }else if(tongkol.getText().trim().equals("")){
            Valid.textKosong(tongkol,"Tongkol");
        }else if(pindang.getText().trim().equals("")){
            Valid.textKosong(pindang,"Pindang");
        }else if(bandeng.getText().trim().equals("")){
            Valid.textKosong(bandeng,"Bandeng");
        }else if(kepiting.getText().trim().equals("")){
            Valid.textKosong(kepiting,"Kepiting");
        }else if(vetsin.getText().trim().equals("")){   
            Valid.textKosong(vetsin,"Vetsin");
        }else if(dagingsapi.getText().trim().equals("")){
            Valid.textKosong(dagingsapi,"Daging Sapi");
        }else if(dagingayam.getText().trim().equals("")){
            Valid.textKosong(dagingayam,"Daging Ayam");
        }else if(dagingkambing.getText().trim().equals("")){
            Valid.textKosong(dagingkambing,"Daging Kambing");
        }else if(kuningtelur.getText().trim().equals("")){
            Valid.textKosong(kuningtelur,"Kuning Telur");
        }else if(putihtelur.getText().trim().equals("")){
            Valid.textKosong(putihtelur,"Putih Telur");
        }else if(kacangmete.getText().trim().equals("")){
            Valid.textKosong(kacangmete,"Kacang Mete");   
        }else if(kacanghijau.getText().trim().equals("")){
            Valid.textKosong(kacanghijau,"Kacang Hijau");
        }else if(kacangtanah.getText().trim().equals("")){
            Valid.textKosong(kacangtanah,"Kacang Tanah");
        }else if(kedelai.getText().trim().equals("")){
            Valid.textKosong(kedelai,"Kedelai");
        }else if(terigu.getText().trim().equals("")){
            Valid.textKosong(terigu,"Terigu");
        }else if(gandum.getText().trim().equals("")){
            Valid.textKosong(gandum,"Gandum");
        }else if(cokelat.getText().trim().equals("")){
            Valid.textKosong(cokelat,"Cokelat");
        }else if(susu.getText().trim().equals("")){
            Valid.textKosong(susu,"Susu");
        }else if(jagung.getText().trim().equals("")){
            Valid.textKosong(jagung,"Jagung");
        }else if(kontrolpositif.getText().trim().equals("")){
            Valid.textKosong(kontrolpositif,"Kontrol Positif");
        }else if(kontrolnegatif.getText().trim().equals("")){
            Valid.textKosong(kontrolnegatif,"Kontrol Negatif");
        }else if(Kesimpulan.getText().trim().equals("")){
            Valid.textKosong(Kesimpulan,"Kesimpulan");
        }else{  //END CUSTOM
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(KdPetugas.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString())){  
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh petugas yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }   
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        //CEK
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();    
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());          
            param.put("logo",Sequel.cariGambar("select logo from setting")); 
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes from gambar"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),37).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),36).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),37).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),38).toString())); 

            param.put("alamat",Sequel.cariIsi("select concat(pasien.alamat,', ',k.nm_kel,', ',k2.nm_kec,', ',k3.nm_kab) as alamat\n" +
                "from pasien\n" +
                "inner join kelurahan k on pasien.kd_kel = k.kd_kel\n" +
                "inner join kecamatan k2 on pasien.kd_kec = k2.kd_kec\n" +
                "inner join kabupaten k3 on pasien.kd_kab = k3.kd_kab\n" +
                "where no_rkm_medis=?",Sequel.cariIsi("select pasien.no_rkm_medis from pasien inner join reg_periksa rp on pasien.no_rkm_medis = rp.no_rkm_medis where rp.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'")));
            
            Valid.MyReportqry("rptCetakTesAlergi.jasper","report","::[ Laporan Tes Alergi ]::",
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.no_ktp,pasien.tgl_lahir,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,tes_alergi.house_dust,"+
                        "tes_alergi.bulu_anjing,tes_alergi.kapuk,tes_alergi.bulu_ayam,tes_alergi.cumi_cumi,tes_alergi.kakap,tes_alergi.udang,tes_alergi.kerang,"+
                        "tes_alergi.ikan_tawar,tes_alergi.tongkol,tes_alergi.pindang,tes_alergi.bandeng,tes_alergi.kepiting,tes_alergi.vetsin,tes_alergi.daging_sapi,"+    
                        "tes_alergi.daging_ayam,tes_alergi.daging_kambing,tes_alergi.kuning_telur,tes_alergi.putih_telur,tes_alergi.kacang_mete,tes_alergi.kacang_hijau,"+
                        "tes_alergi.kacang_tanah,tes_alergi.kedelai,tes_alergi.terigu,tes_alergi.gandum,tes_alergi.cokelat,tes_alergi.susu,"+
                        "tes_alergi.kopi,tes_alergi.wortel,tes_alergi.tomat,tes_alergi.nanas,tes_alergi.teh,"+
                        "petugas.nama,tes_alergi.nip,DATE_FORMAT(tes_alergi.tanggal,'%d/%m/%Y') as tanggal,tes_alergi.kontrol_positif,tes_alergi.kontrol_negatif,tes_alergi.kesimpulan "+ 
                        ",tes_alergi.jagung "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join tes_alergi on reg_periksa.no_rawat=tes_alergi.no_rawat "+
                        "inner join petugas on tes_alergi.nip=petugas.nip where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }else{
            JOptionPane.showMessageDialog(null,"Maaf, silahkan pilih data terlebih dahulu..!!!!");
        }  
//        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        if(tabMode.getRowCount()==0){
//            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//            BtnBatal.requestFocus();
//        }else if(tabMode.getRowCount()!=0){
//            try{
//                if(TCari.getText().equals("")){
//                    ps=koneksi.prepareStatement(
//                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat,penilaian_awal_keperawatan_ralan.tanggal,"+
//                            "penilaian_awal_keperawatan_ralan.informasi,penilaian_awal_keperawatan_ralan.td,penilaian_awal_keperawatan_ralan.nadi,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,"+
//                            "penilaian_awal_keperawatan_ralan.spo2,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.gcs,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,penilaian_awal_keperawatan_ralan.bmi,penilaian_awal_keperawatan_ralan.keluhan_utama,"+    //CUSTOM MUHSIN -> NADI DOUBLE, GANTI 1 JADI SPO2
//                            "penilaian_awal_keperawatan_ralan.rpd,penilaian_awal_keperawatan_ralan.rpk,penilaian_awal_keperawatan_ralan.rpo,penilaian_awal_keperawatan_ralan.alergi,penilaian_awal_keperawatan_ralan.alat_bantu,penilaian_awal_keperawatan_ralan.ket_bantu,penilaian_awal_keperawatan_ralan.prothesa,"+
//                            "penilaian_awal_keperawatan_ralan.ket_pro,penilaian_awal_keperawatan_ralan.adl,penilaian_awal_keperawatan_ralan.status_psiko,penilaian_awal_keperawatan_ralan.ket_psiko,penilaian_awal_keperawatan_ralan.hub_keluarga,penilaian_awal_keperawatan_ralan.tinggal_dengan,"+
//                            "penilaian_awal_keperawatan_ralan.ket_tinggal,penilaian_awal_keperawatan_ralan.ekonomi,penilaian_awal_keperawatan_ralan.edukasi,penilaian_awal_keperawatan_ralan.ket_edukasi,penilaian_awal_keperawatan_ralan.berjalan_a,penilaian_awal_keperawatan_ralan.berjalan_b,"+
//                            "penilaian_awal_keperawatan_ralan.berjalan_c,penilaian_awal_keperawatan_ralan.hasil,penilaian_awal_keperawatan_ralan.lapor,penilaian_awal_keperawatan_ralan.ket_lapor,penilaian_awal_keperawatan_ralan.sg1,penilaian_awal_keperawatan_ralan.nilai1,penilaian_awal_keperawatan_ralan.sg2,penilaian_awal_keperawatan_ralan.nilai2,"+
//                            "penilaian_awal_keperawatan_ralan.total_hasil,penilaian_awal_keperawatan_ralan.nyeri,penilaian_awal_keperawatan_ralan.provokes,penilaian_awal_keperawatan_ralan.ket_provokes,penilaian_awal_keperawatan_ralan.quality,penilaian_awal_keperawatan_ralan.ket_quality,penilaian_awal_keperawatan_ralan.lokasi,penilaian_awal_keperawatan_ralan.menyebar,"+
//                            "penilaian_awal_keperawatan_ralan.skala_nyeri,penilaian_awal_keperawatan_ralan.durasi,penilaian_awal_keperawatan_ralan.nyeri_hilang,penilaian_awal_keperawatan_ralan.ket_nyeri,penilaian_awal_keperawatan_ralan.pada_dokter,penilaian_awal_keperawatan_ralan.ket_dokter,penilaian_awal_keperawatan_ralan.rencana,"+
//                            "penilaian_awal_keperawatan_ralan.nip,petugas.nama,penilaian_awal_keperawatan_ralan.budaya,penilaian_awal_keperawatan_ralan.ket_budaya "+
//                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                            "inner join penilaian_awal_keperawatan_ralan on reg_periksa.no_rawat=penilaian_awal_keperawatan_ralan.no_rawat "+
//                            "inner join petugas on penilaian_awal_keperawatan_ralan.nip=petugas.nip "+
//                            "inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
//                            "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik where "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? order by penilaian_awal_keperawatan_ralan.tanggal");
//                }else{
//                    ps=koneksi.prepareStatement(
//                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,pasien.agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat,penilaian_awal_keperawatan_ralan.tanggal,"+
//                            "penilaian_awal_keperawatan_ralan.informasi,penilaian_awal_keperawatan_ralan.td,penilaian_awal_keperawatan_ralan.nadi,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,"+
//                            "penilaian_awal_keperawatan_ralan.spo2,penilaian_awal_keperawatan_ralan.rr,penilaian_awal_keperawatan_ralan.suhu,penilaian_awal_keperawatan_ralan.gcs,penilaian_awal_keperawatan_ralan.bb,penilaian_awal_keperawatan_ralan.tb,penilaian_awal_keperawatan_ralan.bmi,penilaian_awal_keperawatan_ralan.keluhan_utama,"+    //CUSTOM MUHSIN -> NADI DOUBLE, GANTI 1 JADI SPO2
//                            "penilaian_awal_keperawatan_ralan.rpd,penilaian_awal_keperawatan_ralan.rpk,penilaian_awal_keperawatan_ralan.rpo,penilaian_awal_keperawatan_ralan.alergi,penilaian_awal_keperawatan_ralan.alat_bantu,penilaian_awal_keperawatan_ralan.ket_bantu,penilaian_awal_keperawatan_ralan.prothesa,"+
//                            "penilaian_awal_keperawatan_ralan.ket_pro,penilaian_awal_keperawatan_ralan.adl,penilaian_awal_keperawatan_ralan.status_psiko,penilaian_awal_keperawatan_ralan.ket_psiko,penilaian_awal_keperawatan_ralan.hub_keluarga,penilaian_awal_keperawatan_ralan.tinggal_dengan,"+
//                            "penilaian_awal_keperawatan_ralan.ket_tinggal,penilaian_awal_keperawatan_ralan.ekonomi,penilaian_awal_keperawatan_ralan.edukasi,penilaian_awal_keperawatan_ralan.ket_edukasi,penilaian_awal_keperawatan_ralan.berjalan_a,penilaian_awal_keperawatan_ralan.berjalan_b,"+
//                            "penilaian_awal_keperawatan_ralan.berjalan_c,penilaian_awal_keperawatan_ralan.hasil,penilaian_awal_keperawatan_ralan.lapor,penilaian_awal_keperawatan_ralan.ket_lapor,penilaian_awal_keperawatan_ralan.sg1,penilaian_awal_keperawatan_ralan.nilai1,penilaian_awal_keperawatan_ralan.sg2,penilaian_awal_keperawatan_ralan.nilai2,"+
//                            "penilaian_awal_keperawatan_ralan.total_hasil,penilaian_awal_keperawatan_ralan.nyeri,penilaian_awal_keperawatan_ralan.provokes,penilaian_awal_keperawatan_ralan.ket_provokes,penilaian_awal_keperawatan_ralan.quality,penilaian_awal_keperawatan_ralan.ket_quality,penilaian_awal_keperawatan_ralan.lokasi,penilaian_awal_keperawatan_ralan.menyebar,"+
//                            "penilaian_awal_keperawatan_ralan.skala_nyeri,penilaian_awal_keperawatan_ralan.durasi,penilaian_awal_keperawatan_ralan.nyeri_hilang,penilaian_awal_keperawatan_ralan.ket_nyeri,penilaian_awal_keperawatan_ralan.pada_dokter,penilaian_awal_keperawatan_ralan.ket_dokter,penilaian_awal_keperawatan_ralan.rencana,"+
//                            "penilaian_awal_keperawatan_ralan.nip,petugas.nama,penilaian_awal_keperawatan_ralan.budaya,penilaian_awal_keperawatan_ralan.ket_budaya "+
//                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                            "inner join penilaian_awal_keperawatan_ralan on reg_periksa.no_rawat=penilaian_awal_keperawatan_ralan.no_rawat "+
//                            "inner join petugas on penilaian_awal_keperawatan_ralan.nip=petugas.nip "+
//                            "inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
//                            "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik where "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and reg_periksa.no_rawat like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and pasien.no_rkm_medis like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and pasien.nm_pasien like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and penilaian_awal_keperawatan_ralan.nip like ? or "+
//                            "penilaian_awal_keperawatan_ralan.tanggal between ? and ? and petugas.nama like ? order by penilaian_awal_keperawatan_ralan.tanggal");
//                }
//
//                try {
//                    if(TCari.getText().equals("")){
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                    }else{
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(3,"%"+TCari.getText()+"%");
//                        ps.setString(4,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(5,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(6,"%"+TCari.getText()+"%");
//                        ps.setString(7,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(8,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(9,"%"+TCari.getText()+"%");
//                        ps.setString(10,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(11,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(12,"%"+TCari.getText()+"%");
//                        ps.setString(13,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(14,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(15,"%"+TCari.getText()+"%");
//                    }   
//                    rs=ps.executeQuery();
//                    htmlContent = new StringBuilder();
//                    htmlContent.append(                             
//                        "<tr class='isi'>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='9%'><b>PASIEN & PETUGAS</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='5%'><b>I. KEADAAN UMUM</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='5%'><b>II. STATUS NUTRISI</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='13%'><b>III. RIWAYAT KESEHATAN</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='8%'><b>IV. FUNGSIONAL</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='16%'><b>V. RIWAYAT PSIKO-SOSIAL SPIRITUAL DAN BUDAYA</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='16%'><b>VI. PENILAIAN RESIKO JATUH</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='11%'><b>VII. SKRINING GIZI</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='11%'><b>VIII. PENILAIAN TINGKAT NYERI</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='6%'><b>MASALAH & RENCANA KEPERAWATAN</b></td>"+
//                        "</tr>"
//                    );
//                    while(rs.next()){
//                        //masalahkeperawatan="";
//                        ps2=koneksi.prepareStatement(
//                            "select master_masalah_keperawatan.kode_masalah,master_masalah_keperawatan.nama_masalah from master_masalah_keperawatan "+
//                            "inner join penilaian_awal_keperawatan_ralan_masalah on penilaian_awal_keperawatan_ralan_masalah.kode_masalah=master_masalah_keperawatan.kode_masalah "+
//                            "where penilaian_awal_keperawatan_ralan_masalah.no_rawat=? order by kode_masalah");
//                        try {
//                            ps2.setString(1,rs.getString("no_rawat"));
//                            rs2=ps2.executeQuery();
//                            while(rs2.next()){
////                                masalahkeperawatan=rs2.getString("nama_masalah")+", "+masalahkeperawatan;
//                            }
//                        } catch (Exception e) {
//                            System.out.println("Notif : "+e);
//                        } finally{
//                            if(rs2!=null){
//                                rs2.close();
//                            }
//                            if(ps2!=null){
//                                ps2.close();
//                            }
//                        }
//                        htmlContent.append(
//                            "<tr class='isi'>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>No.Rawat</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("no_rawat")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>No.R.M.</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("no_rkm_medis")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Nama Pasien</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nm_pasien")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>J.K.</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("jk")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Agama</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("agama")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Bahasa</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nama_bahasa")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Tgl.Lahir</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nama_cacat")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Cacat Fisik</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("tgl_lahir")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Petugas</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("nip")+" "+rs.getString("nama")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Tgl.Asuhan</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("tanggal")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Informasi</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("informasi")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>TD</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("td")+"mmHg</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>Nadi</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("nadi")+"x/menit</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>RR</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("rr")+"x/menit</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>Suhu</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("suhu")+"°C</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>GCS</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("gcs")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>BB</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("bb")+"Kg</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>TB</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("tb")+"cm</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='34%' valign='top'>BMI</td><td valign='top'>:&nbsp;</td><td width='65%' valign='top'>"+rs.getString("bmi")+"Kg/m²</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Keluhan Utama</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("keluhan_utama")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>RPD</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("rpd")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>RPK</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("rpk")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>RPO</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("rpo")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='32%' valign='top'>Alergi</td><td valign='top'>:&nbsp;</td><td width='67%' valign='top'>"+rs.getString("alergi")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Alat Bantu</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("alat_bantu")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Alat Bantu</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_bantu")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Prothesa</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("prothesa")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Prothesa</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_pro")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>ADL</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("adl")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Status Psikologis</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("status_psiko")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Psikologi</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_psiko")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Hubungan pasien dengan anggota keluarga</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("hub_keluarga")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Tinggal dengan</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("tinggal_dengan")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Tinggal</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_tinggal")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ekonomi</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ekonomi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Kepercayaan / Budaya / Nilai-nilai khusus yang perlu diperhatikan</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("budaya")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Budaya</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_budaya")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Edukasi diberikan kepada </td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("edukasi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Ket. Edukasi</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_edukasi")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Tidak seimbang/sempoyongan/limbung</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("berjalan_a")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Jalan dengan menggunakan alat bantu (kruk, tripot, kursi roda, orang lain)</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("berjalan_b")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Menopang saat akan duduk, tampak memegang pinggiran kursi atau meja/benda lain sebagai penopang</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("berjalan_c")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Hasil</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("hasil")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Dilaporan ke dokter?</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("lapor")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Jam Lapor</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("ket_lapor")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Apakah ada penurunan berat badanyang tidak diinginkan selama enam bulan terakhir?</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("sg1")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Apakah nafsu makan berkurang karena tidak nafsu makan?</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("sg2")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Nilai 1</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("nilai1")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Nilai 2</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("nilai2")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='64%' valign='top'>Total Skor</td><td valign='top'>:&nbsp;</td><td width='35%' valign='top'>"+rs.getString("total_hasil")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
//                                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'align='center'>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Tingkat Nyeri</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("nyeri")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Provokes</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("provokes")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Provokes</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_provokes")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Kualitas</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("quality")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Kualitas</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_quality")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Lokas</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("lokasi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Menyebar</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("menyebar")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Skala Nyeri</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("skala_nyeri")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Durasi</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("durasi")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Nyeri Hilang</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("nyeri_hilang")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Ket. Hilang Nyeri</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_nyeri")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Lapor Ke Dokter</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("pada_dokter")+"</td>"+
//                                        "</tr>"+
//                                        "<tr class='isi2'>"+
//                                            "<td width='44%' valign='top'>Jam Lapor</td><td valign='top'>:&nbsp;</td><td width='55%' valign='top'>"+rs.getString("ket_dokter")+"</td>"+
//                                        "</tr>"+
//                                    "</table>"+
//                                "</td>"+
//                                "<td valign='top' cellpadding='0' cellspacing='0'>"+
////                                    "Masalah Keperawatan : "+masalahkeperawatan+"<br><br>"+
////                                    "Rencana Keperawatan : "+rs.getString("rencana")+
//                                "</td>"+
//                            "</tr>"
//                        );
//                    }
//                    LoadHTML.setText(
//                        "<html>"+
//                          "<table width='1800px' border='0' align='center' cellpadding='1px' cellspacing='0' class='tbl_form'>"+
//                           htmlContent.toString()+
//                          "</table>"+
//                        "</html>"
//                    );
//
//                    File g = new File("file2.css");            
//                    BufferedWriter bg = new BufferedWriter(new FileWriter(g));
//                    bg.write(
//                        ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
//                        ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
//                        ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
//                        ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
//                        ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
//                        ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
//                    );
//                    bg.close();
//
//                    File f = new File("DataPenilaianAwalKeperawatanRalan.html");            
//                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));            
//                    bw.write(LoadHTML.getText().replaceAll("<head>","<head>"+
//                                "<link href=\"file2.css\" rel=\"stylesheet\" type=\"text/css\" />"+
//                                "<table width='1800px' border='0' align='center' cellpadding='3px' cellspacing='0' class='tbl_form'>"+
//                                    "<tr class='isi2'>"+
//                                        "<td valign='top' align='center'>"+
//                                            "<font size='4' face='Tahoma'>"+akses.getnamars()+"</font><br>"+
//                                            akses.getalamatrs()+", "+akses.getkabupatenrs()+", "+akses.getpropinsirs()+"<br>"+
//                                            akses.getkontakrs()+", E-mail : "+akses.getemailrs()+"<br><br>"+
//                                            "<font size='2' face='Tahoma'>DATA PENILAIAN AWAL KEPERAWATAN RAWAT JALAN<br><br></font>"+        
//                                        "</td>"+
//                                   "</tr>"+
//                                "</table>")
//                    );
//                    bw.close();                         
//                    Desktop.getDesktop().browse(f.toURI());
//                } catch (Exception e) {
//                    System.out.println("Notif : "+e);
//                } finally{
//                    if(rs!=null){
//                        rs.close();
//                    }
//                    if(ps!=null){
//                        ps.close();
//                    }
//                }
//
//            }catch(Exception e){
//                System.out.println("Notifikasi : "+e);
//            }
//        }
//        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                ChkAccor.setSelected(true);
                isMenu();
                getData2();
            } catch (java.lang.NullPointerException e) {
            }
            if(evt.getClickCount()==2){
                getData();
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    ChkAccor.setSelected(true);
                    isMenu();
                    getData2();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened

    }//GEN-LAST:event_formWindowOpened

    private void ChkAccorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccorActionPerformed
        if(tbObat.getSelectedRow()!= -1){
            isMenu();
        }else{
            ChkAccor.setSelected(false);
            JOptionPane.showMessageDialog(null,"Maaf, silahkan pilih data yang mau ditampilkan...!!!!");
        }
    }//GEN-LAST:event_ChkAccorActionPerformed

    private void BtnPrint1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrint1ActionPerformed
        //CEK
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();    
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());          
            param.put("logo",Sequel.cariGambar("select logo from setting")); 
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes from gambar"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),37).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),36).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),37).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),38).toString())); 

            param.put("alamat",Sequel.cariIsi("select concat(pasien.alamat,', ',k.nm_kel,', ',k2.nm_kec,', ',k3.nm_kab) as alamat\n" +
                "from pasien\n" +
                "inner join kelurahan k on pasien.kd_kel = k.kd_kel\n" +
                "inner join kecamatan k2 on pasien.kd_kec = k2.kd_kec\n" +
                "inner join kabupaten k3 on pasien.kd_kab = k3.kd_kab\n" +
                "where no_rkm_medis=?",Sequel.cariIsi("select pasien.no_rkm_medis from pasien inner join reg_periksa rp on pasien.no_rkm_medis = rp.no_rkm_medis where rp.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'")));
            
            Valid.MyReportqry("rptCetakTesAlergi.jasper","report","::[ Laporan Tes Alergi ]::",
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.no_ktp,pasien.tgl_lahir,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,tes_alergi.house_dust,"+
                        "tes_alergi.bulu_anjing,tes_alergi.kapuk,tes_alergi.bulu_ayam,tes_alergi.cumi_cumi,tes_alergi.kakap,tes_alergi.udang,tes_alergi.kerang,"+
                        "tes_alergi.ikan_tawar,tes_alergi.tongkol,tes_alergi.pindang,tes_alergi.bandeng,tes_alergi.kepiting,tes_alergi.vetsin,tes_alergi.daging_sapi,"+    
                        "tes_alergi.daging_ayam,tes_alergi.daging_kambing,tes_alergi.kuning_telur,tes_alergi.putih_telur,tes_alergi.kacang_mete,tes_alergi.kacang_hijau,"+
                        "tes_alergi.kacang_tanah,tes_alergi.kedelai,tes_alergi.terigu,tes_alergi.gandum,tes_alergi.cokelat,tes_alergi.susu,"+
                        "tes_alergi.kopi,tes_alergi.wortel,tes_alergi.tomat,tes_alergi.nanas,tes_alergi.teh,"+
                        "petugas.nama,tes_alergi.nip,DATE_FORMAT(tes_alergi.tanggal,'%d/%m/%Y') as tanggal,tes_alergi.kontrol_positif,tes_alergi.kontrol_negatif,tes_alergi.kesimpulan "+ 
                        ",tes_alergi.jagung "+        
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join tes_alergi on reg_periksa.no_rawat=tes_alergi.no_rawat "+
                        "inner join petugas on tes_alergi.nip=petugas.nip where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }else{
            JOptionPane.showMessageDialog(null,"Maaf, silahkan pilih data terlebih dahulu..!!!!");
        }  
    }//GEN-LAST:event_BtnPrint1ActionPerformed

    private void kerangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kerangKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,udang,ikantawar); //CUSTOM MUHSIN
    }//GEN-LAST:event_kerangKeyPressed

    private void TglAsuhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglAsuhanKeyPressed
        Valid.pindah(evt,NmPetugas,housedust);
    }//GEN-LAST:event_TglAsuhanKeyPressed

    private void udangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_udangKeyPressed
        //Valid.pindah(evt,TB,KeluhanUtama);
        Valid.pindah(evt,kakap,kerang);  //CUSTOM MUHSIN
    }//GEN-LAST:event_udangKeyPressed

    private void kapukKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kapukKeyPressed
        Valid.pindah(evt,buluanjing,buluayam);
    }//GEN-LAST:event_kapukKeyPressed

    private void housedustKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_housedustKeyPressed
        Valid.pindah(evt,TglAsuhan,buluanjing);
    }//GEN-LAST:event_housedustKeyPressed

    private void buluayamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_buluayamKeyPressed
        Valid.pindah(evt,kapuk,cumicumi);
    }//GEN-LAST:event_buluayamKeyPressed

    private void buluanjingKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_buluanjingKeyPressed
        Valid.pindah(evt,housedust,kapuk);
    }//GEN-LAST:event_buluanjingKeyPressed

    private void kakapKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kakapKeyPressed
        Valid.pindah(evt,cumicumi,udang);
    }//GEN-LAST:event_kakapKeyPressed

    private void cumicumiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cumicumiKeyPressed
        Valid.pindah(evt,buluayam,kakap);
    }//GEN-LAST:event_cumicumiKeyPressed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        Valid.pindah(evt,NmPetugas,TglAsuhan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed

    }//GEN-LAST:event_KdPetugasKeyPressed

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{
            Valid.pindah(evt,TCari,BtnDokter);
        }
    }//GEN-LAST:event_TNoRwKeyPressed

    private void ikantawarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ikantawarKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,kerang,tongkol);
    }//GEN-LAST:event_ikantawarKeyPressed

    private void tongkolKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tongkolKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,ikantawar,pindang);
    }//GEN-LAST:event_tongkolKeyPressed

    private void pindangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pindangKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,tongkol,bandeng);
    }//GEN-LAST:event_pindangKeyPressed

    private void bandengKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bandengKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,pindang,kepiting);
    }//GEN-LAST:event_bandengKeyPressed

    private void kepitingKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kepitingKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,bandeng,vetsin);
    }//GEN-LAST:event_kepitingKeyPressed

    private void vetsinKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_vetsinKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,kepiting,dagingsapi);
    }//GEN-LAST:event_vetsinKeyPressed

    private void dagingsapiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dagingsapiKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,vetsin,dagingayam);
    }//GEN-LAST:event_dagingsapiKeyPressed

    private void dagingayamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dagingayamKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,dagingsapi,dagingkambing);
    }//GEN-LAST:event_dagingayamKeyPressed

    private void dagingkambingKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dagingkambingKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,dagingayam,kuningtelur);
    }//GEN-LAST:event_dagingkambingKeyPressed

    private void putihtelurKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_putihtelurKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,kuningtelur,kacangmete);
    }//GEN-LAST:event_putihtelurKeyPressed

    private void kuningtelurKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kuningtelurKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,dagingkambing,putihtelur);
    }//GEN-LAST:event_kuningtelurKeyPressed

    private void kacangmeteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kacangmeteKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,putihtelur,kacanghijau);
    }//GEN-LAST:event_kacangmeteKeyPressed

    private void teriguKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_teriguKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,kedelai,gandum);
    }//GEN-LAST:event_teriguKeyPressed

    private void kacanghijauKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kacanghijauKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,kacangmete,kacangtanah);
    }//GEN-LAST:event_kacanghijauKeyPressed

    private void kedelaiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kedelaiKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,kacangtanah,terigu);
    }//GEN-LAST:event_kedelaiKeyPressed

    private void kacangtanahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kacangtanahKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,kacanghijau,kedelai);
    }//GEN-LAST:event_kacangtanahKeyPressed

    private void cokelatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cokelatKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,gandum,susu);
    }//GEN-LAST:event_cokelatKeyPressed

    private void gandumKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_gandumKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,terigu,cokelat);
    }//GEN-LAST:event_gandumKeyPressed

    private void susuKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_susuKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,cokelat,jagung);
    }//GEN-LAST:event_susuKeyPressed

    private void kontrolpositifKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kontrolpositifKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,jagung,kontrolnegatif);
    }//GEN-LAST:event_kontrolpositifKeyPressed

    private void kontrolnegatifKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kontrolnegatifKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,kontrolpositif,Kesimpulan);
    }//GEN-LAST:event_kontrolnegatifKeyPressed

    private void KesimpulanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KesimpulanKeyPressed
        Valid.pindah(evt,kontrolnegatif,BtnSimpan);
    }//GEN-LAST:event_KesimpulanKeyPressed

    private void jagungKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jagungKeyPressed
        // TODO add your handling code here:
        Valid.pindah(evt,susu,kontrolpositif);
    }//GEN-LAST:event_jagungKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMPenilaianAwalKeperawatanRalan dialog = new RMPenilaianAwalKeperawatanRalan(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnPrint1;
    private widget.Button BtnSimpan;
    private widget.CekBox ChkAccor;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.PanelBiasa FormInput;
    private widget.PanelBiasa FormMenu;
    private widget.TextBox Jk;
    private widget.TextBox KdPetugas;
    private widget.TextArea Kesimpulan;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private widget.TextBox NmPetugas;
    private widget.PanelBiasa PanelAccor;
    private widget.ScrollPane Scroll;
    private widget.ScrollPane Scroll6;
    private widget.ScrollPane Scroll8;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRM1;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.TextBox TPasien1;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal TglAsuhan;
    private widget.TextBox TglLahir;
    private widget.TextBox bandeng;
    private widget.TextBox buluanjing;
    private widget.TextBox buluayam;
    private widget.TextBox cokelat;
    private widget.TextBox cumicumi;
    private widget.TextBox dagingayam;
    private widget.TextBox dagingkambing;
    private widget.TextBox dagingsapi;
    private widget.TextBox gandum;
    private widget.TextBox housedust;
    private widget.TextBox ikantawar;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel11;
    private widget.Label jLabel12;
    private widget.Label jLabel13;
    private widget.Label jLabel14;
    private widget.Label jLabel15;
    private widget.Label jLabel16;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel48;
    private widget.Label jLabel49;
    private widget.Label jLabel50;
    private widget.Label jLabel51;
    private widget.Label jLabel52;
    private widget.Label jLabel53;
    private widget.Label jLabel54;
    private widget.Label jLabel55;
    private widget.Label jLabel56;
    private widget.Label jLabel57;
    private widget.Label jLabel58;
    private widget.Label jLabel59;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel61;
    private widget.Label jLabel62;
    private widget.Label jLabel63;
    private widget.Label jLabel64;
    private widget.Label jLabel65;
    private widget.Label jLabel66;
    private widget.Label jLabel67;
    private widget.Label jLabel68;
    private widget.Label jLabel69;
    private widget.Label jLabel7;
    private widget.Label jLabel70;
    private widget.Label jLabel71;
    private widget.Label jLabel72;
    private widget.Label jLabel73;
    private widget.Label jLabel78;
    private widget.Label jLabel8;
    private widget.Label jLabel80;
    private widget.Label jLabel84;
    private widget.Label jLabel85;
    private widget.Label jLabel86;
    private widget.Label jLabel93;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private widget.TextBox jagung;
    private widget.TextBox kacanghijau;
    private widget.TextBox kacangmete;
    private widget.TextBox kacangtanah;
    private widget.TextBox kakap;
    private widget.TextBox kapuk;
    private widget.TextBox kedelai;
    private widget.TextBox kepiting;
    private widget.TextBox kerang;
    private widget.TextBox kontrolnegatif;
    private widget.TextBox kontrolpositif;
    private widget.TextBox kuningtelur;
    private widget.Label label11;
    private widget.Label label14;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.TextBox pindang;
    private widget.TextBox putihtelur;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane1;
    private widget.TextBox susu;
    private widget.Table tbIntervensiResikoJatuh;
    private widget.Table tbMasalahKeperawatan;
    private widget.Table tbObat;
    private widget.TextBox terigu;
    private widget.TextBox tongkol;
    private widget.TextBox udang;
    private widget.TextBox vetsin;
    // End of variables declaration//GEN-END:variables

    private void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().equals("")){
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,tes_alergi.house_dust,"+
                        "tes_alergi.bulu_anjing,tes_alergi.kapuk,tes_alergi.bulu_ayam,tes_alergi.cumi_cumi,tes_alergi.kakap,tes_alergi.udang,tes_alergi.kerang,"+
                        "tes_alergi.ikan_tawar,tes_alergi.tongkol,tes_alergi.pindang,tes_alergi.bandeng,tes_alergi.kepiting,tes_alergi.vetsin,tes_alergi.daging_sapi,"+    
                        "tes_alergi.daging_ayam,tes_alergi.daging_ayam,tes_alergi.daging_kambing,tes_alergi.kuning_telur,tes_alergi.putih_telur,tes_alergi.kacang_mete,tes_alergi.kacang_hijau,"+
                        "tes_alergi.kacang_tanah,tes_alergi.kedelai,tes_alergi.terigu,tes_alergi.gandum,tes_alergi.cokelat,tes_alergi.susu,"+
                        "tes_alergi.susu,tes_alergi.kopi,tes_alergi.wortel,tes_alergi.tomat,tes_alergi.nanas,tes_alergi.teh,"+
                        "petugas.nama,tes_alergi.nip,tes_alergi.tanggal,tes_alergi.kontrol_positif,tes_alergi.kontrol_negatif,tes_alergi.kesimpulan,tes_alergi.jagung "+ 
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join tes_alergi on reg_periksa.no_rawat=tes_alergi.no_rawat "+
                        "inner join petugas on tes_alergi.nip=petugas.nip "+
                        "where tes_alergi.tanggal between ? and ? order by tes_alergi.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,tes_alergi.house_dust,"+
                        "tes_alergi.bulu_anjing,tes_alergi.kapuk,tes_alergi.bulu_ayam,tes_alergi.cumi_cumi,tes_alergi.kakap,tes_alergi.udang,tes_alergi.kerang,"+
                        "tes_alergi.ikan_tawar,tes_alergi.tongkol,tes_alergi.pindang,tes_alergi.bandeng,tes_alergi.kepiting,tes_alergi.vetsin,tes_alergi.daging_sapi,"+    
                        "tes_alergi.daging_ayam,tes_alergi.daging_ayam,tes_alergi.daging_kambing,tes_alergi.kuning_telur,tes_alergi.putih_telur,tes_alergi.kacang_mete,tes_alergi.kacang_hijau,"+
                        "tes_alergi.kacang_tanah,tes_alergi.kedelai,tes_alergi.terigu,tes_alergi.gandum,tes_alergi.cokelat,tes_alergi.susu,"+
                        "tes_alergi.susu,tes_alergi.kopi,tes_alergi.wortel,tes_alergi.tomat,tes_alergi.nanas,tes_alergi.teh,"+
                        "petugas.nama,tes_alergi.nip,tes_alergi.tanggal,tes_alergi.kontrol_positif,tes_alergi.kontrol_negatif,tes_alergi.kesimpulan,tes_alergi.jagung "+ 
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join tes_alergi on reg_periksa.no_rawat=tes_alergi.no_rawat "+
                        "inner join petugas on tes_alergi.nip=petugas.nip "+
                        "where tes_alergi.tanggal between ? and ? and reg_periksa.no_rawat like ? or "+
                        "tes_alergi.tanggal between ? and ? and pasien.no_rkm_medis like ? or "+
                        "tes_alergi.tanggal between ? and ? and pasien.nm_pasien like ? or "+
                        "tes_alergi.tanggal between ? and ? and tes_alergi.nip like ? or "+
                        "tes_alergi.tanggal between ? and ? and petugas.nama like ? order by tes_alergi.tanggal");
            }
                
            try {
                if(TCari.getText().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(5,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(8,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(11,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(12,"%"+TCari.getText()+"%");
                    ps.setString(13,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(14,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(15,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("jk"),
                        rs.getString("house_dust"),rs.getString("bulu_anjing"),rs.getString("kapuk"),rs.getString("bulu_ayam"),rs.getString("cumi_cumi"),rs.getString("kakap"),rs.getString("udang"),
                        rs.getString("kerang"),rs.getString("ikan_tawar"),rs.getString("tongkol"),rs.getString("pindang"),rs.getString("bandeng"),rs.getString("kepiting"),rs.getString("vetsin"),rs.getString("daging_sapi"),
                        rs.getString("daging_ayam"),rs.getString("daging_kambing"),rs.getString("kuning_telur"),rs.getString("putih_telur"),rs.getString("kacang_mete"),rs.getString("kacang_hijau"),rs.getString("kacang_tanah"),
                        rs.getString("kedelai"),rs.getString("terigu"),rs.getString("gandum"),rs.getString("cokelat"),rs.getString("susu"),rs.getString("kopi"),
                        rs.getString("wortel"),rs.getString("tomat"),rs.getString("nanas"),rs.getString("teh"),rs.getString("nama"),rs.getString("nip"),rs.getString("tanggal"),rs.getString("kontrol_positif"),rs.getString("kontrol_negatif"),rs.getString("kesimpulan"),
                        rs.getString("jagung")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TglAsuhan.setDate(new Date());
        housedust.setText("");
        buluanjing.setText("");
        kapuk.setText("");
        buluayam.setText("");
        cumicumi.setText("");
        kakap.setText("");
        udang.setText("");
        kerang.setText("");
        ikantawar.setText("");
        tongkol.setText("");
        pindang.setText("");
        bandeng.setText("");
        kepiting.setText("");
        vetsin.setText("");
        dagingsapi.setText("");
        dagingayam.setText("");
        dagingkambing.setText("");
        kuningtelur.setText("");
        putihtelur.setText("");
        kacangmete.setText("");
        kacanghijau.setText("");
        kacangtanah.setText("");
        kedelai.setText("");
        terigu.setText("");
        gandum.setText("");
        cokelat.setText("");
        susu.setText("");
        jagung.setText("");
//        kopi.setText("");
//        wortel.setText("");
//        tomat.setText("");
//        nanas.setText("");
//        teh.setText("");
        kontrolpositif.setText("");
        kontrolnegatif.setText("");
        Kesimpulan.setText("Dikatakan alergi jika diameter lebih besar atau sama dengan 3 mm");
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()); 
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString()); 
            housedust.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());
            buluanjing.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            kapuk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            buluayam.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString()); 
            cumicumi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            kakap.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            udang.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            kerang.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            ikantawar.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            tongkol.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            pindang.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            bandeng.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            kepiting.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            vetsin.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            dagingsapi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            dagingayam.setText(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            dagingkambing.setText(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            kuningtelur.setText(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            putihtelur.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            kacangmete.setText(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            kacanghijau.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            kacangtanah.setText(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            kedelai.setText(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            terigu.setText(tbObat.getValueAt(tbObat.getSelectedRow(),27).toString());
            gandum.setText(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            cokelat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            susu.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
//            kopi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
//            wortel.setText(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());
//            tomat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString());
//            nanas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());
//            teh.setText(tbObat.getValueAt(tbObat.getSelectedRow(),35).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),36).toString());
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString());
            Valid.SetTgl2(TglAsuhan,tbObat.getValueAt(tbObat.getSelectedRow(),38).toString());
            kontrolpositif.setText(tbObat.getValueAt(tbObat.getSelectedRow(),39).toString());
            kontrolnegatif.setText(tbObat.getValueAt(tbObat.getSelectedRow(),40).toString());
            Kesimpulan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),41).toString());
            jagung.setText(tbObat.getValueAt(tbObat.getSelectedRow(),42).toString());
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,"+
                    "pasien.tgl_lahir,pasien.agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
                    "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    public void setNoRm(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);    
        isRawat();
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.gettindakan_ralan());
        BtnHapus.setEnabled(akses.gettindakan_ralan());
        BtnEdit.setEnabled(akses.gettindakan_ralan());
        if(akses.getjml2()>=1){
            KdPetugas.setEditable(false);
            BtnDokter.setEnabled(false);
            KdPetugas.setText(akses.getkode());
            Sequel.cariIsi("select nama from petugas where nip=?", NmPetugas,KdPetugas.getText());
            if(NmPetugas.getText().equals("")){
                KdPetugas.setText("");
                JOptionPane.showMessageDialog(null,"User login bukan petugas...!!");
            }
        }            
    }

    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }
    
    private void isMenu(){
        if(ChkAccor.isSelected()==true){
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(470,HEIGHT));
            FormMenu.setVisible(true);  
            ChkAccor.setVisible(true);
        }else if(ChkAccor.isSelected()==false){   
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(15,HEIGHT));
            FormMenu.setVisible(false);  
            ChkAccor.setVisible(true);
        }
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from tes_alergi where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            TNoRM1.setText("");
            TPasien1.setText("");
            ChkAccor.setSelected(false);
            isMenu();
            tampil();
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("tes_alergi","no_rawat=?","no_rawat=?,tanggal=?,house_dust=?,bulu_anjing=?,kapuk=?,bulu_ayam=?,cumi_cumi=?,kakap=?,udang=?,kerang=?,ikan_tawar=?,tongkol=?,pindang=?,bandeng=?,kepiting=?,vetsin=?,daging_sapi=?,daging_ayam=?,daging_kambing=?,kuning_telur=?,putih_telur=?,kacang_mete=?,kacang_hijau=?,kacang_tanah=?,kedelai=?,terigu=?,gandum=?,cokelat=?,susu=?,kopi=?,wortel=?,tomat=?,nanas=?,teh=?,nip=?,kontrol_positif=?,kontrol_negatif=?,kesimpulan=?,jagung=?",40,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglAsuhan.getSelectedItem()+"")+" "+TglAsuhan.getSelectedItem().toString().substring(11,19),housedust.getText(),buluanjing.getText(),kapuk.getText(),
                buluayam.getText(),cumicumi.getText(),kakap.getText(),udang.getText(),kerang.getText(),ikantawar.getText(),tongkol.getText(),pindang.getText(),bandeng.getText(),kepiting.getText(),vetsin.getText(), 
                dagingsapi.getText(),dagingayam.getText(),dagingkambing.getText(),kuningtelur.getText(),putihtelur.getText(),kacangmete.getText(), 
                kacanghijau.getText(),kacangtanah.getText(),kedelai.getText(),terigu.getText(),gandum.getText(),cokelat.getText(),
                susu.getText(),"-","-","-","-","-",KdPetugas.getText(),kontrolpositif.getText(),kontrolnegatif.getText(),Kesimpulan.getText(),jagung.getText(),tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
             })==true){
                emptTeks();
                TabRawat.setSelectedIndex(1);
                tampil();
        }
    }
    
    private void getData2() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRM1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
        }
    }
}
