/*
 * Kontribusi dari Anggit Nurhidayah, RSUP Surakarta
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;

/**
 *
 * @author perpustakaan
 */
public final class RMLainSkoringCURB extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private StringBuilder htmlContent;
    private String finger="";
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMLainSkoringCURB(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tgl.Lahir","J.K.","NIP Dokter","Nama Dokter","Tanggal","R1","R3","R3","R4","R5","R6","R7","R8","R9","R10","Total Respon",
            "C","U","R","B","U65","Total","Level Resiko","Perawatan Disarankan"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        
        tbObat.setModel(tabMode);
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 27; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(150);
            }else if(i==3){
                column.setPreferredWidth(65);
            }else if(i==4){
                column.setPreferredWidth(55);
            }else if(i==5){
                column.setPreferredWidth(80);
            }else if(i==6){
                column.setPreferredWidth(150);
            }else if(i==7){
                column.setPreferredWidth(115);
            }else if(i==8){
                column.setPreferredWidth(50);
            }else if(i==9){
                column.setPreferredWidth(50);
            }else if(i==10){
                column.setPreferredWidth(50);
            }else if(i==11){
                column.setPreferredWidth(50);
            }else if(i==12){
                column.setPreferredWidth(50);
            }else if(i==13){
                column.setPreferredWidth(50);
            }else if(i==14){
                column.setPreferredWidth(50);
            }else if(i==15){
                column.setPreferredWidth(50);
            }else if(i==16){
                column.setPreferredWidth(50);
            }else if(i==17){
                column.setPreferredWidth(50);
            }else if(i==18){
                column.setPreferredWidth(50);
            }else if(i==19){
                column.setPreferredWidth(50);
            }else if(i==20){
                column.setPreferredWidth(50);
            }else if(i==21){
                column.setPreferredWidth(50);
            }else if(i==22){
                column.setPreferredWidth(50);
            }else if(i==23){
                column.setPreferredWidth(50);
            }else if(i==24){
                column.setPreferredWidth(80);
            }else if(i==25){
                column.setPreferredWidth(200);
            }else if(i==26){
                column.setPreferredWidth(350);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){
                    KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                    KdDokter.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });

      
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnCetakMCUPsikiatri = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        KdDokter = new widget.TextBox();
        NmDokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        jLabel11 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        label11 = new widget.Label();
        TglPemeriksaan = new widget.Tanggal();
        label15 = new widget.Label();
        jLabel97 = new widget.Label();
        jLabel102 = new widget.Label();
        jLabel103 = new widget.Label();
        jLabel104 = new widget.Label();
        jLabel106 = new widget.Label();
        jLabel99 = new widget.Label();
        c2 = new widget.ComboBox();
        c3 = new widget.ComboBox();
        c4 = new widget.ComboBox();
        c1_ket = new widget.TextBox();
        c2_ket = new widget.TextBox();
        c3_ket = new widget.TextBox();
        c4_ket = new widget.TextBox();
        jLabel98 = new widget.Label();
        jLabel100 = new widget.Label();
        total = new widget.TextBox();
        jLabel107 = new widget.Label();
        jLabel108 = new widget.Label();
        jLabel109 = new widget.Label();
        c5_ket = new widget.TextBox();
        level = new widget.TextBox();
        jScrollPane5 = new javax.swing.JScrollPane();
        perawatanyangdisarankan = new javax.swing.JTextArea();
        c5 = new widget.ComboBox();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel105 = new widget.Label();
        r10 = new widget.ComboBox();
        jLabel110 = new widget.Label();
        jLabel111 = new widget.Label();
        jLabel112 = new widget.Label();
        jLabel113 = new widget.Label();
        r1 = new widget.ComboBox();
        r2 = new widget.ComboBox();
        jLabel114 = new widget.Label();
        jLabel115 = new widget.Label();
        jLabel116 = new widget.Label();
        jLabel117 = new widget.Label();
        jLabel118 = new widget.Label();
        jLabel119 = new widget.Label();
        jLabel120 = new widget.Label();
        r3 = new widget.ComboBox();
        r4 = new widget.ComboBox();
        r5 = new widget.ComboBox();
        r6 = new widget.ComboBox();
        r7 = new widget.ComboBox();
        r8 = new widget.ComboBox();
        r9 = new widget.ComboBox();
        jLabel121 = new widget.Label();
        totalrespon = new widget.TextBox();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnCetakMCUPsikiatri.setBackground(new java.awt.Color(255, 255, 254));
        MnCetakMCUPsikiatri.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakMCUPsikiatri.setForeground(new java.awt.Color(50, 50, 50));
        MnCetakMCUPsikiatri.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetakMCUPsikiatri.setText("Cetak Skoring CURB 65");
        MnCetakMCUPsikiatri.setActionCommand("Cetak Skoring CURB65");
        MnCetakMCUPsikiatri.setName("MnCetakMCUPsikiatri"); // NOI18N
        MnCetakMCUPsikiatri.setPreferredSize(new java.awt.Dimension(220, 26));
        MnCetakMCUPsikiatri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakMCUPsikiatriActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakMCUPsikiatri);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Penilaian Scoring CURB ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(920, 700));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        KdDokter.setEditable(false);
        KdDokter.setName("KdDokter"); // NOI18N
        KdDokter.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokterKeyPressed(evt);
            }
        });
        FormInput.add(KdDokter);
        KdDokter.setBounds(74, 40, 90, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        NmDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmDokter);
        NmDokter.setBounds(166, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(348, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(640, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 81, 880, 2);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(380, 40, 52, 23);

        TglPemeriksaan.setForeground(new java.awt.Color(50, 70, 50));
        TglPemeriksaan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "29-07-2024 08:07:34" }));
        TglPemeriksaan.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglPemeriksaan.setName("TglPemeriksaan"); // NOI18N
        TglPemeriksaan.setOpaque(false);
        TglPemeriksaan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglPemeriksaanKeyPressed(evt);
            }
        });
        FormInput.add(TglPemeriksaan);
        TglPemeriksaan.setBounds(436, 40, 130, 23);

        label15.setText("Dokter :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label15);
        label15.setBounds(0, 40, 70, 23);

        jLabel97.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel97.setText("SKOR");
        jLabel97.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel97.setName("jLabel97"); // NOI18N
        FormInput.add(jLabel97);
        jLabel97.setBounds(470, 340, 30, 30);

        jLabel102.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel102.setText("2. U BLOOD UREA NITROGEN");
        jLabel102.setName("jLabel102"); // NOI18N
        FormInput.add(jLabel102);
        jLabel102.setBounds(30, 400, 150, 30);

        jLabel103.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel103.setText("3. R RESPIRATORY RATE ");
        jLabel103.setName("jLabel103"); // NOI18N
        FormInput.add(jLabel103);
        jLabel103.setBounds(30, 430, 140, 30);

        jLabel104.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel104.setText("4. B SYSTOLIC BLOOD PRESSUR");
        jLabel104.setName("jLabel104"); // NOI18N
        FormInput.add(jLabel104);
        jLabel104.setBounds(30, 460, 200, 30);

        jLabel106.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel106.setText("Perawatan Yang Disarankan");
        jLabel106.setName("jLabel106"); // NOI18N
        FormInput.add(jLabel106);
        jLabel106.setBounds(40, 600, 135, 20);

        jLabel99.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel99.setText("1. C CONFUSION");
        jLabel99.setName("jLabel99"); // NOI18N
        FormInput.add(jLabel99);
        jLabel99.setBounds(30, 370, 100, 30);

        c2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ureum > 40 mg/dl", "Ureum < 40 mg/dl" }));
        c2.setName("c2"); // NOI18N
        c2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                c2ItemStateChanged(evt);
            }
        });
        c2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                c2KeyPressed(evt);
            }
        });
        FormInput.add(c2);
        c2.setBounds(220, 400, 210, 23);

        c3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "RR > 30 x / menit", "RR < 30 x / menit" }));
        c3.setName("c3"); // NOI18N
        c3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                c3ItemStateChanged(evt);
            }
        });
        c3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                c3KeyPressed(evt);
            }
        });
        FormInput.add(c3);
        c3.setBounds(220, 430, 210, 23);

        c4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Blood Pressure < 90/60 mmHg", "Blood Pressure > 90/60 mmHg" }));
        c4.setName("c4"); // NOI18N
        c4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                c4ItemStateChanged(evt);
            }
        });
        c4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                c4KeyPressed(evt);
            }
        });
        FormInput.add(c4);
        c4.setBounds(220, 460, 210, 23);

        c1_ket.setEditable(false);
        c1_ket.setHighlighter(null);
        c1_ket.setName("c1_ket"); // NOI18N
        c1_ket.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                c1_ketInputMethodTextChanged(evt);
            }
        });
        c1_ket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c1_ketActionPerformed(evt);
            }
        });
        FormInput.add(c1_ket);
        c1_ket.setBounds(450, 370, 80, 23);

        c2_ket.setEditable(false);
        c2_ket.setHighlighter(null);
        c2_ket.setName("c2_ket"); // NOI18N
        c2_ket.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                c2_ketInputMethodTextChanged(evt);
            }
        });
        FormInput.add(c2_ket);
        c2_ket.setBounds(450, 400, 80, 23);

        c3_ket.setEditable(false);
        c3_ket.setHighlighter(null);
        c3_ket.setName("c3_ket"); // NOI18N
        c3_ket.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                c3_ketInputMethodTextChanged(evt);
            }
        });
        c3_ket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c3_ketActionPerformed(evt);
            }
        });
        FormInput.add(c3_ket);
        c3_ket.setBounds(450, 430, 80, 23);

        c4_ket.setEditable(false);
        c4_ket.setHighlighter(null);
        c4_ket.setName("c4_ket"); // NOI18N
        c4_ket.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                c4_ketInputMethodTextChanged(evt);
            }
        });
        c4_ket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c4_ketActionPerformed(evt);
            }
        });
        FormInput.add(c4_ket);
        c4_ket.setBounds(450, 460, 80, 23);

        jLabel98.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel98.setText("GAMBARAN KLINIS");
        jLabel98.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel98.setName("jLabel98"); // NOI18N
        FormInput.add(jLabel98);
        jLabel98.setBounds(40, 340, 110, 30);

        jLabel100.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel100.setText("HASIL PEMERIKSAAN");
        jLabel100.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel100.setName("jLabel100"); // NOI18N
        FormInput.add(jLabel100);
        jLabel100.setBounds(260, 340, 130, 30);

        total.setHighlighter(null);
        total.setName("total"); // NOI18N
        total.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                totalInputMethodTextChanged(evt);
            }
        });
        total.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalActionPerformed(evt);
            }
        });
        FormInput.add(total);
        total.setBounds(450, 530, 80, 23);

        jLabel107.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel107.setText("5. USIA 65");
        jLabel107.setName("jLabel107"); // NOI18N
        FormInput.add(jLabel107);
        jLabel107.setBounds(30, 490, 180, 30);

        jLabel108.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel108.setText("Total Skor :");
        jLabel108.setName("jLabel108"); // NOI18N
        FormInput.add(jLabel108);
        jLabel108.setBounds(380, 530, 60, 30);

        jLabel109.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel109.setText("Level Resiko");
        jLabel109.setName("jLabel109"); // NOI18N
        FormInput.add(jLabel109);
        jLabel109.setBounds(110, 570, 60, 20);

        c5_ket.setEditable(false);
        c5_ket.setHighlighter(null);
        c5_ket.setName("c5_ket"); // NOI18N
        c5_ket.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                c5_ketInputMethodTextChanged(evt);
            }
        });
        c5_ket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c5_ketActionPerformed(evt);
            }
        });
        FormInput.add(c5_ket);
        c5_ket.setBounds(450, 490, 80, 23);

        level.setEditable(false);
        level.setHighlighter(null);
        level.setName("level"); // NOI18N
        level.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                levelActionPerformed(evt);
            }
        });
        FormInput.add(level);
        level.setBounds(200, 570, 250, 23);

        jScrollPane5.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane5.setName("jScrollPane5"); // NOI18N

        perawatanyangdisarankan.setEditable(false);
        perawatanyangdisarankan.setColumns(20);
        perawatanyangdisarankan.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        perawatanyangdisarankan.setLineWrap(true);
        perawatanyangdisarankan.setRows(5);
        perawatanyangdisarankan.setName("perawatanyangdisarankan"); // NOI18N
        jScrollPane5.setViewportView(perawatanyangdisarankan);

        FormInput.add(jScrollPane5);
        jScrollPane5.setBounds(200, 600, 250, 70);

        c5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Umur > 65 tahun", "Umur < 65 tahun" }));
        c5.setName("c5"); // NOI18N
        c5.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                c5ItemStateChanged(evt);
            }
        });
        c5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                c5KeyPressed(evt);
            }
        });
        FormInput.add(c5);
        c5.setBounds(220, 490, 210, 23);

        jSeparator2.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator2.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator2.setName("jSeparator2"); // NOI18N
        FormInput.add(jSeparator2);
        jSeparator2.setBounds(0, 328, 880, 2);

        jLabel105.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel105.setText("Nilai");
        jLabel105.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel105.setName("jLabel105"); // NOI18N
        FormInput.add(jLabel105);
        jLabel105.setBounds(220, 90, 30, 30);

        r10.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r10.setName("r10"); // NOI18N
        r10.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r10ItemStateChanged(evt);
            }
        });
        r10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r10KeyPressed(evt);
            }
        });
        FormInput.add(r10);
        r10.setBounds(590, 240, 60, 23);

        jLabel110.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel110.setText("j) Hitung Mundur (Mulai dari 20 kebelakang)");
        jLabel110.setName("jLabel110"); // NOI18N
        FormInput.add(jLabel110);
        jLabel110.setBounds(320, 240, 220, 14);

        jLabel111.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel111.setText("Respons");
        jLabel111.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel111.setName("jLabel111"); // NOI18N
        FormInput.add(jLabel111);
        jLabel111.setBounds(40, 90, 50, 30);

        jLabel112.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel112.setText("a) Umur");
        jLabel112.setName("jLabel112"); // NOI18N
        FormInput.add(jLabel112);
        jLabel112.setBounds(40, 120, 50, 14);

        jLabel113.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel113.setText("b) Tanggal Lahir");
        jLabel113.setName("jLabel113"); // NOI18N
        FormInput.add(jLabel113);
        jLabel113.setBounds(40, 150, 90, 14);

        r1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r1.setName("r1"); // NOI18N
        r1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r1ItemStateChanged(evt);
            }
        });
        r1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r1KeyPressed(evt);
            }
        });
        FormInput.add(r1);
        r1.setBounds(200, 120, 60, 23);

        r2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r2.setName("r2"); // NOI18N
        r2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r2ItemStateChanged(evt);
            }
        });
        r2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r2KeyPressed(evt);
            }
        });
        FormInput.add(r2);
        r2.setBounds(200, 150, 60, 23);

        jLabel114.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel114.setText("c) Waktu (Untuk Jam Terdekat)");
        jLabel114.setName("jLabel114"); // NOI18N
        FormInput.add(jLabel114);
        jLabel114.setBounds(40, 180, 160, 14);

        jLabel115.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel115.setText("d) Tahun Sekarang");
        jLabel115.setName("jLabel115"); // NOI18N
        FormInput.add(jLabel115);
        jLabel115.setBounds(40, 210, 160, 14);

        jLabel116.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel116.setText("e) Nama Rumah Sakit");
        jLabel116.setName("jLabel116"); // NOI18N
        FormInput.add(jLabel116);
        jLabel116.setBounds(40, 240, 160, 14);

        jLabel117.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel117.setText("f) Dapat Mengidentifikasi 2 Orang (Dokter / Perawat)");
        jLabel117.setName("jLabel117"); // NOI18N
        FormInput.add(jLabel117);
        jLabel117.setBounds(320, 120, 270, 14);

        jLabel118.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel118.setText("g) Alamat Rumah");
        jLabel118.setName("jLabel118"); // NOI18N
        FormInput.add(jLabel118);
        jLabel118.setBounds(320, 150, 160, 14);

        jLabel119.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel119.setText("h) Tanggal Kemerdekaan");
        jLabel119.setName("jLabel119"); // NOI18N
        FormInput.add(jLabel119);
        jLabel119.setBounds(320, 180, 160, 14);

        jLabel120.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel120.setText("i) Nama Presiden");
        jLabel120.setName("jLabel120"); // NOI18N
        FormInput.add(jLabel120);
        jLabel120.setBounds(320, 210, 160, 14);

        r3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r3.setName("r3"); // NOI18N
        r3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r3ItemStateChanged(evt);
            }
        });
        r3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r3KeyPressed(evt);
            }
        });
        FormInput.add(r3);
        r3.setBounds(200, 180, 60, 23);

        r4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r4.setName("r4"); // NOI18N
        r4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r4ItemStateChanged(evt);
            }
        });
        r4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r4KeyPressed(evt);
            }
        });
        FormInput.add(r4);
        r4.setBounds(200, 210, 60, 23);

        r5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r5.setName("r5"); // NOI18N
        r5.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r5ItemStateChanged(evt);
            }
        });
        r5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r5KeyPressed(evt);
            }
        });
        FormInput.add(r5);
        r5.setBounds(200, 240, 60, 23);

        r6.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r6.setName("r6"); // NOI18N
        r6.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r6ItemStateChanged(evt);
            }
        });
        r6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r6KeyPressed(evt);
            }
        });
        FormInput.add(r6);
        r6.setBounds(590, 120, 60, 23);

        r7.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r7.setName("r7"); // NOI18N
        r7.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r7ItemStateChanged(evt);
            }
        });
        r7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r7KeyPressed(evt);
            }
        });
        FormInput.add(r7);
        r7.setBounds(590, 150, 60, 23);

        r8.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r8.setName("r8"); // NOI18N
        r8.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r8ItemStateChanged(evt);
            }
        });
        r8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r8KeyPressed(evt);
            }
        });
        FormInput.add(r8);
        r8.setBounds(590, 180, 60, 23);

        r9.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        r9.setName("r9"); // NOI18N
        r9.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                r9ItemStateChanged(evt);
            }
        });
        r9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r9KeyPressed(evt);
            }
        });
        FormInput.add(r9);
        r9.setBounds(590, 210, 60, 23);

        jLabel121.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel121.setText("Total Skor Respons :");
        jLabel121.setName("jLabel121"); // NOI18N
        FormInput.add(jLabel121);
        jLabel121.setBounds(470, 280, 100, 30);

        totalrespon.setEditable(false);
        totalrespon.setHighlighter(null);
        totalrespon.setName("totalrespon"); // NOI18N
        totalrespon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalresponActionPerformed(evt);
            }
        });
        FormInput.add(totalrespon);
        totalrespon.setBounds(580, 280, 80, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Pemeriksaan", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "29-07-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "29-07-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Pemeriksaan", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{            
            Valid.pindah(evt,TCari,BtnDokter);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(KdDokter.getText().trim().equals("")){
            Valid.textKosong(KdDokter,"Dokter");
        }else if(c1_ket.getText().trim().equals("")){
            Valid.textKosong(c1_ket,"C");
        }else if(c2_ket.getText().trim().equals("")){
            Valid.textKosong(c2_ket,"U");
        }else if(c3_ket.getText().trim().equals("")){
            Valid.textKosong(c3_ket,"R");
        }else if(c4_ket.getText().trim().equals("")){
            Valid.textKosong(c4_ket,"B");
        }else if(c5_ket.getText().trim().equals("")){
            Valid.textKosong(c5_ket,"Umur 65");
        }else if(totalrespon.getText().trim().equals("")){
            Valid.textKosong(totalrespon,"Total Respon");
        }else if(total.getText().trim().equals("")){
            Valid.textKosong(total,"Total");
        }else if(total.getText().trim().equals("0")){
            Valid.textKosong(total,"Data CURB belum dipilih");
        }else{
            if(Sequel.menyimpantf("penilaian_lain_skoringcurb","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",22,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),
                r1.getSelectedItem().toString(),
                r2.getSelectedItem().toString(),
                r3.getSelectedItem().toString(),
                r4.getSelectedItem().toString(),
                r5.getSelectedItem().toString(),
                r6.getSelectedItem().toString(),
                r7.getSelectedItem().toString(),
                r8.getSelectedItem().toString(),
                r9.getSelectedItem().toString(),
                r10.getSelectedItem().toString(),
                totalrespon.getText(),
                c1_ket.getText(),
                c2_ket.getText(),
                c3_ket.getText(),
                c4_ket.getText(),
                c5_ket.getText(),
                total.getText(),
                level.getText(),
                perawatanyangdisarankan.getText()
                })==true){
                    JOptionPane.showMessageDialog(rootPane,"Data berhasil disimpan!");
                    emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,c2,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh dokter/petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }  
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
       if(TNoRw.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Pasien");
        }else if(KdDokter.getText().trim().equals("")){
            Valid.textKosong(KdDokter,"Dokter");
        }else if(c1_ket.getText().trim().equals("")){
            Valid.textKosong(c1_ket,"C");
        }else if(c2_ket.getText().trim().equals("")){
            Valid.textKosong(c2_ket,"U");
        }else if(c3_ket.getText().trim().equals("")){
            Valid.textKosong(c3_ket,"R");
        }else if(c4_ket.getText().trim().equals("")){
            Valid.textKosong(c4_ket,"B");
        }else if(c5_ket.getText().trim().equals("")){
            Valid.textKosong(c5_ket,"Umur 65");
        }else if(totalrespon.getText().trim().equals("")){
            Valid.textKosong(totalrespon,"Total Respon");
        }else if(total.getText().trim().equals("")){
            Valid.textKosong(total,"Total");
        }else if(total.getText().trim().equals("0")){
            Valid.textKosong(total,"Data CURB belum dipilih");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                     ganti();
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }
        }    
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed

}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void KdDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokterKeyPressed
        
    }//GEN-LAST:event_KdDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setAlwaysOnTop(false);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        //Valid.pindah(evt,Monitoring,BtnSimpan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void TglPemeriksaanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglPemeriksaanKeyPressed
  
    }//GEN-LAST:event_TglPemeriksaanKeyPressed

    private void MnCetakMCUPsikiatriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakMCUPsikiatriActionPerformed
      if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),5).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString()));
            Valid.MyReportqry("rptCetakPenilaianCURB65.jasper","report","::[ Formulir CURB 65 ]::",
                "select pls.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk,CONCAT( p.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) AS alamat,reg_periksa.umurdaftar,reg_periksa.sttsumur, " +
                "p.tgl_lahir,pls.kd_dokter,d.nm_dokter,pls.tanggal,pls.res1,pls.res2,pls.res3,pls.res4,pls.res5,pls.res6,pls.res7,pls.res8,pls.res9,pls.res10,pls.totalrespon, "+
                "pls.C,pls.U,pls.R,pls.B,pls.U65,pls.total,pls.level_resiko,pls.perawatan_disarankan " +
                "from reg_periksa " +
                "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                "inner join penilaian_lain_skoringcurb pls on reg_periksa.no_rawat = pls.no_rawat " +
                "inner join dokter d on pls.kd_dokter = d.kd_dokter " +
                "INNER JOIN kelurahan ON kelurahan.kd_kel = p.kd_kel " +
                "INNER JOIN kecamatan ON kecamatan.kd_kec = p.kd_kec " +
                "INNER JOIN kabupaten ON kabupaten.kd_kab = p.kd_kab " +
                "where pls.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and pls.tanggal='"+tbObat.getValueAt(tbObat.getSelectedRow(),7).toString()+"' ",param);
        }    
    }//GEN-LAST:event_MnCetakMCUPsikiatriActionPerformed

    private void c2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_c2KeyPressed
         Valid.pindah(evt,c2,c3);
    }//GEN-LAST:event_c2KeyPressed

    private void c3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_c3KeyPressed
        Valid.pindah(evt,c2,c4);
    }//GEN-LAST:event_c3KeyPressed

    private void c4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_c4KeyPressed
        Valid.pindah(evt,c3,c5);
    }//GEN-LAST:event_c4KeyPressed

    private void c1_ketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c1_ketActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_c1_ketActionPerformed

    private void c3_ketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c3_ketActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_c3_ketActionPerformed

    private void c4_ketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c4_ketActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_c4_ketActionPerformed

    private void totalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_totalActionPerformed

    private void c5_ketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c5_ketActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_c5_ketActionPerformed

    private void levelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_levelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_levelActionPerformed

    private void c2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_c2ItemStateChanged
        isCombo2();
        isjml();
        iskategoriskor();
      
    }//GEN-LAST:event_c2ItemStateChanged

    private void c3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_c3ItemStateChanged
        isCombo3();
        isjml();
        iskategoriskor();
       
    }//GEN-LAST:event_c3ItemStateChanged

    private void c4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_c4ItemStateChanged
        isCombo4();
        isjml();
        iskategoriskor();
      
    }//GEN-LAST:event_c4ItemStateChanged

    private void c5ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_c5ItemStateChanged
      isCombo5();
      isjml();
      iskategoriskor();
    }//GEN-LAST:event_c5ItemStateChanged

    private void c5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_c5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_c5KeyPressed

    private void c1_ketInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_c1_ketInputMethodTextChanged
     isjml();
    }//GEN-LAST:event_c1_ketInputMethodTextChanged

    private void c2_ketInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_c2_ketInputMethodTextChanged
     isjml();
  
    }//GEN-LAST:event_c2_ketInputMethodTextChanged

    private void c3_ketInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_c3_ketInputMethodTextChanged
     isjml();
    }//GEN-LAST:event_c3_ketInputMethodTextChanged

    private void c4_ketInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_c4_ketInputMethodTextChanged
     isjml();
  
    }//GEN-LAST:event_c4_ketInputMethodTextChanged

    private void c5_ketInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_c5_ketInputMethodTextChanged
     isjml();
  
    }//GEN-LAST:event_c5_ketInputMethodTextChanged

    private void r10ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r10ItemStateChanged
    isjml_respon();
    }//GEN-LAST:event_r10ItemStateChanged

    private void r10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r10KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r10KeyPressed

    private void r1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r1ItemStateChanged
      isjml_respon();
    }//GEN-LAST:event_r1ItemStateChanged

    private void r1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r1KeyPressed

    private void r2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r2ItemStateChanged
       isjml_respon();
    }//GEN-LAST:event_r2ItemStateChanged

    private void r2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r2KeyPressed

    private void r3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r3ItemStateChanged
        isjml_respon();
    }//GEN-LAST:event_r3ItemStateChanged

    private void r3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r3KeyPressed

    private void r4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r4ItemStateChanged
        isjml_respon();
    }//GEN-LAST:event_r4ItemStateChanged

    private void r4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r4KeyPressed

    private void r5ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r5ItemStateChanged
       isjml_respon();
    }//GEN-LAST:event_r5ItemStateChanged

    private void r5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r5KeyPressed

    private void r6ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r6ItemStateChanged
       isjml_respon();
    }//GEN-LAST:event_r6ItemStateChanged

    private void r6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r6KeyPressed

    private void r7ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r7ItemStateChanged
       isjml_respon();
    }//GEN-LAST:event_r7ItemStateChanged

    private void r7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r7KeyPressed

    private void r8ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r8ItemStateChanged
        isjml_respon();
    }//GEN-LAST:event_r8ItemStateChanged

    private void r8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r8KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r8KeyPressed

    private void r9ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_r9ItemStateChanged
        isjml_respon();
    }//GEN-LAST:event_r9ItemStateChanged

    private void r9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r9KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r9KeyPressed

    private void totalresponActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totalresponActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_totalresponActionPerformed

    private void totalInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_totalInputMethodTextChanged
      iskategoriskor();
      isjml();
    }//GEN-LAST:event_totalInputMethodTextChanged

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMLainSkoringCURB dialog = new RMLainSkoringCURB(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.PanelBiasa FormInput;
    private widget.TextBox Jk;
    private widget.TextBox KdDokter;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private javax.swing.JMenuItem MnCetakMCUPsikiatri;
    private widget.TextBox NmDokter;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.TextBox TglLahir;
    private widget.Tanggal TglPemeriksaan;
    private widget.TextBox c1_ket;
    private widget.ComboBox c2;
    private widget.TextBox c2_ket;
    private widget.ComboBox c3;
    private widget.TextBox c3_ket;
    private widget.ComboBox c4;
    private widget.TextBox c4_ket;
    private widget.ComboBox c5;
    private widget.TextBox c5_ket;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel100;
    private widget.Label jLabel102;
    private widget.Label jLabel103;
    private widget.Label jLabel104;
    private widget.Label jLabel105;
    private widget.Label jLabel106;
    private widget.Label jLabel107;
    private widget.Label jLabel108;
    private widget.Label jLabel109;
    private widget.Label jLabel11;
    private widget.Label jLabel110;
    private widget.Label jLabel111;
    private widget.Label jLabel112;
    private widget.Label jLabel113;
    private widget.Label jLabel114;
    private widget.Label jLabel115;
    private widget.Label jLabel116;
    private widget.Label jLabel117;
    private widget.Label jLabel118;
    private widget.Label jLabel119;
    private widget.Label jLabel120;
    private widget.Label jLabel121;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private widget.Label jLabel97;
    private widget.Label jLabel98;
    private widget.Label jLabel99;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private widget.Label label11;
    private widget.Label label15;
    private widget.TextBox level;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private javax.swing.JTextArea perawatanyangdisarankan;
    private widget.ComboBox r1;
    private widget.ComboBox r10;
    private widget.ComboBox r2;
    private widget.ComboBox r3;
    private widget.ComboBox r4;
    private widget.ComboBox r5;
    private widget.ComboBox r6;
    private widget.ComboBox r7;
    private widget.ComboBox r8;
    private widget.ComboBox r9;
    private widget.ScrollPane scrollInput;
    private widget.Table tbObat;
    private widget.TextBox total;
    private widget.TextBox totalrespon;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().trim().equals("")){
            ps=koneksi.prepareStatement(
                        "select p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk, " +
                        "p.tgl_lahir,plc.*, " +
                        "d.nm_dokter "+
                        "from reg_periksa " +
                        "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_lain_skoringcurb plc on reg_periksa.no_rawat = plc.no_rawat " +
                        "inner join dokter d on plc.kd_dokter = d.kd_dokter " +
                        "where plc.tanggal between ? and ? " +
                        "order by plc.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                        "select p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk, " +
                        "p.tgl_lahir,plc.*, " +
                        "d.nm_dokter "+
                        "from reg_periksa " +
                        "inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_lain_skoringcurb plc on reg_periksa.no_rawat = plc.no_rawat " +
                        "inner join dokter d on plc.kd_dokter = d.kd_dokter " +
                        "where plc.tanggal between ? and ? and (reg_periksa.no_rawat like ? or p.no_rkm_medis like ? or p.nm_pasien like ? or plc.kd_dokter like ? or d.nm_dokter like ?) " +
                        "order by plc.tanggal");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("tgl_lahir"),rs.getString("jk"),rs.getString("plc.kd_dokter"),rs.getString("d.nm_dokter"),rs.getString("tanggal"),
                        rs.getString("res1"),rs.getString("res2"),rs.getString("res3"),rs.getString("res4"),rs.getString("res5"),rs.getString("res6"),rs.getString("res7"),rs.getString("res8"),rs.getString("res9"),rs.getString("res10"),
                        rs.getString("totalrespon"),rs.getString("C"),rs.getString("U"),rs.getString("R"),rs.getString("B"),rs.getString("U65"),rs.getString("total"),rs.getString("level_resiko"),rs.getString("perawatan_disarankan")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TabRawat.setSelectedIndex(0);
        TglPemeriksaan.setDate(new Date());
        c2.setSelectedIndex(0);
        c3.setSelectedIndex(0);
        c4.setSelectedIndex(0);
        c5.setSelectedIndex(0);
        
        r1.setSelectedIndex(0);
        r2.setSelectedIndex(0);
        r3.setSelectedIndex(0);
        r4.setSelectedIndex(0);
        r5.setSelectedIndex(0);
        r6.setSelectedIndex(0);
        r7.setSelectedIndex(0);
        r8.setSelectedIndex(0);
        r9.setSelectedIndex(0);
        r10.setSelectedIndex(0);
        level.setText("");
        perawatanyangdisarankan.setText("");
        
       
      
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            Valid.SetTgl2(TglPemeriksaan,tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            r1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            r2.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            r3.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            r4.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            r5.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            r6.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            r7.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            r8.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            r9.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            r10.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            totalrespon.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            c1_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            c2_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            c3_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            c4_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            c5_ket.setText(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
               
           
             if(c2_ket.getText().equals("-")){
                    c2.setSelectedItem("-");
                }else if(c2_ket.getText().equals("1")){
                    c2.setSelectedItem("Ureum > 40 mg/dl");
                }else if(c2_ket.getText().equals("0")){
                    c2.setSelectedItem("Ureum < 40 mg/dl");
                }
              if(c3_ket.getText().equals("-")){
                    c3.setSelectedItem("-");
                }else if(c3_ket.getText().equals("1")){
                    c3.setSelectedItem("RR > 30 x / menit");
                }else if(c3_ket.getText().equals("0")){
                    c3.setSelectedItem("RR < 30 x / menit");
                }
              if(c4_ket.getText().equals("-")){
                    c4.setSelectedItem("-");
                }else if(c4_ket.getText().equals("1")){
                    c4.setSelectedItem("Blood Pressure < 90/60 mmHg");
                }else if(c4_ket.getText().equals("0")){
                    c4.setSelectedItem("Blood Pressure > 90/60 mmHg");
                }
              if(c5_ket.getText().equals("-")){
                    c5.setSelectedItem("-");
                }else if(c5_ket.getText().equals("1")){
                    c5.setSelectedItem("Umur > 65 tahun");
                }else if(c4_ket.getText().equals("0")){
                    c5.setSelectedItem("Umur < 65 tahun");
                }
            total.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            level.setText(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            perawatanyangdisarankan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
 
    public void setNoRm(String norwt,Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2); 
        Sequel.cariIsi("select kd_dokter from reg_periksa where no_rawat=?", KdDokter,norwt);
        Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?", NmDokter,KdDokter.getText());
        isRawat(); 
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.gettindakan_ralan());
        BtnHapus.setEnabled(akses.gettindakan_ralan());
        BtnEdit.setEnabled(akses.gettindakan_ralan());        
    }
    
    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from penilaian_lain_skoringcurb where no_rawat=? and tanggal=? ",2,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),
            tbObat.getValueAt(tbObat.getSelectedRow(),7).toString().substring(0,19),
        })==true){
            tampil();
            TabRawat.setSelectedIndex(1);
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("penilaian_lain_skoringcurb","no_rawat=? and tanggal=?","no_rawat=?,tanggal=?,kd_dokter=?,res1=?,res2=?,res3=?,res4=?,res5=?,res6=?,res7=?,res8=?,res9=?,res10=?,totalrespon=?,C=?,U=?,R=?,B=?,U65=?,total=?,level_resiko=?,perawatan_disarankan=?",24,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),
                r1.getSelectedItem().toString(),
                r2.getSelectedItem().toString(),
                r3.getSelectedItem().toString(),
                r4.getSelectedItem().toString(),
                r5.getSelectedItem().toString(),
                r6.getSelectedItem().toString(),
                r7.getSelectedItem().toString(),
                r8.getSelectedItem().toString(),
                r9.getSelectedItem().toString(),
                r10.getSelectedItem().toString(),
                totalrespon.getText(),
                c1_ket.getText(),
                c2_ket.getText(),
                c3_ket.getText(),
                c4_ket.getText(),
                c5_ket.getText(),
                total.getText(),
                level.getText(),
                perawatanyangdisarankan.getText(),
                TNoRw.getText(),
                Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
            })==true){
               tampil();
               emptTeks();
               TabRawat.setSelectedIndex(1);
               
        }
       
    }  
        
    private void isCombo2(){   
        if(c2.getSelectedItem().equals("-")){
            c2_ket.setText("0");
        }else if(c2.getSelectedItem().equals("Ureum > 40 mg/dl")){
            c2_ket.setText("1");
        }else if(c2.getSelectedItem().equals("Ureum < 40 mg/dl")){
            c2_ket.setText("0");
        }
    }
    
    private void isCombo3(){   
        if(c3.getSelectedItem().equals("-")){
            c3_ket.setText("0");
        }else if(c3.getSelectedItem().equals("RR > 30 x / menit")){
            c3_ket.setText("1");
        }else if(c3.getSelectedItem().equals("RR < 30 x / menit")){
            c3_ket.setText("0");
        }
    }
    private void isCombo4(){   
        if(c4.getSelectedItem().equals("-")){
            c4_ket.setText("0");
        }else if(c4.getSelectedItem().equals("Blood Pressure < 90/60 mmHg")){
            c4_ket.setText("1");
        }else if(c4.getSelectedItem().equals("Blood Pressure > 90/60 mmHg")){
            c4_ket.setText("0");
        }
    }
    private void isCombo5(){   
        if(c5.getSelectedItem().equals("-")){
            c5_ket.setText("0");
        }else if(c5.getSelectedItem().equals("Umur > 65 tahun")){
            c5_ket.setText("1");
        }else if(c5.getSelectedItem().equals("Umur < 65 tahun")){
            c5_ket.setText("0");
        }
    }
    
    private void isjml(){
        total.setText(Valid.SetAngka(Valid.SetAngka(c1_ket.getText())+Valid.SetAngka(c2_ket.getText())+Valid.SetAngka(c3_ket.getText())+Valid.SetAngka(c4_ket.getText())+Valid.SetAngka(c5_ket.getText())));    
    }
    
    private void isjml_respon(){
        int res1=0,res2=0,res3=0,res4=0,res5=0,res6=0,res7=0,res8=0,res9=0,res10=0;
        if(r1.getSelectedItem().equals("Ya")){
            res1=1;
        }else{
            res1=0;
        }
        if(r2.getSelectedItem().equals("Ya")){
            res2=1;
        }else{
            res2=0;
        }
        if(r3.getSelectedItem().equals("Ya")){
            res3=1;
        }else{
            res3=0;
        }
        if(r4.getSelectedItem().equals("Ya")){
            res4=1;
        }else{
            res4=0;
        }
        if(r5.getSelectedItem().equals("Ya")){
            res5=1;
        }else{
            res5=0;
        }
        if(r6.getSelectedItem().equals("Ya")){
            res6=1;
        }else{
            res6=0;
        }
        if(r7.getSelectedItem().equals("Ya")){
            res7=1;
        }else{
            res7=0;
        }
        if(r8.getSelectedItem().equals("Ya")){
            res8=1;
        }else{
            res8=0;
        }
        if(r9.getSelectedItem().equals("Ya")){
            res9=1;
        }else{
            res9=0;
        }
        if(r10.getSelectedItem().equals("Ya")){
            res10=1;
        }else{
            res10=0;
        }
        totalrespon.setText(Valid.SetAngka(res1+res2+res3+res4+res5+res6+res7+res8+res9+res10));    
        if((res1+res2+res3+res4+res5+res6+res7+res8+res9+res10) <= 8 ){
            c1_ket.setText("1");
        }else{
            c1_ket.setText("0");
        }
        isjml();
        iskategoriskor();
    }
    
     private void iskategoriskor(){
        double kategori=0;
        kategori = Valid.SetAngka(total.getText());
        
        if(kategori == 0 || kategori== 1 ){
            level.setText("Risiko Kematian Rendah");
            perawatanyangdisarankan.setText("Pasien Dapat Berobat Jalan");
        }else if (kategori ==2 ){
            level.setText("Risiko Kematian Sedang");
            perawatanyangdisarankan.setText("Dapat Dipertimbangkan Untuk Dirawat");
        }else if (kategori == 3){
            level.setText("Risiko Kematian Tinggi");
            perawatanyangdisarankan.setText("Dirawat Harus Ditatalaksana Sebagai Pneumonia Berat");
        }else if (kategori == 4 || kategori == 5){
            level.setText("Risiko Kematian Tinggi");
            perawatanyangdisarankan.setText("Harus Dipertimbangkan Perawatan Intensif");
        }else{
            level.setText("-");
            perawatanyangdisarankan.setText("-");
        }
     }
    
    
   
}
