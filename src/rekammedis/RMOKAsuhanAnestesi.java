/*
  Kontribusi Anggit Nurhidayah 06/01/2025
 */

package rekammedis;

import setting.*;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.WarnaTable;
import fungsi.akses;
import inventory.DlgBarang;
import inventory.DlgCariJenis;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;

/**
 *
 * @author perpustakaan
 */
public class RMOKAsuhanAnestesi extends javax.swing.JDialog {
    private final DefaultTableModel tabModeAnestesi,tabModeInOp,tabModePraInduksi;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private PreparedStatement ps;
    private ResultSet rs;
    private DlgBarang barang=new DlgBarang(null,false);
    private String finger="",finger2="",ruang_rawat="";
    private Integer i=0,x=0;
    private RMCariDiagnosaPreOp caripreop=new RMCariDiagnosaPreOp(null,false);
    private RMCariHasilLaborat carilaborat=new RMCariHasilLaborat(null,false);
    private RMCariHasilRadiologi cariradiologi=new RMCariHasilRadiologi(null,false);

    /**
     *@param parent
     *@param modal*/
    public RMOKAsuhanAnestesi(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        tabModeAnestesi=new DefaultTableModel(null,new Object[]{
              "No.Rawat","Tanggal","Jam","Kode Petugas","Petugas","No.RM","Nama","JK",
              "Tindakan Operasi","Diagnosa","Jenis Anestesi","Keterangan Anestesi",
              "Face Mask Tersedia","Laringoscope","Endotrakeal Tube Tersedia","Oropharingeal Sesuai Ukuran",
              "Sodalime Baik","Compact Breathing Baik","Reservior Bag Tersedia",
              "Mesin Anastesi Baik","Mesin Suction/kateter Tersedia","Gas Oksigen O2, N2O Compress Air terpasang",
              "Vaporizer Terisi","Moitor Pasien Terpasang","Cairan Infus & Darah tersedia",
              "Obat General Anestesi Tersedia","Obat Perlengkapan anestesi regional tersedia","Obat Emergency","Infus Terpasang Baik",
              "Obat sudah dilabel"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        tbChecklist.setModel(tabModeAnestesi);
        tbChecklist.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbChecklist.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 30; i++) {
            TableColumn column = tbChecklist.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(80);
            }if(i==1){
                column.setPreferredWidth(80);
            }if(i==2){
                column.setPreferredWidth(80);
            }if(i==3){
                column.setPreferredWidth(80);
            }if(i==4){
                column.setPreferredWidth(80);
            }if(i==5){
                column.setPreferredWidth(80);
            }if(i==6){
                column.setPreferredWidth(80);
            }if(i==7){
                column.setPreferredWidth(100);
            }if(i==8){
                column.setPreferredWidth(120);
            }if(i==9){
                column.setPreferredWidth(120);
            }if(i==10){
                column.setPreferredWidth(120);
            }if(i==11){
                column.setPreferredWidth(120);
            }if(i==12){
                column.setPreferredWidth(120);
            }if(i==13){
                column.setPreferredWidth(120);
            }if(i==14){
                column.setPreferredWidth(120);
            }if(i==15){
                column.setPreferredWidth(120);
            }if(i==16){
                column.setPreferredWidth(120);
            }if(i==17){
                column.setPreferredWidth(120);
            }if(i==18){
                column.setPreferredWidth(120);
            }if(i==19){
                column.setPreferredWidth(120);
            }if(i==20){
                column.setPreferredWidth(120);
            }if(i==21){
                column.setPreferredWidth(120);
            }if(i==22){
                column.setPreferredWidth(120);
            }if(i==23){
                column.setPreferredWidth(120);
            }if(i==24){
                column.setPreferredWidth(120);
            }if(i==25){
                column.setPreferredWidth(120);
            }if(i==26){
                column.setPreferredWidth(120);
            }if(i==27){
                column.setPreferredWidth(120);
            }if(i==28){
                column.setPreferredWidth(120);
            }if(i==29){
                column.setPreferredWidth(120);
            }
            
        }

        tbChecklist.setDefaultRenderer(Object.class, new WarnaTable());
  
        KdPetugas.setDocument(new batasInput((byte)4).getKata(KdPetugas));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        TCari1.setDocument(new batasInput((int)100).getKata(TCari1));
         
        // TABEL UNTUK  INTA OPERATIF
         tabModeInOp=new DefaultTableModel(null,new Object[]{
             "No.Rawat","Tanggal","Jam","Kode Petugas","Petugas","No.RM","Nama","JK",
             "anamnesis", "diagnosa preop", "rencana operasi", "tb", "bb", "obat dikonsumsi", 
             "obat dikonsumsi ket", "riwayat alergi","keterangan alergi", "riwayat penyakit","keterangan penyakit", "riwayat anestesi", 
             "jenis anestesi", "riwayat merokok", "komplikasi anestesi", "fisik b1", "fisik alat", 
             "fisik rr", "fisik vesikuler", "fisik rhonki", "fisik wheezing plus","fisik wheezing min", "fisik td", "fisik hr", 
             "fisik hr ket", "fisik konjungtiva", "fisik gcse", "fisik gcsm", "fisik gcsv", "fisik pupil", 
             "fisik hemiparese", "fisik urin", "fisik warnaurin", "fisik perut", "fisik diare", "fisik muntah", 
             "fisik alatbantu", "fisik fraktur", "penunjang lab", "penunjang rab", "penunjang elektro", "asa", "asa1", 
             "asa2", "asa3", "asa4", "asa5", "asa6", "asaE", "rencana ga", "rencana reg", "rencana blok", "Obat Obatan","Keterangan Obat Obatan",
             "cairan","Keterangan Cairan","monitoring khusus", "rencana perawatan inap", "rencana hcu", "rencana icu", "rencana rajal", "rencana igd"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        tbPrasedasi.setModel(tabModeInOp);
        tbPrasedasi.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPrasedasi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 69; i++) {
            TableColumn column = tbPrasedasi.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(80);
            }if(i==1){
                column.setPreferredWidth(80);
            }if(i==2){
                column.setPreferredWidth(80);
            }if(i==3){
                column.setPreferredWidth(80);
            }if(i==4){
                column.setPreferredWidth(80);
            }if(i==5){
                column.setPreferredWidth(80);
            }if(i==6){
                column.setPreferredWidth(80);
            }if(i==7){
                column.setPreferredWidth(100);
            }if(i==8){
                column.setPreferredWidth(120);
            }if(i==9){
                column.setPreferredWidth(120);
            }if(i==10){
                column.setPreferredWidth(120);
            }if(i==11){
                column.setPreferredWidth(120);
            }if(i==12){
                column.setPreferredWidth(120);
            }if(i==13){
                column.setPreferredWidth(120);
            }if(i==14){
                column.setPreferredWidth(120);
            }if(i==15){
                column.setPreferredWidth(120);
            }if(i==16){
                column.setPreferredWidth(120);
            }if(i==17){
                column.setPreferredWidth(120);
            }if(i==18){
                column.setPreferredWidth(120);
            }if(i==19){
                column.setPreferredWidth(120);
            }if(i==20){
                column.setPreferredWidth(120);
            }if(i==21){
                column.setPreferredWidth(120);
            }if(i==22){
                column.setPreferredWidth(120);
            }if(i==23){
                column.setPreferredWidth(120);
            }if(i==24){
                column.setPreferredWidth(120);
            }if(i==25){
                column.setPreferredWidth(120);
            }if(i==26){
                column.setPreferredWidth(120);
            }if(i==27){
                column.setPreferredWidth(120);
            }if(i==28){
                column.setPreferredWidth(120);
            }if(i==29){
                column.setPreferredWidth(120);
            }if(i==30){
                column.setPreferredWidth(120);
            }if(i==31){
                column.setPreferredWidth(120);
            }if(i==32){
                column.setPreferredWidth(120);
            }if(i==33){
                column.setPreferredWidth(120);
            }if(i==34){
                column.setPreferredWidth(120);
            }if(i==35){
                column.setPreferredWidth(120);
            }if(i==36){
                column.setPreferredWidth(120);
            }if(i==37){
                column.setPreferredWidth(120);
            }if(i==38){
                column.setPreferredWidth(120);
            }if(i==39){
                column.setPreferredWidth(120);
            }if(i==40){
                column.setPreferredWidth(120);
            }if(i==41){
                column.setPreferredWidth(120);
            }if(i==42){
                column.setPreferredWidth(120);
            }if(i==43){
                column.setPreferredWidth(120);
            }if(i==44){
                column.setPreferredWidth(120);
            }if(i==45){
                column.setPreferredWidth(120);
            }if(i==46){
                column.setPreferredWidth(120);
            }if(i==47){
                column.setPreferredWidth(120);
            }if(i==48){
                column.setPreferredWidth(120);
            }if(i==49){
                column.setPreferredWidth(120);
            }if(i==50){
                column.setPreferredWidth(120);
            }if(i==51){
                column.setPreferredWidth(120);
            }if(i==52){
                column.setPreferredWidth(120);
            }if(i==53){
                column.setPreferredWidth(120);
            }if(i==54){
                column.setPreferredWidth(120);
            }if(i==55){
                column.setPreferredWidth(120);
            }if(i==56){
                column.setPreferredWidth(120);
            }if(i==57){
                column.setPreferredWidth(120);
            }if(i==59){
                column.setPreferredWidth(120);
            }if(i==60){
                column.setPreferredWidth(120);
            }if(i==61){
                column.setPreferredWidth(120);
            }if(i==62){
                column.setPreferredWidth(120);
            }if(i==63){
                column.setPreferredWidth(120);
            }if(i==64){
                column.setPreferredWidth(120);
            }if(i==65){
                column.setPreferredWidth(120);
            }if(i==66){
                column.setPreferredWidth(120);
            }if(i==67){
                column.setPreferredWidth(120);
            }if(i==68){
                column.setPreferredWidth(120);
            }
        }

        tbPrasedasi.setDefaultRenderer(Object.class, new WarnaTable());
        
        
        // TABEL UNTUK  PRA INDUKSI
         tabModePraInduksi=new DefaultTableModel(null,new Object[]{
            "No. Rawat", "Tanggal", "Jam", "Kode Dokter", "Nama Dokter", 
            "No. Rekam Medis", "Nama Pasien", "Alergi","Keteranga Alergi", "Bb", "Td", "Nadi", 
            "Tb", "Respirasi", "Gol Darah", "Suhu", "Gcs", "Rh", "Hb", "Vas", 
            "Ht", "Lain", "Fisik Normal", "Fisik Bukamulut", "Fisik Jarak", 
            "Fisik Mallampati", "Fisik Gerakan Leher", "Fisik Abnormal", 
            "Anamnesis Auto", "Anamnesis Allo", "Asa1", "Asa2", "Asa3", 
            "Asa4", "Asa5", "Asa E", "Penyulit Praanestesi", "Ijin Operasi", "Cek Mohon Anestesi", 
            "Cek Suction Unit", "Persiapan Obat Obatan", "Antibiotika Profilaksis", "GA", "Tiva", "LMA", "Facemask", "ET", 
            "Regional", "Spinal", "Epidural", "Kaudal", "Blok Saraf Tepi", "Anestesi Lain","Ket. Anestesi Lain", "Monitoring 1", 
            "Monitoring 2", "Monitoring 3", "Monitoring 3 Ket", "Monitoring 4", 
            "Monitoring 4 Ket", "Monitoring 5", "Monitoring 6", "Monitoring 7", 
            "Monitoring 8", "Monitoring 9"


            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        tbPrainduksi.setModel(tabModePraInduksi);
        tbPrainduksi.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPrainduksi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 65; i++) {
            TableColumn column = tbPrainduksi.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(80);
            }if(i==1){
                column.setPreferredWidth(80);
            }if(i==2){
                column.setPreferredWidth(80);
            }if(i==3){
                column.setPreferredWidth(80);
            }if(i==4){
                column.setPreferredWidth(80);
            }if(i==5){
                column.setPreferredWidth(80);
            }if(i==6){
                column.setPreferredWidth(80);
            }if(i==7){
                column.setPreferredWidth(100);
            }if(i==8){
                column.setPreferredWidth(120);
            }if(i==9){
                column.setPreferredWidth(120);
            }if(i==10){
                column.setPreferredWidth(120);
            }if(i==11){
                column.setPreferredWidth(120);
            }if(i==12){
                column.setPreferredWidth(120);
            }if(i==13){
                column.setPreferredWidth(120);
            }if(i==14){
                column.setPreferredWidth(120);
            }if(i==15){
                column.setPreferredWidth(120);
            }if(i==16){
                column.setPreferredWidth(120);
            }if(i==17){
                column.setPreferredWidth(120);
            }if(i==18){
                column.setPreferredWidth(120);
            }if(i==19){
                column.setPreferredWidth(120);
            }if(i==20){
                column.setPreferredWidth(120);
            }if(i==21){
                column.setPreferredWidth(120);
            }if(i==22){
                column.setPreferredWidth(120);
            }if(i==23){
                column.setPreferredWidth(120);
            }if(i==24){
                column.setPreferredWidth(120);
            }if(i==25){
                column.setPreferredWidth(120);
            }if(i==26){
                column.setPreferredWidth(120);
            }if(i==27){
                column.setPreferredWidth(120);
            }if(i==28){
                column.setPreferredWidth(120);
            }if(i==29){
                column.setPreferredWidth(120);
            }if(i==30){
                column.setPreferredWidth(120);
            }if(i==31){
                column.setPreferredWidth(120);
            }if(i==32){
                column.setPreferredWidth(120);
            }if(i==33){
                column.setPreferredWidth(120);
            }if(i==34){
                column.setPreferredWidth(120);
            }if(i==35){
                column.setPreferredWidth(120);
            }if(i==36){
                column.setPreferredWidth(120);
            }if(i==37){
                column.setPreferredWidth(120);
            }if(i==38){
                column.setPreferredWidth(120);
            }if(i==39){
                column.setPreferredWidth(120);
            }if(i==40){
                column.setPreferredWidth(120);
            }if(i==41){
                column.setPreferredWidth(120);
            }if(i==42){
                column.setPreferredWidth(120);
            }if(i==43){
                column.setPreferredWidth(120);
            }if(i==44){
                column.setPreferredWidth(120);
            }if(i==45){
                column.setPreferredWidth(120);
            }if(i==46){
                column.setPreferredWidth(120);
            }if(i==47){
                column.setPreferredWidth(120);
            }if(i==48){
                column.setPreferredWidth(120);
            }if(i==49){
                column.setPreferredWidth(120);
            }if(i==50){
                column.setPreferredWidth(120);
            }if(i==51){
                column.setPreferredWidth(120);
            }if(i==52){
                column.setPreferredWidth(120);
            }if(i==53){
                column.setPreferredWidth(120);
            }if(i==54){
                column.setPreferredWidth(120);
            }if(i==55){
                column.setPreferredWidth(120);
            }if(i==56){
                column.setPreferredWidth(120);
            }if(i==57){
                column.setPreferredWidth(120);
            }if(i==59){
                column.setPreferredWidth(120);
            }if(i==60){
                column.setPreferredWidth(120);
            }if(i==61){
                column.setPreferredWidth(120);
            }if(i==62){
                column.setPreferredWidth(120);
            }if(i==63){
                column.setPreferredWidth(120);
            }if(i==64){
                column.setPreferredWidth(120);
            }
        }

         tbPrainduksi.setDefaultRenderer(Object.class, new WarnaTable());
         petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(TabOK.getSelectedIndex()==0){
                    if(petugas.getTable().getSelectedRow()!= -1){     
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString()); 
                    }
                }else if(TabOK.getSelectedIndex()==1){
                    if(petugas.getTable().getSelectedRow()!= -1){     
                    KdPetugas1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString()); 
                    }
                }
                
                
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
         
        carilaborat.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                
                  if(TabOK.getSelectedIndex()==1){
                    if(carilaborat.getTable().getSelectedRow()!= -1){     
                    //START CUSTOM
                        if(carilaborat.getTable().getRowCount() > 0){
                          
                            for(i=0;i<carilaborat.getTable().getRowCount();i++){ 
                               
                                if(carilaborat.getTable().getValueAt(i,0).toString().equals("true")){
                              
                                    Laboratorium.append(carilaborat.getTable().getValueAt(i,3).toString()+", ");
                                }
                            }
                        }
                    //END CUSTOM 
                    }
                }else if(TabOK.getSelectedIndex()==2){
                    if(carilaborat.getTable().getSelectedRow()!= -1){     
                    //START CUSTOM
                    if(carilaborat.getTable().getRowCount() > 0){
                         if(x==2){
                            for(i=0;i<carilaborat.getTable().getRowCount();i++){ 
                                if(carilaborat.getTable().getValueAt(i,0).toString().equals("true")){
                                    tab3_hb.setText(carilaborat.getTable().getValueAt(i,3).toString());
                                    tab3_hb.requestFocus();
                                }
                            }
                         } 
                         if(x==3){
                             for(i=0;i<carilaborat.getTable().getRowCount();i++){ 
                                if(carilaborat.getTable().getValueAt(i,0).toString().equals("true")){
                                    tab3_ht.setText(carilaborat.getTable().getValueAt(i,3).toString());
                                    tab3_ht.requestFocus();
                                }
                            }
                         
                         }
                    }
                    //END CUSTOM 
                    }
                }
                  
                    
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        cariradiologi.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                //START CUSTOM
                if(cariradiologi.getTable().getRowCount() > 0){
                    for(i=0;i<cariradiologi.getTable().getRowCount();i++){ 
                        if(cariradiologi.getTable().getValueAt(i,0).toString().equals("true")){
                            Radiologi.append(cariradiologi.getTable().getValueAt(i,3).toString()+", ");
                            Radiologi.requestFocus();
                        }
                    }
                }
                //END CUSTOM
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                 if(TabOK.getSelectedIndex()==1){
                    if(dokter.getTable().getSelectedRow()!= -1){      
                        KdPetugas1.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NmPetugas1.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());  
                    } 
                }else if(TabOK.getSelectedIndex()==2){
                   if(dokter.getTable().getSelectedRow()!= -1){      
                        KdDokter3.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NmDokter3.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());  
                    }  
                }      
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        caripreop.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                 if(TabOK.getSelectedIndex()==1){
                    if(caripreop.getTable().getSelectedRow()!= -1){      
                        Preop.setText(caripreop.getTable().getValueAt(caripreop.getTable().getSelectedRow(),3).toString());
                        Rencanaop.setText(caripreop.getTable().getValueAt(caripreop.getTable().getSelectedRow(),5).toString());  
                        tb.setText(caripreop.getTable().getValueAt(caripreop.getTable().getSelectedRow(),12).toString()); 
                        bb.setText(caripreop.getTable().getValueAt(caripreop.getTable().getSelectedRow(),13).toString());  
                    } 
                }      
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        jam();
        jam1();
        jam2();
        ChkAccor.setSelected(false);
        ChkAccor1.setSelected(false);
        ChkAccor2.setSelected(false);
        isPhoto();
        isPhoto1();
        isPhoto2();
        }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Popup = new javax.swing.JPopupMenu();
        pp2 = new javax.swing.JMenuItem();
        Popup1 = new javax.swing.JPopupMenu();
        pp1 = new javax.swing.JMenuItem();
        Popup2 = new javax.swing.JPopupMenu();
        ppCetakLembarFormulir = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        internalFrame1 = new widget.InternalFrame();
        panelisi1 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnKeluar = new widget.Button();
        TabOK = new javax.swing.JTabbedPane();
        internalFrame3 = new widget.InternalFrame();
        panelisi5 = new widget.panelisi();
        label9 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        label10 = new widget.Label();
        LCount = new widget.Label();
        scrollInput1 = new widget.ScrollPane();
        FormInput1 = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnPetugas = new widget.Button();
        jLabel11 = new widget.Label();
        jSeparator5 = new javax.swing.JSeparator();
        Scroll6 = new widget.ScrollPane();
        tbMasalahKeperawatan = new widget.Table();
        A4 = new widget.CekBox();
        label68 = new widget.Label();
        label35 = new widget.Label();
        jLabel17 = new widget.Label();
        Tanggal = new widget.Tanggal();
        Jam = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Detik = new widget.ComboBox();
        ChkJam1 = new widget.CekBox();
        jk = new widget.TextBox();
        label105 = new widget.Label();
        label198 = new widget.Label();
        label199 = new widget.Label();
        label203 = new widget.Label();
        label205 = new widget.Label();
        label206 = new widget.Label();
        A1 = new widget.CekBox();
        A2 = new widget.CekBox();
        A3 = new widget.CekBox();
        B4 = new widget.CekBox();
        label207 = new widget.Label();
        label208 = new widget.Label();
        label209 = new widget.Label();
        label210 = new widget.Label();
        label211 = new widget.Label();
        B1 = new widget.CekBox();
        B2 = new widget.CekBox();
        B3 = new widget.CekBox();
        label212 = new widget.Label();
        label213 = new widget.Label();
        label214 = new widget.Label();
        label215 = new widget.Label();
        label216 = new widget.Label();
        C1 = new widget.CekBox();
        C2 = new widget.CekBox();
        D1 = new widget.CekBox();
        label217 = new widget.Label();
        label218 = new widget.Label();
        D2 = new widget.CekBox();
        label282 = new widget.Label();
        label283 = new widget.Label();
        label284 = new widget.Label();
        label285 = new widget.Label();
        label286 = new widget.Label();
        label287 = new widget.Label();
        label288 = new widget.Label();
        D4 = new widget.CekBox();
        D5 = new widget.CekBox();
        D6 = new widget.CekBox();
        D7 = new widget.CekBox();
        D8 = new widget.CekBox();
        D9 = new widget.CekBox();
        jenis_anestesi = new widget.ComboBox();
        tindakan_ok = new widget.TextBox();
        diagnosa_ok = new widget.TextBox();
        label200 = new widget.Label();
        jenisanestesi_text = new widget.TextBox();
        PanelAccor = new widget.PanelBiasa();
        ChkAccor = new widget.CekBox();
        TabData = new javax.swing.JTabbedPane();
        FormTelaah = new widget.PanelBiasa();
        FormPass3 = new widget.PanelBiasa();
        BtnRefreshPhoto1 = new widget.Button();
        Scroll5 = new widget.ScrollPane();
        tbChecklist = new widget.Table();
        internalFrame4 = new widget.InternalFrame();
        panelisi6 = new widget.panelisi();
        label11 = new widget.Label();
        TCari1 = new widget.TextBox();
        BtnCari1 = new widget.Button();
        BtnAll1 = new widget.Button();
        label12 = new widget.Label();
        LCount1 = new widget.Label();
        scrollInput2 = new widget.ScrollPane();
        FormInput2 = new widget.PanelBiasa();
        TNoRw2 = new widget.TextBox();
        TPasien2 = new widget.TextBox();
        TNoRM2 = new widget.TextBox();
        label15 = new widget.Label();
        KdPetugas1 = new widget.TextBox();
        NmPetugas1 = new widget.TextBox();
        BtnPetugas1 = new widget.Button();
        jLabel12 = new widget.Label();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        label38 = new widget.Label();
        tb = new widget.TextBox();
        label45 = new widget.Label();
        bb = new widget.TextBox();
        label152 = new widget.Label();
        obat_text = new widget.TextBox();
        jLabel18 = new widget.Label();
        Tanggal1 = new widget.Tanggal();
        Jam1 = new widget.ComboBox();
        Menit1 = new widget.ComboBox();
        Detik1 = new widget.ComboBox();
        ChkJam2 = new widget.CekBox();
        jk1 = new widget.TextBox();
        scrollPane1 = new widget.ScrollPane();
        Rencanaop = new widget.TextArea();
        label18 = new widget.Label();
        label19 = new widget.Label();
        label20 = new widget.Label();
        scrollPane2 = new widget.ScrollPane();
        Anamnesis = new widget.TextArea();
        scrollPane3 = new widget.ScrollPane();
        Preop = new widget.TextArea();
        label46 = new widget.Label();
        label47 = new widget.Label();
        CmbObat = new widget.ComboBox();
        label153 = new widget.Label();
        label154 = new widget.Label();
        label155 = new widget.Label();
        CmbAnestesi2 = new widget.ComboBox();
        CmbAlergi = new widget.ComboBox();
        CmbPenyakit = new widget.ComboBox();
        label156 = new widget.Label();
        CmbMerokok = new widget.ComboBox();
        CmbAnestesi = new widget.ComboBox();
        Komplikasi = new widget.TextBox();
        label157 = new widget.Label();
        jSeparator9 = new javax.swing.JSeparator();
        label158 = new widget.Label();
        label159 = new widget.Label();
        label160 = new widget.Label();
        label161 = new widget.Label();
        label162 = new widget.Label();
        label163 = new widget.Label();
        label164 = new widget.Label();
        jSeparator10 = new javax.swing.JSeparator();
        label165 = new widget.Label();
        label166 = new widget.Label();
        label167 = new widget.Label();
        label168 = new widget.Label();
        jSeparator13 = new javax.swing.JSeparator();
        jSeparator14 = new javax.swing.JSeparator();
        label169 = new widget.Label();
        label170 = new widget.Label();
        Asa = new widget.CekBox();
        label171 = new widget.Label();
        Asa1 = new widget.CekBox();
        label172 = new widget.Label();
        Asa2 = new widget.CekBox();
        Asa3 = new widget.CekBox();
        label173 = new widget.Label();
        Asa4 = new widget.CekBox();
        label174 = new widget.Label();
        label175 = new widget.Label();
        Asa5 = new widget.CekBox();
        Asa6 = new widget.CekBox();
        label176 = new widget.Label();
        Asa7 = new widget.CekBox();
        label177 = new widget.Label();
        label178 = new widget.Label();
        label179 = new widget.Label();
        label185 = new widget.Label();
        label186 = new widget.Label();
        label187 = new widget.Label();
        label188 = new widget.Label();
        label189 = new widget.Label();
        jSeparator15 = new javax.swing.JSeparator();
        label190 = new widget.Label();
        Cmbmonitoring = new widget.ComboBox();
        label191 = new widget.Label();
        label192 = new widget.Label();
        rencana_inap = new widget.CekBox();
        label193 = new widget.Label();
        rencana_hcu = new widget.CekBox();
        label194 = new widget.Label();
        rencana_icu = new widget.CekBox();
        label195 = new widget.Label();
        rencana_ralan = new widget.CekBox();
        label196 = new widget.Label();
        rencana_igd = new widget.CekBox();
        B3_cmbpupil = new widget.ComboBox();
        label197 = new widget.Label();
        B1_alat = new widget.TextBox();
        label39 = new widget.Label();
        B1_vesikuler = new widget.TextBox();
        label48 = new widget.Label();
        label201 = new widget.Label();
        B1_rr = new widget.TextBox();
        label202 = new widget.Label();
        B1_rhonki = new widget.TextBox();
        label204 = new widget.Label();
        B1_wheezing_plus = new widget.TextBox();
        label40 = new widget.Label();
        B2_td = new widget.TextBox();
        label41 = new widget.Label();
        B2_hr = new widget.TextBox();
        CmbB1 = new widget.ComboBox();
        label42 = new widget.Label();
        B2_cmb = new widget.ComboBox();
        label43 = new widget.Label();
        B3_e = new widget.TextBox();
        label44 = new widget.Label();
        label49 = new widget.Label();
        B4_urin = new widget.TextBox();
        label50 = new widget.Label();
        B3_v = new widget.TextBox();
        label51 = new widget.Label();
        B2_cmbkonjungtiva = new widget.ComboBox();
        label52 = new widget.Label();
        B4_cmbwarnaurin = new widget.ComboBox();
        label53 = new widget.Label();
        B3_m = new widget.TextBox();
        label54 = new widget.Label();
        label55 = new widget.Label();
        B5_cmbkembung = new widget.ComboBox();
        label56 = new widget.Label();
        B3_cmbhemiparese = new widget.ComboBox();
        label57 = new widget.Label();
        B6_cmbalatbantu = new widget.ComboBox();
        B5_cmbmuntah = new widget.ComboBox();
        label58 = new widget.Label();
        label59 = new widget.Label();
        B5_cmbdiare = new widget.ComboBox();
        label60 = new widget.Label();
        B6_cmbfraktur = new widget.ComboBox();
        scrollPane4 = new widget.ScrollPane();
        Elektrokardiografi = new widget.TextArea();
        scrollPane5 = new widget.ScrollPane();
        Laboratorium = new widget.TextArea();
        scrollPane6 = new widget.ScrollPane();
        obat_obatan_text = new widget.TextArea();
        scrollPane7 = new widget.ScrollPane();
        Radiologi = new widget.TextArea();
        B1_wheezing_min = new widget.TextBox();
        label228 = new widget.Label();
        label280 = new widget.Label();
        cmb_GA = new widget.ComboBox();
        cmb_REGIONAL = new widget.ComboBox();
        scrollPane8 = new widget.ScrollPane();
        anestesi_blok = new widget.TextArea();
        cmb_obat = new widget.ComboBox();
        label281 = new widget.Label();
        cmb_cairan = new widget.ComboBox();
        scrollPane9 = new widget.ScrollPane();
        cairan_teks = new widget.TextArea();
        riwayat_alergi_text = new widget.TextBox();
        riwayat_penyakit_text = new widget.TextBox();
        BtnPetugas3 = new widget.Button();
        BtnRad = new widget.Button();
        BtnLab = new widget.Button();
        PanelAccor1 = new widget.PanelBiasa();
        ChkAccor1 = new widget.CekBox();
        TabData1 = new javax.swing.JTabbedPane();
        FormTelaah1 = new widget.PanelBiasa();
        FormPass4 = new widget.PanelBiasa();
        BtnRefreshPhoto2 = new widget.Button();
        Scroll8 = new widget.ScrollPane();
        tbPrasedasi = new widget.Table();
        internalFrame5 = new widget.InternalFrame();
        panelisi7 = new widget.panelisi();
        label13 = new widget.Label();
        TCari2 = new widget.TextBox();
        BtnCari2 = new widget.Button();
        BtnAll2 = new widget.Button();
        label16 = new widget.Label();
        LCount2 = new widget.Label();
        scrollInput3 = new widget.ScrollPane();
        FormInput3 = new widget.PanelBiasa();
        TNoRw3 = new widget.TextBox();
        TPasien3 = new widget.TextBox();
        TNoRM3 = new widget.TextBox();
        label17 = new widget.Label();
        KdDokter3 = new widget.TextBox();
        NmDokter3 = new widget.TextBox();
        BtnPetugas2 = new widget.Button();
        jLabel13 = new widget.Label();
        jSeparator11 = new javax.swing.JSeparator();
        jSeparator12 = new javax.swing.JSeparator();
        label180 = new widget.Label();
        tab3_bb = new widget.TextBox();
        label181 = new widget.Label();
        tab3_td = new widget.TextBox();
        label182 = new widget.Label();
        tab3_nadi = new widget.TextBox();
        label183 = new widget.Label();
        tab3_tb = new widget.TextBox();
        label184 = new widget.Label();
        jLabel19 = new widget.Label();
        Tanggal2 = new widget.Tanggal();
        Jam2 = new widget.ComboBox();
        Menit2 = new widget.ComboBox();
        Detik2 = new widget.ComboBox();
        ChkJam3 = new widget.CekBox();
        jk2 = new widget.TextBox();
        tab3_respirasi = new widget.TextBox();
        tab3_normal = new widget.CekBox();
        tab3_alergi1 = new widget.CekBox();
        tab3_alergi2 = new widget.CekBox();
        tab3_bukamulut = new widget.CekBox();
        tab3_jarak = new widget.CekBox();
        tab3_mallampati = new widget.CekBox();
        tab3_leher = new widget.CekBox();
        tab3_abnormal = new widget.CekBox();
        label219 = new widget.Label();
        label220 = new widget.Label();
        label221 = new widget.Label();
        label222 = new widget.Label();
        label223 = new widget.Label();
        label224 = new widget.Label();
        label225 = new widget.Label();
        label226 = new widget.Label();
        jSeparator16 = new javax.swing.JSeparator();
        label227 = new widget.Label();
        tab3_auto = new widget.CekBox();
        label_100 = new widget.Label();
        tab3_allo = new widget.CekBox();
        label229 = new widget.Label();
        jSeparator17 = new javax.swing.JSeparator();
        label230 = new widget.Label();
        label231 = new widget.Label();
        tab3_asa1 = new widget.CekBox();
        label232 = new widget.Label();
        tab3_asa2 = new widget.CekBox();
        label233 = new widget.Label();
        tab3_asa3 = new widget.CekBox();
        label234 = new widget.Label();
        tab3_asa4 = new widget.CekBox();
        label235 = new widget.Label();
        tab3_asa5 = new widget.CekBox();
        label236 = new widget.Label();
        tab3_asaE = new widget.CekBox();
        label237 = new widget.Label();
        penyulit_praanestesi = new widget.TextBox();
        jSeparator18 = new javax.swing.JSeparator();
        label238 = new widget.Label();
        tab3_cek1 = new widget.CekBox();
        label239 = new widget.Label();
        label240 = new widget.Label();
        tab3_cek2 = new widget.CekBox();
        tab3_cek3 = new widget.CekBox();
        label241 = new widget.Label();
        tab3_cek4 = new widget.CekBox();
        label242 = new widget.Label();
        tab3_cek5 = new widget.CekBox();
        label243 = new widget.Label();
        jSeparator19 = new javax.swing.JSeparator();
        label244 = new widget.Label();
        tab3_tek1 = new widget.CekBox();
        label245 = new widget.Label();
        tab3_tek6 = new widget.CekBox();
        label246 = new widget.Label();
        label247 = new widget.Label();
        tab3_tek11 = new widget.CekBox();
        tab3_tek2 = new widget.CekBox();
        label248 = new widget.Label();
        label249 = new widget.Label();
        tab3_tek3 = new widget.CekBox();
        label250 = new widget.Label();
        tab3_tek4 = new widget.CekBox();
        label251 = new widget.Label();
        tab3_tek5 = new widget.CekBox();
        tab3_tek7 = new widget.CekBox();
        label252 = new widget.Label();
        tab3_tek8 = new widget.CekBox();
        label253 = new widget.Label();
        tab3_tek9 = new widget.CekBox();
        label254 = new widget.Label();
        tab3_tek10 = new widget.CekBox();
        label255 = new widget.Label();
        jSeparator20 = new javax.swing.JSeparator();
        tab3_moni1 = new widget.CekBox();
        label256 = new widget.Label();
        label257 = new widget.Label();
        label258 = new widget.Label();
        tab3_moni5 = new widget.CekBox();
        tab3_moni8 = new widget.CekBox();
        label259 = new widget.Label();
        label260 = new widget.Label();
        tab3_moni2 = new widget.CekBox();
        tab3_moni6 = new widget.CekBox();
        label261 = new widget.Label();
        tab3_moni3 = new widget.CekBox();
        label262 = new widget.Label();
        label263 = new widget.Label();
        tab3_moni7 = new widget.CekBox();
        tab3_moni9 = new widget.CekBox();
        label264 = new widget.Label();
        label265 = new widget.Label();
        tab3_moni4 = new widget.CekBox();
        label266 = new widget.Label();
        label267 = new widget.Label();
        label268 = new widget.Label();
        label269 = new widget.Label();
        label270 = new widget.Label();
        label271 = new widget.Label();
        label272 = new widget.Label();
        tab3_darah = new widget.TextBox();
        label273 = new widget.Label();
        tab3_suhu = new widget.TextBox();
        label274 = new widget.Label();
        tab3_gcs = new widget.TextBox();
        label275 = new widget.Label();
        tab3_rh = new widget.TextBox();
        label276 = new widget.Label();
        tab3_hb = new widget.TextBox();
        label277 = new widget.Label();
        tab3_vas = new widget.TextBox();
        label278 = new widget.Label();
        tab3_ht = new widget.TextBox();
        tab3_lain = new widget.TextBox();
        tab3_moni4_ket = new widget.TextBox();
        tab3_moni3_ket = new widget.TextBox();
        alergi_teks = new widget.TextBox();
        BtnCariLab = new widget.Button();
        label290 = new widget.Label();
        BtnCariLab1 = new widget.Button();
        anestesi_teks = new widget.TextBox();
        PanelAccor2 = new widget.PanelBiasa();
        ChkAccor2 = new widget.CekBox();
        TabData2 = new javax.swing.JTabbedPane();
        FormTelaah2 = new widget.PanelBiasa();
        FormPass5 = new widget.PanelBiasa();
        BtnRefreshPhoto3 = new widget.Button();
        Scroll9 = new widget.ScrollPane();
        tbPrainduksi = new widget.Table();

        Popup.setName("Popup"); // NOI18N

        pp2.setBackground(new java.awt.Color(255, 255, 254));
        pp2.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        pp2.setForeground(new java.awt.Color(50, 50, 50));
        pp2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        pp2.setText("-");
        pp2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pp2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        pp2.setName("pp2"); // NOI18N
        pp2.setPreferredSize(new java.awt.Dimension(300, 25));
        pp2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pp2ActionPerformed(evt);
            }
        });
        Popup.add(pp2);

        Popup1.setName("Popup1"); // NOI18N

        pp1.setBackground(new java.awt.Color(255, 255, 254));
        pp1.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        pp1.setForeground(new java.awt.Color(50, 50, 50));
        pp1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        pp1.setText("Cetak Inta Operatif");
        pp1.setActionCommand("-");
        pp1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pp1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        pp1.setName("pp1"); // NOI18N
        pp1.setPreferredSize(new java.awt.Dimension(300, 25));
        pp1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pp1ActionPerformed(evt);
            }
        });
        Popup1.add(pp1);

        Popup2.setName("Popup2"); // NOI18N

        ppCetakLembarFormulir.setBackground(new java.awt.Color(255, 255, 254));
        ppCetakLembarFormulir.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppCetakLembarFormulir.setForeground(new java.awt.Color(50, 50, 50));
        ppCetakLembarFormulir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        ppCetakLembarFormulir.setText("Cetak Formulir");
        ppCetakLembarFormulir.setActionCommand("-");
        ppCetakLembarFormulir.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppCetakLembarFormulir.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppCetakLembarFormulir.setName("ppCetakLembarFormulir"); // NOI18N
        ppCetakLembarFormulir.setPreferredSize(new java.awt.Dimension(300, 25));
        ppCetakLembarFormulir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppCetakLembarFormulirActionPerformed(evt);
            }
        });
        Popup2.add(ppCetakLembarFormulir);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Asuhan Anestesi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi1.setName("panelisi1"); // NOI18N
        panelisi1.setPreferredSize(new java.awt.Dimension(100, 54));
        panelisi1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelisi1.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelisi1.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelisi1.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setIconTextGap(3);
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelisi1.add(BtnEdit);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelisi1.add(BtnKeluar);

        internalFrame1.add(panelisi1, java.awt.BorderLayout.PAGE_END);

        TabOK.setBackground(new java.awt.Color(255, 255, 254));
        TabOK.setForeground(new java.awt.Color(50, 50, 50));
        TabOK.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        TabOK.setName("TabOK"); // NOI18N
        TabOK.setPreferredSize(new java.awt.Dimension(452, 300));
        TabOK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabOKMouseClicked(evt);
            }
        });

        internalFrame3.setBackground(new java.awt.Color(255, 255, 255));
        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi5.setName("panelisi5"); // NOI18N
        panelisi5.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        label9.setText("Key Word :");
        label9.setName("label9"); // NOI18N
        label9.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi5.add(label9);

        TCari.setEditable(false);
        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(350, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelisi5.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('1');
        BtnCari.setToolTipText("Alt+1");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelisi5.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('2');
        BtnAll.setToolTipText("Alt+2");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelisi5.add(BtnAll);

        label10.setText("Record :");
        label10.setName("label10"); // NOI18N
        label10.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi5.add(label10);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi5.add(LCount);

        internalFrame3.add(panelisi5, java.awt.BorderLayout.PAGE_END);

        scrollInput1.setName("scrollInput1"); // NOI18N
        scrollInput1.setPreferredSize(new java.awt.Dimension(102, 200));

        FormInput1.setBackground(new java.awt.Color(255, 255, 255));
        FormInput1.setBorder(null);
        FormInput1.setName("FormInput1"); // NOI18N
        FormInput1.setPreferredSize(new java.awt.Dimension(870, 900));
        FormInput1.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput1.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput1.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput1.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput1.add(label14);
        label14.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput1.add(KdPetugas);
        KdPetugas.setBounds(74, 40, 100, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput1.add(NmPetugas);
        NmPetugas.setBounds(176, 40, 180, 23);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas.setMnemonic('2');
        BtnPetugas.setToolTipText("Alt+2");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        FormInput1.add(BtnPetugas);
        BtnPetugas.setBounds(358, 40, 28, 23);

        jLabel11.setText("No.Rawat :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput1.add(jLabel11);
        jLabel11.setBounds(0, 10, 70, 23);

        jSeparator5.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator5.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator5.setName("jSeparator5"); // NOI18N
        FormInput1.add(jSeparator5);
        jSeparator5.setBounds(0, 70, 880, 1);

        Scroll6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll6.setName("Scroll6"); // NOI18N
        Scroll6.setOpaque(true);
        Scroll6.setPreferredSize(new java.awt.Dimension(452, 200));

        tbMasalahKeperawatan.setName("tbMasalahKeperawatan"); // NOI18N
        Scroll6.setViewportView(tbMasalahKeperawatan);

        FormInput1.add(Scroll6);
        Scroll6.setBounds(10, 1210, 400, 133);

        A4.setBorder(null);
        A4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A4.setName("A4"); // NOI18N
        FormInput1.add(A4);
        A4.setBounds(370, 250, 23, 23);

        label68.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label68.setText("d. Oropharingeal airway sesuai ukuran pasien.");
        label68.setName("label68"); // NOI18N
        label68.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label68);
        label68.setBounds(60, 250, 290, 23);

        label35.setText("Jenis Anestesi :");
        label35.setName("label35"); // NOI18N
        label35.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label35);
        label35.setBounds(10, 140, 100, 23);

        jLabel17.setText("Tanggal :");
        jLabel17.setName("jLabel17"); // NOI18N
        jLabel17.setVerifyInputWhenFocusTarget(false);
        FormInput1.add(jLabel17);
        jLabel17.setBounds(380, 40, 75, 23);

        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "03-03-2025" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput1.add(Tanggal);
        Tanggal.setBounds(460, 40, 110, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setName("Jam"); // NOI18N
        Jam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JamActionPerformed(evt);
            }
        });
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        FormInput1.add(Jam);
        Jam.setBounds(580, 40, 50, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        FormInput1.add(Menit);
        Menit.setBounds(640, 40, 50, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        FormInput1.add(Detik);
        Detik.setBounds(700, 40, 50, 23);

        ChkJam1.setBorder(null);
        ChkJam1.setSelected(true);
        ChkJam1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam1.setName("ChkJam1"); // NOI18N
        FormInput1.add(ChkJam1);
        ChkJam1.setBounds(750, 40, 23, 23);

        jk.setEditable(false);
        jk.setName("jk"); // NOI18N
        jk.setPreferredSize(new java.awt.Dimension(50, 23));
        jk.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jkMouseMoved(evt);
            }
        });
        jk.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jkMouseExited(evt);
            }
        });
        jk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jkActionPerformed(evt);
            }
        });
        jk.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jkKeyPressed(evt);
            }
        });
        FormInput1.add(jk);
        jk.setBounds(580, 10, 50, 23);

        label105.setText("Tindakan Operasi : ");
        label105.setName("label105"); // NOI18N
        label105.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label105);
        label105.setBounds(10, 80, 100, 23);

        label198.setText("Diagnosa :");
        label198.setName("label198"); // NOI18N
        label198.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label198);
        label198.setBounds(10, 110, 100, 23);

        label199.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label199.setText("  YA");
        label199.setName("label199"); // NOI18N
        label199.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label199);
        label199.setBounds(370, 170, 30, 23);

        label203.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label203.setText("a. Face mask tersedia sesuai ukuran pasien.");
        label203.setName("label203"); // NOI18N
        label203.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label203);
        label203.setBounds(60, 190, 230, 23);

        label205.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label205.setText("b. Laringoscope ( Lampu Terang ).");
        label205.setName("label205"); // NOI18N
        label205.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label205);
        label205.setBounds(60, 210, 230, 23);

        label206.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label206.setText("c. Endotrakeal Tube tersedia sesuai ukuran pasien.");
        label206.setName("label206"); // NOI18N
        label206.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label206);
        label206.setBounds(60, 230, 280, 23);

        A1.setBorder(null);
        A1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A1.setName("A1"); // NOI18N
        FormInput1.add(A1);
        A1.setBounds(370, 190, 23, 23);

        A2.setBorder(null);
        A2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A2.setName("A2"); // NOI18N
        FormInput1.add(A2);
        A2.setBounds(370, 210, 23, 23);

        A3.setBorder(null);
        A3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        A3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        A3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        A3.setName("A3"); // NOI18N
        FormInput1.add(A3);
        A3.setBounds(370, 230, 23, 23);

        B4.setBorder(null);
        B4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        B4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        B4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        B4.setName("B4"); // NOI18N
        FormInput1.add(B4);
        B4.setBounds(370, 360, 23, 23);

        label207.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label207.setText("d. Mesin anastesi baik dan tidak ada kebocoran");
        label207.setName("label207"); // NOI18N
        label207.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label207);
        label207.setBounds(60, 360, 290, 23);

        label208.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label208.setText("BREATHING");
        label208.setName("label208"); // NOI18N
        label208.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label208);
        label208.setBounds(60, 280, 80, 23);

        label209.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label209.setText("a. Sodalime dalam keadaan baik");
        label209.setName("label209"); // NOI18N
        label209.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label209);
        label209.setBounds(60, 300, 230, 23);

        label210.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label210.setText("b. Compact breathing terpasang dengan baik.");
        label210.setName("label210"); // NOI18N
        label210.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label210);
        label210.setBounds(60, 320, 230, 23);

        label211.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label211.setText("c. Reservior Bag tersedia dan sesuai kebutuhan pasien.");
        label211.setName("label211"); // NOI18N
        label211.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label211);
        label211.setBounds(60, 340, 280, 23);

        B1.setBorder(null);
        B1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        B1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        B1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        B1.setName("B1"); // NOI18N
        FormInput1.add(B1);
        B1.setBounds(370, 300, 23, 23);

        B2.setBorder(null);
        B2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        B2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        B2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        B2.setName("B2"); // NOI18N
        FormInput1.add(B2);
        B2.setBounds(370, 320, 23, 23);

        B3.setBorder(null);
        B3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        B3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        B3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        B3.setName("B3"); // NOI18N
        FormInput1.add(B3);
        B3.setBounds(370, 340, 23, 23);

        label212.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label212.setText("sudah tersedia.");
        label212.setName("label212"); // NOI18N
        label212.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label212);
        label212.setBounds(70, 610, 80, 20);

        label213.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label213.setText("OBAT DAN PERALATAN");
        label213.setName("label213"); // NOI18N
        label213.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label213);
        label213.setBounds(60, 390, 220, 23);

        label214.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label214.setText("a. Mesin suction dan KAteter suction sudah tersedia");
        label214.setName("label214"); // NOI18N
        label214.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label214);
        label214.setBounds(60, 420, 270, 23);

        label215.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label215.setText("b. Gas oksigen O2, N2O, Compress air sudah terpasang");
        label215.setName("label215"); // NOI18N
        label215.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label215);
        label215.setBounds(60, 440, 290, 23);

        label216.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label216.setText(" ke mesin anastesi dan cukup persediaannya.");
        label216.setName("label216"); // NOI18N
        label216.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label216);
        label216.setBounds(70, 460, 260, 23);

        C1.setBorder(null);
        C1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        C1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        C1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        C1.setName("C1"); // NOI18N
        FormInput1.add(C1);
        C1.setBounds(370, 420, 23, 23);

        C2.setBorder(null);
        C2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        C2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        C2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        C2.setName("C2"); // NOI18N
        FormInput1.add(C2);
        C2.setBounds(370, 440, 23, 23);

        D1.setBorder(null);
        D1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        D1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        D1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        D1.setName("D1"); // NOI18N
        FormInput1.add(D1);
        D1.setBounds(370, 490, 23, 23);

        label217.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label217.setText("c. Vaporizer telah terisi.");
        label217.setName("label217"); // NOI18N
        label217.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label217);
        label217.setBounds(60, 490, 280, 20);

        label218.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label218.setText("d. Monitor pasien sudah terpasang dan dalam keadaaan baik.");
        label218.setName("label218"); // NOI18N
        label218.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label218);
        label218.setBounds(60, 510, 310, 20);

        D2.setBorder(null);
        D2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        D2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        D2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        D2.setName("D2"); // NOI18N
        FormInput1.add(D2);
        D2.setBounds(370, 510, 23, 23);

        label282.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label282.setText("    ( NIBP, Oksimetri, O2, EKG, Monitor suhu pasien ).");
        label282.setName("label282"); // NOI18N
        label282.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label282);
        label282.setBounds(60, 530, 310, 20);

        label283.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label283.setText("e. Cairan infus dan darah tersedia sesuai kebutuhan.");
        label283.setName("label283"); // NOI18N
        label283.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label283);
        label283.setBounds(60, 550, 310, 20);

        label284.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label284.setText("f. Obat general anestesi sudah tersedia.");
        label284.setName("label284"); // NOI18N
        label284.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label284);
        label284.setBounds(60, 570, 310, 20);

        label285.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label285.setText("g. Obat dan perlengkapan anestesi regional ( spinal / epidural )");
        label285.setName("label285"); // NOI18N
        label285.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label285);
        label285.setBounds(60, 590, 310, 20);

        label286.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label286.setText("j. Obat - obat yang akan dipakai sudah diberi label.");
        label286.setName("label286"); // NOI18N
        label286.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label286);
        label286.setBounds(60, 670, 310, 20);

        label287.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label287.setText("h. Obat-obat emergency sudah tersedia.");
        label287.setName("label287"); // NOI18N
        label287.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label287);
        label287.setBounds(60, 630, 310, 20);

        label288.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label288.setText("i. Infus terpasang dengan baik dan tetesan lancar.");
        label288.setName("label288"); // NOI18N
        label288.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label288);
        label288.setBounds(60, 650, 310, 20);

        D4.setBorder(null);
        D4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        D4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        D4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        D4.setName("D4"); // NOI18N
        FormInput1.add(D4);
        D4.setBounds(370, 550, 23, 23);

        D5.setBorder(null);
        D5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        D5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        D5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        D5.setName("D5"); // NOI18N
        FormInput1.add(D5);
        D5.setBounds(370, 570, 23, 23);

        D6.setBorder(null);
        D6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        D6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        D6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        D6.setName("D6"); // NOI18N
        FormInput1.add(D6);
        D6.setBounds(370, 590, 23, 23);

        D7.setBorder(null);
        D7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        D7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        D7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        D7.setName("D7"); // NOI18N
        FormInput1.add(D7);
        D7.setBounds(370, 630, 23, 23);

        D8.setBorder(null);
        D8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        D8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        D8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        D8.setName("D8"); // NOI18N
        FormInput1.add(D8);
        D8.setBounds(370, 650, 23, 23);

        D9.setBorder(null);
        D9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        D9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        D9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        D9.setName("D9"); // NOI18N
        FormInput1.add(D9);
        D9.setBounds(370, 670, 23, 23);

        jenis_anestesi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SAB", "GA ETT", "GA LMA", "GA FM", "TIVA" }));
        jenis_anestesi.setName("jenis_anestesi"); // NOI18N
        jenis_anestesi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jenis_anestesiActionPerformed(evt);
            }
        });
        jenis_anestesi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jenis_anestesiKeyPressed(evt);
            }
        });
        FormInput1.add(jenis_anestesi);
        jenis_anestesi.setBounds(120, 140, 130, 23);

        tindakan_ok.setHighlighter(null);
        tindakan_ok.setName("tindakan_ok"); // NOI18N
        tindakan_ok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tindakan_okKeyPressed(evt);
            }
        });
        FormInput1.add(tindakan_ok);
        tindakan_ok.setBounds(120, 80, 510, 23);

        diagnosa_ok.setHighlighter(null);
        diagnosa_ok.setName("diagnosa_ok"); // NOI18N
        diagnosa_ok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                diagnosa_okKeyPressed(evt);
            }
        });
        FormInput1.add(diagnosa_ok);
        diagnosa_ok.setBounds(120, 110, 510, 23);

        label200.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label200.setText("AIR WAY");
        label200.setName("label200"); // NOI18N
        label200.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label200);
        label200.setBounds(60, 170, 50, 23);

        jenisanestesi_text.setHighlighter(null);
        jenisanestesi_text.setName("jenisanestesi_text"); // NOI18N
        jenisanestesi_text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jenisanestesi_textKeyPressed(evt);
            }
        });
        FormInput1.add(jenisanestesi_text);
        jenisanestesi_text.setBounds(260, 140, 370, 23);

        scrollInput1.setViewportView(FormInput1);

        internalFrame3.add(scrollInput1, java.awt.BorderLayout.CENTER);

        PanelAccor.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor.setName("PanelAccor"); // NOI18N
        PanelAccor.setPreferredSize(new java.awt.Dimension(445, 43));
        PanelAccor.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setSelected(true);
        ChkAccor.setFocusable(false);
        ChkAccor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor.setName("ChkAccor"); // NOI18N
        ChkAccor.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccorActionPerformed(evt);
            }
        });
        PanelAccor.add(ChkAccor, java.awt.BorderLayout.WEST);

        TabData.setBackground(new java.awt.Color(254, 255, 254));
        TabData.setForeground(new java.awt.Color(50, 50, 50));
        TabData.setName("TabData"); // NOI18N
        TabData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabDataMouseClicked(evt);
            }
        });

        FormTelaah.setBackground(new java.awt.Color(255, 255, 255));
        FormTelaah.setBorder(null);
        FormTelaah.setName("FormTelaah"); // NOI18N
        FormTelaah.setPreferredSize(new java.awt.Dimension(115, 73));
        FormTelaah.setLayout(new java.awt.BorderLayout());

        FormPass3.setBackground(new java.awt.Color(255, 255, 255));
        FormPass3.setBorder(null);
        FormPass3.setName("FormPass3"); // NOI18N
        FormPass3.setPreferredSize(new java.awt.Dimension(115, 40));

        BtnRefreshPhoto1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto1.setMnemonic('U');
        BtnRefreshPhoto1.setText("Refresh");
        BtnRefreshPhoto1.setToolTipText("Alt+U");
        BtnRefreshPhoto1.setName("BtnRefreshPhoto1"); // NOI18N
        BtnRefreshPhoto1.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto1ActionPerformed(evt);
            }
        });
        FormPass3.add(BtnRefreshPhoto1);

        FormTelaah.add(FormPass3, java.awt.BorderLayout.PAGE_END);

        Scroll5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll5.setName("Scroll5"); // NOI18N
        Scroll5.setOpaque(true);
        Scroll5.setPreferredSize(new java.awt.Dimension(200, 200));

        tbChecklist.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbChecklist.setComponentPopupMenu(Popup2);
        tbChecklist.setName("tbChecklist"); // NOI18N
        tbChecklist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbChecklistMouseClicked(evt);
            }
        });
        tbChecklist.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbChecklistKeyPressed(evt);
            }
        });
        Scroll5.setViewportView(tbChecklist);

        FormTelaah.add(Scroll5, java.awt.BorderLayout.CENTER);

        TabData.addTab("Data Checklist Kelengkapan", FormTelaah);

        PanelAccor.add(TabData, java.awt.BorderLayout.CENTER);

        internalFrame3.add(PanelAccor, java.awt.BorderLayout.EAST);

        TabOK.addTab("KELENGKAPAN ANESTESI", internalFrame3);

        internalFrame4.setBackground(new java.awt.Color(255, 255, 255));
        internalFrame4.setBorder(null);
        internalFrame4.setName("internalFrame4"); // NOI18N
        internalFrame4.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi6.setName("panelisi6"); // NOI18N
        panelisi6.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        label11.setText("Key Word :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi6.add(label11);

        TCari1.setEditable(false);
        TCari1.setName("TCari1"); // NOI18N
        TCari1.setPreferredSize(new java.awt.Dimension(350, 23));
        TCari1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCari1KeyPressed(evt);
            }
        });
        panelisi6.add(TCari1);

        BtnCari1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari1.setMnemonic('1');
        BtnCari1.setToolTipText("Alt+1");
        BtnCari1.setName("BtnCari1"); // NOI18N
        BtnCari1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCari1ActionPerformed(evt);
            }
        });
        BtnCari1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCari1KeyPressed(evt);
            }
        });
        panelisi6.add(BtnCari1);

        BtnAll1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll1.setMnemonic('2');
        BtnAll1.setToolTipText("Alt+2");
        BtnAll1.setName("BtnAll1"); // NOI18N
        BtnAll1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAll1ActionPerformed(evt);
            }
        });
        BtnAll1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAll1KeyPressed(evt);
            }
        });
        panelisi6.add(BtnAll1);

        label12.setText("Record :");
        label12.setName("label12"); // NOI18N
        label12.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi6.add(label12);

        LCount1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount1.setText("0");
        LCount1.setName("LCount1"); // NOI18N
        LCount1.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi6.add(LCount1);

        internalFrame4.add(panelisi6, java.awt.BorderLayout.PAGE_END);

        scrollInput2.setName("scrollInput2"); // NOI18N
        scrollInput2.setPreferredSize(new java.awt.Dimension(102, 200));

        FormInput2.setBackground(new java.awt.Color(255, 255, 255));
        FormInput2.setBorder(null);
        FormInput2.setName("FormInput2"); // NOI18N
        FormInput2.setPreferredSize(new java.awt.Dimension(870, 1450));
        FormInput2.setLayout(null);

        TNoRw2.setHighlighter(null);
        TNoRw2.setName("TNoRw2"); // NOI18N
        TNoRw2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRw2KeyPressed(evt);
            }
        });
        FormInput2.add(TNoRw2);
        TNoRw2.setBounds(74, 10, 131, 23);

        TPasien2.setEditable(false);
        TPasien2.setHighlighter(null);
        TPasien2.setName("TPasien2"); // NOI18N
        FormInput2.add(TPasien2);
        TPasien2.setBounds(309, 10, 260, 23);

        TNoRM2.setEditable(false);
        TNoRM2.setHighlighter(null);
        TNoRM2.setName("TNoRM2"); // NOI18N
        FormInput2.add(TNoRM2);
        TNoRM2.setBounds(207, 10, 100, 23);

        label15.setText("Rencana Operasi :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput2.add(label15);
        label15.setBounds(380, 140, 110, 23);

        KdPetugas1.setEditable(false);
        KdPetugas1.setName("KdPetugas1"); // NOI18N
        KdPetugas1.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugas1KeyPressed(evt);
            }
        });
        FormInput2.add(KdPetugas1);
        KdPetugas1.setBounds(100, 40, 100, 23);

        NmPetugas1.setEditable(false);
        NmPetugas1.setName("NmPetugas1"); // NOI18N
        NmPetugas1.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput2.add(NmPetugas1);
        NmPetugas1.setBounds(200, 40, 180, 23);

        BtnPetugas1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas1.setMnemonic('2');
        BtnPetugas1.setToolTipText("Alt+2");
        BtnPetugas1.setName("BtnPetugas1"); // NOI18N
        BtnPetugas1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugas1ActionPerformed(evt);
            }
        });
        BtnPetugas1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugas1KeyPressed(evt);
            }
        });
        FormInput2.add(BtnPetugas1);
        BtnPetugas1.setBounds(380, 40, 28, 23);

        jLabel12.setText("No.Rawat :");
        jLabel12.setName("jLabel12"); // NOI18N
        FormInput2.add(jLabel12);
        jLabel12.setBounds(0, 10, 70, 23);

        jSeparator7.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator7.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator7.setName("jSeparator7"); // NOI18N
        FormInput2.add(jSeparator7);
        jSeparator7.setBounds(0, 70, 880, 1);

        jSeparator8.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator8.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator8.setName("jSeparator8"); // NOI18N
        FormInput2.add(jSeparator8);
        jSeparator8.setBounds(0, 294, 880, 3);

        label38.setText("TB :");
        label38.setName("label38"); // NOI18N
        label38.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label38);
        label38.setBounds(500, 190, 30, 23);

        tb.setHighlighter(null);
        tb.setName("tb"); // NOI18N
        tb.setPreferredSize(new java.awt.Dimension(50, 23));
        tb.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tbMouseMoved(evt);
            }
        });
        tb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tbMouseExited(evt);
            }
        });
        tb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbKeyPressed(evt);
            }
        });
        FormInput2.add(tb);
        tb.setBounds(530, 190, 50, 23);

        label45.setText("KG");
        label45.setName("label45"); // NOI18N
        label45.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label45);
        label45.setBounds(700, 190, 20, 23);

        bb.setName("bb"); // NOI18N
        bb.setPreferredSize(new java.awt.Dimension(50, 23));
        bb.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                bbMouseMoved(evt);
            }
        });
        bb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                bbMouseExited(evt);
            }
        });
        bb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bbActionPerformed(evt);
            }
        });
        bb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bbKeyPressed(evt);
            }
        });
        FormInput2.add(bb);
        bb.setBounds(650, 190, 50, 23);

        label152.setText("Riwayat Anestesi :");
        label152.setName("label152"); // NOI18N
        label152.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label152);
        label152.setBounds(30, 380, 90, 23);

        obat_text.setName("obat_text"); // NOI18N
        obat_text.setPreferredSize(new java.awt.Dimension(50, 23));
        obat_text.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                obat_textMouseMoved(evt);
            }
        });
        obat_text.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                obat_textMouseExited(evt);
            }
        });
        obat_text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                obat_textKeyPressed(evt);
            }
        });
        FormInput2.add(obat_text);
        obat_text.setBounds(500, 250, 220, 23);

        jLabel18.setText("Tanggal :");
        jLabel18.setName("jLabel18"); // NOI18N
        jLabel18.setVerifyInputWhenFocusTarget(false);
        FormInput2.add(jLabel18);
        jLabel18.setBounds(405, 40, 50, 23);

        Tanggal1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "03-03-2025" }));
        Tanggal1.setDisplayFormat("dd-MM-yyyy");
        Tanggal1.setName("Tanggal1"); // NOI18N
        Tanggal1.setOpaque(false);
        Tanggal1.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput2.add(Tanggal1);
        Tanggal1.setBounds(460, 40, 110, 23);

        Jam1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam1.setName("Jam1"); // NOI18N
        Jam1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam1ActionPerformed(evt);
            }
        });
        Jam1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam1KeyPressed(evt);
            }
        });
        FormInput2.add(Jam1);
        Jam1.setBounds(580, 40, 50, 23);

        Menit1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit1.setName("Menit1"); // NOI18N
        Menit1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit1KeyPressed(evt);
            }
        });
        FormInput2.add(Menit1);
        Menit1.setBounds(640, 40, 50, 23);

        Detik1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik1.setName("Detik1"); // NOI18N
        Detik1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik1KeyPressed(evt);
            }
        });
        FormInput2.add(Detik1);
        Detik1.setBounds(700, 40, 50, 23);

        ChkJam2.setBorder(null);
        ChkJam2.setSelected(true);
        ChkJam2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam2.setName("ChkJam2"); // NOI18N
        FormInput2.add(ChkJam2);
        ChkJam2.setBounds(750, 40, 23, 23);

        jk1.setEditable(false);
        jk1.setName("jk1"); // NOI18N
        jk1.setPreferredSize(new java.awt.Dimension(50, 23));
        jk1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jk1MouseMoved(evt);
            }
        });
        jk1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jk1MouseExited(evt);
            }
        });
        jk1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jk1ActionPerformed(evt);
            }
        });
        jk1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jk1KeyPressed(evt);
            }
        });
        FormInput2.add(jk1);
        jk1.setBounds(580, 10, 50, 23);

        scrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane1.setName("scrollPane1"); // NOI18N

        Rencanaop.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Rencanaop.setColumns(20);
        Rencanaop.setRows(5);
        Rencanaop.setName("Rencanaop"); // NOI18N
        Rencanaop.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RencanaopKeyPressed(evt);
            }
        });
        scrollPane1.setViewportView(Rencanaop);

        FormInput2.add(scrollPane1);
        scrollPane1.setBounds(500, 140, 220, 40);

        label18.setText("Dokter Anestesi :");
        label18.setName("label18"); // NOI18N
        label18.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput2.add(label18);
        label18.setBounds(0, 40, 90, 23);

        label19.setText("Anamnesis :");
        label19.setName("label19"); // NOI18N
        label19.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput2.add(label19);
        label19.setBounds(50, 90, 70, 23);

        label20.setText("Diagnosa Pre Operasi :");
        label20.setName("label20"); // NOI18N
        label20.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput2.add(label20);
        label20.setBounds(380, 90, 110, 23);

        scrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane2.setName("scrollPane2"); // NOI18N

        Anamnesis.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Anamnesis.setColumns(20);
        Anamnesis.setRows(5);
        Anamnesis.setName("Anamnesis"); // NOI18N
        Anamnesis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AnamnesisKeyPressed(evt);
            }
        });
        scrollPane2.setViewportView(Anamnesis);

        FormInput2.add(scrollPane2);
        scrollPane2.setBounds(130, 90, 220, 190);

        scrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane3.setName("scrollPane3"); // NOI18N

        Preop.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Preop.setColumns(20);
        Preop.setRows(5);
        Preop.setName("Preop"); // NOI18N
        Preop.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PreopKeyPressed(evt);
            }
        });
        scrollPane3.setViewportView(Preop);

        FormInput2.add(scrollPane3);
        scrollPane3.setBounds(500, 90, 220, 40);

        label46.setText("BB :");
        label46.setName("label46"); // NOI18N
        label46.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label46);
        label46.setBounds(610, 190, 40, 23);

        label47.setText("CM");
        label47.setName("label47"); // NOI18N
        label47.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label47);
        label47.setBounds(570, 190, 30, 23);

        CmbObat.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak Ada", "Ada" }));
        CmbObat.setName("CmbObat"); // NOI18N
        CmbObat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbObatActionPerformed(evt);
            }
        });
        CmbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CmbObatKeyPressed(evt);
            }
        });
        FormInput2.add(CmbObat);
        CmbObat.setBounds(400, 250, 80, 23);

        label153.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label153.setText("Obat yang sedang dikonsumsi saat ini :");
        label153.setName("label153"); // NOI18N
        label153.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label153);
        label153.setBounds(400, 220, 190, 23);

        label154.setText("PERENCANAAN TINDAKAN ANESTESI");
        label154.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        label154.setName("label154"); // NOI18N
        label154.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label154);
        label154.setBounds(20, 1060, 200, 23);

        label155.setText("Riwayat Penyakit :");
        label155.setName("label155"); // NOI18N
        label155.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label155);
        label155.setBounds(30, 350, 90, 23);

        CmbAnestesi2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "SAB", "GA ETT", "GA LMA", "GA FM", "TIVA" }));
        CmbAnestesi2.setName("CmbAnestesi2"); // NOI18N
        CmbAnestesi2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbAnestesi2ActionPerformed(evt);
            }
        });
        CmbAnestesi2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CmbAnestesi2KeyPressed(evt);
            }
        });
        FormInput2.add(CmbAnestesi2);
        CmbAnestesi2.setBounds(220, 380, 80, 20);

        CmbAlergi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak", "Ya" }));
        CmbAlergi.setName("CmbAlergi"); // NOI18N
        CmbAlergi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbAlergiActionPerformed(evt);
            }
        });
        CmbAlergi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CmbAlergiKeyPressed(evt);
            }
        });
        FormInput2.add(CmbAlergi);
        CmbAlergi.setBounds(130, 320, 80, 20);

        CmbPenyakit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak", "Ya" }));
        CmbPenyakit.setName("CmbPenyakit"); // NOI18N
        CmbPenyakit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbPenyakitActionPerformed(evt);
            }
        });
        CmbPenyakit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CmbPenyakitKeyPressed(evt);
            }
        });
        FormInput2.add(CmbPenyakit);
        CmbPenyakit.setBounds(130, 350, 80, 20);

        label156.setText("Komplikasi Anestesi :");
        label156.setName("label156"); // NOI18N
        label156.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label156);
        label156.setBounds(450, 350, 110, 23);

        CmbMerokok.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak", "Ya" }));
        CmbMerokok.setName("CmbMerokok"); // NOI18N
        CmbMerokok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbMerokokActionPerformed(evt);
            }
        });
        CmbMerokok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CmbMerokokKeyPressed(evt);
            }
        });
        FormInput2.add(CmbMerokok);
        CmbMerokok.setBounds(570, 320, 90, 20);

        CmbAnestesi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak", "Ya" }));
        CmbAnestesi.setName("CmbAnestesi"); // NOI18N
        CmbAnestesi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbAnestesiActionPerformed(evt);
            }
        });
        CmbAnestesi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CmbAnestesiKeyPressed(evt);
            }
        });
        FormInput2.add(CmbAnestesi);
        CmbAnestesi.setBounds(130, 380, 80, 20);

        Komplikasi.setHighlighter(null);
        Komplikasi.setName("Komplikasi"); // NOI18N
        Komplikasi.setPreferredSize(new java.awt.Dimension(50, 23));
        Komplikasi.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                KomplikasiMouseMoved(evt);
            }
        });
        Komplikasi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                KomplikasiMouseExited(evt);
            }
        });
        Komplikasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KomplikasiKeyPressed(evt);
            }
        });
        FormInput2.add(Komplikasi);
        Komplikasi.setBounds(570, 350, 190, 23);

        label157.setText("Riwayat Merokok :");
        label157.setName("label157"); // NOI18N
        label157.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label157);
        label157.setBounds(450, 320, 110, 23);

        jSeparator9.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator9.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator9.setName("jSeparator9"); // NOI18N
        FormInput2.add(jSeparator9);
        jSeparator9.setBounds(0, 431, 880, 3);

        label158.setText("Riwayat Alergi :");
        label158.setName("label158"); // NOI18N
        label158.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label158);
        label158.setBounds(40, 320, 80, 23);

        label159.setText("PEMERIKSAAN PENUNJANG");
        label159.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        label159.setName("label159"); // NOI18N
        label159.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label159);
        label159.setBounds(0, 780, 150, 23);

        label160.setText("Alat Pembebas Jalan Nafas :");
        label160.setName("label160"); // NOI18N
        label160.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label160);
        label160.setBounds(230, 470, 150, 23);

        label161.setText("Vesikuler :");
        label161.setName("label161"); // NOI18N
        label161.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label161);
        label161.setBounds(140, 500, 50, 23);

        label162.setText("B3 / Brain :");
        label162.setName("label162"); // NOI18N
        label162.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label162);
        label162.setBounds(30, 590, 100, 23);

        label163.setText("B4 / Bladder :");
        label163.setName("label163"); // NOI18N
        label163.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label163);
        label163.setBounds(30, 620, 100, 23);

        label164.setText("B5 / Bowel :");
        label164.setName("label164"); // NOI18N
        label164.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label164);
        label164.setBounds(30, 650, 100, 23);

        jSeparator10.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator10.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator10.setName("jSeparator10"); // NOI18N
        FormInput2.add(jSeparator10);
        jSeparator10.setBounds(0, 770, 880, 3);

        label165.setText("PEMERIKSAAN FISIK");
        label165.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        label165.setName("label165"); // NOI18N
        label165.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label165);
        label165.setBounds(10, 440, 120, 23);

        label166.setText("B6 / Bone :");
        label166.setName("label166"); // NOI18N
        label166.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label166);
        label166.setBounds(30, 680, 100, 23);

        label167.setText("Laboratorium :");
        label167.setName("label167"); // NOI18N
        label167.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label167);
        label167.setBounds(20, 810, 100, 23);

        label168.setText("Radiologi :");
        label168.setName("label168"); // NOI18N
        label168.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label168);
        label168.setBounds(20, 870, 100, 23);

        jSeparator13.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator13.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator13.setName("jSeparator13"); // NOI18N
        FormInput2.add(jSeparator13);
        jSeparator13.setBounds(0, 987, 880, 3);

        jSeparator14.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator14.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator14.setName("jSeparator14"); // NOI18N
        FormInput2.add(jSeparator14);
        jSeparator14.setBounds(0, 1050, 880, 3);

        label169.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label169.setText("Rencana Tindakan Anestesi :");
        label169.setName("label169"); // NOI18N
        label169.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label169);
        label169.setBounds(30, 1090, 160, 23);

        label170.setText("Elektrokardiografi :");
        label170.setName("label170"); // NOI18N
        label170.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label170);
        label170.setBounds(20, 930, 100, 23);

        Asa.setBorder(null);
        Asa.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Asa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Asa.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Asa.setName("Asa"); // NOI18N
        FormInput2.add(Asa);
        Asa.setBounds(60, 1020, 23, 23);

        label171.setText("Asa 1 :");
        label171.setName("label171"); // NOI18N
        label171.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label171);
        label171.setBounds(90, 1020, 40, 23);

        Asa1.setBorder(null);
        Asa1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Asa1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Asa1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Asa1.setName("Asa1"); // NOI18N
        FormInput2.add(Asa1);
        Asa1.setBounds(130, 1020, 23, 23);

        label172.setText("Asa 2 :");
        label172.setName("label172"); // NOI18N
        label172.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label172);
        label172.setBounds(160, 1020, 40, 23);

        Asa2.setBorder(null);
        Asa2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Asa2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Asa2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Asa2.setName("Asa2"); // NOI18N
        FormInput2.add(Asa2);
        Asa2.setBounds(200, 1020, 23, 23);

        Asa3.setBorder(null);
        Asa3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Asa3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Asa3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Asa3.setName("Asa3"); // NOI18N
        FormInput2.add(Asa3);
        Asa3.setBounds(270, 1020, 23, 23);

        label173.setText("Asa 3 :");
        label173.setName("label173"); // NOI18N
        label173.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label173);
        label173.setBounds(230, 1020, 40, 23);

        Asa4.setBorder(null);
        Asa4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Asa4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Asa4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Asa4.setName("Asa4"); // NOI18N
        FormInput2.add(Asa4);
        Asa4.setBounds(340, 1020, 23, 23);

        label174.setText("Asa 4 :");
        label174.setName("label174"); // NOI18N
        label174.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label174);
        label174.setBounds(300, 1020, 40, 23);

        label175.setText("Asa 5 :");
        label175.setName("label175"); // NOI18N
        label175.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label175);
        label175.setBounds(370, 1020, 40, 23);

        Asa5.setBorder(null);
        Asa5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Asa5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Asa5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Asa5.setName("Asa5"); // NOI18N
        FormInput2.add(Asa5);
        Asa5.setBounds(410, 1020, 23, 23);

        Asa6.setBorder(null);
        Asa6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Asa6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Asa6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Asa6.setName("Asa6"); // NOI18N
        FormInput2.add(Asa6);
        Asa6.setBounds(480, 1020, 23, 23);

        label176.setText("Asa 6 :");
        label176.setName("label176"); // NOI18N
        label176.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label176);
        label176.setBounds(440, 1020, 40, 23);

        Asa7.setBorder(null);
        Asa7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Asa7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Asa7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Asa7.setName("Asa7"); // NOI18N
        FormInput2.add(Asa7);
        Asa7.setBounds(530, 1020, 23, 23);

        label177.setText("E :");
        label177.setName("label177"); // NOI18N
        label177.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label177);
        label177.setBounds(510, 1020, 20, 23);

        label178.setText("STATUS FISIK");
        label178.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        label178.setName("label178"); // NOI18N
        label178.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label178);
        label178.setBounds(20, 990, 80, 23);

        label179.setText("( Cairan ) :");
        label179.setName("label179"); // NOI18N
        label179.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label179);
        label179.setBounds(120, 1280, 130, 23);

        label185.setText("Asa :");
        label185.setName("label185"); // NOI18N
        label185.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label185);
        label185.setBounds(10, 1020, 40, 23);

        label186.setText("GA :");
        label186.setName("label186"); // NOI18N
        label186.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label186);
        label186.setBounds(50, 1110, 40, 23);

        label187.setText("REGIONAL :");
        label187.setName("label187"); // NOI18N
        label187.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label187);
        label187.setBounds(20, 1140, 70, 23);

        label188.setText("Prosedur Monitoring Khusus Saat Tindakan Anestesi :");
        label188.setName("label188"); // NOI18N
        label188.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label188);
        label188.setBounds(20, 1320, 260, 23);

        label189.setText("ALAT / BAHAN KHSUSUS YANG DIPERLUKAN :");
        label189.setName("label189"); // NOI18N
        label189.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label189);
        label189.setBounds(20, 1230, 230, 23);

        jSeparator15.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator15.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator15.setName("jSeparator15"); // NOI18N
        FormInput2.add(jSeparator15);
        jSeparator15.setBounds(0, 1314, 880, 3);

        label190.setText("BLOK :");
        label190.setName("label190"); // NOI18N
        label190.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label190);
        label190.setBounds(20, 1180, 70, 23);

        Cmbmonitoring.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tidak", "Ya" }));
        Cmbmonitoring.setName("Cmbmonitoring"); // NOI18N
        Cmbmonitoring.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbmonitoringActionPerformed(evt);
            }
        });
        Cmbmonitoring.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CmbmonitoringKeyPressed(evt);
            }
        });
        FormInput2.add(Cmbmonitoring);
        Cmbmonitoring.setBounds(30, 1350, 90, 20);

        label191.setText("Rencana Perawatan Setelah Tindakan :");
        label191.setName("label191"); // NOI18N
        label191.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label191);
        label191.setBounds(20, 1380, 190, 23);

        label192.setText("Rawat Inap");
        label192.setName("label192"); // NOI18N
        label192.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label192);
        label192.setBounds(40, 1410, 60, 23);

        rencana_inap.setBorder(null);
        rencana_inap.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        rencana_inap.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rencana_inap.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        rencana_inap.setName("rencana_inap"); // NOI18N
        FormInput2.add(rencana_inap);
        rencana_inap.setBounds(20, 1410, 23, 23);

        label193.setText("HCU");
        label193.setName("label193"); // NOI18N
        label193.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label193);
        label193.setBounds(130, 1410, 30, 23);

        rencana_hcu.setBorder(null);
        rencana_hcu.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        rencana_hcu.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rencana_hcu.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        rencana_hcu.setName("rencana_hcu"); // NOI18N
        FormInput2.add(rencana_hcu);
        rencana_hcu.setBounds(110, 1410, 23, 23);

        label194.setText("ICU/PICU/NICU");
        label194.setName("label194"); // NOI18N
        label194.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label194);
        label194.setBounds(200, 1410, 80, 23);

        rencana_icu.setBorder(null);
        rencana_icu.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        rencana_icu.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rencana_icu.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        rencana_icu.setName("rencana_icu"); // NOI18N
        FormInput2.add(rencana_icu);
        rencana_icu.setBounds(180, 1410, 23, 23);

        label195.setText("Rawat Jalan");
        label195.setName("label195"); // NOI18N
        label195.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label195);
        label195.setBounds(330, 1410, 60, 23);

        rencana_ralan.setBorder(null);
        rencana_ralan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        rencana_ralan.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rencana_ralan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        rencana_ralan.setName("rencana_ralan"); // NOI18N
        FormInput2.add(rencana_ralan);
        rencana_ralan.setBounds(300, 1410, 23, 23);

        label196.setText("IGD");
        label196.setName("label196"); // NOI18N
        label196.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label196);
        label196.setBounds(430, 1410, 20, 23);

        rencana_igd.setBorder(null);
        rencana_igd.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        rencana_igd.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rencana_igd.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        rencana_igd.setName("rencana_igd"); // NOI18N
        FormInput2.add(rencana_igd);
        rencana_igd.setBounds(400, 1410, 23, 23);

        B3_cmbpupil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Isokor", "Anisokor" }));
        B3_cmbpupil.setName("B3_cmbpupil"); // NOI18N
        B3_cmbpupil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B3_cmbpupilActionPerformed(evt);
            }
        });
        B3_cmbpupil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B3_cmbpupilKeyPressed(evt);
            }
        });
        FormInput2.add(B3_cmbpupil);
        B3_cmbpupil.setBounds(470, 590, 80, 20);

        label197.setText("B1 / Breathing :");
        label197.setName("label197"); // NOI18N
        label197.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label197);
        label197.setBounds(30, 470, 100, 23);

        B1_alat.setHighlighter(null);
        B1_alat.setName("B1_alat"); // NOI18N
        B1_alat.setPreferredSize(new java.awt.Dimension(50, 23));
        B1_alat.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B1_alatMouseMoved(evt);
            }
        });
        B1_alat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B1_alatMouseExited(evt);
            }
        });
        B1_alat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B1_alatKeyPressed(evt);
            }
        });
        FormInput2.add(B1_alat);
        B1_alat.setBounds(390, 470, 230, 23);

        label39.setText("RR :");
        label39.setName("label39"); // NOI18N
        label39.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label39);
        label39.setBounds(630, 470, 30, 23);

        B1_vesikuler.setHighlighter(null);
        B1_vesikuler.setName("B1_vesikuler"); // NOI18N
        B1_vesikuler.setPreferredSize(new java.awt.Dimension(50, 23));
        B1_vesikuler.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B1_vesikulerMouseMoved(evt);
            }
        });
        B1_vesikuler.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B1_vesikulerMouseExited(evt);
            }
        });
        B1_vesikuler.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B1_vesikulerKeyPressed(evt);
            }
        });
        FormInput2.add(B1_vesikuler);
        B1_vesikuler.setBounds(200, 500, 90, 23);

        label48.setText("x / Menit");
        label48.setName("label48"); // NOI18N
        label48.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label48);
        label48.setBounds(700, 470, 60, 23);

        label201.setText("B2 / Blood :");
        label201.setName("label201"); // NOI18N
        label201.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label201);
        label201.setBounds(30, 560, 100, 23);

        B1_rr.setHighlighter(null);
        B1_rr.setName("B1_rr"); // NOI18N
        B1_rr.setPreferredSize(new java.awt.Dimension(50, 23));
        B1_rr.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B1_rrMouseMoved(evt);
            }
        });
        B1_rr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B1_rrMouseExited(evt);
            }
        });
        B1_rr.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B1_rrKeyPressed(evt);
            }
        });
        FormInput2.add(B1_rr);
        B1_rr.setBounds(660, 470, 50, 23);

        label202.setText("Rhonki :");
        label202.setName("label202"); // NOI18N
        label202.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label202);
        label202.setBounds(290, 500, 50, 23);

        B1_rhonki.setHighlighter(null);
        B1_rhonki.setName("B1_rhonki"); // NOI18N
        B1_rhonki.setPreferredSize(new java.awt.Dimension(50, 23));
        B1_rhonki.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B1_rhonkiMouseMoved(evt);
            }
        });
        B1_rhonki.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B1_rhonkiMouseExited(evt);
            }
        });
        B1_rhonki.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B1_rhonkiKeyPressed(evt);
            }
        });
        FormInput2.add(B1_rhonki);
        B1_rhonki.setBounds(350, 500, 90, 23);

        label204.setText("-");
        label204.setName("label204"); // NOI18N
        label204.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label204);
        label204.setBounds(520, 500, 20, 23);

        B1_wheezing_plus.setHighlighter(null);
        B1_wheezing_plus.setName("B1_wheezing_plus"); // NOI18N
        B1_wheezing_plus.setPreferredSize(new java.awt.Dimension(50, 23));
        B1_wheezing_plus.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B1_wheezing_plusMouseMoved(evt);
            }
        });
        B1_wheezing_plus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B1_wheezing_plusMouseExited(evt);
            }
        });
        B1_wheezing_plus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B1_wheezing_plusKeyPressed(evt);
            }
        });
        FormInput2.add(B1_wheezing_plus);
        B1_wheezing_plus.setBounds(670, 500, 90, 23);

        label40.setText("TD :");
        label40.setName("label40"); // NOI18N
        label40.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label40);
        label40.setBounds(130, 560, 30, 23);

        B2_td.setHighlighter(null);
        B2_td.setName("B2_td"); // NOI18N
        B2_td.setPreferredSize(new java.awt.Dimension(50, 23));
        B2_td.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B2_tdMouseMoved(evt);
            }
        });
        B2_td.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B2_tdMouseExited(evt);
            }
        });
        B2_td.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B2_tdKeyPressed(evt);
            }
        });
        FormInput2.add(B2_td);
        B2_td.setBounds(160, 560, 50, 23);

        label41.setText("Konjungtiva :");
        label41.setName("label41"); // NOI18N
        label41.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label41);
        label41.setBounds(380, 560, 80, 23);

        B2_hr.setHighlighter(null);
        B2_hr.setName("B2_hr"); // NOI18N
        B2_hr.setPreferredSize(new java.awt.Dimension(50, 23));
        B2_hr.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B2_hrMouseMoved(evt);
            }
        });
        B2_hr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B2_hrMouseExited(evt);
            }
        });
        B2_hr.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B2_hrKeyPressed(evt);
            }
        });
        FormInput2.add(B2_hr);
        B2_hr.setBounds(240, 560, 50, 23);

        CmbB1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Clear", "Unclear" }));
        CmbB1.setName("CmbB1"); // NOI18N
        CmbB1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbB1ActionPerformed(evt);
            }
        });
        CmbB1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CmbB1KeyPressed(evt);
            }
        });
        FormInput2.add(CmbB1);
        CmbB1.setBounds(140, 470, 80, 20);

        label42.setText("HR :");
        label42.setName("label42"); // NOI18N
        label42.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label42);
        label42.setBounds(210, 560, 30, 23);

        B2_cmb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Reguler", "Irreguler" }));
        B2_cmb.setName("B2_cmb"); // NOI18N
        B2_cmb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B2_cmbActionPerformed(evt);
            }
        });
        B2_cmb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B2_cmbKeyPressed(evt);
            }
        });
        FormInput2.add(B2_cmb);
        B2_cmb.setBounds(300, 560, 80, 20);

        label43.setText("E :");
        label43.setName("label43"); // NOI18N
        label43.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label43);
        label43.setBounds(170, 590, 30, 23);

        B3_e.setHighlighter(null);
        B3_e.setName("B3_e"); // NOI18N
        B3_e.setPreferredSize(new java.awt.Dimension(50, 23));
        B3_e.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B3_eMouseMoved(evt);
            }
        });
        B3_e.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B3_eMouseExited(evt);
            }
        });
        B3_e.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B3_eKeyPressed(evt);
            }
        });
        FormInput2.add(B3_e);
        B3_e.setBounds(200, 590, 50, 23);

        label44.setText("Warna Urin :");
        label44.setName("label44"); // NOI18N
        label44.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label44);
        label44.setBounds(380, 620, 80, 23);

        label49.setText("CC / Jam");
        label49.setName("label49"); // NOI18N
        label49.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label49);
        label49.setBounds(280, 620, 50, 23);

        B4_urin.setHighlighter(null);
        B4_urin.setName("B4_urin"); // NOI18N
        B4_urin.setPreferredSize(new java.awt.Dimension(50, 23));
        B4_urin.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B4_urinMouseMoved(evt);
            }
        });
        B4_urin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B4_urinMouseExited(evt);
            }
        });
        B4_urin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B4_urinKeyPressed(evt);
            }
        });
        FormInput2.add(B4_urin);
        B4_urin.setBounds(230, 620, 50, 23);

        label50.setText("Pupil :");
        label50.setName("label50"); // NOI18N
        label50.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label50);
        label50.setBounds(440, 590, 30, 23);

        B3_v.setHighlighter(null);
        B3_v.setName("B3_v"); // NOI18N
        B3_v.setPreferredSize(new java.awt.Dimension(50, 23));
        B3_v.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B3_vMouseMoved(evt);
            }
        });
        B3_v.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B3_vMouseExited(evt);
            }
        });
        B3_v.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B3_vKeyPressed(evt);
            }
        });
        FormInput2.add(B3_v);
        B3_v.setBounds(380, 590, 50, 23);

        label51.setText("V :");
        label51.setName("label51"); // NOI18N
        label51.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label51);
        label51.setBounds(350, 590, 30, 23);

        B2_cmbkonjungtiva.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Anemis", "Tidak" }));
        B2_cmbkonjungtiva.setName("B2_cmbkonjungtiva"); // NOI18N
        B2_cmbkonjungtiva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B2_cmbkonjungtivaActionPerformed(evt);
            }
        });
        B2_cmbkonjungtiva.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B2_cmbkonjungtivaKeyPressed(evt);
            }
        });
        FormInput2.add(B2_cmbkonjungtiva);
        B2_cmbkonjungtiva.setBounds(470, 560, 80, 20);

        label52.setText("Hemiparese :");
        label52.setName("label52"); // NOI18N
        label52.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label52);
        label52.setBounds(560, 590, 80, 23);

        B4_cmbwarnaurin.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Jernih", "Keruh" }));
        B4_cmbwarnaurin.setName("B4_cmbwarnaurin"); // NOI18N
        B4_cmbwarnaurin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B4_cmbwarnaurinActionPerformed(evt);
            }
        });
        B4_cmbwarnaurin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B4_cmbwarnaurinKeyPressed(evt);
            }
        });
        FormInput2.add(B4_cmbwarnaurin);
        B4_cmbwarnaurin.setBounds(470, 620, 80, 20);

        label53.setText("GCS :");
        label53.setName("label53"); // NOI18N
        label53.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label53);
        label53.setBounds(150, 590, 30, 23);

        B3_m.setHighlighter(null);
        B3_m.setName("B3_m"); // NOI18N
        B3_m.setPreferredSize(new java.awt.Dimension(50, 23));
        B3_m.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B3_mMouseMoved(evt);
            }
        });
        B3_m.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B3_mMouseExited(evt);
            }
        });
        B3_m.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B3_mKeyPressed(evt);
            }
        });
        FormInput2.add(B3_m);
        B3_m.setBounds(290, 590, 50, 23);

        label54.setText("M :");
        label54.setName("label54"); // NOI18N
        label54.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label54);
        label54.setBounds(260, 590, 30, 23);

        label55.setText("Alat Bantu Jalan :");
        label55.setName("label55"); // NOI18N
        label55.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label55);
        label55.setBounds(130, 680, 90, 23);

        B5_cmbkembung.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        B5_cmbkembung.setName("B5_cmbkembung"); // NOI18N
        B5_cmbkembung.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B5_cmbkembungActionPerformed(evt);
            }
        });
        B5_cmbkembung.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B5_cmbkembungKeyPressed(evt);
            }
        });
        FormInput2.add(B5_cmbkembung);
        B5_cmbkembung.setBounds(220, 650, 80, 20);

        label56.setText("Produksi Urin :");
        label56.setName("label56"); // NOI18N
        label56.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label56);
        label56.setBounds(140, 620, 80, 23);

        B3_cmbhemiparese.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        B3_cmbhemiparese.setName("B3_cmbhemiparese"); // NOI18N
        B3_cmbhemiparese.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B3_cmbhemipareseActionPerformed(evt);
            }
        });
        B3_cmbhemiparese.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B3_cmbhemipareseKeyPressed(evt);
            }
        });
        FormInput2.add(B3_cmbhemiparese);
        B3_cmbhemiparese.setBounds(650, 590, 80, 20);

        label57.setText("Diare :");
        label57.setName("label57"); // NOI18N
        label57.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label57);
        label57.setBounds(310, 650, 40, 23);

        B6_cmbalatbantu.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        B6_cmbalatbantu.setName("B6_cmbalatbantu"); // NOI18N
        B6_cmbalatbantu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B6_cmbalatbantuActionPerformed(evt);
            }
        });
        B6_cmbalatbantu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B6_cmbalatbantuKeyPressed(evt);
            }
        });
        FormInput2.add(B6_cmbalatbantu);
        B6_cmbalatbantu.setBounds(220, 680, 80, 20);

        B5_cmbmuntah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        B5_cmbmuntah.setName("B5_cmbmuntah"); // NOI18N
        B5_cmbmuntah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B5_cmbmuntahActionPerformed(evt);
            }
        });
        B5_cmbmuntah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B5_cmbmuntahKeyPressed(evt);
            }
        });
        FormInput2.add(B5_cmbmuntah);
        B5_cmbmuntah.setBounds(520, 650, 80, 20);

        label58.setText("Muntah :");
        label58.setName("label58"); // NOI18N
        label58.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label58);
        label58.setBounds(460, 650, 50, 23);

        label59.setText("Perut Kembung :");
        label59.setName("label59"); // NOI18N
        label59.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label59);
        label59.setBounds(140, 650, 80, 23);

        B5_cmbdiare.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        B5_cmbdiare.setName("B5_cmbdiare"); // NOI18N
        B5_cmbdiare.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B5_cmbdiareActionPerformed(evt);
            }
        });
        B5_cmbdiare.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B5_cmbdiareKeyPressed(evt);
            }
        });
        FormInput2.add(B5_cmbdiare);
        B5_cmbdiare.setBounds(360, 650, 80, 20);

        label60.setText("Fraktur :");
        label60.setName("label60"); // NOI18N
        label60.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput2.add(label60);
        label60.setBounds(300, 680, 50, 23);

        B6_cmbfraktur.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak" }));
        B6_cmbfraktur.setName("B6_cmbfraktur"); // NOI18N
        B6_cmbfraktur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B6_cmbfrakturActionPerformed(evt);
            }
        });
        B6_cmbfraktur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B6_cmbfrakturKeyPressed(evt);
            }
        });
        FormInput2.add(B6_cmbfraktur);
        B6_cmbfraktur.setBounds(360, 680, 80, 20);

        scrollPane4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane4.setName("scrollPane4"); // NOI18N

        Elektrokardiografi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Elektrokardiografi.setColumns(20);
        Elektrokardiografi.setRows(5);
        Elektrokardiografi.setName("Elektrokardiografi"); // NOI18N
        Elektrokardiografi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ElektrokardiografiKeyPressed(evt);
            }
        });
        scrollPane4.setViewportView(Elektrokardiografi);

        FormInput2.add(scrollPane4);
        scrollPane4.setBounds(130, 930, 580, 50);

        scrollPane5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane5.setName("scrollPane5"); // NOI18N

        Laboratorium.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Laboratorium.setColumns(20);
        Laboratorium.setRows(5);
        Laboratorium.setName("Laboratorium"); // NOI18N
        Laboratorium.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LaboratoriumKeyPressed(evt);
            }
        });
        scrollPane5.setViewportView(Laboratorium);

        FormInput2.add(scrollPane5);
        scrollPane5.setBounds(130, 810, 580, 50);

        scrollPane6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane6.setName("scrollPane6"); // NOI18N

        obat_obatan_text.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        obat_obatan_text.setColumns(20);
        obat_obatan_text.setRows(5);
        obat_obatan_text.setName("obat_obatan_text"); // NOI18N
        obat_obatan_text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                obat_obatan_textKeyPressed(evt);
            }
        });
        scrollPane6.setViewportView(obat_obatan_text);

        FormInput2.add(scrollPane6);
        scrollPane6.setBounds(370, 1240, 310, 30);

        scrollPane7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane7.setName("scrollPane7"); // NOI18N

        Radiologi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Radiologi.setColumns(20);
        Radiologi.setRows(5);
        Radiologi.setName("Radiologi"); // NOI18N
        Radiologi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RadiologiKeyPressed(evt);
            }
        });
        scrollPane7.setViewportView(Radiologi);

        FormInput2.add(scrollPane7);
        scrollPane7.setBounds(130, 870, 580, 50);

        B1_wheezing_min.setHighlighter(null);
        B1_wheezing_min.setName("B1_wheezing_min"); // NOI18N
        B1_wheezing_min.setPreferredSize(new java.awt.Dimension(50, 23));
        B1_wheezing_min.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                B1_wheezing_minMouseMoved(evt);
            }
        });
        B1_wheezing_min.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B1_wheezing_minMouseExited(evt);
            }
        });
        B1_wheezing_min.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B1_wheezing_minKeyPressed(evt);
            }
        });
        FormInput2.add(B1_wheezing_min);
        B1_wheezing_min.setBounds(550, 500, 90, 23);

        label228.setText("Wheezing :");
        label228.setName("label228"); // NOI18N
        label228.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label228);
        label228.setBounds(450, 500, 70, 23);

        label280.setText("+");
        label280.setName("label280"); // NOI18N
        label280.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label280);
        label280.setBounds(640, 500, 20, 23);

        cmb_GA.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "ETT", "LMA", "FM", "TIVA" }));
        cmb_GA.setName("cmb_GA"); // NOI18N
        cmb_GA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_GAActionPerformed(evt);
            }
        });
        cmb_GA.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmb_GAKeyPressed(evt);
            }
        });
        FormInput2.add(cmb_GA);
        cmb_GA.setBounds(100, 1110, 260, 20);

        cmb_REGIONAL.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "SAB", "EPIDURAL" }));
        cmb_REGIONAL.setName("cmb_REGIONAL"); // NOI18N
        cmb_REGIONAL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_REGIONALActionPerformed(evt);
            }
        });
        cmb_REGIONAL.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmb_REGIONALKeyPressed(evt);
            }
        });
        FormInput2.add(cmb_REGIONAL);
        cmb_REGIONAL.setBounds(100, 1140, 260, 20);

        scrollPane8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane8.setName("scrollPane8"); // NOI18N

        anestesi_blok.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        anestesi_blok.setColumns(20);
        anestesi_blok.setRows(5);
        anestesi_blok.setName("anestesi_blok"); // NOI18N
        anestesi_blok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                anestesi_blokKeyPressed(evt);
            }
        });
        scrollPane8.setViewportView(anestesi_blok);

        FormInput2.add(scrollPane8);
        scrollPane8.setBounds(100, 1180, 580, 40);

        cmb_obat.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "YA", "TIDAK" }));
        cmb_obat.setName("cmb_obat"); // NOI18N
        cmb_obat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_obatActionPerformed(evt);
            }
        });
        cmb_obat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmb_obatKeyPressed(evt);
            }
        });
        FormInput2.add(cmb_obat);
        cmb_obat.setBounds(260, 1250, 100, 20);

        label281.setText("( Obat - obatan ) :");
        label281.setName("label281"); // NOI18N
        label281.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput2.add(label281);
        label281.setBounds(120, 1250, 130, 23);

        cmb_cairan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "YA", "TIDAK" }));
        cmb_cairan.setName("cmb_cairan"); // NOI18N
        cmb_cairan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_cairanActionPerformed(evt);
            }
        });
        cmb_cairan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmb_cairanKeyPressed(evt);
            }
        });
        FormInput2.add(cmb_cairan);
        cmb_cairan.setBounds(260, 1280, 100, 20);

        scrollPane9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane9.setName("scrollPane9"); // NOI18N

        cairan_teks.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cairan_teks.setColumns(20);
        cairan_teks.setRows(5);
        cairan_teks.setName("cairan_teks"); // NOI18N
        cairan_teks.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cairan_teksKeyPressed(evt);
            }
        });
        scrollPane9.setViewportView(cairan_teks);

        FormInput2.add(scrollPane9);
        scrollPane9.setBounds(370, 1280, 310, 30);

        riwayat_alergi_text.setHighlighter(null);
        riwayat_alergi_text.setName("riwayat_alergi_text"); // NOI18N
        riwayat_alergi_text.setPreferredSize(new java.awt.Dimension(50, 23));
        riwayat_alergi_text.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                riwayat_alergi_textMouseMoved(evt);
            }
        });
        riwayat_alergi_text.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                riwayat_alergi_textMouseExited(evt);
            }
        });
        riwayat_alergi_text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                riwayat_alergi_textKeyPressed(evt);
            }
        });
        FormInput2.add(riwayat_alergi_text);
        riwayat_alergi_text.setBounds(220, 320, 220, 23);

        riwayat_penyakit_text.setHighlighter(null);
        riwayat_penyakit_text.setName("riwayat_penyakit_text"); // NOI18N
        riwayat_penyakit_text.setPreferredSize(new java.awt.Dimension(50, 23));
        riwayat_penyakit_text.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                riwayat_penyakit_textMouseMoved(evt);
            }
        });
        riwayat_penyakit_text.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                riwayat_penyakit_textMouseExited(evt);
            }
        });
        riwayat_penyakit_text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                riwayat_penyakit_textKeyPressed(evt);
            }
        });
        FormInput2.add(riwayat_penyakit_text);
        riwayat_penyakit_text.setBounds(220, 350, 220, 23);

        BtnPetugas3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas3.setMnemonic('2');
        BtnPetugas3.setToolTipText("Alt+2");
        BtnPetugas3.setName("BtnPetugas3"); // NOI18N
        BtnPetugas3.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugas3ActionPerformed(evt);
            }
        });
        BtnPetugas3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugas3KeyPressed(evt);
            }
        });
        FormInput2.add(BtnPetugas3);
        BtnPetugas3.setBounds(460, 110, 28, 23);

        BtnRad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnRad.setMnemonic('2');
        BtnRad.setToolTipText("Alt+2");
        BtnRad.setName("BtnRad"); // NOI18N
        BtnRad.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnRad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRadActionPerformed(evt);
            }
        });
        FormInput2.add(BtnRad);
        BtnRad.setBounds(90, 900, 28, 23);

        BtnLab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnLab.setMnemonic('2');
        BtnLab.setToolTipText("Alt+2");
        BtnLab.setName("BtnLab"); // NOI18N
        BtnLab.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLabActionPerformed(evt);
            }
        });
        FormInput2.add(BtnLab);
        BtnLab.setBounds(90, 840, 28, 23);

        scrollInput2.setViewportView(FormInput2);

        internalFrame4.add(scrollInput2, java.awt.BorderLayout.CENTER);

        PanelAccor1.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor1.setName("PanelAccor1"); // NOI18N
        PanelAccor1.setPreferredSize(new java.awt.Dimension(445, 43));
        PanelAccor1.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor1.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor1.setSelected(true);
        ChkAccor1.setFocusable(false);
        ChkAccor1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor1.setName("ChkAccor1"); // NOI18N
        ChkAccor1.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor1.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor1.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccor1ActionPerformed(evt);
            }
        });
        PanelAccor1.add(ChkAccor1, java.awt.BorderLayout.WEST);

        TabData1.setBackground(new java.awt.Color(254, 255, 254));
        TabData1.setForeground(new java.awt.Color(50, 50, 50));
        TabData1.setName("TabData1"); // NOI18N
        TabData1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabData1MouseClicked(evt);
            }
        });

        FormTelaah1.setBackground(new java.awt.Color(255, 255, 255));
        FormTelaah1.setBorder(null);
        FormTelaah1.setName("FormTelaah1"); // NOI18N
        FormTelaah1.setPreferredSize(new java.awt.Dimension(115, 73));
        FormTelaah1.setLayout(new java.awt.BorderLayout());

        FormPass4.setBackground(new java.awt.Color(255, 255, 255));
        FormPass4.setBorder(null);
        FormPass4.setName("FormPass4"); // NOI18N
        FormPass4.setPreferredSize(new java.awt.Dimension(115, 40));

        BtnRefreshPhoto2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto2.setMnemonic('U');
        BtnRefreshPhoto2.setText("Refresh");
        BtnRefreshPhoto2.setToolTipText("Alt+U");
        BtnRefreshPhoto2.setName("BtnRefreshPhoto2"); // NOI18N
        BtnRefreshPhoto2.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto2ActionPerformed(evt);
            }
        });
        FormPass4.add(BtnRefreshPhoto2);

        FormTelaah1.add(FormPass4, java.awt.BorderLayout.PAGE_END);

        Scroll8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll8.setName("Scroll8"); // NOI18N
        Scroll8.setOpaque(true);
        Scroll8.setPreferredSize(new java.awt.Dimension(200, 200));

        tbPrasedasi.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbPrasedasi.setComponentPopupMenu(Popup2);
        tbPrasedasi.setName("tbPrasedasi"); // NOI18N
        tbPrasedasi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPrasedasiMouseClicked(evt);
            }
        });
        tbPrasedasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPrasedasiKeyPressed(evt);
            }
        });
        Scroll8.setViewportView(tbPrasedasi);

        FormTelaah1.add(Scroll8, java.awt.BorderLayout.CENTER);

        TabData1.addTab("Data Asesmen Pra Sedasi", FormTelaah1);

        PanelAccor1.add(TabData1, java.awt.BorderLayout.CENTER);

        internalFrame4.add(PanelAccor1, java.awt.BorderLayout.EAST);

        TabOK.addTab("ASESMEN PRA SEDASI", internalFrame4);

        internalFrame5.setBackground(new java.awt.Color(255, 255, 255));
        internalFrame5.setBorder(null);
        internalFrame5.setName("internalFrame5"); // NOI18N
        internalFrame5.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi7.setName("panelisi7"); // NOI18N
        panelisi7.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        label13.setText("Key Word :");
        label13.setName("label13"); // NOI18N
        label13.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi7.add(label13);

        TCari2.setEditable(false);
        TCari2.setName("TCari2"); // NOI18N
        TCari2.setPreferredSize(new java.awt.Dimension(350, 23));
        TCari2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCari2KeyPressed(evt);
            }
        });
        panelisi7.add(TCari2);

        BtnCari2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari2.setMnemonic('1');
        BtnCari2.setToolTipText("Alt+1");
        BtnCari2.setName("BtnCari2"); // NOI18N
        BtnCari2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCari2ActionPerformed(evt);
            }
        });
        BtnCari2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCari2KeyPressed(evt);
            }
        });
        panelisi7.add(BtnCari2);

        BtnAll2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll2.setMnemonic('2');
        BtnAll2.setToolTipText("Alt+2");
        BtnAll2.setName("BtnAll2"); // NOI18N
        BtnAll2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAll2ActionPerformed(evt);
            }
        });
        BtnAll2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAll2KeyPressed(evt);
            }
        });
        panelisi7.add(BtnAll2);

        label16.setText("Record :");
        label16.setName("label16"); // NOI18N
        label16.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi7.add(label16);

        LCount2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount2.setText("0");
        LCount2.setName("LCount2"); // NOI18N
        LCount2.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi7.add(LCount2);

        internalFrame5.add(panelisi7, java.awt.BorderLayout.PAGE_END);

        scrollInput3.setName("scrollInput3"); // NOI18N
        scrollInput3.setPreferredSize(new java.awt.Dimension(102, 200));

        FormInput3.setBackground(new java.awt.Color(255, 255, 255));
        FormInput3.setBorder(null);
        FormInput3.setName("FormInput3"); // NOI18N
        FormInput3.setPreferredSize(new java.awt.Dimension(870, 1000));
        FormInput3.setLayout(null);

        TNoRw3.setHighlighter(null);
        TNoRw3.setName("TNoRw3"); // NOI18N
        TNoRw3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRw3KeyPressed(evt);
            }
        });
        FormInput3.add(TNoRw3);
        TNoRw3.setBounds(74, 10, 131, 23);

        TPasien3.setEditable(false);
        TPasien3.setHighlighter(null);
        TPasien3.setName("TPasien3"); // NOI18N
        FormInput3.add(TPasien3);
        TPasien3.setBounds(309, 10, 260, 23);

        TNoRM3.setEditable(false);
        TNoRM3.setHighlighter(null);
        TNoRM3.setName("TNoRM3"); // NOI18N
        FormInput3.add(TNoRM3);
        TNoRM3.setBounds(207, 10, 100, 23);

        label17.setText("Dokter Anestesi :");
        label17.setName("label17"); // NOI18N
        label17.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput3.add(label17);
        label17.setBounds(10, 40, 90, 23);

        KdDokter3.setEditable(false);
        KdDokter3.setName("KdDokter3"); // NOI18N
        KdDokter3.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDokter3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokter3KeyPressed(evt);
            }
        });
        FormInput3.add(KdDokter3);
        KdDokter3.setBounds(100, 40, 100, 23);

        NmDokter3.setEditable(false);
        NmDokter3.setName("NmDokter3"); // NOI18N
        NmDokter3.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput3.add(NmDokter3);
        NmDokter3.setBounds(200, 40, 180, 23);

        BtnPetugas2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas2.setMnemonic('2');
        BtnPetugas2.setToolTipText("Alt+2");
        BtnPetugas2.setName("BtnPetugas2"); // NOI18N
        BtnPetugas2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugas2ActionPerformed(evt);
            }
        });
        BtnPetugas2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugas2KeyPressed(evt);
            }
        });
        FormInput3.add(BtnPetugas2);
        BtnPetugas2.setBounds(380, 40, 28, 23);

        jLabel13.setText("No.Rawat :");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput3.add(jLabel13);
        jLabel13.setBounds(0, 10, 70, 23);

        jSeparator11.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator11.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator11.setName("jSeparator11"); // NOI18N
        FormInput3.add(jSeparator11);
        jSeparator11.setBounds(0, 70, 880, 1);

        jSeparator12.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator12.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator12.setName("jSeparator12"); // NOI18N
        FormInput3.add(jSeparator12);
        jSeparator12.setBounds(0, 211, 880, 3);

        label180.setText("BB :");
        label180.setName("label180"); // NOI18N
        label180.setPreferredSize(new java.awt.Dimension(82, 23));
        FormInput3.add(label180);
        label180.setBounds(260, 80, 30, 23);

        tab3_bb.setHighlighter(null);
        tab3_bb.setName("tab3_bb"); // NOI18N
        tab3_bb.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_bb.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_bbMouseMoved(evt);
            }
        });
        tab3_bb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_bbMouseExited(evt);
            }
        });
        tab3_bb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_bbKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_bb);
        tab3_bb.setBounds(300, 80, 50, 23);

        label181.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label181.setText(" ");
        label181.setName("label181"); // NOI18N
        label181.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label181);
        label181.setBounds(0, 240, 50, 23);

        tab3_td.setName("tab3_td"); // NOI18N
        tab3_td.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_td.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_tdMouseMoved(evt);
            }
        });
        tab3_td.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_tdMouseExited(evt);
            }
        });
        tab3_td.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tab3_tdActionPerformed(evt);
            }
        });
        tab3_td.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_tdKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_td);
        tab3_td.setBounds(300, 110, 50, 23);

        label182.setText("Nadi:");
        label182.setName("label182"); // NOI18N
        label182.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label182);
        label182.setBounds(260, 140, 30, 23);

        tab3_nadi.setName("tab3_nadi"); // NOI18N
        tab3_nadi.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_nadi.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_nadiMouseMoved(evt);
            }
        });
        tab3_nadi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_nadiMouseExited(evt);
            }
        });
        tab3_nadi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_nadiKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_nadi);
        tab3_nadi.setBounds(300, 140, 50, 23);

        label183.setText("TB :");
        label183.setName("label183"); // NOI18N
        label183.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label183);
        label183.setBounds(260, 170, 30, 23);

        tab3_tb.setName("tab3_tb"); // NOI18N
        tab3_tb.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_tb.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_tbMouseMoved(evt);
            }
        });
        tab3_tb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_tbMouseExited(evt);
            }
        });
        tab3_tb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_tbKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_tb);
        tab3_tb.setBounds(300, 170, 50, 23);

        label184.setText("Respirasi :");
        label184.setName("label184"); // NOI18N
        label184.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label184);
        label184.setBounds(390, 80, 50, 23);

        jLabel19.setText("Tanggal :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setVerifyInputWhenFocusTarget(false);
        FormInput3.add(jLabel19);
        jLabel19.setBounds(380, 40, 75, 23);

        Tanggal2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "03-03-2025" }));
        Tanggal2.setDisplayFormat("dd-MM-yyyy");
        Tanggal2.setName("Tanggal2"); // NOI18N
        Tanggal2.setOpaque(false);
        Tanggal2.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput3.add(Tanggal2);
        Tanggal2.setBounds(460, 40, 110, 23);

        Jam2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam2.setName("Jam2"); // NOI18N
        Jam2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam2ActionPerformed(evt);
            }
        });
        Jam2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam2KeyPressed(evt);
            }
        });
        FormInput3.add(Jam2);
        Jam2.setBounds(580, 40, 50, 23);

        Menit2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit2.setName("Menit2"); // NOI18N
        Menit2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit2KeyPressed(evt);
            }
        });
        FormInput3.add(Menit2);
        Menit2.setBounds(640, 40, 50, 23);

        Detik2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik2.setName("Detik2"); // NOI18N
        Detik2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik2KeyPressed(evt);
            }
        });
        FormInput3.add(Detik2);
        Detik2.setBounds(700, 40, 50, 23);

        ChkJam3.setBorder(null);
        ChkJam3.setSelected(true);
        ChkJam3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam3.setName("ChkJam3"); // NOI18N
        FormInput3.add(ChkJam3);
        ChkJam3.setBounds(750, 40, 23, 23);

        jk2.setEditable(false);
        jk2.setName("jk2"); // NOI18N
        jk2.setPreferredSize(new java.awt.Dimension(50, 23));
        jk2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jk2MouseMoved(evt);
            }
        });
        jk2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jk2MouseExited(evt);
            }
        });
        jk2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jk2ActionPerformed(evt);
            }
        });
        jk2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jk2KeyPressed(evt);
            }
        });
        FormInput3.add(jk2);
        jk2.setBounds(580, 10, 50, 23);

        tab3_respirasi.setName("tab3_respirasi"); // NOI18N
        tab3_respirasi.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_respirasi.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_respirasiMouseMoved(evt);
            }
        });
        tab3_respirasi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_respirasiMouseExited(evt);
            }
        });
        tab3_respirasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_respirasiKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_respirasi);
        tab3_respirasi.setBounds(450, 80, 50, 23);

        tab3_normal.setBorder(null);
        tab3_normal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_normal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_normal.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_normal.setName("tab3_normal"); // NOI18N
        FormInput3.add(tab3_normal);
        tab3_normal.setBounds(50, 240, 23, 23);

        tab3_alergi1.setBorder(null);
        buttonGroup1.add(tab3_alergi1);
        tab3_alergi1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_alergi1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_alergi1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_alergi1.setName("tab3_alergi1"); // NOI18N
        FormInput3.add(tab3_alergi1);
        tab3_alergi1.setBounds(50, 110, 23, 23);

        tab3_alergi2.setBorder(null);
        buttonGroup1.add(tab3_alergi2);
        tab3_alergi2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_alergi2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_alergi2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_alergi2.setName("tab3_alergi2"); // NOI18N
        tab3_alergi2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tab3_alergi2ActionPerformed(evt);
            }
        });
        FormInput3.add(tab3_alergi2);
        tab3_alergi2.setBounds(40, 140, 40, 23);

        tab3_bukamulut.setBorder(null);
        tab3_bukamulut.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_bukamulut.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_bukamulut.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_bukamulut.setName("tab3_bukamulut"); // NOI18N
        FormInput3.add(tab3_bukamulut);
        tab3_bukamulut.setBounds(150, 240, 23, 23);

        tab3_jarak.setBorder(null);
        tab3_jarak.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_jarak.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_jarak.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_jarak.setName("tab3_jarak"); // NOI18N
        FormInput3.add(tab3_jarak);
        tab3_jarak.setBounds(150, 270, 23, 23);

        tab3_mallampati.setBorder(null);
        tab3_mallampati.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_mallampati.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_mallampati.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_mallampati.setName("tab3_mallampati"); // NOI18N
        FormInput3.add(tab3_mallampati);
        tab3_mallampati.setBounds(150, 300, 23, 23);

        tab3_leher.setBorder(null);
        tab3_leher.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_leher.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_leher.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_leher.setName("tab3_leher"); // NOI18N
        FormInput3.add(tab3_leher);
        tab3_leher.setBounds(150, 330, 23, 23);

        tab3_abnormal.setBorder(null);
        tab3_abnormal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_abnormal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_abnormal.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_abnormal.setName("tab3_abnormal"); // NOI18N
        FormInput3.add(tab3_abnormal);
        tab3_abnormal.setBounds(360, 240, 23, 23);

        label219.setText("TD :");
        label219.setName("label219"); // NOI18N
        label219.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label219);
        label219.setBounds(250, 110, 40, 23);

        label220.setText("STATUS FISIK ASA :");
        label220.setName("label220"); // NOI18N
        label220.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label220);
        label220.setBounds(20, 410, 110, 23);

        label221.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label221.setText("Abnormal");
        label221.setName("label221"); // NOI18N
        label221.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label221);
        label221.setBounds(390, 240, 140, 23);

        label222.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label222.setText("Alergi :");
        label222.setName("label222"); // NOI18N
        label222.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label222);
        label222.setBounds(10, 110, 40, 23);

        label223.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label223.setText("Ya");
        label223.setName("label223"); // NOI18N
        label223.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label223);
        label223.setBounds(80, 140, 40, 20);

        label224.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label224.setText("Jarak Thyromental > 3 Jam");
        label224.setName("label224"); // NOI18N
        label224.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label224);
        label224.setBounds(180, 270, 140, 23);

        label225.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label225.setText("Mallampati I / II / III / IV");
        label225.setName("label225"); // NOI18N
        label225.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label225);
        label225.setBounds(180, 300, 140, 23);

        label226.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label226.setText("Gerakan Leher Maksimal");
        label226.setName("label226"); // NOI18N
        label226.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label226);
        label226.setBounds(180, 330, 140, 23);

        jSeparator16.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator16.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator16.setName("jSeparator16"); // NOI18N
        FormInput3.add(jSeparator16);
        jSeparator16.setBounds(0, 364, 880, 3);

        label227.setText("ANAMNESIS :");
        label227.setName("label227"); // NOI18N
        label227.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label227);
        label227.setBounds(20, 370, 110, 23);

        tab3_auto.setBorder(null);
        tab3_auto.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_auto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_auto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_auto.setName("tab3_auto"); // NOI18N
        FormInput3.add(tab3_auto);
        tab3_auto.setBounds(150, 370, 23, 23);

        label_100.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_100.setText("Auto");
        label_100.setName("label_100"); // NOI18N
        label_100.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label_100);
        label_100.setBounds(180, 370, 30, 23);

        tab3_allo.setBorder(null);
        tab3_allo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_allo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_allo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_allo.setName("tab3_allo"); // NOI18N
        FormInput3.add(tab3_allo);
        tab3_allo.setBounds(210, 370, 23, 23);

        label229.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label229.setText("Allo");
        label229.setName("label229"); // NOI18N
        label229.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label229);
        label229.setBounds(240, 370, 50, 23);

        jSeparator17.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator17.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator17.setName("jSeparator17"); // NOI18N
        FormInput3.add(jSeparator17);
        jSeparator17.setBounds(0, 397, 880, 3);

        label230.setText("KEADAAN PRAINDUKSI :");
        label230.setName("label230"); // NOI18N
        label230.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label230);
        label230.setBounds(10, 80, 120, 23);

        label231.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label231.setText("1");
        label231.setName("label231"); // NOI18N
        label231.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label231);
        label231.setBounds(180, 410, 10, 23);

        tab3_asa1.setBorder(null);
        tab3_asa1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_asa1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_asa1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_asa1.setName("tab3_asa1"); // NOI18N
        FormInput3.add(tab3_asa1);
        tab3_asa1.setBounds(150, 410, 23, 23);

        label232.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label232.setText("2");
        label232.setName("label232"); // NOI18N
        label232.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label232);
        label232.setBounds(220, 410, 10, 23);

        tab3_asa2.setBorder(null);
        tab3_asa2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_asa2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_asa2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_asa2.setName("tab3_asa2"); // NOI18N
        FormInput3.add(tab3_asa2);
        tab3_asa2.setBounds(190, 410, 23, 23);

        label233.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label233.setText("3");
        label233.setName("label233"); // NOI18N
        label233.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label233);
        label233.setBounds(260, 410, 10, 23);

        tab3_asa3.setBorder(null);
        tab3_asa3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_asa3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_asa3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_asa3.setName("tab3_asa3"); // NOI18N
        FormInput3.add(tab3_asa3);
        tab3_asa3.setBounds(230, 410, 23, 23);

        label234.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label234.setText("4");
        label234.setName("label234"); // NOI18N
        label234.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label234);
        label234.setBounds(300, 410, 10, 23);

        tab3_asa4.setBorder(null);
        tab3_asa4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_asa4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_asa4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_asa4.setName("tab3_asa4"); // NOI18N
        tab3_asa4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tab3_asa4ActionPerformed(evt);
            }
        });
        FormInput3.add(tab3_asa4);
        tab3_asa4.setBounds(270, 410, 23, 23);

        label235.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label235.setText("5");
        label235.setName("label235"); // NOI18N
        label235.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label235);
        label235.setBounds(340, 410, 10, 23);

        tab3_asa5.setBorder(null);
        tab3_asa5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_asa5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_asa5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_asa5.setName("tab3_asa5"); // NOI18N
        FormInput3.add(tab3_asa5);
        tab3_asa5.setBounds(310, 410, 23, 23);

        label236.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label236.setText("Penyulit Praanestesi :");
        label236.setName("label236"); // NOI18N
        label236.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label236);
        label236.setBounds(430, 410, 110, 23);

        tab3_asaE.setBorder(null);
        tab3_asaE.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_asaE.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_asaE.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_asaE.setName("tab3_asaE"); // NOI18N
        FormInput3.add(tab3_asaE);
        tab3_asaE.setBounds(360, 410, 23, 23);

        label237.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label237.setText("E");
        label237.setName("label237"); // NOI18N
        label237.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label237);
        label237.setBounds(390, 410, 10, 23);

        penyulit_praanestesi.setName("penyulit_praanestesi"); // NOI18N
        penyulit_praanestesi.setPreferredSize(new java.awt.Dimension(50, 23));
        penyulit_praanestesi.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                penyulit_praanestesiMouseMoved(evt);
            }
        });
        penyulit_praanestesi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                penyulit_praanestesiMouseExited(evt);
            }
        });
        penyulit_praanestesi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                penyulit_praanestesiKeyPressed(evt);
            }
        });
        FormInput3.add(penyulit_praanestesi);
        penyulit_praanestesi.setBounds(540, 410, 300, 23);

        jSeparator18.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator18.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator18.setName("jSeparator18"); // NOI18N
        FormInput3.add(jSeparator18);
        jSeparator18.setBounds(0, 447, 880, 3);

        label238.setText("Checklist Sebelum Induksi :");
        label238.setName("label238"); // NOI18N
        label238.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label238);
        label238.setBounds(20, 460, 130, 23);

        tab3_cek1.setBorder(null);
        tab3_cek1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_cek1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_cek1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_cek1.setName("tab3_cek1"); // NOI18N
        FormInput3.add(tab3_cek1);
        tab3_cek1.setBounds(150, 460, 23, 23);

        label239.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label239.setText("Ijin Operasi");
        label239.setName("label239"); // NOI18N
        label239.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label239);
        label239.setBounds(180, 460, 60, 23);

        label240.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label240.setText("Cek Mohon Anestesi");
        label240.setName("label240"); // NOI18N
        label240.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label240);
        label240.setBounds(270, 460, 100, 23);

        tab3_cek2.setBorder(null);
        tab3_cek2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_cek2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_cek2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_cek2.setName("tab3_cek2"); // NOI18N
        FormInput3.add(tab3_cek2);
        tab3_cek2.setBounds(240, 460, 23, 23);

        tab3_cek3.setBorder(null);
        tab3_cek3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_cek3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_cek3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_cek3.setName("tab3_cek3"); // NOI18N
        FormInput3.add(tab3_cek3);
        tab3_cek3.setBounds(380, 460, 23, 23);

        label241.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label241.setText("Cek Suction Unit");
        label241.setName("label241"); // NOI18N
        label241.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label241);
        label241.setBounds(410, 460, 80, 23);

        tab3_cek4.setBorder(null);
        tab3_cek4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_cek4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_cek4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_cek4.setName("tab3_cek4"); // NOI18N
        FormInput3.add(tab3_cek4);
        tab3_cek4.setBounds(500, 460, 23, 23);

        label242.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label242.setText("Persiapan Obat - Obatan");
        label242.setName("label242"); // NOI18N
        label242.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label242);
        label242.setBounds(530, 460, 130, 23);

        tab3_cek5.setBorder(null);
        tab3_cek5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_cek5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_cek5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_cek5.setName("tab3_cek5"); // NOI18N
        FormInput3.add(tab3_cek5);
        tab3_cek5.setBounds(670, 460, 23, 23);

        label243.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label243.setText("Antibiotika Profilaksis");
        label243.setName("label243"); // NOI18N
        label243.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label243);
        label243.setBounds(700, 460, 130, 23);

        jSeparator19.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator19.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator19.setName("jSeparator19"); // NOI18N
        FormInput3.add(jSeparator19);
        jSeparator19.setBounds(0, 494, 880, 3);

        label244.setText("Monitoring :");
        label244.setName("label244"); // NOI18N
        label244.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label244);
        label244.setBounds(50, 640, 100, 23);

        tab3_tek1.setBorder(null);
        tab3_tek1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek1.setName("tab3_tek1"); // NOI18N
        FormInput3.add(tab3_tek1);
        tab3_tek1.setBounds(150, 510, 23, 23);

        label245.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label245.setText("GA");
        label245.setName("label245"); // NOI18N
        label245.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label245);
        label245.setBounds(180, 510, 60, 23);

        tab3_tek6.setBorder(null);
        tab3_tek6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek6.setName("tab3_tek6"); // NOI18N
        FormInput3.add(tab3_tek6);
        tab3_tek6.setBounds(150, 530, 23, 23);

        label246.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label246.setText("REGIONAL");
        label246.setName("label246"); // NOI18N
        label246.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label246);
        label246.setBounds(180, 530, 60, 23);

        label247.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label247.setText("LAIN-LAIN");
        label247.setName("label247"); // NOI18N
        label247.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label247);
        label247.setBounds(180, 560, 60, 23);

        tab3_tek11.setBorder(null);
        tab3_tek11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek11.setName("tab3_tek11"); // NOI18N
        FormInput3.add(tab3_tek11);
        tab3_tek11.setBounds(150, 560, 23, 23);

        tab3_tek2.setBorder(null);
        tab3_tek2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek2.setName("tab3_tek2"); // NOI18N
        FormInput3.add(tab3_tek2);
        tab3_tek2.setBounds(260, 510, 23, 23);

        label248.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label248.setText("TIVA");
        label248.setName("label248"); // NOI18N
        label248.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label248);
        label248.setBounds(290, 510, 40, 23);

        label249.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label249.setText("LMA");
        label249.setName("label249"); // NOI18N
        label249.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label249);
        label249.setBounds(360, 510, 30, 23);

        tab3_tek3.setBorder(null);
        tab3_tek3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek3.setName("tab3_tek3"); // NOI18N
        FormInput3.add(tab3_tek3);
        tab3_tek3.setBounds(330, 510, 23, 23);

        label250.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label250.setText("FACEMASK");
        label250.setName("label250"); // NOI18N
        label250.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label250);
        label250.setBounds(430, 510, 60, 23);

        tab3_tek4.setBorder(null);
        tab3_tek4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek4.setName("tab3_tek4"); // NOI18N
        FormInput3.add(tab3_tek4);
        tab3_tek4.setBounds(400, 510, 23, 23);

        label251.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label251.setText("ET");
        label251.setName("label251"); // NOI18N
        label251.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label251);
        label251.setBounds(520, 510, 20, 23);

        tab3_tek5.setBorder(null);
        tab3_tek5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek5.setName("tab3_tek5"); // NOI18N
        FormInput3.add(tab3_tek5);
        tab3_tek5.setBounds(490, 510, 23, 23);

        tab3_tek7.setBorder(null);
        tab3_tek7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek7.setName("tab3_tek7"); // NOI18N
        FormInput3.add(tab3_tek7);
        tab3_tek7.setBounds(260, 530, 23, 23);

        label252.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label252.setText("Spinal");
        label252.setName("label252"); // NOI18N
        label252.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label252);
        label252.setBounds(290, 530, 40, 23);

        tab3_tek8.setBorder(null);
        tab3_tek8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek8.setName("tab3_tek8"); // NOI18N
        FormInput3.add(tab3_tek8);
        tab3_tek8.setBounds(330, 530, 23, 23);

        label253.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label253.setText("Epidural");
        label253.setName("label253"); // NOI18N
        label253.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label253);
        label253.setBounds(360, 530, 40, 23);

        tab3_tek9.setBorder(null);
        tab3_tek9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek9.setName("tab3_tek9"); // NOI18N
        FormInput3.add(tab3_tek9);
        tab3_tek9.setBounds(400, 530, 23, 23);

        label254.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label254.setText("Kaudal");
        label254.setName("label254"); // NOI18N
        label254.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label254);
        label254.setBounds(430, 530, 60, 23);

        tab3_tek10.setBorder(null);
        tab3_tek10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_tek10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_tek10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_tek10.setName("tab3_tek10"); // NOI18N
        FormInput3.add(tab3_tek10);
        tab3_tek10.setBounds(490, 530, 23, 23);

        label255.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label255.setText("Blok Saraf Tepi");
        label255.setName("label255"); // NOI18N
        label255.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label255);
        label255.setBounds(520, 530, 80, 23);

        jSeparator20.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator20.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator20.setName("jSeparator20"); // NOI18N
        FormInput3.add(jSeparator20);
        jSeparator20.setBounds(0, 620, 880, 3);

        tab3_moni1.setBorder(null);
        tab3_moni1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_moni1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_moni1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_moni1.setName("tab3_moni1"); // NOI18N
        FormInput3.add(tab3_moni1);
        tab3_moni1.setBounds(150, 640, 23, 23);

        label256.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label256.setText("EKG Lead");
        label256.setName("label256"); // NOI18N
        label256.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label256);
        label256.setBounds(180, 640, 60, 23);

        label257.setText("Teknik Anestesi :");
        label257.setName("label257"); // NOI18N
        label257.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label257);
        label257.setBounds(50, 510, 100, 23);

        label258.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label258.setText("CVC");
        label258.setName("label258"); // NOI18N
        label258.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label258);
        label258.setBounds(180, 660, 60, 23);

        tab3_moni5.setBorder(null);
        tab3_moni5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_moni5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_moni5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_moni5.setName("tab3_moni5"); // NOI18N
        FormInput3.add(tab3_moni5);
        tab3_moni5.setBounds(150, 660, 23, 23);

        tab3_moni8.setBorder(null);
        tab3_moni8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_moni8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_moni8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_moni8.setName("tab3_moni8"); // NOI18N
        FormInput3.add(tab3_moni8);
        tab3_moni8.setBounds(150, 680, 23, 23);

        label259.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label259.setText("Stetoscop");
        label259.setName("label259"); // NOI18N
        label259.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label259);
        label259.setBounds(180, 680, 60, 23);

        label260.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label260.setText("SPO2");
        label260.setName("label260"); // NOI18N
        label260.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label260);
        label260.setBounds(300, 640, 60, 23);

        tab3_moni2.setBorder(null);
        tab3_moni2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_moni2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_moni2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_moni2.setName("tab3_moni2"); // NOI18N
        FormInput3.add(tab3_moni2);
        tab3_moni2.setBounds(270, 640, 23, 23);

        tab3_moni6.setBorder(null);
        tab3_moni6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_moni6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_moni6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_moni6.setName("tab3_moni6"); // NOI18N
        FormInput3.add(tab3_moni6);
        tab3_moni6.setBounds(270, 660, 23, 23);

        label261.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label261.setText("PCO2");
        label261.setName("label261"); // NOI18N
        label261.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label261);
        label261.setBounds(300, 660, 60, 23);

        tab3_moni3.setBorder(null);
        tab3_moni3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_moni3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_moni3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_moni3.setName("tab3_moni3"); // NOI18N
        FormInput3.add(tab3_moni3);
        tab3_moni3.setBounds(360, 640, 23, 23);

        label262.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label262.setText("mmHG");
        label262.setName("label262"); // NOI18N
        label262.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label262);
        label262.setBounds(470, 640, 40, 23);

        label263.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label263.setText("Urin Catheter");
        label263.setName("label263"); // NOI18N
        label263.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label263);
        label263.setBounds(390, 660, 70, 23);

        tab3_moni7.setBorder(null);
        tab3_moni7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_moni7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_moni7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_moni7.setName("tab3_moni7"); // NOI18N
        FormInput3.add(tab3_moni7);
        tab3_moni7.setBounds(360, 660, 23, 23);

        tab3_moni9.setBorder(null);
        tab3_moni9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_moni9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_moni9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_moni9.setName("tab3_moni9"); // NOI18N
        FormInput3.add(tab3_moni9);
        tab3_moni9.setBounds(360, 680, 23, 23);

        label264.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label264.setText("NGT");
        label264.setName("label264"); // NOI18N
        label264.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label264);
        label264.setBounds(390, 680, 30, 23);

        label265.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label265.setText("TD");
        label265.setName("label265"); // NOI18N
        label265.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label265);
        label265.setBounds(390, 640, 30, 23);

        tab3_moni4.setBorder(null);
        tab3_moni4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tab3_moni4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tab3_moni4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tab3_moni4.setName("tab3_moni4"); // NOI18N
        FormInput3.add(tab3_moni4);
        tab3_moni4.setBounds(520, 640, 23, 23);

        label266.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label266.setText("Temp");
        label266.setName("label266"); // NOI18N
        label266.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label266);
        label266.setBounds(550, 640, 40, 23);

        label267.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label267.setText("C");
        label267.setName("label267"); // NOI18N
        label267.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label267);
        label267.setBounds(640, 640, 10, 23);

        label268.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label268.setText("Normal");
        label268.setName("label268"); // NOI18N
        label268.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label268);
        label268.setBounds(80, 240, 50, 23);

        label269.setText("PEMERIKSAAN FISIK :");
        label269.setName("label269"); // NOI18N
        label269.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label269);
        label269.setBounds(20, 220, 110, 23);

        label270.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label270.setText("Buka Mulut > 2 Jari");
        label270.setName("label270"); // NOI18N
        label270.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label270);
        label270.setBounds(180, 240, 100, 23);

        label271.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label271.setText("Tidak");
        label271.setName("label271"); // NOI18N
        label271.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label271);
        label271.setBounds(80, 120, 40, 10);

        label272.setText("Gol. Darah :");
        label272.setName("label272"); // NOI18N
        label272.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label272);
        label272.setBounds(380, 110, 60, 23);

        tab3_darah.setName("tab3_darah"); // NOI18N
        tab3_darah.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_darah.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_darahMouseMoved(evt);
            }
        });
        tab3_darah.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_darahMouseExited(evt);
            }
        });
        tab3_darah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_darahKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_darah);
        tab3_darah.setBounds(450, 110, 50, 23);

        label273.setText("Suhu :");
        label273.setName("label273"); // NOI18N
        label273.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label273);
        label273.setBounds(380, 140, 60, 23);

        tab3_suhu.setName("tab3_suhu"); // NOI18N
        tab3_suhu.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_suhu.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_suhuMouseMoved(evt);
            }
        });
        tab3_suhu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_suhuMouseExited(evt);
            }
        });
        tab3_suhu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_suhuKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_suhu);
        tab3_suhu.setBounds(450, 140, 50, 23);

        label274.setText("GCS :");
        label274.setName("label274"); // NOI18N
        label274.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label274);
        label274.setBounds(380, 170, 60, 23);

        tab3_gcs.setName("tab3_gcs"); // NOI18N
        tab3_gcs.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_gcs.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_gcsMouseMoved(evt);
            }
        });
        tab3_gcs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_gcsMouseExited(evt);
            }
        });
        tab3_gcs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_gcsKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_gcs);
        tab3_gcs.setBounds(450, 170, 50, 23);

        label275.setText("Rh :");
        label275.setName("label275"); // NOI18N
        label275.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label275);
        label275.setBounds(510, 80, 30, 23);

        tab3_rh.setName("tab3_rh"); // NOI18N
        tab3_rh.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_rh.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_rhMouseMoved(evt);
            }
        });
        tab3_rh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_rhMouseExited(evt);
            }
        });
        tab3_rh.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_rhKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_rh);
        tab3_rh.setBounds(550, 80, 100, 23);

        label276.setText("Hb :");
        label276.setName("label276"); // NOI18N
        label276.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label276);
        label276.setBounds(510, 110, 30, 23);

        tab3_hb.setName("tab3_hb"); // NOI18N
        tab3_hb.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_hb.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_hbMouseMoved(evt);
            }
        });
        tab3_hb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_hbMouseExited(evt);
            }
        });
        tab3_hb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_hbKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_hb);
        tab3_hb.setBounds(550, 110, 100, 23);

        label277.setText("VAS :");
        label277.setName("label277"); // NOI18N
        label277.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label277);
        label277.setBounds(510, 140, 30, 20);

        tab3_vas.setName("tab3_vas"); // NOI18N
        tab3_vas.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_vas.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_vasMouseMoved(evt);
            }
        });
        tab3_vas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_vasMouseExited(evt);
            }
        });
        tab3_vas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_vasKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_vas);
        tab3_vas.setBounds(550, 140, 100, 23);

        label278.setText("Ht :");
        label278.setName("label278"); // NOI18N
        label278.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label278);
        label278.setBounds(510, 170, 30, 20);

        tab3_ht.setName("tab3_ht"); // NOI18N
        tab3_ht.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_ht.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_htMouseMoved(evt);
            }
        });
        tab3_ht.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_htMouseExited(evt);
            }
        });
        tab3_ht.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_htKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_ht);
        tab3_ht.setBounds(550, 170, 100, 23);

        tab3_lain.setName("tab3_lain"); // NOI18N
        tab3_lain.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_lain.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_lainMouseMoved(evt);
            }
        });
        tab3_lain.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_lainMouseExited(evt);
            }
        });
        tab3_lain.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_lainKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_lain);
        tab3_lain.setBounds(750, 80, 110, 23);

        tab3_moni4_ket.setName("tab3_moni4_ket"); // NOI18N
        tab3_moni4_ket.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_moni4_ket.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_moni4_ketMouseMoved(evt);
            }
        });
        tab3_moni4_ket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_moni4_ketMouseExited(evt);
            }
        });
        tab3_moni4_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_moni4_ketKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_moni4_ket);
        tab3_moni4_ket.setBounds(580, 640, 50, 23);

        tab3_moni3_ket.setName("tab3_moni3_ket"); // NOI18N
        tab3_moni3_ket.setPreferredSize(new java.awt.Dimension(50, 23));
        tab3_moni3_ket.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tab3_moni3_ketMouseMoved(evt);
            }
        });
        tab3_moni3_ket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tab3_moni3_ketMouseExited(evt);
            }
        });
        tab3_moni3_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tab3_moni3_ketKeyPressed(evt);
            }
        });
        FormInput3.add(tab3_moni3_ket);
        tab3_moni3_ket.setBounds(410, 640, 50, 23);

        alergi_teks.setName("alergi_teks"); // NOI18N
        alergi_teks.setPreferredSize(new java.awt.Dimension(50, 23));
        alergi_teks.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                alergi_teksMouseMoved(evt);
            }
        });
        alergi_teks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                alergi_teksMouseExited(evt);
            }
        });
        alergi_teks.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                alergi_teksKeyPressed(evt);
            }
        });
        FormInput3.add(alergi_teks);
        alergi_teks.setBounds(100, 140, 140, 23);

        BtnCariLab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnCariLab.setMnemonic('2');
        BtnCariLab.setToolTipText("Alt+2");
        BtnCariLab.setName("BtnCariLab"); // NOI18N
        BtnCariLab.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariLabActionPerformed(evt);
            }
        });
        FormInput3.add(BtnCariLab);
        BtnCariLab.setBounds(650, 110, 28, 23);

        label290.setText("Lain-lain :");
        label290.setName("label290"); // NOI18N
        label290.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput3.add(label290);
        label290.setBounds(680, 80, 60, 20);

        BtnCariLab1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnCariLab1.setMnemonic('2');
        BtnCariLab1.setToolTipText("Alt+2");
        BtnCariLab1.setName("BtnCariLab1"); // NOI18N
        BtnCariLab1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariLab1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariLab1ActionPerformed(evt);
            }
        });
        FormInput3.add(BtnCariLab1);
        BtnCariLab1.setBounds(650, 170, 28, 23);

        anestesi_teks.setName("anestesi_teks"); // NOI18N
        anestesi_teks.setPreferredSize(new java.awt.Dimension(50, 23));
        anestesi_teks.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                anestesi_teksMouseMoved(evt);
            }
        });
        anestesi_teks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                anestesi_teksMouseExited(evt);
            }
        });
        anestesi_teks.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                anestesi_teksKeyPressed(evt);
            }
        });
        FormInput3.add(anestesi_teks);
        anestesi_teks.setBounds(260, 560, 220, 23);

        scrollInput3.setViewportView(FormInput3);

        internalFrame5.add(scrollInput3, java.awt.BorderLayout.CENTER);

        PanelAccor2.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor2.setName("PanelAccor2"); // NOI18N
        PanelAccor2.setPreferredSize(new java.awt.Dimension(445, 43));
        PanelAccor2.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor2.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor2.setSelected(true);
        ChkAccor2.setFocusable(false);
        ChkAccor2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor2.setName("ChkAccor2"); // NOI18N
        ChkAccor2.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor2.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor2.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor2.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccor2ActionPerformed(evt);
            }
        });
        PanelAccor2.add(ChkAccor2, java.awt.BorderLayout.WEST);

        TabData2.setBackground(new java.awt.Color(254, 255, 254));
        TabData2.setForeground(new java.awt.Color(50, 50, 50));
        TabData2.setName("TabData2"); // NOI18N
        TabData2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabData2MouseClicked(evt);
            }
        });

        FormTelaah2.setBackground(new java.awt.Color(255, 255, 255));
        FormTelaah2.setBorder(null);
        FormTelaah2.setName("FormTelaah2"); // NOI18N
        FormTelaah2.setPreferredSize(new java.awt.Dimension(115, 73));
        FormTelaah2.setLayout(new java.awt.BorderLayout());

        FormPass5.setBackground(new java.awt.Color(255, 255, 255));
        FormPass5.setBorder(null);
        FormPass5.setName("FormPass5"); // NOI18N
        FormPass5.setPreferredSize(new java.awt.Dimension(115, 40));

        BtnRefreshPhoto3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto3.setMnemonic('U');
        BtnRefreshPhoto3.setText("Refresh");
        BtnRefreshPhoto3.setToolTipText("Alt+U");
        BtnRefreshPhoto3.setName("BtnRefreshPhoto3"); // NOI18N
        BtnRefreshPhoto3.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto3ActionPerformed(evt);
            }
        });
        FormPass5.add(BtnRefreshPhoto3);

        FormTelaah2.add(FormPass5, java.awt.BorderLayout.PAGE_END);

        Scroll9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll9.setName("Scroll9"); // NOI18N
        Scroll9.setOpaque(true);
        Scroll9.setPreferredSize(new java.awt.Dimension(200, 200));
        Scroll9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Scroll9KeyPressed(evt);
            }
        });

        tbPrainduksi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbPrainduksi.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbPrainduksi.setComponentPopupMenu(Popup2);
        tbPrainduksi.setName("tbPrainduksi"); // NOI18N
        tbPrainduksi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPrainduksiMouseClicked(evt);
            }
        });
        tbPrainduksi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPrainduksiKeyPressed(evt);
            }
        });
        Scroll9.setViewportView(tbPrainduksi);

        FormTelaah2.add(Scroll9, java.awt.BorderLayout.CENTER);

        TabData2.addTab("Data Asesmen Pra Induksi", FormTelaah2);

        PanelAccor2.add(TabData2, java.awt.BorderLayout.CENTER);

        internalFrame5.add(PanelAccor2, java.awt.BorderLayout.EAST);

        TabOK.addTab("ASESMENT PRA INDUKSI", internalFrame5);

        internalFrame1.add(TabOK, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(TNoRw.getText().trim().equals("")){
                Valid.textKosong(TNoRw,"Silahkan pilih data pasien terlebih dahulu !");
            }else{         
                if(Sequel.menyimpantf("penilaian_awal_anestesi_checklist","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Asuhan Anestesi",26,new String[]{
                        TNoRw.getText(),
                        Valid.SetTgl(Tanggal.getSelectedItem()+""),
                        Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                        KdPetugas.getText(),
                        tindakan_ok.getText(),
                        diagnosa_ok.getText(),
                        jenis_anestesi.getSelectedItem().toString(),
                        jenisanestesi_text.getText(),
                        String.valueOf(A1.isSelected()),
                        String.valueOf(A2.isSelected()),
                        String.valueOf(A3.isSelected()),
                        String.valueOf(A4.isSelected()),
                        String.valueOf(B1.isSelected()),
                        String.valueOf(B2.isSelected()),
                        String.valueOf(B3.isSelected()),
                        String.valueOf(B4.isSelected()),
                        String.valueOf(C1.isSelected()),
                        String.valueOf(C2.isSelected()),
                        String.valueOf(D1.isSelected()),
                        String.valueOf(D2.isSelected()),
                        String.valueOf(D4.isSelected()),
                        String.valueOf(D5.isSelected()),
                        String.valueOf(D6.isSelected()),
                        String.valueOf(D7.isSelected()),
                        String.valueOf(D8.isSelected()),
                        String.valueOf(D9.isSelected()),
                       
                        
                })==true){
                    tampil();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Menyimpan Data");
            }
               
      }
        }else if(TabOK.getSelectedIndex()==1){
            if(TNoRw2.getText().trim().equals("")){
                Valid.textKosong(TNoRw2,"Silahkan pilih data pasien terlebih dahulu !");
            }else if(KdPetugas1.getText().trim().equals("")){
                Valid.textKosong(KdPetugas1,"Petugas");
            }else if(Anamnesis.getText().trim().equals("")){
                Valid.textKosong(Anamnesis,"Anamnesis");
            }else if(Preop.getText().trim().equals("")){
                Valid.textKosong(Preop,"Diagnosa Pre Operasi");
            }else if(Rencanaop.getText().trim().equals("")){
                Valid.textKosong(Rencanaop,"Rencana Operasi");
            }else if(tb.getText().trim().equals("")){
                Valid.textKosong(tb,"TB");
            }else if(bb.getText().trim().equals("")){
                Valid.textKosong(bb,"BB");
            }else{ 
                if(Sequel.menyimpantf("penilaian_awal_anestesi_prasedasi","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Pra Sedasi",66,new String[]{
                        TNoRw2.getText(),
                        Valid.SetTgl(Tanggal1.getSelectedItem()+""),
                        Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                        KdPetugas1.getText(),
                        Anamnesis.getText(),
                        Preop.getText(),
                        Rencanaop.getText(),
                        tb.getText(),
                        bb.getText(),
                        CmbObat.getSelectedItem().toString(),
                        obat_text.getText(),
                        CmbAlergi.getSelectedItem().toString(),
                        riwayat_alergi_text.getText(),
                        CmbPenyakit.getSelectedItem().toString(),
                        riwayat_penyakit_text.getText(),
                        CmbAnestesi.getSelectedItem().toString(),
                        CmbAnestesi2.getSelectedItem().toString(),
                        CmbMerokok.getSelectedItem().toString(),
                        Komplikasi.getText(),
                        CmbB1.getSelectedItem().toString(),
                        B1_alat.getText(),
                        B1_rr.getText(),
                        B1_vesikuler.getText(),
                        B1_rhonki.getText(),
                        B1_wheezing_plus.getText(),
                        B1_wheezing_min.getText(),
                        B2_td.getText(),
                        B2_hr.getText(),
                        B2_cmb.getSelectedItem().toString(),
                        B2_cmbkonjungtiva.getSelectedItem().toString(),
                        B3_e.getText(),
                        B3_m.getText(),
                        B3_v.getText(),
                        B3_cmbpupil.getSelectedItem().toString(),
                        B3_cmbhemiparese.getSelectedItem().toString(),
                        B4_urin.getText(),
                        B4_cmbwarnaurin.getSelectedItem().toString(),
                        B5_cmbkembung.getSelectedItem().toString(),
                        B5_cmbdiare.getSelectedItem().toString(),
                        B5_cmbmuntah.getSelectedItem().toString(),
                        B6_cmbalatbantu.getSelectedItem().toString(),
                        B6_cmbfraktur.getSelectedItem().toString(),
                        Laboratorium.getText(),
                        Radiologi.getText(),
                        Elektrokardiografi.getText(),
                        String.valueOf(Asa.isSelected()),
                        String.valueOf(Asa1.isSelected()),
                        String.valueOf(Asa2.isSelected()),
                        String.valueOf(Asa3.isSelected()),
                        String.valueOf(Asa4.isSelected()),
                        String.valueOf(Asa5.isSelected()),
                        String.valueOf(Asa6.isSelected()),
                        String.valueOf(Asa7.isSelected()),
                        cmb_GA.getSelectedItem().toString(),
                        cmb_REGIONAL.getSelectedItem().toString(),
                        anestesi_blok.getText(),
                        cmb_obat.getSelectedItem().toString(),
                        obat_obatan_text.getText(),
                        cmb_cairan.getSelectedItem().toString(),
                        cairan_teks.getText(),
                        Cmbmonitoring.getSelectedItem().toString(),
                        String.valueOf(rencana_inap.isSelected()),
                        String.valueOf(rencana_hcu.isSelected()),
                        String.valueOf(rencana_icu.isSelected()),
                        String.valueOf(rencana_ralan.isSelected()),
                        String.valueOf(rencana_igd.isSelected())
      
                })==true){
                    tampilprasedasi();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Menyimpan Data");
            }  
          }
        }else if(TabOK.getSelectedIndex()==2){
           if(TNoRw3.getText().trim().equals("")){
                Valid.textKosong(TNoRw3,"Silahkan pilih data pasien terlebih dahulu !");
            }else if(tab3_td.getText().trim().equals("")){
                Valid.textKosong(tab3_td,"SPO");
            }else if(tab3_nadi.getText().trim().equals("")){
                Valid.textKosong(tab3_nadi,"HR");
            }else if(tab3_tb.getText().trim().equals("")){
                Valid.textKosong(tab3_tb,"RR");
            }else if(tab3_respirasi.getText().trim().equals("")){
                Valid.textKosong(tab3_respirasi,"Suhu");
            }else{    
                if(Sequel.menyimpantf("penilaian_awal_anestesi_prainduksi","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Awal Pra Induksi",62,new String[]{
                        TNoRw3.getText(),
                        Valid.SetTgl(Tanggal2.getSelectedItem()+""),
                        Jam2.getSelectedItem()+":"+Menit2.getSelectedItem()+":"+Detik2.getSelectedItem(),
                        KdDokter3.getText(),
                        String.valueOf(tab3_alergi1.isSelected()),
                        alergi_teks.getText(),
                        tab3_bb.getText(),
                        tab3_td.getText(),
                        tab3_nadi.getText(),
                        tab3_tb.getText(),
                        tab3_respirasi.getText(),
                        tab3_darah.getText(),
                        tab3_suhu.getText(),
                        tab3_gcs.getText(),
                        tab3_rh.getText(),
                        tab3_hb.getText(),
                        tab3_vas.getText(),
                        tab3_ht.getText(),
                        tab3_lain.getText(),
                        String.valueOf(tab3_normal.isSelected()),
                        String.valueOf(tab3_bukamulut.isSelected()),
                        String.valueOf(tab3_jarak.isSelected()),
                        String.valueOf(tab3_mallampati.isSelected()),
                        String.valueOf(tab3_leher.isSelected()),
                        String.valueOf(tab3_abnormal.isSelected()),
                        String.valueOf(tab3_auto.isSelected()),
                        String.valueOf(tab3_allo.isSelected()),
                        String.valueOf(tab3_asa1.isSelected()),
                        String.valueOf(tab3_asa2.isSelected()),
                        String.valueOf(tab3_asa3.isSelected()),
                        String.valueOf(tab3_asa4.isSelected()),
                        String.valueOf(tab3_asa5.isSelected()),
                        String.valueOf(tab3_asaE.isSelected()),
                        penyulit_praanestesi.getText(),
                        String.valueOf(tab3_cek1.isSelected()),
                        String.valueOf(tab3_cek2.isSelected()),
                        String.valueOf(tab3_cek3.isSelected()),
                        String.valueOf(tab3_cek4.isSelected()),
                        String.valueOf(tab3_cek5.isSelected()),
                        String.valueOf(tab3_tek1.isSelected()),
                        String.valueOf(tab3_tek2.isSelected()),
                        String.valueOf(tab3_tek3.isSelected()),
                        String.valueOf(tab3_tek4.isSelected()),
                        String.valueOf(tab3_tek5.isSelected()),
                        String.valueOf(tab3_tek6.isSelected()),
                        String.valueOf(tab3_tek7.isSelected()),
                        String.valueOf(tab3_tek8.isSelected()),
                        String.valueOf(tab3_tek9.isSelected()),
                        String.valueOf(tab3_tek10.isSelected()),
                        String.valueOf(tab3_tek11.isSelected()),
                        anestesi_teks.getText(),
                        String.valueOf(tab3_moni1.isSelected()),
                        String.valueOf(tab3_moni2.isSelected()),
                        String.valueOf(tab3_moni3.isSelected()),
                        tab3_moni3_ket.getText(),
                        String.valueOf(tab3_moni4.isSelected()),
                        tab3_moni4_ket.getText(),
                        String.valueOf(tab3_moni5.isSelected()),
                        String.valueOf(tab3_moni6.isSelected()),
                        String.valueOf(tab3_moni7.isSelected()),
                        String.valueOf(tab3_moni8.isSelected()),
                        String.valueOf(tab3_moni9.isSelected()),
                        
                })==true){
                    tampilprainduksi();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Menyimpan Data Pra Induksi");
            }  
          }
        }else if(TabOK.getSelectedIndex()==3){
           
       
        }            
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            if(TabOK.getSelectedIndex()==0){
                Valid.pindah(evt,TNoRw,BtnBatal);
            }else if(TabOK.getSelectedIndex()==1){
                Valid.pindah(evt,TNoRw,BtnBatal);
            }else if(TabOK.getSelectedIndex()==2){
                Valid.pindah(evt,TNoRw,BtnBatal);
            }else if(TabOK.getSelectedIndex()==3){
                Valid.pindah(evt,TNoRw,BtnBatal);
            }                
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(tabModeAnestesi.getRowCount()!=0){
                if(tbChecklist.getSelectedRow()>-1){
                    if(Sequel.queryu2tf("delete from penilaian_awal_anestesi_checklist where no_rawat=?",1,new String[]{
                    tbChecklist.getValueAt(tbChecklist.getSelectedRow(),0).toString()
                })==true){
                    tampil();
                    emptTeks();
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Asuhan Pre Op terlebih dahulu..!!");
            }   
        }  
           
        }else if(TabOK.getSelectedIndex()==1){
           if(tabModeInOp.getRowCount()!=0){
                if(tbPrasedasi.getSelectedRow()>-1){
                    if(Sequel.queryu2tf("delete from penilaian_awal_anestesi_prasedasi where no_rawat=? and tanggal=? and jam =?",3,new String[]{
                    tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),0).toString(),
                    tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),1).toString(),
                    tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString(),
                })==true){
                    tampilprasedasi();
                    emptTeks();
                    
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Asuhan Pra Sedasi terlebih dahulu..!!");
            }   
        }  
        }else if(TabOK.getSelectedIndex()==2){
             if(tabModePraInduksi.getRowCount()!=0){
                if(tbPrainduksi.getSelectedRow()>-1){
                    if(Sequel.queryu2tf("delete from penilaian_awal_anestesi_prainduksi where no_rawat=?",1,new String[]{
                    tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),0).toString()
                })==true){
                    tampilprainduksi();
                    emptTeks();
                    
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Asuhan Pra Induksi terlebih dahulu..!!");
            }   
            } 
        }else if(TabOK.getSelectedIndex()==3){
         
        }             
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnHapus);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
    dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            dispose();
        }else{Valid.pindah(evt,BtnHapus,BtnKeluar);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
//        TabSettingMouseClicked(null);
    }//GEN-LAST:event_formWindowOpened

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(TNoRw.getText().trim().equals("")){
                Valid.textKosong(TNoRw,"Nomor Rawat");
            }else if(TNoRM.getText().trim().equals("")){
                Valid.textKosong(TNoRM,"Nomor RM");
            }else if(TPasien.getText().trim().equals("")){
                Valid.textKosong(TPasien,"Pasien");
            }else{
                if(tbChecklist.getSelectedRow()>-1){
                        if(Sequel.mengedittf("penilaian_awal_anestesi_checklist","no_rawat=?","no_rawat=?, tanggal=?, jam=?, kd_petugas=?, tindakan_ok=?, diagnosa_ok=?, jenis_anestesi=?,jenisanestesi_text=?, airway_1=?, airway_2=?, airway_3=?, airway_4=?, breathing_1=?, breathing_2=?, breathing_3=?, breathing_4=?, obat_1=?, obat_2=?, obat_3=?, obat_4=?, obat_5=?, obat_6=?, obat_7=?, obat_8=?, obat_9=?, obat_10=?",27,new String[]{
                                TNoRw.getText(),
                                Valid.SetTgl(Tanggal.getSelectedItem()+""),
                                Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                                KdPetugas.getText(),
                                tindakan_ok.getText(),
                                diagnosa_ok.getText(),
                                jenis_anestesi.getSelectedItem().toString(),
                                jenisanestesi_text.getText(),
                                String.valueOf(A1.isSelected()),
                                String.valueOf(A2.isSelected()),
                                String.valueOf(A3.isSelected()),
                                String.valueOf(A4.isSelected()),
                                String.valueOf(B1.isSelected()),
                                String.valueOf(B2.isSelected()),
                                String.valueOf(B3.isSelected()),
                                String.valueOf(B4.isSelected()),
                                String.valueOf(C1.isSelected()),
                                String.valueOf(C2.isSelected()),
                                String.valueOf(D1.isSelected()),
                                String.valueOf(D2.isSelected()),
                                String.valueOf(D4.isSelected()),
                                String.valueOf(D5.isSelected()),
                                String.valueOf(D6.isSelected()),
                                String.valueOf(D7.isSelected()),
                                String.valueOf(D8.isSelected()),
                                String.valueOf(D9.isSelected()),
                                tbChecklist.getValueAt(tbChecklist.getSelectedRow(),0).toString()
                       })==true){
                           JOptionPane.showMessageDialog(rootPane,"Berhasil Mengedit Data Checklist..!!");
                           tampil();
                           emptTeks();
                   }
                
                }else{
                    JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Checklist terlebih dahulu..!!");
                }
                tampil();
                emptTeks();
            }
        }else if(TabOK.getSelectedIndex()==1){            
            if(TNoRw2.getText().trim().equals("")){
                Valid.textKosong(TNoRw2,"Silahkan pilih data pasien terlebih dahulu !");
            }else if(KdPetugas1.getText().trim().equals("")){
                Valid.textKosong(KdPetugas1,"Petugas");
            }else if(Anamnesis.getText().trim().equals("")){
                Valid.textKosong(Anamnesis,"Anamnesis");
            }else if(Preop.getText().trim().equals("")){
                Valid.textKosong(Preop,"Diagnosa Pre Operasi");
            }else if(Rencanaop.getText().trim().equals("")){
                Valid.textKosong(Rencanaop,"Rencana Operasi");
            }else if(tb.getText().trim().equals("")){
                Valid.textKosong(tb,"TB");
            }else if(bb.getText().trim().equals("")){
                Valid.textKosong(bb,"BB");
            }else{ 
                if(tbPrasedasi.getSelectedRow()>-1){
                        if(Sequel.mengedittf("penilaian_awal_anestesi_prasedasi","no_rawat=?","no_rawat=?,tanggal=?,jam=?,kd_dokter=?,anamnesis=?,diagnosa_preop=?,rencana_operasi=?,tb=?,bb=?,obat_dikonsumsi=?,obat_dikonsumsi_ket=?,riwayat_alergi=?,riwayat_alergi_ket=?,riwayat_penyakit=?,riwayat_penyakit_ket=?,riwayat_anestesi=?,jenis_anestesi=?,riwayat_merokok=?,komplikasi_anestesi=?,fisik_b1=?,fisik_alat=?,fisik_rr=?,fisik_vesikuler=?,fisik_rhonki=?,fisik_wheezing_plus=?,fisik_wheezing_min=?,fisik_td=?,fisik_hr=?,fisik_hr_ket=?,fisik_konjungtiva=?,fisik_gcse=?,fisik_gcsm=?,fisik_gcsv=?,fisik_pupil=?,fisik_hemiparese=?,fisik_urin=?,fisik_warnaurin=?,fisik_perut=?,fisik_diare=?,fisik_muntah=?,fisik_alatbantu=?,fisik_fraktur=?,penunjang_lab=?,penunjang_rad=?,penunjang_elektro=?,asa=?,asa1=?,asa2=?,asa3=?,asa4=?,asa5=?,asa6=?,asaE=?,rencana_ga= ?, rencana_reg = ?, rencana_blok = ?, obat_obatan=?,obat_obatan_ket=?,cairan=?,cairan_ket=?,monitoring_khusus=?,rencana_perawatan_inap=?,rencana_hcu=?,rencana_icu=?,rencana_rajal=?,rencana_igd=?",67,new String[]{
                                TNoRw2.getText(),
                                Valid.SetTgl(Tanggal1.getSelectedItem()+""),
                                Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                                KdPetugas1.getText(),
                                Anamnesis.getText(),
                                Preop.getText(),
                                Rencanaop.getText(),
                                tb.getText(),
                                bb.getText(),
                                CmbObat.getSelectedItem().toString(),
                                obat_text.getText(),
                                CmbAlergi.getSelectedItem().toString(),
                                riwayat_alergi_text.getText(),
                                CmbPenyakit.getSelectedItem().toString(),
                                riwayat_penyakit_text.getText(),
                                CmbAnestesi.getSelectedItem().toString(),
                                CmbAnestesi2.getSelectedItem().toString(),
                                CmbMerokok.getSelectedItem().toString(),
                                Komplikasi.getText(),
                                CmbB1.getSelectedItem().toString(),
                                B1_alat.getText(),
                                B1_rr.getText(),
                                B1_vesikuler.getText(),
                                B1_rhonki.getText(),
                                B1_wheezing_plus.getText(),
                                B1_wheezing_min.getText(),
                                B2_td.getText(),
                                B2_hr.getText(),
                                B2_cmb.getSelectedItem().toString(),
                                B2_cmbkonjungtiva.getSelectedItem().toString(),
                                B3_e.getText(),
                                B3_m.getText(),
                                B3_v.getText(),
                                B3_cmbpupil.getSelectedItem().toString(),
                                B3_cmbhemiparese.getSelectedItem().toString(),
                                B4_urin.getText(),
                                B4_cmbwarnaurin.getSelectedItem().toString(),
                                B5_cmbkembung.getSelectedItem().toString(),
                                B5_cmbdiare.getSelectedItem().toString(),
                                B5_cmbmuntah.getSelectedItem().toString(),
                                B6_cmbalatbantu.getSelectedItem().toString(),
                                B6_cmbfraktur.getSelectedItem().toString(),
                                Laboratorium.getText(),
                                Radiologi.getText(),
                                Elektrokardiografi.getText(),
                                String.valueOf(Asa.isSelected()),
                                String.valueOf(Asa1.isSelected()),
                                String.valueOf(Asa2.isSelected()),
                                String.valueOf(Asa3.isSelected()),
                                String.valueOf(Asa4.isSelected()),
                                String.valueOf(Asa5.isSelected()),
                                String.valueOf(Asa6.isSelected()),
                                String.valueOf(Asa7.isSelected()),
                                cmb_GA.getSelectedItem().toString(),
                                cmb_REGIONAL.getSelectedItem().toString(),
                                anestesi_blok.getText(),
                                cmb_obat.getSelectedItem().toString(),
                                obat_obatan_text.getText(),
                                cmb_cairan.getSelectedItem().toString(),
                                cairan_teks.getText(),
                                Cmbmonitoring.getSelectedItem().toString(),
                                String.valueOf(rencana_inap.isSelected()),
                                String.valueOf(rencana_hcu.isSelected()),
                                String.valueOf(rencana_icu.isSelected()),
                                String.valueOf(rencana_ralan.isSelected()),
                                String.valueOf(rencana_igd.isSelected()),
                                tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),0).toString()
                       })==true){
                           JOptionPane.showMessageDialog(rootPane,"Berhasil Mengedit Pra Sedasi..!!");
                           tampilprasedasi();
                           emptTeks();
                   }             
                }else{
                    JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Pra Sedasi terlebih dahulu..!!");
                }
                tampilprasedasi();
                emptTeks();
                 
         }
          
        }else if(TabOK.getSelectedIndex()==2){
            if(TNoRw.getText().trim().equals("")){
                Valid.textKosong(TNoRw,"Nomor Rawat");
            }else if(TNoRM.getText().trim().equals("")){
                Valid.textKosong(TNoRM,"Nomor RM");
            }else if(TPasien.getText().trim().equals("")){
                Valid.textKosong(TPasien,"Pasien");
            }else{
                 if(Sequel.mengedittf("penilaian_awal_anestesi_prainduksi","no_rawat=?","no_rawat=?, tanggal=?, jam=?, kd_dokter=?, alergi=?,alergi_ket=?, bb=?, td=?, nadi=?, tb=?, respirasi=?, gol_darah=?, suhu=?, gcs=?, rh=?, hb=?, vas=?, ht=?, lain=?, fisik_normal=?, fisik_bukamulut=?, fisik_jarak=?, fisik_mallampati=?, fisik_gerakanleher=?, fisik_abnormal=?, anamnesis_auto=?, anamnesis_allo=?, asa1=?, asa2=?, asa3=?, asa4=?, asa5=?, asaE=?, penyulit_praanestesi=?, cek_1=?, cek_2=?, cek_3=?, cek_4=?, cek_5=?, ga_1=?, ga_2=?, ga_3=?, ga_4=?, ga_5=?, reg_1=?, reg_2=?, reg_3=?, reg_4=?, reg_5=?, anestesi_lain=?,anestesi_lain_ket=?, monitoring_1=?, monitoring_2=?, monitoring_3=?, monitoring_3_ket=?, monitoring_4=?, monitoring_4_ket=?, monitoring_5=?, monitoring_6=?, monitoring_7=?, monitoring_8=?, monitoring_9=? ",63,new String[]{
                        TNoRw2.getText(),
                        Valid.SetTgl(Tanggal2.getSelectedItem()+""),
                        Jam2.getSelectedItem()+":"+Menit2.getSelectedItem()+":"+Detik2.getSelectedItem(),
                        KdDokter3.getText(),
                        String.valueOf(tab3_alergi1.isSelected()),
                        alergi_teks.getText(),
                        tab3_bb.getText(),
                        tab3_td.getText(),
                        tab3_nadi.getText(),
                        tab3_tb.getText(),
                        tab3_respirasi.getText(),
                        tab3_darah.getText(),
                        tab3_suhu.getText(),
                        tab3_gcs.getText(),
                        tab3_rh.getText(),
                        tab3_hb.getText(),
                        tab3_vas.getText(),
                        tab3_ht.getText(),
                        tab3_lain.getText(),
                        String.valueOf(tab3_normal.isSelected()),
                        String.valueOf(tab3_bukamulut.isSelected()),
                        String.valueOf(tab3_jarak.isSelected()),
                        String.valueOf(tab3_mallampati.isSelected()),
                        String.valueOf(tab3_leher.isSelected()),
                        String.valueOf(tab3_abnormal.isSelected()),
                        String.valueOf(tab3_auto.isSelected()),
                        String.valueOf(tab3_allo.isSelected()),
                        String.valueOf(tab3_asa1.isSelected()),
                        String.valueOf(tab3_asa2.isSelected()),
                        String.valueOf(tab3_asa3.isSelected()),
                        String.valueOf(tab3_asa4.isSelected()),
                        String.valueOf(tab3_asa5.isSelected()),
                        String.valueOf(tab3_asaE.isSelected()),
                        penyulit_praanestesi.getText(),
                        String.valueOf(tab3_cek1.isSelected()),
                        String.valueOf(tab3_cek2.isSelected()),
                        String.valueOf(tab3_cek3.isSelected()),
                        String.valueOf(tab3_cek4.isSelected()),
                        String.valueOf(tab3_cek5.isSelected()),
                        String.valueOf(tab3_tek1.isSelected()),
                        String.valueOf(tab3_tek2.isSelected()),
                        String.valueOf(tab3_tek3.isSelected()),
                        String.valueOf(tab3_tek4.isSelected()),
                        String.valueOf(tab3_tek5.isSelected()),
                        String.valueOf(tab3_tek6.isSelected()),
                        String.valueOf(tab3_tek7.isSelected()),
                        String.valueOf(tab3_tek8.isSelected()),
                        String.valueOf(tab3_tek9.isSelected()),
                        String.valueOf(tab3_tek10.isSelected()),
                        String.valueOf(tab3_tek11.isSelected()),
                        anestesi_teks.getText(),
                        String.valueOf(tab3_moni1.isSelected()),
                        String.valueOf(tab3_moni2.isSelected()),
                        String.valueOf(tab3_moni3.isSelected()),
                        tab3_moni3_ket.getText(),
                        String.valueOf(tab3_moni4.isSelected()),
                        tab3_moni4_ket.getText(),
                        String.valueOf(tab3_moni5.isSelected()),
                        String.valueOf(tab3_moni6.isSelected()),
                        String.valueOf(tab3_moni7.isSelected()),
                        String.valueOf(tab3_moni8.isSelected()),
                        String.valueOf(tab3_moni9.isSelected()),
                        tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),0).toString()
                })==true){
                    tampilprainduksi();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Megubah Data");
                }  
            }
        }else if(TabOK.getSelectedIndex()==3){
           
        
        }              
    }//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnKeluar);
        }
    }//GEN-LAST:event_BtnEditKeyPressed

    private void pp2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pp2ActionPerformed
             
    }//GEN-LAST:event_pp2ActionPerformed

    private void TabOKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabOKMouseClicked
        if(TabOK.getSelectedIndex()==0){
            tampil();
        }else if(TabOK.getSelectedIndex()==1){
            tampil();
        }else if(TabOK.getSelectedIndex()==2){
            tampil();
            getdataprainduksi();
        }else if(TabOK.getSelectedIndex()==3){
            tampil();
           
        }
    }//GEN-LAST:event_TabOKMouseClicked

    private void pp1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pp1ActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        for (int i = 0; i < tbChecklist.getRowCount(); i++) {             
                                     
        }            
        this.setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_pp1ActionPerformed

    private void ppCetakLembarFormulirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppCetakLembarFormulirActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(tabModeAnestesi.getRowCount()!=0){
                    try {
                       int row=tbChecklist.getSelectedRow();
                         if(row!= -1){
                                   if(tbChecklist.getSelectedRow()>-1){
                                         Map<String, Object> param = new HashMap<>();
                                         param.put("namars",akses.getnamars());
                                         param.put("alamatrs",akses.getalamatrs());
                                         param.put("kotars",akses.getkabupatenrs());
                                         param.put("propinsirs",akses.getpropinsirs());
                                         param.put("kontakrs",akses.getkontakrs());
                                         param.put("emailrs",akses.getemailrs());
                                         param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                                         param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                                         param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                                         param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                                         param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                                         param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                                         param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                                         finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbChecklist.getValueAt(tbChecklist.getSelectedRow(),3).toString());
                                         param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbChecklist.getValueAt(tbChecklist.getSelectedRow(),4).toString()+"\nID "+(finger.equals("")?tbChecklist.getValueAt(tbChecklist.getSelectedRow(),4).toString():finger)+"\n"+tbChecklist.getValueAt(tbChecklist.getSelectedRow(),1).toString()+" "+tbChecklist.getValueAt(tbChecklist.getSelectedRow(),2).toString());  
                                         Valid.MyReportqry("rptCetakPenilaianAnestesi.jasper","report","::[ Formulir Checklist Kelengkapan Peralatan & Obat ]::",
                                                 "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                                                 "penilaian_awal_anestesi_checklist.*,petugas.nip,petugas.nama "+
                                                 "from penilaian_awal_anestesi_checklist inner join reg_periksa on penilaian_awal_anestesi_checklist.no_rawat=reg_periksa.no_rawat "+
                                                 "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                                                 "inner join petugas on petugas.nip=penilaian_awal_anestesi_checklist.kd_petugas "+
                                                 "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                                                 "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                                                 "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                                                 "where penilaian_awal_anestesi_checklist.no_rawat='"+tbChecklist.getValueAt(tbChecklist.getSelectedRow(),0).toString()+"'",param);
                                        }
                            }
                        } catch (java.lang.NullPointerException e) {
                       }
          }
        }else if(TabOK.getSelectedIndex()==1){
          if(tabModeInOp.getRowCount()!=0){
                 try {
                      int row=tbPrasedasi.getSelectedRow();
                        if(row!= -1){
                                  if(tbPrasedasi.getSelectedRow()>-1){
                                        Map<String, Object> param = new HashMap<>();
                                        param.put("namars",akses.getnamars());
                                        param.put("alamatrs",akses.getalamatrs());
                                        param.put("kotars",akses.getkabupatenrs());
                                        param.put("propinsirs",akses.getpropinsirs());
                                        param.put("kontakrs",akses.getkontakrs());
                                        param.put("emailrs",akses.getemailrs());
                                        param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                                        param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                                        param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                                        param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                                        param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                                        param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                                        param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                                        finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),3).toString());
                                        param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),4).toString()+"\nID "+(finger.equals("")?tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),4).toString():finger)+"\n"+tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),1).toString()+" "+tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString());  
                                        ruang_rawat=Sequel.cariIsi("SELECT bangsal.nm_bangsal FROM kamar_inap INNER JOIN kamar ON kamar_inap.kd_kamar = kamar.kd_kamar INNER JOIN bangsal ON bangsal.kd_bangsal = kamar.kd_bangsal WHERE kamar_inap.no_rawat = ? order by kamar_inap.tgl_masuk desc, kamar_inap.jam_masuk desc limit 1",tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),0).toString());
                                        param.put("ruang_rawat", ruang_rawat);
                                        Valid.MyReportqry("rptCetakPenilaianPrasedasi.jasper","report","::[ Formulir Pra Sedasi ]::",
                                                "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                                                "penilaian_awal_anestesi_prasedasi.*,dokter.kd_dokter,dokter.nm_dokter "+
                                                "from penilaian_awal_anestesi_prasedasi inner join reg_periksa on penilaian_awal_anestesi_prasedasi.no_rawat=reg_periksa.no_rawat "+
                                                "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                                                "inner join dokter on dokter.kd_dokter=penilaian_awal_anestesi_prasedasi.kd_dokter "+
                                                "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                                                "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                                                "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                                                "where penilaian_awal_anestesi_prasedasi.no_rawat='"+tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),0).toString()+"'",param);
                                    }
                        }
                    } catch (java.lang.NullPointerException e) {
                   }
            }     
        }else if(TabOK.getSelectedIndex()==2){
            if(tabModePraInduksi.getRowCount()!=0){
                 try {
                      int row=tbPrainduksi.getSelectedRow();
                        if(row!= -1){
                                  if(tbPrainduksi.getSelectedRow()>-1){
                                        Map<String, Object> param = new HashMap<>();
                                        param.put("namars",akses.getnamars());
                                        param.put("alamatrs",akses.getalamatrs());
                                        param.put("kotars",akses.getkabupatenrs());
                                        param.put("propinsirs",akses.getpropinsirs());
                                        param.put("kontakrs",akses.getkontakrs());
                                        param.put("emailrs",akses.getemailrs());
                                        param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                                        param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                                        param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                                        param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                                        param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                                        param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                                        param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                                        finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),3).toString());
                                        param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),4).toString()+"\nID "+(finger.equals("")?tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),3).toString():finger)+"\n"+tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),1).toString()+" "+tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),2).toString());  
                                        ruang_rawat=Sequel.cariIsi("SELECT bangsal.nm_bangsal FROM kamar_inap INNER JOIN kamar ON kamar_inap.kd_kamar = kamar.kd_kamar INNER JOIN bangsal ON bangsal.kd_bangsal = kamar.kd_bangsal WHERE kamar_inap.no_rawat = ? order by kamar_inap.tgl_masuk desc, kamar_inap.jam_masuk desc limit 1",tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),0).toString());
                                        param.put("ruang_rawat", ruang_rawat);
                                        Valid.MyReportqry("rptCetakPenilaianPrainduksi.jasper","report","::[ Formulir Pra Induksi ]::",
                                                "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                                                "penilaian_awal_anestesi_prainduksi.*,dokter.kd_dokter,dokter.nm_dokter "+
                                                "from penilaian_awal_anestesi_prainduksi inner join reg_periksa on penilaian_awal_anestesi_prainduksi.no_rawat=reg_periksa.no_rawat "+
                                                "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                                                "inner join dokter on dokter.kd_dokter=penilaian_awal_anestesi_prainduksi.kd_dokter "+
                                                "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                                                "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                                                "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                                                "where penilaian_awal_anestesi_prainduksi.no_rawat='"+tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),0).toString()+"'",param);
                                    }
                        }
                    } catch (java.lang.NullPointerException e) {
                }
            }  
        }

    }//GEN-LAST:event_ppCetakLembarFormulirActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnCari, TCari);
        }
    }//GEN-LAST:event_BtnAllKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
    }//GEN-LAST:event_BtnAllActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
    }//GEN-LAST:event_BtnCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
    }//GEN-LAST:event_BtnCariActionPerformed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCariKeyPressed

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
     
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed

        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed

    }//GEN-LAST:event_KdPetugasKeyPressed

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
          
        }else{
            Valid.pindah(evt,TCari,BtnPetugas);
        }
    }//GEN-LAST:event_TNoRwKeyPressed

    private void JamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JamActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamActionPerformed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MenitKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DetikKeyPressed

    private void ChkAccorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccorActionPerformed
        if(!(TNoRw.getText().equals(""))){
            if(TNoRw.getText().equals("")){
                Valid.textKosong(TCari,"Pasien");
            }else{
                isPhoto();
                TabDataMouseClicked(null);
            }
        }else{
            ChkAccor.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih Pasien..!!!");
        }
    }//GEN-LAST:event_ChkAccorActionPerformed

    private void BtnRefreshPhoto1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto1ActionPerformed
        tampil();
    }//GEN-LAST:event_BtnRefreshPhoto1ActionPerformed

    private void TabDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabDataMouseClicked
        if(TabData.getSelectedIndex()==0){
           
        }else if(TabData.getSelectedIndex()==1){
           
        }
    }//GEN-LAST:event_TabDataMouseClicked

    private void tbChecklistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbChecklistMouseClicked
      if(tabModeAnestesi.getRowCount()!=0){
                   try {
                       getData();
                   } catch (java.lang.NullPointerException e) {
                   }
      }  
    }//GEN-LAST:event_tbChecklistMouseClicked

    private void tbChecklistKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbChecklistKeyPressed
               if(tabModeAnestesi.getRowCount()!=0){
                   try {
                       getData();
                   } catch (java.lang.NullPointerException e) {
                   }
               }     
    }//GEN-LAST:event_tbChecklistKeyPressed

    private void BtnAll1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAll1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnCari, TCari);
        }
    }//GEN-LAST:event_BtnAll1KeyPressed

    private void BtnAll1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAll1ActionPerformed
        TCari1.setText("");
        tampil();
    }//GEN-LAST:event_BtnAll1ActionPerformed

    private void BtnCari1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCari1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
    }//GEN-LAST:event_BtnCari1KeyPressed

    private void BtnCari1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCari1ActionPerformed
        tampil();
    }//GEN-LAST:event_BtnCari1ActionPerformed

    private void TCari1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCari1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCari1ActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari1.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCari1KeyPressed

    private void jkMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jkMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jkMouseMoved

    private void jkMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jkMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jkMouseExited

    private void jkKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jkKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jkKeyPressed

    private void jkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jkActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jkActionPerformed

    private void TNoRw2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRw2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TNoRw2KeyPressed

    private void KdPetugas1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugas1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdPetugas1KeyPressed

    private void BtnPetugas1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugas1ActionPerformed
        i = 2;
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnPetugas1ActionPerformed

    private void BtnPetugas1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugas1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugas1KeyPressed

    private void tbMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tbMouseMoved

    private void tbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tbMouseExited

    private void tbKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbKeyPressed

    private void bbMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bbMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_bbMouseMoved

    private void bbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bbMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_bbMouseExited

    private void bbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bbActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bbActionPerformed

    private void bbKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bbKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_bbKeyPressed

    private void obat_textMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_obat_textMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_obat_textMouseMoved

    private void obat_textMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_obat_textMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_obat_textMouseExited

    private void obat_textKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_obat_textKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_obat_textKeyPressed

    private void Jam1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1ActionPerformed

    private void Jam1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1KeyPressed

    private void Menit1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit1KeyPressed

    private void Detik1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik1KeyPressed

    private void jk1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk1MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1MouseMoved

    private void jk1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1MouseExited

    private void jk1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jk1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1ActionPerformed

    private void jk1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jk1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1KeyPressed

    private void ChkAccor1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccor1ActionPerformed
     if(!(TNoRw.getText().equals(""))){
            if(TNoRw.getText().equals("")){
                Valid.textKosong(TCari,"Pasien");
            }else{
                isPhoto1();
                TabDataMouseClicked(null);
            }
        }else{
            ChkAccor.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih Pasien..!!!");
        }
    }//GEN-LAST:event_ChkAccor1ActionPerformed

    private void BtnRefreshPhoto2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto2ActionPerformed
        tampilprasedasi();
    }//GEN-LAST:event_BtnRefreshPhoto2ActionPerformed

    private void tbPrasedasiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPrasedasiMouseClicked
        getData2();
    }//GEN-LAST:event_tbPrasedasiMouseClicked

    private void tbPrasedasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPrasedasiKeyPressed
        getData2();
    }//GEN-LAST:event_tbPrasedasiKeyPressed

    private void TabData1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabData1MouseClicked
        if(TabData1.getSelectedIndex()==0){
           
        }else if(TabData1.getSelectedIndex()==1){
           
        }
    }//GEN-LAST:event_TabData1MouseClicked

    private void TCari2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCari2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TCari2KeyPressed

    private void BtnCari2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCari2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnCari2ActionPerformed

    private void BtnCari2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCari2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnCari2KeyPressed

    private void BtnAll2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAll2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAll2ActionPerformed

    private void BtnAll2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAll2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAll2KeyPressed

    private void TNoRw3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRw3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TNoRw3KeyPressed

    private void KdDokter3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokter3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdDokter3KeyPressed

    private void BtnPetugas2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugas2ActionPerformed
        i = 1;
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnPetugas2ActionPerformed

    private void BtnPetugas2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugas2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugas2KeyPressed

    private void tab3_bbMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_bbMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_bbMouseMoved

    private void tab3_bbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_bbMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_bbMouseExited

    private void tab3_bbKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_bbKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_bbKeyPressed

    private void tab3_tdMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_tdMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_tdMouseMoved

    private void tab3_tdMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_tdMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_tdMouseExited

    private void tab3_tdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tab3_tdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_tdActionPerformed

    private void tab3_tdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_tdKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_tdKeyPressed

    private void tab3_nadiMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_nadiMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_nadiMouseMoved

    private void tab3_nadiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_nadiMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_nadiMouseExited

    private void tab3_nadiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_nadiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_nadiKeyPressed

    private void tab3_tbMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_tbMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_tbMouseMoved

    private void tab3_tbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_tbMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_tbMouseExited

    private void tab3_tbKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_tbKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_tbKeyPressed

    private void Jam2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam2ActionPerformed

    private void Jam2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam2KeyPressed

    private void Menit2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit2KeyPressed

    private void Detik2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik2KeyPressed

    private void jk2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk2MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2MouseMoved

    private void jk2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2MouseExited

    private void jk2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jk2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2ActionPerformed

    private void jk2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jk2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2KeyPressed

    private void tab3_respirasiMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_respirasiMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_respirasiMouseMoved

    private void tab3_respirasiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_respirasiMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_respirasiMouseExited

    private void tab3_respirasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_respirasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_respirasiKeyPressed

    private void ChkAccor2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccor2ActionPerformed
        if(!(TNoRw3.getText().equals(""))){
            if(TNoRw3.getText().equals("")){
                Valid.textKosong(TCari2,"Pasien");
            }else{
                isPhoto2();
                TabDataMouseClicked(null);
            }
        }else{
            ChkAccor2.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih Pasien..!!!");
        }
    }//GEN-LAST:event_ChkAccor2ActionPerformed

    private void BtnRefreshPhoto3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto3ActionPerformed
            tampilprainduksi();
    }//GEN-LAST:event_BtnRefreshPhoto3ActionPerformed

    private void tbPrainduksiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPrainduksiMouseClicked
     getData3();
    }//GEN-LAST:event_tbPrainduksiMouseClicked

    private void tbPrainduksiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPrainduksiKeyPressed
     getData3();
    }//GEN-LAST:event_tbPrainduksiKeyPressed

    private void TabData2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabData2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_TabData2MouseClicked

    private void jenis_anestesiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jenis_anestesiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenis_anestesiActionPerformed

    private void jenis_anestesiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jenis_anestesiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenis_anestesiKeyPressed

    private void tindakan_okKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tindakan_okKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tindakan_okKeyPressed

    private void diagnosa_okKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_diagnosa_okKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_diagnosa_okKeyPressed

    private void jenisanestesi_textKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jenisanestesi_textKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenisanestesi_textKeyPressed

    private void RencanaopKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RencanaopKeyPressed

    }//GEN-LAST:event_RencanaopKeyPressed

    private void AnamnesisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AnamnesisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AnamnesisKeyPressed

    private void PreopKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PreopKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PreopKeyPressed

    private void CmbObatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbObatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbObatActionPerformed

    private void CmbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbObatKeyPressed
        Valid.pindah(evt,Rencanaop,BtnSimpan);
    }//GEN-LAST:event_CmbObatKeyPressed

    private void CmbAnestesi2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbAnestesi2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbAnestesi2ActionPerformed

    private void CmbAnestesi2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbAnestesi2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbAnestesi2KeyPressed

    private void CmbAlergiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbAlergiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbAlergiActionPerformed

    private void CmbAlergiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbAlergiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbAlergiKeyPressed

    private void CmbPenyakitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbPenyakitActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbPenyakitActionPerformed

    private void CmbPenyakitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbPenyakitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbPenyakitKeyPressed

    private void CmbMerokokActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbMerokokActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbMerokokActionPerformed

    private void CmbMerokokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbMerokokKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbMerokokKeyPressed

    private void CmbAnestesiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbAnestesiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbAnestesiActionPerformed

    private void CmbAnestesiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbAnestesiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbAnestesiKeyPressed

    private void KomplikasiMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_KomplikasiMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_KomplikasiMouseMoved

    private void KomplikasiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_KomplikasiMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_KomplikasiMouseExited

    private void KomplikasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KomplikasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KomplikasiKeyPressed

    private void CmbmonitoringActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbmonitoringActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbmonitoringActionPerformed

    private void CmbmonitoringKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbmonitoringKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbmonitoringKeyPressed

    private void B3_cmbpupilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B3_cmbpupilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_cmbpupilActionPerformed

    private void B3_cmbpupilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B3_cmbpupilKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_cmbpupilKeyPressed

    private void B1_alatMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_alatMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_alatMouseMoved

    private void B1_alatMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_alatMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_alatMouseExited

    private void B1_alatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B1_alatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_alatKeyPressed

    private void B1_vesikulerMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_vesikulerMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_vesikulerMouseMoved

    private void B1_vesikulerMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_vesikulerMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_vesikulerMouseExited

    private void B1_vesikulerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B1_vesikulerKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_vesikulerKeyPressed

    private void B1_rrMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_rrMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_rrMouseMoved

    private void B1_rrMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_rrMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_rrMouseExited

    private void B1_rrKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B1_rrKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_rrKeyPressed

    private void B1_rhonkiMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_rhonkiMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_rhonkiMouseMoved

    private void B1_rhonkiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_rhonkiMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_rhonkiMouseExited

    private void B1_rhonkiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B1_rhonkiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_rhonkiKeyPressed

    private void B1_wheezing_plusMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_wheezing_plusMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_wheezing_plusMouseMoved

    private void B1_wheezing_plusMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_wheezing_plusMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_wheezing_plusMouseExited

    private void B1_wheezing_plusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B1_wheezing_plusKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_wheezing_plusKeyPressed

    private void B2_tdMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B2_tdMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_tdMouseMoved

    private void B2_tdMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B2_tdMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_tdMouseExited

    private void B2_tdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B2_tdKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_tdKeyPressed

    private void B2_hrMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B2_hrMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_hrMouseMoved

    private void B2_hrMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B2_hrMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_hrMouseExited

    private void B2_hrKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B2_hrKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_hrKeyPressed

    private void CmbB1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbB1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbB1ActionPerformed

    private void CmbB1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbB1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbB1KeyPressed

    private void B2_cmbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B2_cmbActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_cmbActionPerformed

    private void B2_cmbKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B2_cmbKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_cmbKeyPressed

    private void B3_eMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B3_eMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_eMouseMoved

    private void B3_eMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B3_eMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_eMouseExited

    private void B3_eKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B3_eKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_eKeyPressed

    private void B4_urinMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B4_urinMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B4_urinMouseMoved

    private void B4_urinMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B4_urinMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B4_urinMouseExited

    private void B4_urinKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B4_urinKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B4_urinKeyPressed

    private void B3_vMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B3_vMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_vMouseMoved

    private void B3_vMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B3_vMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_vMouseExited

    private void B3_vKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B3_vKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_vKeyPressed

    private void B2_cmbkonjungtivaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B2_cmbkonjungtivaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_cmbkonjungtivaActionPerformed

    private void B2_cmbkonjungtivaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B2_cmbkonjungtivaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B2_cmbkonjungtivaKeyPressed

    private void B4_cmbwarnaurinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B4_cmbwarnaurinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B4_cmbwarnaurinActionPerformed

    private void B4_cmbwarnaurinKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B4_cmbwarnaurinKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B4_cmbwarnaurinKeyPressed

    private void B3_mMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B3_mMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_mMouseMoved

    private void B3_mMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B3_mMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_mMouseExited

    private void B3_mKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B3_mKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_mKeyPressed

    private void B5_cmbkembungActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B5_cmbkembungActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B5_cmbkembungActionPerformed

    private void B5_cmbkembungKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B5_cmbkembungKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B5_cmbkembungKeyPressed

    private void B3_cmbhemipareseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B3_cmbhemipareseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_cmbhemipareseActionPerformed

    private void B3_cmbhemipareseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B3_cmbhemipareseKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B3_cmbhemipareseKeyPressed

    private void B6_cmbalatbantuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B6_cmbalatbantuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B6_cmbalatbantuActionPerformed

    private void B6_cmbalatbantuKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B6_cmbalatbantuKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B6_cmbalatbantuKeyPressed

    private void B5_cmbmuntahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B5_cmbmuntahActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B5_cmbmuntahActionPerformed

    private void B5_cmbmuntahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B5_cmbmuntahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B5_cmbmuntahKeyPressed

    private void B5_cmbdiareActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B5_cmbdiareActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B5_cmbdiareActionPerformed

    private void B5_cmbdiareKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B5_cmbdiareKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B5_cmbdiareKeyPressed

    private void B6_cmbfrakturActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B6_cmbfrakturActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_B6_cmbfrakturActionPerformed

    private void B6_cmbfrakturKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B6_cmbfrakturKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B6_cmbfrakturKeyPressed

    private void ElektrokardiografiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ElektrokardiografiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ElektrokardiografiKeyPressed

    private void LaboratoriumKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LaboratoriumKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_LaboratoriumKeyPressed

    private void obat_obatan_textKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_obat_obatan_textKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_obat_obatan_textKeyPressed

    private void RadiologiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RadiologiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RadiologiKeyPressed

    private void tab3_asa4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tab3_asa4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_asa4ActionPerformed

    private void penyulit_praanestesiMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_penyulit_praanestesiMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_penyulit_praanestesiMouseMoved

    private void penyulit_praanestesiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_penyulit_praanestesiMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_penyulit_praanestesiMouseExited

    private void penyulit_praanestesiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_penyulit_praanestesiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_penyulit_praanestesiKeyPressed

    private void tab3_darahMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_darahMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_darahMouseMoved

    private void tab3_darahMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_darahMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_darahMouseExited

    private void tab3_darahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_darahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_darahKeyPressed

    private void tab3_suhuMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_suhuMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_suhuMouseMoved

    private void tab3_suhuMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_suhuMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_suhuMouseExited

    private void tab3_suhuKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_suhuKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_suhuKeyPressed

    private void tab3_gcsMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_gcsMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_gcsMouseMoved

    private void tab3_gcsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_gcsMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_gcsMouseExited

    private void tab3_gcsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_gcsKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_gcsKeyPressed

    private void tab3_rhMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_rhMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_rhMouseMoved

    private void tab3_rhMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_rhMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_rhMouseExited

    private void tab3_rhKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_rhKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_rhKeyPressed

    private void tab3_hbMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_hbMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_hbMouseMoved

    private void tab3_hbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_hbMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_hbMouseExited

    private void tab3_hbKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_hbKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_hbKeyPressed

    private void tab3_vasMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_vasMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_vasMouseMoved

    private void tab3_vasMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_vasMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_vasMouseExited

    private void tab3_vasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_vasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_vasKeyPressed

    private void tab3_htMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_htMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_htMouseMoved

    private void tab3_htMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_htMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_htMouseExited

    private void tab3_htKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_htKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_htKeyPressed

    private void tab3_lainMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_lainMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_lainMouseMoved

    private void tab3_lainMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_lainMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_lainMouseExited

    private void tab3_lainKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_lainKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_lainKeyPressed

    private void tab3_moni4_ketMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_moni4_ketMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_moni4_ketMouseMoved

    private void tab3_moni4_ketMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_moni4_ketMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_moni4_ketMouseExited

    private void tab3_moni4_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_moni4_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_moni4_ketKeyPressed

    private void tab3_moni3_ketMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_moni3_ketMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_moni3_ketMouseMoved

    private void tab3_moni3_ketMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tab3_moni3_ketMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_moni3_ketMouseExited

    private void tab3_moni3_ketKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tab3_moni3_ketKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_moni3_ketKeyPressed

    private void tab3_alergi2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tab3_alergi2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tab3_alergi2ActionPerformed

    private void Scroll9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Scroll9KeyPressed
      getData3();
    }//GEN-LAST:event_Scroll9KeyPressed

    private void B1_wheezing_minMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_wheezing_minMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_wheezing_minMouseMoved

    private void B1_wheezing_minMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1_wheezing_minMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_wheezing_minMouseExited

    private void B1_wheezing_minKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B1_wheezing_minKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_B1_wheezing_minKeyPressed

    private void cmb_GAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_GAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_GAActionPerformed

    private void cmb_GAKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmb_GAKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_GAKeyPressed

    private void cmb_REGIONALActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_REGIONALActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_REGIONALActionPerformed

    private void cmb_REGIONALKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmb_REGIONALKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_REGIONALKeyPressed

    private void anestesi_blokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_anestesi_blokKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_anestesi_blokKeyPressed

    private void cmb_obatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_obatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_obatActionPerformed

    private void cmb_obatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmb_obatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_obatKeyPressed

    private void cmb_cairanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_cairanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_cairanActionPerformed

    private void cmb_cairanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmb_cairanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_cairanKeyPressed

    private void cairan_teksKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cairan_teksKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cairan_teksKeyPressed

    private void riwayat_alergi_textMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_riwayat_alergi_textMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_riwayat_alergi_textMouseMoved

    private void riwayat_alergi_textMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_riwayat_alergi_textMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_riwayat_alergi_textMouseExited

    private void riwayat_alergi_textKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_riwayat_alergi_textKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_riwayat_alergi_textKeyPressed

    private void riwayat_penyakit_textMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_riwayat_penyakit_textMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_riwayat_penyakit_textMouseMoved

    private void riwayat_penyakit_textMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_riwayat_penyakit_textMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_riwayat_penyakit_textMouseExited

    private void riwayat_penyakit_textKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_riwayat_penyakit_textKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_riwayat_penyakit_textKeyPressed

    private void BtnPetugas3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugas3ActionPerformed
       if(TNoRw.getText().equals("")&&TNoRM.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Pasien masih kosong...!!!");
        }else{
            caripreop.setNoRawat(TNoRw.getText());
            caripreop.tampil();
            caripreop.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            caripreop.setLocationRelativeTo(internalFrame1);
            caripreop.setVisible(true);
        }
    }//GEN-LAST:event_BtnPetugas3ActionPerformed

    private void BtnPetugas3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugas3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugas3KeyPressed

    private void BtnRadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRadActionPerformed
      
        if(TNoRw2.getText().equals("")&&TNoRM2.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Pasien masih kosong...!!!");
        }else{
            cariradiologi.setNoRawat(TNoRw2.getText());
            cariradiologi.tampil();
            cariradiologi.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            cariradiologi.setLocationRelativeTo(internalFrame1);
            cariradiologi.setVisible(true);
        }
    }//GEN-LAST:event_BtnRadActionPerformed

    private void BtnLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLabActionPerformed
        if(TNoRw2.getText().equals("")&&TNoRM2.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Pasien masih kosong...!!!");
        }else{
            carilaborat.setNoRawat(TNoRw2.getText());
            carilaborat.tampil();
            carilaborat.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            carilaborat.setLocationRelativeTo(internalFrame1);
            carilaborat.setVisible(true);
        }
    }//GEN-LAST:event_BtnLabActionPerformed

    private void alergi_teksMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_alergi_teksMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_alergi_teksMouseMoved

    private void alergi_teksMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_alergi_teksMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_alergi_teksMouseExited

    private void alergi_teksKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_alergi_teksKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_alergi_teksKeyPressed

    private void BtnCariLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariLabActionPerformed
        x=2;
        if(TNoRw.getText().equals("")&&TNoRM.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Pasien masih kosong...!!!");
        }else{
            carilaborat.setNoRawat(TNoRw.getText());
            carilaborat.tampil();
            carilaborat.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            carilaborat.setLocationRelativeTo(internalFrame1);
            carilaborat.setVisible(true);
        }
    }//GEN-LAST:event_BtnCariLabActionPerformed

    private void BtnCariLab1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariLab1ActionPerformed
        x=3;
        if(TNoRw.getText().equals("")&&TNoRM.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Pasien masih kosong...!!!");
        }else{
            carilaborat.setNoRawat(TNoRw.getText());
            carilaborat.tampil();
            carilaborat.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            carilaborat.setLocationRelativeTo(internalFrame1);
            carilaborat.setVisible(true);
        }
    }//GEN-LAST:event_BtnCariLab1ActionPerformed

    private void anestesi_teksMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_anestesi_teksMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_anestesi_teksMouseMoved

    private void anestesi_teksMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_anestesi_teksMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_anestesi_teksMouseExited

    private void anestesi_teksKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_anestesi_teksKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_anestesi_teksKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMOKAsuhanAnestesi dialog = new RMOKAsuhanAnestesi(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.CekBox A1;
    private widget.CekBox A2;
    private widget.CekBox A3;
    private widget.CekBox A4;
    private widget.TextArea Anamnesis;
    private widget.CekBox Asa;
    private widget.CekBox Asa1;
    private widget.CekBox Asa2;
    private widget.CekBox Asa3;
    private widget.CekBox Asa4;
    private widget.CekBox Asa5;
    private widget.CekBox Asa6;
    private widget.CekBox Asa7;
    private widget.CekBox B1;
    private widget.TextBox B1_alat;
    private widget.TextBox B1_rhonki;
    private widget.TextBox B1_rr;
    private widget.TextBox B1_vesikuler;
    private widget.TextBox B1_wheezing_min;
    private widget.TextBox B1_wheezing_plus;
    private widget.CekBox B2;
    private widget.ComboBox B2_cmb;
    private widget.ComboBox B2_cmbkonjungtiva;
    private widget.TextBox B2_hr;
    private widget.TextBox B2_td;
    private widget.CekBox B3;
    private widget.ComboBox B3_cmbhemiparese;
    private widget.ComboBox B3_cmbpupil;
    private widget.TextBox B3_e;
    private widget.TextBox B3_m;
    private widget.TextBox B3_v;
    private widget.CekBox B4;
    private widget.ComboBox B4_cmbwarnaurin;
    private widget.TextBox B4_urin;
    private widget.ComboBox B5_cmbdiare;
    private widget.ComboBox B5_cmbkembung;
    private widget.ComboBox B5_cmbmuntah;
    private widget.ComboBox B6_cmbalatbantu;
    private widget.ComboBox B6_cmbfraktur;
    private widget.Button BtnAll;
    private widget.Button BtnAll1;
    private widget.Button BtnAll2;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnCari1;
    private widget.Button BtnCari2;
    private widget.Button BtnCariLab;
    private widget.Button BtnCariLab1;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnLab;
    private widget.Button BtnPetugas;
    private widget.Button BtnPetugas1;
    private widget.Button BtnPetugas2;
    private widget.Button BtnPetugas3;
    private widget.Button BtnRad;
    private widget.Button BtnRefreshPhoto1;
    private widget.Button BtnRefreshPhoto2;
    private widget.Button BtnRefreshPhoto3;
    private widget.Button BtnSimpan;
    private widget.CekBox C1;
    private widget.CekBox C2;
    private widget.CekBox ChkAccor;
    private widget.CekBox ChkAccor1;
    private widget.CekBox ChkAccor2;
    private widget.CekBox ChkJam1;
    private widget.CekBox ChkJam2;
    private widget.CekBox ChkJam3;
    private widget.ComboBox CmbAlergi;
    private widget.ComboBox CmbAnestesi;
    private widget.ComboBox CmbAnestesi2;
    private widget.ComboBox CmbB1;
    private widget.ComboBox CmbMerokok;
    private widget.ComboBox CmbObat;
    private widget.ComboBox CmbPenyakit;
    private widget.ComboBox Cmbmonitoring;
    private widget.CekBox D1;
    private widget.CekBox D2;
    private widget.CekBox D4;
    private widget.CekBox D5;
    private widget.CekBox D6;
    private widget.CekBox D7;
    private widget.CekBox D8;
    private widget.CekBox D9;
    private widget.ComboBox Detik;
    private widget.ComboBox Detik1;
    private widget.ComboBox Detik2;
    private widget.TextArea Elektrokardiografi;
    private widget.PanelBiasa FormInput1;
    private widget.PanelBiasa FormInput2;
    private widget.PanelBiasa FormInput3;
    private widget.PanelBiasa FormPass3;
    private widget.PanelBiasa FormPass4;
    private widget.PanelBiasa FormPass5;
    private widget.PanelBiasa FormTelaah;
    private widget.PanelBiasa FormTelaah1;
    private widget.PanelBiasa FormTelaah2;
    private widget.ComboBox Jam;
    private widget.ComboBox Jam1;
    private widget.ComboBox Jam2;
    private widget.TextBox KdDokter3;
    private widget.TextBox KdPetugas;
    private widget.TextBox KdPetugas1;
    private widget.TextBox Komplikasi;
    private widget.Label LCount;
    private widget.Label LCount1;
    private widget.Label LCount2;
    private widget.TextArea Laboratorium;
    private widget.ComboBox Menit;
    private widget.ComboBox Menit1;
    private widget.ComboBox Menit2;
    private widget.TextBox NmDokter3;
    private widget.TextBox NmPetugas;
    private widget.TextBox NmPetugas1;
    private widget.PanelBiasa PanelAccor;
    private widget.PanelBiasa PanelAccor1;
    private widget.PanelBiasa PanelAccor2;
    private javax.swing.JPopupMenu Popup;
    private javax.swing.JPopupMenu Popup1;
    private javax.swing.JPopupMenu Popup2;
    private widget.TextArea Preop;
    private widget.TextArea Radiologi;
    private widget.TextArea Rencanaop;
    private widget.ScrollPane Scroll5;
    private widget.ScrollPane Scroll6;
    private widget.ScrollPane Scroll8;
    private widget.ScrollPane Scroll9;
    private widget.TextBox TCari;
    private widget.TextBox TCari1;
    private widget.TextBox TCari2;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRM2;
    private widget.TextBox TNoRM3;
    private widget.TextBox TNoRw;
    private widget.TextBox TNoRw2;
    private widget.TextBox TNoRw3;
    private widget.TextBox TPasien;
    private widget.TextBox TPasien2;
    private widget.TextBox TPasien3;
    private javax.swing.JTabbedPane TabData;
    private javax.swing.JTabbedPane TabData1;
    private javax.swing.JTabbedPane TabData2;
    private javax.swing.JTabbedPane TabOK;
    private widget.Tanggal Tanggal;
    private widget.Tanggal Tanggal1;
    private widget.Tanggal Tanggal2;
    private widget.TextBox alergi_teks;
    private widget.TextArea anestesi_blok;
    private widget.TextBox anestesi_teks;
    private widget.TextBox bb;
    private javax.swing.ButtonGroup buttonGroup1;
    private widget.TextArea cairan_teks;
    private widget.ComboBox cmb_GA;
    private widget.ComboBox cmb_REGIONAL;
    private widget.ComboBox cmb_cairan;
    private widget.ComboBox cmb_obat;
    private widget.TextBox diagnosa_ok;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame3;
    private widget.InternalFrame internalFrame4;
    private widget.InternalFrame internalFrame5;
    private widget.Label jLabel11;
    private widget.Label jLabel12;
    private widget.Label jLabel13;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator17;
    private javax.swing.JSeparator jSeparator18;
    private javax.swing.JSeparator jSeparator19;
    private javax.swing.JSeparator jSeparator20;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private widget.ComboBox jenis_anestesi;
    private widget.TextBox jenisanestesi_text;
    private widget.TextBox jk;
    private widget.TextBox jk1;
    private widget.TextBox jk2;
    private widget.Label label10;
    private widget.Label label105;
    private widget.Label label11;
    private widget.Label label12;
    private widget.Label label13;
    private widget.Label label14;
    private widget.Label label15;
    private widget.Label label152;
    private widget.Label label153;
    private widget.Label label154;
    private widget.Label label155;
    private widget.Label label156;
    private widget.Label label157;
    private widget.Label label158;
    private widget.Label label159;
    private widget.Label label16;
    private widget.Label label160;
    private widget.Label label161;
    private widget.Label label162;
    private widget.Label label163;
    private widget.Label label164;
    private widget.Label label165;
    private widget.Label label166;
    private widget.Label label167;
    private widget.Label label168;
    private widget.Label label169;
    private widget.Label label17;
    private widget.Label label170;
    private widget.Label label171;
    private widget.Label label172;
    private widget.Label label173;
    private widget.Label label174;
    private widget.Label label175;
    private widget.Label label176;
    private widget.Label label177;
    private widget.Label label178;
    private widget.Label label179;
    private widget.Label label18;
    private widget.Label label180;
    private widget.Label label181;
    private widget.Label label182;
    private widget.Label label183;
    private widget.Label label184;
    private widget.Label label185;
    private widget.Label label186;
    private widget.Label label187;
    private widget.Label label188;
    private widget.Label label189;
    private widget.Label label19;
    private widget.Label label190;
    private widget.Label label191;
    private widget.Label label192;
    private widget.Label label193;
    private widget.Label label194;
    private widget.Label label195;
    private widget.Label label196;
    private widget.Label label197;
    private widget.Label label198;
    private widget.Label label199;
    private widget.Label label20;
    private widget.Label label200;
    private widget.Label label201;
    private widget.Label label202;
    private widget.Label label203;
    private widget.Label label204;
    private widget.Label label205;
    private widget.Label label206;
    private widget.Label label207;
    private widget.Label label208;
    private widget.Label label209;
    private widget.Label label210;
    private widget.Label label211;
    private widget.Label label212;
    private widget.Label label213;
    private widget.Label label214;
    private widget.Label label215;
    private widget.Label label216;
    private widget.Label label217;
    private widget.Label label218;
    private widget.Label label219;
    private widget.Label label220;
    private widget.Label label221;
    private widget.Label label222;
    private widget.Label label223;
    private widget.Label label224;
    private widget.Label label225;
    private widget.Label label226;
    private widget.Label label227;
    private widget.Label label228;
    private widget.Label label229;
    private widget.Label label230;
    private widget.Label label231;
    private widget.Label label232;
    private widget.Label label233;
    private widget.Label label234;
    private widget.Label label235;
    private widget.Label label236;
    private widget.Label label237;
    private widget.Label label238;
    private widget.Label label239;
    private widget.Label label240;
    private widget.Label label241;
    private widget.Label label242;
    private widget.Label label243;
    private widget.Label label244;
    private widget.Label label245;
    private widget.Label label246;
    private widget.Label label247;
    private widget.Label label248;
    private widget.Label label249;
    private widget.Label label250;
    private widget.Label label251;
    private widget.Label label252;
    private widget.Label label253;
    private widget.Label label254;
    private widget.Label label255;
    private widget.Label label256;
    private widget.Label label257;
    private widget.Label label258;
    private widget.Label label259;
    private widget.Label label260;
    private widget.Label label261;
    private widget.Label label262;
    private widget.Label label263;
    private widget.Label label264;
    private widget.Label label265;
    private widget.Label label266;
    private widget.Label label267;
    private widget.Label label268;
    private widget.Label label269;
    private widget.Label label270;
    private widget.Label label271;
    private widget.Label label272;
    private widget.Label label273;
    private widget.Label label274;
    private widget.Label label275;
    private widget.Label label276;
    private widget.Label label277;
    private widget.Label label278;
    private widget.Label label280;
    private widget.Label label281;
    private widget.Label label282;
    private widget.Label label283;
    private widget.Label label284;
    private widget.Label label285;
    private widget.Label label286;
    private widget.Label label287;
    private widget.Label label288;
    private widget.Label label290;
    private widget.Label label35;
    private widget.Label label38;
    private widget.Label label39;
    private widget.Label label40;
    private widget.Label label41;
    private widget.Label label42;
    private widget.Label label43;
    private widget.Label label44;
    private widget.Label label45;
    private widget.Label label46;
    private widget.Label label47;
    private widget.Label label48;
    private widget.Label label49;
    private widget.Label label50;
    private widget.Label label51;
    private widget.Label label52;
    private widget.Label label53;
    private widget.Label label54;
    private widget.Label label55;
    private widget.Label label56;
    private widget.Label label57;
    private widget.Label label58;
    private widget.Label label59;
    private widget.Label label60;
    private widget.Label label68;
    private widget.Label label9;
    private widget.Label label_100;
    private widget.TextArea obat_obatan_text;
    private widget.TextBox obat_text;
    private widget.panelisi panelisi1;
    private widget.panelisi panelisi5;
    private widget.panelisi panelisi6;
    private widget.panelisi panelisi7;
    private widget.TextBox penyulit_praanestesi;
    private javax.swing.JMenuItem pp1;
    private javax.swing.JMenuItem pp2;
    private javax.swing.JMenuItem ppCetakLembarFormulir;
    private widget.CekBox rencana_hcu;
    private widget.CekBox rencana_icu;
    private widget.CekBox rencana_igd;
    private widget.CekBox rencana_inap;
    private widget.CekBox rencana_ralan;
    private widget.TextBox riwayat_alergi_text;
    private widget.TextBox riwayat_penyakit_text;
    private widget.ScrollPane scrollInput1;
    private widget.ScrollPane scrollInput2;
    private widget.ScrollPane scrollInput3;
    private widget.ScrollPane scrollPane1;
    private widget.ScrollPane scrollPane2;
    private widget.ScrollPane scrollPane3;
    private widget.ScrollPane scrollPane4;
    private widget.ScrollPane scrollPane5;
    private widget.ScrollPane scrollPane6;
    private widget.ScrollPane scrollPane7;
    private widget.ScrollPane scrollPane8;
    private widget.ScrollPane scrollPane9;
    private widget.CekBox tab3_abnormal;
    private widget.CekBox tab3_alergi1;
    private widget.CekBox tab3_alergi2;
    private widget.CekBox tab3_allo;
    private widget.CekBox tab3_asa1;
    private widget.CekBox tab3_asa2;
    private widget.CekBox tab3_asa3;
    private widget.CekBox tab3_asa4;
    private widget.CekBox tab3_asa5;
    private widget.CekBox tab3_asaE;
    private widget.CekBox tab3_auto;
    private widget.TextBox tab3_bb;
    private widget.CekBox tab3_bukamulut;
    private widget.CekBox tab3_cek1;
    private widget.CekBox tab3_cek2;
    private widget.CekBox tab3_cek3;
    private widget.CekBox tab3_cek4;
    private widget.CekBox tab3_cek5;
    private widget.TextBox tab3_darah;
    private widget.TextBox tab3_gcs;
    private widget.TextBox tab3_hb;
    private widget.TextBox tab3_ht;
    private widget.CekBox tab3_jarak;
    private widget.TextBox tab3_lain;
    private widget.CekBox tab3_leher;
    private widget.CekBox tab3_mallampati;
    private widget.CekBox tab3_moni1;
    private widget.CekBox tab3_moni2;
    private widget.CekBox tab3_moni3;
    private widget.TextBox tab3_moni3_ket;
    private widget.CekBox tab3_moni4;
    private widget.TextBox tab3_moni4_ket;
    private widget.CekBox tab3_moni5;
    private widget.CekBox tab3_moni6;
    private widget.CekBox tab3_moni7;
    private widget.CekBox tab3_moni8;
    private widget.CekBox tab3_moni9;
    private widget.TextBox tab3_nadi;
    private widget.CekBox tab3_normal;
    private widget.TextBox tab3_respirasi;
    private widget.TextBox tab3_rh;
    private widget.TextBox tab3_suhu;
    private widget.TextBox tab3_tb;
    private widget.TextBox tab3_td;
    private widget.CekBox tab3_tek1;
    private widget.CekBox tab3_tek10;
    private widget.CekBox tab3_tek11;
    private widget.CekBox tab3_tek2;
    private widget.CekBox tab3_tek3;
    private widget.CekBox tab3_tek4;
    private widget.CekBox tab3_tek5;
    private widget.CekBox tab3_tek6;
    private widget.CekBox tab3_tek7;
    private widget.CekBox tab3_tek8;
    private widget.CekBox tab3_tek9;
    private widget.TextBox tab3_vas;
    private widget.TextBox tb;
    private widget.Table tbChecklist;
    private widget.Table tbMasalahKeperawatan;
    private widget.Table tbPrainduksi;
    private widget.Table tbPrasedasi;
    private widget.TextBox tindakan_ok;
    // End of variables declaration//GEN-END:variables

    private void tampil() {
        Valid.tabelKosong(tabModeAnestesi);
        try{
            ps=koneksi.prepareStatement(
                   "select penilaian_awal_anestesi_checklist.*,pasien.nm_pasien,pasien.jk,pasien.no_rkm_medis,petugas.nip,petugas.nama "+
                   "from penilaian_awal_anestesi_checklist "+
                   "left join reg_periksa on reg_periksa.no_rawat = penilaian_awal_anestesi_checklist.no_rawat "+
                   "left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                   "left join petugas on penilaian_awal_anestesi_checklist.kd_petugas = petugas.nip "+
                   "where penilaian_awal_anestesi_checklist.no_rawat like ? order by penilaian_awal_anestesi_checklist.no_rawat");
            try {
                ps.setString(1,"%"+TCari.getText().trim()+"%");
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeAnestesi.addRow(new Object[]{
                        rs.getString("no_rawat"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("nip"),
                        rs.getString("nama"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("jk"),
                        rs.getString("tindakan_ok"),
                        rs.getString("diagnosa_ok"),
                        rs.getString("jenis_anestesi"),
                        rs.getString("jenisanestesi_text"),
                        rs.getString("airway_1"),
                        rs.getString("airway_2"),
                        rs.getString("airway_3"),
                        rs.getString("airway_4"),
                        rs.getString("breathing_1"),
                        rs.getString("breathing_2"),
                        rs.getString("breathing_3"),
                        rs.getString("breathing_4"),
                        rs.getString("obat_1"),
                        rs.getString("obat_2"),
                        rs.getString("obat_3"),
                        rs.getString("obat_4"),
                        rs.getString("obat_5"),
                        rs.getString("obat_6"),
                        rs.getString("obat_7"),
                        rs.getString("obat_8"),
                        rs.getString("obat_9"),
                        rs.getString("obat_10"),
                    });
                }
            } catch (Exception e) {
                System.out.println("Notifikasi : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            LCount.setText(""+tabModeAnestesi.getRowCount());
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
     
    private void tampilprasedasi() {
        Valid.tabelKosong(tabModeInOp);
        try{
            ps=koneksi.prepareStatement("select penilaian_awal_anestesi_prasedasi.*,dokter.kd_dokter,dokter.nm_dokter,pasien.nm_pasien,pasien.jk,pasien.no_rkm_medis "+
                   "from penilaian_awal_anestesi_prasedasi "+
                   "left join reg_periksa on reg_periksa.no_rawat = penilaian_awal_anestesi_prasedasi.no_rawat "+
                   "left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                   "left join dokter on penilaian_awal_anestesi_prasedasi.kd_dokter = dokter.kd_dokter "+
                   "where penilaian_awal_anestesi_prasedasi.no_rawat = '"+TNoRw.getText()+"' order by penilaian_awal_anestesi_prasedasi.no_rawat");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeInOp.addRow(new Object[]{
                        rs.getString("no_rawat"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("dokter.kd_dokter"),
                        rs.getString("dokter.nm_dokter"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("jk"),
                        rs.getString("anamnesis"),
                        rs.getString("diagnosa_preop"),
                        rs.getString("rencana_operasi"),
                        rs.getString("tb"),
                        rs.getString("bb"),
                        rs.getString("obat_dikonsumsi"),
                        rs.getString("obat_dikonsumsi_ket"),
                        rs.getString("riwayat_alergi"),
                        rs.getString("riwayat_alergi_ket"),
                        rs.getString("riwayat_penyakit"),
                        rs.getString("riwayat_penyakit_ket"),
                        rs.getString("riwayat_anestesi"),
                        rs.getString("jenis_anestesi"),
                        rs.getString("riwayat_merokok"),
                        rs.getString("komplikasi_anestesi"),
                        rs.getString("fisik_b1"),
                        rs.getString("fisik_alat"),
                        rs.getString("fisik_rr"),
                        rs.getString("fisik_vesikuler"),
                        rs.getString("fisik_rhonki"),
                        rs.getString("fisik_wheezing_plus"),
                        rs.getString("fisik_wheezing_min"),
                        rs.getString("fisik_td"),
                        rs.getString("fisik_hr"),
                        rs.getString("fisik_hr_ket"),
                        rs.getString("fisik_konjungtiva"),
                        rs.getString("fisik_gcse"),
                        rs.getString("fisik_gcsm"),
                        rs.getString("fisik_gcsv"),
                        rs.getString("fisik_pupil"),
                        rs.getString("fisik_hemiparese"),
                        rs.getString("fisik_urin"),
                        rs.getString("fisik_warnaurin"),
                        rs.getString("fisik_perut"),
                        rs.getString("fisik_diare"),
                        rs.getString("fisik_muntah"),
                        rs.getString("fisik_alatbantu"),
                        rs.getString("fisik_fraktur"),
                        rs.getString("penunjang_lab"),
                        rs.getString("penunjang_rad"),
                        rs.getString("penunjang_elektro"),
                        rs.getString("asa"),
                        rs.getString("asa1"),
                        rs.getString("asa2"),
                        rs.getString("asa3"),
                        rs.getString("asa4"),
                        rs.getString("asa5"),
                        rs.getString("asa6"),
                        rs.getString("asaE"),
                        rs.getString("rencana_ga"),
                        rs.getString("rencana_reg"),
                        rs.getString("rencana_blok"),
                        rs.getString("obat_obatan"),
                        rs.getString("obat_obatan_ket"),
                        rs.getString("cairan"),
                        rs.getString("cairan_ket"),
                        rs.getString("monitoring_khusus"),
                        rs.getString("rencana_perawatan_inap"),
                        rs.getString("rencana_hcu"),
                        rs.getString("rencana_icu"),
                        rs.getString("rencana_rajal"),
                        rs.getString("rencana_igd")

                    });
                }
            } catch (Exception e) {
                System.out.println("Notifikasi : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
    
    private void tampilprainduksi() {
        Valid.tabelKosong(tabModePraInduksi);
        try{
            ps=koneksi.prepareStatement(
                   "select penilaian_awal_anestesi_prainduksi.*,dokter.nm_dokter,dokter.kd_dokter,pasien.nm_pasien,pasien.jk,pasien.no_rkm_medis "+
                   "from penilaian_awal_anestesi_prainduksi "+
                   "left join reg_periksa on reg_periksa.no_rawat = penilaian_awal_anestesi_prainduksi.no_rawat "+
                   "left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                   "left join dokter on penilaian_awal_anestesi_prainduksi.kd_dokter = dokter.kd_dokter "+
                   "where penilaian_awal_anestesi_prainduksi.no_rawat = '"+TNoRw3.getText()+"' order by penilaian_awal_anestesi_prainduksi.no_rawat");
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModePraInduksi.addRow(new Object[]{
                        rs.getString("no_rawat"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("dokter.kd_dokter"),
                        rs.getString("dokter.nm_dokter"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("alergi"),
                        rs.getString("alergi_ket"),
                        rs.getString("bb"),
                        rs.getString("td"),
                        rs.getString("nadi"),
                        rs.getString("tb"),
                        rs.getString("respirasi"),
                        rs.getString("gol_darah"),
                        rs.getString("suhu"),
                        rs.getString("gcs"),
                        rs.getString("rh"),
                        rs.getString("hb"),
                        rs.getString("vas"),
                        rs.getString("ht"),
                        rs.getString("lain"),
                        rs.getString("fisik_normal"),
                        rs.getString("fisik_bukamulut"),
                        rs.getString("fisik_jarak"),
                        rs.getString("fisik_mallampati"),
                        rs.getString("fisik_gerakanleher"),
                        rs.getString("fisik_abnormal"),
                        rs.getString("anamnesis_auto"),
                        rs.getString("anamnesis_allo"),
                        rs.getString("asa1"),
                        rs.getString("asa2"),
                        rs.getString("asa3"),
                        rs.getString("asa4"),
                        rs.getString("asa5"),
                        rs.getString("asaE"),
                        rs.getString("penyulit_praanestesi"),
                        rs.getString("cek_1"),
                        rs.getString("cek_2"),
                        rs.getString("cek_3"),
                        rs.getString("cek_4"),
                        rs.getString("cek_5"),
                        rs.getString("ga_1"),
                        rs.getString("ga_2"),
                        rs.getString("ga_3"),
                        rs.getString("ga_4"),
                        rs.getString("ga_5"),
                        rs.getString("reg_1"),
                        rs.getString("reg_2"),
                        rs.getString("reg_3"),
                        rs.getString("reg_4"),
                        rs.getString("reg_5"),
                        rs.getString("anestesi_lain"),
                        rs.getString("anestesi_lain_ket"),
                        rs.getString("monitoring_1"),
                        rs.getString("monitoring_2"),
                        rs.getString("monitoring_3"),
                        rs.getString("monitoring_3_ket"),
                        rs.getString("monitoring_4"),
                        rs.getString("monitoring_4_ket"),
                        rs.getString("monitoring_5"),
                        rs.getString("monitoring_6"),
                        rs.getString("monitoring_7"),
                        rs.getString("monitoring_8"),
                        rs.getString("monitoring_9"),


                    });
                  
                }
            } catch (Exception e) {
                System.out.println("Notifikasi : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }

    
    public void setNoRm(String NoRawat){
        TNoRw.setText(NoRawat);
        TCari.setText(NoRawat);
        TNoRw2.setText(NoRawat);
        TCari1.setText(NoRawat);
        TNoRw3.setText(NoRawat);
        TCari2.setText(NoRawat);
        tampil();
        tampilprasedasi();
        tampilprainduksi();
        String no_rm_pasien = Sequel.cariIsi("select reg_periksa.no_rkm_medis from reg_periksa where reg_periksa.no_rawat='"+TNoRw.getText()+"'");
        String nama_pasien = Sequel.cariIsi("select pasien.nm_pasien from pasien where pasien.no_rkm_medis='"+no_rm_pasien+"'");
        String jk_pasien = Sequel.cariIsi("select pasien.jk from pasien where pasien.no_rkm_medis='"+no_rm_pasien+"'");
        TNoRM.setText(no_rm_pasien);
        TNoRM2.setText(no_rm_pasien);
        TNoRM3.setText(no_rm_pasien);
        TPasien.setText(nama_pasien);
        TPasien2.setText(nama_pasien);
        TPasien3.setText(nama_pasien);
        
        jk.setText(jk_pasien);
        jk1.setText(jk_pasien);
        jk2.setText(jk_pasien);
        
        ChkJam1.setSelected(true);
        ChkJam2.setSelected(true);
        ChkJam3.setSelected(true);
       
        
    }
    public void setNoRm2(String NoRawat,String NoRkmMedis,String Nama,String Tanggal){
        tampil();
        tampilprasedasi();
        tampilprainduksi();
    }
    private void getData() {
        int row=tbChecklist.getSelectedRow();
        int selectedRow = tbChecklist.getSelectedRow();
       
        if(row!= -1){
            ChkJam1.setSelected(false);
            TNoRw.setText(tbChecklist.getValueAt(row,0).toString());
            Valid.SetTgl2(Tanggal,tbChecklist.getValueAt(tbChecklist.getSelectedRow(),1).toString());
            Jam.setSelectedItem(tbChecklist.getValueAt(tbChecklist.getSelectedRow(),2).toString().substring(0,2));
            Menit.setSelectedItem(tbChecklist.getValueAt(tbChecklist.getSelectedRow(),2).toString().substring(3,5));
            Detik.setSelectedItem(tbChecklist.getValueAt(tbChecklist.getSelectedRow(),2).toString().substring(6,8));
            KdPetugas.setText(tbChecklist.getValueAt(row,3).toString());
            NmPetugas.setText(tbChecklist.getValueAt(row,4).toString());
            TNoRM.setText(tbChecklist.getValueAt(row,5).toString());
            TPasien.setText(tbChecklist.getValueAt(row,6).toString());
            jk.setText(tbChecklist.getValueAt(row,7).toString()); 
            tindakan_ok.setText(tbChecklist.getValueAt(row,8).toString());
            diagnosa_ok.setText(tbChecklist.getValueAt(row,9).toString());
            jenis_anestesi.setSelectedItem(tbChecklist.getValueAt(row,10).toString());
            jenisanestesi_text.setText(tbChecklist.getValueAt(row,11).toString());
            A1.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 12))));
            A2.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 13))));
            A3.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 14))));
            A4.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 15))));
            B1.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 16))));
            B2.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 17))));
            B3.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 18))));
            B4.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 19))));
            C1.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 20))));
            C2.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 21))));
            D1.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 22))));
            D2.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 23))));
            D4.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 24))));
            D5.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 25))));
            D6.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 26))));
            D7.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 27))));
            D8.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 28))));
            D9.setSelected("true".equals(String.valueOf(tbChecklist.getValueAt(selectedRow, 29))));
          
           
        }
    }
    
  private void getData2() {
        int row=tbPrasedasi.getSelectedRow();
        int selectedRow = tbPrasedasi.getSelectedRow();
        if(row!= -1){
            ChkJam2.setSelected(false);
            TNoRw2.setText(tbPrasedasi.getValueAt(row,0).toString());
            Valid.SetTgl2(Tanggal1,tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),1).toString());
            Jam1.setSelectedItem(tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString().substring(0,2));
            Menit1.setSelectedItem(tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString().substring(3,5));
            Detik1.setSelectedItem(tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString().substring(6,8));
            KdPetugas1.setText(tbPrasedasi.getValueAt(row,3).toString());
            NmPetugas1.setText(tbPrasedasi.getValueAt(row,4).toString());
            TNoRM2.setText(tbPrasedasi.getValueAt(row,5).toString());
            TPasien2.setText(tbPrasedasi.getValueAt(row,6).toString());
            jk1.setText(tbPrasedasi.getValueAt(row,7).toString()); 
            Anamnesis.setText(tbPrasedasi.getValueAt(row,8).toString()); 
            Preop.setText(tbPrasedasi.getValueAt(row,9).toString()); 
            Rencanaop.setText(tbPrasedasi.getValueAt(row,10).toString()); 
            tb.setText(tbPrasedasi.getValueAt(row,11).toString());
            bb.setText(tbPrasedasi.getValueAt(row,12).toString());
            CmbObat.setSelectedItem(tbPrasedasi.getValueAt(row,13).toString());
            obat_text.setText(tbPrasedasi.getValueAt(row,14).toString());
            CmbAlergi.setSelectedItem(tbPrasedasi.getValueAt(row,15).toString());
            riwayat_alergi_text.setText(tbPrasedasi.getValueAt(row,16).toString());
            CmbPenyakit.setSelectedItem(tbPrasedasi.getValueAt(row,17).toString());
            riwayat_penyakit_text.setText(tbPrasedasi.getValueAt(row,18).toString());
            CmbAnestesi.setSelectedItem(tbPrasedasi.getValueAt(row,19).toString());
            CmbAnestesi2.setSelectedItem(tbPrasedasi.getValueAt(row,20).toString());
            CmbMerokok.setSelectedItem(tbPrasedasi.getValueAt(row,21).toString());
            Komplikasi.setText(tbPrasedasi.getValueAt(row,22).toString());
            CmbB1.setSelectedItem(tbPrasedasi.getValueAt(row,23).toString());
            B1_alat.setText(tbPrasedasi.getValueAt(row,24).toString());
            B1_rr.setText(tbPrasedasi.getValueAt(row,25).toString());
            B1_vesikuler.setText(tbPrasedasi.getValueAt(row,26).toString());
            B1_rhonki.setText(tbPrasedasi.getValueAt(row,27).toString());
            B1_wheezing_plus.setText(tbPrasedasi.getValueAt(row,28).toString());
            B1_wheezing_min.setText(tbPrasedasi.getValueAt(row,29).toString());
            B2_td.setText(tbPrasedasi.getValueAt(row,30).toString());
            B2_hr.setText(tbPrasedasi.getValueAt(row,31).toString());
            B2_cmb.setSelectedItem(tbPrasedasi.getValueAt(row,32).toString());
            B2_cmbkonjungtiva.setSelectedItem(tbPrasedasi.getValueAt(row,33).toString());
            B3_e.setText(tbPrasedasi.getValueAt(row,34).toString());
            B3_m.setText(tbPrasedasi.getValueAt(row,35).toString());
            B3_v.setText(tbPrasedasi.getValueAt(row,36).toString());
            B3_cmbpupil.setSelectedItem(tbPrasedasi.getValueAt(row,37).toString());
            B3_cmbhemiparese.setSelectedItem(tbPrasedasi.getValueAt(row,38).toString());
            B4_urin.setText(tbPrasedasi.getValueAt(row,39).toString());
            B4_cmbwarnaurin.setSelectedItem(tbPrasedasi.getValueAt(row,40).toString());
            B5_cmbkembung.setSelectedItem(tbPrasedasi.getValueAt(row,41).toString());
            B5_cmbdiare.setSelectedItem(tbPrasedasi.getValueAt(row,42).toString());
            B5_cmbmuntah.setSelectedItem(tbPrasedasi.getValueAt(row,43).toString());
            B6_cmbalatbantu.setSelectedItem(tbPrasedasi.getValueAt(row,44).toString());
            B6_cmbfraktur.setSelectedItem(tbPrasedasi.getValueAt(row,45).toString());
            Laboratorium.setText(tbPrasedasi.getValueAt(row,46).toString());
            Radiologi.setText(tbPrasedasi.getValueAt(row,47).toString());
            Elektrokardiografi.setText(tbPrasedasi.getValueAt(row,48).toString());
            Asa.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 49))));
            Asa1.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 50))));
            Asa2.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 51))));
            Asa3.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 52))));
            Asa4.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 53))));
            Asa5.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 54))));
            Asa6.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 55))));
            Asa7.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 56))));
            cmb_GA.setSelectedItem(tbPrasedasi.getValueAt(row,57).toString());
            cmb_REGIONAL.setSelectedItem(tbPrasedasi.getValueAt(row,58).toString());
            anestesi_blok.setText(tbPrasedasi.getValueAt(row,59).toString());
            cmb_obat.setSelectedItem(tbPrasedasi.getValueAt(row,60).toString());
            obat_obatan_text.setText(tbPrasedasi.getValueAt(row,61).toString());
            cmb_cairan.setSelectedItem(tbPrasedasi.getValueAt(row,62).toString());
            cairan_teks.setText(tbPrasedasi.getValueAt(row,63).toString());
            Cmbmonitoring.setSelectedItem(tbPrasedasi.getValueAt(row,64).toString());
            rencana_inap.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 65))));
            rencana_hcu.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 66))));
            rencana_icu.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 67))));
            rencana_ralan.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 68))));
            rencana_igd.setSelected("true".equals(String.valueOf(tbPrasedasi.getValueAt(selectedRow, 69))));
        }
  }
  
  
  private void getData3() {
        int row=tbPrainduksi.getSelectedRow();
        int selectedRow = tbPrainduksi.getSelectedRow();
        if(row!= -1){
            ChkJam3.setSelected(false);
            TNoRw3.setText(tbPrainduksi.getValueAt(row,0).toString());
            Valid.SetTgl2(Tanggal2,tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),1).toString());
            Jam2.setSelectedItem(tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),2).toString().substring(0,2));
            Menit2.setSelectedItem(tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),2).toString().substring(3,5));
            Detik2.setSelectedItem(tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),2).toString().substring(6,8));
            KdDokter3.setText(tbPrainduksi.getValueAt(row,3).toString());
            NmDokter3.setText(tbPrainduksi.getValueAt(row,4).toString());
            TNoRM3.setText(tbPrainduksi.getValueAt(row,5).toString());
            TPasien3.setText(tbPrainduksi.getValueAt(row,6).toString());
            tab3_alergi1.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 7))));
            tab3_alergi2.setSelected("false".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 7))));
            alergi_teks.setText(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 8)));
            tab3_bb.setText(tbPrainduksi.getValueAt(row,9).toString());
            tab3_td.setText(tbPrainduksi.getValueAt(row,10).toString());
            tab3_nadi.setText(tbPrainduksi.getValueAt(row,11).toString());
            tab3_tb.setText(tbPrainduksi.getValueAt(row,12).toString());
            tab3_respirasi.setText(tbPrainduksi.getValueAt(row,13).toString());
            tab3_darah.setText(tbPrainduksi.getValueAt(row,14).toString());
            tab3_suhu.setText(tbPrainduksi.getValueAt(row,15).toString());
            tab3_gcs.setText(tbPrainduksi.getValueAt(row,16).toString());
            tab3_rh.setText(tbPrainduksi.getValueAt(row,17).toString());
            tab3_hb.setText(tbPrainduksi.getValueAt(row,18).toString());
            tab3_vas.setText(tbPrainduksi.getValueAt(row,19).toString());
            tab3_ht.setText(tbPrainduksi.getValueAt(row,20).toString());
            tab3_lain.setText(tbPrainduksi.getValueAt(row,21).toString());
            tab3_normal.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 22))));
            tab3_bukamulut.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 23))));
            tab3_jarak.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 24))));
            tab3_mallampati.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 25))));
            tab3_leher.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 26))));
            tab3_abnormal.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 27))));
            tab3_auto.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 28))));
            tab3_allo.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 29))));
            tab3_asa1.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 30))));
            tab3_asa2.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 31))));
            tab3_asa3.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 32))));
            tab3_asa4.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 33))));
            tab3_asa5.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 34))));
            tab3_asaE.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 35))));
            penyulit_praanestesi.setText(tbPrainduksi.getValueAt(row,36).toString());
            tab3_cek1.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 37))));
            tab3_cek2.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 38))));
            tab3_cek3.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 39))));
            tab3_cek4.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 40))));
            tab3_cek5.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 41))));
            
            tab3_tek1.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 42))));
            tab3_tek2.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 43))));
            tab3_tek3.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 44))));
            tab3_tek4.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 45))));
            tab3_tek5.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 46))));
            tab3_tek6.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 47))));
            tab3_tek7.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 48))));
            tab3_tek8.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 49))));
            tab3_tek9.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 50))));
            tab3_tek10.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 51))));
            tab3_tek11.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 52))));
            anestesi_teks.setText(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 53)));
            tab3_moni1.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 54))));
            tab3_moni2.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 55))));
            tab3_moni3.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 56))));
            tab3_moni3_ket.setText(tbPrainduksi.getValueAt(selectedRow, 57).toString());
            tab3_moni4.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 58))));
            tab3_moni4_ket.setText(tbPrainduksi.getValueAt(selectedRow, 59).toString());
            tab3_moni5.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 60))));
            tab3_moni6.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 61))));
            tab3_moni7.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 62))));
            tab3_moni8.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 63))));
            tab3_moni9.setSelected("true".equals(String.valueOf(tbPrainduksi.getValueAt(selectedRow, 64)))); 
        }
  }

    
    public void emptTeks() {
        if(TabOK.getSelectedIndex()==0){
            ChkJam1.setSelected(true);
            KdPetugas.setText("");
            NmPetugas.setText("");
            KdPetugas.setText("");
            NmPetugas.setText("");
            jk.setText(""); 
            tindakan_ok.setText("");
            diagnosa_ok.setText("");
            jenis_anestesi.setSelectedIndex(0);
            jenisanestesi_text.setText("");
            A1.setSelected(false);
            A2.setSelected(false);
            A3.setSelected(false);
            A4.setSelected(false);
            B1.setSelected(false);
            B2.setSelected(false);
            B3.setSelected(false);
            B4.setSelected(false);
            C1.setSelected(false);
            C2.setSelected(false);
            D1.setSelected(false);
            D2.setSelected(false);
            D4.setSelected(false);
            D5.setSelected(false);
            D6.setSelected(false);
            D7.setSelected(false);
            D8.setSelected(false);
            D9.setSelected(false);
        }else if(TabOK.getSelectedIndex()==1){
                        ChkJam2.setSelected(true);
                        KdPetugas1.setText("");
                        NmPetugas1.setText("");
                        Anamnesis.setText("");
                        Preop.setText("");
                        Rencanaop.setText("");
                        tb.setText("");
                        bb.setText("");
                        CmbObat.setSelectedIndex(0);
                        obat_text.setText("");
                        CmbAlergi.setSelectedIndex(0);
                        riwayat_alergi_text.setText("");
                        CmbPenyakit.setSelectedIndex(0);
                        riwayat_penyakit_text.setText("");
                        CmbAnestesi.setSelectedIndex(0);
                        CmbAnestesi2.setSelectedIndex(0);
                        CmbMerokok.setSelectedIndex(0);
                        Komplikasi.setText("");
                        CmbB1.setSelectedIndex(0);
                        B1_alat.setText("");
                        B1_rr.setText("");
                        B1_vesikuler.setText("");
                        B1_rhonki.setText("");
                        B1_wheezing_plus.setText("");
                        B1_wheezing_min.setText("");
                        B2_td.setText("");
                        B2_hr.setText("");
                        B2_cmb.setSelectedIndex(0);
                        B2_cmbkonjungtiva.setSelectedIndex(0);
                        B3_e.setText("");
                        B3_m.setText("");
                        B3_v.setText("");
                        B3_cmbpupil.setSelectedIndex(0);
                        B3_cmbhemiparese.setSelectedIndex(0);
                        B4_urin.setText("");
                        B4_cmbwarnaurin.setSelectedIndex(0);
                        B5_cmbkembung.setSelectedIndex(0);
                        B5_cmbdiare.setSelectedIndex(0);
                        B5_cmbmuntah.setSelectedIndex(0);
                        B6_cmbalatbantu.setSelectedIndex(0);
                        B6_cmbfraktur.setSelectedIndex(0);
                        Radiologi.setText("");
                        Elektrokardiografi.setText("");
                        Asa.setSelected(false);
                        Asa1.setSelected(false);
                        Asa2.setSelected(false);
                        Asa3.setSelected(false);
                        Asa4.setSelected(false);
                        Asa5.setSelected(false);
                        Asa6.setSelected(false);
                        Asa7.setSelected(false);
                        cmb_GA.setSelectedIndex(0);
                        cmb_REGIONAL.setSelectedIndex(0);
                        anestesi_blok.setText("");
                        cmb_obat.setSelectedIndex(0);
                        obat_obatan_text.setText("");
                        cmb_cairan.setSelectedIndex(0);
                        cairan_teks.setText("");
                        Cmbmonitoring.setSelectedIndex(0);
                        rencana_inap.setSelected(false);
                        rencana_hcu.setSelected(false);
                        rencana_icu.setSelected(false);
                        rencana_ralan.setSelected(false);
                        rencana_igd.setSelected(false);          
        }else if(TabOK.getSelectedIndex()==2){
                        ChkJam3.setSelected(true);
                        KdDokter3.setText("");
                        NmDokter3.setText("");
                        tab3_alergi1.setSelected(false);
                        tab3_bb.setText("");
                        tab3_td.setText("");
                        tab3_nadi.setText("");
                        tab3_tb.setText("");
                        tab3_respirasi.setText("");
                        tab3_darah.setText("");
                        tab3_suhu.setText("");
                        tab3_gcs.setText("");
                        tab3_rh.setText("");
                        tab3_hb.setText("");
                        tab3_vas.setText("");
                        tab3_ht.setText("");
                        tab3_lain.setText("");
                        tab3_normal.setSelected(false);
                        tab3_bukamulut.setSelected(false);
                        tab3_jarak.setSelected(false);
                        tab3_mallampati.setSelected(false);
                        tab3_leher.setSelected(false);
                        tab3_abnormal.setSelected(false);
                        tab3_auto.setSelected(false);
                        tab3_allo.setSelected(false);
                        tab3_asa1.setSelected(false);
                        tab3_asa2.setSelected(false);
                        tab3_asa3.setSelected(false);
                        tab3_asa4.setSelected(false);
                        tab3_asa5.setSelected(false);
                        tab3_asaE.setSelected(false);
                        penyulit_praanestesi.setText("");
                        tab3_cek1.setSelected(false);
                        tab3_cek2.setSelected(false);
                        tab3_cek3.setSelected(false);
                        tab3_cek4.setSelected(false);
                        tab3_cek5.setSelected(false);
                        tab3_tek1.setSelected(false);
                        tab3_tek2.setSelected(false);
                        tab3_tek3.setSelected(false);
                        tab3_tek4.setSelected(false);
                        tab3_tek5.setSelected(false);
                        tab3_tek6.setSelected(false);
                        tab3_tek7.setSelected(false);
                        tab3_tek8.setSelected(false);
                        tab3_tek9.setSelected(false);
                        tab3_tek10.setSelected(false);
                        tab3_tek11.setSelected(false);
                        tab3_moni1.setSelected(false);
                        tab3_moni2.setSelected(false);
                        tab3_moni3.setSelected(false);
                        tab3_moni3_ket.setText("");
                        tab3_moni4.setSelected(false);
                        tab3_moni4_ket.setText("");
                        tab3_moni5.setSelected(false);
                        tab3_moni6.setSelected(false);
                        tab3_moni7.setSelected(false);
                        tab3_moni8.setSelected(false);
                        tab3_moni9.setSelected(false);
                        anestesi_teks.setText("");
                        alergi_teks.setText("");
                  
                     
        }else if(TabOK.getSelectedIndex()==3){
                       
                        
           
        }            
    }
    
    private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam1.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkJam1.isSelected()==false){
                    nilai_jam =Jam.getSelectedIndex();
                    nilai_menit =Menit.getSelectedIndex();
                    nilai_detik =Detik.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam.setSelectedItem(jam);
                Menit.setSelectedItem(menit);
                Detik.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    private void jam1(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam2;
            private int nilai_menit2;
            private int nilai_detik2;
            public void actionPerformed(ActionEvent e) {
                String nol_jam2 = "";
                String nol_menit2 = "";
                String nol_detik2 = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam2.isSelected()==true){
                    nilai_jam2 = now.getHours();
                    nilai_menit2 = now.getMinutes();
                    nilai_detik2 = now.getSeconds();
                }else if(ChkJam2.isSelected()==false){
                    nilai_jam2 =Jam1.getSelectedIndex();
                    nilai_menit2 =Menit1.getSelectedIndex();
                    nilai_detik2 =Detik1.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam2 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam2 = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit2 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit2 = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik2 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik2 = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam2 = nol_jam2 + Integer.toString(nilai_jam2);
                String menit2 = nol_menit2 + Integer.toString(nilai_menit2);
                String detik2 = nol_detik2 + Integer.toString(nilai_detik2);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam1.setSelectedItem(jam2);
                Menit1.setSelectedItem(menit2);
                Detik1.setSelectedItem(detik2);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
    private void jam2(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam3;
            private int nilai_menit3;
            private int nilai_detik3;
            public void actionPerformed(ActionEvent e) {
                String nol_jam3 = "";
                String nol_menit3 = "";
                String nol_detik3 = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam3.isSelected()==true){
                    nilai_jam3 = now.getHours();
                    nilai_menit3 = now.getMinutes();
                    nilai_detik3 = now.getSeconds();
                }else if(ChkJam3.isSelected()==false){
                    nilai_jam3 =Jam2.getSelectedIndex();
                    nilai_menit3 =Menit2.getSelectedIndex();
                    nilai_detik3 =Detik2.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam3 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam3 = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit3 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit3 = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik3 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik3 = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam3 = nol_jam3 + Integer.toString(nilai_jam3);
                String menit3 = nol_menit3 + Integer.toString(nilai_menit3);
                String detik3 = nol_detik3 + Integer.toString(nilai_detik3);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam2.setSelectedItem(jam3);
                Menit2.setSelectedItem(menit3);
                Detik2.setSelectedItem(detik3);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
    private void isPhoto(){
        if(ChkAccor.isSelected()==true){
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(internalFrame1.getWidth()-650,HEIGHT));
            TabData.setVisible(true);  
            ChkAccor.setVisible(true);
        }else if(ChkAccor.isSelected()==false){    
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(15,HEIGHT));
            TabData.setVisible(false);  
            ChkAccor.setVisible(true);
        }
    }
    private void isPhoto1(){
        if(ChkAccor1.isSelected()==true){
            ChkAccor1.setVisible(false);
            PanelAccor1.setPreferredSize(new Dimension(internalFrame1.getWidth()-650,HEIGHT));
            TabData1.setVisible(true);  
            ChkAccor1.setVisible(true);
        }else if(ChkAccor1.isSelected()==false){    
            ChkAccor1.setVisible(false);
            PanelAccor1.setPreferredSize(new Dimension(15,HEIGHT));
            TabData1.setVisible(false);  
            ChkAccor1.setVisible(true);
        }
    }
    private void isPhoto2(){
        if(ChkAccor2.isSelected()==true){
            ChkAccor2.setVisible(false);
            PanelAccor2.setPreferredSize(new Dimension(internalFrame1.getWidth()-650,HEIGHT));
            TabData2.setVisible(true);  
            ChkAccor2.setVisible(true);
        }else if(ChkAccor2.isSelected()==false){    
            ChkAccor2.setVisible(false);
            PanelAccor2.setPreferredSize(new Dimension(15,HEIGHT));
            TabData2.setVisible(false);  
            ChkAccor2.setVisible(true);
        }
    }
    private void getdataprainduksi(){
         try {
            ps = koneksi.prepareStatement(
                    "SELECT pemeriksaan_ranap.* from pemeriksaan_ranap where pemeriksaan_ranap.no_rawat = ?  order by tgl_perawatan desc, jam_rawat DESC LIMIT 1");

            try {
                ps.setString(1, TNoRw.getText());  // Mengambil no_rawat dari komponen GUI

                rs = ps.executeQuery();
                if (rs.next()) {
                    // Memastikan bahwa kolom-kolom ini ada dalam query dan mengisi nilai ke GUI dengan benar
                    tab3_bb.setText(rs.getString("berat"));
                    tab3_td.setText(rs.getString("tensi"));
                    tab3_nadi.setText(rs.getString("nadi"));
                    tab3_tb.setText(rs.getString("tinggi"));
                    tab3_respirasi.setText(rs.getString("respirasi"));
                    
                    tab3_suhu.setText(rs.getString("suhu_tubuh"));
                    tab3_gcs.setText(rs.getString("gcs"));
//                    tab3_rh.setText("X");  // Isikan sesuai kebutuhan
//                    tab3_hb.setText("X");  // Isikan sesuai kebutuhan
//                    tab3_vas.setText("X"); // Isikan sesuai kebutuhan
//                    tab3_ht.setText("X");  // Isikan sesuai kebutuhan
//                    tab3_lain.setText("X"); // Isikan sesuai kebutuhan
                }
            } catch (Exception e) {
                System.out.println("Notif : " + e);  // Perbaiki penanganan error sesuai dengan kebutuhan aplikasi Anda
            } finally {
                // Pastikan resource ditutup dengan benar
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (Exception e) {
                        System.out.println("Error closing ResultSet: " + e.getMessage());
                    }
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception e) {
                        System.out.println("Error closing PreparedStatement: " + e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : " + e);
        }

    }
     
}
