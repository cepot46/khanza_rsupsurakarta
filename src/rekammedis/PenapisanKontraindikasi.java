/*
 * Kontribusi dari Abdul Wahid, RSUD Cipayung Jakarta Timur
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;

/**
 *
 * @author perpustakaan
 */
public final class PenapisanKontraindikasi extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private StringBuilder htmlContent;
    private String finger="";
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public PenapisanKontraindikasi(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tgl.Lahir","J.K.","NIP Dokter","Nama Dokter",
            "Tanggal Verifikasi","NIP Petugas","Nama Petugas","Tanggal Petugas",
            "Sakit","Ket. Sakit","Alergi","Ket. Alergi","Alergi Berat","Ket. Alergi Berat",
            "Penyakit Kronis","Ket. Penyakit Kronis","Daya Tahan Tubuh","Ket. Daya Tahan Tubuh", "Obat Daya Tahan Tubuh","Ket. Obat Daya Tahan Tubuh",
            "Sistem Syaraf","Ket. Sistem Syaraf","Transfusi Darah","Ket. Transfusi Darah","Hamil","Ket. Hamil",
            "Vaksinasi","Ket. Vaksinasi","Kartu Vaksinasi","Ket. Kartu Vaksinasi"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        
        tbObat.setModel(tabMode);
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 33; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(150);
            }else if(i==3){
                column.setPreferredWidth(65);
            }else if(i==4){
                column.setPreferredWidth(55);
            }else if(i==5){
                column.setPreferredWidth(80);
            }else {
                column.setPreferredWidth(120);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        KetSakit.setDocument(new batasInput((int)500).getKata(KetSakit));
        KetAlergi.setDocument(new batasInput((int)500).getKata(KetAlergi));
        KetAlergiBerat.setDocument(new batasInput((int)500).getKata(KetAlergiBerat));
        KetPenyakitKronis.setDocument(new batasInput((int)500).getKata(KetPenyakitKronis));
        KetDayaTahanTubuh.setDocument(new batasInput((int)500).getKata(KetDayaTahanTubuh));
        KetObatDayaTahanTubuh.setDocument(new batasInput((int)500).getKata(KetObatDayaTahanTubuh));
        KetSistemSyaraf.setDocument(new batasInput((int)500).getKata(KetSistemSyaraf));
        KetTransfusiDarah.setDocument(new batasInput((int)500).getKata(KetTransfusiDarah));
        KetHamil.setDocument(new batasInput((int)500).getKata(KetHamil));
        KetVaksinasi.setDocument(new batasInput((int)500).getKata(KetVaksinasi));
        KetKartuVaksinasi.setDocument(new batasInput((int)500).getKata(KetKartuVaksinasi));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){
                    KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                    KdDokter.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });

        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                    KdPetugas.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnCetakFormulir = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdDokter = new widget.TextBox();
        NmDokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        jLabel11 = new widget.Label();
        jLabel37 = new widget.Label();
        scrollPane5 = new widget.ScrollPane();
        KetSakit = new widget.TextArea();
        jSeparator1 = new javax.swing.JSeparator();
        Sakit = new widget.ComboBox();
        jLabel99 = new widget.Label();
        label11 = new widget.Label();
        TglVerifikasi = new widget.Tanggal();
        label15 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnPetugas = new widget.Button();
        jLabel100 = new widget.Label();
        Alergi = new widget.ComboBox();
        jLabel38 = new widget.Label();
        scrollPane7 = new widget.ScrollPane();
        KetAlergi = new widget.TextArea();
        jLabel101 = new widget.Label();
        AlergiBerat = new widget.ComboBox();
        jLabel39 = new widget.Label();
        scrollPane8 = new widget.ScrollPane();
        KetAlergiBerat = new widget.TextArea();
        jLabel102 = new widget.Label();
        PenyakitKronis = new widget.ComboBox();
        jLabel40 = new widget.Label();
        scrollPane9 = new widget.ScrollPane();
        KetPenyakitKronis = new widget.TextArea();
        jLabel103 = new widget.Label();
        DayaTahanTubuh = new widget.ComboBox();
        jLabel41 = new widget.Label();
        scrollPane10 = new widget.ScrollPane();
        KetDayaTahanTubuh = new widget.TextArea();
        jLabel104 = new widget.Label();
        jLabel105 = new widget.Label();
        ObatDayaTahanTubuh = new widget.ComboBox();
        jLabel42 = new widget.Label();
        scrollPane11 = new widget.ScrollPane();
        KetObatDayaTahanTubuh = new widget.TextArea();
        jLabel106 = new widget.Label();
        SistemSyaraf = new widget.ComboBox();
        jLabel43 = new widget.Label();
        scrollPane12 = new widget.ScrollPane();
        KetSistemSyaraf = new widget.TextArea();
        jLabel107 = new widget.Label();
        scrollPane13 = new widget.ScrollPane();
        KetTransfusiDarah = new widget.TextArea();
        TransfusiDarah = new widget.ComboBox();
        jLabel44 = new widget.Label();
        jLabel108 = new widget.Label();
        scrollPane14 = new widget.ScrollPane();
        KetHamil = new widget.TextArea();
        jLabel45 = new widget.Label();
        Hamil = new widget.ComboBox();
        jLabel109 = new widget.Label();
        Vaksinasi = new widget.ComboBox();
        jLabel46 = new widget.Label();
        scrollPane15 = new widget.ScrollPane();
        KetVaksinasi = new widget.TextArea();
        jLabel110 = new widget.Label();
        KartuVaksinasi = new widget.ComboBox();
        jLabel47 = new widget.Label();
        scrollPane16 = new widget.ScrollPane();
        KetKartuVaksinasi = new widget.TextArea();
        label12 = new widget.Label();
        TglPetugas = new widget.Tanggal();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnCetakFormulir.setBackground(new java.awt.Color(255, 255, 254));
        MnCetakFormulir.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakFormulir.setForeground(new java.awt.Color(50, 50, 50));
        MnCetakFormulir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetakFormulir.setText("Cetak Formulir");
        MnCetakFormulir.setName("MnCetakFormulir"); // NOI18N
        MnCetakFormulir.setPreferredSize(new java.awt.Dimension(220, 26));
        MnCetakFormulir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakFormulirActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakFormulir);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Daftar Tilik Penapisan Kontraindikasi untuk Vaksinasi Dewasa ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 1100));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 70, 70, 23);

        KdDokter.setEditable(false);
        KdDokter.setName("KdDokter"); // NOI18N
        KdDokter.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokterKeyPressed(evt);
            }
        });
        FormInput.add(KdDokter);
        KdDokter.setBounds(74, 40, 90, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        NmDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmDokter);
        NmDokter.setBounds(166, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(348, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jLabel37.setText("Keterangan :");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(180, 120, 80, 23);

        scrollPane5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane5.setName("scrollPane5"); // NOI18N

        KetSakit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetSakit.setColumns(20);
        KetSakit.setRows(5);
        KetSakit.setName("KetSakit"); // NOI18N
        KetSakit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetSakitKeyPressed(evt);
            }
        });
        scrollPane5.setViewportView(KetSakit);

        FormInput.add(scrollPane5);
        scrollPane5.setBounds(270, 120, 220, 60);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 100, 880, 1);

        Sakit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        Sakit.setName("Sakit"); // NOI18N
        Sakit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SakitKeyPressed(evt);
            }
        });
        FormInput.add(Sakit);
        Sakit.setBounds(50, 120, 118, 23);

        jLabel99.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel99.setText("2. Apakah anda memiliki alergi terhadap obat-obatan, makanan, komponen vaksin atau lateks ?");
        jLabel99.setName("jLabel99"); // NOI18N
        FormInput.add(jLabel99);
        jLabel99.setBounds(10, 190, 600, 23);

        label11.setText("Tanggal Petugas :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(380, 70, 110, 23);

        TglVerifikasi.setForeground(new java.awt.Color(50, 70, 50));
        TglVerifikasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01-04-2024 08:35:34" }));
        TglVerifikasi.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglVerifikasi.setName("TglVerifikasi"); // NOI18N
        TglVerifikasi.setOpaque(false);
        TglVerifikasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglVerifikasiKeyPressed(evt);
            }
        });
        FormInput.add(TglVerifikasi);
        TglVerifikasi.setBounds(510, 40, 130, 23);

        label15.setText("Dokter :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label15);
        label15.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(74, 70, 90, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(166, 70, 180, 23);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas.setMnemonic('2');
        BtnPetugas.setToolTipText("Alt+2");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        FormInput.add(BtnPetugas);
        BtnPetugas.setBounds(348, 70, 28, 23);

        jLabel100.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel100.setText("1. Apakah anda sedang sakit hari ini ?");
        jLabel100.setName("jLabel100"); // NOI18N
        FormInput.add(jLabel100);
        jLabel100.setBounds(10, 100, 200, 23);

        Alergi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        Alergi.setName("Alergi"); // NOI18N
        Alergi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AlergiKeyPressed(evt);
            }
        });
        FormInput.add(Alergi);
        Alergi.setBounds(50, 210, 118, 23);

        jLabel38.setText("Keterangan :");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(180, 210, 80, 23);

        scrollPane7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane7.setName("scrollPane7"); // NOI18N

        KetAlergi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetAlergi.setColumns(20);
        KetAlergi.setRows(5);
        KetAlergi.setName("KetAlergi"); // NOI18N
        KetAlergi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetAlergiKeyPressed(evt);
            }
        });
        scrollPane7.setViewportView(KetAlergi);

        FormInput.add(scrollPane7);
        scrollPane7.setBounds(270, 210, 220, 60);

        jLabel101.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel101.setText("3. Apakah anda pernah mengalami reaksi alergi berat setelah menerima vaksinasi ?");
        jLabel101.setName("jLabel101"); // NOI18N
        FormInput.add(jLabel101);
        jLabel101.setBounds(10, 280, 600, 23);

        AlergiBerat.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        AlergiBerat.setName("AlergiBerat"); // NOI18N
        AlergiBerat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AlergiBeratKeyPressed(evt);
            }
        });
        FormInput.add(AlergiBerat);
        AlergiBerat.setBounds(50, 300, 118, 23);

        jLabel39.setText("Keterangan :");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(180, 300, 80, 23);

        scrollPane8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane8.setName("scrollPane8"); // NOI18N

        KetAlergiBerat.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetAlergiBerat.setColumns(20);
        KetAlergiBerat.setRows(5);
        KetAlergiBerat.setName("KetAlergiBerat"); // NOI18N
        KetAlergiBerat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetAlergiBeratKeyPressed(evt);
            }
        });
        scrollPane8.setViewportView(KetAlergiBerat);

        FormInput.add(scrollPane8);
        scrollPane8.setBounds(270, 300, 220, 60);

        jLabel102.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel102.setText("4. Apakah anda memiliki penyakit kronis terkait jantung, paru-paru, asma, ginjal, penyakit metabolik (diabetes), anemia atau penyakit kelainan darah ?");
        jLabel102.setName("jLabel102"); // NOI18N
        FormInput.add(jLabel102);
        jLabel102.setBounds(10, 370, 830, 23);

        PenyakitKronis.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        PenyakitKronis.setName("PenyakitKronis"); // NOI18N
        PenyakitKronis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PenyakitKronisKeyPressed(evt);
            }
        });
        FormInput.add(PenyakitKronis);
        PenyakitKronis.setBounds(50, 390, 118, 23);

        jLabel40.setText("Keterangan :");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(180, 390, 80, 23);

        scrollPane9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane9.setName("scrollPane9"); // NOI18N

        KetPenyakitKronis.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetPenyakitKronis.setColumns(20);
        KetPenyakitKronis.setRows(5);
        KetPenyakitKronis.setName("KetPenyakitKronis"); // NOI18N
        KetPenyakitKronis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetPenyakitKronisKeyPressed(evt);
            }
        });
        scrollPane9.setViewportView(KetPenyakitKronis);

        FormInput.add(scrollPane9);
        scrollPane9.setBounds(270, 390, 220, 60);

        jLabel103.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel103.setText("5. Apakah anda menderita kanker, leukimia, HIV/AIDS atau gangguan sistem daya tahan tubuh ?");
        jLabel103.setName("jLabel103"); // NOI18N
        FormInput.add(jLabel103);
        jLabel103.setBounds(10, 460, 830, 23);

        DayaTahanTubuh.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        DayaTahanTubuh.setName("DayaTahanTubuh"); // NOI18N
        DayaTahanTubuh.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DayaTahanTubuhKeyPressed(evt);
            }
        });
        FormInput.add(DayaTahanTubuh);
        DayaTahanTubuh.setBounds(50, 480, 118, 23);

        jLabel41.setText("Keterangan :");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(180, 480, 80, 23);

        scrollPane10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane10.setName("scrollPane10"); // NOI18N

        KetDayaTahanTubuh.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetDayaTahanTubuh.setColumns(20);
        KetDayaTahanTubuh.setRows(5);
        KetDayaTahanTubuh.setName("KetDayaTahanTubuh"); // NOI18N
        KetDayaTahanTubuh.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetDayaTahanTubuhKeyPressed(evt);
            }
        });
        scrollPane10.setViewportView(KetDayaTahanTubuh);

        FormInput.add(scrollPane10);
        scrollPane10.setBounds(270, 480, 220, 60);

        jLabel104.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel104.setText("6. Dalam 3 bulan terakhir, apakah anda mendapatkan pengobatan yang melemahkan daya tahan tubuh, seperti kortison, prednison, ");
        jLabel104.setName("jLabel104"); // NOI18N
        FormInput.add(jLabel104);
        jLabel104.setBounds(10, 550, 830, 23);

        jLabel105.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel105.setText(" steroid lainnya atau obat anti kanker, atau dalam terapi radiasi ?");
        jLabel105.setName("jLabel105"); // NOI18N
        FormInput.add(jLabel105);
        jLabel105.setBounds(10, 560, 830, 23);

        ObatDayaTahanTubuh.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        ObatDayaTahanTubuh.setName("ObatDayaTahanTubuh"); // NOI18N
        ObatDayaTahanTubuh.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ObatDayaTahanTubuhKeyPressed(evt);
            }
        });
        FormInput.add(ObatDayaTahanTubuh);
        ObatDayaTahanTubuh.setBounds(50, 580, 118, 23);

        jLabel42.setText("Keterangan :");
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(180, 580, 80, 23);

        scrollPane11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane11.setName("scrollPane11"); // NOI18N

        KetObatDayaTahanTubuh.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetObatDayaTahanTubuh.setColumns(20);
        KetObatDayaTahanTubuh.setRows(5);
        KetObatDayaTahanTubuh.setName("KetObatDayaTahanTubuh"); // NOI18N
        KetObatDayaTahanTubuh.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetObatDayaTahanTubuhKeyPressed(evt);
            }
        });
        scrollPane11.setViewportView(KetObatDayaTahanTubuh);

        FormInput.add(scrollPane11);
        scrollPane11.setBounds(270, 580, 220, 60);

        jLabel106.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel106.setText("7. Apakah anda pernah mengalami kejang atau gangguan sistem syaraf lainnya ?");
        jLabel106.setName("jLabel106"); // NOI18N
        FormInput.add(jLabel106);
        jLabel106.setBounds(10, 650, 830, 23);

        SistemSyaraf.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        SistemSyaraf.setName("SistemSyaraf"); // NOI18N
        SistemSyaraf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SistemSyarafKeyPressed(evt);
            }
        });
        FormInput.add(SistemSyaraf);
        SistemSyaraf.setBounds(50, 670, 118, 23);

        jLabel43.setText("Keterangan :");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(180, 670, 80, 23);

        scrollPane12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane12.setName("scrollPane12"); // NOI18N

        KetSistemSyaraf.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetSistemSyaraf.setColumns(20);
        KetSistemSyaraf.setRows(5);
        KetSistemSyaraf.setName("KetSistemSyaraf"); // NOI18N
        KetSistemSyaraf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetSistemSyarafKeyPressed(evt);
            }
        });
        scrollPane12.setViewportView(KetSistemSyaraf);

        FormInput.add(scrollPane12);
        scrollPane12.setBounds(270, 670, 220, 60);

        jLabel107.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel107.setText("8. Apakah anda menerima transfusi darah atau produk darah, atau mendapat terapi Imun (gamma) globulin, atau obat antiviral dalam satu tahun terakhir ?");
        jLabel107.setName("jLabel107"); // NOI18N
        FormInput.add(jLabel107);
        jLabel107.setBounds(10, 740, 830, 23);

        scrollPane13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane13.setName("scrollPane13"); // NOI18N

        KetTransfusiDarah.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetTransfusiDarah.setColumns(20);
        KetTransfusiDarah.setRows(5);
        KetTransfusiDarah.setName("KetTransfusiDarah"); // NOI18N
        KetTransfusiDarah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetTransfusiDarahKeyPressed(evt);
            }
        });
        scrollPane13.setViewportView(KetTransfusiDarah);

        FormInput.add(scrollPane13);
        scrollPane13.setBounds(270, 760, 220, 60);

        TransfusiDarah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        TransfusiDarah.setName("TransfusiDarah"); // NOI18N
        TransfusiDarah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TransfusiDarahKeyPressed(evt);
            }
        });
        FormInput.add(TransfusiDarah);
        TransfusiDarah.setBounds(50, 760, 118, 23);

        jLabel44.setText("Keterangan :");
        jLabel44.setName("jLabel44"); // NOI18N
        FormInput.add(jLabel44);
        jLabel44.setBounds(180, 760, 80, 23);

        jLabel108.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel108.setText("9. Apakah anda sedang hamil atau berencana untuk hamil dalam 1 bulan ke depan ?");
        jLabel108.setName("jLabel108"); // NOI18N
        FormInput.add(jLabel108);
        jLabel108.setBounds(10, 830, 830, 23);

        scrollPane14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane14.setName("scrollPane14"); // NOI18N

        KetHamil.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetHamil.setColumns(20);
        KetHamil.setRows(5);
        KetHamil.setName("KetHamil"); // NOI18N
        KetHamil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetHamilKeyPressed(evt);
            }
        });
        scrollPane14.setViewportView(KetHamil);

        FormInput.add(scrollPane14);
        scrollPane14.setBounds(270, 850, 220, 60);

        jLabel45.setText("Keterangan :");
        jLabel45.setName("jLabel45"); // NOI18N
        FormInput.add(jLabel45);
        jLabel45.setBounds(180, 850, 80, 23);

        Hamil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        Hamil.setName("Hamil"); // NOI18N
        Hamil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HamilKeyPressed(evt);
            }
        });
        FormInput.add(Hamil);
        Hamil.setBounds(50, 850, 118, 23);

        jLabel109.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel109.setText("10. Apakah anda mendapatkan vaksinasi dalam 4 minggu terakhir ?");
        jLabel109.setName("jLabel109"); // NOI18N
        FormInput.add(jLabel109);
        jLabel109.setBounds(10, 920, 830, 23);

        Vaksinasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        Vaksinasi.setName("Vaksinasi"); // NOI18N
        Vaksinasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                VaksinasiKeyPressed(evt);
            }
        });
        FormInput.add(Vaksinasi);
        Vaksinasi.setBounds(50, 940, 118, 23);

        jLabel46.setText("Keterangan :");
        jLabel46.setName("jLabel46"); // NOI18N
        FormInput.add(jLabel46);
        jLabel46.setBounds(180, 940, 80, 23);

        scrollPane15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane15.setName("scrollPane15"); // NOI18N

        KetVaksinasi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetVaksinasi.setColumns(20);
        KetVaksinasi.setRows(5);
        KetVaksinasi.setName("KetVaksinasi"); // NOI18N
        KetVaksinasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetVaksinasiKeyPressed(evt);
            }
        });
        scrollPane15.setViewportView(KetVaksinasi);

        FormInput.add(scrollPane15);
        scrollPane15.setBounds(270, 940, 220, 60);

        jLabel110.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel110.setText("11. Apakah anda membawa kartu vaksinasi ?");
        jLabel110.setName("jLabel110"); // NOI18N
        FormInput.add(jLabel110);
        jLabel110.setBounds(10, 1010, 830, 23);

        KartuVaksinasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Ya", "Tidak", "Tidak Tahu" }));
        KartuVaksinasi.setName("KartuVaksinasi"); // NOI18N
        KartuVaksinasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KartuVaksinasiKeyPressed(evt);
            }
        });
        FormInput.add(KartuVaksinasi);
        KartuVaksinasi.setBounds(50, 1030, 118, 23);

        jLabel47.setText("Keterangan :");
        jLabel47.setName("jLabel47"); // NOI18N
        FormInput.add(jLabel47);
        jLabel47.setBounds(180, 1030, 80, 23);

        scrollPane16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane16.setName("scrollPane16"); // NOI18N

        KetKartuVaksinasi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KetKartuVaksinasi.setColumns(20);
        KetKartuVaksinasi.setRows(5);
        KetKartuVaksinasi.setName("KetKartuVaksinasi"); // NOI18N
        KetKartuVaksinasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetKartuVaksinasiKeyPressed(evt);
            }
        });
        scrollPane16.setViewportView(KetKartuVaksinasi);

        FormInput.add(scrollPane16);
        scrollPane16.setBounds(270, 1030, 220, 60);

        label12.setText("Tanggal Verifikasi :");
        label12.setName("label12"); // NOI18N
        label12.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label12);
        label12.setBounds(380, 40, 110, 23);

        TglPetugas.setForeground(new java.awt.Color(50, 70, 50));
        TglPetugas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01-04-2024 09:59:53" }));
        TglPetugas.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglPetugas.setName("TglPetugas"); // NOI18N
        TglPetugas.setOpaque(false);
        TglPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglPetugasKeyPressed(evt);
            }
        });
        FormInput.add(TglPetugas);
        TglPetugas.setBounds(510, 70, 130, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Kontraindikasi", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01-04-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01-04-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Kontraindikasi", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{            
            Valid.pindah(evt,TCari,BtnDokter);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnPetugas,"Petugas");
        }else if(Sakit.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Sakit,"Pertanyaan No. 1");
        }else if(KetSakit.getText().trim().equals("")){
            Valid.textKosong(KetSakit,"Keterangan Pertanyaan No. 1");
        }else if(Alergi.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Sakit,"Pertanyaan No. 2");
        }else if(KetAlergi.getText().trim().equals("")){
            Valid.textKosong(KetAlergi,"Keterangan Pertanyaan No. 2");
        }else if(AlergiBerat.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Sakit,"Pertanyaan No. 3");
        }else if(KetAlergiBerat.getText().trim().equals("")){
            Valid.textKosong(KetAlergiBerat,"Keterangan Pertanyaan No. 3");
        }else if(PenyakitKronis.getSelectedItem().toString().equals("-")){
            Valid.textKosong(PenyakitKronis,"Pertanyaan No. 4");
        }else if(KetPenyakitKronis.getText().trim().equals("")){
            Valid.textKosong(KetPenyakitKronis,"Keterangan Pertanyaan No. 4");
        }else if(DayaTahanTubuh.getSelectedItem().toString().equals("-")){
            Valid.textKosong(DayaTahanTubuh,"Pertanyaan No. 5");
        }else if(KetDayaTahanTubuh.getText().trim().equals("")){
            Valid.textKosong(KetDayaTahanTubuh,"Keterangan Pertanyaan No. 5");
        }else if(ObatDayaTahanTubuh.getSelectedItem().toString().equals("-")){
            Valid.textKosong(ObatDayaTahanTubuh,"Pertanyaan No. 6");
        }else if(KetObatDayaTahanTubuh.getText().trim().equals("")){
            Valid.textKosong(KetObatDayaTahanTubuh,"Keterangan Pertanyaan No. 6");
        }else if(SistemSyaraf.getSelectedItem().toString().equals("-")){
            Valid.textKosong(SistemSyaraf,"Pertanyaan No. 7");
        }else if(KetSistemSyaraf.getText().trim().equals("")){
            Valid.textKosong(KetSistemSyaraf,"Keterangan Pertanyaan No. 7");
        }else if(TransfusiDarah.getSelectedItem().toString().equals("-")){
            Valid.textKosong(TransfusiDarah,"Pertanyaan No. 8");
        }else if(KetTransfusiDarah.getText().trim().equals("")){
            Valid.textKosong(KetTransfusiDarah,"Keterangan Pertanyaan No. 8");
        }else if(Hamil.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Hamil,"Pertanyaan No. 9");
        }else if(KetHamil.getText().trim().equals("")){
            Valid.textKosong(KetHamil,"Keterangan Pertanyaan No. 9");
        }else if(Vaksinasi.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Vaksinasi,"Pertanyaan No. 10");
        }else if(KetVaksinasi.getText().trim().equals("")){
            Valid.textKosong(KetVaksinasi,"Keterangan Pertanyaan No. 10");
        }else if(KartuVaksinasi.getSelectedItem().toString().equals("-")){
            Valid.textKosong(KartuVaksinasi,"Pertanyaan No. 11");
        }else if(KetKartuVaksinasi.getText().trim().equals("")){
            Valid.textKosong(KetKartuVaksinasi,"Keterangan Pertanyaan No. 11");
        }else{
            if(Sequel.menyimpantf("penapisan_kontraindikasi_vaksinasi","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",27,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglVerifikasi.getSelectedItem()+"")+" "+TglVerifikasi.getSelectedItem().toString().substring(11,19),
                Valid.SetTgl(TglPetugas.getSelectedItem()+"")+" "+TglPetugas.getSelectedItem().toString().substring(11,19),KdDokter.getText(),KdPetugas.getText(),
                Sakit.getSelectedItem().toString(),KetSakit.getText(),Alergi.getSelectedItem().toString(),KetAlergi.getText(),
                AlergiBerat.getSelectedItem().toString(),KetAlergiBerat.getText(),PenyakitKronis.getSelectedItem().toString(),KetPenyakitKronis.getText(),
                DayaTahanTubuh.getSelectedItem().toString(),KetDayaTahanTubuh.getText(),ObatDayaTahanTubuh.getSelectedItem().toString(),KetObatDayaTahanTubuh.getText(),
                SistemSyaraf.getSelectedItem().toString(),KetSistemSyaraf.getText(),TransfusiDarah.getSelectedItem().toString(),KetTransfusiDarah.getText(),
                Hamil.getSelectedItem().toString(),KetHamil.getText(),Vaksinasi.getSelectedItem().toString(),KetVaksinasi.getText(),
                KartuVaksinasi.getSelectedItem().toString(),KetKartuVaksinasi.getText()
                })==true){
                    JOptionPane.showMessageDialog(rootPane,"Data berhasil disimpan!");
                    emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,KetSakit,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()) || akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh dokter/petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }              
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnPetugas,"Petugas");
        }else if(Sakit.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Sakit,"Pertanyaan No. 1");
        }else if(KetSakit.getText().trim().equals("")){
            Valid.textKosong(KetSakit,"Keterangan Pertanyaan No. 1");
        }else if(Alergi.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Sakit,"Pertanyaan No. 2");
        }else if(KetAlergi.getText().trim().equals("")){
            Valid.textKosong(KetAlergi,"Keterangan Pertanyaan No. 2");
        }else if(AlergiBerat.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Sakit,"Pertanyaan No. 3");
        }else if(KetAlergiBerat.getText().trim().equals("")){
            Valid.textKosong(KetAlergiBerat,"Keterangan Pertanyaan No. 3");
        }else if(PenyakitKronis.getSelectedItem().toString().equals("-")){
            Valid.textKosong(PenyakitKronis,"Pertanyaan No. 4");
        }else if(KetPenyakitKronis.getText().trim().equals("")){
            Valid.textKosong(KetPenyakitKronis,"Keterangan Pertanyaan No. 4");
        }else if(DayaTahanTubuh.getSelectedItem().toString().equals("-")){
            Valid.textKosong(DayaTahanTubuh,"Pertanyaan No. 5");
        }else if(KetDayaTahanTubuh.getText().trim().equals("")){
            Valid.textKosong(KetDayaTahanTubuh,"Keterangan Pertanyaan No. 5");
        }else if(ObatDayaTahanTubuh.getSelectedItem().toString().equals("-")){
            Valid.textKosong(ObatDayaTahanTubuh,"Pertanyaan No. 6");
        }else if(KetObatDayaTahanTubuh.getText().trim().equals("")){
            Valid.textKosong(KetObatDayaTahanTubuh,"Keterangan Pertanyaan No. 6");
        }else if(SistemSyaraf.getSelectedItem().toString().equals("-")){
            Valid.textKosong(SistemSyaraf,"Pertanyaan No. 7");
        }else if(KetSistemSyaraf.getText().trim().equals("")){
            Valid.textKosong(KetSistemSyaraf,"Keterangan Pertanyaan No. 7");
        }else if(TransfusiDarah.getSelectedItem().toString().equals("-")){
            Valid.textKosong(TransfusiDarah,"Pertanyaan No. 8");
        }else if(KetTransfusiDarah.getText().trim().equals("")){
            Valid.textKosong(KetTransfusiDarah,"Keterangan Pertanyaan No. 8");
        }else if(Hamil.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Hamil,"Pertanyaan No. 9");
        }else if(KetHamil.getText().trim().equals("")){
            Valid.textKosong(KetHamil,"Keterangan Pertanyaan No. 9");
        }else if(Vaksinasi.getSelectedItem().toString().equals("-")){
            Valid.textKosong(Vaksinasi,"Pertanyaan No. 10");
        }else if(KetVaksinasi.getText().trim().equals("")){
            Valid.textKosong(KetVaksinasi,"Keterangan Pertanyaan No. 10");
        }else if(KartuVaksinasi.getSelectedItem().toString().equals("-")){
            Valid.textKosong(KartuVaksinasi,"Pertanyaan No. 11");
        }else if(KetKartuVaksinasi.getText().trim().equals("")){
            Valid.textKosong(KetKartuVaksinasi,"Keterangan Pertanyaan No. 11");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()) || akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString())){
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh dokter/petugas yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
//        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        if(tabMode.getRowCount()==0){
//            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//            BtnBatal.requestFocus();
//        }else if(tabMode.getRowCount()!=0){
//            try{
//                if(TCari.getText().trim().equals("")){
//                    ps=koneksi.prepareStatement(
//                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_medis_ralan.tanggal,"+
//                        "penilaian_medis_ralan.kd_dokter,penilaian_medis_ralan.anamnesis,penilaian_medis_ralan.hubungan,penilaian_medis_ralan.keluhan_utama,penilaian_medis_ralan.rps,penilaian_medis_ralan.rpk,penilaian_medis_ralan.rpd,penilaian_medis_ralan.rpo,penilaian_medis_ralan.alergi,"+
//                        "penilaian_medis_ralan.keadaan,penilaian_medis_ralan.gcs,penilaian_medis_ralan.kesadaran,penilaian_medis_ralan.td,penilaian_medis_ralan.nadi,penilaian_medis_ralan.rr,penilaian_medis_ralan.suhu,penilaian_medis_ralan.spo,penilaian_medis_ralan.bb,penilaian_medis_ralan.tb,"+
//                        "penilaian_medis_ralan.kepala,penilaian_medis_ralan.gigi,penilaian_medis_ralan.tht,penilaian_medis_ralan.thoraks,penilaian_medis_ralan.abdomen,penilaian_medis_ralan.ekstremitas,penilaian_medis_ralan.genital,penilaian_medis_ralan.kulit,"+
//                        "penilaian_medis_ralan.ket_fisik,penilaian_medis_ralan.ket_lokalis,penilaian_medis_ralan.penunjang,penilaian_medis_ralan.diagnosis,penilaian_medis_ralan.tata,penilaian_medis_ralan.konsulrujuk,dokter.nm_dokter "+
//                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                        "inner join penilaian_medis_ralan on reg_periksa.no_rawat=penilaian_medis_ralan.no_rawat "+
//                        "inner join dokter on penilaian_medis_ralan.kd_dokter=dokter.kd_dokter where "+
//                        "penilaian_medis_ralan.tanggal between ? and ? order by penilaian_medis_ralan.tanggal");
//                }else{
//                    ps=koneksi.prepareStatement(
//                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_medis_ralan.tanggal,"+
//                        "penilaian_medis_ralan.kd_dokter,penilaian_medis_ralan.anamnesis,penilaian_medis_ralan.hubungan,penilaian_medis_ralan.keluhan_utama,penilaian_medis_ralan.rps,penilaian_medis_ralan.rpk,penilaian_medis_ralan.rpd,penilaian_medis_ralan.rpo,penilaian_medis_ralan.alergi,"+
//                        "penilaian_medis_ralan.keadaan,penilaian_medis_ralan.gcs,penilaian_medis_ralan.kesadaran,penilaian_medis_ralan.td,penilaian_medis_ralan.nadi,penilaian_medis_ralan.rr,penilaian_medis_ralan.suhu,penilaian_medis_ralan.spo,penilaian_medis_ralan.bb,penilaian_medis_ralan.tb,"+
//                        "penilaian_medis_ralan.kepala,penilaian_medis_ralan.gigi,penilaian_medis_ralan.tht,penilaian_medis_ralan.thoraks,penilaian_medis_ralan.abdomen,penilaian_medis_ralan.ekstremitas,penilaian_medis_ralan.genital,penilaian_medis_ralan.kulit,"+
//                        "penilaian_medis_ralan.ket_fisik,penilaian_medis_ralan.ket_lokalis,penilaian_medis_ralan.penunjang,penilaian_medis_ralan.diagnosis,penilaian_medis_ralan.tata,penilaian_medis_ralan.konsulrujuk,dokter.nm_dokter "+
//                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                        "inner join penilaian_medis_ralan on reg_periksa.no_rawat=penilaian_medis_ralan.no_rawat "+
//                        "inner join dokter on penilaian_medis_ralan.kd_dokter=dokter.kd_dokter where "+
//                        "penilaian_medis_ralan.tanggal between ? and ? and (reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
//                        "penilaian_medis_ralan.kd_dokter like ? or dokter.nm_dokter like ?) order by penilaian_medis_ralan.tanggal");
//                }
//
//                try {
//                    if(TCari.getText().trim().equals("")){
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                    }else{
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(3,"%"+TCari.getText()+"%");
//                        ps.setString(4,"%"+TCari.getText()+"%");
//                        ps.setString(5,"%"+TCari.getText()+"%");
//                        ps.setString(6,"%"+TCari.getText()+"%");
//                        ps.setString(7,"%"+TCari.getText()+"%");
//                    }  
//                    rs=ps.executeQuery();
//                    htmlContent = new StringBuilder();
//                    htmlContent.append(                             
//                        "<tr class='isi'>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='105px'><b>No.Rawat</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='70px'><b>No.RM</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Nama Pasien</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='65px'><b>Tgl.Lahir</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='55px'><b>J.K.</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>NIP</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Nama Dokter</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='115px'><b>Tanggal</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Anamnesis</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='100px'><b>Hubungan</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Keluhan Utama</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Sekarang</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Dahulu</b></td>"+
//			    "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Keluarga</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penggunaan Obat</b></td>"+   //CUSTOM MUHSIN PENGGUNAKAN JADI PENGGUNAAN
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='120px'><b>Riwayat Alergi</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='90px'><b>Keadaan Umum</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='50px'><b>GCS</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kesadaran</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='60px'><b>TD(mmHg)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='75px'><b>Nadi(x/menit)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='67px'><b>RR(x/menit)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>Suhu</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>SpO2</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>BB(Kg)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>TB(cm)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kepala</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Gigi & Mulut</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>THT</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Thoraks</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Abdomen</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Genital & Anus</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Ekstremitas</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kulit</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Ket.Pemeriksaan Fisik</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='200px'><b>Ket.Status Lokalis</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='170px'><b>Pemeriksaa Penunjang</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Diagnosis/Asesmen</b></td>"+
//			    "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Tatalaksana</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Konsul/Rujuk</b></td>"+
//                        "</tr>"
//                    );
//                    while(rs.next()){
//                        htmlContent.append(
//                            "<tr class='isi'>"+
//                               "<td valign='top'>"+rs.getString("no_rawat")+"</td>"+
//                               "<td valign='top'>"+rs.getString("no_rkm_medis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nm_pasien")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tgl_lahir")+"</td>"+
//                               "<td valign='top'>"+rs.getString("jk")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kd_dokter")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nm_dokter")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tanggal")+"</td>"+
//                               "<td valign='top'>"+rs.getString("anamnesis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("hubungan")+"</td>"+
//                               "<td valign='top'>"+rs.getString("keluhan_utama")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rps")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpd")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpk")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpo")+"</td>"+
//                               "<td valign='top'>"+rs.getString("alergi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("keadaan")+"</td>"+
//                               "<td valign='top'>"+rs.getString("gcs")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kesadaran")+"</td>"+
//                               "<td valign='top'>"+rs.getString("td")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nadi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rr")+"</td>"+
//                               "<td valign='top'>"+rs.getString("suhu")+"</td>"+
//                               "<td valign='top'>"+rs.getString("spo")+"</td>"+
//                               "<td valign='top'>"+rs.getString("bb")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tb")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kepala")+"</td>"+
//                               "<td valign='top'>"+rs.getString("gigi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tht")+"</td>"+
//                               "<td valign='top'>"+rs.getString("thoraks")+"</td>"+
//                               "<td valign='top'>"+rs.getString("abdomen")+"</td>"+
//                               "<td valign='top'>"+rs.getString("genital")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ekstremitas")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kulit")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ket_fisik")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ket_lokalis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("penunjang")+"</td>"+
//                               "<td valign='top'>"+rs.getString("diagnosis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tata")+"</td>"+
//                               "<td valign='top'>"+rs.getString("konsulrujuk")+"</td>"+
//                            "</tr>");
//                    }
//                    LoadHTML.setText(
//                        "<html>"+
//                          "<table width='4400px' border='0' align='center' cellpadding='1px' cellspacing='0' class='tbl_form'>"+
//                           htmlContent.toString()+
//                          "</table>"+
//                        "</html>"
//                    );
//
//                    File g = new File("file2.css");            
//                    BufferedWriter bg = new BufferedWriter(new FileWriter(g));
//                    bg.write(
//                        ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
//                        ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
//                        ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
//                        ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
//                        ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
//                        ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
//                    );
//                    bg.close();
//
//                    File f = new File("DataPenilaianAwalMedisRalan.html");            
//                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));            
//                    bw.write(LoadHTML.getText().replaceAll("<head>","<head>"+
//                                "<link href=\"file2.css\" rel=\"stylesheet\" type=\"text/css\" />"+
//                                "<table width='4400px' border='0' align='center' cellpadding='3px' cellspacing='0' class='tbl_form'>"+
//                                    "<tr class='isi2'>"+
//                                        "<td valign='top' align='center'>"+
//                                            "<font size='4' face='Tahoma'>"+akses.getnamars()+"</font><br>"+
//                                            akses.getalamatrs()+", "+akses.getkabupatenrs()+", "+akses.getpropinsirs()+"<br>"+
//                                            akses.getkontakrs()+", E-mail : "+akses.getemailrs()+"<br><br>"+
//                                            "<font size='2' face='Tahoma'>DATA PENILAIAN AWAL MEDIS RAWAT JALAN<br><br></font>"+        
//                                        "</td>"+
//                                   "</tr>"+
//                                "</table>")
//                    );
//                    bw.close();                         
//                    Desktop.getDesktop().browse(f.toURI());
//                } catch (Exception e) {
//                    System.out.println("Notif : "+e);
//                } finally{
//                    if(rs!=null){
//                        rs.close();
//                    }
//                    if(ps!=null){
//                        ps.close();
//                    }
//                }
//
//            }catch(Exception e){
//                System.out.println("Notifikasi : "+e);
//            }
//        }
//        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void KdDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokterKeyPressed
        
    }//GEN-LAST:event_KdDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setAlwaysOnTop(false);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        //Valid.pindah(evt,Monitoring,BtnSimpan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void KetSakitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetSakitKeyPressed
        Valid.pindah2(evt,Sakit,Alergi);
    }//GEN-LAST:event_KetSakitKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void SakitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SakitKeyPressed
        Valid.pindah(evt,BtnPetugas,KetSakit);
    }//GEN-LAST:event_SakitKeyPressed

    private void TglVerifikasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglVerifikasiKeyPressed
        Valid.pindah(evt,BtnDokter,BtnPetugas);
    }//GEN-LAST:event_TglVerifikasiKeyPressed

    private void MnCetakFormulirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakFormulirActionPerformed
        if(tbObat.getSelectedRow()>-1){
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());          
            param.put("logo",Sequel.cariGambar("select logo from setting")); 
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes from gambar_tambahan")); 
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),5).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString())); 
            param.put("alamat",Sequel.cariIsi("select concat(pasien.alamat,', ',k.nm_kel,', ',k2.nm_kec,', ',k3.nm_kab) as alamat\n" +
                                "from pasien\n" +
                                "inner join kelurahan k on pasien.kd_kel = k.kd_kel\n" +
                                "inner join kecamatan k2 on pasien.kd_kec = k2.kd_kec\n" +
                                "inner join kabupaten k3 on pasien.kd_kab = k3.kd_kab\n" +
                                "where no_rkm_medis=?",tbObat.getValueAt(tbObat.getSelectedRow(),1).toString()));            
            
            Valid.MyReportqry("rptCetakPenapisanKontraindikasiVaksinasi.jasper","report","::[ Formulir Daftar Tilik Penapisan Kontraindikasi Untuk Vaksinasi ]::",
                "select pkm.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk," +
                "DATE_FORMAT(p.tgl_lahir,'%d-%m-%Y') as tgl_lahir,pkm.kd_dokter,d.nm_dokter,pkm.kd_petugas,p2.nama,"+ 
                "DATE_FORMAT(pkm.tanggal_verifikasi, '%d-%m-%Y') as tanggal_verifikasi,DATE_FORMAT(pkm.tanggal_petugas, '%d-%m-%Y') as tanggal_petugas," +
                "pkm.sakit,pkm.ket_sakit,pkm.alergi,pkm.ket_alergi,pkm.alergi_berat,pkm.ket_alergi_berat," +
                "pkm.penyakit_kronis,pkm.ket_penyakit_kronis,pkm.daya_tahan_tubuh,pkm.ket_daya_tahan_tubuh,pkm.obat_daya_tahan_tubuh,pkm.ket_obat_daya_tahan_tubuh,"+  
                "pkm.sistem_syaraf,pkm.ket_sistem_syaraf,pkm.transfusi_darah,pkm.ket_transfusi_darah,pkm.hamil,pkm.ket_hamil,"+  
                "pkm.vaksinasi,pkm.ket_vaksinasi,pkm.kartu_vaksinasi,pkm.ket_kartu_vaksinasi "+      
                "from reg_periksa inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                "inner join penapisan_kontraindikasi_vaksinasi pkm on reg_periksa.no_rawat = pkm.no_rawat " +
                "inner join dokter d on pkm.kd_dokter = d.kd_dokter " +
                "inner join petugas p2 on pkm.kd_petugas = p2.nip " +
                "where pkm.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
            this.setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_MnCetakFormulirActionPerformed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdPetugasKeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed
        // TODO add your handling code here:
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);        
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void AlergiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AlergiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlergiKeyPressed

    private void KetAlergiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetAlergiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetAlergiKeyPressed

    private void AlergiBeratKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AlergiBeratKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AlergiBeratKeyPressed

    private void KetAlergiBeratKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetAlergiBeratKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetAlergiBeratKeyPressed

    private void PenyakitKronisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PenyakitKronisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PenyakitKronisKeyPressed

    private void KetPenyakitKronisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetPenyakitKronisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetPenyakitKronisKeyPressed

    private void DayaTahanTubuhKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DayaTahanTubuhKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DayaTahanTubuhKeyPressed

    private void KetDayaTahanTubuhKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetDayaTahanTubuhKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetDayaTahanTubuhKeyPressed

    private void ObatDayaTahanTubuhKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ObatDayaTahanTubuhKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatDayaTahanTubuhKeyPressed

    private void KetObatDayaTahanTubuhKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetObatDayaTahanTubuhKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetObatDayaTahanTubuhKeyPressed

    private void SistemSyarafKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SistemSyarafKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_SistemSyarafKeyPressed

    private void KetSistemSyarafKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetSistemSyarafKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetSistemSyarafKeyPressed

    private void KetTransfusiDarahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetTransfusiDarahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetTransfusiDarahKeyPressed

    private void TransfusiDarahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TransfusiDarahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TransfusiDarahKeyPressed

    private void KetHamilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetHamilKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetHamilKeyPressed

    private void HamilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HamilKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HamilKeyPressed

    private void VaksinasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_VaksinasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_VaksinasiKeyPressed

    private void KetVaksinasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetVaksinasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetVaksinasiKeyPressed

    private void KartuVaksinasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KartuVaksinasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KartuVaksinasiKeyPressed

    private void KetKartuVaksinasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetKartuVaksinasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetKartuVaksinasiKeyPressed

    private void TglPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglPetugasKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            PenapisanKontraindikasi dialog = new PenapisanKontraindikasi(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.ComboBox Alergi;
    private widget.ComboBox AlergiBerat;
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPetugas;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox DayaTahanTubuh;
    private widget.PanelBiasa FormInput;
    private widget.ComboBox Hamil;
    private widget.TextBox Jk;
    private widget.ComboBox KartuVaksinasi;
    private widget.TextBox KdDokter;
    private widget.TextBox KdPetugas;
    private widget.TextArea KetAlergi;
    private widget.TextArea KetAlergiBerat;
    private widget.TextArea KetDayaTahanTubuh;
    private widget.TextArea KetHamil;
    private widget.TextArea KetKartuVaksinasi;
    private widget.TextArea KetObatDayaTahanTubuh;
    private widget.TextArea KetPenyakitKronis;
    private widget.TextArea KetSakit;
    private widget.TextArea KetSistemSyaraf;
    private widget.TextArea KetTransfusiDarah;
    private widget.TextArea KetVaksinasi;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private javax.swing.JMenuItem MnCetakFormulir;
    private widget.TextBox NmDokter;
    private widget.TextBox NmPetugas;
    private widget.ComboBox ObatDayaTahanTubuh;
    private widget.ComboBox PenyakitKronis;
    private widget.ComboBox Sakit;
    private widget.ScrollPane Scroll;
    private widget.ComboBox SistemSyaraf;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.TextBox TglLahir;
    private widget.Tanggal TglPetugas;
    private widget.Tanggal TglVerifikasi;
    private widget.ComboBox TransfusiDarah;
    private widget.ComboBox Vaksinasi;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel100;
    private widget.Label jLabel101;
    private widget.Label jLabel102;
    private widget.Label jLabel103;
    private widget.Label jLabel104;
    private widget.Label jLabel105;
    private widget.Label jLabel106;
    private widget.Label jLabel107;
    private widget.Label jLabel108;
    private widget.Label jLabel109;
    private widget.Label jLabel11;
    private widget.Label jLabel110;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private widget.Label jLabel99;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private widget.Label label11;
    private widget.Label label12;
    private widget.Label label14;
    private widget.Label label15;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane10;
    private widget.ScrollPane scrollPane11;
    private widget.ScrollPane scrollPane12;
    private widget.ScrollPane scrollPane13;
    private widget.ScrollPane scrollPane14;
    private widget.ScrollPane scrollPane15;
    private widget.ScrollPane scrollPane16;
    private widget.ScrollPane scrollPane5;
    private widget.ScrollPane scrollPane7;
    private widget.ScrollPane scrollPane8;
    private widget.ScrollPane scrollPane9;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().trim().equals("")){
                ps=koneksi.prepareStatement(
                        "select pkm.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk," +
                        "p.tgl_lahir,pkm.kd_dokter,d.nm_dokter,pkm.kd_petugas,p2.nama,"+ 
                        "pkm.tanggal_verifikasi,pkm.tanggal_petugas," +
                        "pkm.sakit,pkm.ket_sakit,pkm.alergi,pkm.ket_alergi,pkm.alergi_berat,pkm.ket_alergi_berat," +
                        "pkm.penyakit_kronis,pkm.ket_penyakit_kronis,pkm.daya_tahan_tubuh,pkm.ket_daya_tahan_tubuh,pkm.obat_daya_tahan_tubuh,pkm.ket_obat_daya_tahan_tubuh,"+  
                        "pkm.sistem_syaraf,pkm.ket_sistem_syaraf,pkm.transfusi_darah,pkm.ket_transfusi_darah,pkm.hamil,pkm.ket_hamil,"+  
                        "pkm.vaksinasi,pkm.ket_vaksinasi,pkm.kartu_vaksinasi,pkm.ket_kartu_vaksinasi "+      
                        "from reg_periksa inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penapisan_kontraindikasi_vaksinasi pkm on reg_periksa.no_rawat = pkm.no_rawat " +
                        "inner join dokter d on pkm.kd_dokter = d.kd_dokter " +
                        "inner join petugas p2 on pkm.kd_petugas = p2.nip " +
                        "where pkm.tanggal_verifikasi between ? and ? " +
                        "order by pkm.tanggal_verifikasi");
            }else{
                ps=koneksi.prepareStatement(
                        "select pkm.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk," +
                        "p.tgl_lahir,pkm.kd_dokter,d.nm_dokter,pkm.kd_petugas,p2.nama,"+ 
                        "pkm.tanggal_verifikasi,pkm.tanggal_petugas," +
                        "pkm.sakit,pkm.ket_sakit,pkm.alergi,pkm.ket_alergi,pkm.alergi_berat,pkm.ket_alergi_berat," +
                        "pkm.penyakit_kronis,pkm.ket_penyakit_kronis,pkm.daya_tahan_tubuh,pkm.ket_daya_tahan_tubuh,pkm.obat_daya_tahan_tubuh,pkm.ket_obat_daya_tahan_tubuh,"+  
                        "pkm.sistem_syaraf,pkm.ket_sistem_syaraf,pkm.transfusi_darah,pkm.ket_transfusi_darah,pkm.hamil,pkm.ket_hamil,"+  
                        "pkm.vaksinasi,pkm.ket_vaksinasi,pkm.kartu_vaksinasi,pkm.ket_kartu_vaksinasi "+      
                        "from reg_periksa inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penapisan_kontraindikasi_vaksinasi pkm on reg_periksa.no_rawat = pkm.no_rawat " +
                        "inner join dokter d on pkm.kd_dokter = d.kd_dokter " +
                        "inner join petugas p2 on pkm.kd_petugas = p2.nip " +
                        "where pkm.tanggal_verifikasi between ? and ? and (reg_periksa.no_rawat like ? or p.no_rkm_medis like ? or p.nm_pasien like ? or pkm.kd_dokter like ? or d.nm_dokter like ?) " +
                        "order by pkm.tanggal_verifikasi");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("tgl_lahir"),rs.getString("jk"),rs.getString("kd_dokter"),rs.getString("nm_dokter"),
                        rs.getString("tanggal_verifikasi"),rs.getString("kd_petugas"),rs.getString("nama"),rs.getString("tanggal_petugas"),
                        rs.getString("sakit"),rs.getString("ket_sakit"),rs.getString("alergi"),rs.getString("ket_alergi"),rs.getString("alergi_berat"),rs.getString("ket_alergi_berat"),
                        rs.getString("penyakit_kronis"),rs.getString("ket_penyakit_kronis"),rs.getString("daya_tahan_tubuh"),rs.getString("ket_daya_tahan_tubuh"),rs.getString("obat_daya_tahan_tubuh"),rs.getString("ket_obat_daya_tahan_tubuh"),
                        rs.getString("sistem_syaraf"),rs.getString("ket_sistem_syaraf"),rs.getString("transfusi_darah"),rs.getString("ket_transfusi_darah"),rs.getString("hamil"),rs.getString("ket_hamil"),
                        rs.getString("vaksinasi"),rs.getString("ket_vaksinasi"),rs.getString("kartu_vaksinasi"),rs.getString("ket_kartu_vaksinasi")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TabRawat.setSelectedIndex(0);
        TglVerifikasi.setDate(new Date());
        TglPetugas.setDate(new Date());
        Sakit.setSelectedIndex(0);
        KetSakit.setText("");
        Alergi.setSelectedIndex(0);
        KetAlergi.setText("");
        AlergiBerat.setSelectedIndex(0);
        KetAlergiBerat.setText("");
        PenyakitKronis.setSelectedIndex(0);
        KetPenyakitKronis.setText("");
        DayaTahanTubuh.setSelectedIndex(0);
        KetDayaTahanTubuh.setText("");
        ObatDayaTahanTubuh.setSelectedIndex(0);
        KetObatDayaTahanTubuh.setText("");
        SistemSyaraf.setSelectedIndex(0);
        KetSistemSyaraf.setText("");
        TransfusiDarah.setSelectedIndex(0);
        KetTransfusiDarah.setText("");
        Hamil.setSelectedIndex(0);
        KetHamil.setText("");
        Vaksinasi.setSelectedIndex(0);
        KetVaksinasi.setText("");
        KartuVaksinasi.setSelectedIndex(0);
        KetKartuVaksinasi.setText("");
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            Valid.SetTgl2(TglVerifikasi,tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            Valid.SetTgl2(TglPetugas,tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            Sakit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            KetSakit.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            Alergi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            KetAlergi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            AlergiBerat.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            KetAlergiBerat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            PenyakitKronis.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            KetPenyakitKronis.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            DayaTahanTubuh.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            KetDayaTahanTubuh.setText(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            ObatDayaTahanTubuh.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            KetObatDayaTahanTubuh.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            SistemSyaraf.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            KetSistemSyaraf.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            TransfusiDarah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            KetTransfusiDarah.setText(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            Hamil.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),27).toString());
            KetHamil.setText(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            Vaksinasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            KetVaksinasi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
            KartuVaksinasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            KetKartuVaksinasi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());            
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
 
    public void setNoRm(String norwt,Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);    
        //CUSTOM KHANZA
        Sequel.cariIsi("select kd_dokter from reg_periksa where no_rawat=?", KdDokter,norwt);
        Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?", NmDokter,KdDokter.getText());
        KdPetugas.setText(akses.getkode());
        Sequel.cariIsi("select nama from petugas where nip=?", NmPetugas,KdPetugas.getText());
        //END CUSTOM
        isRawat(); 
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_awal_medis_ralan());
        BtnHapus.setEnabled(akses.getpenilaian_awal_medis_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_medis_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_medis_ralan());         
    }
    
    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from penapisan_kontraindikasi_vaksinasi where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            tampil();
            TabRawat.setSelectedIndex(1);
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("penapisan_kontraindikasi_vaksinasi","no_rawat=?",
            "no_rawat=?,tanggal_verifikasi=?,tanggal_petugas=?,kd_dokter=?,kd_petugas=?,"+ 
            "sakit=?,ket_sakit=?,alergi=?,ket_alergi=?,alergi_berat=?,ket_alergi_berat=?,"+ 
            "penyakit_kronis=?,ket_penyakit_kronis=?,daya_tahan_tubuh=?,ket_daya_tahan_tubuh=?,obat_daya_tahan_tubuh=?,ket_obat_daya_tahan_tubuh=?,"+  
            "sistem_syaraf=?,ket_sistem_syaraf=?,transfusi_darah=?,ket_transfusi_darah=?,hamil=?,ket_hamil=?,"+
            "vaksinasi=?,ket_vaksinasi=?,kartu_vaksinasi=?,ket_kartu_vaksinasi=?",28,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglVerifikasi.getSelectedItem()+"")+" "+TglVerifikasi.getSelectedItem().toString().substring(11,19),
                Valid.SetTgl(TglPetugas.getSelectedItem()+"")+" "+TglPetugas.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),KdPetugas.getText(),
                Sakit.getSelectedItem().toString(),KetSakit.getText(),
                Alergi.getSelectedItem().toString(),KetAlergi.getText(),
                AlergiBerat.getSelectedItem().toString(),KetAlergiBerat.getText(),
                PenyakitKronis.getSelectedItem().toString(),KetPenyakitKronis.getText(),
                DayaTahanTubuh.getSelectedItem().toString(),KetDayaTahanTubuh.getText(),
                ObatDayaTahanTubuh.getSelectedItem().toString(),KetObatDayaTahanTubuh.getText(),
                SistemSyaraf.getSelectedItem().toString(),KetSistemSyaraf.getText(),
                TransfusiDarah.getSelectedItem().toString(),KetTransfusiDarah.getText(),
                Hamil.getSelectedItem().toString(),KetHamil.getText(),
                Vaksinasi.getSelectedItem().toString(),KetVaksinasi.getText(),
                KartuVaksinasi.getSelectedItem().toString(),KetKartuVaksinasi.getText(),
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
            })==true){
               tampil();
               emptTeks();
               TabRawat.setSelectedIndex(1);
        }
    }
}
