/*
  Kontribusi Anggit Nurhidayah
 */

package rekammedis;

import setting.*;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.WarnaTable;
import fungsi.akses;
import inventory.DlgBarang;
import inventory.DlgCariJenis;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;

/**
 *
 * @author perpustakaan
 */
public class RMOKAsesmenPraBedahdanVerifikasi extends javax.swing.JDialog {
    private final DefaultTableModel tabModePraBedah,tabModeInOp,tabModePraInduksi;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private RMCariPemeriksaan caripemeriksaan=new RMCariPemeriksaan(null,false);
    private PreparedStatement ps;
    private ResultSet rs;
    private DlgBarang barang=new DlgBarang(null,false);
    private String finger="",finger2="",ruang_rawat="";
    private int i=0;

    /**
     *@param parent
     *@param modal*/
    public RMOKAsesmenPraBedahdanVerifikasi(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        tabModePraBedah=new DefaultTableModel(null,new Object[]{
              "No.Rawat","Tanggal","Jam","No.RM","Nama Pasien",
              "JK","Data Subyektif","Data Obyektif","Diagnosis Pra Operasi","Rencana Operasi","Deskripsi Penandaan","Posisi Pasien","Posisi Pasien Lainnya" ,"Kd Dokter","Dokter"
                
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        tbChecklist.setModel(tabModePraBedah);
        tbChecklist.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbChecklist.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 14; i++) {
            TableColumn column = tbChecklist.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(80);
            }if(i==1){
                column.setPreferredWidth(80);
            }if(i==2){
                column.setPreferredWidth(80);
            }if(i==3){
                column.setPreferredWidth(80);
            }if(i==4){
                column.setPreferredWidth(80);
            }if(i==5){
                column.setPreferredWidth(80);
            }if(i==6){
                column.setPreferredWidth(80);
            }if(i==7){
                column.setPreferredWidth(100);
            }if(i==8){
                column.setPreferredWidth(120);
            }if(i==9){
                column.setPreferredWidth(120);
            }if(i==10){
                column.setPreferredWidth(120);
            }if(i==11){
                column.setPreferredWidth(120);
            }if(i==12){
                column.setPreferredWidth(120);
            }if(i==13){
                column.setPreferredWidth(120);
            }if(i==14){
                column.setPreferredWidth(120);
            }
            
        }

        tbChecklist.setDefaultRenderer(Object.class, new WarnaTable());
       
        
        KdDokter.setDocument(new batasInput((byte)20).getKata(KdDokter));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        TCari1.setDocument(new batasInput((int)100).getKata(TCari1));
        caripemeriksaan.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
               if(i==1){
                if(caripemeriksaan.getTable().getSelectedRow()!= -1){
                    data_subyektif.append(caripemeriksaan.getTable().getValueAt(caripemeriksaan.getTable().getSelectedRow(),3).toString()+", ");
                    data_subyektif.requestFocus();
                }
               } else if(i==2) {
                  if(caripemeriksaan.getTable().getSelectedRow()!= -1){
                    data_obyektif.append(caripemeriksaan.getTable().getValueAt(caripemeriksaan.getTable().getSelectedRow(),3).toString()+", ");
                    data_obyektif.requestFocus();
                }
               }
                
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        // TABEL UNTUK  INTA OPERATIF
         tabModeInOp=new DefaultTableModel(null,new Object[]{
             "No.Rawat","Tanggal","Jam","Kode Petugas","Petugas","No.RM","Nama","JK",
             "anamnesis", "diagnosa preop", "rencana operasi", "tb", "bb", "obat dikonsumsi", 
             "obat dikonsumsi ket", "riwayat alergi", "riwayat penyakit", "riwayat anestesi", 
             "jenis anestesi", "riwayat merokok", "komplikasi anestesi", "fisik b1", "fisik alat", 
             "fisik rr", "fisik vesikuler", "fisik rhonki", "fisik wheezing", "fisik td", "fisik hr", 
             "fisik hr ket", "fisik konjungtiva", "fisik gcse", "fisik gcsm", "fisik gcsv", "fisik pupil", 
             "fisik hemiparese", "fisik urin", "fisik warnaurin", "fisik perut", "fisik diare", "fisik muntah", 
             "fisik alatbantu", "fisik fraktur", "penunjang lab", "penunjang rab", "penunjang elektro", "asa", "asa1", 
             "asa2", "asa3", "asa4", "asa5", "asa6", "asaE", "rencana ga", "rencana reg", "rencana blok", "rencana alat khusus", 
             "monitoring khusus", "rencana perawatan inap", "rencana hcu", "rencana icu", "rencana rajal", "rencana igd"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        tbPrasedasi.setModel(tabModeInOp);
        tbPrasedasi.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPrasedasi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 63; i++) {
            TableColumn column = tbPrasedasi.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(80);
            }if(i==1){
                column.setPreferredWidth(80);
            }if(i==2){
                column.setPreferredWidth(80);
            }if(i==3){
                column.setPreferredWidth(80);
            }if(i==4){
                column.setPreferredWidth(80);
            }if(i==5){
                column.setPreferredWidth(80);
            }if(i==6){
                column.setPreferredWidth(80);
            }if(i==7){
                column.setPreferredWidth(100);
            }if(i==8){
                column.setPreferredWidth(120);
            }if(i==9){
                column.setPreferredWidth(120);
            }if(i==10){
                column.setPreferredWidth(120);
            }if(i==11){
                column.setPreferredWidth(120);
            }if(i==12){
                column.setPreferredWidth(120);
            }if(i==13){
                column.setPreferredWidth(120);
            }if(i==14){
                column.setPreferredWidth(120);
            }if(i==15){
                column.setPreferredWidth(120);
            }if(i==16){
                column.setPreferredWidth(120);
            }if(i==17){
                column.setPreferredWidth(120);
            }if(i==18){
                column.setPreferredWidth(120);
            }if(i==19){
                column.setPreferredWidth(120);
            }if(i==20){
                column.setPreferredWidth(120);
            }if(i==21){
                column.setPreferredWidth(120);
            }if(i==22){
                column.setPreferredWidth(120);
            }if(i==23){
                column.setPreferredWidth(120);
            }if(i==24){
                column.setPreferredWidth(120);
            }if(i==25){
                column.setPreferredWidth(120);
            }if(i==26){
                column.setPreferredWidth(120);
            }if(i==27){
                column.setPreferredWidth(120);
            }if(i==28){
                column.setPreferredWidth(120);
            }if(i==29){
                column.setPreferredWidth(120);
            }if(i==30){
                column.setPreferredWidth(120);
            }if(i==31){
                column.setPreferredWidth(120);
            }if(i==32){
                column.setPreferredWidth(120);
            }if(i==33){
                column.setPreferredWidth(120);
            }if(i==34){
                column.setPreferredWidth(120);
            }if(i==35){
                column.setPreferredWidth(120);
            }if(i==36){
                column.setPreferredWidth(120);
            }if(i==37){
                column.setPreferredWidth(120);
            }if(i==38){
                column.setPreferredWidth(120);
            }if(i==39){
                column.setPreferredWidth(120);
            }if(i==40){
                column.setPreferredWidth(120);
            }if(i==41){
                column.setPreferredWidth(120);
            }if(i==42){
                column.setPreferredWidth(120);
            }if(i==43){
                column.setPreferredWidth(120);
            }if(i==44){
                column.setPreferredWidth(120);
            }if(i==45){
                column.setPreferredWidth(120);
            }if(i==46){
                column.setPreferredWidth(120);
            }if(i==47){
                column.setPreferredWidth(120);
            }if(i==48){
                column.setPreferredWidth(120);
            }if(i==49){
                column.setPreferredWidth(120);
            }if(i==50){
                column.setPreferredWidth(120);
            }if(i==51){
                column.setPreferredWidth(120);
            }if(i==52){
                column.setPreferredWidth(120);
            }if(i==53){
                column.setPreferredWidth(120);
            }if(i==54){
                column.setPreferredWidth(120);
            }if(i==55){
                column.setPreferredWidth(120);
            }if(i==56){
                column.setPreferredWidth(120);
            }if(i==57){
                column.setPreferredWidth(120);
            }if(i==59){
                column.setPreferredWidth(120);
            }if(i==60){
                column.setPreferredWidth(120);
            }if(i==61){
                column.setPreferredWidth(120);
            }if(i==62){
                column.setPreferredWidth(120);
            }if(i==63){
                column.setPreferredWidth(120);
            }
        }

        tbPrasedasi.setDefaultRenderer(Object.class, new WarnaTable());
        
        
        // TABEL UNTUK  PRA INDUKSI
         tabModePraInduksi=new DefaultTableModel(null,new Object[]{
            "No. Rawat", "Tanggal", "Jam", "Kode Dokter", "Nama Dokter", 
            "No. Rekam Medis", "Nama Pasien", "Alergi", "Bb", "Td", "Nadi", 
            "Tb", "Respirasi", "Gol Darah", "Suhu", "Gcs", "Rh", "Hb", "Vas", 
            "Ht", "Lain", "Fisik Normal", "Fisik Bukamulut", "Fisik Jarak", 
            "Fisik Mallampati", "Fisik Gerakan Leher", "Fisik Abnormal", 
            "Anamnesis Auto", "Anamnesis Allo", "Asa1", "Asa2", "Asa3", 
            "Asa4", "Asa5", "Asa E", "Penyulit Praanestesi", "Ijin Operasi", "Cek Mohon Anestesi", 
            "Cek Suction Unit", "Persiapan Obat Obatan", "Antibiotika Profilaksis", "GA", "Tiva", "LMA", "Facemask", "ET", 
            "Regional", "Spinal", "Epidural", "Kaudal", "Blok Saraf Tepi", "Anestesi Lain", "Monitoring 1", 
            "Monitoring 2", "Monitoring 3", "Monitoring 3 Ket", "Monitoring 4", 
            "Monitoring 4 Ket", "Monitoring 5", "Monitoring 6", "Monitoring 7", 
            "Monitoring 8", "Monitoring 9"


            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        tbPrainduksi.setModel(tabModePraInduksi);
        tbPrainduksi.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPrainduksi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 63; i++) {
            TableColumn column = tbPrainduksi.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(80);
            }if(i==1){
                column.setPreferredWidth(80);
            }if(i==2){
                column.setPreferredWidth(80);
            }if(i==3){
                column.setPreferredWidth(80);
            }if(i==4){
                column.setPreferredWidth(80);
            }if(i==5){
                column.setPreferredWidth(80);
            }if(i==6){
                column.setPreferredWidth(80);
            }if(i==7){
                column.setPreferredWidth(100);
            }if(i==8){
                column.setPreferredWidth(120);
            }if(i==9){
                column.setPreferredWidth(120);
            }if(i==10){
                column.setPreferredWidth(120);
            }if(i==11){
                column.setPreferredWidth(120);
            }if(i==12){
                column.setPreferredWidth(120);
            }if(i==13){
                column.setPreferredWidth(120);
            }if(i==14){
                column.setPreferredWidth(120);
            }if(i==15){
                column.setPreferredWidth(120);
            }if(i==16){
                column.setPreferredWidth(120);
            }if(i==17){
                column.setPreferredWidth(120);
            }if(i==18){
                column.setPreferredWidth(120);
            }if(i==19){
                column.setPreferredWidth(120);
            }if(i==20){
                column.setPreferredWidth(120);
            }if(i==21){
                column.setPreferredWidth(120);
            }if(i==22){
                column.setPreferredWidth(120);
            }if(i==23){
                column.setPreferredWidth(120);
            }if(i==24){
                column.setPreferredWidth(120);
            }if(i==25){
                column.setPreferredWidth(120);
            }if(i==26){
                column.setPreferredWidth(120);
            }if(i==27){
                column.setPreferredWidth(120);
            }if(i==28){
                column.setPreferredWidth(120);
            }if(i==29){
                column.setPreferredWidth(120);
            }if(i==30){
                column.setPreferredWidth(120);
            }if(i==31){
                column.setPreferredWidth(120);
            }if(i==32){
                column.setPreferredWidth(120);
            }if(i==33){
                column.setPreferredWidth(120);
            }if(i==34){
                column.setPreferredWidth(120);
            }if(i==35){
                column.setPreferredWidth(120);
            }if(i==36){
                column.setPreferredWidth(120);
            }if(i==37){
                column.setPreferredWidth(120);
            }if(i==38){
                column.setPreferredWidth(120);
            }if(i==39){
                column.setPreferredWidth(120);
            }if(i==40){
                column.setPreferredWidth(120);
            }if(i==41){
                column.setPreferredWidth(120);
            }if(i==42){
                column.setPreferredWidth(120);
            }if(i==43){
                column.setPreferredWidth(120);
            }if(i==44){
                column.setPreferredWidth(120);
            }if(i==45){
                column.setPreferredWidth(120);
            }if(i==46){
                column.setPreferredWidth(120);
            }if(i==47){
                column.setPreferredWidth(120);
            }if(i==48){
                column.setPreferredWidth(120);
            }if(i==49){
                column.setPreferredWidth(120);
            }if(i==50){
                column.setPreferredWidth(120);
            }if(i==51){
                column.setPreferredWidth(120);
            }if(i==52){
                column.setPreferredWidth(120);
            }if(i==53){
                column.setPreferredWidth(120);
            }if(i==54){
                column.setPreferredWidth(120);
            }if(i==55){
                column.setPreferredWidth(120);
            }if(i==56){
                column.setPreferredWidth(120);
            }if(i==57){
                column.setPreferredWidth(120);
            }if(i==59){
                column.setPreferredWidth(120);
            }if(i==60){
                column.setPreferredWidth(120);
            }if(i==61){
                column.setPreferredWidth(120);
            }if(i==62){
                column.setPreferredWidth(120);
            }
        }

        tbPrainduksi.setDefaultRenderer(Object.class, new WarnaTable());
         petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(TabOK.getSelectedIndex()==0){
                    if(petugas.getTable().getSelectedRow()!= -1){     
                    KdDokter.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmDokter.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString()); 
                    }
                }else if(TabOK.getSelectedIndex()==1){
                    if(petugas.getTable().getSelectedRow()!= -1){     
                    KdPetugas1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString()); 
                }
                } else if(TabOK.getSelectedIndex()==2){
                    if(petugas.getTable().getSelectedRow()!= -1){     
                    KdDokter3.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmDokter3.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString()); 
                }
                }
                
                
                
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
         
         
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                 if(TabOK.getSelectedIndex()==0){
                    if(dokter.getTable().getSelectedRow()!= -1){      
                        KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());

                    }  
                  
                }
               
                    
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        jam();
        jam1();
        jam2();
        ChkAccor.setSelected(false);
        ChkAccor1.setSelected(false);
        ChkAccor2.setSelected(false);
        isPhoto();
        isPhoto1();
        isPhoto2();
        }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Popup = new javax.swing.JPopupMenu();
        pp2 = new javax.swing.JMenuItem();
        Popup1 = new javax.swing.JPopupMenu();
        pp1 = new javax.swing.JMenuItem();
        Popup2 = new javax.swing.JPopupMenu();
        ppCetakLembarFormulir = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        internalFrame1 = new widget.InternalFrame();
        panelisi1 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnKeluar = new widget.Button();
        TabOK = new javax.swing.JTabbedPane();
        internalFrame3 = new widget.InternalFrame();
        panelisi5 = new widget.panelisi();
        label9 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        label10 = new widget.Label();
        LCount = new widget.Label();
        scrollInput1 = new widget.ScrollPane();
        FormInput1 = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdDokter = new widget.TextBox();
        NmDokter = new widget.TextBox();
        BtnPetugas = new widget.Button();
        jLabel11 = new widget.Label();
        jSeparator5 = new javax.swing.JSeparator();
        Scroll6 = new widget.ScrollPane();
        tbMasalahKeperawatan = new widget.Table();
        jLabel17 = new widget.Label();
        Tanggal = new widget.Tanggal();
        Jam = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Detik = new widget.ComboBox();
        ChkJam1 = new widget.CekBox();
        jk = new widget.TextBox();
        label198 = new widget.Label();
        scrollPane1 = new widget.ScrollPane();
        data_obyektif = new widget.TextArea();
        scrollPane2 = new widget.ScrollPane();
        data_subyektif = new widget.TextArea();
        label201 = new widget.Label();
        label202 = new widget.Label();
        scrollPane3 = new widget.ScrollPane();
        diagnosa_pra_op = new widget.TextArea();
        label204 = new widget.Label();
        scrollPane4 = new widget.ScrollPane();
        deskripsi_penandaan = new widget.TextArea();
        PanelWall = new usu.widget.glass.PanelGlass();
        label219 = new widget.Label();
        scrollPane5 = new widget.ScrollPane();
        rencana_operasi = new widget.TextArea();
        label220 = new widget.Label();
        label221 = new widget.Label();
        label222 = new widget.Label();
        scrollPane6 = new widget.ScrollPane();
        posisi_pasien = new widget.TextArea();
        label223 = new widget.Label();
        BtnRiwayatSoap = new widget.Button();
        BtnRiwayatSoap1 = new widget.Button();
        CbPosisiPasien = new widget.ComboBox();
        label224 = new widget.Label();
        PanelAccor = new widget.PanelBiasa();
        ChkAccor = new widget.CekBox();
        TabData = new javax.swing.JTabbedPane();
        FormTelaah = new widget.PanelBiasa();
        FormPass3 = new widget.PanelBiasa();
        BtnRefreshPhoto1 = new widget.Button();
        Scroll5 = new widget.ScrollPane();
        tbChecklist = new widget.Table();
        internalFrame4 = new widget.InternalFrame();
        panelisi6 = new widget.panelisi();
        label11 = new widget.Label();
        TCari1 = new widget.TextBox();
        BtnCari1 = new widget.Button();
        BtnAll1 = new widget.Button();
        label12 = new widget.Label();
        LCount1 = new widget.Label();
        scrollInput2 = new widget.ScrollPane();
        FormInput2 = new widget.PanelBiasa();
        TNoRw2 = new widget.TextBox();
        TPasien2 = new widget.TextBox();
        TNoRM2 = new widget.TextBox();
        KdPetugas1 = new widget.TextBox();
        NmPetugas1 = new widget.TextBox();
        BtnPetugas1 = new widget.Button();
        jLabel12 = new widget.Label();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel18 = new widget.Label();
        Tanggal1 = new widget.Tanggal();
        Jam1 = new widget.ComboBox();
        Menit1 = new widget.ComboBox();
        Detik1 = new widget.ComboBox();
        ChkJam2 = new widget.CekBox();
        jk1 = new widget.TextBox();
        label18 = new widget.Label();
        jSeparator9 = new javax.swing.JSeparator();
        jSeparator10 = new javax.swing.JSeparator();
        jSeparator13 = new javax.swing.JSeparator();
        jSeparator14 = new javax.swing.JSeparator();
        jSeparator15 = new javax.swing.JSeparator();
        PanelAccor1 = new widget.PanelBiasa();
        ChkAccor1 = new widget.CekBox();
        TabData1 = new javax.swing.JTabbedPane();
        FormTelaah1 = new widget.PanelBiasa();
        FormPass4 = new widget.PanelBiasa();
        BtnRefreshPhoto2 = new widget.Button();
        Scroll8 = new widget.ScrollPane();
        tbPrasedasi = new widget.Table();
        internalFrame5 = new widget.InternalFrame();
        panelisi7 = new widget.panelisi();
        label13 = new widget.Label();
        TCari2 = new widget.TextBox();
        BtnCari2 = new widget.Button();
        BtnAll2 = new widget.Button();
        label16 = new widget.Label();
        LCount2 = new widget.Label();
        scrollInput3 = new widget.ScrollPane();
        FormInput3 = new widget.PanelBiasa();
        TNoRw3 = new widget.TextBox();
        TPasien3 = new widget.TextBox();
        TNoRM3 = new widget.TextBox();
        label17 = new widget.Label();
        KdDokter3 = new widget.TextBox();
        NmDokter3 = new widget.TextBox();
        BtnPetugas2 = new widget.Button();
        jLabel13 = new widget.Label();
        jSeparator11 = new javax.swing.JSeparator();
        jSeparator12 = new javax.swing.JSeparator();
        jLabel19 = new widget.Label();
        Tanggal2 = new widget.Tanggal();
        Jam2 = new widget.ComboBox();
        Menit2 = new widget.ComboBox();
        Detik2 = new widget.ComboBox();
        ChkJam3 = new widget.CekBox();
        jk2 = new widget.TextBox();
        jSeparator16 = new javax.swing.JSeparator();
        jSeparator17 = new javax.swing.JSeparator();
        jSeparator18 = new javax.swing.JSeparator();
        jSeparator19 = new javax.swing.JSeparator();
        PanelAccor2 = new widget.PanelBiasa();
        ChkAccor2 = new widget.CekBox();
        TabData2 = new javax.swing.JTabbedPane();
        FormTelaah2 = new widget.PanelBiasa();
        FormPass5 = new widget.PanelBiasa();
        BtnRefreshPhoto3 = new widget.Button();
        Scroll9 = new widget.ScrollPane();
        tbPrainduksi = new widget.Table();

        Popup.setName("Popup"); // NOI18N

        pp2.setBackground(new java.awt.Color(255, 255, 254));
        pp2.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        pp2.setForeground(new java.awt.Color(50, 50, 50));
        pp2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        pp2.setText("-");
        pp2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pp2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        pp2.setName("pp2"); // NOI18N
        pp2.setPreferredSize(new java.awt.Dimension(300, 25));
        pp2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pp2ActionPerformed(evt);
            }
        });
        Popup.add(pp2);

        Popup1.setName("Popup1"); // NOI18N

        pp1.setBackground(new java.awt.Color(255, 255, 254));
        pp1.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        pp1.setForeground(new java.awt.Color(50, 50, 50));
        pp1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        pp1.setText("Cetak Inta Operatif");
        pp1.setActionCommand("-");
        pp1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pp1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        pp1.setName("pp1"); // NOI18N
        pp1.setPreferredSize(new java.awt.Dimension(300, 25));
        pp1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pp1ActionPerformed(evt);
            }
        });
        Popup1.add(pp1);

        Popup2.setName("Popup2"); // NOI18N

        ppCetakLembarFormulir.setBackground(new java.awt.Color(255, 255, 254));
        ppCetakLembarFormulir.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppCetakLembarFormulir.setForeground(new java.awt.Color(50, 50, 50));
        ppCetakLembarFormulir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        ppCetakLembarFormulir.setText("Cetak Formulir");
        ppCetakLembarFormulir.setActionCommand("-");
        ppCetakLembarFormulir.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppCetakLembarFormulir.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppCetakLembarFormulir.setName("ppCetakLembarFormulir"); // NOI18N
        ppCetakLembarFormulir.setPreferredSize(new java.awt.Dimension(300, 25));
        ppCetakLembarFormulir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppCetakLembarFormulirActionPerformed(evt);
            }
        });
        Popup2.add(ppCetakLembarFormulir);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Asesmen Pra Bedah &  Verifikasi Operasi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi1.setName("panelisi1"); // NOI18N
        panelisi1.setPreferredSize(new java.awt.Dimension(100, 54));
        panelisi1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelisi1.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelisi1.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelisi1.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setIconTextGap(3);
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelisi1.add(BtnEdit);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelisi1.add(BtnKeluar);

        internalFrame1.add(panelisi1, java.awt.BorderLayout.PAGE_END);

        TabOK.setBackground(new java.awt.Color(255, 255, 254));
        TabOK.setForeground(new java.awt.Color(50, 50, 50));
        TabOK.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        TabOK.setName("TabOK"); // NOI18N
        TabOK.setPreferredSize(new java.awt.Dimension(452, 300));
        TabOK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabOKMouseClicked(evt);
            }
        });

        internalFrame3.setBackground(new java.awt.Color(255, 255, 255));
        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi5.setName("panelisi5"); // NOI18N
        panelisi5.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        label9.setText("Key Word :");
        label9.setName("label9"); // NOI18N
        label9.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi5.add(label9);

        TCari.setEditable(false);
        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(350, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelisi5.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('1');
        BtnCari.setToolTipText("Alt+1");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelisi5.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('2');
        BtnAll.setToolTipText("Alt+2");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelisi5.add(BtnAll);

        label10.setText("Record :");
        label10.setName("label10"); // NOI18N
        label10.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi5.add(label10);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi5.add(LCount);

        internalFrame3.add(panelisi5, java.awt.BorderLayout.PAGE_END);

        scrollInput1.setName("scrollInput1"); // NOI18N
        scrollInput1.setPreferredSize(new java.awt.Dimension(102, 200));

        FormInput1.setBackground(new java.awt.Color(255, 255, 255));
        FormInput1.setBorder(null);
        FormInput1.setName("FormInput1"); // NOI18N
        FormInput1.setPreferredSize(new java.awt.Dimension(870, 900));
        FormInput1.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput1.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput1.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput1.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Dokter :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput1.add(label14);
        label14.setBounds(0, 40, 70, 23);

        KdDokter.setEditable(false);
        KdDokter.setName("KdDokter"); // NOI18N
        KdDokter.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokterKeyPressed(evt);
            }
        });
        FormInput1.add(KdDokter);
        KdDokter.setBounds(74, 40, 130, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        NmDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput1.add(NmDokter);
        NmDokter.setBounds(206, 40, 360, 23);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas.setMnemonic('2');
        BtnPetugas.setToolTipText("Alt+2");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        FormInput1.add(BtnPetugas);
        BtnPetugas.setBounds(580, 40, 28, 23);

        jLabel11.setText("No.Rawat :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput1.add(jLabel11);
        jLabel11.setBounds(0, 10, 70, 23);

        jSeparator5.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator5.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator5.setName("jSeparator5"); // NOI18N
        FormInput1.add(jSeparator5);
        jSeparator5.setBounds(0, 70, 880, 1);

        Scroll6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll6.setName("Scroll6"); // NOI18N
        Scroll6.setOpaque(true);
        Scroll6.setPreferredSize(new java.awt.Dimension(452, 200));

        tbMasalahKeperawatan.setName("tbMasalahKeperawatan"); // NOI18N
        Scroll6.setViewportView(tbMasalahKeperawatan);

        FormInput1.add(Scroll6);
        Scroll6.setBounds(10, 1210, 400, 133);

        jLabel17.setText("Tanggal & Jam :");
        jLabel17.setName("jLabel17"); // NOI18N
        jLabel17.setVerifyInputWhenFocusTarget(false);
        FormInput1.add(jLabel17);
        jLabel17.setBounds(100, 80, 80, 23);

        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "26-02-2025" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput1.add(Tanggal);
        Tanggal.setBounds(190, 80, 110, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setName("Jam"); // NOI18N
        Jam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JamActionPerformed(evt);
            }
        });
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        FormInput1.add(Jam);
        Jam.setBounds(310, 80, 50, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        FormInput1.add(Menit);
        Menit.setBounds(370, 80, 50, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        FormInput1.add(Detik);
        Detik.setBounds(430, 80, 50, 23);

        ChkJam1.setBorder(null);
        ChkJam1.setSelected(true);
        ChkJam1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam1.setName("ChkJam1"); // NOI18N
        FormInput1.add(ChkJam1);
        ChkJam1.setBounds(480, 80, 23, 23);

        jk.setEditable(false);
        jk.setName("jk"); // NOI18N
        jk.setPreferredSize(new java.awt.Dimension(50, 23));
        jk.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jkMouseMoved(evt);
            }
        });
        jk.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jkMouseExited(evt);
            }
        });
        jk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jkActionPerformed(evt);
            }
        });
        jk.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jkKeyPressed(evt);
            }
        });
        FormInput1.add(jk);
        jk.setBounds(580, 10, 50, 23);

        label198.setText("Data obyektif ( pemeriksaan fisik ) :");
        label198.setName("label198"); // NOI18N
        label198.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label198);
        label198.setBounds(10, 190, 170, 23);

        scrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane1.setName("scrollPane1"); // NOI18N

        data_obyektif.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        data_obyektif.setColumns(20);
        data_obyektif.setRows(5);
        data_obyektif.setName("data_obyektif"); // NOI18N
        data_obyektif.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                data_obyektifKeyPressed(evt);
            }
        });
        scrollPane1.setViewportView(data_obyektif);

        FormInput1.add(scrollPane1);
        scrollPane1.setBounds(190, 190, 450, 60);

        scrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane2.setName("scrollPane2"); // NOI18N

        data_subyektif.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        data_subyektif.setColumns(20);
        data_subyektif.setRows(5);
        data_subyektif.setName("data_subyektif"); // NOI18N
        data_subyektif.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                data_subyektifKeyPressed(evt);
            }
        });
        scrollPane2.setViewportView(data_subyektif);

        FormInput1.add(scrollPane2);
        scrollPane2.setBounds(190, 110, 450, 60);

        label201.setText("Data Subyektif ( anamnesis ) :");
        label201.setName("label201"); // NOI18N
        label201.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label201);
        label201.setBounds(10, 110, 170, 23);

        label202.setText("Diagnosis Pra Operasi :");
        label202.setName("label202"); // NOI18N
        label202.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label202);
        label202.setBounds(10, 310, 170, 23);

        scrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane3.setName("scrollPane3"); // NOI18N

        diagnosa_pra_op.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        diagnosa_pra_op.setColumns(20);
        diagnosa_pra_op.setRows(5);
        diagnosa_pra_op.setName("diagnosa_pra_op"); // NOI18N
        diagnosa_pra_op.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                diagnosa_pra_opKeyPressed(evt);
            }
        });
        scrollPane3.setViewportView(diagnosa_pra_op);

        FormInput1.add(scrollPane3);
        scrollPane3.setBounds(190, 310, 450, 60);

        label204.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label204.setText("Berikan tanda pada gambar sesuai dengan penandaan lokasi operasi pada tubuh pasien");
        label204.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        label204.setName("label204"); // NOI18N
        label204.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label204);
        label204.setBounds(50, 520, 520, 23);

        scrollPane4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane4.setName("scrollPane4"); // NOI18N

        deskripsi_penandaan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        deskripsi_penandaan.setColumns(20);
        deskripsi_penandaan.setRows(5);
        deskripsi_penandaan.setName("deskripsi_penandaan"); // NOI18N
        deskripsi_penandaan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deskripsi_penandaanKeyPressed(evt);
            }
        });
        scrollPane4.setViewportView(deskripsi_penandaan);

        FormInput1.add(scrollPane4);
        scrollPane4.setBounds(450, 620, 320, 170);

        PanelWall.setBackground(new java.awt.Color(29, 29, 29));
        PanelWall.setBackgroundImage(new javax.swing.ImageIcon(getClass().getResource("/picture/img_anatomy_ok_verifikasi.png"))); // NOI18N
        PanelWall.setBackgroundImageType(usu.widget.constan.BackgroundConstan.BACKGROUND_IMAGE_STRECT);
        PanelWall.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelWall.setRound(false);
        PanelWall.setWarna(new java.awt.Color(110, 110, 110));
        PanelWall.setLayout(null);
        FormInput1.add(PanelWall);
        PanelWall.setBounds(60, 570, 360, 220);

        label219.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label219.setText("tubuh pasien");
        label219.setName("label219"); // NOI18N
        label219.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label219);
        label219.setBounds(450, 590, 320, 20);

        scrollPane5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane5.setName("scrollPane5"); // NOI18N

        rencana_operasi.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        rencana_operasi.setColumns(20);
        rencana_operasi.setRows(5);
        rencana_operasi.setName("rencana_operasi"); // NOI18N
        rencana_operasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rencana_operasiKeyPressed(evt);
            }
        });
        scrollPane5.setViewportView(rencana_operasi);

        FormInput1.add(scrollPane5);
        scrollPane5.setBounds(190, 400, 450, 60);

        label220.setText("  Depan                        Belakang              Sisi Kanan         Sisi Kiri           ");
        label220.setName("label220"); // NOI18N
        label220.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label220);
        label220.setBounds(90, 550, 340, 23);

        label221.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label221.setText("Deskripsi singkat apabila tidak dapat dilakukan penandaan pada");
        label221.setName("label221"); // NOI18N
        label221.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label221);
        label221.setBounds(450, 570, 320, 20);

        label222.setText("Lainnya :");
        label222.setName("label222"); // NOI18N
        label222.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label222);
        label222.setBounds(380, 810, 60, 23);

        scrollPane6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane6.setName("scrollPane6"); // NOI18N

        posisi_pasien.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        posisi_pasien.setColumns(20);
        posisi_pasien.setRows(5);
        posisi_pasien.setName("posisi_pasien"); // NOI18N
        posisi_pasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                posisi_pasienKeyPressed(evt);
            }
        });
        scrollPane6.setViewportView(posisi_pasien);

        FormInput1.add(scrollPane6);
        scrollPane6.setBounds(450, 810, 320, 60);

        label223.setText("Rencana Operasi :");
        label223.setName("label223"); // NOI18N
        label223.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label223);
        label223.setBounds(10, 400, 170, 23);

        BtnRiwayatSoap.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnRiwayatSoap.setMnemonic('2');
        BtnRiwayatSoap.setToolTipText("Alt+2");
        BtnRiwayatSoap.setName("BtnRiwayatSoap"); // NOI18N
        BtnRiwayatSoap.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnRiwayatSoap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRiwayatSoapActionPerformed(evt);
            }
        });
        BtnRiwayatSoap.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnRiwayatSoapKeyPressed(evt);
            }
        });
        FormInput1.add(BtnRiwayatSoap);
        BtnRiwayatSoap.setBounds(150, 220, 28, 23);

        BtnRiwayatSoap1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnRiwayatSoap1.setMnemonic('2');
        BtnRiwayatSoap1.setToolTipText("Alt+2");
        BtnRiwayatSoap1.setName("BtnRiwayatSoap1"); // NOI18N
        BtnRiwayatSoap1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnRiwayatSoap1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRiwayatSoap1ActionPerformed(evt);
            }
        });
        BtnRiwayatSoap1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnRiwayatSoap1KeyPressed(evt);
            }
        });
        FormInput1.add(BtnRiwayatSoap1);
        BtnRiwayatSoap1.setBounds(150, 140, 28, 23);

        CbPosisiPasien.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Supine", "Prone", "Litotomy", "Fowler", "Lateral", "Trendelenburg", "Lainnya" }));
        CbPosisiPasien.setName("CbPosisiPasien"); // NOI18N
        CbPosisiPasien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CbPosisiPasienActionPerformed(evt);
            }
        });
        CbPosisiPasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CbPosisiPasienKeyPressed(evt);
            }
        });
        FormInput1.add(CbPosisiPasien);
        CbPosisiPasien.setBounds(200, 810, 170, 23);

        label224.setText("Posisi pasien dalam operasi :");
        label224.setName("label224"); // NOI18N
        label224.setPreferredSize(new java.awt.Dimension(82, 120));
        FormInput1.add(label224);
        label224.setBounds(20, 810, 170, 23);

        scrollInput1.setViewportView(FormInput1);

        internalFrame3.add(scrollInput1, java.awt.BorderLayout.CENTER);

        PanelAccor.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor.setName("PanelAccor"); // NOI18N
        PanelAccor.setPreferredSize(new java.awt.Dimension(445, 43));
        PanelAccor.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setSelected(true);
        ChkAccor.setFocusable(false);
        ChkAccor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor.setName("ChkAccor"); // NOI18N
        ChkAccor.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccorActionPerformed(evt);
            }
        });
        PanelAccor.add(ChkAccor, java.awt.BorderLayout.WEST);

        TabData.setBackground(new java.awt.Color(254, 255, 254));
        TabData.setForeground(new java.awt.Color(50, 50, 50));
        TabData.setName("TabData"); // NOI18N
        TabData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabDataMouseClicked(evt);
            }
        });

        FormTelaah.setBackground(new java.awt.Color(255, 255, 255));
        FormTelaah.setBorder(null);
        FormTelaah.setName("FormTelaah"); // NOI18N
        FormTelaah.setPreferredSize(new java.awt.Dimension(115, 73));
        FormTelaah.setLayout(new java.awt.BorderLayout());

        FormPass3.setBackground(new java.awt.Color(255, 255, 255));
        FormPass3.setBorder(null);
        FormPass3.setName("FormPass3"); // NOI18N
        FormPass3.setPreferredSize(new java.awt.Dimension(115, 40));

        BtnRefreshPhoto1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto1.setMnemonic('U');
        BtnRefreshPhoto1.setText("Refresh");
        BtnRefreshPhoto1.setToolTipText("Alt+U");
        BtnRefreshPhoto1.setName("BtnRefreshPhoto1"); // NOI18N
        BtnRefreshPhoto1.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto1ActionPerformed(evt);
            }
        });
        FormPass3.add(BtnRefreshPhoto1);

        FormTelaah.add(FormPass3, java.awt.BorderLayout.PAGE_END);

        Scroll5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll5.setName("Scroll5"); // NOI18N
        Scroll5.setOpaque(true);
        Scroll5.setPreferredSize(new java.awt.Dimension(200, 200));

        tbChecklist.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbChecklist.setComponentPopupMenu(Popup2);
        tbChecklist.setName("tbChecklist"); // NOI18N
        tbChecklist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbChecklistMouseClicked(evt);
            }
        });
        tbChecklist.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbChecklistKeyPressed(evt);
            }
        });
        Scroll5.setViewportView(tbChecklist);

        FormTelaah.add(Scroll5, java.awt.BorderLayout.CENTER);

        TabData.addTab("Data Asesmen Pra Bedah & Verifikasi", FormTelaah);

        PanelAccor.add(TabData, java.awt.BorderLayout.CENTER);

        internalFrame3.add(PanelAccor, java.awt.BorderLayout.EAST);

        TabOK.addTab("PRA BEDAH & VERIFIKASI", internalFrame3);

        internalFrame4.setBackground(new java.awt.Color(255, 255, 255));
        internalFrame4.setBorder(null);
        internalFrame4.setName("internalFrame4"); // NOI18N
        internalFrame4.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi6.setName("panelisi6"); // NOI18N
        panelisi6.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        label11.setText("Key Word :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi6.add(label11);

        TCari1.setEditable(false);
        TCari1.setName("TCari1"); // NOI18N
        TCari1.setPreferredSize(new java.awt.Dimension(350, 23));
        TCari1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCari1KeyPressed(evt);
            }
        });
        panelisi6.add(TCari1);

        BtnCari1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari1.setMnemonic('1');
        BtnCari1.setToolTipText("Alt+1");
        BtnCari1.setName("BtnCari1"); // NOI18N
        BtnCari1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCari1ActionPerformed(evt);
            }
        });
        BtnCari1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCari1KeyPressed(evt);
            }
        });
        panelisi6.add(BtnCari1);

        BtnAll1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll1.setMnemonic('2');
        BtnAll1.setToolTipText("Alt+2");
        BtnAll1.setName("BtnAll1"); // NOI18N
        BtnAll1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAll1ActionPerformed(evt);
            }
        });
        BtnAll1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAll1KeyPressed(evt);
            }
        });
        panelisi6.add(BtnAll1);

        label12.setText("Record :");
        label12.setName("label12"); // NOI18N
        label12.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi6.add(label12);

        LCount1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount1.setText("0");
        LCount1.setName("LCount1"); // NOI18N
        LCount1.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi6.add(LCount1);

        internalFrame4.add(panelisi6, java.awt.BorderLayout.PAGE_END);

        scrollInput2.setName("scrollInput2"); // NOI18N
        scrollInput2.setPreferredSize(new java.awt.Dimension(102, 200));

        FormInput2.setBackground(new java.awt.Color(255, 255, 255));
        FormInput2.setBorder(null);
        FormInput2.setName("FormInput2"); // NOI18N
        FormInput2.setPreferredSize(new java.awt.Dimension(870, 1450));
        FormInput2.setLayout(null);

        TNoRw2.setHighlighter(null);
        TNoRw2.setName("TNoRw2"); // NOI18N
        TNoRw2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRw2KeyPressed(evt);
            }
        });
        FormInput2.add(TNoRw2);
        TNoRw2.setBounds(74, 10, 131, 23);

        TPasien2.setEditable(false);
        TPasien2.setHighlighter(null);
        TPasien2.setName("TPasien2"); // NOI18N
        FormInput2.add(TPasien2);
        TPasien2.setBounds(309, 10, 260, 23);

        TNoRM2.setEditable(false);
        TNoRM2.setHighlighter(null);
        TNoRM2.setName("TNoRM2"); // NOI18N
        FormInput2.add(TNoRM2);
        TNoRM2.setBounds(207, 10, 100, 23);

        KdPetugas1.setEditable(false);
        KdPetugas1.setName("KdPetugas1"); // NOI18N
        KdPetugas1.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugas1KeyPressed(evt);
            }
        });
        FormInput2.add(KdPetugas1);
        KdPetugas1.setBounds(74, 40, 100, 23);

        NmPetugas1.setEditable(false);
        NmPetugas1.setName("NmPetugas1"); // NOI18N
        NmPetugas1.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput2.add(NmPetugas1);
        NmPetugas1.setBounds(176, 40, 180, 23);

        BtnPetugas1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas1.setMnemonic('2');
        BtnPetugas1.setToolTipText("Alt+2");
        BtnPetugas1.setName("BtnPetugas1"); // NOI18N
        BtnPetugas1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugas1ActionPerformed(evt);
            }
        });
        BtnPetugas1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugas1KeyPressed(evt);
            }
        });
        FormInput2.add(BtnPetugas1);
        BtnPetugas1.setBounds(358, 40, 28, 23);

        jLabel12.setText("No.Rawat :");
        jLabel12.setName("jLabel12"); // NOI18N
        FormInput2.add(jLabel12);
        jLabel12.setBounds(0, 10, 70, 23);

        jSeparator7.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator7.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator7.setName("jSeparator7"); // NOI18N
        FormInput2.add(jSeparator7);
        jSeparator7.setBounds(0, 70, 880, 1);

        jSeparator8.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator8.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator8.setName("jSeparator8"); // NOI18N
        FormInput2.add(jSeparator8);
        jSeparator8.setBounds(0, 294, 880, 3);

        jLabel18.setText("Tanggal :");
        jLabel18.setName("jLabel18"); // NOI18N
        jLabel18.setVerifyInputWhenFocusTarget(false);
        FormInput2.add(jLabel18);
        jLabel18.setBounds(380, 40, 75, 23);

        Tanggal1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "26-02-2025" }));
        Tanggal1.setDisplayFormat("dd-MM-yyyy");
        Tanggal1.setName("Tanggal1"); // NOI18N
        Tanggal1.setOpaque(false);
        Tanggal1.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput2.add(Tanggal1);
        Tanggal1.setBounds(460, 40, 110, 23);

        Jam1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam1.setName("Jam1"); // NOI18N
        Jam1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam1ActionPerformed(evt);
            }
        });
        Jam1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam1KeyPressed(evt);
            }
        });
        FormInput2.add(Jam1);
        Jam1.setBounds(580, 40, 50, 23);

        Menit1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit1.setName("Menit1"); // NOI18N
        Menit1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit1KeyPressed(evt);
            }
        });
        FormInput2.add(Menit1);
        Menit1.setBounds(640, 40, 50, 23);

        Detik1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik1.setName("Detik1"); // NOI18N
        Detik1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik1KeyPressed(evt);
            }
        });
        FormInput2.add(Detik1);
        Detik1.setBounds(700, 40, 50, 23);

        ChkJam2.setBorder(null);
        ChkJam2.setSelected(true);
        ChkJam2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam2.setName("ChkJam2"); // NOI18N
        FormInput2.add(ChkJam2);
        ChkJam2.setBounds(750, 40, 23, 23);

        jk1.setEditable(false);
        jk1.setName("jk1"); // NOI18N
        jk1.setPreferredSize(new java.awt.Dimension(50, 23));
        jk1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jk1MouseMoved(evt);
            }
        });
        jk1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jk1MouseExited(evt);
            }
        });
        jk1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jk1ActionPerformed(evt);
            }
        });
        jk1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jk1KeyPressed(evt);
            }
        });
        FormInput2.add(jk1);
        jk1.setBounds(580, 10, 50, 23);

        label18.setText("Petugas :");
        label18.setName("label18"); // NOI18N
        label18.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput2.add(label18);
        label18.setBounds(0, 40, 70, 23);

        jSeparator9.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator9.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator9.setName("jSeparator9"); // NOI18N
        FormInput2.add(jSeparator9);
        jSeparator9.setBounds(0, 431, 880, 3);

        jSeparator10.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator10.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator10.setName("jSeparator10"); // NOI18N
        FormInput2.add(jSeparator10);
        jSeparator10.setBounds(0, 770, 880, 3);

        jSeparator13.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator13.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator13.setName("jSeparator13"); // NOI18N
        FormInput2.add(jSeparator13);
        jSeparator13.setBounds(0, 987, 880, 3);

        jSeparator14.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator14.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator14.setName("jSeparator14"); // NOI18N
        FormInput2.add(jSeparator14);
        jSeparator14.setBounds(0, 1050, 880, 3);

        jSeparator15.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator15.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator15.setName("jSeparator15"); // NOI18N
        FormInput2.add(jSeparator15);
        jSeparator15.setBounds(0, 1270, 880, 3);

        scrollInput2.setViewportView(FormInput2);

        internalFrame4.add(scrollInput2, java.awt.BorderLayout.CENTER);

        PanelAccor1.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor1.setName("PanelAccor1"); // NOI18N
        PanelAccor1.setPreferredSize(new java.awt.Dimension(445, 43));
        PanelAccor1.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor1.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor1.setSelected(true);
        ChkAccor1.setFocusable(false);
        ChkAccor1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor1.setName("ChkAccor1"); // NOI18N
        ChkAccor1.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor1.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor1.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccor1ActionPerformed(evt);
            }
        });
        PanelAccor1.add(ChkAccor1, java.awt.BorderLayout.WEST);

        TabData1.setBackground(new java.awt.Color(254, 255, 254));
        TabData1.setForeground(new java.awt.Color(50, 50, 50));
        TabData1.setName("TabData1"); // NOI18N
        TabData1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabData1MouseClicked(evt);
            }
        });

        FormTelaah1.setBackground(new java.awt.Color(255, 255, 255));
        FormTelaah1.setBorder(null);
        FormTelaah1.setName("FormTelaah1"); // NOI18N
        FormTelaah1.setPreferredSize(new java.awt.Dimension(115, 73));
        FormTelaah1.setLayout(new java.awt.BorderLayout());

        FormPass4.setBackground(new java.awt.Color(255, 255, 255));
        FormPass4.setBorder(null);
        FormPass4.setName("FormPass4"); // NOI18N
        FormPass4.setPreferredSize(new java.awt.Dimension(115, 40));

        BtnRefreshPhoto2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto2.setMnemonic('U');
        BtnRefreshPhoto2.setText("Refresh");
        BtnRefreshPhoto2.setToolTipText("Alt+U");
        BtnRefreshPhoto2.setName("BtnRefreshPhoto2"); // NOI18N
        BtnRefreshPhoto2.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto2ActionPerformed(evt);
            }
        });
        FormPass4.add(BtnRefreshPhoto2);

        FormTelaah1.add(FormPass4, java.awt.BorderLayout.PAGE_END);

        Scroll8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll8.setName("Scroll8"); // NOI18N
        Scroll8.setOpaque(true);
        Scroll8.setPreferredSize(new java.awt.Dimension(200, 200));

        tbPrasedasi.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbPrasedasi.setComponentPopupMenu(Popup2);
        tbPrasedasi.setName("tbPrasedasi"); // NOI18N
        tbPrasedasi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPrasedasiMouseClicked(evt);
            }
        });
        tbPrasedasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPrasedasiKeyPressed(evt);
            }
        });
        Scroll8.setViewportView(tbPrasedasi);

        FormTelaah1.add(Scroll8, java.awt.BorderLayout.CENTER);

        TabData1.addTab("-", FormTelaah1);

        PanelAccor1.add(TabData1, java.awt.BorderLayout.CENTER);

        internalFrame4.add(PanelAccor1, java.awt.BorderLayout.EAST);

        TabOK.addTab("", internalFrame4);

        internalFrame5.setBackground(new java.awt.Color(255, 255, 255));
        internalFrame5.setBorder(null);
        internalFrame5.setName("internalFrame5"); // NOI18N
        internalFrame5.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi7.setName("panelisi7"); // NOI18N
        panelisi7.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        label13.setText("Key Word :");
        label13.setName("label13"); // NOI18N
        label13.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi7.add(label13);

        TCari2.setEditable(false);
        TCari2.setName("TCari2"); // NOI18N
        TCari2.setPreferredSize(new java.awt.Dimension(350, 23));
        TCari2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCari2KeyPressed(evt);
            }
        });
        panelisi7.add(TCari2);

        BtnCari2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari2.setMnemonic('1');
        BtnCari2.setToolTipText("Alt+1");
        BtnCari2.setName("BtnCari2"); // NOI18N
        BtnCari2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCari2ActionPerformed(evt);
            }
        });
        BtnCari2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCari2KeyPressed(evt);
            }
        });
        panelisi7.add(BtnCari2);

        BtnAll2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll2.setMnemonic('2');
        BtnAll2.setToolTipText("Alt+2");
        BtnAll2.setName("BtnAll2"); // NOI18N
        BtnAll2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAll2ActionPerformed(evt);
            }
        });
        BtnAll2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAll2KeyPressed(evt);
            }
        });
        panelisi7.add(BtnAll2);

        label16.setText("Record :");
        label16.setName("label16"); // NOI18N
        label16.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi7.add(label16);

        LCount2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount2.setText("0");
        LCount2.setName("LCount2"); // NOI18N
        LCount2.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi7.add(LCount2);

        internalFrame5.add(panelisi7, java.awt.BorderLayout.PAGE_END);

        scrollInput3.setName("scrollInput3"); // NOI18N
        scrollInput3.setPreferredSize(new java.awt.Dimension(102, 200));

        FormInput3.setBackground(new java.awt.Color(255, 255, 255));
        FormInput3.setBorder(null);
        FormInput3.setName("FormInput3"); // NOI18N
        FormInput3.setPreferredSize(new java.awt.Dimension(870, 1000));
        FormInput3.setLayout(null);

        TNoRw3.setHighlighter(null);
        TNoRw3.setName("TNoRw3"); // NOI18N
        TNoRw3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRw3KeyPressed(evt);
            }
        });
        FormInput3.add(TNoRw3);
        TNoRw3.setBounds(74, 10, 131, 23);

        TPasien3.setEditable(false);
        TPasien3.setHighlighter(null);
        TPasien3.setName("TPasien3"); // NOI18N
        FormInput3.add(TPasien3);
        TPasien3.setBounds(309, 10, 260, 23);

        TNoRM3.setEditable(false);
        TNoRM3.setHighlighter(null);
        TNoRM3.setName("TNoRM3"); // NOI18N
        FormInput3.add(TNoRM3);
        TNoRM3.setBounds(207, 10, 100, 23);

        label17.setText("Dokter Anestesi :");
        label17.setName("label17"); // NOI18N
        label17.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput3.add(label17);
        label17.setBounds(0, 40, 90, 23);

        KdDokter3.setEditable(false);
        KdDokter3.setName("KdDokter3"); // NOI18N
        KdDokter3.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDokter3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokter3KeyPressed(evt);
            }
        });
        FormInput3.add(KdDokter3);
        KdDokter3.setBounds(100, 40, 100, 23);

        NmDokter3.setEditable(false);
        NmDokter3.setName("NmDokter3"); // NOI18N
        NmDokter3.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput3.add(NmDokter3);
        NmDokter3.setBounds(200, 40, 180, 23);

        BtnPetugas2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas2.setMnemonic('2');
        BtnPetugas2.setToolTipText("Alt+2");
        BtnPetugas2.setName("BtnPetugas2"); // NOI18N
        BtnPetugas2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugas2ActionPerformed(evt);
            }
        });
        BtnPetugas2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugas2KeyPressed(evt);
            }
        });
        FormInput3.add(BtnPetugas2);
        BtnPetugas2.setBounds(380, 40, 28, 23);

        jLabel13.setText("No.Rawat :");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput3.add(jLabel13);
        jLabel13.setBounds(0, 10, 70, 23);

        jSeparator11.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator11.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator11.setName("jSeparator11"); // NOI18N
        FormInput3.add(jSeparator11);
        jSeparator11.setBounds(0, 70, 880, 1);

        jSeparator12.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator12.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator12.setName("jSeparator12"); // NOI18N
        FormInput3.add(jSeparator12);
        jSeparator12.setBounds(0, 211, 880, 3);

        jLabel19.setText("Tanggal :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setVerifyInputWhenFocusTarget(false);
        FormInput3.add(jLabel19);
        jLabel19.setBounds(380, 40, 75, 23);

        Tanggal2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "26-02-2025" }));
        Tanggal2.setDisplayFormat("dd-MM-yyyy");
        Tanggal2.setName("Tanggal2"); // NOI18N
        Tanggal2.setOpaque(false);
        Tanggal2.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput3.add(Tanggal2);
        Tanggal2.setBounds(460, 40, 110, 23);

        Jam2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam2.setName("Jam2"); // NOI18N
        Jam2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam2ActionPerformed(evt);
            }
        });
        Jam2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam2KeyPressed(evt);
            }
        });
        FormInput3.add(Jam2);
        Jam2.setBounds(580, 40, 50, 23);

        Menit2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit2.setName("Menit2"); // NOI18N
        Menit2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit2KeyPressed(evt);
            }
        });
        FormInput3.add(Menit2);
        Menit2.setBounds(640, 40, 50, 23);

        Detik2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik2.setName("Detik2"); // NOI18N
        Detik2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik2KeyPressed(evt);
            }
        });
        FormInput3.add(Detik2);
        Detik2.setBounds(700, 40, 50, 23);

        ChkJam3.setBorder(null);
        ChkJam3.setSelected(true);
        ChkJam3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam3.setName("ChkJam3"); // NOI18N
        FormInput3.add(ChkJam3);
        ChkJam3.setBounds(750, 40, 23, 23);

        jk2.setEditable(false);
        jk2.setName("jk2"); // NOI18N
        jk2.setPreferredSize(new java.awt.Dimension(50, 23));
        jk2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jk2MouseMoved(evt);
            }
        });
        jk2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jk2MouseExited(evt);
            }
        });
        jk2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jk2ActionPerformed(evt);
            }
        });
        jk2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jk2KeyPressed(evt);
            }
        });
        FormInput3.add(jk2);
        jk2.setBounds(580, 10, 50, 23);

        jSeparator16.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator16.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator16.setName("jSeparator16"); // NOI18N
        FormInput3.add(jSeparator16);
        jSeparator16.setBounds(0, 364, 880, 3);

        jSeparator17.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator17.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator17.setName("jSeparator17"); // NOI18N
        FormInput3.add(jSeparator17);
        jSeparator17.setBounds(0, 397, 880, 3);

        jSeparator18.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator18.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator18.setName("jSeparator18"); // NOI18N
        FormInput3.add(jSeparator18);
        jSeparator18.setBounds(0, 447, 880, 3);

        jSeparator19.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator19.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator19.setName("jSeparator19"); // NOI18N
        FormInput3.add(jSeparator19);
        jSeparator19.setBounds(0, 494, 880, 3);

        scrollInput3.setViewportView(FormInput3);

        internalFrame5.add(scrollInput3, java.awt.BorderLayout.CENTER);

        PanelAccor2.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor2.setName("PanelAccor2"); // NOI18N
        PanelAccor2.setPreferredSize(new java.awt.Dimension(445, 43));
        PanelAccor2.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor2.setBackground(new java.awt.Color(255, 250, 248));
        ChkAccor2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor2.setSelected(true);
        ChkAccor2.setFocusable(false);
        ChkAccor2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor2.setName("ChkAccor2"); // NOI18N
        ChkAccor2.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor2.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor2.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor2.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccor2ActionPerformed(evt);
            }
        });
        PanelAccor2.add(ChkAccor2, java.awt.BorderLayout.WEST);

        TabData2.setBackground(new java.awt.Color(254, 255, 254));
        TabData2.setForeground(new java.awt.Color(50, 50, 50));
        TabData2.setName("TabData2"); // NOI18N
        TabData2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabData2MouseClicked(evt);
            }
        });

        FormTelaah2.setBackground(new java.awt.Color(255, 255, 255));
        FormTelaah2.setBorder(null);
        FormTelaah2.setName("FormTelaah2"); // NOI18N
        FormTelaah2.setPreferredSize(new java.awt.Dimension(115, 73));
        FormTelaah2.setLayout(new java.awt.BorderLayout());

        FormPass5.setBackground(new java.awt.Color(255, 255, 255));
        FormPass5.setBorder(null);
        FormPass5.setName("FormPass5"); // NOI18N
        FormPass5.setPreferredSize(new java.awt.Dimension(115, 40));

        BtnRefreshPhoto3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto3.setMnemonic('U');
        BtnRefreshPhoto3.setText("Refresh");
        BtnRefreshPhoto3.setToolTipText("Alt+U");
        BtnRefreshPhoto3.setName("BtnRefreshPhoto3"); // NOI18N
        BtnRefreshPhoto3.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto3ActionPerformed(evt);
            }
        });
        FormPass5.add(BtnRefreshPhoto3);

        FormTelaah2.add(FormPass5, java.awt.BorderLayout.PAGE_END);

        Scroll9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll9.setName("Scroll9"); // NOI18N
        Scroll9.setOpaque(true);
        Scroll9.setPreferredSize(new java.awt.Dimension(200, 200));
        Scroll9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Scroll9KeyPressed(evt);
            }
        });

        tbPrainduksi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbPrainduksi.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbPrainduksi.setComponentPopupMenu(Popup2);
        tbPrainduksi.setName("tbPrainduksi"); // NOI18N
        tbPrainduksi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPrainduksiMouseClicked(evt);
            }
        });
        tbPrainduksi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPrainduksiKeyPressed(evt);
            }
        });
        Scroll9.setViewportView(tbPrainduksi);

        FormTelaah2.add(Scroll9, java.awt.BorderLayout.CENTER);

        TabData2.addTab("-", FormTelaah2);

        PanelAccor2.add(TabData2, java.awt.BorderLayout.CENTER);

        internalFrame5.add(PanelAccor2, java.awt.BorderLayout.EAST);

        TabOK.addTab("", internalFrame5);

        internalFrame1.add(TabOK, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(TNoRw.getText().trim().equals("")){
                Valid.textKosong(TNoRw,"Silahkan pilih data pasien terlebih dahulu !");
            }else{         
                if(Sequel.menyimpantf("penilaian_awal_ok_asuhan_prabedah","?,?,?,?,?,?,?,?,?,?,?","Asuhan Pra Bedah",11,new String[]{
                        TNoRw.getText(),
                        Valid.SetTgl(Tanggal.getSelectedItem()+""),
                        Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                        KdDokter.getText(),
                        data_subyektif.getText(),
                        data_obyektif.getText(),
                        diagnosa_pra_op.getText(),
                        rencana_operasi.getText(),
                        deskripsi_penandaan.getText(),
                        CbPosisiPasien.getSelectedItem().toString(),
                        posisi_pasien.getText(),    
                })==true){
                    tampil();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Menyimpan Data");
            }
               
      }
        }else if(TabOK.getSelectedIndex()==1){
            if(TNoRw2.getText().trim().equals("")){
                Valid.textKosong(TNoRw2,"Silahkan pilih data pasien terlebih dahulu !");
            }else{ 
                if(Sequel.menyimpantf("penilaian_awal_anestesi_prasedasi","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Pra Sedasi",60,new String[]{
                        TNoRw2.getText(),
                        Valid.SetTgl(Tanggal1.getSelectedItem()+""),
                        Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                        KdPetugas1.getText(),
                   
                })==true){
                    tampilprasedasi();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Menyimpan Data");
            }  
          }
        }else if(TabOK.getSelectedIndex()==2){
           if(TNoRw3.getText().trim().equals("")){
                Valid.textKosong(TNoRw3,"Silahkan pilih data pasien terlebih dahulu !");
           }else{    
                if(Sequel.menyimpantf("penilaian_awal_anestesi_prainduksi","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Awal Pra Induksi",60,new String[]{
                        TNoRw3.getText(),
                        Valid.SetTgl(Tanggal2.getSelectedItem()+""),
                        Jam2.getSelectedItem()+":"+Menit2.getSelectedItem()+":"+Detik2.getSelectedItem(),
                        KdDokter3.getText(),
                      
                        
                })==true){
                    tampilprainduksi();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Menyimpan Data Pra Induksi");
            }  
          }
        }else if(TabOK.getSelectedIndex()==3){
           
       
        }            
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            if(TabOK.getSelectedIndex()==0){
                Valid.pindah(evt,TNoRw,BtnBatal);
            }else if(TabOK.getSelectedIndex()==1){
                Valid.pindah(evt,TNoRw,BtnBatal);
            }else if(TabOK.getSelectedIndex()==2){
                Valid.pindah(evt,TNoRw,BtnBatal);
            }else if(TabOK.getSelectedIndex()==3){
                Valid.pindah(evt,TNoRw,BtnBatal);
            }                
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(tabModePraBedah.getRowCount()!=0){
                if(tbChecklist.getSelectedRow()>-1){
                    if(Sequel.queryu2tf("delete from penilaian_awal_ok_asuhan_prabedah where no_rawat=?",1,new String[]{
                    tbChecklist.getValueAt(tbChecklist.getSelectedRow(),0).toString()
                })==true){
                    tampil();
                    emptTeks();
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Asuhan Pra Bedah terlebih dahulu..!!");
            }   
        }  
           
        }else if(TabOK.getSelectedIndex()==1){
           if(tabModeInOp.getRowCount()!=0){
                if(tbPrasedasi.getSelectedRow()>-1){
                    if(Sequel.queryu2tf("delete from penilaian_awal_anestesi_prasedasi where no_rawat=? and tanggal=? and jam =?",3,new String[]{
                    tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),0).toString(),
                    tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),1).toString(),
                    tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString(),
                })==true){
                    tampilprasedasi();
                    emptTeks();
                    
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Asuhan Pra Sedasi terlebih dahulu..!!");
            }   
        }  
        }else if(TabOK.getSelectedIndex()==2){
             if(tabModePraInduksi.getRowCount()!=0){
                if(tbPrainduksi.getSelectedRow()>-1){
                    if(Sequel.queryu2tf("delete from penilaian_awal_anestesi_prainduksi where no_rawat=?",1,new String[]{
                    tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),0).toString()
                })==true){
                    tampilprainduksi();
                    emptTeks();
                    
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Asuhan Pra Induksi terlebih dahulu..!!");
            }   
            } 
        }else if(TabOK.getSelectedIndex()==3){
         
        }             
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnHapus);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
    dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            dispose();
        }else{Valid.pindah(evt,BtnHapus,BtnKeluar);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
//        TabSettingMouseClicked(null);
    }//GEN-LAST:event_formWindowOpened

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TabOK.getSelectedIndex()==0){
            if(TNoRw.getText().trim().equals("")){
                Valid.textKosong(TNoRw,"Nomor Rawat");
            }else if(TNoRM.getText().trim().equals("")){
                Valid.textKosong(TNoRM,"Nomor RM");
            }else if(TPasien.getText().trim().equals("")){
                Valid.textKosong(TPasien,"Pasien");
            }else{
                if(tbChecklist.getSelectedRow()>-1){
                        if(Sequel.mengedittf("penilaian_awal_ok_asuhan_prabedah","no_rawat=?","no_rawat=?, tanggal=?, jam=?, kd_dokter=?, data_subyektif=?, data_obyektif=?, diagnosis_pra_operasi=?,rencana_operasi=?, deskripsi_penandaan=?,posisi_pasien=?,posisi_pasien_lainnya=?",12,new String[]{
                                TNoRw.getText(),
                                Valid.SetTgl(Tanggal.getSelectedItem()+""),
                                Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                                KdDokter.getText(),
                                data_subyektif.getText(),
                                data_obyektif.getText(),
                                diagnosa_pra_op.getText(),
                                rencana_operasi.getText(),
                                deskripsi_penandaan.getText(),
                                CbPosisiPasien.getSelectedItem().toString(),
                                posisi_pasien.getText(),    
                                tbChecklist.getValueAt(tbChecklist.getSelectedRow(),0).toString()
                       })==true){
                           JOptionPane.showMessageDialog(rootPane,"Berhasil Mengedit Data Asuhan Pra Bedah..!!");
                           tampil();
                           emptTeks();
                   }
                
                }else{
                    JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Checklist terlebih dahulu..!!");
                }
                tampil();
                emptTeks();
            }
        }else if(TabOK.getSelectedIndex()==1){            
            if(TNoRw2.getText().trim().equals("")){
                Valid.textKosong(TNoRw2,"Silahkan pilih data pasien terlebih dahulu !");
            }else{ 
                if(tbPrasedasi.getSelectedRow()>-1){
                        if(Sequel.mengedittf("penilaian_awal_anestesi_prasedasi","no_rawat=?","no_rawat=?,tanggal=?,jam=?,kd_petugas=?,anamnesis=?,diagnosa_preop=?,rencana_operasi=?,tb=?,bb=?,obat_dikonsumsi=?,obat_dikonsumsi_ket=?,riwayat_alergi=?,riwayat_penyakit=?,riwayat_anestesi=?,jenis_anestesi=?,riwayat_merokok=?,komplikasi_anestesi=?,fisik_b1=?,fisik_alat=?,fisik_rr=?,fisik_vesikuler=?,fisik_rhonki=?,fisik_wheezing=?,fisik_td=?,fisik_hr=?,fisik_hr_ket=?,fisik_konjungtiva=?,fisik_gcse=?,fisik_gcsm=?,fisik_gcsv=?,fisik_pupil=?,fisik_hemiparese=?,fisik_urin=?,fisik_warnaurin=?,fisik_perut=?,fisik_diare=?,fisik_muntah=?,fisik_alatbantu=?,fisik_fraktur=?,penunjang_lab=?,penunjang_rad=?,penunjang_elektro=?,asa=?,asa1=?,asa2=?,asa3=?,asa4=?,asa5=?,asa6=?,asaE=?,rencana_ga= ?, rencana_reg = ?, rencana_blok = ?, rencana_alat_khusus=?,monitoring_khusus=?,rencana_perawatan_inap=?,rencana_hcu=?,rencana_icu=?,rencana_rajal=?,rencana_igd=?",61,new String[]{
                                TNoRw2.getText(),
                                Valid.SetTgl(Tanggal1.getSelectedItem()+""),
                                Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                                KdPetugas1.getText(),
                             
                                tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),0).toString()
                       })==true){
                           JOptionPane.showMessageDialog(rootPane,"Berhasil Mengedit Pra Sedasi..!!");
                           tampilprasedasi();
                           emptTeks();
                   }             
                }else{
                    JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data Pra Sedasi terlebih dahulu..!!");
                }
                tampilprasedasi();
                emptTeks();
                 
         }
          
        }else if(TabOK.getSelectedIndex()==2){
            if(TNoRw.getText().trim().equals("")){
                Valid.textKosong(TNoRw,"Nomor Rawat");
            }else if(TNoRM.getText().trim().equals("")){
                Valid.textKosong(TNoRM,"Nomor RM");
            }else if(TPasien.getText().trim().equals("")){
                Valid.textKosong(TPasien,"Pasien");
            }else{
                 if(Sequel.mengedittf("penilaian_awal_anestesi_prainduksi","no_rawat=?","no_rawat=?, tanggal=?, jam=?, kd_dokter=?, alergi=?, bb=?, td=?, nadi=?, tb=?, respirasi=?, gol_darah=?, suhu=?, gcs=?, rh=?, hb=?, vas=?, ht=?, lain=?, fisik_normal=?, fisik_bukamulut=?, fisik_jarak=?, fisik_mallampati=?, fisik_gerakanleher=?, fisik_abnormal=?, anamnesis_auto=?, anamnesis_allo=?, asa1=?, asa2=?, asa3=?, asa4=?, asa5=?, asaE=?, penyulit_praanestesi=?, cek_1=?, cek_2=?, cek_3=?, cek_4=?, cek_5=?, ga_1=?, ga_2=?, ga_3=?, ga_4=?, ga_5=?, reg_1=?, reg_2=?, reg_3=?, reg_4=?, reg_5=?, anestesi_lain=?, monitoring_1=?, monitoring_2=?, monitoring_3=?, monitoring_3_ket=?, monitoring_4=?, monitoring_4_ket=?, monitoring_5=?, monitoring_6=?, monitoring_7=?, monitoring_8=?, monitoring_9=? ",61,new String[]{
                        TNoRw2.getText(),
                        Valid.SetTgl(Tanggal2.getSelectedItem()+""),
                        Jam2.getSelectedItem()+":"+Menit2.getSelectedItem()+":"+Detik2.getSelectedItem(),
                        KdDokter3.getText(),
                     
                        tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),0).toString()
                })==true){
                    tampilprainduksi();
                    emptTeks();
                    JOptionPane.showMessageDialog(rootPane, "Berhasil Megubah Data");
                }  
            }
        }else if(TabOK.getSelectedIndex()==3){
           
        
        }              
    }//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnKeluar);
        }
    }//GEN-LAST:event_BtnEditKeyPressed

    private void pp2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pp2ActionPerformed
             
    }//GEN-LAST:event_pp2ActionPerformed

    private void TabOKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabOKMouseClicked
        if(TabOK.getSelectedIndex()==0){
            tampil();
        }else if(TabOK.getSelectedIndex()==1){
            tampil();
        }else if(TabOK.getSelectedIndex()==2){
            tampil();
        }else if(TabOK.getSelectedIndex()==3){
            tampil();
        }
    }//GEN-LAST:event_TabOKMouseClicked

    private void pp1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pp1ActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        for (int i = 0; i < tbChecklist.getRowCount(); i++) {             
                                     
        }            
        this.setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_pp1ActionPerformed

    private void ppCetakLembarFormulirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppCetakLembarFormulirActionPerformed
       if(tabModePraBedah.getRowCount()!=0){
                   try {
                      int row=tbChecklist.getSelectedRow();
                        if(row!= -1){
                                  if(tbChecklist.getSelectedRow()>-1){
                                        Map<String, Object> param = new HashMap<>();
                                        param.put("namars",akses.getnamars());
                                        param.put("alamatrs",akses.getalamatrs());
                                        param.put("kotars",akses.getkabupatenrs());
                                        param.put("propinsirs",akses.getpropinsirs());
                                        param.put("kontakrs",akses.getkontakrs());
                                        param.put("emailrs",akses.getemailrs());
                                        param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                                        param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                                        param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                                        param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                                        param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                                        param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                                        param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                                        param.put("rme_asesment_prabedah",Sequel.cariGambar("select rme_asesment_prabedah from gambar"));
                                        finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbChecklist.getValueAt(tbChecklist.getSelectedRow(),13).toString());
                                        param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbChecklist.getValueAt(tbChecklist.getSelectedRow(),14).toString()+"\nID "+(finger.equals("")?tbChecklist.getValueAt(tbChecklist.getSelectedRow(),13).toString():finger)+"\n"+tbChecklist.getValueAt(tbChecklist.getSelectedRow(),1).toString()+" "+tbChecklist.getValueAt(tbChecklist.getSelectedRow(),2).toString());  
                                        Valid.MyReportqry("rptCetakPenilaianOKPrabedah.jasper","report","::[ Formulir Penilaian Asuhan Prabedah & Verifikasi]::",
                                                "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                                                "penilaian_awal_ok_asuhan_prabedah.*,dokter.kd_dokter,dokter.nm_dokter "+
                                                "from penilaian_awal_ok_asuhan_prabedah inner join reg_periksa on penilaian_awal_ok_asuhan_prabedah.no_rawat=reg_periksa.no_rawat "+
                                                "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                                                "inner join dokter on dokter.kd_dokter=penilaian_awal_ok_asuhan_prabedah.kd_dokter "+
                                                "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                                                "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                                                "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                                                "where penilaian_awal_ok_asuhan_prabedah.no_rawat='"+tbChecklist.getValueAt(tbChecklist.getSelectedRow(),0).toString()+"'",param);
                                    }
                        }
                    } catch (java.lang.NullPointerException e) {
                   }
      }else if(tabModeInOp.getRowCount()!=0){
                 try {
                      int row=tbPrasedasi.getSelectedRow();
                        if(row!= -1){
                                  if(tbPrasedasi.getSelectedRow()>-1){
                                        Map<String, Object> param = new HashMap<>();
                                        param.put("namars",akses.getnamars());
                                        param.put("alamatrs",akses.getalamatrs());
                                        param.put("kotars",akses.getkabupatenrs());
                                        param.put("propinsirs",akses.getpropinsirs());
                                        param.put("kontakrs",akses.getkontakrs());
                                        param.put("emailrs",akses.getemailrs());
                                        param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                                        param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                                        param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                                        param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                                        param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                                        param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                                        param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                                        finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),3).toString());
                                        param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),4).toString()+"\nID "+(finger.equals("")?tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),4).toString():finger)+"\n"+tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),1).toString()+" "+tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString());  
                                        ruang_rawat=Sequel.cariIsi("SELECT bangsal.nm_bangsal FROM kamar_inap INNER JOIN kamar ON kamar_inap.kd_kamar = kamar.kd_kamar INNER JOIN bangsal ON bangsal.kd_bangsal = kamar.kd_bangsal WHERE kamar_inap.no_rawat = ? order by kamar_inap.tgl_masuk desc, kamar_inap.jam_masuk desc limit 1",tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),0).toString());
                                        param.put("ruang_rawat", ruang_rawat);
                                        Valid.MyReportqry("rptCetakPenilaianPrasedasi.jasper","report","::[ Formulir Pra Sedasi ]::",
                                                "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                                                "penilaian_awal_anestesi_prasedasi.*,petugas.nip,petugas.nama "+
                                                "from penilaian_awal_anestesi_prasedasi inner join reg_periksa on penilaian_awal_anestesi_prasedasi.no_rawat=reg_periksa.no_rawat "+
                                                "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                                                "inner join petugas on petugas.nip=penilaian_awal_anestesi_prasedasi.kd_petugas "+
                                                "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                                                "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                                                "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                                                "where penilaian_awal_anestesi_prasedasi.no_rawat='"+tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),0).toString()+"'",param);
                                    }
                        }
                    } catch (java.lang.NullPointerException e) {
                   }
      }else if(tabModePraInduksi.getRowCount()!=0){
                 try {
                      int row=tbPrainduksi.getSelectedRow();
                        if(row!= -1){
                                  if(tbPrainduksi.getSelectedRow()>-1){
                                        Map<String, Object> param = new HashMap<>();
                                        param.put("namars",akses.getnamars());
                                        param.put("alamatrs",akses.getalamatrs());
                                        param.put("kotars",akses.getkabupatenrs());
                                        param.put("propinsirs",akses.getpropinsirs());
                                        param.put("kontakrs",akses.getkontakrs());
                                        param.put("emailrs",akses.getemailrs());
                                        param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                                        param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                                        param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                                        param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                                        param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                                        param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                                        param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                                        finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),3).toString());
                                        param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),4).toString()+"\nID "+(finger.equals("")?tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),3).toString():finger)+"\n"+tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),1).toString()+" "+tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),2).toString());  
                                        ruang_rawat=Sequel.cariIsi("SELECT bangsal.nm_bangsal FROM kamar_inap INNER JOIN kamar ON kamar_inap.kd_kamar = kamar.kd_kamar INNER JOIN bangsal ON bangsal.kd_bangsal = kamar.kd_bangsal WHERE kamar_inap.no_rawat = ? order by kamar_inap.tgl_masuk desc, kamar_inap.jam_masuk desc limit 1",tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),0).toString());
                                        param.put("ruang_rawat", ruang_rawat);
                                        Valid.MyReportqry("rptCetakPenilaianPrainduksi.jasper","report","::[ Formulir Pra Induksi ]::",
                                                "select reg_periksa.no_rawat,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                                                "penilaian_awal_anestesi_prainduksi.*,dokter.kd_dokter,dokter.nm_dokter "+
                                                "from penilaian_awal_anestesi_prainduksi inner join reg_periksa on penilaian_awal_anestesi_prainduksi.no_rawat=reg_periksa.no_rawat "+
                                                "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                                                "inner join dokter on dokter.kd_dokter=penilaian_awal_anestesi_prainduksi.kd_dokter "+
                                                "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                                                "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                                                "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                                                "where penilaian_awal_anestesi_prainduksi.no_rawat='"+tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),0).toString()+"'",param);
                                    }
                        }
                    } catch (java.lang.NullPointerException e) {
                }
      }  
       
    }//GEN-LAST:event_ppCetakLembarFormulirActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnCari, TCari);
        }
    }//GEN-LAST:event_BtnAllKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
    }//GEN-LAST:event_BtnAllActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
    }//GEN-LAST:event_BtnCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
    }//GEN-LAST:event_BtnCariActionPerformed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCariKeyPressed

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
     
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed

        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
      dokter.setVisible(true);
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void KdDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokterKeyPressed

    }//GEN-LAST:event_KdDokterKeyPressed

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
          
        }else{
            Valid.pindah(evt,TCari,BtnPetugas);
        }
    }//GEN-LAST:event_TNoRwKeyPressed

    private void JamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JamActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamActionPerformed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MenitKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DetikKeyPressed

    private void ChkAccorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccorActionPerformed
        if(!(TNoRw.getText().equals(""))){
            if(TNoRw.getText().equals("")){
                Valid.textKosong(TCari,"Pasien");
            }else{
                isPhoto();
                TabDataMouseClicked(null);
            }
        }else{
            ChkAccor.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih Pasien..!!!");
        }
    }//GEN-LAST:event_ChkAccorActionPerformed

    private void BtnRefreshPhoto1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto1ActionPerformed
        tampil();
    }//GEN-LAST:event_BtnRefreshPhoto1ActionPerformed

    private void TabDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabDataMouseClicked
        if(TabData.getSelectedIndex()==0){
           
        }else if(TabData.getSelectedIndex()==1){
           
        }
    }//GEN-LAST:event_TabDataMouseClicked

    private void tbChecklistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbChecklistMouseClicked
      if(tabModePraBedah.getRowCount()!=0){
                   try {
                       getData();
                   } catch (java.lang.NullPointerException e) {
                   }
      }  
    }//GEN-LAST:event_tbChecklistMouseClicked

    private void tbChecklistKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbChecklistKeyPressed
               if(tabModePraBedah.getRowCount()!=0){
                   try {
                       getData();
                   } catch (java.lang.NullPointerException e) {
                   }
               }     
    }//GEN-LAST:event_tbChecklistKeyPressed

    private void BtnAll1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAll1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnCari, TCari);
        }
    }//GEN-LAST:event_BtnAll1KeyPressed

    private void BtnAll1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAll1ActionPerformed
        TCari1.setText("");
        tampil();
    }//GEN-LAST:event_BtnAll1ActionPerformed

    private void BtnCari1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCari1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
    }//GEN-LAST:event_BtnCari1KeyPressed

    private void BtnCari1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCari1ActionPerformed
        tampil();
    }//GEN-LAST:event_BtnCari1ActionPerformed

    private void TCari1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCari1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCari1ActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari1.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCari1KeyPressed

    private void TNoRw2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRw2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TNoRw2KeyPressed

    private void KdPetugas1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugas1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdPetugas1KeyPressed

    private void BtnPetugas1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugas1ActionPerformed
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnPetugas1ActionPerformed

    private void BtnPetugas1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugas1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugas1KeyPressed

    private void Jam1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1ActionPerformed

    private void Jam1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1KeyPressed

    private void Menit1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit1KeyPressed

    private void Detik1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik1KeyPressed

    private void jk1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk1MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1MouseMoved

    private void jk1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1MouseExited

    private void jk1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jk1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1ActionPerformed

    private void jk1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jk1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk1KeyPressed

    private void ChkAccor1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccor1ActionPerformed
     if(!(TNoRw.getText().equals(""))){
            if(TNoRw.getText().equals("")){
                Valid.textKosong(TCari,"Pasien");
            }else{
                isPhoto1();
                TabDataMouseClicked(null);
            }
        }else{
            ChkAccor.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih Pasien..!!!");
        }
    }//GEN-LAST:event_ChkAccor1ActionPerformed

    private void BtnRefreshPhoto2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto2ActionPerformed
        tampilprasedasi();
    }//GEN-LAST:event_BtnRefreshPhoto2ActionPerformed

    private void tbPrasedasiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPrasedasiMouseClicked
        getData2();
    }//GEN-LAST:event_tbPrasedasiMouseClicked

    private void tbPrasedasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPrasedasiKeyPressed
        getData2();
    }//GEN-LAST:event_tbPrasedasiKeyPressed

    private void TabData1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabData1MouseClicked
        if(TabData1.getSelectedIndex()==0){
           
        }else if(TabData1.getSelectedIndex()==1){
           
        }
    }//GEN-LAST:event_TabData1MouseClicked

    private void TCari2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCari2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TCari2KeyPressed

    private void BtnCari2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCari2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnCari2ActionPerformed

    private void BtnCari2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCari2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnCari2KeyPressed

    private void BtnAll2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAll2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAll2ActionPerformed

    private void BtnAll2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAll2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAll2KeyPressed

    private void TNoRw3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRw3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TNoRw3KeyPressed

    private void KdDokter3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokter3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdDokter3KeyPressed

    private void BtnPetugas2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugas2ActionPerformed
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnPetugas2ActionPerformed

    private void BtnPetugas2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugas2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugas2KeyPressed

    private void Jam2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam2ActionPerformed

    private void Jam2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam2KeyPressed

    private void Menit2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit2KeyPressed

    private void Detik2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik2KeyPressed

    private void jk2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk2MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2MouseMoved

    private void jk2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jk2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2MouseExited

    private void jk2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jk2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2ActionPerformed

    private void jk2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jk2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jk2KeyPressed

    private void ChkAccor2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccor2ActionPerformed
        if(!(TNoRw3.getText().equals(""))){
            if(TNoRw3.getText().equals("")){
                Valid.textKosong(TCari2,"Pasien");
            }else{
                isPhoto2();
                TabDataMouseClicked(null);
            }
        }else{
            ChkAccor2.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih Pasien..!!!");
        }
    }//GEN-LAST:event_ChkAccor2ActionPerformed

    private void BtnRefreshPhoto3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto3ActionPerformed
            tampilprainduksi();
    }//GEN-LAST:event_BtnRefreshPhoto3ActionPerformed

    private void tbPrainduksiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPrainduksiMouseClicked
     getData3();
    }//GEN-LAST:event_tbPrainduksiMouseClicked

    private void tbPrainduksiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPrainduksiKeyPressed
     getData3();
    }//GEN-LAST:event_tbPrainduksiKeyPressed

    private void TabData2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabData2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_TabData2MouseClicked

    private void Scroll9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Scroll9KeyPressed
      getData3();
    }//GEN-LAST:event_Scroll9KeyPressed

    private void data_obyektifKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_data_obyektifKeyPressed

    }//GEN-LAST:event_data_obyektifKeyPressed

    private void data_subyektifKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_data_subyektifKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_data_subyektifKeyPressed

    private void diagnosa_pra_opKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_diagnosa_pra_opKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_diagnosa_pra_opKeyPressed

    private void deskripsi_penandaanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deskripsi_penandaanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deskripsi_penandaanKeyPressed

    private void rencana_operasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rencana_operasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_rencana_operasiKeyPressed

    private void posisi_pasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_posisi_pasienKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_posisi_pasienKeyPressed

    private void jkKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jkKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jkKeyPressed

    private void jkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jkActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jkActionPerformed

    private void jkMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jkMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jkMouseExited

    private void jkMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jkMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jkMouseMoved

    private void BtnRiwayatSoapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRiwayatSoapActionPerformed
         i=2;
            caripemeriksaan.setNoRawat(TNoRw.getText());
            caripemeriksaan.tampil();
            caripemeriksaan.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            caripemeriksaan.setLocationRelativeTo(internalFrame1);
            caripemeriksaan.setVisible(true);
    }//GEN-LAST:event_BtnRiwayatSoapActionPerformed

    private void BtnRiwayatSoapKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnRiwayatSoapKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnRiwayatSoapKeyPressed

    private void BtnRiwayatSoap1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRiwayatSoap1ActionPerformed
       if(TNoRw.getText().equals("")&&TNoRM.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Pasien masih kosong...!!!");
        }else{
            i=1;
            caripemeriksaan.setNoRawat(TNoRw.getText());
            caripemeriksaan.tampil();
            caripemeriksaan.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            caripemeriksaan.setLocationRelativeTo(internalFrame1);
            caripemeriksaan.setVisible(true);
        }
    }//GEN-LAST:event_BtnRiwayatSoap1ActionPerformed

    private void BtnRiwayatSoap1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnRiwayatSoap1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnRiwayatSoap1KeyPressed

    private void CbPosisiPasienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CbPosisiPasienActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CbPosisiPasienActionPerformed

    private void CbPosisiPasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CbPosisiPasienKeyPressed
        // TODO add your handling code here:
     
    }//GEN-LAST:event_CbPosisiPasienKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMOKAsesmenPraBedahdanVerifikasi dialog = new RMOKAsesmenPraBedahdanVerifikasi(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnAll1;
    private widget.Button BtnAll2;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnCari1;
    private widget.Button BtnCari2;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPetugas;
    private widget.Button BtnPetugas1;
    private widget.Button BtnPetugas2;
    private widget.Button BtnRefreshPhoto1;
    private widget.Button BtnRefreshPhoto2;
    private widget.Button BtnRefreshPhoto3;
    private widget.Button BtnRiwayatSoap;
    private widget.Button BtnRiwayatSoap1;
    private widget.Button BtnSimpan;
    private widget.ComboBox CbPosisiPasien;
    private widget.CekBox ChkAccor;
    private widget.CekBox ChkAccor1;
    private widget.CekBox ChkAccor2;
    private widget.CekBox ChkJam1;
    private widget.CekBox ChkJam2;
    private widget.CekBox ChkJam3;
    private widget.ComboBox Detik;
    private widget.ComboBox Detik1;
    private widget.ComboBox Detik2;
    private widget.PanelBiasa FormInput1;
    private widget.PanelBiasa FormInput2;
    private widget.PanelBiasa FormInput3;
    private widget.PanelBiasa FormPass3;
    private widget.PanelBiasa FormPass4;
    private widget.PanelBiasa FormPass5;
    private widget.PanelBiasa FormTelaah;
    private widget.PanelBiasa FormTelaah1;
    private widget.PanelBiasa FormTelaah2;
    private widget.ComboBox Jam;
    private widget.ComboBox Jam1;
    private widget.ComboBox Jam2;
    private widget.TextBox KdDokter;
    private widget.TextBox KdDokter3;
    private widget.TextBox KdPetugas1;
    private widget.Label LCount;
    private widget.Label LCount1;
    private widget.Label LCount2;
    private widget.ComboBox Menit;
    private widget.ComboBox Menit1;
    private widget.ComboBox Menit2;
    private widget.TextBox NmDokter;
    private widget.TextBox NmDokter3;
    private widget.TextBox NmPetugas1;
    private widget.PanelBiasa PanelAccor;
    private widget.PanelBiasa PanelAccor1;
    private widget.PanelBiasa PanelAccor2;
    private usu.widget.glass.PanelGlass PanelWall;
    private javax.swing.JPopupMenu Popup;
    private javax.swing.JPopupMenu Popup1;
    private javax.swing.JPopupMenu Popup2;
    private widget.ScrollPane Scroll5;
    private widget.ScrollPane Scroll6;
    private widget.ScrollPane Scroll8;
    private widget.ScrollPane Scroll9;
    private widget.TextBox TCari;
    private widget.TextBox TCari1;
    private widget.TextBox TCari2;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRM2;
    private widget.TextBox TNoRM3;
    private widget.TextBox TNoRw;
    private widget.TextBox TNoRw2;
    private widget.TextBox TNoRw3;
    private widget.TextBox TPasien;
    private widget.TextBox TPasien2;
    private widget.TextBox TPasien3;
    private javax.swing.JTabbedPane TabData;
    private javax.swing.JTabbedPane TabData1;
    private javax.swing.JTabbedPane TabData2;
    private javax.swing.JTabbedPane TabOK;
    private widget.Tanggal Tanggal;
    private widget.Tanggal Tanggal1;
    private widget.Tanggal Tanggal2;
    private javax.swing.ButtonGroup buttonGroup1;
    private widget.TextArea data_obyektif;
    private widget.TextArea data_subyektif;
    private widget.TextArea deskripsi_penandaan;
    private widget.TextArea diagnosa_pra_op;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame3;
    private widget.InternalFrame internalFrame4;
    private widget.InternalFrame internalFrame5;
    private widget.Label jLabel11;
    private widget.Label jLabel12;
    private widget.Label jLabel13;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator17;
    private javax.swing.JSeparator jSeparator18;
    private javax.swing.JSeparator jSeparator19;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private widget.TextBox jk;
    private widget.TextBox jk1;
    private widget.TextBox jk2;
    private widget.Label label10;
    private widget.Label label11;
    private widget.Label label12;
    private widget.Label label13;
    private widget.Label label14;
    private widget.Label label16;
    private widget.Label label17;
    private widget.Label label18;
    private widget.Label label198;
    private widget.Label label201;
    private widget.Label label202;
    private widget.Label label204;
    private widget.Label label219;
    private widget.Label label220;
    private widget.Label label221;
    private widget.Label label222;
    private widget.Label label223;
    private widget.Label label224;
    private widget.Label label9;
    private widget.panelisi panelisi1;
    private widget.panelisi panelisi5;
    private widget.panelisi panelisi6;
    private widget.panelisi panelisi7;
    private widget.TextArea posisi_pasien;
    private javax.swing.JMenuItem pp1;
    private javax.swing.JMenuItem pp2;
    private javax.swing.JMenuItem ppCetakLembarFormulir;
    private widget.TextArea rencana_operasi;
    private widget.ScrollPane scrollInput1;
    private widget.ScrollPane scrollInput2;
    private widget.ScrollPane scrollInput3;
    private widget.ScrollPane scrollPane1;
    private widget.ScrollPane scrollPane2;
    private widget.ScrollPane scrollPane3;
    private widget.ScrollPane scrollPane4;
    private widget.ScrollPane scrollPane5;
    private widget.ScrollPane scrollPane6;
    private widget.Table tbChecklist;
    private widget.Table tbMasalahKeperawatan;
    private widget.Table tbPrainduksi;
    private widget.Table tbPrasedasi;
    // End of variables declaration//GEN-END:variables

    private void tampil() {
        Valid.tabelKosong(tabModePraBedah);
        try{
            ps=koneksi.prepareStatement(
                   "select penilaian_awal_ok_asuhan_prabedah.*,pasien.nm_pasien,pasien.jk,pasien.no_rkm_medis,dokter.kd_dokter,dokter.nm_dokter "+
                   "from penilaian_awal_ok_asuhan_prabedah "+
                   "left join reg_periksa on reg_periksa.no_rawat = penilaian_awal_ok_asuhan_prabedah.no_rawat "+
                   "left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis "+
                   "left join dokter on penilaian_awal_ok_asuhan_prabedah.kd_dokter = dokter.kd_dokter "+
                   "where penilaian_awal_ok_asuhan_prabedah.no_rawat like ? order by penilaian_awal_ok_asuhan_prabedah.no_rawat");
            try {
                ps.setString(1,"%"+TCari.getText().trim()+"%");
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModePraBedah.addRow(new Object[]{
                        rs.getString("no_rawat"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("jk"),
                        rs.getString("data_subyektif"),
                        rs.getString("data_obyektif"),
                        rs.getString("diagnosis_pra_operasi"),
                        rs.getString("rencana_operasi"),
                        rs.getString("deskripsi_penandaan"),
                        rs.getString("posisi_pasien"),
                        rs.getString("posisi_pasien_lainnya"),
                        rs.getString("kd_dokter"),
                        rs.getString("nm_dokter"),
                    });
                }
            } catch (Exception e) {
                System.out.println("Notifikasi : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            LCount.setText(""+tabModePraBedah.getRowCount());
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
     
    private void tampilprasedasi() {
       
    }
    
    private void tampilprainduksi() {
      
    }

    
    public void setNoRm(String NoRawat){
        TNoRw.setText(NoRawat);
        TCari.setText(NoRawat);
        TNoRw2.setText(NoRawat);
        TCari1.setText(NoRawat);
        TNoRw3.setText(NoRawat);
        TCari2.setText(NoRawat);
        tampil();
        tampilprasedasi();
        tampilprainduksi();
        
        String no_rm_pasien = Sequel.cariIsi("select reg_periksa.no_rkm_medis from reg_periksa where reg_periksa.no_rawat='"+TNoRw.getText()+"'");
        String nama_pasien = Sequel.cariIsi("select pasien.nm_pasien from pasien where pasien.no_rkm_medis='"+no_rm_pasien+"'");
        String jk_pasien = Sequel.cariIsi("select pasien.jk from pasien where pasien.no_rkm_medis='"+no_rm_pasien+"'");
        TNoRM.setText(no_rm_pasien);
        TNoRM2.setText(no_rm_pasien);
        TNoRM3.setText(no_rm_pasien);
        TPasien.setText(nama_pasien);
        TPasien2.setText(nama_pasien);
        TPasien3.setText(nama_pasien);
        
        jk.setText(jk_pasien);
        jk1.setText(jk_pasien);
        jk2.setText(jk_pasien);
        
        ChkJam1.setSelected(true);
        ChkJam2.setSelected(true);
        ChkJam3.setSelected(true);
       
        
    }
    public void setNoRm2(String NoRawat,String NoRkmMedis,String Nama,String Tanggal){
        tampil();
        tampilprasedasi();
        tampilprainduksi();
    }
    private void getData() {
        int row=tbChecklist.getSelectedRow();
        int selectedRow = tbChecklist.getSelectedRow();
       
        if(row!= -1){
            ChkJam1.setSelected(false);
            TNoRw.setText(tbChecklist.getValueAt(row,0).toString());
            Valid.SetTgl2(Tanggal,tbChecklist.getValueAt(tbChecklist.getSelectedRow(),1).toString());
            Jam.setSelectedItem(tbChecklist.getValueAt(tbChecklist.getSelectedRow(),2).toString().substring(0,2));
            Menit.setSelectedItem(tbChecklist.getValueAt(tbChecklist.getSelectedRow(),2).toString().substring(3,5));
            Detik.setSelectedItem(tbChecklist.getValueAt(tbChecklist.getSelectedRow(),2).toString().substring(6,8));
            TNoRM.setText(tbChecklist.getValueAt(row,3).toString());
            TPasien.setText(tbChecklist.getValueAt(row,4).toString());
            jk.setText(tbChecklist.getValueAt(row,5).toString());
            data_subyektif.setText(tbChecklist.getValueAt(row,6).toString()); 
            data_obyektif.setText(tbChecklist.getValueAt(row,7).toString()); 
            diagnosa_pra_op.setText(tbChecklist.getValueAt(row,8).toString());
            rencana_operasi.setText(tbChecklist.getValueAt(row,9).toString()); 
            deskripsi_penandaan.setText(tbChecklist.getValueAt(row,10).toString()); 
            CbPosisiPasien.setSelectedItem(tbChecklist.getValueAt(row, 11).toString());
            posisi_pasien.setText(tbChecklist.getValueAt(row, 12).toString());
            KdDokter.setText(tbChecklist.getValueAt(row,13).toString());
            NmDokter.setText(tbChecklist.getValueAt(row,14).toString());
            
          
           
        }
    }
    
  private void getData2() {
        int row=tbPrasedasi.getSelectedRow();
        int selectedRow = tbPrasedasi.getSelectedRow();
        if(row!= -1){
            ChkJam2.setSelected(false);
            TNoRw2.setText(tbPrasedasi.getValueAt(row,0).toString());
            Valid.SetTgl2(Tanggal1,tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),1).toString());
            Jam1.setSelectedItem(tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString().substring(0,2));
            Menit1.setSelectedItem(tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString().substring(3,5));
            Detik1.setSelectedItem(tbPrasedasi.getValueAt(tbPrasedasi.getSelectedRow(),2).toString().substring(6,8));
            KdPetugas1.setText(tbPrasedasi.getValueAt(row,3).toString());
            NmPetugas1.setText(tbPrasedasi.getValueAt(row,4).toString());
            TNoRM2.setText(tbPrasedasi.getValueAt(row,5).toString());
            TPasien2.setText(tbPrasedasi.getValueAt(row,6).toString());
            jk1.setText(tbPrasedasi.getValueAt(row,7).toString()); 
          
        }
  }
  
  
  private void getData3() {
        int row=tbPrainduksi.getSelectedRow();
        int selectedRow = tbPrainduksi.getSelectedRow();
        if(row!= -1){
            ChkJam3.setSelected(false);
            TNoRw3.setText(tbPrainduksi.getValueAt(row,0).toString());
            Valid.SetTgl2(Tanggal2,tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),1).toString());
            Jam2.setSelectedItem(tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),2).toString().substring(0,2));
            Menit2.setSelectedItem(tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),2).toString().substring(3,5));
            Detik2.setSelectedItem(tbPrainduksi.getValueAt(tbPrainduksi.getSelectedRow(),2).toString().substring(6,8));
            KdDokter3.setText(tbPrainduksi.getValueAt(row,3).toString());
            NmDokter3.setText(tbPrainduksi.getValueAt(row,4).toString());
            TNoRM3.setText(tbPrainduksi.getValueAt(row,5).toString());
            TPasien3.setText(tbPrainduksi.getValueAt(row,6).toString());
           
        }
  }

    
    public void emptTeks() {
        if(TabOK.getSelectedIndex()==0){
            jk.setText(""); 
            ChkJam1.setSelected(true);
            data_subyektif.setText(""); 
            data_obyektif.setText(""); 
            diagnosa_pra_op.setText("");
            rencana_operasi.setText(""); 
            deskripsi_penandaan.setText(""); 
            posisi_pasien.setText("");
            CbPosisiPasien.setSelectedIndex(0);
            KdDokter.setText("");
            NmDokter.setText("");
          
        }else if(TabOK.getSelectedIndex()==1){
                        ChkJam2.setSelected(true);
                        KdPetugas1.setText("");
                  
                     
        }else if(TabOK.getSelectedIndex()==2){
                        ChkJam3.setSelected(true);
                        KdDokter3.setText("");
                        NmDokter3.setText("");
                      
                  
                     
        }else if(TabOK.getSelectedIndex()==3){
                       
                        
           
        }            
    }
    
    private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam1.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkJam1.isSelected()==false){
                    nilai_jam =Jam.getSelectedIndex();
                    nilai_menit =Menit.getSelectedIndex();
                    nilai_detik =Detik.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam.setSelectedItem(jam);
                Menit.setSelectedItem(menit);
                Detik.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    private void jam1(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam2;
            private int nilai_menit2;
            private int nilai_detik2;
            public void actionPerformed(ActionEvent e) {
                String nol_jam2 = "";
                String nol_menit2 = "";
                String nol_detik2 = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam2.isSelected()==true){
                    nilai_jam2 = now.getHours();
                    nilai_menit2 = now.getMinutes();
                    nilai_detik2 = now.getSeconds();
                }else if(ChkJam2.isSelected()==false){
                    nilai_jam2 =Jam1.getSelectedIndex();
                    nilai_menit2 =Menit1.getSelectedIndex();
                    nilai_detik2 =Detik1.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam2 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam2 = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit2 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit2 = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik2 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik2 = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam2 = nol_jam2 + Integer.toString(nilai_jam2);
                String menit2 = nol_menit2 + Integer.toString(nilai_menit2);
                String detik2 = nol_detik2 + Integer.toString(nilai_detik2);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam1.setSelectedItem(jam2);
                Menit1.setSelectedItem(menit2);
                Detik1.setSelectedItem(detik2);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
    private void jam2(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam3;
            private int nilai_menit3;
            private int nilai_detik3;
            public void actionPerformed(ActionEvent e) {
                String nol_jam3 = "";
                String nol_menit3 = "";
                String nol_detik3 = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam3.isSelected()==true){
                    nilai_jam3 = now.getHours();
                    nilai_menit3 = now.getMinutes();
                    nilai_detik3 = now.getSeconds();
                }else if(ChkJam3.isSelected()==false){
                    nilai_jam3 =Jam2.getSelectedIndex();
                    nilai_menit3 =Menit2.getSelectedIndex();
                    nilai_detik3 =Detik2.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam3 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam3 = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit3 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit3 = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik3 <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik3 = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam3 = nol_jam3 + Integer.toString(nilai_jam3);
                String menit3 = nol_menit3 + Integer.toString(nilai_menit3);
                String detik3 = nol_detik3 + Integer.toString(nilai_detik3);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam2.setSelectedItem(jam3);
                Menit2.setSelectedItem(menit3);
                Detik2.setSelectedItem(detik3);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
    private void isPhoto(){
        if(ChkAccor.isSelected()==true){
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(internalFrame1.getWidth()-650,HEIGHT));
            TabData.setVisible(true);  
            ChkAccor.setVisible(true);
        }else if(ChkAccor.isSelected()==false){    
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(15,HEIGHT));
            TabData.setVisible(false);  
            ChkAccor.setVisible(true);
        }
    }
    private void isPhoto1(){
        if(ChkAccor1.isSelected()==true){
            ChkAccor1.setVisible(false);
            PanelAccor1.setPreferredSize(new Dimension(internalFrame1.getWidth()-650,HEIGHT));
            TabData1.setVisible(true);  
            ChkAccor1.setVisible(true);
        }else if(ChkAccor1.isSelected()==false){    
            ChkAccor1.setVisible(false);
            PanelAccor1.setPreferredSize(new Dimension(15,HEIGHT));
            TabData1.setVisible(false);  
            ChkAccor1.setVisible(true);
        }
    }
    private void isPhoto2(){
        if(ChkAccor2.isSelected()==true){
            ChkAccor2.setVisible(false);
            PanelAccor2.setPreferredSize(new Dimension(internalFrame1.getWidth()-650,HEIGHT));
            TabData2.setVisible(true);  
            ChkAccor2.setVisible(true);
        }else if(ChkAccor2.isSelected()==false){    
            ChkAccor2.setVisible(false);
            PanelAccor2.setPreferredSize(new Dimension(15,HEIGHT));
            TabData2.setVisible(false);  
            ChkAccor2.setVisible(true);
        }
    }
}
