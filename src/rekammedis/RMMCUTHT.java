/*
 * Kontribusi dari Abdul Wahid, RSUD Cipayung Jakarta Timur
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;

/**
 *
 * @author perpustakaan
 */
public final class RMMCUTHT extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private StringBuilder htmlContent;
    private String finger="";
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMMCUTHT(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tgl.Lahir","J.K.","NIP Dokter","Nama Dokter","Tanggal",
            "Keluhan","HidungMukosaKiri","HidungMukosaKanan","HidungSekretKiri","HidungSekretKanan",
            "HidungConchaKiri","HidungConchaKanan","SeptumDeviasiKiri","SeptumDeviasiKanan",
            "Massa","NasopharingMukosa","NasopharingSekret","Pharinx","Tonsil","Epiglotis",
            "Arytenoid","PergerakanPitaSuara","LiangTelingaKiri","LiangTelingaKanan",
            "MembranaTympanisKiri","MembranaTympanisKanan","ReflexCahayaKiri","ReflexCahayaKanan",
            "Audiogram","Kesan","Saran",
            "NIP Petugas","Nama Petugas"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        
        tbObat.setModel(tabMode);
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 14; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(150);
            }else if(i==3){
                column.setPreferredWidth(65);
            }else if(i==4){
                column.setPreferredWidth(55);
            }else if(i==5){
                column.setPreferredWidth(80);
            }else if(i==6){
                column.setPreferredWidth(150);
            }else if(i==7){
                column.setPreferredWidth(115);
            }else if(i==8 || i==9 || i==10 ){
                column.setPreferredWidth(100);
            }else if(i==11){
                column.setPreferredWidth(160);
            }else if(i==34){
                column.setPreferredWidth(80);
            }else if(i==35){
                column.setPreferredWidth(150);
            }else{
                column.setPreferredWidth(100);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        Keluhan.setDocument(new batasInput((int)250).getKata(Keluhan));
        HidungMukosaKiri.setDocument(new batasInput((int)250).getKata(HidungMukosaKiri));
        HidungMukosaKanan.setDocument(new batasInput((int)250).getKata(HidungMukosaKanan));
        HidungSekretKiri.setDocument(new batasInput((int)250).getKata(HidungSekretKiri));
        HidungSekretKanan.setDocument(new batasInput((int)250).getKata(HidungSekretKanan));
        HidungConchaKiri.setDocument(new batasInput((int)250).getKata(HidungConchaKiri));
        HidungConchaKanan.setDocument(new batasInput((int)250).getKata(HidungConchaKanan));        
        SeptumDeviasiKiri.setDocument(new batasInput((int)250).getKata(SeptumDeviasiKiri));
        SeptumDeviasiKanan.setDocument(new batasInput((int)250).getKata(SeptumDeviasiKanan));        
        Massa.setDocument(new batasInput((int)250).getKata(Massa));
        NasopharingMukosa.setDocument(new batasInput((int)250).getKata(NasopharingMukosa));
        NasopharingSekret.setDocument(new batasInput((int)250).getKata(NasopharingSekret));
        Pharinx.setDocument(new batasInput((int)250).getKata(Pharinx));
        Tonsil.setDocument(new batasInput((int)250).getKata(Tonsil));
        Epiglotis.setDocument(new batasInput((int)250).getKata(Epiglotis));
        Arytenoid.setDocument(new batasInput((int)250).getKata(Arytenoid));
        PergerakanPitaSuara.setDocument(new batasInput((int)250).getKata(PergerakanPitaSuara));
        LiangTelingaKiri.setDocument(new batasInput((int)250).getKata(LiangTelingaKiri));
        LiangTelingaKanan.setDocument(new batasInput((int)250).getKata(LiangTelingaKanan));
        MembranaTympanisKiri.setDocument(new batasInput((int)250).getKata(MembranaTympanisKiri));
        MembranaTympanisKanan.setDocument(new batasInput((int)250).getKata(MembranaTympanisKanan));        
        ReflexCahayaKiri.setDocument(new batasInput((int)250).getKata(ReflexCahayaKiri));
        ReflexCahayaKanan.setDocument(new batasInput((int)250).getKata(ReflexCahayaKanan));
        Audiogram.setDocument(new batasInput((int)250).getKata(Audiogram));
        Saran.setDocument(new batasInput((int)250).getKata(Kesan));
        Kesan.setDocument(new batasInput((int)250).getKata(Saran));        
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){
                    KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                    KdDokter.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });

        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                    KdPetugas.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnSuratKeterangan = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdDokter = new widget.TextBox();
        NmDokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        jLabel11 = new widget.Label();
        jLabel94 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel99 = new widget.Label();
        label11 = new widget.Label();
        TglPemeriksaan = new widget.Tanggal();
        label15 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnPetugas = new widget.Button();
        scrollPane6 = new widget.ScrollPane();
        Keluhan = new widget.TextArea();
        scrollPane7 = new widget.ScrollPane();
        HidungMukosaKanan = new widget.TextArea();
        jLabel95 = new widget.Label();
        jLabel96 = new widget.Label();
        scrollPane8 = new widget.ScrollPane();
        HidungSekretKanan = new widget.TextArea();
        jLabel97 = new widget.Label();
        scrollPane9 = new widget.ScrollPane();
        HidungMukosaKiri = new widget.TextArea();
        jLabel98 = new widget.Label();
        scrollPane10 = new widget.ScrollPane();
        HidungSekretKiri = new widget.TextArea();
        jLabel100 = new widget.Label();
        scrollPane11 = new widget.ScrollPane();
        HidungConchaKiri = new widget.TextArea();
        jLabel101 = new widget.Label();
        scrollPane12 = new widget.ScrollPane();
        HidungConchaKanan = new widget.TextArea();
        jLabel102 = new widget.Label();
        scrollPane13 = new widget.ScrollPane();
        SeptumDeviasiKiri = new widget.TextArea();
        jLabel103 = new widget.Label();
        scrollPane14 = new widget.ScrollPane();
        SeptumDeviasiKanan = new widget.TextArea();
        jLabel104 = new widget.Label();
        jLabel105 = new widget.Label();
        jLabel106 = new widget.Label();
        jLabel107 = new widget.Label();
        scrollPane15 = new widget.ScrollPane();
        Massa = new widget.TextArea();
        scrollPane16 = new widget.ScrollPane();
        NasopharingMukosa = new widget.TextArea();
        jLabel108 = new widget.Label();
        jLabel109 = new widget.Label();
        scrollPane17 = new widget.ScrollPane();
        NasopharingSekret = new widget.TextArea();
        jLabel110 = new widget.Label();
        jLabel111 = new widget.Label();
        scrollPane18 = new widget.ScrollPane();
        Pharinx = new widget.TextArea();
        jLabel112 = new widget.Label();
        scrollPane19 = new widget.ScrollPane();
        Tonsil = new widget.TextArea();
        jLabel113 = new widget.Label();
        jLabel114 = new widget.Label();
        scrollPane20 = new widget.ScrollPane();
        Epiglotis = new widget.TextArea();
        jLabel115 = new widget.Label();
        scrollPane21 = new widget.ScrollPane();
        Arytenoid = new widget.TextArea();
        jLabel116 = new widget.Label();
        scrollPane22 = new widget.ScrollPane();
        PergerakanPitaSuara = new widget.TextArea();
        jLabel117 = new widget.Label();
        jLabel118 = new widget.Label();
        jLabel119 = new widget.Label();
        jLabel120 = new widget.Label();
        scrollPane23 = new widget.ScrollPane();
        LiangTelingaKiri = new widget.TextArea();
        jLabel121 = new widget.Label();
        scrollPane24 = new widget.ScrollPane();
        LiangTelingaKanan = new widget.TextArea();
        jLabel122 = new widget.Label();
        jLabel123 = new widget.Label();
        jLabel124 = new widget.Label();
        scrollPane25 = new widget.ScrollPane();
        MembranaTympanisKiri = new widget.TextArea();
        jLabel125 = new widget.Label();
        scrollPane26 = new widget.ScrollPane();
        MembranaTympanisKanan = new widget.TextArea();
        jLabel126 = new widget.Label();
        jLabel127 = new widget.Label();
        jLabel128 = new widget.Label();
        jLabel129 = new widget.Label();
        jLabel130 = new widget.Label();
        scrollPane27 = new widget.ScrollPane();
        ReflexCahayaKiri = new widget.TextArea();
        jLabel131 = new widget.Label();
        scrollPane28 = new widget.ScrollPane();
        ReflexCahayaKanan = new widget.TextArea();
        jLabel132 = new widget.Label();
        jLabel133 = new widget.Label();
        jLabel134 = new widget.Label();
        scrollPane29 = new widget.ScrollPane();
        Audiogram = new widget.TextArea();
        jLabel135 = new widget.Label();
        scrollPane30 = new widget.ScrollPane();
        Kesan = new widget.TextArea();
        jLabel136 = new widget.Label();
        scrollPane31 = new widget.ScrollPane();
        Saran = new widget.TextArea();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnSuratKeterangan.setBackground(new java.awt.Color(255, 255, 254));
        MnSuratKeterangan.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnSuratKeterangan.setForeground(new java.awt.Color(50, 50, 50));
        MnSuratKeterangan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnSuratKeterangan.setText("Surat Keterangan Kesehatan THT");
        MnSuratKeterangan.setName("MnSuratKeterangan"); // NOI18N
        MnSuratKeterangan.setPreferredSize(new java.awt.Dimension(220, 26));
        MnSuratKeterangan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnSuratKeteranganActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnSuratKeterangan);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Medical Checkup THT ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 960));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 70, 70, 23);

        KdDokter.setEditable(false);
        KdDokter.setName("KdDokter"); // NOI18N
        KdDokter.setPreferredSize(new java.awt.Dimension(80, 23));
        KdDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokterKeyPressed(evt);
            }
        });
        FormInput.add(KdDokter);
        KdDokter.setBounds(74, 40, 90, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        NmDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmDokter);
        NmDokter.setBounds(166, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(348, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jLabel94.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel94.setText("Mukosa Kanan");
        jLabel94.setName("jLabel94"); // NOI18N
        FormInput.add(jLabel94);
        jLabel94.setBounds(325, 190, 70, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 100, 880, 1);

        jLabel99.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel99.setText("I. KELUHAN");
        jLabel99.setName("jLabel99"); // NOI18N
        FormInput.add(jLabel99);
        jLabel99.setBounds(10, 100, 180, 23);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(380, 40, 52, 23);

        TglPemeriksaan.setForeground(new java.awt.Color(50, 70, 50));
        TglPemeriksaan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "27-05-2024 14:43:23" }));
        TglPemeriksaan.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglPemeriksaan.setName("TglPemeriksaan"); // NOI18N
        TglPemeriksaan.setOpaque(false);
        TglPemeriksaan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglPemeriksaanKeyPressed(evt);
            }
        });
        FormInput.add(TglPemeriksaan);
        TglPemeriksaan.setBounds(436, 40, 130, 23);

        label15.setText("Dokter :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label15);
        label15.setBounds(0, 40, 70, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(74, 70, 90, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(166, 70, 180, 23);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnPetugas.setMnemonic('2');
        BtnPetugas.setToolTipText("Alt+2");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        FormInput.add(BtnPetugas);
        BtnPetugas.setBounds(348, 70, 28, 23);

        scrollPane6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane6.setName("scrollPane6"); // NOI18N

        Keluhan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Keluhan.setColumns(20);
        Keluhan.setRows(5);
        Keluhan.setName("Keluhan"); // NOI18N
        Keluhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeluhanKeyPressed(evt);
            }
        });
        scrollPane6.setViewportView(Keluhan);

        FormInput.add(scrollPane6);
        scrollPane6.setBounds(50, 120, 540, 50);

        scrollPane7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane7.setName("scrollPane7"); // NOI18N

        HidungMukosaKanan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        HidungMukosaKanan.setColumns(20);
        HidungMukosaKanan.setRows(5);
        HidungMukosaKanan.setName("HidungMukosaKanan"); // NOI18N
        HidungMukosaKanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HidungMukosaKananKeyPressed(evt);
            }
        });
        scrollPane7.setViewportView(HidungMukosaKanan);

        FormInput.add(scrollPane7);
        scrollPane7.setBounds(400, 190, 190, 40);

        jLabel95.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel95.setText("II. HIDUNG");
        jLabel95.setName("jLabel95"); // NOI18N
        FormInput.add(jLabel95);
        jLabel95.setBounds(10, 170, 210, 23);

        jLabel96.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel96.setText("Sekret Kanan");
        jLabel96.setName("jLabel96"); // NOI18N
        FormInput.add(jLabel96);
        jLabel96.setBounds(325, 240, 70, 23);

        scrollPane8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane8.setName("scrollPane8"); // NOI18N

        HidungSekretKanan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        HidungSekretKanan.setColumns(20);
        HidungSekretKanan.setRows(5);
        HidungSekretKanan.setName("HidungSekretKanan"); // NOI18N
        HidungSekretKanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HidungSekretKananKeyPressed(evt);
            }
        });
        scrollPane8.setViewportView(HidungSekretKanan);

        FormInput.add(scrollPane8);
        scrollPane8.setBounds(400, 240, 190, 40);

        jLabel97.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel97.setText("Mukosa Kiri");
        jLabel97.setName("jLabel97"); // NOI18N
        FormInput.add(jLabel97);
        jLabel97.setBounds(50, 190, 80, 23);

        scrollPane9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane9.setName("scrollPane9"); // NOI18N

        HidungMukosaKiri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        HidungMukosaKiri.setColumns(20);
        HidungMukosaKiri.setRows(5);
        HidungMukosaKiri.setName("HidungMukosaKiri"); // NOI18N
        HidungMukosaKiri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HidungMukosaKiriKeyPressed(evt);
            }
        });
        scrollPane9.setViewportView(HidungMukosaKiri);

        FormInput.add(scrollPane9);
        scrollPane9.setBounds(125, 190, 190, 40);

        jLabel98.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel98.setText("Sekret Kiri");
        jLabel98.setName("jLabel98"); // NOI18N
        FormInput.add(jLabel98);
        jLabel98.setBounds(50, 240, 80, 23);

        scrollPane10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane10.setName("scrollPane10"); // NOI18N

        HidungSekretKiri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        HidungSekretKiri.setColumns(20);
        HidungSekretKiri.setRows(5);
        HidungSekretKiri.setName("HidungSekretKiri"); // NOI18N
        HidungSekretKiri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HidungSekretKiriKeyPressed(evt);
            }
        });
        scrollPane10.setViewportView(HidungSekretKiri);

        FormInput.add(scrollPane10);
        scrollPane10.setBounds(125, 240, 190, 40);

        jLabel100.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel100.setText("Concha Kiri");
        jLabel100.setName("jLabel100"); // NOI18N
        FormInput.add(jLabel100);
        jLabel100.setBounds(50, 290, 80, 23);

        scrollPane11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane11.setName("scrollPane11"); // NOI18N

        HidungConchaKiri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        HidungConchaKiri.setColumns(20);
        HidungConchaKiri.setRows(5);
        HidungConchaKiri.setName("HidungConchaKiri"); // NOI18N
        HidungConchaKiri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HidungConchaKiriKeyPressed(evt);
            }
        });
        scrollPane11.setViewportView(HidungConchaKiri);

        FormInput.add(scrollPane11);
        scrollPane11.setBounds(125, 290, 190, 40);

        jLabel101.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel101.setText("Concha Kanan");
        jLabel101.setName("jLabel101"); // NOI18N
        FormInput.add(jLabel101);
        jLabel101.setBounds(325, 290, 70, 23);

        scrollPane12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane12.setName("scrollPane12"); // NOI18N

        HidungConchaKanan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        HidungConchaKanan.setColumns(20);
        HidungConchaKanan.setRows(5);
        HidungConchaKanan.setName("HidungConchaKanan"); // NOI18N
        HidungConchaKanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HidungConchaKananKeyPressed(evt);
            }
        });
        scrollPane12.setViewportView(HidungConchaKanan);

        FormInput.add(scrollPane12);
        scrollPane12.setBounds(400, 290, 190, 40);

        jLabel102.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel102.setText("Kanan");
        jLabel102.setName("jLabel102"); // NOI18N
        FormInput.add(jLabel102);
        jLabel102.setBounds(320, 350, 70, 23);

        scrollPane13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane13.setName("scrollPane13"); // NOI18N

        SeptumDeviasiKiri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        SeptumDeviasiKiri.setColumns(20);
        SeptumDeviasiKiri.setRows(5);
        SeptumDeviasiKiri.setName("SeptumDeviasiKiri"); // NOI18N
        SeptumDeviasiKiri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SeptumDeviasiKiriKeyPressed(evt);
            }
        });
        scrollPane13.setViewportView(SeptumDeviasiKiri);

        FormInput.add(scrollPane13);
        scrollPane13.setBounds(125, 340, 190, 40);

        jLabel103.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel103.setText("Septum Deviasi");
        jLabel103.setName("jLabel103"); // NOI18N
        FormInput.add(jLabel103);
        jLabel103.setBounds(320, 340, 80, 23);

        scrollPane14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane14.setName("scrollPane14"); // NOI18N

        SeptumDeviasiKanan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        SeptumDeviasiKanan.setColumns(20);
        SeptumDeviasiKanan.setRows(5);
        SeptumDeviasiKanan.setName("SeptumDeviasiKanan"); // NOI18N
        SeptumDeviasiKanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SeptumDeviasiKananKeyPressed(evt);
            }
        });
        scrollPane14.setViewportView(SeptumDeviasiKanan);

        FormInput.add(scrollPane14);
        scrollPane14.setBounds(400, 340, 190, 40);

        jLabel104.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel104.setText("Septum Deviasi");
        jLabel104.setName("jLabel104"); // NOI18N
        FormInput.add(jLabel104);
        jLabel104.setBounds(40, 340, 80, 23);

        jLabel105.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel105.setText("Kiri");
        jLabel105.setName("jLabel105"); // NOI18N
        FormInput.add(jLabel105);
        jLabel105.setBounds(40, 350, 80, 23);

        jLabel106.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel106.setText("III. NASOPHARING");
        jLabel106.setName("jLabel106"); // NOI18N
        FormInput.add(jLabel106);
        jLabel106.setBounds(10, 390, 210, 23);

        jLabel107.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel107.setText("Massa");
        jLabel107.setName("jLabel107"); // NOI18N
        FormInput.add(jLabel107);
        jLabel107.setBounds(50, 410, 80, 23);

        scrollPane15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane15.setName("scrollPane15"); // NOI18N

        Massa.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Massa.setColumns(20);
        Massa.setRows(5);
        Massa.setName("Massa"); // NOI18N
        Massa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MassaKeyPressed(evt);
            }
        });
        scrollPane15.setViewportView(Massa);

        FormInput.add(scrollPane15);
        scrollPane15.setBounds(125, 410, 190, 40);

        scrollPane16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane16.setName("scrollPane16"); // NOI18N

        NasopharingMukosa.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        NasopharingMukosa.setColumns(20);
        NasopharingMukosa.setRows(5);
        NasopharingMukosa.setName("NasopharingMukosa"); // NOI18N
        NasopharingMukosa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NasopharingMukosaKeyPressed(evt);
            }
        });
        scrollPane16.setViewportView(NasopharingMukosa);

        FormInput.add(scrollPane16);
        scrollPane16.setBounds(400, 410, 190, 40);

        jLabel108.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel108.setText("Mukosa");
        jLabel108.setName("jLabel108"); // NOI18N
        FormInput.add(jLabel108);
        jLabel108.setBounds(325, 410, 70, 23);

        jLabel109.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel109.setText("Sekret");
        jLabel109.setName("jLabel109"); // NOI18N
        FormInput.add(jLabel109);
        jLabel109.setBounds(620, 410, 40, 23);

        scrollPane17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane17.setName("scrollPane17"); // NOI18N

        NasopharingSekret.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        NasopharingSekret.setColumns(20);
        NasopharingSekret.setRows(5);
        NasopharingSekret.setName("NasopharingSekret"); // NOI18N
        NasopharingSekret.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NasopharingSekretKeyPressed(evt);
            }
        });
        scrollPane17.setViewportView(NasopharingSekret);

        FormInput.add(scrollPane17);
        scrollPane17.setBounds(660, 410, 190, 40);

        jLabel110.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel110.setText("IV. TENGGOROKAN");
        jLabel110.setName("jLabel110"); // NOI18N
        FormInput.add(jLabel110);
        jLabel110.setBounds(10, 460, 210, 23);

        jLabel111.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel111.setText("Pharinx");
        jLabel111.setName("jLabel111"); // NOI18N
        FormInput.add(jLabel111);
        jLabel111.setBounds(50, 480, 80, 23);

        scrollPane18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane18.setName("scrollPane18"); // NOI18N

        Pharinx.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Pharinx.setColumns(20);
        Pharinx.setRows(5);
        Pharinx.setName("Pharinx"); // NOI18N
        Pharinx.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PharinxKeyPressed(evt);
            }
        });
        scrollPane18.setViewportView(Pharinx);

        FormInput.add(scrollPane18);
        scrollPane18.setBounds(125, 480, 190, 40);

        jLabel112.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel112.setText("Tonsil");
        jLabel112.setName("jLabel112"); // NOI18N
        FormInput.add(jLabel112);
        jLabel112.setBounds(325, 480, 70, 23);

        scrollPane19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane19.setName("scrollPane19"); // NOI18N

        Tonsil.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Tonsil.setColumns(20);
        Tonsil.setRows(5);
        Tonsil.setName("Tonsil"); // NOI18N
        Tonsil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TonsilKeyPressed(evt);
            }
        });
        scrollPane19.setViewportView(Tonsil);

        FormInput.add(scrollPane19);
        scrollPane19.setBounds(400, 480, 190, 40);

        jLabel113.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel113.setText("V. LARINX");
        jLabel113.setName("jLabel113"); // NOI18N
        FormInput.add(jLabel113);
        jLabel113.setBounds(10, 520, 210, 23);

        jLabel114.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel114.setText("Epiglotis");
        jLabel114.setName("jLabel114"); // NOI18N
        FormInput.add(jLabel114);
        jLabel114.setBounds(50, 540, 80, 23);

        scrollPane20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane20.setName("scrollPane20"); // NOI18N

        Epiglotis.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Epiglotis.setColumns(20);
        Epiglotis.setRows(5);
        Epiglotis.setName("Epiglotis"); // NOI18N
        Epiglotis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EpiglotisKeyPressed(evt);
            }
        });
        scrollPane20.setViewportView(Epiglotis);

        FormInput.add(scrollPane20);
        scrollPane20.setBounds(125, 540, 190, 40);

        jLabel115.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel115.setText("Arytenoid");
        jLabel115.setName("jLabel115"); // NOI18N
        FormInput.add(jLabel115);
        jLabel115.setBounds(325, 540, 70, 23);

        scrollPane21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane21.setName("scrollPane21"); // NOI18N

        Arytenoid.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Arytenoid.setColumns(20);
        Arytenoid.setRows(5);
        Arytenoid.setName("Arytenoid"); // NOI18N
        Arytenoid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ArytenoidKeyPressed(evt);
            }
        });
        scrollPane21.setViewportView(Arytenoid);

        FormInput.add(scrollPane21);
        scrollPane21.setBounds(400, 540, 190, 40);

        jLabel116.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel116.setText("Suara");
        jLabel116.setName("jLabel116"); // NOI18N
        FormInput.add(jLabel116);
        jLabel116.setBounds(600, 560, 60, 23);

        scrollPane22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane22.setName("scrollPane22"); // NOI18N

        PergerakanPitaSuara.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        PergerakanPitaSuara.setColumns(20);
        PergerakanPitaSuara.setRows(5);
        PergerakanPitaSuara.setName("PergerakanPitaSuara"); // NOI18N
        PergerakanPitaSuara.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PergerakanPitaSuaraKeyPressed(evt);
            }
        });
        scrollPane22.setViewportView(PergerakanPitaSuara);

        FormInput.add(scrollPane22);
        scrollPane22.setBounds(660, 540, 190, 40);

        jLabel117.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel117.setText("Pergerakan");
        jLabel117.setName("jLabel117"); // NOI18N
        FormInput.add(jLabel117);
        jLabel117.setBounds(600, 540, 60, 23);

        jLabel118.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel118.setText("Pita");
        jLabel118.setName("jLabel118"); // NOI18N
        FormInput.add(jLabel118);
        jLabel118.setBounds(600, 550, 60, 23);

        jLabel119.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel119.setText("VI. TELINGA");
        jLabel119.setName("jLabel119"); // NOI18N
        FormInput.add(jLabel119);
        jLabel119.setBounds(10, 580, 210, 23);

        jLabel120.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel120.setText("Kiri");
        jLabel120.setName("jLabel120"); // NOI18N
        FormInput.add(jLabel120);
        jLabel120.setBounds(50, 610, 70, 23);

        scrollPane23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane23.setName("scrollPane23"); // NOI18N

        LiangTelingaKiri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        LiangTelingaKiri.setColumns(20);
        LiangTelingaKiri.setRows(5);
        LiangTelingaKiri.setName("LiangTelingaKiri"); // NOI18N
        LiangTelingaKiri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LiangTelingaKiriKeyPressed(evt);
            }
        });
        scrollPane23.setViewportView(LiangTelingaKiri);

        FormInput.add(scrollPane23);
        scrollPane23.setBounds(125, 600, 190, 40);

        jLabel121.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel121.setText("Kanan");
        jLabel121.setName("jLabel121"); // NOI18N
        FormInput.add(jLabel121);
        jLabel121.setBounds(330, 610, 60, 23);

        scrollPane24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane24.setName("scrollPane24"); // NOI18N

        LiangTelingaKanan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        LiangTelingaKanan.setColumns(20);
        LiangTelingaKanan.setRows(5);
        LiangTelingaKanan.setName("LiangTelingaKanan"); // NOI18N
        LiangTelingaKanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LiangTelingaKananKeyPressed(evt);
            }
        });
        scrollPane24.setViewportView(LiangTelingaKanan);

        FormInput.add(scrollPane24);
        scrollPane24.setBounds(400, 600, 190, 40);

        jLabel122.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel122.setText("Liang Telinga");
        jLabel122.setName("jLabel122"); // NOI18N
        FormInput.add(jLabel122);
        jLabel122.setBounds(50, 600, 70, 23);

        jLabel123.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel123.setText("Liang Telinga");
        jLabel123.setName("jLabel123"); // NOI18N
        FormInput.add(jLabel123);
        jLabel123.setBounds(325, 600, 70, 23);

        jLabel124.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel124.setText("Kiri");
        jLabel124.setName("jLabel124"); // NOI18N
        FormInput.add(jLabel124);
        jLabel124.setBounds(40, 710, 60, 23);

        scrollPane25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane25.setName("scrollPane25"); // NOI18N

        MembranaTympanisKiri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        MembranaTympanisKiri.setColumns(20);
        MembranaTympanisKiri.setRows(5);
        MembranaTympanisKiri.setName("MembranaTympanisKiri"); // NOI18N
        MembranaTympanisKiri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MembranaTympanisKiriKeyPressed(evt);
            }
        });
        scrollPane25.setViewportView(MembranaTympanisKiri);

        FormInput.add(scrollPane25);
        scrollPane25.setBounds(125, 650, 190, 40);

        jLabel125.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel125.setText("Kanan");
        jLabel125.setName("jLabel125"); // NOI18N
        FormInput.add(jLabel125);
        jLabel125.setBounds(320, 710, 70, 23);

        scrollPane26.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane26.setName("scrollPane26"); // NOI18N

        MembranaTympanisKanan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        MembranaTympanisKanan.setColumns(20);
        MembranaTympanisKanan.setRows(5);
        MembranaTympanisKanan.setName("MembranaTympanisKanan"); // NOI18N
        MembranaTympanisKanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MembranaTympanisKananKeyPressed(evt);
            }
        });
        scrollPane26.setViewportView(MembranaTympanisKanan);

        FormInput.add(scrollPane26);
        scrollPane26.setBounds(400, 650, 190, 40);

        jLabel126.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel126.setText("Membrana");
        jLabel126.setName("jLabel126"); // NOI18N
        FormInput.add(jLabel126);
        jLabel126.setBounds(50, 650, 60, 23);

        jLabel127.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel127.setText("Tympanis");
        jLabel127.setName("jLabel127"); // NOI18N
        FormInput.add(jLabel127);
        jLabel127.setBounds(50, 660, 60, 23);

        jLabel128.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel128.setText("Membrana");
        jLabel128.setName("jLabel128"); // NOI18N
        FormInput.add(jLabel128);
        jLabel128.setBounds(330, 650, 70, 23);

        jLabel129.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel129.setText("Tympanis");
        jLabel129.setName("jLabel129"); // NOI18N
        FormInput.add(jLabel129);
        jLabel129.setBounds(330, 660, 70, 23);

        jLabel130.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel130.setText("Reflex Cahaya");
        jLabel130.setName("jLabel130"); // NOI18N
        FormInput.add(jLabel130);
        jLabel130.setBounds(40, 700, 80, 23);

        scrollPane27.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane27.setName("scrollPane27"); // NOI18N

        ReflexCahayaKiri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        ReflexCahayaKiri.setColumns(20);
        ReflexCahayaKiri.setRows(5);
        ReflexCahayaKiri.setName("ReflexCahayaKiri"); // NOI18N
        ReflexCahayaKiri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ReflexCahayaKiriKeyPressed(evt);
            }
        });
        scrollPane27.setViewportView(ReflexCahayaKiri);

        FormInput.add(scrollPane27);
        scrollPane27.setBounds(125, 700, 190, 40);

        jLabel131.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel131.setText("Reflex Cahaya");
        jLabel131.setName("jLabel131"); // NOI18N
        FormInput.add(jLabel131);
        jLabel131.setBounds(320, 700, 80, 23);

        scrollPane28.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane28.setName("scrollPane28"); // NOI18N

        ReflexCahayaKanan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        ReflexCahayaKanan.setColumns(20);
        ReflexCahayaKanan.setRows(5);
        ReflexCahayaKanan.setName("ReflexCahayaKanan"); // NOI18N
        ReflexCahayaKanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ReflexCahayaKananKeyPressed(evt);
            }
        });
        scrollPane28.setViewportView(ReflexCahayaKanan);

        FormInput.add(scrollPane28);
        scrollPane28.setBounds(400, 700, 190, 40);

        jLabel132.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel132.setText("Kiri");
        jLabel132.setName("jLabel132"); // NOI18N
        FormInput.add(jLabel132);
        jLabel132.setBounds(50, 670, 60, 23);

        jLabel133.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel133.setText("Kanan");
        jLabel133.setName("jLabel133"); // NOI18N
        FormInput.add(jLabel133);
        jLabel133.setBounds(330, 670, 70, 23);

        jLabel134.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel134.setText("VII. AUDIOGRAM");
        jLabel134.setName("jLabel134"); // NOI18N
        FormInput.add(jLabel134);
        jLabel134.setBounds(10, 740, 210, 23);

        scrollPane29.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane29.setName("scrollPane29"); // NOI18N

        Audiogram.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Audiogram.setColumns(20);
        Audiogram.setRows(5);
        Audiogram.setName("Audiogram"); // NOI18N
        Audiogram.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AudiogramKeyPressed(evt);
            }
        });
        scrollPane29.setViewportView(Audiogram);

        FormInput.add(scrollPane29);
        scrollPane29.setBounds(125, 760, 470, 40);

        jLabel135.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel135.setText("VIII. KESAN");
        jLabel135.setName("jLabel135"); // NOI18N
        FormInput.add(jLabel135);
        jLabel135.setBounds(10, 800, 210, 23);

        scrollPane30.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane30.setName("scrollPane30"); // NOI18N

        Kesan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Kesan.setColumns(20);
        Kesan.setRows(5);
        Kesan.setName("Kesan"); // NOI18N
        Kesan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KesanKeyPressed(evt);
            }
        });
        scrollPane30.setViewportView(Kesan);

        FormInput.add(scrollPane30);
        scrollPane30.setBounds(125, 810, 470, 40);

        jLabel136.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel136.setText("IX. SARAN");
        jLabel136.setName("jLabel136"); // NOI18N
        FormInput.add(jLabel136);
        jLabel136.setBounds(10, 850, 210, 23);

        scrollPane31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane31.setName("scrollPane31"); // NOI18N

        Saran.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Saran.setColumns(20);
        Saran.setRows(5);
        Saran.setName("Saran"); // NOI18N
        Saran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SaranKeyPressed(evt);
            }
        });
        scrollPane31.setViewportView(Saran);

        FormInput.add(scrollPane31);
        scrollPane31.setBounds(125, 860, 470, 40);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Pemeriksaan", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "27-05-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "27-05-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Pemeriksaan", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{            
            Valid.pindah(evt,TCari,BtnDokter);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnPetugas,"Petugas");
        }else if(Keluhan.getText().equals("")){
            Valid.textKosong(Keluhan, "Keluhan");
        }else if(Audiogram.getText().equals("")){
            Valid.textKosong(Audiogram, "Audiogram");
        }else if(Saran.getText().equals("")){
            Valid.textKosong(Saran, "Saran");
        }else if(Kesan.getText().trim().equals("")){
            Valid.textKosong(Kesan,"Kesan");
        }else{
            if(Sequel.menyimpantf("penilaian_mcu_tht","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",30,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),KdPetugas.getText(),
                Keluhan.getText(),HidungMukosaKiri.getText(),HidungMukosaKanan.getText(),
                HidungSekretKiri.getText(),HidungSekretKanan.getText(),HidungConchaKiri.getText(),HidungConchaKanan.getText(),
                SeptumDeviasiKiri.getText(),SeptumDeviasiKanan.getText(),Massa.getText(),NasopharingMukosa.getText(),NasopharingSekret.getText(),
                Pharinx.getText(),Tonsil.getText(),Epiglotis.getText(),Arytenoid.getText(),PergerakanPitaSuara.getText(),
                LiangTelingaKiri.getText(),LiangTelingaKanan.getText(),MembranaTympanisKiri.getText(),MembranaTympanisKanan.getText(),
                ReflexCahayaKiri.getText(),ReflexCahayaKanan.getText(),
                Audiogram.getText(),Kesan.getText(),Saran.getText()
                })==true){
                    JOptionPane.showMessageDialog(rootPane,"Data berhasil disimpan!");
                    emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,Saran,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()) || akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh dokter/petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }  
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(NmDokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Dokter");
        }else if(NmPetugas.getText().trim().equals("")){
            Valid.textKosong(BtnPetugas,"Petugas");
        }else if(Keluhan.getText().equals("")){
            Valid.textKosong(Keluhan, "Keluhan");
        }else if(Audiogram.getText().equals("")){
            Valid.textKosong(Audiogram, "Audiogram");
        }else if(Saran.getText().equals("")){
            Valid.textKosong(Saran, "Saran");
        }else if(Kesan.getText().trim().equals("")){
            Valid.textKosong(Kesan,"Kesan");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()) || akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString())){
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh dokter/petugas yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
//        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        if(tabMode.getRowCount()==0){
//            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//            BtnBatal.requestFocus();
//        }else if(tabMode.getRowCount()!=0){
//            try{
//                if(TCari.getText().trim().equals("")){
//                    ps=koneksi.prepareStatement(
//                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_medis_ralan.tanggal,"+
//                        "penilaian_medis_ralan.kd_dokter,penilaian_medis_ralan.anamnesis,penilaian_medis_ralan.hubungan,penilaian_medis_ralan.keluhan_utama,penilaian_medis_ralan.rps,penilaian_medis_ralan.rpk,penilaian_medis_ralan.rpd,penilaian_medis_ralan.rpo,penilaian_medis_ralan.alergi,"+
//                        "penilaian_medis_ralan.keadaan,penilaian_medis_ralan.gcs,penilaian_medis_ralan.kesadaran,penilaian_medis_ralan.td,penilaian_medis_ralan.nadi,penilaian_medis_ralan.rr,penilaian_medis_ralan.suhu,penilaian_medis_ralan.spo,penilaian_medis_ralan.bb,penilaian_medis_ralan.tb,"+
//                        "penilaian_medis_ralan.kepala,penilaian_medis_ralan.gigi,penilaian_medis_ralan.tht,penilaian_medis_ralan.thoraks,penilaian_medis_ralan.abdomen,penilaian_medis_ralan.ekstremitas,penilaian_medis_ralan.genital,penilaian_medis_ralan.kulit,"+
//                        "penilaian_medis_ralan.ket_fisik,penilaian_medis_ralan.ket_lokalis,penilaian_medis_ralan.penunjang,penilaian_medis_ralan.diagnosis,penilaian_medis_ralan.tata,penilaian_medis_ralan.konsulrujuk,dokter.nm_dokter "+
//                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                        "inner join penilaian_medis_ralan on reg_periksa.no_rawat=penilaian_medis_ralan.no_rawat "+
//                        "inner join dokter on penilaian_medis_ralan.kd_dokter=dokter.kd_dokter where "+
//                        "penilaian_medis_ralan.tanggal between ? and ? order by penilaian_medis_ralan.tanggal");
//                }else{
//                    ps=koneksi.prepareStatement(
//                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_medis_ralan.tanggal,"+
//                        "penilaian_medis_ralan.kd_dokter,penilaian_medis_ralan.anamnesis,penilaian_medis_ralan.hubungan,penilaian_medis_ralan.keluhan_utama,penilaian_medis_ralan.rps,penilaian_medis_ralan.rpk,penilaian_medis_ralan.rpd,penilaian_medis_ralan.rpo,penilaian_medis_ralan.alergi,"+
//                        "penilaian_medis_ralan.keadaan,penilaian_medis_ralan.gcs,penilaian_medis_ralan.kesadaran,penilaian_medis_ralan.td,penilaian_medis_ralan.nadi,penilaian_medis_ralan.rr,penilaian_medis_ralan.suhu,penilaian_medis_ralan.spo,penilaian_medis_ralan.bb,penilaian_medis_ralan.tb,"+
//                        "penilaian_medis_ralan.kepala,penilaian_medis_ralan.gigi,penilaian_medis_ralan.tht,penilaian_medis_ralan.thoraks,penilaian_medis_ralan.abdomen,penilaian_medis_ralan.ekstremitas,penilaian_medis_ralan.genital,penilaian_medis_ralan.kulit,"+
//                        "penilaian_medis_ralan.ket_fisik,penilaian_medis_ralan.ket_lokalis,penilaian_medis_ralan.penunjang,penilaian_medis_ralan.diagnosis,penilaian_medis_ralan.tata,penilaian_medis_ralan.konsulrujuk,dokter.nm_dokter "+
//                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
//                        "inner join penilaian_medis_ralan on reg_periksa.no_rawat=penilaian_medis_ralan.no_rawat "+
//                        "inner join dokter on penilaian_medis_ralan.kd_dokter=dokter.kd_dokter where "+
//                        "penilaian_medis_ralan.tanggal between ? and ? and (reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
//                        "penilaian_medis_ralan.kd_dokter like ? or dokter.nm_dokter like ?) order by penilaian_medis_ralan.tanggal");
//                }
//
//                try {
//                    if(TCari.getText().trim().equals("")){
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                    }else{
//                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
//                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
//                        ps.setString(3,"%"+TCari.getText()+"%");
//                        ps.setString(4,"%"+TCari.getText()+"%");
//                        ps.setString(5,"%"+TCari.getText()+"%");
//                        ps.setString(6,"%"+TCari.getText()+"%");
//                        ps.setString(7,"%"+TCari.getText()+"%");
//                    }  
//                    rs=ps.executeQuery();
//                    htmlContent = new StringBuilder();
//                    htmlContent.append(                             
//                        "<tr class='isi'>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='105px'><b>No.Rawat</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='70px'><b>No.RM</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Nama Pasien</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='65px'><b>Tgl.Lahir</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='55px'><b>J.K.</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>NIP</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Nama Dokter</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='115px'><b>Tanggal</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Anamnesis</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='100px'><b>Hubungan</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Keluhan Utama</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Sekarang</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Dahulu</b></td>"+
//			    "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penyakit Keluarga</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Riwayat Penggunaan Obat</b></td>"+   //CUSTOM MUHSIN PENGGUNAKAN JADI PENGGUNAAN
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='120px'><b>Riwayat Alergi</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='90px'><b>Keadaan Umum</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='50px'><b>GCS</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kesadaran</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='60px'><b>TD(mmHg)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='75px'><b>Nadi(x/menit)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='67px'><b>RR(x/menit)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>Suhu</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>SpO2</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>BB(Kg)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='40px'><b>TB(cm)</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kepala</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Gigi & Mulut</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>THT</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Thoraks</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Abdomen</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Genital & Anus</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Ekstremitas</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>Kulit</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Ket.Pemeriksaan Fisik</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='200px'><b>Ket.Status Lokalis</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='170px'><b>Pemeriksaa Penunjang</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Diagnosis/Asesmen</b></td>"+
//			    "<td valign='middle' bgcolor='#FFFAF8' align='center' width='300px'><b>Tatalaksana</b></td>"+
//                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Konsul/Rujuk</b></td>"+
//                        "</tr>"
//                    );
//                    while(rs.next()){
//                        htmlContent.append(
//                            "<tr class='isi'>"+
//                               "<td valign='top'>"+rs.getString("no_rawat")+"</td>"+
//                               "<td valign='top'>"+rs.getString("no_rkm_medis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nm_pasien")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tgl_lahir")+"</td>"+
//                               "<td valign='top'>"+rs.getString("jk")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kd_dokter")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nm_dokter")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tanggal")+"</td>"+
//                               "<td valign='top'>"+rs.getString("anamnesis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("hubungan")+"</td>"+
//                               "<td valign='top'>"+rs.getString("keluhan_utama")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rps")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpd")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpk")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rpo")+"</td>"+
//                               "<td valign='top'>"+rs.getString("alergi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("keadaan")+"</td>"+
//                               "<td valign='top'>"+rs.getString("gcs")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kesadaran")+"</td>"+
//                               "<td valign='top'>"+rs.getString("td")+"</td>"+
//                               "<td valign='top'>"+rs.getString("nadi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("rr")+"</td>"+
//                               "<td valign='top'>"+rs.getString("suhu")+"</td>"+
//                               "<td valign='top'>"+rs.getString("spo")+"</td>"+
//                               "<td valign='top'>"+rs.getString("bb")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tb")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kepala")+"</td>"+
//                               "<td valign='top'>"+rs.getString("gigi")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tht")+"</td>"+
//                               "<td valign='top'>"+rs.getString("thoraks")+"</td>"+
//                               "<td valign='top'>"+rs.getString("abdomen")+"</td>"+
//                               "<td valign='top'>"+rs.getString("genital")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ekstremitas")+"</td>"+
//                               "<td valign='top'>"+rs.getString("kulit")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ket_fisik")+"</td>"+
//                               "<td valign='top'>"+rs.getString("ket_lokalis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("penunjang")+"</td>"+
//                               "<td valign='top'>"+rs.getString("diagnosis")+"</td>"+
//                               "<td valign='top'>"+rs.getString("tata")+"</td>"+
//                               "<td valign='top'>"+rs.getString("konsulrujuk")+"</td>"+
//                            "</tr>");
//                    }
//                    LoadHTML.setText(
//                        "<html>"+
//                          "<table width='4400px' border='0' align='center' cellpadding='1px' cellspacing='0' class='tbl_form'>"+
//                           htmlContent.toString()+
//                          "</table>"+
//                        "</html>"
//                    );
//
//                    File g = new File("file2.css");            
//                    BufferedWriter bg = new BufferedWriter(new FileWriter(g));
//                    bg.write(
//                        ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
//                        ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
//                        ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
//                        ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
//                        ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
//                        ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
//                        ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
//                    );
//                    bg.close();
//
//                    File f = new File("DataPenilaianAwalMedisRalan.html");            
//                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));            
//                    bw.write(LoadHTML.getText().replaceAll("<head>","<head>"+
//                                "<link href=\"file2.css\" rel=\"stylesheet\" type=\"text/css\" />"+
//                                "<table width='4400px' border='0' align='center' cellpadding='3px' cellspacing='0' class='tbl_form'>"+
//                                    "<tr class='isi2'>"+
//                                        "<td valign='top' align='center'>"+
//                                            "<font size='4' face='Tahoma'>"+akses.getnamars()+"</font><br>"+
//                                            akses.getalamatrs()+", "+akses.getkabupatenrs()+", "+akses.getpropinsirs()+"<br>"+
//                                            akses.getkontakrs()+", E-mail : "+akses.getemailrs()+"<br><br>"+
//                                            "<font size='2' face='Tahoma'>DATA PENILAIAN AWAL MEDIS RAWAT JALAN<br><br></font>"+        
//                                        "</td>"+
//                                   "</tr>"+
//                                "</table>")
//                    );
//                    bw.close();                         
//                    Desktop.getDesktop().browse(f.toURI());
//                } catch (Exception e) {
//                    System.out.println("Notif : "+e);
//                } finally{
//                    if(rs!=null){
//                        rs.close();
//                    }
//                    if(ps!=null){
//                        ps.close();
//                    }
//                }
//
//            }catch(Exception e){
//                System.out.println("Notifikasi : "+e);
//            }
//        }
//        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void KdDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokterKeyPressed
        
    }//GEN-LAST:event_KdDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setAlwaysOnTop(false);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        //Valid.pindah(evt,Monitoring,BtnSimpan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void TglPemeriksaanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglPemeriksaanKeyPressed
        Valid.pindah(evt,BtnDokter,BtnPetugas);
    }//GEN-LAST:event_TglPemeriksaanKeyPressed

    private void MnSuratKeteranganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnSuratKeteranganActionPerformed
       if(tbObat.getSelectedRow()>-1){
           this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            Map<String, Object> param = new HashMap<>();
                param.put("namars",akses.getnamars());
                param.put("alamatrs",akses.getalamatrs());
                param.put("kotars",akses.getkabupatenrs());
                param.put("propinsirs",akses.getpropinsirs());
                param.put("kontakrs",akses.getkontakrs());
                param.put("emailrs",akses.getemailrs());  
                param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
                param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan")); 
                param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan")); 
                finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
                param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),6).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString())); 
                
                Valid.MyReportqry("rptCetakPenilaianMCUTHT.jasper","report","::[ Penilaian MCU THT ]::","select penilaian_mcu_tht.*,reg_periksa.*,pasien.tgl_lahir,if(pasien.jk='L','LAKI-LAKI','PEREMPUAN') as jk," +
                    "pasien.nm_pasien,pasien.tmp_lahir,dokter.kd_dokter,dokter.nm_dokter,"+ 
                    "pasien.alamat,kelurahan.nm_kel,kecamatan.nm_kec,kabupaten.nm_kab,petugas.nip,petugas.nama,"+ 
                    "CONCAT(DATE_FORMAT(pasien.tgl_lahir, '%d '),"+ 
                    "CASE MONTH(pasien.tgl_lahir) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, CONCAT(' ', YEAR(pasien.tgl_lahir))) AS ttl,dokter.no_ijn_praktek as sip " +
                    "from penilaian_mcu_tht inner join reg_periksa on penilaian_mcu_tht.no_rawat=reg_periksa.no_rawat " +
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis " +
                    "INNER JOIN kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                    "INNER JOIN kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                    "INNER JOIN kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                    "INNER JOIN dokter ON penilaian_mcu_tht.kd_dokter = dokter.kd_dokter " +
                    "INNER JOIN petugas ON penilaian_mcu_tht.kd_petugas = petugas.nip " +
                    "where penilaian_mcu_tht.no_rawat ='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' order by penilaian_mcu_tht.no_rawat",param);
           this.setCursor(Cursor.getDefaultCursor());
       }
    }//GEN-LAST:event_MnSuratKeteranganActionPerformed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdPetugasKeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed
        // TODO add your handling code here:
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);        
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void KeluhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeluhanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeluhanKeyPressed

    private void HidungMukosaKananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HidungMukosaKananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HidungMukosaKananKeyPressed

    private void HidungSekretKananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HidungSekretKananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HidungSekretKananKeyPressed

    private void HidungMukosaKiriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HidungMukosaKiriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HidungMukosaKiriKeyPressed

    private void HidungSekretKiriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HidungSekretKiriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HidungSekretKiriKeyPressed

    private void HidungConchaKiriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HidungConchaKiriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HidungConchaKiriKeyPressed

    private void HidungConchaKananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HidungConchaKananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HidungConchaKananKeyPressed

    private void SeptumDeviasiKiriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SeptumDeviasiKiriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_SeptumDeviasiKiriKeyPressed

    private void SeptumDeviasiKananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SeptumDeviasiKananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_SeptumDeviasiKananKeyPressed

    private void MassaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MassaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MassaKeyPressed

    private void NasopharingMukosaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NasopharingMukosaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_NasopharingMukosaKeyPressed

    private void NasopharingSekretKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NasopharingSekretKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_NasopharingSekretKeyPressed

    private void PharinxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PharinxKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PharinxKeyPressed

    private void TonsilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TonsilKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TonsilKeyPressed

    private void EpiglotisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EpiglotisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_EpiglotisKeyPressed

    private void ArytenoidKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ArytenoidKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ArytenoidKeyPressed

    private void PergerakanPitaSuaraKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PergerakanPitaSuaraKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PergerakanPitaSuaraKeyPressed

    private void LiangTelingaKiriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LiangTelingaKiriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_LiangTelingaKiriKeyPressed

    private void LiangTelingaKananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LiangTelingaKananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_LiangTelingaKananKeyPressed

    private void MembranaTympanisKiriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MembranaTympanisKiriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MembranaTympanisKiriKeyPressed

    private void MembranaTympanisKananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MembranaTympanisKananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MembranaTympanisKananKeyPressed

    private void ReflexCahayaKiriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ReflexCahayaKiriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ReflexCahayaKiriKeyPressed

    private void ReflexCahayaKananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ReflexCahayaKananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ReflexCahayaKananKeyPressed

    private void AudiogramKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AudiogramKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AudiogramKeyPressed

    private void KesanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KesanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KesanKeyPressed

    private void SaranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SaranKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_SaranKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMMCUTHT dialog = new RMMCUTHT(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.TextArea Arytenoid;
    private widget.TextArea Audiogram;
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPetugas;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.TextArea Epiglotis;
    private widget.PanelBiasa FormInput;
    private widget.TextArea HidungConchaKanan;
    private widget.TextArea HidungConchaKiri;
    private widget.TextArea HidungMukosaKanan;
    private widget.TextArea HidungMukosaKiri;
    private widget.TextArea HidungSekretKanan;
    private widget.TextArea HidungSekretKiri;
    private widget.TextBox Jk;
    private widget.TextBox KdDokter;
    private widget.TextBox KdPetugas;
    private widget.TextArea Keluhan;
    private widget.TextArea Kesan;
    private widget.Label LCount;
    private widget.TextArea LiangTelingaKanan;
    private widget.TextArea LiangTelingaKiri;
    private widget.editorpane LoadHTML;
    private widget.TextArea Massa;
    private widget.TextArea MembranaTympanisKanan;
    private widget.TextArea MembranaTympanisKiri;
    private javax.swing.JMenuItem MnSuratKeterangan;
    private widget.TextArea NasopharingMukosa;
    private widget.TextArea NasopharingSekret;
    private widget.TextBox NmDokter;
    private widget.TextBox NmPetugas;
    private widget.TextArea PergerakanPitaSuara;
    private widget.TextArea Pharinx;
    private widget.TextArea ReflexCahayaKanan;
    private widget.TextArea ReflexCahayaKiri;
    private widget.TextArea Saran;
    private widget.ScrollPane Scroll;
    private widget.TextArea SeptumDeviasiKanan;
    private widget.TextArea SeptumDeviasiKiri;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.TextBox TglLahir;
    private widget.Tanggal TglPemeriksaan;
    private widget.TextArea Tonsil;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel100;
    private widget.Label jLabel101;
    private widget.Label jLabel102;
    private widget.Label jLabel103;
    private widget.Label jLabel104;
    private widget.Label jLabel105;
    private widget.Label jLabel106;
    private widget.Label jLabel107;
    private widget.Label jLabel108;
    private widget.Label jLabel109;
    private widget.Label jLabel11;
    private widget.Label jLabel110;
    private widget.Label jLabel111;
    private widget.Label jLabel112;
    private widget.Label jLabel113;
    private widget.Label jLabel114;
    private widget.Label jLabel115;
    private widget.Label jLabel116;
    private widget.Label jLabel117;
    private widget.Label jLabel118;
    private widget.Label jLabel119;
    private widget.Label jLabel120;
    private widget.Label jLabel121;
    private widget.Label jLabel122;
    private widget.Label jLabel123;
    private widget.Label jLabel124;
    private widget.Label jLabel125;
    private widget.Label jLabel126;
    private widget.Label jLabel127;
    private widget.Label jLabel128;
    private widget.Label jLabel129;
    private widget.Label jLabel130;
    private widget.Label jLabel131;
    private widget.Label jLabel132;
    private widget.Label jLabel133;
    private widget.Label jLabel134;
    private widget.Label jLabel135;
    private widget.Label jLabel136;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private widget.Label jLabel94;
    private widget.Label jLabel95;
    private widget.Label jLabel96;
    private widget.Label jLabel97;
    private widget.Label jLabel98;
    private widget.Label jLabel99;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private widget.Label label11;
    private widget.Label label14;
    private widget.Label label15;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane10;
    private widget.ScrollPane scrollPane11;
    private widget.ScrollPane scrollPane12;
    private widget.ScrollPane scrollPane13;
    private widget.ScrollPane scrollPane14;
    private widget.ScrollPane scrollPane15;
    private widget.ScrollPane scrollPane16;
    private widget.ScrollPane scrollPane17;
    private widget.ScrollPane scrollPane18;
    private widget.ScrollPane scrollPane19;
    private widget.ScrollPane scrollPane20;
    private widget.ScrollPane scrollPane21;
    private widget.ScrollPane scrollPane22;
    private widget.ScrollPane scrollPane23;
    private widget.ScrollPane scrollPane24;
    private widget.ScrollPane scrollPane25;
    private widget.ScrollPane scrollPane26;
    private widget.ScrollPane scrollPane27;
    private widget.ScrollPane scrollPane28;
    private widget.ScrollPane scrollPane29;
    private widget.ScrollPane scrollPane30;
    private widget.ScrollPane scrollPane31;
    private widget.ScrollPane scrollPane6;
    private widget.ScrollPane scrollPane7;
    private widget.ScrollPane scrollPane8;
    private widget.ScrollPane scrollPane9;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().trim().equals("")){
                ps=koneksi.prepareStatement(
                        "select pmm.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk, " +
                        "p.tgl_lahir,pmm.tanggal,"+ 
                        "pmm.keluhan,pmm.hidung_mukosa_kiri,pmm.hidung_mukosa_kanan,pmm.hidung_sekret_kiri,pmm.hidung_sekret_kanan," +
                        "pmm.hidung_concha_kiri,pmm.hidung_concha_kanan,pmm.septum_deviasi_kiri,pmm.septum_deviasi_kanan,"+ 
                        "pmm.massa,pmm.nasopharing_mukosa,pmm.nasopharing_sekret,pmm.pharinx,pmm.tonsil,"+
                        "pmm.epiglotis,pmm.arytenoid,pmm.pergerakanpitasuara,pmm.liangtelinga_kiri,pmm.liangtelinga_kanan,"+
                        "pmm.membranatympanis_kiri,pmm.membranatympanis_kanan,pmm.reflexcahaya_kiri,pmm.reflexcahaya_kanan,"+
                        "pmm.audiogram,pmm.kesan,pmm.saran,"+        
                        "pmm.kd_dokter,d.nm_dokter,pmm.kd_petugas,p2.nama " +
                        "from reg_periksa inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_mcu_tht pmm on reg_periksa.no_rawat = pmm.no_rawat " +
                        "inner join dokter d on pmm.kd_dokter = d.kd_dokter " +
                        "inner join petugas p2 on pmm.kd_petugas = p2.nip " +
                        "where pmm.tanggal between ? and ? " +
                        "order by pmm.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                        "select pmm.no_rawat,p.no_rkm_medis,p.nm_pasien,if(p.jk='L','Laki-Laki','Perempuan') as jk, " +
                        "p.tgl_lahir,pmm.tanggal,"+ 
                        "pmm.keluhan,pmm.hidung_mukosa_kiri,pmm.hidung_mukosa_kanan,pmm.hidung_sekret_kiri,pmm.hidung_sekret_kanan," +
                        "pmm.hidung_concha_kiri,pmm.hidung_concha_kanan,pmm.septum_deviasi_kiri,pmm.septum_deviasi_kanan,"+ 
                        "pmm.massa,pmm.nasopharing_mukosa,pmm.nasopharing_sekret,pmm.pharinx,pmm.tonsil,"+
                        "pmm.epiglotis,pmm.arytenoid,pmm.pergerakanpitasuara,pmm.liangtelinga_kiri,pmm.liangtelinga_kanan,"+
                        "pmm.membranatympanis_kiri,pmm.membranatympanis_kanan,pmm.reflexcahaya_kiri,pmm.reflexcahaya_kanan,"+
                        "pmm.audiogram,pmm.kesan,pmm.saran,"+        
                        "pmm.kd_dokter,d.nm_dokter,pmm.kd_petugas,p2.nama " +
                        "from reg_periksa inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                        "inner join penilaian_mcu_tht pmm on reg_periksa.no_rawat = pmm.no_rawat " +
                        "inner join dokter d on pmm.kd_dokter = d.kd_dokter " +
                        "inner join petugas p2 on pmm.kd_petugas = p2.nip " +
                        "where pmm.tanggal between ? and ? and (reg_periksa.no_rawat like ? or p.no_rkm_medis like ? or p.nm_pasien like ? or pmm.kd_dokter like ? or d.nm_dokter like ?) " +
                        "order by pmm.tanggal");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("tgl_lahir"),rs.getString("jk"),rs.getString("kd_dokter"),rs.getString("nm_dokter"),rs.getString("tanggal"),
                        rs.getString("keluhan"),rs.getString("hidung_mukosa_kiri"),rs.getString("hidung_mukosa_kanan"),
                        rs.getString("hidung_sekret_kiri"),rs.getString("hidung_sekret_kanan"),rs.getString("hidung_concha_kiri"),rs.getString("hidung_concha_kanan"),
                        rs.getString("septum_deviasi_kiri"),rs.getString("septum_deviasi_kanan"),rs.getString("massa"),rs.getString("nasopharing_mukosa"),
                        rs.getString("nasopharing_sekret"),rs.getString("pharinx"),rs.getString("tonsil"),rs.getString("epiglotis"),
                        rs.getString("arytenoid"),rs.getString("pergerakanpitasuara"),rs.getString("liangtelinga_kiri"),rs.getString("liangtelinga_kanan"),
                        rs.getString("membranatympanis_kiri"),rs.getString("membranatympanis_kanan"),rs.getString("reflexcahaya_kiri"),rs.getString("reflexcahaya_kanan"),
                        rs.getString("audiogram"),rs.getString("kesan"),rs.getString("saran"),
                        rs.getString("kd_petugas"),rs.getString("nama")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TabRawat.setSelectedIndex(0);
        TglPemeriksaan.setDate(new Date());
        Keluhan.setText("");
        HidungMukosaKiri.setText("");
        HidungMukosaKanan.setText("");
        HidungSekretKiri.setText("");
        HidungSekretKanan.setText("");
        HidungConchaKiri.setText("");
        HidungConchaKanan.setText("");
        SeptumDeviasiKiri.setText("");
        SeptumDeviasiKanan.setText("");
        Massa.setText("");
        NasopharingMukosa.setText("");        
        NasopharingSekret.setText("");
        Pharinx.setText("");        
        Tonsil.setText("");
        Epiglotis.setText("");
        Arytenoid.setText("");
        PergerakanPitaSuara.setText("");        
        LiangTelingaKiri.setText("");
        LiangTelingaKanan.setText("");
        MembranaTympanisKiri.setText("");
        MembranaTympanisKanan.setText("");
        ReflexCahayaKiri.setText("");
        ReflexCahayaKanan.setText("");
        Audiogram.setText("");
        Kesan.setText("");
        Saran.setText("");        
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            Valid.SetTgl2(TglPemeriksaan,tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            Keluhan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            HidungMukosaKiri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            HidungMukosaKanan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            HidungSekretKiri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            HidungSekretKanan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            HidungConchaKiri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            HidungConchaKanan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            SeptumDeviasiKiri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            SeptumDeviasiKanan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            Massa.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            NasopharingMukosa.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            NasopharingSekret.setText(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            Pharinx.setText(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());        
            Tonsil.setText(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            Epiglotis.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            Arytenoid.setText(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            PergerakanPitaSuara.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            LiangTelingaKiri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            LiangTelingaKanan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            MembranaTympanisKiri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),27).toString());
            MembranaTympanisKanan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            ReflexCahayaKiri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            ReflexCahayaKanan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
            Audiogram.setText(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            Kesan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());
            Saran.setText(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString());
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),35).toString());
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
 
    public void setNoRm(String norwt,Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2); 
        Sequel.cariIsi("select kd_dokter from reg_periksa where no_rawat=?", KdDokter,norwt);
        Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?", NmDokter,KdDokter.getText());
        KdPetugas.setText(akses.getkode());
        Sequel.cariIsi("select nama from petugas where nip=?", NmPetugas,KdPetugas.getText());
        isRawat(); 
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_mcu());
        BtnHapus.setEnabled(akses.getpenilaian_mcu());
        BtnEdit.setEnabled(akses.getpenilaian_mcu());        
    }
    
    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from penilaian_mcu_tht where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            tampil();
            TabRawat.setSelectedIndex(1);
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("penilaian_mcu_tht","no_rawat=?","no_rawat=?,tanggal=?,kd_dokter=?,kd_petugas=?,keluhan=?,"
                + "hidung_mukosa_kiri=?,hidung_mukosa_kanan=?,hidung_sekret_kiri=?,hidung_sekret_kanan=?,hidung_concha_kiri=?,hidung_concha_kanan=?,septum_deviasi_kiri=?,septum_deviasi_kanan=?,massa=?,nasopharing_mukosa=?,nasopharing_sekret=?,pharinx=?,tonsil=?,epiglotis=?,arytenoid=?,pergerakanpitasuara=?,liangtelinga_kiri=?,liangtelinga_kanan=?,membranatympanis_kiri=?,membranatympanis_kanan=?,reflexcahaya_kiri=?,reflexcahaya_kanan=?,audiogram=?,kesan=?,saran=?",31,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglPemeriksaan.getSelectedItem()+"")+" "+TglPemeriksaan.getSelectedItem().toString().substring(11,19),
                KdDokter.getText(),KdPetugas.getText(),
                Keluhan.getText(),HidungMukosaKiri.getText(),HidungMukosaKanan.getText(),
                HidungSekretKiri.getText(),HidungSekretKanan.getText(),HidungConchaKiri.getText(),HidungConchaKanan.getText(),
                SeptumDeviasiKiri.getText(),SeptumDeviasiKanan.getText(),Massa.getText(),NasopharingMukosa.getText(),NasopharingSekret.getText(),
                Pharinx.getText(),Tonsil.getText(),Epiglotis.getText(),Arytenoid.getText(),PergerakanPitaSuara.getText(),
                LiangTelingaKiri.getText(),LiangTelingaKanan.getText(),MembranaTympanisKiri.getText(),MembranaTympanisKanan.getText(),
                ReflexCahayaKiri.getText(),ReflexCahayaKanan.getText(),
                Audiogram.getText(),Kesan.getText(),Saran.getText(),
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
            })==true){
               tampil();
               emptTeks();
               TabRawat.setSelectedIndex(1);
        }
    }
}
