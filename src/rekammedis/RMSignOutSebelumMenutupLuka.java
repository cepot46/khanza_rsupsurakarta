/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.akses;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;
import simrskhanza.DlgCariOperatorTimeOut;


/**
 *
 * @author perpustakaan
 */
public final class RMSignOutSebelumMenutupLuka extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0,pilihan=0;    
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private DlgCariOperatorTimeOut carioperatortimeout=new DlgCariOperatorTimeOut(null,false);
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private String finger="",finger2="",finger3="";
    private StringBuilder htmlContent;
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMSignOutSebelumMenutupLuka(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(628,674);

        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tgl.Lahir","J.K.","Tanggal","Jam","Kode Dokter Operator","Dokter Operator","Kode Dokter Anestesi","Dokter Anestesi",
            "Konfirmasi Prosedur","Ket Prosedur","Konfirmasi Kelengkapan Instrumen","Konfirmasi Label Spesimen","Keterangan Spesimen","Permasalahan Peralatan","Keterangan Permasalahan","Pesan Pemulihan",
            "Jumlah Instrumen Sebelum Instrumen","Jumlah Instrumen Intra","Jumlah Instrumen Tambahan","Jumlah Instrumen Pasca","Satuan",
            "Jumlah Jarum Sebelum","Jumlah Jarum Intra","Jumlah Jarum Tambahan","Jumlah Jarum Pasca","Satuan",
            "Jumlah Pisau Sebelum","Jumlah Pisau Intra","Jumlah Pisau Tambahan","Jumlah Pisau Pasca","Satuan",
            "Jumlah Kassa Sebelum","Jumlah Kassa Intra","Jumlah Kassa Tambahan","Jumlah Kassa Pasca","Satuan",
            "Item Lainnya 4","Jumlah Item 4","Jumlah Lainnya 1 Intra","Jumlah Lainnya 1 Tambahan","Jumlah Lainnya 1 Pasca","Satuan",
            "Item Lainnya 5","Jumlah Item 5","Jumlah Lainnya 2 Intra","Jumlah Lainnya 2 Tambahan","Jumlah Lainnya 2 Pasca","Satuan",
            "Item Lainnya 6","Jumlah Item 6","Jumlah Lainnya 3 Intra","Jumlah Lainnya 3 Tambahan","Jumlah Lainnya 3 Pasca","Satuan",
            "Total Sebelum","Total Intra","Total Tambahan","Total Pasca",
            "Kode Petugas","Nama Petugas"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 62; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(150);
            }else if(i==3){
                column.setPreferredWidth(65);
            }else if(i==4){
                column.setPreferredWidth(25);
            }else if(i==5){
                column.setPreferredWidth(115);
            }else if(i==6){
                column.setPreferredWidth(90);
            }else if(i==7){
                column.setPreferredWidth(160);
            }else if(i==8){
                column.setPreferredWidth(100);
            }else if(i==9){
                column.setPreferredWidth(160);
            }else if(i==10){
                column.setPreferredWidth(200);
            }else if(i==11){
                column.setPreferredWidth(150);
            }else if(i==12){
                column.setPreferredWidth(200);
            }else if(i==13){
                column.setPreferredWidth(67);
            }else if(i==14){
                column.setPreferredWidth(200);
            }else if(i==15){
                column.setPreferredWidth(200);
            }else if(i==16){
                column.setPreferredWidth(160);
            }else if(i==17){
                column.setPreferredWidth(160);
            }else if(i==18){
                column.setPreferredWidth(160);
            }else if(i==19){
                column.setPreferredWidth(160);
            }else if(i==20){
                column.setPreferredWidth(160);
            }else if(i==21){
                column.setPreferredWidth(160);
            }else if(i==22){
                column.setPreferredWidth(160);
            }else if(i==23){
                column.setPreferredWidth(160);
            }else if(i==24){
                column.setPreferredWidth(160);
            }else if(i==25){
                column.setPreferredWidth(160);
            }else if(i==26){
                column.setPreferredWidth(160);
            }else if(i==27){
                column.setPreferredWidth(160);
            }else if(i==28){
                column.setPreferredWidth(160);
            }else if(i==29){
                column.setPreferredWidth(160);
            }else if(i==30){
                column.setPreferredWidth(160);
            }else if(i==31){
                column.setPreferredWidth(160);
            }else if(i==32){
                column.setPreferredWidth(160);
            }else if(i==33){
                column.setPreferredWidth(160);
            }else if(i==34){
                column.setPreferredWidth(160);
            }else if(i==35){
                column.setPreferredWidth(160);
            }else if(i==36){
                column.setPreferredWidth(160);
            }else if(i==37){
                column.setPreferredWidth(160);
            }else if(i==38){
                column.setPreferredWidth(160);
            }else if(i==39){
                column.setPreferredWidth(160);
            }else if(i==40){
                column.setPreferredWidth(160);
            }else if(i==41){
                column.setPreferredWidth(96);
            }else if(i==42){
                column.setPreferredWidth(96);
            }else if(i==43){
                column.setPreferredWidth(96);
            }else if(i==44){
                column.setPreferredWidth(96);
            }else if(i==45){
                column.setPreferredWidth(96);
            }else if(i==46){
                column.setPreferredWidth(96);
            }else if(i==47){
                column.setPreferredWidth(96);
            }else if(i==48){
                column.setPreferredWidth(96);
            }else if(i==49){
                column.setPreferredWidth(96);
            }else if(i==50){
                column.setPreferredWidth(96);
            }else if(i==51){
                column.setPreferredWidth(96);
            }else if(i==52){
                column.setPreferredWidth(96);
            }else if(i==53){
                column.setPreferredWidth(96);
            }else if(i==54){
                column.setPreferredWidth(96);
            }else if(i==55){
                column.setPreferredWidth(96);
            }else if(i==56){
                column.setPreferredWidth(96);
            }else if(i==57){
                column.setPreferredWidth(96);
            }else if(i==58){
                column.setPreferredWidth(96);
            }else if(i==59){
                column.setPreferredWidth(96);
            }else if(i==60){
                column.setPreferredWidth(96);
            }else if(i==61){
                column.setPreferredWidth(96);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());

        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        js1.setDocument(new batasInput((byte)5).getKata(js1));
        ji1.setDocument(new batasInput((byte)5).getKata(ji1));
        jt1.setDocument(new batasInput((byte)5).getKata(jt1));
        js2.setDocument(new batasInput((byte)5).getKata(js2));
        ji2.setDocument(new batasInput((byte)5).getKata(ji2));
        jt2.setDocument(new batasInput((byte)5).getKata(jt2));
        js3.setDocument(new batasInput((byte)5).getKata(js3));
        ji3.setDocument(new batasInput((byte)5).getKata(ji3));
        jt3.setDocument(new batasInput((byte)5).getKata(jt3));
        js4.setDocument(new batasInput((byte)5).getKata(js4));
        ji4.setDocument(new batasInput((byte)5).getKata(ji4));
        jt4.setDocument(new batasInput((byte)5).getKata(jt4));
        js5.setDocument(new batasInput((byte)5).getKata(js5));
        ji5.setDocument(new batasInput((byte)5).getKata(ji5));
        jt5.setDocument(new batasInput((byte)5).getKata(jt5));
        js6.setDocument(new batasInput((byte)5).getKata(js6));
        ji6.setDocument(new batasInput((byte)5).getKata(ji6));
        jt6.setDocument(new batasInput((byte)5).getKata(jt6));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){     
                    KdPetugasOK.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugasOK.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                    btnPetugasOK.requestFocus();
                }   
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){      
                    if(pilihan==1){
                        KodeDokterOperator.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NamaDokterOperator.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                        btnDokterOperator.requestFocus();
                    }else if(pilihan==2){
                        KodeDokterAnestesi.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                        NamaDokterAnestesi.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                        btnDokterAnestesi.requestFocus();
                    }   
                }  
                    
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        jp1.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
        });
        jp2.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
        });
        jp3.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
        });
        jp4.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
        });
        jp5.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
        });
        jp6.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
        });
        jp8.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                isjml();
                jml_js();
                jml_ji();
                jml_jt();
            }
        });
        js1.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_1();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_1();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_1();
            }
        });
        jt1.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_1();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_1();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_1();
            }
        });
        js2.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_2();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_2();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_2();
            }
        });
        jt2.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_2();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_2();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_2();
            }
        });
        js3.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_3();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_3();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_3();
            }
        });
        jt3.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_3();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_3();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_3();
            }
        });
        js4.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_4();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_4();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_4();
            }
        });
        jt4.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_4();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_4();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_4();
            }
        });
        js5.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_5();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_5();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_5();
            }
        });
        jt5.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_5();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_5();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_5();
            }
        });
        js6.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_6();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_6();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_6();
            }
        });
        jt6.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_6();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_6();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_6();
            }
        });
        js7.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_7();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_7();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_7();
            }
        });
        jt7.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_7();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_7();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_7();
            }
        });
        ji1.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_ji();
            }
        });
        ji2.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_ji();
            }
        });
        ji3.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_ji();
            }
        });
        ji4.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_ji();
            }
        });
        ji5.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_ji();
            }
        });
        ji6.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_ji();
            }
        });
        ji7.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                jml_ji();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                jml_ji();
            }
        });
        carioperatortimeout.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(carioperatortimeout.getTable().getSelectedRow()!= -1){
                        KodeDokterOperator.setText(carioperatortimeout.getTable().getValueAt(carioperatortimeout.getTable().getSelectedRow(),7).toString());
                        NamaDokterOperator.setText(carioperatortimeout.getTable().getValueAt(carioperatortimeout.getTable().getSelectedRow(),8).toString());
                        KodeDokterAnestesi.setText(carioperatortimeout.getTable().getValueAt(carioperatortimeout.getTable().getSelectedRow(),11).toString());
                        NamaDokterAnestesi.setText(carioperatortimeout.getTable().getValueAt(carioperatortimeout.getTable().getSelectedRow(),12).toString());
                        KdPetugasOK.setText(carioperatortimeout.getTable().getValueAt(carioperatortimeout.getTable().getSelectedRow(),19).toString());
                        NmPetugasOK.setText(carioperatortimeout.getTable().getValueAt(carioperatortimeout.getTable().getSelectedRow(),20).toString());
                   
                }   
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        ChkInput.setSelected(false);
        isForm();
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
        jam();
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnSignOutSebelumMenutupLuka = new javax.swing.JMenuItem();
        LoadHTML = new widget.editorpane();
        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        jPanel3 = new javax.swing.JPanel();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        BtnKeluar = new widget.Button();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        PanelInput = new javax.swing.JPanel();
        ChkInput = new widget.CekBox();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        jLabel4 = new widget.Label();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        jLabel16 = new widget.Label();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        jLabel20 = new widget.Label();
        jLabel23 = new widget.Label();
        KodeDokterOperator = new widget.TextBox();
        NamaDokterOperator = new widget.TextBox();
        btnDokterOperator = new widget.Button();
        btnDokterAnestesi = new widget.Button();
        NamaDokterAnestesi = new widget.TextBox();
        KodeDokterAnestesi = new widget.TextBox();
        jLabel24 = new widget.Label();
        jLabel26 = new widget.Label();
        KdPetugasOK = new widget.TextBox();
        NmPetugasOK = new widget.TextBox();
        btnPetugasOK = new widget.Button();
        jLabel5 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel54 = new widget.Label();
        jLabel28 = new widget.Label();
        cProsedur = new widget.ComboBox();
        jLabel30 = new widget.Label();
        cInstrumen = new widget.ComboBox();
        jLabel55 = new widget.Label();
        cSpesimen = new widget.ComboBox();
        jLabel56 = new widget.Label();
        jLabel57 = new widget.Label();
        scrollPane2 = new widget.ScrollPane();
        Permasalahan = new widget.TextArea();
        scrollPane3 = new widget.ScrollPane();
        PesanPemulihan = new widget.TextArea();
        jLabel31 = new widget.Label();
        Tanggal = new widget.Tanggal();
        Jam = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Detik = new widget.ComboBox();
        ChkJam = new widget.CekBox();
        ket_prosedur = new widget.TextBox();
        ket_label = new widget.TextBox();
        cmasalah = new widget.ComboBox();
        jp1 = new widget.TextBox();
        si1 = new widget.ComboBox();
        js1 = new widget.TextBox();
        ji1 = new widget.TextBox();
        jt1 = new widget.TextBox();
        jLabel33 = new widget.Label();
        js2 = new widget.TextBox();
        ji2 = new widget.TextBox();
        si2 = new widget.ComboBox();
        jt2 = new widget.TextBox();
        jp2 = new widget.TextBox();
        jLabel34 = new widget.Label();
        js3 = new widget.TextBox();
        ji3 = new widget.TextBox();
        si3 = new widget.ComboBox();
        jt3 = new widget.TextBox();
        jp3 = new widget.TextBox();
        jLabel35 = new widget.Label();
        js4 = new widget.TextBox();
        ji4 = new widget.TextBox();
        si4 = new widget.ComboBox();
        jt4 = new widget.TextBox();
        jp4 = new widget.TextBox();
        jLabel36 = new widget.Label();
        js5 = new widget.TextBox();
        ji5 = new widget.TextBox();
        si5 = new widget.ComboBox();
        jt5 = new widget.TextBox();
        jp5 = new widget.TextBox();
        jLabel37 = new widget.Label();
        js6 = new widget.TextBox();
        ji6 = new widget.TextBox();
        si6 = new widget.ComboBox();
        jt6 = new widget.TextBox();
        jp7 = new widget.TextBox();
        jp6 = new widget.TextBox();
        jLabel38 = new widget.Label();
        jLabel39 = new widget.Label();
        jLabel32 = new widget.Label();
        jLabel40 = new widget.Label();
        jLabel41 = new widget.Label();
        jLabel42 = new widget.Label();
        js_tot = new widget.TextBox();
        jt_tot = new widget.TextBox();
        ji_tot = new widget.TextBox();
        lainnya1 = new widget.TextBox();
        lainnya2 = new widget.TextBox();
        lainnya3 = new widget.TextBox();
        jLabel43 = new widget.Label();
        js7 = new widget.TextBox();
        ji7 = new widget.TextBox();
        jt7 = new widget.TextBox();
        jp8 = new widget.TextBox();
        si7 = new widget.ComboBox();
        label15 = new widget.Label();
        btnDataOperatorTimeOut = new widget.Button();

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnSignOutSebelumMenutupLuka.setBackground(new java.awt.Color(255, 255, 254));
        MnSignOutSebelumMenutupLuka.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnSignOutSebelumMenutupLuka.setForeground(new java.awt.Color(50, 50, 50));
        MnSignOutSebelumMenutupLuka.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnSignOutSebelumMenutupLuka.setText("Formulir Sign-Out Sebelum Menutup Luka");
        MnSignOutSebelumMenutupLuka.setName("MnSignOutSebelumMenutupLuka"); // NOI18N
        MnSignOutSebelumMenutupLuka.setPreferredSize(new java.awt.Dimension(270, 26));
        MnSignOutSebelumMenutupLuka.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnSignOutSebelumMenutupLukaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnSignOutSebelumMenutupLuka);

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Sign-Out Sebelum Menutup Luka ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(44, 100));
        jPanel3.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass8.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass8.add(LCount);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        jPanel3.add(panelGlass8, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tanggal :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "20-09-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "20-09-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(310, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnAll);

        jPanel3.add(panelGlass9, java.awt.BorderLayout.PAGE_START);

        internalFrame1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        PanelInput.setName("PanelInput"); // NOI18N
        PanelInput.setOpaque(false);
        PanelInput.setPreferredSize(new java.awt.Dimension(192, 366));
        PanelInput.setLayout(new java.awt.BorderLayout(1, 1));

        ChkInput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setMnemonic('I');
        ChkInput.setText(".: Input Data");
        ChkInput.setToolTipText("Alt+I");
        ChkInput.setBorderPainted(true);
        ChkInput.setBorderPaintedFlat(true);
        ChkInput.setFocusable(false);
        ChkInput.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ChkInput.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ChkInput.setName("ChkInput"); // NOI18N
        ChkInput.setPreferredSize(new java.awt.Dimension(192, 20));
        ChkInput.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkInputActionPerformed(evt);
            }
        });
        PanelInput.add(ChkInput, java.awt.BorderLayout.PAGE_END);

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(250, 255, 245));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(100, 880));
        FormInput.setLayout(null);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("No.Rawat");
        jLabel4.setName("jLabel4"); // NOI18N
        FormInput.add(jLabel4);
        jLabel4.setBounds(21, 10, 75, 23);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(79, 10, 141, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        TPasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TPasienKeyPressed(evt);
            }
        });
        FormInput.add(TPasien);
        TPasien.setBounds(336, 10, 285, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        TNoRM.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRMKeyPressed(evt);
            }
        });
        FormInput.add(TNoRM);
        TNoRM.setBounds(222, 10, 112, 23);

        jLabel16.setText("Tanggal :");
        jLabel16.setName("jLabel16"); // NOI18N
        jLabel16.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel16);
        jLabel16.setBounds(430, 70, 75, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(625, 10, 60, 23);

        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(690, 10, 110, 23);

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setText("Dilakukan sebelum pasien meninggalkan Kamar Bedah, dihadiri oleh perawat, ahli anestesi, operator I & II");
        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setName("jLabel20"); // NOI18N
        FormInput.add(jLabel20);
        jLabel20.setBounds(30, 140, 650, 23);

        jLabel23.setText("Operator :");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(10, 40, 80, 23);

        KodeDokterOperator.setEditable(false);
        KodeDokterOperator.setHighlighter(null);
        KodeDokterOperator.setName("KodeDokterOperator"); // NOI18N
        FormInput.add(KodeDokterOperator);
        KodeDokterOperator.setBounds(100, 40, 97, 23);

        NamaDokterOperator.setEditable(false);
        NamaDokterOperator.setName("NamaDokterOperator"); // NOI18N
        FormInput.add(NamaDokterOperator);
        NamaDokterOperator.setBounds(200, 40, 175, 23);

        btnDokterOperator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnDokterOperator.setMnemonic('2');
        btnDokterOperator.setToolTipText("ALt+2");
        btnDokterOperator.setName("btnDokterOperator"); // NOI18N
        btnDokterOperator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDokterOperatorActionPerformed(evt);
            }
        });
        btnDokterOperator.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnDokterOperatorKeyPressed(evt);
            }
        });
        FormInput.add(btnDokterOperator);
        btnDokterOperator.setBounds(380, 40, 28, 23);

        btnDokterAnestesi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnDokterAnestesi.setMnemonic('2');
        btnDokterAnestesi.setToolTipText("ALt+2");
        btnDokterAnestesi.setName("btnDokterAnestesi"); // NOI18N
        btnDokterAnestesi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDokterAnestesiActionPerformed(evt);
            }
        });
        btnDokterAnestesi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnDokterAnestesiKeyPressed(evt);
            }
        });
        FormInput.add(btnDokterAnestesi);
        btnDokterAnestesi.setBounds(380, 70, 28, 23);

        NamaDokterAnestesi.setEditable(false);
        NamaDokterAnestesi.setName("NamaDokterAnestesi"); // NOI18N
        FormInput.add(NamaDokterAnestesi);
        NamaDokterAnestesi.setBounds(200, 70, 175, 23);

        KodeDokterAnestesi.setEditable(false);
        KodeDokterAnestesi.setHighlighter(null);
        KodeDokterAnestesi.setName("KodeDokterAnestesi"); // NOI18N
        FormInput.add(KodeDokterAnestesi);
        KodeDokterAnestesi.setBounds(100, 70, 97, 23);

        jLabel24.setText("Dokter Anestesi :");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(0, 70, 90, 23);

        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel26.setText("Perawat Sirkuler :");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(420, 40, 90, 23);

        KdPetugasOK.setEditable(false);
        KdPetugasOK.setHighlighter(null);
        KdPetugasOK.setName("KdPetugasOK"); // NOI18N
        KdPetugasOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdPetugasOKActionPerformed(evt);
            }
        });
        FormInput.add(KdPetugasOK);
        KdPetugasOK.setBounds(510, 40, 100, 23);

        NmPetugasOK.setEditable(false);
        NmPetugasOK.setName("NmPetugasOK"); // NOI18N
        FormInput.add(NmPetugasOK);
        NmPetugasOK.setBounds(620, 40, 180, 23);

        btnPetugasOK.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnPetugasOK.setMnemonic('2');
        btnPetugasOK.setToolTipText("ALt+2");
        btnPetugasOK.setName("btnPetugasOK"); // NOI18N
        btnPetugasOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPetugasOKActionPerformed(evt);
            }
        });
        btnPetugasOK.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPetugasOKKeyPressed(evt);
            }
        });
        FormInput.add(btnPetugasOK);
        btnPetugasOK.setBounds(800, 40, 28, 23);

        jLabel5.setText(":");
        jLabel5.setName("jLabel5"); // NOI18N
        FormInput.add(jLabel5);
        jLabel5.setBounds(0, 10, 75, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 133, 810, 3);

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("Tim keperawatan secara lisan mengkonfirmasi di hadapan Tim : ");
        jLabel54.setName("jLabel54"); // NOI18N
        FormInput.add(jLabel54);
        jLabel54.setBounds(30, 170, 350, 23);

        jLabel28.setText("Nama Prosedur :");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(20, 200, 90, 23);

        cProsedur.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        cProsedur.setName("cProsedur"); // NOI18N
        cProsedur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cProsedurKeyPressed(evt);
            }
        });
        FormInput.add(cProsedur);
        cProsedur.setBounds(120, 200, 80, 23);

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel30.setText("[Jumlah Pasca]");
        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(510, 280, 90, 23);

        cInstrumen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        cInstrumen.setName("cInstrumen"); // NOI18N
        cInstrumen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cInstrumenKeyPressed(evt);
            }
        });
        FormInput.add(cInstrumen);
        cInstrumen.setBounds(340, 230, 80, 23);

        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel55.setText("Label spesimen (minimal terdapat Nama Spesimen, Asal Jaringan, Nama pasien, Tanggal lahir,Nomor RM, Label /  Barcode Pasien ) :");
        jLabel55.setName("jLabel55"); // NOI18N
        FormInput.add(jLabel55);
        jLabel55.setBounds(30, 600, 640, 23);

        cSpesimen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lengkap", "Tidak Lengkap", "Tidak Ada Pemeriksaan Spesimen" }));
        cSpesimen.setName("cSpesimen"); // NOI18N
        cSpesimen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cSpesimenKeyPressed(evt);
            }
        });
        FormInput.add(cSpesimen);
        cSpesimen.setBounds(30, 630, 110, 23);

        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel56.setText("Kepada Operator, Dokter Anestesi dan Tim Keperawatan, Apakah terdapat pesan khusus untuk pemulihan pasien ?");
        jLabel56.setName("jLabel56"); // NOI18N
        FormInput.add(jLabel56);
        jLabel56.setBounds(30, 760, 560, 23);

        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel57.setText("Apakah terdapat permasalahan peralatan yang perlu disampaikan ?");
        jLabel57.setName("jLabel57"); // NOI18N
        FormInput.add(jLabel57);
        jLabel57.setBounds(30, 660, 340, 23);

        scrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane2.setName("scrollPane2"); // NOI18N

        Permasalahan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Permasalahan.setColumns(20);
        Permasalahan.setRows(5);
        Permasalahan.setName("Permasalahan"); // NOI18N
        Permasalahan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PermasalahanKeyPressed(evt);
            }
        });
        scrollPane2.setViewportView(Permasalahan);

        FormInput.add(scrollPane2);
        scrollPane2.setBounds(30, 690, 560, 60);

        scrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane3.setName("scrollPane3"); // NOI18N

        PesanPemulihan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        PesanPemulihan.setColumns(20);
        PesanPemulihan.setRows(5);
        PesanPemulihan.setName("PesanPemulihan"); // NOI18N
        PesanPemulihan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PesanPemulihanKeyPressed(evt);
            }
        });
        scrollPane3.setViewportView(PesanPemulihan);

        FormInput.add(scrollPane3);
        scrollPane3.setBounds(30, 790, 560, 60);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("Kelengkapan Instrumen, spons, jarum, dan kassa sudah sesuai :");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(30, 230, 320, 23);

        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "20-09-2024" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.setPreferredSize(new java.awt.Dimension(90, 23));
        FormInput.add(Tanggal);
        Tanggal.setBounds(510, 70, 110, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setName("Jam"); // NOI18N
        Jam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JamActionPerformed(evt);
            }
        });
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        FormInput.add(Jam);
        Jam.setBounds(630, 70, 50, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        FormInput.add(Menit);
        Menit.setBounds(690, 70, 50, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        FormInput.add(Detik);
        Detik.setBounds(750, 70, 50, 23);

        ChkJam.setBorder(null);
        ChkJam.setSelected(true);
        ChkJam.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkJam.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkJam.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkJam.setName("ChkJam"); // NOI18N
        FormInput.add(ChkJam);
        ChkJam.setBounds(800, 70, 23, 23);

        ket_prosedur.setHighlighter(null);
        ket_prosedur.setName("ket_prosedur"); // NOI18N
        ket_prosedur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ket_prosedurKeyPressed(evt);
            }
        });
        FormInput.add(ket_prosedur);
        ket_prosedur.setBounds(210, 200, 380, 23);

        ket_label.setHighlighter(null);
        ket_label.setName("ket_label"); // NOI18N
        ket_label.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ket_labelKeyPressed(evt);
            }
        });
        FormInput.add(ket_label);
        ket_label.setBounds(220, 630, 440, 23);

        cmasalah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ada", "Tidak Ada" }));
        cmasalah.setName("cmasalah"); // NOI18N
        cmasalah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmasalahKeyPressed(evt);
            }
        });
        FormInput.add(cmasalah);
        cmasalah.setBounds(360, 660, 110, 23);

        jp1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jp1.setHighlighter(null);
        jp1.setName("jp1"); // NOI18N
        jp1.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jp1InputMethodTextChanged(evt);
            }
        });
        jp1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jp1ActionPerformed(evt);
            }
        });
        jp1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jp1KeyPressed(evt);
            }
        });
        FormInput.add(jp1);
        jp1.setBounds(510, 320, 90, 23);

        si1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pcs", "Set", "Lembar", "Botol" }));
        si1.setName("si1"); // NOI18N
        si1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                si1KeyPressed(evt);
            }
        });
        FormInput.add(si1);
        si1.setBounds(610, 320, 60, 23);

        js1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        js1.setHighlighter(null);
        js1.setName("js1"); // NOI18N
        js1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                js1KeyPressed(evt);
            }
        });
        FormInput.add(js1);
        js1.setBounds(170, 320, 64, 23);

        ji1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ji1.setHighlighter(null);
        ji1.setName("ji1"); // NOI18N
        ji1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ji1KeyPressed(evt);
            }
        });
        FormInput.add(ji1);
        ji1.setBounds(290, 320, 64, 23);

        jt1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jt1.setHighlighter(null);
        jt1.setName("jt1"); // NOI18N
        jt1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jt1KeyPressed(evt);
            }
        });
        FormInput.add(jt1);
        jt1.setBounds(410, 320, 64, 23);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel33.setText("Jarum");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(80, 350, 70, 23);

        js2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        js2.setHighlighter(null);
        js2.setName("js2"); // NOI18N
        js2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                js2KeyPressed(evt);
            }
        });
        FormInput.add(js2);
        js2.setBounds(170, 350, 64, 23);

        ji2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ji2.setHighlighter(null);
        ji2.setName("ji2"); // NOI18N
        ji2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ji2KeyPressed(evt);
            }
        });
        FormInput.add(ji2);
        ji2.setBounds(290, 350, 64, 23);

        si2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pcs", "Set", "Lembar", "Botol" }));
        si2.setName("si2"); // NOI18N
        si2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                si2KeyPressed(evt);
            }
        });
        FormInput.add(si2);
        si2.setBounds(610, 350, 60, 23);

        jt2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jt2.setHighlighter(null);
        jt2.setName("jt2"); // NOI18N
        jt2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jt2KeyPressed(evt);
            }
        });
        FormInput.add(jt2);
        jt2.setBounds(410, 350, 64, 23);

        jp2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jp2.setHighlighter(null);
        jp2.setName("jp2"); // NOI18N
        jp2.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jp2InputMethodTextChanged(evt);
            }
        });
        jp2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jp2PropertyChange(evt);
            }
        });
        jp2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jp2KeyPressed(evt);
            }
        });
        FormInput.add(jp2);
        jp2.setBounds(510, 350, 90, 23);

        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel34.setText("Kassa");
        jLabel34.setName("jLabel34"); // NOI18N
        FormInput.add(jLabel34);
        jLabel34.setBounds(80, 410, 60, 23);

        js3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        js3.setHighlighter(null);
        js3.setName("js3"); // NOI18N
        js3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                js3KeyPressed(evt);
            }
        });
        FormInput.add(js3);
        js3.setBounds(170, 410, 64, 23);

        ji3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ji3.setHighlighter(null);
        ji3.setName("ji3"); // NOI18N
        ji3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ji3KeyPressed(evt);
            }
        });
        FormInput.add(ji3);
        ji3.setBounds(290, 410, 64, 23);

        si3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pcs", "Set", "Lembar", "Botol" }));
        si3.setName("si3"); // NOI18N
        si3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                si3KeyPressed(evt);
            }
        });
        FormInput.add(si3);
        si3.setBounds(610, 410, 60, 23);

        jt3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jt3.setHighlighter(null);
        jt3.setName("jt3"); // NOI18N
        jt3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jt3KeyPressed(evt);
            }
        });
        FormInput.add(jt3);
        jt3.setBounds(410, 410, 64, 23);

        jp3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jp3.setHighlighter(null);
        jp3.setName("jp3"); // NOI18N
        jp3.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jp3InputMethodTextChanged(evt);
            }
        });
        jp3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jp3KeyPressed(evt);
            }
        });
        FormInput.add(jp3);
        jp3.setBounds(510, 410, 90, 23);

        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel35.setText("Lainnya 1");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(10, 440, 50, 23);

        js4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        js4.setHighlighter(null);
        js4.setName("js4"); // NOI18N
        js4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                js4KeyPressed(evt);
            }
        });
        FormInput.add(js4);
        js4.setBounds(170, 440, 64, 23);

        ji4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ji4.setHighlighter(null);
        ji4.setName("ji4"); // NOI18N
        ji4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ji4KeyPressed(evt);
            }
        });
        FormInput.add(ji4);
        ji4.setBounds(290, 440, 64, 23);

        si4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pcs", "Set", "Lembar", "Botol" }));
        si4.setName("si4"); // NOI18N
        si4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                si4KeyPressed(evt);
            }
        });
        FormInput.add(si4);
        si4.setBounds(610, 440, 60, 23);

        jt4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jt4.setHighlighter(null);
        jt4.setName("jt4"); // NOI18N
        jt4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jt4KeyPressed(evt);
            }
        });
        FormInput.add(jt4);
        jt4.setBounds(410, 440, 64, 23);

        jp4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jp4.setHighlighter(null);
        jp4.setName("jp4"); // NOI18N
        jp4.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jp4InputMethodTextChanged(evt);
            }
        });
        jp4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jp4KeyPressed(evt);
            }
        });
        FormInput.add(jp4);
        jp4.setBounds(510, 440, 90, 23);

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel36.setText("Lainnya 2");
        jLabel36.setName("jLabel36"); // NOI18N
        FormInput.add(jLabel36);
        jLabel36.setBounds(10, 470, 50, 23);

        js5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        js5.setHighlighter(null);
        js5.setName("js5"); // NOI18N
        js5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                js5KeyPressed(evt);
            }
        });
        FormInput.add(js5);
        js5.setBounds(170, 470, 64, 23);

        ji5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ji5.setHighlighter(null);
        ji5.setName("ji5"); // NOI18N
        ji5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ji5KeyPressed(evt);
            }
        });
        FormInput.add(ji5);
        ji5.setBounds(290, 470, 64, 23);

        si5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pcs", "Set", "Lembar", "Botol" }));
        si5.setName("si5"); // NOI18N
        si5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                si5KeyPressed(evt);
            }
        });
        FormInput.add(si5);
        si5.setBounds(610, 470, 60, 23);

        jt5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jt5.setHighlighter(null);
        jt5.setName("jt5"); // NOI18N
        jt5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jt5KeyPressed(evt);
            }
        });
        FormInput.add(jt5);
        jt5.setBounds(410, 470, 64, 23);

        jp5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jp5.setHighlighter(null);
        jp5.setName("jp5"); // NOI18N
        jp5.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jp5InputMethodTextChanged(evt);
            }
        });
        jp5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jp5KeyPressed(evt);
            }
        });
        FormInput.add(jp5);
        jp5.setBounds(510, 470, 90, 23);

        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel37.setText("Total");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(130, 540, 30, 23);

        js6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        js6.setHighlighter(null);
        js6.setName("js6"); // NOI18N
        js6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                js6KeyPressed(evt);
            }
        });
        FormInput.add(js6);
        js6.setBounds(170, 500, 64, 23);

        ji6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ji6.setHighlighter(null);
        ji6.setName("ji6"); // NOI18N
        ji6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ji6KeyPressed(evt);
            }
        });
        FormInput.add(ji6);
        ji6.setBounds(290, 500, 64, 23);

        si6.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pcs", "Set", "Lembar", "Botol" }));
        si6.setName("si6"); // NOI18N
        si6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                si6KeyPressed(evt);
            }
        });
        FormInput.add(si6);
        si6.setBounds(610, 500, 60, 23);

        jt6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jt6.setHighlighter(null);
        jt6.setName("jt6"); // NOI18N
        jt6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jt6KeyPressed(evt);
            }
        });
        FormInput.add(jt6);
        jt6.setBounds(410, 500, 64, 23);

        jp7.setEditable(false);
        jp7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jp7.setHighlighter(null);
        jp7.setName("jp7"); // NOI18N
        jp7.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jp7PropertyChange(evt);
            }
        });
        jp7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jp7KeyPressed(evt);
            }
        });
        FormInput.add(jp7);
        jp7.setBounds(510, 540, 90, 23);

        jp6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jp6.setHighlighter(null);
        jp6.setName("jp6"); // NOI18N
        jp6.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                jp6CaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jp6InputMethodTextChanged(evt);
            }
        });
        jp6.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jp6PropertyChange(evt);
            }
        });
        jp6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jp6KeyPressed(evt);
            }
        });
        FormInput.add(jp6);
        jp6.setBounds(510, 500, 90, 23);

        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel38.setText("Lainnya 3");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(10, 500, 50, 23);

        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel39.setText("Instrumen");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(80, 320, 60, 23);

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel32.setText("[ Nama Barang ]");
        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(60, 280, 100, 23);

        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel40.setText("[ Jumlah Sebelum]");
        jLabel40.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(160, 280, 110, 23);

        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel41.setText("[ Jumlah Intra]");
        jLabel41.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(280, 280, 90, 23);

        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel42.setText("[ Jumlah Tambahan]");
        jLabel42.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(380, 280, 120, 23);

        js_tot.setEditable(false);
        js_tot.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        js_tot.setHighlighter(null);
        js_tot.setName("js_tot"); // NOI18N
        js_tot.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                js_totPropertyChange(evt);
            }
        });
        js_tot.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                js_totKeyPressed(evt);
            }
        });
        FormInput.add(js_tot);
        js_tot.setBounds(170, 540, 70, 23);

        jt_tot.setEditable(false);
        jt_tot.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jt_tot.setHighlighter(null);
        jt_tot.setName("jt_tot"); // NOI18N
        jt_tot.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jt_totPropertyChange(evt);
            }
        });
        jt_tot.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jt_totKeyPressed(evt);
            }
        });
        FormInput.add(jt_tot);
        jt_tot.setBounds(410, 540, 70, 23);

        ji_tot.setEditable(false);
        ji_tot.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ji_tot.setHighlighter(null);
        ji_tot.setName("ji_tot"); // NOI18N
        ji_tot.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                ji_totPropertyChange(evt);
            }
        });
        ji_tot.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ji_totKeyPressed(evt);
            }
        });
        FormInput.add(ji_tot);
        ji_tot.setBounds(290, 540, 64, 23);

        lainnya1.setHighlighter(null);
        lainnya1.setName("lainnya1"); // NOI18N
        lainnya1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lainnya1KeyPressed(evt);
            }
        });
        FormInput.add(lainnya1);
        lainnya1.setBounds(60, 440, 100, 23);

        lainnya2.setHighlighter(null);
        lainnya2.setName("lainnya2"); // NOI18N
        lainnya2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lainnya2KeyPressed(evt);
            }
        });
        FormInput.add(lainnya2);
        lainnya2.setBounds(60, 470, 100, 23);

        lainnya3.setHighlighter(null);
        lainnya3.setName("lainnya3"); // NOI18N
        lainnya3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lainnya3KeyPressed(evt);
            }
        });
        FormInput.add(lainnya3);
        lainnya3.setBounds(60, 500, 100, 23);

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel43.setText(" Pisau");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(80, 380, 70, 23);

        js7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        js7.setHighlighter(null);
        js7.setName("js7"); // NOI18N
        js7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                js7KeyPressed(evt);
            }
        });
        FormInput.add(js7);
        js7.setBounds(170, 380, 64, 23);

        ji7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ji7.setHighlighter(null);
        ji7.setName("ji7"); // NOI18N
        ji7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ji7KeyPressed(evt);
            }
        });
        FormInput.add(ji7);
        ji7.setBounds(290, 380, 64, 23);

        jt7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jt7.setHighlighter(null);
        jt7.setName("jt7"); // NOI18N
        jt7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jt7KeyPressed(evt);
            }
        });
        FormInput.add(jt7);
        jt7.setBounds(410, 380, 64, 23);

        jp8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jp8.setHighlighter(null);
        jp8.setName("jp8"); // NOI18N
        jp8.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jp8InputMethodTextChanged(evt);
            }
        });
        jp8.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jp8PropertyChange(evt);
            }
        });
        jp8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jp8KeyPressed(evt);
            }
        });
        FormInput.add(jp8);
        jp8.setBounds(510, 380, 90, 23);

        si7.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pcs", "Set", "Lembar", "Botol" }));
        si7.setName("si7"); // NOI18N
        si7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                si7KeyPressed(evt);
            }
        });
        FormInput.add(si7);
        si7.setBounds(610, 380, 60, 23);

        label15.setText("Cari Operator Time Out :");
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label15);
        label15.setBounds(220, 100, 160, 23);

        btnDataOperatorTimeOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnDataOperatorTimeOut.setMnemonic('2');
        btnDataOperatorTimeOut.setToolTipText("Alt+2");
        btnDataOperatorTimeOut.setName("btnDataOperatorTimeOut"); // NOI18N
        btnDataOperatorTimeOut.setPreferredSize(new java.awt.Dimension(28, 23));
        btnDataOperatorTimeOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDataOperatorTimeOutActionPerformed(evt);
            }
        });
        FormInput.add(btnDataOperatorTimeOut);
        btnDataOperatorTimeOut.setBounds(380, 100, 28, 23);

        scrollInput.setViewportView(FormInput);

        PanelInput.add(scrollInput, java.awt.BorderLayout.CENTER);

        internalFrame1.add(PanelInput, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
            isPsien();
        }else{            
            //Valid.pindah(evt,TCari,Tanggal);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void TPasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TPasienKeyPressed
        Valid.pindah(evt,TCari,BtnSimpan);
}//GEN-LAST:event_TPasienKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(KodeDokterOperator.getText().trim().equals("")||NamaDokterOperator.getText().trim().equals("")){
            Valid.textKosong(btnDokterOperator,"Dokter Bedah");
        }else if(KodeDokterAnestesi.getText().trim().equals("")||NamaDokterAnestesi.getText().trim().equals("")){
            Valid.textKosong(KodeDokterAnestesi,"Dokter Anestesi");
        }else if(KdPetugasOK.getText().trim().equals("")||NmPetugasOK.getText().trim().equals("")){
            Valid.textKosong(KdPetugasOK,"Perawat Sirkuler");
        }else{
            if(Sequel.menyimpantf("signout_sebelum_menutupluka","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Data",56,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(Tanggal.getSelectedItem()+""),
                Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                KodeDokterOperator.getText(),
                KodeDokterAnestesi.getText(),
                KdPetugasOK.getText(),
                cProsedur.getSelectedItem().toString(),
                ket_prosedur.getText(),
                cInstrumen.getSelectedItem().toString(),
                cSpesimen.getSelectedItem().toString(),
                ket_label.getText(),
                cmasalah.getSelectedItem().toString(),
                Permasalahan.getText(),
                PesanPemulihan.getText(),
                js1.getText(),
                ji1.getText(),
                jt1.getText(),
                jp1.getText(),
                si1.getSelectedItem().toString(),
                js2.getText(),
                ji2.getText(),
                jt2.getText(),
                jp2.getText(),
                si2.getSelectedItem().toString(),
                js3.getText(),
                ji3.getText(),
                jt3.getText(),
                jp3.getText(),
                si3.getSelectedItem().toString(),
                lainnya1.getText(),
                js4.getText(),
                ji4.getText(),
                jt4.getText(),
                jp4.getText(),
                si4.getSelectedItem().toString(),
                lainnya2.getText(),
                js5.getText(),
                ji5.getText(),
                jt5.getText(),
                jp5.getText(),
                si5.getSelectedItem().toString(),
                lainnya3.getText(),
                js6.getText(),
                ji6.getText(),
                jt6.getText(),
                jp6.getText(),
                si6.getSelectedItem().toString(),
                js7.getText(),
                ji7.getText(),
                jt7.getText(),
                jp8.getText(),
                si7.getSelectedItem().toString(),
                js_tot.getText(),
                ji_tot.getText(),
                jt_tot.getText(),
                jp7.getText()
            })==true){
                tampil();
                emptTeks();
            } 
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,btnPetugasOK,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        ChkJam.setSelected(true);
        isForm(); 
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
            
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else {
                hapus();
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }   
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(KodeDokterOperator.getText().trim().equals("")||NamaDokterOperator.getText().trim().equals("")){
            Valid.textKosong(btnDokterOperator,"Dokter Bedah");
        }else if(KodeDokterAnestesi.getText().trim().equals("")||NamaDokterAnestesi.getText().trim().equals("")){
            Valid.textKosong(KodeDokterAnestesi,"Dokter Anestesi");
        }else if(KdPetugasOK.getText().trim().equals("")||NmPetugasOK.getText().trim().equals("")){
            Valid.textKosong(KdPetugasOK,"Perawat Sirkuler");
        }else{  
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                  
                }else {
                    ganti();
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        petugas.dispose();
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
     
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            tampil();
            TCari.setText("");
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void TNoRMKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRMKeyPressed
        // Valid.pindah(evt, TNm, BtnSimpan);
}//GEN-LAST:event_TNoRMKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void MnSignOutSebelumMenutupLukaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnSignOutSebelumMenutupLukaActionPerformed
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),61).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),62).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),61).toString():finger)+"\n"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+" "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()); 
            finger2=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            param.put("finger2","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),10).toString()+"\nID "+(finger2.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),10).toString():finger2)+"\n"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+" "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()); 
            finger3=Sequel.cariIsi("select sha1(sidikjari.sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            param.put("finger3","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),8).toString()+"\nID "+(finger3.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),7).toString():finger3)+"\n"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+" "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            Valid.MyReportqry("rptFormulirSignOutSebelumMenutupLuka.jasper","report","::[ Formulir Sign-Out Sebelum Menutup Luka ]::",
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,reg_periksa.umurdaftar,reg_periksa.sttsumur,pasien.tgl_lahir,if(pasien.jk='L','Laki-Laki','Perempuan') as jk, CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,"+
                    "signout_sebelum_menutupluka.*,petugas.nama,d1.kd_dokter as kd_operator,d1.nm_dokter as nm_operator,d2.kd_dokter as kd_anestesi,d2.nm_dokter as nm_anestesi  "+
                    "from signout_sebelum_menutupluka "+
                    "inner join reg_periksa on signout_sebelum_menutupluka.no_rawat=reg_periksa.no_rawat inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter as d1 on d1.kd_dokter=signout_sebelum_menutupluka.kd_operator "+
                    "inner join dokter as d2 on d2.kd_dokter=signout_sebelum_menutupluka.kd_anestesi "+
                    "inner join petugas on petugas.nip=signout_sebelum_menutupluka.kd_petugas  "+
                    "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                    "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                    "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                    "where signout_sebelum_menutupluka.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and signout_sebelum_menutupluka.tanggal='"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+"' and signout_sebelum_menutupluka.jam='"+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"'",param);
        }
    }//GEN-LAST:event_MnSignOutSebelumMenutupLukaActionPerformed

    private void ChkInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkInputActionPerformed
        isForm();
    }//GEN-LAST:event_ChkInputActionPerformed

    private void btnDokterOperatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDokterOperatorActionPerformed
        pilihan=1;
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_btnDokterOperatorActionPerformed

    private void btnDokterOperatorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnDokterOperatorKeyPressed
      
    }//GEN-LAST:event_btnDokterOperatorKeyPressed

    private void btnDokterAnestesiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDokterAnestesiActionPerformed
        pilihan=2;
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_btnDokterAnestesiActionPerformed

    private void btnDokterAnestesiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnDokterAnestesiKeyPressed
        Valid.pindah(evt,btnDokterOperator,cProsedur);
    }//GEN-LAST:event_btnDokterAnestesiKeyPressed

    private void btnPetugasOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPetugasOKActionPerformed
        pilihan=2;
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_btnPetugasOKActionPerformed

    private void btnPetugasOKKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPetugasOKKeyPressed
        
    }//GEN-LAST:event_btnPetugasOKKeyPressed

    private void cProsedurKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cProsedurKeyPressed
        Valid.pindah(evt,btnDokterAnestesi,cInstrumen);
    }//GEN-LAST:event_cProsedurKeyPressed

    private void cInstrumenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cInstrumenKeyPressed
       
    }//GEN-LAST:event_cInstrumenKeyPressed

    private void cSpesimenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cSpesimenKeyPressed
       
    }//GEN-LAST:event_cSpesimenKeyPressed

    private void PermasalahanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PermasalahanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PermasalahanKeyPressed

    private void PesanPemulihanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PesanPemulihanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PesanPemulihanKeyPressed

    private void KdPetugasOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdPetugasOKActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdPetugasOKActionPerformed

    private void JamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JamActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamActionPerformed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MenitKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DetikKeyPressed

    private void ket_prosedurKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ket_prosedurKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ket_prosedurKeyPressed

    private void ket_labelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ket_labelKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ket_labelKeyPressed

    private void cmasalahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmasalahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmasalahKeyPressed

    private void jp1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jp1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jp1KeyPressed

    private void si1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_si1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_si1KeyPressed

    private void js1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_js1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_js1KeyPressed

    private void ji1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ji1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ji1KeyPressed

    private void jt1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jt1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jt1KeyPressed

    private void js2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_js2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_js2KeyPressed

    private void ji2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ji2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ji2KeyPressed

    private void si2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_si2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_si2KeyPressed

    private void jt2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jt2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jt2KeyPressed

    private void jp2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jp2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jp2KeyPressed

    private void js3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_js3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_js3KeyPressed

    private void ji3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ji3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ji3KeyPressed

    private void si3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_si3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_si3KeyPressed

    private void jt3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jt3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jt3KeyPressed

    private void jp3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jp3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jp3KeyPressed

    private void js4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_js4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_js4KeyPressed

    private void ji4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ji4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ji4KeyPressed

    private void si4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_si4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_si4KeyPressed

    private void jt4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jt4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jt4KeyPressed

    private void jp4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jp4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jp4KeyPressed

    private void js5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_js5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_js5KeyPressed

    private void ji5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ji5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ji5KeyPressed

    private void si5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_si5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_si5KeyPressed

    private void jt5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jt5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jt5KeyPressed

    private void jp5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jp5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jp5KeyPressed

    private void js6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_js6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_js6KeyPressed

    private void ji6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ji6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ji6KeyPressed

    private void si6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_si6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_si6KeyPressed

    private void jt6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jt6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jt6KeyPressed

    private void jp7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jp7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jp7KeyPressed

    private void jp6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jp6KeyPressed
     
    }//GEN-LAST:event_jp6KeyPressed

    private void jp1InputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jp1InputMethodTextChanged
     
    }//GEN-LAST:event_jp1InputMethodTextChanged

    private void jp2InputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jp2InputMethodTextChanged
    
    }//GEN-LAST:event_jp2InputMethodTextChanged

    private void jp3InputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jp3InputMethodTextChanged
   
    }//GEN-LAST:event_jp3InputMethodTextChanged

    private void jp4InputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jp4InputMethodTextChanged
        
    }//GEN-LAST:event_jp4InputMethodTextChanged

    private void jp5InputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jp5InputMethodTextChanged
       
    }//GEN-LAST:event_jp5InputMethodTextChanged

    private void jp6InputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jp6InputMethodTextChanged
   
    }//GEN-LAST:event_jp6InputMethodTextChanged

    private void jp6CaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jp6CaretPositionChanged
   
    }//GEN-LAST:event_jp6CaretPositionChanged

    private void jp6PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jp6PropertyChange
   
    }//GEN-LAST:event_jp6PropertyChange

    private void jp1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jp1ActionPerformed

    }//GEN-LAST:event_jp1ActionPerformed

    private void jp7PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jp7PropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jp7PropertyChange

    private void jp2PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jp2PropertyChange
   
    }//GEN-LAST:event_jp2PropertyChange

    private void js_totPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_js_totPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_js_totPropertyChange

    private void js_totKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_js_totKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_js_totKeyPressed

    private void jt_totPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jt_totPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jt_totPropertyChange

    private void jt_totKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jt_totKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jt_totKeyPressed

    private void ji_totPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_ji_totPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_ji_totPropertyChange

    private void ji_totKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ji_totKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ji_totKeyPressed

    private void lainnya1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lainnya1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_lainnya1KeyPressed

    private void lainnya2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lainnya2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_lainnya2KeyPressed

    private void lainnya3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lainnya3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_lainnya3KeyPressed

    private void js7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_js7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_js7KeyPressed

    private void ji7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ji7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ji7KeyPressed

    private void jt7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jt7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jt7KeyPressed

    private void jp8InputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jp8InputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_jp8InputMethodTextChanged

    private void jp8PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jp8PropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jp8PropertyChange

    private void jp8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jp8KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jp8KeyPressed

    private void si7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_si7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_si7KeyPressed

    private void btnDataOperatorTimeOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDataOperatorTimeOutActionPerformed

        carioperatortimeout.emptTeks();
        carioperatortimeout.setnorawat(TNoRw.getText());
        carioperatortimeout.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        carioperatortimeout.setLocationRelativeTo(internalFrame1);
        carioperatortimeout.setVisible(true);
    }//GEN-LAST:event_btnDataOperatorTimeOutActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMSignOutSebelumMenutupLuka dialog = new RMSignOutSebelumMenutupLuka(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.CekBox ChkInput;
    private widget.CekBox ChkJam;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox Detik;
    private widget.PanelBiasa FormInput;
    private widget.ComboBox Jam;
    private widget.TextBox KdPetugasOK;
    private widget.TextBox KodeDokterAnestesi;
    private widget.TextBox KodeDokterOperator;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private widget.ComboBox Menit;
    private javax.swing.JMenuItem MnSignOutSebelumMenutupLuka;
    private widget.TextBox NamaDokterAnestesi;
    private widget.TextBox NamaDokterOperator;
    private widget.TextBox NmPetugasOK;
    private javax.swing.JPanel PanelInput;
    private widget.TextArea Permasalahan;
    private widget.TextArea PesanPemulihan;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.Tanggal Tanggal;
    private widget.TextBox TglLahir;
    private widget.Button btnDataOperatorTimeOut;
    private widget.Button btnDokterAnestesi;
    private widget.Button btnDokterOperator;
    private widget.Button btnPetugasOK;
    private widget.ComboBox cInstrumen;
    private widget.ComboBox cProsedur;
    private widget.ComboBox cSpesimen;
    private widget.ComboBox cmasalah;
    private widget.InternalFrame internalFrame1;
    private widget.Label jLabel16;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel26;
    private widget.Label jLabel28;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel4;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel5;
    private widget.Label jLabel54;
    private widget.Label jLabel55;
    private widget.Label jLabel56;
    private widget.Label jLabel57;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private widget.TextBox ji1;
    private widget.TextBox ji2;
    private widget.TextBox ji3;
    private widget.TextBox ji4;
    private widget.TextBox ji5;
    private widget.TextBox ji6;
    private widget.TextBox ji7;
    private widget.TextBox ji_tot;
    private widget.TextBox jp1;
    private widget.TextBox jp2;
    private widget.TextBox jp3;
    private widget.TextBox jp4;
    private widget.TextBox jp5;
    private widget.TextBox jp6;
    private widget.TextBox jp7;
    private widget.TextBox jp8;
    private widget.TextBox js1;
    private widget.TextBox js2;
    private widget.TextBox js3;
    private widget.TextBox js4;
    private widget.TextBox js5;
    private widget.TextBox js6;
    private widget.TextBox js7;
    private widget.TextBox js_tot;
    private widget.TextBox jt1;
    private widget.TextBox jt2;
    private widget.TextBox jt3;
    private widget.TextBox jt4;
    private widget.TextBox jt5;
    private widget.TextBox jt6;
    private widget.TextBox jt7;
    private widget.TextBox jt_tot;
    private widget.TextBox ket_label;
    private widget.TextBox ket_prosedur;
    private widget.Label label15;
    private widget.TextBox lainnya1;
    private widget.TextBox lainnya2;
    private widget.TextBox lainnya3;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane2;
    private widget.ScrollPane scrollPane3;
    private widget.ComboBox si1;
    private widget.ComboBox si2;
    private widget.ComboBox si3;
    private widget.ComboBox si4;
    private widget.ComboBox si5;
    private widget.ComboBox si6;
    private widget.ComboBox si7;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables
    
    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().trim().equals("")){
                ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,pasien.jk,signout_sebelum_menutupluka.*,"+
                    "petugas.nama,d1.kd_dokter as kd_operator,d1.nm_dokter as nm_operator,d2.kd_dokter as kd_anestesi,d2.nm_dokter as nm_anestesi  "+
                    "from signout_sebelum_menutupluka "+
                    "inner join reg_periksa on signout_sebelum_menutupluka.no_rawat=reg_periksa.no_rawat  "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter as d1 on d1.kd_dokter=signout_sebelum_menutupluka.kd_operator "+
                    "inner join dokter as d2 on d2.kd_dokter=signout_sebelum_menutupluka.kd_anestesi "+
                    "inner join petugas on petugas.nip=signout_sebelum_menutupluka.kd_petugas "+
                    "where signout_sebelum_menutupluka.tanggal between ? and ? order by signout_sebelum_menutupluka.tanggal ");
            }else{
                ps=koneksi.prepareStatement(
                   "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,pasien.jk,signout_sebelum_menutupluka.*,"+
                    "petugas.nama,d1.kd_dokter as kd_operator,d1.nm_dokter as nm_operator,d2.kd_dokter as kd_anestesi,d2.nm_dokter as nm_anestesi  "+
                    "from signout_sebelum_menutupluka "+
                    "inner join reg_periksa on signout_sebelum_menutupluka.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter as d1 on d1.kd_dokter=signout_sebelum_menutupluka.kd_operator "+
                    "inner join dokter as d2 on d2.kd_dokter=signout_sebelum_menutupluka.kd_anestesi "+
                    "inner join petugas on petugas.nip=signout_sebelum_menutupluka.kd_petugas "+
                    "where signout_sebelum_menutupluka.tanggal between ? and ? and (reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or "+
                    "pasien.nm_pasien like ? or d1.nm_dokter like ? or d2.nm_dokter like ? or petugas.nama like ?) "+
                    "order by signout_sebelum_menutupluka.tanggal ");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                }
                    
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("tgl_lahir"),
                        rs.getString("jk"),
                        rs.getString("tanggal"),
                        rs.getString("jam"),
                        rs.getString("kd_operator"),
                        rs.getString("nm_operator"),
                        rs.getString("kd_anestesi"),
                        rs.getString("nm_anestesi"),
                        rs.getString("prosedur"),
                        rs.getString("ket_prosedur"),
                        rs.getString("instrumen"),
                        rs.getString("spesimen"),
                        rs.getString("ket_spesimen"),
                        rs.getString("konfirmasi_permasalahan"),
                        rs.getString("permasalahan_peralatan"),
                        rs.getString("pesan_pemulihan"),
                        rs.getString("js1"),
                        rs.getString("ji1"),
                        rs.getString("jt1"),
                        rs.getString("jp1"),
                        rs.getString("sat1"),
                        rs.getString("js2"),
                        rs.getString("ji2"),
                        rs.getString("jt2"),
                        rs.getString("jp2"),
                        rs.getString("sat2"),
                        rs.getString("js3"),
                        rs.getString("ji3"),
                        rs.getString("jt3"),
                        rs.getString("jp3"),
                        rs.getString("sat3"),
                        rs.getString("js7"),
                        rs.getString("ji7"),
                        rs.getString("jt7"),
                        rs.getString("jp8"),
                        rs.getString("sat7"),
                        rs.getString("item4"),
                        rs.getString("js4"),
                        rs.getString("ji4"),
                        rs.getString("jt4"),
                        rs.getString("jp4"),
                        rs.getString("sat4"),
                        rs.getString("item5"),
                        rs.getString("js5"),
                        rs.getString("ji5"),
                        rs.getString("jt5"),
                        rs.getString("jp5"),
                        rs.getString("sat5"),
                        rs.getString("item6"),
                        rs.getString("js6"),
                        rs.getString("ji6"),
                        rs.getString("jt6"),
                        rs.getString("jp6"),
                        rs.getString("sat6"),
                        
                        rs.getString("tot_sebelum"),
                        rs.getString("tot_intra"),
                        rs.getString("tot_tambahan"),
                        rs.getString("tot_pasca"),
                        rs.getString("kd_petugas"),
                        rs.getString("nama")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }
    
    public void emptTeks() {
        KodeDokterOperator.setText("");
        NamaDokterOperator.setText("");
        KodeDokterAnestesi.setText("");
        NamaDokterAnestesi.setText("");
        KdPetugasOK.setText("");
        NmPetugasOK.setText("");
        Tanggal.setDate(new Date());
        cProsedur.setSelectedIndex(0);
        ket_prosedur.setText("");
        cInstrumen.setSelectedIndex(0);
        js1.setText("");
        ji1.setText("");
        jt1.setText("");
        jp1.setText("");
        js2.setText("");
        ji2.setText("");
        jt2.setText("");
        jp2.setText("");
        js3.setText("");
        ji3.setText("");
        jt3.setText("");
        jp3.setText("");
        lainnya1.setText("");
        js4.setText("");
        ji4.setText("");
        jt4.setText("");
        jp4.setText("");
        lainnya2.setText("");
        js5.setText("");
        ji5.setText("");
        jt5.setText("");
        jp5.setText("");
        lainnya3.setText("");
        js6.setText("");
        ji6.setText("");
        jt6.setText("");
        jp6.setText("");
        js7.setText("");
        ji7.setText("");
        jt7.setText("");
        jp7.setText("");
        jp8.setText("");
        si1.setSelectedIndex(0);
        si2.setSelectedIndex(0);
        si3.setSelectedIndex(0);
        si4.setSelectedIndex(0);
        si5.setSelectedIndex(0);
        si6.setSelectedIndex(0);
        si7.setSelectedIndex(0);
        cSpesimen.setSelectedIndex(0);
        ket_label.setText("");
        cmasalah.setSelectedItem("");
        Permasalahan.setText("");
        PesanPemulihan.setText("");
    
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            emptTeks();
            ChkJam.setSelected(false);
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Valid.SetTgl2(Tanggal,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            Jam.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString().substring(0,2));
            Menit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString().substring(3,5));
            Detik.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString().substring(6,8));
            KodeDokterOperator.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            NamaDokterOperator.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            KodeDokterAnestesi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            NamaDokterAnestesi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            cProsedur.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            ket_prosedur.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            cInstrumen.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            cSpesimen.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            ket_label.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            cmasalah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            Permasalahan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            PesanPemulihan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString());
            js1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            ji1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            jt1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            jp1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            si1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            js2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            ji2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString());
            jt2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            jp2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),27).toString());
            si2.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            js3.setText(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            ji3.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
            jt3.setText(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            jp3.setText(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());
            si3.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString());
            js7.setText(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());
            ji7.setText(tbObat.getValueAt(tbObat.getSelectedRow(),35).toString());
            jt7.setText(tbObat.getValueAt(tbObat.getSelectedRow(),36).toString());
            jp8.setText(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString());
            si7.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),38).toString());
            lainnya1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),39).toString());
            js4.setText(tbObat.getValueAt(tbObat.getSelectedRow(),40).toString());
            ji4.setText(tbObat.getValueAt(tbObat.getSelectedRow(),41).toString());
            jt4.setText(tbObat.getValueAt(tbObat.getSelectedRow(),42).toString());
            jp4.setText(tbObat.getValueAt(tbObat.getSelectedRow(),43).toString());
            si4.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString());
            lainnya2.setText(tbObat.getValueAt(tbObat.getSelectedRow(),45).toString());
            js5.setText(tbObat.getValueAt(tbObat.getSelectedRow(),46).toString());
            ji5.setText(tbObat.getValueAt(tbObat.getSelectedRow(),47).toString());
            jt5.setText(tbObat.getValueAt(tbObat.getSelectedRow(),48).toString());
            jp5.setText(tbObat.getValueAt(tbObat.getSelectedRow(),49).toString());
            si1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),50).toString());
            lainnya3.setText(tbObat.getValueAt(tbObat.getSelectedRow(),51).toString());
            js6.setText(tbObat.getValueAt(tbObat.getSelectedRow(),52).toString());
            ji6.setText(tbObat.getValueAt(tbObat.getSelectedRow(),53).toString());
            jt6.setText(tbObat.getValueAt(tbObat.getSelectedRow(),54).toString());
            jp6.setText(tbObat.getValueAt(tbObat.getSelectedRow(),55).toString());
            si6.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),56).toString());  
            js_tot.setText(tbObat.getValueAt(tbObat.getSelectedRow(),57).toString());
            ji_tot.setText(tbObat.getValueAt(tbObat.getSelectedRow(),58).toString());
            jt_tot.setText(tbObat.getValueAt(tbObat.getSelectedRow(),59).toString());
            jp7.setText(tbObat.getValueAt(tbObat.getSelectedRow(),60).toString());
            KdPetugasOK.setText(tbObat.getValueAt(tbObat.getSelectedRow(),61).toString());
            NmPetugasOK.setText(tbObat.getValueAt(tbObat.getSelectedRow(),62).toString());
            
        }
    }
    private void isRawat() {
         Sequel.cariIsi("select reg_periksa.no_rkm_medis from reg_periksa where reg_periksa.no_rawat='"+TNoRw.getText()+"' ",TNoRM);
    }

    private void isPsien() {
        Sequel.cariIsi("select pasien.nm_pasien from pasien where pasien.no_rkm_medis='"+TNoRM.getText()+"' ",TPasien);
        Sequel.cariIsi("select DATE_FORMAT(pasien.tgl_lahir,'%d-%m-%Y') from pasien where pasien.no_rkm_medis=? ",TglLahir,TNoRM.getText());
    }
    
    public void setNoRm(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        Sequel.cariIsi("select reg_periksa.tgl_registrasi from reg_periksa where reg_periksa.no_rawat='"+norwt+"'", DTPCari1);
        DTPCari2.setDate(tgl2);
        isRawat();
        isPsien();
        ChkInput.setSelected(true);
        isForm();
    }
    
    public void setNoRm(String norwt, Date tgl2,String KodeDokter,String NamaDokter,String Operasi) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        Sequel.cariIsi("select reg_periksa.tgl_registrasi from reg_periksa where reg_periksa.no_rawat='"+norwt+"'", DTPCari1);
        DTPCari2.setDate(tgl2);
        isRawat();
        isPsien();
        ChkInput.setSelected(true);
        isForm();
        KodeDokterOperator.setText(KodeDokter);
        NamaDokterOperator.setText(NamaDokter);
       
    }
    
    private void isForm(){
        if(ChkInput.isSelected()==true){
            if(internalFrame1.getHeight()>538){
                ChkInput.setVisible(false);
                PanelInput.setPreferredSize(new Dimension(WIDTH,366));
                FormInput.setVisible(true);      
                ChkInput.setVisible(true);
            }else{
                ChkInput.setVisible(false);
                PanelInput.setPreferredSize(new Dimension(WIDTH,internalFrame1.getHeight()-172));
                FormInput.setVisible(true);      
                ChkInput.setVisible(true);
            }
        }else if(ChkInput.isSelected()==false){           
            ChkInput.setVisible(false);            
            PanelInput.setPreferredSize(new Dimension(WIDTH,20));
            FormInput.setVisible(false);      
            ChkInput.setVisible(true);
        }
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getoperasi());
        BtnHapus.setEnabled(akses.getoperasi());
        BtnEdit.setEnabled(akses.getoperasi());
        BtnPrint.setEnabled(akses.getoperasi()); 
    }

    private void ganti() {
        Sequel.mengedit("signout_sebelum_menutupluka",
                "no_rawat=? and tanggal=? and jam=?",
                "no_rawat=?,tanggal=?,jam=?,kd_operator=?,kd_anestesi=?,"+
                "kd_petugas=?,prosedur=?,ket_prosedur=?,instrumen=?,spesimen=?,ket_spesimen=?,konfirmasi_permasalahan=?,permasalahan_peralatan=?,"+
                "pesan_pemulihan=?,js1=?,ji1=?,jt1=?,jp1=?,sat1=?,js2=?,ji2=?,jt2=?,jp2=?,sat2=?,js3=?,ji3=?,jt3=?,jp3=?,sat3=?,item4=?,js4=?,ji4=?,jt4=?,jp4=?,sat4=?,item5=?,js5=?,ji5=?,jt5=?,jp5=?,sat5=?,item6=?,js6=?,ji6=?,jt6=?,jp6=?,sat6=?,js7=?,ji7=?,jt7=?,jp8=?,sat7=?,tot_sebelum=?,tot_intra=?,tot_tambahan=?,tot_pasca=?",59,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(Tanggal.getSelectedItem()+""),
                Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                KodeDokterOperator.getText(),
                KodeDokterAnestesi.getText(),
                KdPetugasOK.getText(),
                cProsedur.getSelectedItem().toString(),
                ket_prosedur.getText(),
                cInstrumen.getSelectedItem().toString(),
                cSpesimen.getSelectedItem().toString(),
                ket_label.getText(),
                cmasalah.getSelectedItem().toString(),
                Permasalahan.getText(),
                PesanPemulihan.getText(),
                js1.getText(),
                ji1.getText(),
                jt1.getText(),
                jp1.getText(),
                si1.getSelectedItem().toString(),
                js2.getText(),
                ji2.getText(),
                jt2.getText(),
                jp2.getText(),
                si2.getSelectedItem().toString(),
                js3.getText(),
                ji3.getText(),
                jt3.getText(),
                jp3.getText(),
                si3.getSelectedItem().toString(),
                lainnya1.getText(),
                js4.getText(),
                ji4.getText(),
                jt4.getText(),
                jp4.getText(),
                si4.getSelectedItem().toString(),
                lainnya2.getText(),
                js5.getText(),
                ji5.getText(),
                jt5.getText(),
                jp5.getText(),
                si5.getSelectedItem().toString(),
                lainnya3.getText(),
                js6.getText(),
                ji6.getText(),
                jt6.getText(),
                jp6.getText(),
                si6.getSelectedItem().toString(),
                js7.getText(),
                ji7.getText(),
                jt7.getText(),
                jp8.getText(),
                si7.getSelectedItem().toString(),
                js_tot.getText(),
                ji_tot.getText(),
                jt_tot.getText(),
                jp7.getText(),
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),
                tbObat.getValueAt(tbObat.getSelectedRow(),5).toString(),
                tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()
        });
            
        if(tabMode.getRowCount()!=0){tampil();}
        emptTeks();
        tampil();
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from signout_sebelum_menutupluka where no_rawat=? and tanggal=? and jam=?",3,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),
            tbObat.getValueAt(tbObat.getSelectedRow(),5).toString(),
            tbObat.getValueAt(tbObat.getSelectedRow(),6).toString(),
        })==true){
            tabMode.removeRow(tbObat.getSelectedRow());
            LCount.setText(""+tabMode.getRowCount());
            emptTeks();
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }
    
   public void isjml(){
        jp7.setText(Valid.SetAngka8(Valid.SetAngka(jp1.getText())+Valid.SetAngka(jp2.getText())+Valid.SetAngka(jp3.getText())+Valid.SetAngka(jp4.getText())+Valid.SetAngka(jp5.getText())+Valid.SetAngka(jp6.getText())+Valid.SetAngka(jp8.getText()),1)+"");    
   }
   
   public void jml_1(){
        jp1.setText(Valid.SetAngka8(Valid.SetAngka(js1.getText())+Valid.SetAngka(jt1.getText()),1)+"");
   }
   public void jml_2(){
        jp2.setText(Valid.SetAngka8(Valid.SetAngka(js2.getText())+Valid.SetAngka(jt2.getText()) ,1)+"");
   }
   public void jml_3(){
        jp3.setText(Valid.SetAngka8(Valid.SetAngka(js3.getText())+Valid.SetAngka(jt3.getText()) ,1)+"");
   }
   public void jml_4(){
        jp4.setText(Valid.SetAngka8(Valid.SetAngka(js4.getText())+Valid.SetAngka(jt4.getText()) ,1)+"");
   }
   public void jml_5(){
        jp5.setText(Valid.SetAngka8(Valid.SetAngka(js5.getText())+Valid.SetAngka(jt5.getText()) ,1)+"");
   }
   public void jml_6(){
        jp6.setText(Valid.SetAngka8(Valid.SetAngka(js6.getText())+Valid.SetAngka(jt6.getText()) ,1)+"");
   }
   public void jml_7(){
        jp8.setText(Valid.SetAngka8(Valid.SetAngka(js7.getText())+Valid.SetAngka(jt7.getText()) ,1)+"");
   }
   public void jml_js(){
        js_tot.setText(Valid.SetAngka8(Valid.SetAngka(js1.getText())+Valid.SetAngka(js2.getText())+Valid.SetAngka(js3.getText())+Valid.SetAngka(js4.getText())+Valid.SetAngka(js5.getText())+Valid.SetAngka(js6.getText())+Valid.SetAngka(js7.getText()),1)+"");
   }

   public void jml_ji(){
        ji_tot.setText(Valid.SetAngka8(Valid.SetAngka(ji1.getText())+Valid.SetAngka(ji2.getText())+Valid.SetAngka(ji3.getText())+Valid.SetAngka(ji4.getText())+Valid.SetAngka(ji5.getText())+Valid.SetAngka(ji6.getText())+Valid.SetAngka(ji7.getText()),1)+"");
   }
   public void jml_jt(){
        jt_tot.setText(Valid.SetAngka8(Valid.SetAngka(jt1.getText())+Valid.SetAngka(jt2.getText())+Valid.SetAngka(jt3.getText())+Valid.SetAngka(jt4.getText())+Valid.SetAngka(jt5.getText())+Valid.SetAngka(jt6.getText())+Valid.SetAngka(jt7.getText()),1)+"");
   }
   private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkJam.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkJam.isSelected()==false){
                    nilai_jam =Jam.getSelectedIndex();
                    nilai_menit =Menit.getSelectedIndex();
                    nilai_detik =Detik.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam.setSelectedItem(jam);
                Menit.setSelectedItem(menit);
                Detik.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
}
