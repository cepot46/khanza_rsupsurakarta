/*
 * Kontribusi dari M. Syukur RS. Jiwa Prov Sultra
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariPetugas;


/**
 *
 * @author perpustakaan
 */
public final class RMTransferPasienAntarRuang extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps,psigd,psralan,pssoapigd,pssoapranap,pssoapralan;
    private ResultSet rs,rsigd,rsralan,rssoapigd,rssoapranap,rssoapralan;
    private int i=0,pilihan=0;
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private StringBuilder htmlContent;
    private String finger="",finger_pj="",checkboxRM="",checkboxRAD="",checkboxLAB="",checkboxLAINNYA="",checkboxRM1="",checkboxRAD1="",checkboxLAB1="",checkboxLAINNYA1="";
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMTransferPasienAntarRuang(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tgl.Lahir","J.K.","Tanggal Masuk","Tanggal Pindah","Asal Ruang","Ruang Selanjutnya",
            "Alasan Transfer","Diagnosa","Indikasi Pindah Ruang","Keterangan Indikasi Pindah","Prosedur Yang Sudah Dilakukan",
            "Obat Yang Telah Diberikan","Metode Pemindahan Pasien","Peralatan Yang Menyertai","Obat / Cairan Yang Menyertai",
            "Dokumen RM dikirim","Hasil Laboratorium dikirim","Hasil Radiologi dikirim","Dokumen Lainnya","Keterangan",
            "Dokumen RM diterima","Hasil Laboratorium diterima","Hasil Radiologi diterima","Dokumen Lainnya","Keterangan",
            "Pemeriksaan Penunjang yang dilakukan",
            "Menyetujui Pemindahan","Nama Keluarga/Penanggung Jawab","Hubungan","Keluhan Utama SbT","Kesadaran","TD SbT","Nadi SbT","RR SbT","Suhu Sbt",
            "Keluhan Utama StT","Kesadaran StT","TD StT","Nadi StT","RR StT","Suhu Stt","NIP Menyerahkan",
            "Petugas Yang Menyerahkan","NIP Menerima","Petugas Yang Menerima","Level","Rencana Tindak lanjut","Tanggal Diterima","Waktu Diterima","S","B","A","R"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        
        tbObat.setModel(tabMode);
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 56; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(150);
            }else if(i==3){
                column.setPreferredWidth(65);
            }else if(i==4){
                column.setPreferredWidth(55);
            }else if(i==5){
                column.setPreferredWidth(115);
            }else if(i==6){
                column.setPreferredWidth(115);
            }else if(i==7){
                column.setPreferredWidth(190);
            }else if(i==8){
                column.setPreferredWidth(140);
            }else if(i==9){
                column.setPreferredWidth(150);
            }else if(i==10){
                column.setPreferredWidth(150);
            }else if(i==11){
                column.setPreferredWidth(108);
            }else if(i==12){
                column.setPreferredWidth(160);
            }else if(i==13){
                column.setPreferredWidth(160);
            }else if(i==14){
                column.setPreferredWidth(200);
            }else if(i==15){
                column.setPreferredWidth(230);
            }else if(i==16){
                column.setPreferredWidth(236);
            }else if(i==17){
                column.setPreferredWidth(134);
            }else if(i==18){
                column.setPreferredWidth(165);
            }else if(i==19){
                column.setPreferredWidth(127);
            }else if(i==20){
                column.setPreferredWidth(180);
            }else if(i==21){
                column.setPreferredWidth(100);
            }else if(i==22){
                column.setPreferredWidth(107);
            }else if(i==23){
                column.setPreferredWidth(100);
            }else if(i==24){
                column.setPreferredWidth(100);
            }else if(i==25){
                column.setPreferredWidth(100);
            }else if(i==26){
                column.setPreferredWidth(100);
            }else if(i==27){
                column.setPreferredWidth(200);
            }else if(i==28){
                column.setPreferredWidth(107);
            }else if(i==29){
                column.setPreferredWidth(100);
            }else if(i==30){
                column.setPreferredWidth(100);
            }else if(i==31){
                column.setPreferredWidth(100);
            }else if(i==32){
                column.setPreferredWidth(100);
            }else if(i==33){
                column.setPreferredWidth(200);
            }else if(i==34){
                column.setPreferredWidth(95);
            }else if(i==35){
                column.setPreferredWidth(150);
            }else if(i==36){
                column.setPreferredWidth(95);
            }else if(i==37){
                column.setPreferredWidth(150);
            }else if(i==38){
                column.setPreferredWidth(100);
            }else if(i==39){
                column.setPreferredWidth(200);
            }else if(i==40){
                column.setPreferredWidth(200);
            }else if(i==41){
                column.setPreferredWidth(200);
            }else if(i==42){
                column.setPreferredWidth(200);
            }else if(i==43){
                column.setPreferredWidth(150);
            }else if(i==44){
                column.setPreferredWidth(100);
            }else if(i==45){
                column.setPreferredWidth(200);
            }else if(i==46){
                column.setPreferredWidth(200);
            }else if(i==47){
                column.setPreferredWidth(200);
            }else if(i==48){
                column.setPreferredWidth(200);
            }else if(i==46){
                column.setPreferredWidth(200);
            }else if(i==47){
                column.setPreferredWidth(200);
            }else if(i==48){
                column.setPreferredWidth(200);
            }else if(i==49){
                column.setPreferredWidth(200);
            }else if(i==50){
                column.setPreferredWidth(200);
            }else if(i==51){
                column.setPreferredWidth(200);
            }else if(i==52){
                column.setPreferredWidth(200);
            }else if(i==53){
                column.setPreferredWidth(200);
            }else if(i==54){
                column.setPreferredWidth(200);
            }else if(i==55){
                column.setPreferredWidth(200);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        AsalRuang.setDocument(new batasInput((byte)30).getKata(AsalRuang));
        RuangSelanjutnya.setDocument(new batasInput((byte)30).getKata(RuangSelanjutnya));
        AlasanTransfer.setDocument(new batasInput((int)50).getKata(AlasanTransfer));
        DiagnosaSekunder.setDocument(new batasInput((int)100).getKata(DiagnosaSekunder));
        KeteranganIndikasiPindahRuang.setDocument(new batasInput((int)50).getKata(KeteranganIndikasiPindahRuang));
        ProsedurDilakukan.setDocument(new batasInput((int)800).getKata(ProsedurDilakukan));
        ObatYangDiberikan.setDocument(new batasInput((int)800).getKata(ObatYangDiberikan));
       
        PemeriksaanPenunjang.setDocument(new batasInput((int)500).getKata(PemeriksaanPenunjang));
        NamaMenyetujui.setDocument(new batasInput((int)50).getKata(NamaMenyetujui));
        KeluhanUtamaSebelumTransfer.setDocument(new batasInput((int)200).getKata(KeluhanUtamaSebelumTransfer));
        TDSebelumTransfer.setDocument(new batasInput((int)7).getKata(TDSebelumTransfer));
        NadiSebelumTransfer.setDocument(new batasInput((int)5).getKata(NadiSebelumTransfer));
        RRSebelumTransfer.setDocument(new batasInput((int)5).getKata(RRSebelumTransfer));
        SuhuSebelumTransfer.setDocument(new batasInput((int)5).getKata(SuhuSebelumTransfer));
       
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        

        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){
                    if(pilihan==1){
                        KdPetugasMenyerahkan.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        NmPetugasMenyerahkan.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        KdPetugasMenyerahkan.requestFocus();
                    }else if(pilihan==3){
                        KdPetugasPJ.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        NmPetugasPJ.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        KdPetugasPJ.requestFocus();
                    }
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        LoadHTML2.setEditable(true);
        LoadHTML2.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
        LoadHTML2.setDocument(doc);
        
        ChkAccor.setSelected(false);
        isPhoto();
        jam2();
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        DlgSP = new javax.swing.JDialog();
        internalFrame7 = new widget.InternalFrame();
        panelBiasa6 = new widget.PanelBiasa();
        BtnSimpanVerifikasi = new widget.Button();
        BtnKeluarSP = new widget.Button();
        jLabel53 = new widget.Label();
        BtnCetakVerifikasi = new widget.Button();
        DTPVerifikasi = new widget.Tanggal();
        NoRawat = new widget.TextBox();
        jLabel55 = new widget.Label();
        jLabel56 = new widget.Label();
        NoRM = new widget.TextBox();
        jLabel60 = new widget.Label();
        dari = new widget.TextBox();
        jLabel61 = new widget.Label();
        NmPasien = new widget.TextBox();
        jLabel63 = new widget.Label();
        jLabel64 = new widget.Label();
        Jam1 = new widget.ComboBox();
        Menit1 = new widget.ComboBox();
        Detik1 = new widget.ComboBox();
        ChkTransfer = new widget.CekBox();
        jLabel65 = new widget.Label();
        NmPetugasPJ = new widget.TextBox();
        KdPetugasPJ = new widget.TextBox();
        jLabel66 = new widget.Label();
        ke = new widget.TextBox();
        jLabel67 = new widget.Label();
        level1 = new widget.ComboBox();
        jLabel68 = new widget.Label();
        tanggalmasuk1 = new widget.Tanggal();
        tanggalpindah1 = new widget.Tanggal();
        scrollPane8 = new widget.ScrollPane();
        rtl1 = new widget.TextArea();
        BtnMenerima1 = new widget.Button();
        jLabel62 = new widget.Label();
        jLabel69 = new widget.Label();
        KeadaanUmumSetelahTransfer1 = new widget.ComboBox();
        jLabel70 = new widget.Label();
        TDSetelahTransfer1 = new widget.TextBox();
        jLabel71 = new widget.Label();
        NadiSetelahTransfer1 = new widget.TextBox();
        jLabel72 = new widget.Label();
        jLabel73 = new widget.Label();
        RRSetelahTransfer1 = new widget.TextBox();
        jLabel74 = new widget.Label();
        SuhuSetelahTransfer1 = new widget.TextBox();
        jLabel75 = new widget.Label();
        jLabel76 = new widget.Label();
        jLabel77 = new widget.Label();
        jLabel78 = new widget.Label();
        scrollPane9 = new widget.ScrollPane();
        KeluhanUtamaSetelahTransfer1 = new widget.TextArea();
        jLabel54 = new widget.Label();
        ket_lainnya1 = new widget.TextBox();
        ChkRM1 = new widget.CekBox();
        ChkLAB1 = new widget.CekBox();
        jLabel79 = new widget.Label();
        ChkRAD1 = new widget.CekBox();
        jLabel80 = new widget.Label();
        ChkLainnya1 = new widget.CekBox();
        jLabel81 = new widget.Label();
        jLabel82 = new widget.Label();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnSerahTerimaTransferPasien = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        jSeparator14 = new javax.swing.JSeparator();
        MenyetujuiPemindahan = new widget.ComboBox();
        jLabel57 = new widget.Label();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        label11 = new widget.Label();
        jLabel11 = new widget.Label();
        TanggalMasuk = new widget.Tanggal();
        label12 = new widget.Label();
        TanggalPindah = new widget.Tanggal();
        KeteranganIndikasiPindahRuang = new widget.TextBox();
        jSeparator1 = new javax.swing.JSeparator();
        RuangSelanjutnya = new widget.TextBox();
        jLabel14 = new widget.Label();
        jLabel15 = new widget.Label();
        jLabel16 = new widget.Label();
        AlasanTransfer = new widget.TextBox();
        jLabel18 = new widget.Label();
        jLabel30 = new widget.Label();
        scrollPane1 = new widget.ScrollPane();
        DiagnosaSekunder = new widget.TextArea();
        jLabel31 = new widget.Label();
        scrollPane2 = new widget.ScrollPane();
        ProsedurDilakukan = new widget.TextArea();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel32 = new widget.Label();
        jLabel33 = new widget.Label();
        scrollPane3 = new widget.ScrollPane();
        ObatYangDiberikan = new widget.TextArea();
        scrollPane4 = new widget.ScrollPane();
        PemeriksaanPenunjang = new widget.TextArea();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel34 = new widget.Label();
        IndikasiPindah = new widget.ComboBox();
        AsalRuang = new widget.TextBox();
        MetodePemindahan = new widget.ComboBox();
        jLabel36 = new widget.Label();
        jLabel37 = new widget.Label();
        NamaMenyetujui = new widget.TextBox();
        jLabel38 = new widget.Label();
        HubunganMenyetujui = new widget.ComboBox();
        jLabel39 = new widget.Label();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel40 = new widget.Label();
        jLabel41 = new widget.Label();
        scrollPane5 = new widget.ScrollPane();
        KeluhanUtamaSebelumTransfer = new widget.TextArea();
        jLabel42 = new widget.Label();
        KeadaanUmumSebelumTransfer = new widget.ComboBox();
        TDSebelumTransfer = new widget.TextBox();
        jLabel23 = new widget.Label();
        jLabel17 = new widget.Label();
        jLabel22 = new widget.Label();
        NadiSebelumTransfer = new widget.TextBox();
        jLabel26 = new widget.Label();
        RRSebelumTransfer = new widget.TextBox();
        jLabel25 = new widget.Label();
        jLabel24 = new widget.Label();
        SuhuSebelumTransfer = new widget.TextBox();
        jLabel27 = new widget.Label();
        jLabel28 = new widget.Label();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        label14 = new widget.Label();
        KdPetugasMenyerahkan = new widget.TextBox();
        NmPetugasMenyerahkan = new widget.TextBox();
        BtnDokter = new widget.Button();
        label16 = new widget.Label();
        level = new widget.ComboBox();
        jLabel58 = new widget.Label();
        jLabel59 = new widget.Label();
        scrollPane7 = new widget.ScrollPane();
        rtl = new widget.TextArea();
        jLabel43 = new widget.Label();
        scrollPane6 = new widget.ScrollPane();
        PeralatanMenyertai = new widget.TextArea();
        scrollPane10 = new widget.ScrollPane();
        ObatMenyertai = new widget.TextArea();
        jLabel44 = new widget.Label();
        scrollPane11 = new widget.ScrollPane();
        A = new widget.TextArea();
        jLabel45 = new widget.Label();
        jLabel46 = new widget.Label();
        scrollPane12 = new widget.ScrollPane();
        S = new widget.TextArea();
        jLabel47 = new widget.Label();
        scrollPane13 = new widget.ScrollPane();
        B = new widget.TextArea();
        jLabel48 = new widget.Label();
        scrollPane14 = new widget.ScrollPane();
        R = new widget.TextArea();
        ket_lainnya = new widget.TextBox();
        ChkRM = new widget.CekBox();
        jLabel49 = new widget.Label();
        ChkLAB = new widget.CekBox();
        jLabel50 = new widget.Label();
        ChkRAD = new widget.CekBox();
        jLabel51 = new widget.Label();
        ChkLainnya = new widget.CekBox();
        jLabel52 = new widget.Label();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        PanelAccor = new widget.PanelBiasa();
        ChkAccor = new widget.CekBox();
        FormPhoto = new widget.PanelBiasa();
        FormPass3 = new widget.PanelBiasa();
        btnAmbil = new widget.Button();
        BtnRefreshPhoto1 = new widget.Button();
        BtnRefreshPhoto2 = new widget.Button();
        Scroll5 = new widget.ScrollPane();
        LoadHTML2 = new widget.editorpane();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        DlgSP.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        DlgSP.setMinimumSize(new java.awt.Dimension(10, 35));
        DlgSP.setName("DlgSP"); // NOI18N
        DlgSP.setUndecorated(true);

        internalFrame7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(230, 235, 225)), "::[Serah Terima Transfer Pasien ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 70, 50))); // NOI18N
        internalFrame7.setMinimumSize(new java.awt.Dimension(10, 35));
        internalFrame7.setName("internalFrame7"); // NOI18N
        internalFrame7.setPreferredSize(new java.awt.Dimension(10, 35));
        internalFrame7.setLayout(new java.awt.BorderLayout(1, 1));

        panelBiasa6.setBackground(new java.awt.Color(255, 255, 255));
        panelBiasa6.setName("panelBiasa6"); // NOI18N
        panelBiasa6.setLayout(null);

        BtnSimpanVerifikasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpanVerifikasi.setMnemonic('T');
        BtnSimpanVerifikasi.setText("Simpan");
        BtnSimpanVerifikasi.setToolTipText("Alt+T");
        BtnSimpanVerifikasi.setName("BtnSimpanVerifikasi"); // NOI18N
        BtnSimpanVerifikasi.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpanVerifikasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanVerifikasiActionPerformed(evt);
            }
        });
        panelBiasa6.add(BtnSimpanVerifikasi);
        BtnSimpanVerifikasi.setBounds(30, 460, 130, 30);

        BtnKeluarSP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluarSP.setMnemonic('K');
        BtnKeluarSP.setText("Tutup");
        BtnKeluarSP.setToolTipText("Alt+K");
        BtnKeluarSP.setName("BtnKeluarSP"); // NOI18N
        BtnKeluarSP.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluarSP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarSPActionPerformed(evt);
            }
        });
        panelBiasa6.add(BtnKeluarSP);
        BtnKeluarSP.setBounds(650, 460, 100, 30);

        jLabel53.setText("Tgl. Terima :");
        jLabel53.setName("jLabel53"); // NOI18N
        jLabel53.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel53);
        jLabel53.setBounds(50, 420, 70, 23);

        BtnCetakVerifikasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnCetakVerifikasi.setMnemonic('T');
        BtnCetakVerifikasi.setText("Cetak Lembar Transfer");
        BtnCetakVerifikasi.setToolTipText("Alt+T");
        BtnCetakVerifikasi.setName("BtnCetakVerifikasi"); // NOI18N
        BtnCetakVerifikasi.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnCetakVerifikasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCetakVerifikasiActionPerformed(evt);
            }
        });
        panelBiasa6.add(BtnCetakVerifikasi);
        BtnCetakVerifikasi.setBounds(180, 460, 190, 30);

        DTPVerifikasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-07-2024" }));
        DTPVerifikasi.setDisplayFormat("dd-MM-yyyy");
        DTPVerifikasi.setName("DTPVerifikasi"); // NOI18N
        DTPVerifikasi.setOpaque(false);
        DTPVerifikasi.setPreferredSize(new java.awt.Dimension(90, 23));
        panelBiasa6.add(DTPVerifikasi);
        DTPVerifikasi.setBounds(130, 420, 110, 23);

        NoRawat.setEditable(false);
        NoRawat.setName("NoRawat"); // NOI18N
        NoRawat.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NoRawat);
        NoRawat.setBounds(100, 10, 220, 23);

        jLabel55.setText("No Rawat :");
        jLabel55.setName("jLabel55"); // NOI18N
        jLabel55.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel55);
        jLabel55.setBounds(30, 10, 60, 23);

        jLabel56.setText("Nama Pasien :");
        jLabel56.setName("jLabel56"); // NOI18N
        jLabel56.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel56);
        jLabel56.setBounds(10, 40, 80, 23);

        NoRM.setEditable(false);
        NoRM.setName("NoRM"); // NOI18N
        NoRM.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NoRM);
        NoRM.setBounds(100, 40, 80, 23);

        jLabel60.setText("Rencana Tindak Lanjut :");
        jLabel60.setName("jLabel60"); // NOI18N
        jLabel60.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel60);
        jLabel60.setBounds(60, 100, 120, 23);

        dari.setEditable(false);
        dari.setName("dari"); // NOI18N
        dari.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(dari);
        dari.setBounds(100, 70, 150, 23);

        jLabel61.setText("Jam. Terima :");
        jLabel61.setName("jLabel61"); // NOI18N
        jLabel61.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel61);
        jLabel61.setBounds(240, 420, 80, 23);

        NmPasien.setEditable(false);
        NmPasien.setName("NmPasien"); // NOI18N
        NmPasien.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NmPasien);
        NmPasien.setBounds(190, 40, 280, 23);

        jLabel63.setText("Tgl. Masuk :");
        jLabel63.setName("jLabel63"); // NOI18N
        jLabel63.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel63);
        jLabel63.setBounds(490, 10, 70, 23);

        jLabel64.setText("Tgl. Pindah :");
        jLabel64.setName("jLabel64"); // NOI18N
        jLabel64.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel64);
        jLabel64.setBounds(500, 40, 60, 23);

        Jam1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam1.setName("Jam1"); // NOI18N
        Jam1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jam1ActionPerformed(evt);
            }
        });
        Jam1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam1KeyPressed(evt);
            }
        });
        panelBiasa6.add(Jam1);
        Jam1.setBounds(320, 420, 62, 23);

        Menit1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit1.setName("Menit1"); // NOI18N
        Menit1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit1KeyPressed(evt);
            }
        });
        panelBiasa6.add(Menit1);
        Menit1.setBounds(390, 420, 62, 23);

        Detik1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik1.setName("Detik1"); // NOI18N
        Detik1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik1KeyPressed(evt);
            }
        });
        panelBiasa6.add(Detik1);
        Detik1.setBounds(460, 420, 62, 23);

        ChkTransfer.setBorder(null);
        ChkTransfer.setSelected(true);
        ChkTransfer.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkTransfer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkTransfer.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkTransfer.setName("ChkTransfer"); // NOI18N
        panelBiasa6.add(ChkTransfer);
        ChkTransfer.setBounds(520, 420, 23, 23);

        jLabel65.setText("Petugas Penerima :");
        jLabel65.setName("jLabel65"); // NOI18N
        jLabel65.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel65);
        jLabel65.setBounds(50, 390, 100, 23);

        NmPetugasPJ.setEditable(false);
        NmPetugasPJ.setName("NmPetugasPJ"); // NOI18N
        NmPetugasPJ.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NmPetugasPJ);
        NmPetugasPJ.setBounds(260, 390, 250, 23);

        KdPetugasPJ.setEditable(false);
        KdPetugasPJ.setName("KdPetugasPJ"); // NOI18N
        KdPetugasPJ.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(KdPetugasPJ);
        KdPetugasPJ.setBounds(150, 390, 110, 23);

        jLabel66.setText("Ke :");
        jLabel66.setName("jLabel66"); // NOI18N
        jLabel66.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel66);
        jLabel66.setBounds(250, 70, 30, 23);

        ke.setEditable(false);
        ke.setName("ke"); // NOI18N
        ke.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(ke);
        ke.setBounds(290, 70, 180, 23);

        jLabel67.setText("Dari :");
        jLabel67.setName("jLabel67"); // NOI18N
        jLabel67.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel67);
        jLabel67.setBounds(10, 70, 80, 23);

        level1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "2", "3" }));
        level1.setEnabled(false);
        level1.setName("level1"); // NOI18N
        level1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                level1KeyPressed(evt);
            }
        });
        panelBiasa6.add(level1);
        level1.setBounds(410, 10, 62, 23);

        jLabel68.setText("Level :");
        jLabel68.setName("jLabel68"); // NOI18N
        jLabel68.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel68);
        jLabel68.setBounds(320, 10, 80, 23);

        tanggalmasuk1.setForeground(new java.awt.Color(50, 70, 50));
        tanggalmasuk1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-07-2024 11:19:25" }));
        tanggalmasuk1.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        tanggalmasuk1.setName("tanggalmasuk1"); // NOI18N
        tanggalmasuk1.setOpaque(false);
        tanggalmasuk1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tanggalmasuk1KeyPressed(evt);
            }
        });
        panelBiasa6.add(tanggalmasuk1);
        tanggalmasuk1.setBounds(570, 10, 130, 23);

        tanggalpindah1.setForeground(new java.awt.Color(50, 70, 50));
        tanggalpindah1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-07-2024 11:19:25" }));
        tanggalpindah1.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        tanggalpindah1.setName("tanggalpindah1"); // NOI18N
        tanggalpindah1.setOpaque(false);
        tanggalpindah1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tanggalpindah1KeyPressed(evt);
            }
        });
        panelBiasa6.add(tanggalpindah1);
        tanggalpindah1.setBounds(570, 40, 130, 23);

        scrollPane8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane8.setName("scrollPane8"); // NOI18N

        rtl1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        rtl1.setColumns(20);
        rtl1.setRows(5);
        rtl1.setEnabled(false);
        rtl1.setName("rtl1"); // NOI18N
        rtl1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rtl1KeyPressed(evt);
            }
        });
        scrollPane8.setViewportView(rtl1);

        panelBiasa6.add(scrollPane8);
        scrollPane8.setBounds(60, 120, 410, 70);

        BtnMenerima1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnMenerima1.setMnemonic('2');
        BtnMenerima1.setToolTipText("Alt+2");
        BtnMenerima1.setName("BtnMenerima1"); // NOI18N
        BtnMenerima1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnMenerima1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMenerima1ActionPerformed(evt);
            }
        });
        BtnMenerima1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnMenerima1KeyPressed(evt);
            }
        });
        panelBiasa6.add(BtnMenerima1);
        BtnMenerima1.setBounds(520, 390, 28, 23);

        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel62.setText("Keadaan Pasien Saat Pindah Setelah Transfer :");
        jLabel62.setName("jLabel62"); // NOI18N
        panelBiasa6.add(jLabel62);
        jLabel62.setBounds(60, 200, 320, 23);

        jLabel69.setText("Dokumen Diterima :");
        jLabel69.setName("jLabel69"); // NOI18N
        panelBiasa6.add(jLabel69);
        jLabel69.setBounds(490, 260, 100, 23);

        KeadaanUmumSetelahTransfer1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Compos Mentis", "Gelisah", "Delirium", "Koma" }));
        KeadaanUmumSetelahTransfer1.setName("KeadaanUmumSetelahTransfer1"); // NOI18N
        KeadaanUmumSetelahTransfer1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeadaanUmumSetelahTransfer1KeyPressed(evt);
            }
        });
        panelBiasa6.add(KeadaanUmumSetelahTransfer1);
        KeadaanUmumSetelahTransfer1.setBounds(600, 230, 160, 23);

        jLabel70.setText("TD :");
        jLabel70.setName("jLabel70"); // NOI18N
        panelBiasa6.add(jLabel70);
        jLabel70.setBounds(50, 320, 30, 23);

        TDSetelahTransfer1.setFocusTraversalPolicyProvider(true);
        TDSetelahTransfer1.setName("TDSetelahTransfer1"); // NOI18N
        TDSetelahTransfer1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TDSetelahTransfer1KeyPressed(evt);
            }
        });
        panelBiasa6.add(TDSetelahTransfer1);
        TDSetelahTransfer1.setBounds(80, 320, 60, 23);

        jLabel71.setText("Nadi :");
        jLabel71.setName("jLabel71"); // NOI18N
        panelBiasa6.add(jLabel71);
        jLabel71.setBounds(40, 260, 40, 23);

        NadiSetelahTransfer1.setFocusTraversalPolicyProvider(true);
        NadiSetelahTransfer1.setName("NadiSetelahTransfer1"); // NOI18N
        NadiSetelahTransfer1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NadiSetelahTransfer1KeyPressed(evt);
            }
        });
        panelBiasa6.add(NadiSetelahTransfer1);
        NadiSetelahTransfer1.setBounds(80, 260, 60, 23);

        jLabel72.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel72.setText("x/menit");
        jLabel72.setName("jLabel72"); // NOI18N
        panelBiasa6.add(jLabel72);
        jLabel72.setBounds(150, 260, 50, 23);

        jLabel73.setText("RR :");
        jLabel73.setName("jLabel73"); // NOI18N
        panelBiasa6.add(jLabel73);
        jLabel73.setBounds(50, 350, 30, 23);

        RRSetelahTransfer1.setFocusTraversalPolicyProvider(true);
        RRSetelahTransfer1.setName("RRSetelahTransfer1"); // NOI18N
        RRSetelahTransfer1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RRSetelahTransfer1KeyPressed(evt);
            }
        });
        panelBiasa6.add(RRSetelahTransfer1);
        RRSetelahTransfer1.setBounds(80, 350, 60, 23);

        jLabel74.setText("Suhu :");
        jLabel74.setName("jLabel74"); // NOI18N
        panelBiasa6.add(jLabel74);
        jLabel74.setBounds(40, 290, 40, 23);

        SuhuSetelahTransfer1.setFocusTraversalPolicyProvider(true);
        SuhuSetelahTransfer1.setName("SuhuSetelahTransfer1"); // NOI18N
        SuhuSetelahTransfer1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SuhuSetelahTransfer1KeyPressed(evt);
            }
        });
        panelBiasa6.add(SuhuSetelahTransfer1);
        SuhuSetelahTransfer1.setBounds(80, 290, 60, 23);

        jLabel75.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel75.setText("°C");
        jLabel75.setName("jLabel75"); // NOI18N
        panelBiasa6.add(jLabel75);
        jLabel75.setBounds(150, 290, 30, 23);

        jLabel76.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel76.setText("mmHg");
        jLabel76.setName("jLabel76"); // NOI18N
        panelBiasa6.add(jLabel76);
        jLabel76.setBounds(150, 320, 50, 23);

        jLabel77.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel77.setText("x/menit");
        jLabel77.setName("jLabel77"); // NOI18N
        panelBiasa6.add(jLabel77);
        jLabel77.setBounds(150, 350, 50, 23);

        jLabel78.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel78.setText("Keluhan Utama :");
        jLabel78.setName("jLabel78"); // NOI18N
        panelBiasa6.add(jLabel78);
        jLabel78.setBounds(210, 230, 170, 23);

        scrollPane9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane9.setName("scrollPane9"); // NOI18N

        KeluhanUtamaSetelahTransfer1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KeluhanUtamaSetelahTransfer1.setColumns(20);
        KeluhanUtamaSetelahTransfer1.setRows(5);
        KeluhanUtamaSetelahTransfer1.setName("KeluhanUtamaSetelahTransfer1"); // NOI18N
        KeluhanUtamaSetelahTransfer1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeluhanUtamaSetelahTransfer1KeyPressed(evt);
            }
        });
        scrollPane9.setViewportView(KeluhanUtamaSetelahTransfer1);

        panelBiasa6.add(scrollPane9);
        scrollPane9.setBounds(210, 260, 260, 110);

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("Rekam Medis");
        jLabel54.setName("jLabel54"); // NOI18N
        panelBiasa6.add(jLabel54);
        jLabel54.setBounds(520, 280, 70, 23);

        ket_lainnya1.setHighlighter(null);
        ket_lainnya1.setName("ket_lainnya1"); // NOI18N
        ket_lainnya1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ket_lainnya1KeyPressed(evt);
            }
        });
        panelBiasa6.add(ket_lainnya1);
        ket_lainnya1.setBounds(560, 340, 200, 23);

        ChkRM1.setBorder(null);
        ChkRM1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkRM1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkRM1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkRM1.setName("ChkRM1"); // NOI18N
        panelBiasa6.add(ChkRM1);
        ChkRM1.setBounds(490, 280, 23, 23);

        ChkLAB1.setBorder(null);
        ChkLAB1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkLAB1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkLAB1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkLAB1.setName("ChkLAB1"); // NOI18N
        panelBiasa6.add(ChkLAB1);
        ChkLAB1.setBounds(490, 300, 23, 23);

        jLabel79.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel79.setText("Hasil Laboratorium");
        jLabel79.setName("jLabel79"); // NOI18N
        panelBiasa6.add(jLabel79);
        jLabel79.setBounds(520, 300, 90, 23);

        ChkRAD1.setBorder(null);
        ChkRAD1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkRAD1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkRAD1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkRAD1.setName("ChkRAD1"); // NOI18N
        panelBiasa6.add(ChkRAD1);
        ChkRAD1.setBounds(490, 320, 23, 23);

        jLabel80.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel80.setText("Hasil Radiologi");
        jLabel80.setName("jLabel80"); // NOI18N
        panelBiasa6.add(jLabel80);
        jLabel80.setBounds(520, 320, 90, 23);

        ChkLainnya1.setBorder(null);
        ChkLainnya1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkLainnya1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkLainnya1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkLainnya1.setName("ChkLainnya1"); // NOI18N
        panelBiasa6.add(ChkLainnya1);
        ChkLainnya1.setBounds(490, 340, 23, 23);

        jLabel81.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel81.setText("Lainnya");
        jLabel81.setName("jLabel81"); // NOI18N
        panelBiasa6.add(jLabel81);
        jLabel81.setBounds(520, 340, 40, 23);

        jLabel82.setText("Keadaan Umum :");
        jLabel82.setName("jLabel82"); // NOI18N
        panelBiasa6.add(jLabel82);
        jLabel82.setBounds(490, 230, 90, 23);

        internalFrame7.add(panelBiasa6, java.awt.BorderLayout.CENTER);

        DlgSP.getContentPane().add(internalFrame7, java.awt.BorderLayout.CENTER);

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnSerahTerimaTransferPasien.setBackground(new java.awt.Color(255, 255, 254));
        MnSerahTerimaTransferPasien.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnSerahTerimaTransferPasien.setForeground(new java.awt.Color(50, 50, 50));
        MnSerahTerimaTransferPasien.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnSerahTerimaTransferPasien.setText("Serah Terima Transfer Pasien");
        MnSerahTerimaTransferPasien.setName("MnSerahTerimaTransferPasien"); // NOI18N
        MnSerahTerimaTransferPasien.setPreferredSize(new java.awt.Dimension(240, 26));
        MnSerahTerimaTransferPasien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnSerahTerimaTransferPasienActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnSerahTerimaTransferPasien);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Transfer Pasien Antar Ruang ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 950));
        FormInput.setLayout(null);

        jSeparator14.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator14.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator14.setName("jSeparator14"); // NOI18N
        FormInput.add(jSeparator14);
        jSeparator14.setBounds(0, 861, 880, 0);

        MenyetujuiPemindahan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ya", "Tidak" }));
        MenyetujuiPemindahan.setName("MenyetujuiPemindahan"); // NOI18N
        MenyetujuiPemindahan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenyetujuiPemindahanKeyPressed(evt);
            }
        });
        FormInput.add(MenyetujuiPemindahan);
        MenyetujuiPemindahan.setBounds(780, 450, 80, 23);

        jLabel57.setText("Rencana Tindak Lanjut :");
        jLabel57.setName("jLabel57"); // NOI18N
        FormInput.add(jLabel57);
        jLabel57.setBounds(480, 90, 120, 30);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        label11.setText("Masuk :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(0, 40, 70, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        TanggalMasuk.setForeground(new java.awt.Color(50, 70, 50));
        TanggalMasuk.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-07-2024 11:19:24" }));
        TanggalMasuk.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TanggalMasuk.setName("TanggalMasuk"); // NOI18N
        TanggalMasuk.setOpaque(false);
        TanggalMasuk.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TanggalMasukKeyPressed(evt);
            }
        });
        FormInput.add(TanggalMasuk);
        TanggalMasuk.setBounds(74, 40, 130, 23);

        label12.setText("Pindah :");
        label12.setName("label12"); // NOI18N
        label12.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label12);
        label12.setBounds(201, 40, 55, 23);

        TanggalPindah.setForeground(new java.awt.Color(50, 70, 50));
        TanggalPindah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-07-2024 11:19:24" }));
        TanggalPindah.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TanggalPindah.setName("TanggalPindah"); // NOI18N
        TanggalPindah.setOpaque(false);
        TanggalPindah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TanggalPindahKeyPressed(evt);
            }
        });
        FormInput.add(TanggalPindah);
        TanggalPindah.setBounds(260, 40, 130, 23);

        KeteranganIndikasiPindahRuang.setHighlighter(null);
        KeteranganIndikasiPindahRuang.setName("KeteranganIndikasiPindahRuang"); // NOI18N
        KeteranganIndikasiPindahRuang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeteranganIndikasiPindahRuangKeyPressed(evt);
            }
        });
        FormInput.add(KeteranganIndikasiPindahRuang);
        KeteranganIndikasiPindahRuang.setBounds(730, 40, 130, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 70, 880, 1);

        RuangSelanjutnya.setHighlighter(null);
        RuangSelanjutnya.setName("RuangSelanjutnya"); // NOI18N
        RuangSelanjutnya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RuangSelanjutnyaKeyPressed(evt);
            }
        });
        FormInput.add(RuangSelanjutnya);
        RuangSelanjutnya.setBounds(190, 170, 260, 23);

        jLabel14.setText("Metode Pemindahan :");
        jLabel14.setName("jLabel14"); // NOI18N
        FormInput.add(jLabel14);
        jLabel14.setBounds(190, 100, 104, 23);

        jLabel15.setText("Ruang Rawat Selanjutnya :");
        jLabel15.setName("jLabel15"); // NOI18N
        FormInput.add(jLabel15);
        jLabel15.setBounds(10, 170, 160, 23);

        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel16.setText("Alasan Transfer :");
        jLabel16.setName("jLabel16"); // NOI18N
        FormInput.add(jLabel16);
        jLabel16.setBounds(90, 200, 90, 23);

        AlasanTransfer.setHighlighter(null);
        AlasanTransfer.setName("AlasanTransfer"); // NOI18N
        AlasanTransfer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AlasanTransferKeyPressed(evt);
            }
        });
        FormInput.add(AlasanTransfer);
        AlasanTransfer.setBounds(190, 200, 660, 23);

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel18.setText("Asal Ruang Rawat / Poliklinik :");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(30, 140, 149, 23);

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel30.setText("Diagnosa  :");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(20, 270, 170, 23);

        scrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane1.setName("scrollPane1"); // NOI18N

        DiagnosaSekunder.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        DiagnosaSekunder.setColumns(20);
        DiagnosaSekunder.setRows(5);
        DiagnosaSekunder.setName("DiagnosaSekunder"); // NOI18N
        DiagnosaSekunder.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DiagnosaSekunderKeyPressed(evt);
            }
        });
        scrollPane1.setViewportView(DiagnosaSekunder);

        FormInput.add(scrollPane1);
        scrollPane1.setBounds(20, 290, 410, 43);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("Prosedur Yang Sudah Dilakukan :");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(450, 270, 170, 23);

        scrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane2.setName("scrollPane2"); // NOI18N

        ProsedurDilakukan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        ProsedurDilakukan.setColumns(20);
        ProsedurDilakukan.setRows(5);
        ProsedurDilakukan.setName("ProsedurDilakukan"); // NOI18N
        ProsedurDilakukan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ProsedurDilakukanKeyPressed(evt);
            }
        });
        scrollPane2.setViewportView(ProsedurDilakukan);

        FormInput.add(scrollPane2);
        scrollPane2.setBounds(450, 290, 410, 43);

        jSeparator2.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator2.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator2.setName("jSeparator2"); // NOI18N
        FormInput.add(jSeparator2);
        jSeparator2.setBounds(0, 230, 880, 1);

        jSeparator3.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator3.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator3.setName("jSeparator3"); // NOI18N
        FormInput.add(jSeparator3);
        jSeparator3.setBounds(0, 340, 880, 1);

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel32.setText("Obat Yang Telah Diberikan :");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(20, 340, 170, 23);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel33.setText("Pemeriksaan Penunjang Yang Sudah Dilakukan :");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(450, 340, 370, 23);

        scrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane3.setName("scrollPane3"); // NOI18N

        ObatYangDiberikan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        ObatYangDiberikan.setColumns(20);
        ObatYangDiberikan.setRows(5);
        ObatYangDiberikan.setName("ObatYangDiberikan"); // NOI18N
        ObatYangDiberikan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ObatYangDiberikanKeyPressed(evt);
            }
        });
        scrollPane3.setViewportView(ObatYangDiberikan);

        FormInput.add(scrollPane3);
        scrollPane3.setBounds(20, 360, 410, 73);

        scrollPane4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane4.setName("scrollPane4"); // NOI18N

        PemeriksaanPenunjang.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        PemeriksaanPenunjang.setColumns(20);
        PemeriksaanPenunjang.setRows(5);
        PemeriksaanPenunjang.setName("PemeriksaanPenunjang"); // NOI18N
        PemeriksaanPenunjang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PemeriksaanPenunjangKeyPressed(evt);
            }
        });
        scrollPane4.setViewportView(PemeriksaanPenunjang);

        FormInput.add(scrollPane4);
        scrollPane4.setBounds(450, 360, 410, 73);

        jSeparator4.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator4.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator4.setName("jSeparator4"); // NOI18N
        FormInput.add(jSeparator4);
        jSeparator4.setBounds(0, 440, 880, 1);

        jLabel34.setText("Pasien/Keluarga Mengetahui & Menyetujui Alasan Pemindahan :");
        jLabel34.setName("jLabel34"); // NOI18N
        FormInput.add(jLabel34);
        jLabel34.setBounds(440, 450, 320, 23);

        IndikasiPindah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Kondisi Pasien Stabil", "Kondisi Pasien Tidak Ada Perubahan", "Kondisi Pasien Memburuk", "Fasilitas Kurang Memadai", "Fasilitas Butuh Lebih Baik", "Tenaga Membutuhkan Yang Lebih Ahli", "Tenaga Kurang", "Lain-lain" }));
        IndikasiPindah.setName("IndikasiPindah"); // NOI18N
        IndikasiPindah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                IndikasiPindahKeyPressed(evt);
            }
        });
        FormInput.add(IndikasiPindah);
        IndikasiPindah.setBounds(480, 40, 235, 23);

        AsalRuang.setHighlighter(null);
        AsalRuang.setName("AsalRuang"); // NOI18N
        AsalRuang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AsalRuangKeyPressed(evt);
            }
        });
        FormInput.add(AsalRuang);
        AsalRuang.setBounds(190, 140, 260, 23);

        MetodePemindahan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Kursi Roda", "Tempat Tidur", "Brankar", "Jalan Sendiri", "-" }));
        MetodePemindahan.setName("MetodePemindahan"); // NOI18N
        MetodePemindahan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MetodePemindahanKeyPressed(evt);
            }
        });
        FormInput.add(MetodePemindahan);
        MetodePemindahan.setBounds(300, 100, 150, 23);

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel36.setText(" Dokumen yang disertakan :");
        jLabel36.setName("jLabel36"); // NOI18N
        FormInput.add(jLabel36);
        jLabel36.setBounds(10, 620, 170, 23);

        jLabel37.setText(":");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(370, 450, 20, 23);

        NamaMenyetujui.setHighlighter(null);
        NamaMenyetujui.setName("NamaMenyetujui"); // NOI18N
        NamaMenyetujui.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NamaMenyetujuiKeyPressed(evt);
            }
        });
        FormInput.add(NamaMenyetujui);
        NamaMenyetujui.setBounds(20, 480, 410, 23);

        jLabel38.setText("Hubungan :");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(620, 480, 80, 23);

        HubunganMenyetujui.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Anak", "Kakak", "Adik", "Saudara", "Keluarga", "Kakek", "Nenek", "Orang Tua", "Suami", "Istri", "Penanggung Jawab", "Menantu", "Ipar", "Mertua", "-" }));
        HubunganMenyetujui.setName("HubunganMenyetujui"); // NOI18N
        HubunganMenyetujui.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HubunganMenyetujuiKeyPressed(evt);
            }
        });
        FormInput.add(HubunganMenyetujui);
        HubunganMenyetujui.setBounds(710, 480, 150, 23);

        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel39.setText("Bila Pemberi Persetujuan Adalah Keluarga/Penanggung Jawab Pasien, Nama");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(20, 450, 380, 23);

        jSeparator5.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator5.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator5.setName("jSeparator5"); // NOI18N
        FormInput.add(jSeparator5);
        jSeparator5.setBounds(0, 510, 880, 1);

        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel40.setText("Keadaan Pasien Saat Pindah Sebelum Transfer :");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(20, 510, 320, 23);

        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel41.setText("Keluhan Utama :");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(450, 530, 170, 23);

        scrollPane5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane5.setName("scrollPane5"); // NOI18N

        KeluhanUtamaSebelumTransfer.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        KeluhanUtamaSebelumTransfer.setColumns(20);
        KeluhanUtamaSebelumTransfer.setRows(5);
        KeluhanUtamaSebelumTransfer.setName("KeluhanUtamaSebelumTransfer"); // NOI18N
        KeluhanUtamaSebelumTransfer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeluhanUtamaSebelumTransferKeyPressed(evt);
            }
        });
        scrollPane5.setViewportView(KeluhanUtamaSebelumTransfer);

        FormInput.add(scrollPane5);
        scrollPane5.setBounds(450, 550, 410, 63);

        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel42.setText("Rekam Medis");
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(40, 640, 70, 23);

        KeadaanUmumSebelumTransfer.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Compos Mentis", "Gelisah", "Delirium", "Koma" }));
        KeadaanUmumSebelumTransfer.setName("KeadaanUmumSebelumTransfer"); // NOI18N
        KeadaanUmumSebelumTransfer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeadaanUmumSebelumTransferKeyPressed(evt);
            }
        });
        FormInput.add(KeadaanUmumSebelumTransfer);
        KeadaanUmumSebelumTransfer.setBounds(20, 570, 120, 30);

        TDSebelumTransfer.setFocusTraversalPolicyProvider(true);
        TDSebelumTransfer.setName("TDSebelumTransfer"); // NOI18N
        TDSebelumTransfer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TDSebelumTransferKeyPressed(evt);
            }
        });
        FormInput.add(TDSebelumTransfer);
        TDSebelumTransfer.setBounds(320, 550, 60, 23);

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel23.setText("mmHg");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(390, 550, 50, 23);

        jLabel17.setText("Nadi :");
        jLabel17.setName("jLabel17"); // NOI18N
        FormInput.add(jLabel17);
        jLabel17.setBounds(100, 550, 90, 23);

        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel22.setText("x/menit");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(260, 550, 50, 23);

        NadiSebelumTransfer.setFocusTraversalPolicyProvider(true);
        NadiSebelumTransfer.setName("NadiSebelumTransfer"); // NOI18N
        NadiSebelumTransfer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NadiSebelumTransferKeyPressed(evt);
            }
        });
        FormInput.add(NadiSebelumTransfer);
        NadiSebelumTransfer.setBounds(190, 550, 60, 23);

        jLabel26.setText("RR :");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(290, 580, 30, 23);

        RRSebelumTransfer.setFocusTraversalPolicyProvider(true);
        RRSebelumTransfer.setName("RRSebelumTransfer"); // NOI18N
        RRSebelumTransfer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RRSebelumTransferKeyPressed(evt);
            }
        });
        FormInput.add(RRSebelumTransfer);
        RRSebelumTransfer.setBounds(320, 580, 60, 23);

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel25.setText("x/menit");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(390, 580, 50, 23);

        jLabel24.setText("Suhu :");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(100, 580, 90, 23);

        SuhuSebelumTransfer.setFocusTraversalPolicyProvider(true);
        SuhuSebelumTransfer.setName("SuhuSebelumTransfer"); // NOI18N
        SuhuSebelumTransfer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SuhuSebelumTransferKeyPressed(evt);
            }
        });
        FormInput.add(SuhuSebelumTransfer);
        SuhuSebelumTransfer.setBounds(190, 580, 60, 23);

        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel27.setText("°C");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(260, 580, 30, 23);

        jLabel28.setText("TD :");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(290, 550, 30, 23);

        jSeparator6.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator6.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator6.setName("jSeparator6"); // NOI18N
        FormInput.add(jSeparator6);
        jSeparator6.setBounds(0, 620, 880, 1);

        jSeparator7.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator7.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator7.setName("jSeparator7"); // NOI18N
        FormInput.add(jSeparator7);
        jSeparator7.setBounds(0, 730, 880, 1);

        label14.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label14.setText("Petugas / Perawat :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(460, 850, 130, 23);

        KdPetugasMenyerahkan.setEditable(false);
        KdPetugasMenyerahkan.setName("KdPetugasMenyerahkan"); // NOI18N
        KdPetugasMenyerahkan.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput.add(KdPetugasMenyerahkan);
        KdPetugasMenyerahkan.setBounds(540, 870, 100, 23);

        NmPetugasMenyerahkan.setEditable(false);
        NmPetugasMenyerahkan.setName("NmPetugasMenyerahkan"); // NOI18N
        NmPetugasMenyerahkan.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugasMenyerahkan);
        NmPetugasMenyerahkan.setBounds(640, 870, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(830, 870, 28, 23);

        label16.setText("Menyerahkan :");
        label16.setName("label16"); // NOI18N
        label16.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label16);
        label16.setBounds(440, 870, 90, 23);

        level.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "2", "3" }));
        level.setName("level"); // NOI18N
        level.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                levelKeyPressed(evt);
            }
        });
        FormInput.add(level);
        level.setBounds(90, 100, 90, 23);

        jLabel58.setText("Indikasi Pindah :");
        jLabel58.setName("jLabel58"); // NOI18N
        FormInput.add(jLabel58);
        jLabel58.setBounds(400, 40, 80, 23);

        jLabel59.setText(" Level :");
        jLabel59.setName("jLabel59"); // NOI18N
        FormInput.add(jLabel59);
        jLabel59.setBounds(40, 100, 40, 23);

        scrollPane7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane7.setName("scrollPane7"); // NOI18N

        rtl.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        rtl.setColumns(20);
        rtl.setRows(5);
        rtl.setName("rtl"); // NOI18N
        rtl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rtlKeyPressed(evt);
            }
        });
        scrollPane7.setViewportView(rtl);

        FormInput.add(scrollPane7);
        scrollPane7.setBounds(480, 120, 370, 70);

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel43.setText("Peralatan Yang Menyertai :");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(240, 620, 150, 23);

        scrollPane6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane6.setName("scrollPane6"); // NOI18N

        PeralatanMenyertai.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        PeralatanMenyertai.setColumns(20);
        PeralatanMenyertai.setRows(5);
        PeralatanMenyertai.setName("PeralatanMenyertai"); // NOI18N
        PeralatanMenyertai.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PeralatanMenyertaiKeyPressed(evt);
            }
        });
        scrollPane6.setViewportView(PeralatanMenyertai);

        FormInput.add(scrollPane6);
        scrollPane6.setBounds(240, 640, 290, 80);

        scrollPane10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane10.setName("scrollPane10"); // NOI18N

        ObatMenyertai.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        ObatMenyertai.setColumns(20);
        ObatMenyertai.setRows(5);
        ObatMenyertai.setName("ObatMenyertai"); // NOI18N
        ObatMenyertai.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ObatMenyertaiKeyPressed(evt);
            }
        });
        scrollPane10.setViewportView(ObatMenyertai);

        FormInput.add(scrollPane10);
        scrollPane10.setBounds(570, 640, 290, 80);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel44.setText("Obat / Cairan yang dibawa pada saat transfer ruang :");
        jLabel44.setName("jLabel44"); // NOI18N
        FormInput.add(jLabel44);
        jLabel44.setBounds(570, 620, 290, 23);

        scrollPane11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane11.setName("scrollPane11"); // NOI18N

        A.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        A.setColumns(20);
        A.setRows(5);
        A.setName("A"); // NOI18N
        A.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AKeyPressed(evt);
            }
        });
        scrollPane11.setViewportView(A);

        FormInput.add(scrollPane11);
        scrollPane11.setBounds(460, 740, 180, 90);

        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel45.setText("A :");
        jLabel45.setName("jLabel45"); // NOI18N
        FormInput.add(jLabel45);
        jLabel45.setBounds(440, 740, 20, 20);

        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel46.setText("S :");
        jLabel46.setName("jLabel46"); // NOI18N
        FormInput.add(jLabel46);
        jLabel46.setBounds(10, 740, 20, 20);

        scrollPane12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane12.setName("scrollPane12"); // NOI18N

        S.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        S.setColumns(20);
        S.setRows(5);
        S.setName("S"); // NOI18N
        S.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SKeyPressed(evt);
            }
        });
        scrollPane12.setViewportView(S);

        FormInput.add(scrollPane12);
        scrollPane12.setBounds(30, 740, 180, 90);

        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel47.setText("B :");
        jLabel47.setName("jLabel47"); // NOI18N
        FormInput.add(jLabel47);
        jLabel47.setBounds(230, 740, 20, 20);

        scrollPane13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane13.setName("scrollPane13"); // NOI18N

        B.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        B.setColumns(20);
        B.setRows(5);
        B.setName("B"); // NOI18N
        B.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BKeyPressed(evt);
            }
        });
        scrollPane13.setViewportView(B);

        FormInput.add(scrollPane13);
        scrollPane13.setBounds(250, 740, 180, 90);

        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel48.setText("R :");
        jLabel48.setName("jLabel48"); // NOI18N
        FormInput.add(jLabel48);
        jLabel48.setBounds(650, 740, 20, 20);

        scrollPane14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane14.setName("scrollPane14"); // NOI18N

        R.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        R.setColumns(20);
        R.setRows(5);
        R.setName("R"); // NOI18N
        R.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RKeyPressed(evt);
            }
        });
        scrollPane14.setViewportView(R);

        FormInput.add(scrollPane14);
        scrollPane14.setBounds(670, 740, 190, 90);

        ket_lainnya.setHighlighter(null);
        ket_lainnya.setName("ket_lainnya"); // NOI18N
        ket_lainnya.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ket_lainnyaKeyPressed(evt);
            }
        });
        FormInput.add(ket_lainnya);
        ket_lainnya.setBounds(80, 700, 130, 23);

        ChkRM.setBorder(null);
        ChkRM.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkRM.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkRM.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkRM.setName("ChkRM"); // NOI18N
        FormInput.add(ChkRM);
        ChkRM.setBounds(10, 640, 23, 23);

        jLabel49.setText("Keadaan Umum :");
        jLabel49.setName("jLabel49"); // NOI18N
        FormInput.add(jLabel49);
        jLabel49.setBounds(10, 550, 90, 23);

        ChkLAB.setBorder(null);
        ChkLAB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkLAB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkLAB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkLAB.setName("ChkLAB"); // NOI18N
        FormInput.add(ChkLAB);
        ChkLAB.setBounds(10, 660, 23, 23);

        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel50.setText("Hasil Laboratorium");
        jLabel50.setName("jLabel50"); // NOI18N
        FormInput.add(jLabel50);
        jLabel50.setBounds(40, 660, 90, 23);

        ChkRAD.setBorder(null);
        ChkRAD.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkRAD.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkRAD.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkRAD.setName("ChkRAD"); // NOI18N
        FormInput.add(ChkRAD);
        ChkRAD.setBounds(10, 680, 23, 23);

        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel51.setText("Hasil Radiologi");
        jLabel51.setName("jLabel51"); // NOI18N
        FormInput.add(jLabel51);
        jLabel51.setBounds(40, 680, 90, 23);

        ChkLainnya.setBorder(null);
        ChkLainnya.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkLainnya.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkLainnya.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkLainnya.setName("ChkLainnya"); // NOI18N
        FormInput.add(ChkLainnya);
        ChkLainnya.setBounds(10, 700, 23, 23);

        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel52.setText("Lainnya");
        jLabel52.setName("jLabel52"); // NOI18N
        FormInput.add(jLabel52);
        jLabel52.setBounds(40, 700, 40, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Transfer Pasien", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Pindah :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(68, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-07-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-07-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(197, 23));
        TCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TCariActionPerformed(evt);
            }
        });
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        PanelAccor.setBackground(new java.awt.Color(255, 255, 255));
        PanelAccor.setName("PanelAccor"); // NOI18N
        PanelAccor.setPreferredSize(new java.awt.Dimension(430, 43));
        PanelAccor.setLayout(new java.awt.BorderLayout(1, 1));

        ChkAccor.setBackground(new java.awt.Color(255, 250, 250));
        ChkAccor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setSelected(true);
        ChkAccor.setFocusable(false);
        ChkAccor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkAccor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkAccor.setName("ChkAccor"); // NOI18N
        ChkAccor.setPreferredSize(new java.awt.Dimension(15, 20));
        ChkAccor.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kiri.png"))); // NOI18N
        ChkAccor.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/kanan.png"))); // NOI18N
        ChkAccor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkAccorActionPerformed(evt);
            }
        });
        PanelAccor.add(ChkAccor, java.awt.BorderLayout.WEST);

        FormPhoto.setBackground(new java.awt.Color(255, 255, 255));
        FormPhoto.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1), " Bukti Pengambilan Persetujuan : ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        FormPhoto.setName("FormPhoto"); // NOI18N
        FormPhoto.setPreferredSize(new java.awt.Dimension(115, 73));
        FormPhoto.setLayout(new java.awt.BorderLayout());

        FormPass3.setBackground(new java.awt.Color(255, 255, 255));
        FormPass3.setBorder(null);
        FormPass3.setName("FormPass3"); // NOI18N
        FormPass3.setPreferredSize(new java.awt.Dimension(115, 40));

        btnAmbil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        btnAmbil.setMnemonic('U');
        btnAmbil.setText("Ambil");
        btnAmbil.setToolTipText("Alt+U");
        btnAmbil.setName("btnAmbil"); // NOI18N
        btnAmbil.setPreferredSize(new java.awt.Dimension(100, 30));
        btnAmbil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAmbilActionPerformed(evt);
            }
        });
        FormPass3.add(btnAmbil);

        BtnRefreshPhoto1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/refresh.png"))); // NOI18N
        BtnRefreshPhoto1.setMnemonic('U');
        BtnRefreshPhoto1.setText("Refresh");
        BtnRefreshPhoto1.setToolTipText("Alt+U");
        BtnRefreshPhoto1.setName("BtnRefreshPhoto1"); // NOI18N
        BtnRefreshPhoto1.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnRefreshPhoto1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto1ActionPerformed(evt);
            }
        });
        FormPass3.add(BtnRefreshPhoto1);

        BtnRefreshPhoto2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/item.png"))); // NOI18N
        BtnRefreshPhoto2.setMnemonic('U');
        BtnRefreshPhoto2.setToolTipText("Alt+U");
        BtnRefreshPhoto2.setLabel("Cetak");
        BtnRefreshPhoto2.setName("BtnRefreshPhoto2"); // NOI18N
        BtnRefreshPhoto2.setPreferredSize(new java.awt.Dimension(110, 30));
        BtnRefreshPhoto2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefreshPhoto2ActionPerformed(evt);
            }
        });
        FormPass3.add(BtnRefreshPhoto2);

        FormPhoto.add(FormPass3, java.awt.BorderLayout.PAGE_END);

        Scroll5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll5.setName("Scroll5"); // NOI18N
        Scroll5.setOpaque(true);
        Scroll5.setPreferredSize(new java.awt.Dimension(200, 200));

        LoadHTML2.setBorder(null);
        LoadHTML2.setName("LoadHTML2"); // NOI18N
        Scroll5.setViewportView(LoadHTML2);

        FormPhoto.add(Scroll5, java.awt.BorderLayout.CENTER);

        PanelAccor.add(FormPhoto, java.awt.BorderLayout.CENTER);

        internalFrame3.add(PanelAccor, java.awt.BorderLayout.EAST);

        TabRawat.addTab("Data Transfer Pasien", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(level.getSelectedItem().toString().equals("")){
            Valid.textKosong(level,"Level");
        }else if(AsalRuang.getText().trim().equals("")){
            Valid.textKosong(AsalRuang,"Asal Ruang");
        }else if(RuangSelanjutnya.getText().trim().equals("")){
            Valid.textKosong(RuangSelanjutnya,"Ruang Selanjutnya");
        }else if(AlasanTransfer.getText().trim().equals("")){
            Valid.textKosong(AlasanTransfer,"Alasan Transfer");
        }else if(NmPetugasMenyerahkan.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Petugas Yang Menyerahkan");
        }else{
            if(akses.getkode().equals("Admin Utama")){
                simpan();
            }else {
                simpan();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else {
                if(akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString())||akses.getkode().equals(tbObat.getValueAt(tbObat.getSelectedRow(),46).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Harus salah satu petugas sesuai user login..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }             
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(level.getSelectedItem().toString().equals("")){
            Valid.textKosong(level,"Level");
        }else if(AsalRuang.getText().trim().equals("")){
            Valid.textKosong(AsalRuang,"Asal Ruang");
        }else if(RuangSelanjutnya.getText().trim().equals("")){
            Valid.textKosong(RuangSelanjutnya,"Ruang Selanjutnya");
        }else if(AlasanTransfer.getText().trim().equals("")){
            Valid.textKosong(AlasanTransfer,"Alasan Transfer");
        }else if(NmPetugasMenyerahkan.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Petugas Yang Menyerahkan");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                    tampil();
                }else {
                    // CUSTOM SIAPA SAJA BOLEH  EDIT
                        ganti();
                        tampil();
                    // END CUSTOM
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
            BtnBatal.requestFocus();
        }else if(tabMode.getRowCount()!=0){
            try{
                if(TCari.getText().trim().equals("")){
                    ps=koneksi.prepareStatement(
                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,"+
                            "transfer_pasien_antar_ruang.tanggal_masuk,transfer_pasien_antar_ruang.tanggal_pindah,transfer_pasien_antar_ruang.asal_ruang,"+
                            "transfer_pasien_antar_ruang.ruang_selanjutnya,transfer_pasien_antar_ruang.diagnosa_sekunder,"+
                            "transfer_pasien_antar_ruang.indikasi_pindah_ruang,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang,"+
                            "transfer_pasien_antar_ruang.prosedur_yang_sudah_dilakukan,transfer_pasien_antar_ruang.obat_yang_telah_diberikan,"+
                            "transfer_pasien_antar_ruang.metode_pemindahan_pasien,transfer_pasien_antar_ruang.peralatan_yang_menyertai,"+
                            "transfer_pasien_antar_ruang.keterangan_peralatan_yang_menyertai,transfer_pasien_antar_ruang.pemeriksaan_penunjang_yang_dilakukan,"+
                            "transfer_pasien_antar_ruang.pasien_keluarga_menyetujui,transfer_pasien_antar_ruang.nama_menyetujui,transfer_pasien_antar_ruang.hubungan_menyetujui,"+
                            "transfer_pasien_antar_ruang.keluhan_utama_sebelum_transfer,transfer_pasien_antar_ruang.keadaan_umum_sebelum_transfer,"+
                            "transfer_pasien_antar_ruang.td_sebelum_transfer,transfer_pasien_antar_ruang.nadi_sebelum_transfer,transfer_pasien_antar_ruang.rr_sebelum_transfer,"+
                            "transfer_pasien_antar_ruang.suhu_sebelum_transfer,transfer_pasien_antar_ruang.keluhan_utama_sesudah_transfer,"+
                            "transfer_pasien_antar_ruang.keadaan_umum_sesudah_transfer,transfer_pasien_antar_ruang.td_sesudah_transfer,"+
                            "transfer_pasien_antar_ruang.nadi_sesudah_transfer,transfer_pasien_antar_ruang.rr_sesudah_transfer,transfer_pasien_antar_ruang.suhu_sesudah_transfer,"+
                            "transfer_pasien_antar_ruang.nip_menyerahkan,petugasmenyerahkan.nama as petugasmenyerahkan,transfer_pasien_antar_ruang.nip_menerima,"+
                            "petugasmenerima.nama as petugasmenerima "+
                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                            "inner join transfer_pasien_antar_ruang on reg_periksa.no_rawat=transfer_pasien_antar_ruang.no_rawat "+
                            "inner join petugas as petugasmenyerahkan on transfer_pasien_antar_ruang.nip_menyerahkan=petugasmenyerahkan.nip "+
                            "inner join petugas as petugasmenerima on transfer_pasien_antar_ruang.nip_menerima=petugasmenerima.nip where "+
                            "transfer_pasien_antar_ruang.tanggal_pindah between ? and ? order by transfer_pasien_antar_ruang.tanggal_pindah");
                }else{
                    ps=koneksi.prepareStatement(
                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,"+
                            "transfer_pasien_antar_ruang.tanggal_masuk,transfer_pasien_antar_ruang.tanggal_pindah,transfer_pasien_antar_ruang.asal_ruang,"+
                            "transfer_pasien_antar_ruang.ruang_selanjutnya,transfer_pasien_antar_ruang.diagnosa_sekunder,"+
                            "transfer_pasien_antar_ruang.indikasi_pindah_ruang,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang,"+
                            "transfer_pasien_antar_ruang.prosedur_yang_sudah_dilakukan,transfer_pasien_antar_ruang.obat_yang_telah_diberikan,"+
                            "transfer_pasien_antar_ruang.metode_pemindahan_pasien,transfer_pasien_antar_ruang.peralatan_yang_menyertai,"+
                            "transfer_pasien_antar_ruang.keterangan_peralatan_yang_menyertai,transfer_pasien_antar_ruang.pemeriksaan_penunjang_yang_dilakukan,"+
                            "transfer_pasien_antar_ruang.pasien_keluarga_menyetujui,transfer_pasien_antar_ruang.nama_menyetujui,transfer_pasien_antar_ruang.hubungan_menyetujui,"+
                            "transfer_pasien_antar_ruang.keluhan_utama_sebelum_transfer,transfer_pasien_antar_ruang.keadaan_umum_sebelum_transfer,"+
                            "transfer_pasien_antar_ruang.td_sebelum_transfer,transfer_pasien_antar_ruang.nadi_sebelum_transfer,transfer_pasien_antar_ruang.rr_sebelum_transfer,"+
                            "transfer_pasien_antar_ruang.suhu_sebelum_transfer,transfer_pasien_antar_ruang.keluhan_utama_sesudah_transfer,"+
                            "transfer_pasien_antar_ruang.keadaan_umum_sesudah_transfer,transfer_pasien_antar_ruang.td_sesudah_transfer,"+
                            "transfer_pasien_antar_ruang.nadi_sesudah_transfer,transfer_pasien_antar_ruang.rr_sesudah_transfer,transfer_pasien_antar_ruang.suhu_sesudah_transfer,"+
                            "transfer_pasien_antar_ruang.nip_menyerahkan,petugasmenyerahkan.nama as petugasmenyerahkan,transfer_pasien_antar_ruang.nip_menerima,"+
                            "petugasmenerima.nama as petugasmenerima "+
                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                            "inner join transfer_pasien_antar_ruang on reg_periksa.no_rawat=transfer_pasien_antar_ruang.no_rawat "+
                            "inner join petugas as petugasmenyerahkan on transfer_pasien_antar_ruang.nip_menyerahkan=petugasmenyerahkan.nip "+
                            "inner join petugas as petugasmenerima on transfer_pasien_antar_ruang.nip_menerima=petugasmenerima.nip where "+
                            "transfer_pasien_antar_ruang.tanggal_pindah between ? and ? and (reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
                            "transfer_pasien_antar_ruang.nip_menyerahkan like ? or petugasmenyerahkan.nama like ?) order by transfer_pasien_antar_ruang.tanggal_pindah");
                }

                try {
                    if(TCari.getText().trim().equals("")){
                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    }else{
                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                        ps.setString(3,"%"+TCari.getText()+"%");
                        ps.setString(4,"%"+TCari.getText()+"%");
                        ps.setString(5,"%"+TCari.getText()+"%");
                        ps.setString(6,"%"+TCari.getText()+"%");
                        ps.setString(7,"%"+TCari.getText()+"%");
                    }
                    rs=ps.executeQuery();
                    htmlContent = new StringBuilder();
                    htmlContent.append(                             
                        "<tr class='isi'>"+
                           "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>No.Rawat</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>No.RM</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Nama Pasien</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Tgl.Lahir</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>J.K.</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Tanggal Masuk</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Tanggal Pindah</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Indikasi Pindah</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Keterangan Indikasi Pindah</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Asal Ruang Rawat / Poliklinik</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Ruang Rawat Selanjutnya</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Metode Pemindahan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Diagnosa Utama</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Diagnosa Sekunder</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Prosedur Yang Sudah Dilakukan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Obat Yang Telah Diberikan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Pemeriksaan Penunjang Yang Sudah Dilakukan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Peralatan Yang Menyertai</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Keterangan Peralatan Menyertai</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Menyetujui Pemindahan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Nama Keluarga/Penanggung Jawab</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Hubungan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Keadaan Umum SbT</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>TD SbT</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Nadi SbT</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>RR SbT</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Suhu Sbt</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Keluhan Utama Sebelum Transfer</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Keadaan Umum StT</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>TD StT</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Nadi StT</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>RR StT</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Suhu Stt</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Keluhan Utama Setelah Transfer</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>NIP Menyerahkan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Petugas Yang Menyerahkan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>NIP Menerima</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center'><b>Petugas Yang Menerima</b></td>"+
                        "</tr>"
                    );
                    while(rs.next()){
                        htmlContent.append(
                            "<tr class='isi'>"+
                               "<td valign='top'>"+rs.getString("no_rawat")+"</td>"+
                               "<td valign='top'>"+rs.getString("no_rkm_medis")+"</td>"+
                               "<td valign='top'>"+rs.getString("nm_pasien")+"</td>"+
                               "<td valign='top'>"+rs.getString("tgl_lahir")+"</td>"+
                               "<td valign='top'>"+rs.getString("jk")+"</td>"+
                               "<td valign='top'>"+rs.getString("tanggal_masuk")+"</td>"+
                               "<td valign='top'>"+rs.getString("tanggal_pindah")+"</td>"+
                               "<td valign='top'>"+rs.getString("indikasi_pindah_ruang")+"</td>"+
                               "<td valign='top'>"+rs.getString("keterangan_indikasi_pindah_ruang")+"</td>"+
                               "<td valign='top'>"+rs.getString("asal_ruang")+"</td>"+
                               "<td valign='top'>"+rs.getString("ruang_selanjutnya")+"</td>"+
                               "<td valign='top'>"+rs.getString("metode_pemindahan_pasien")+"</td>"+
                               "<td valign='top'>"+rs.getString("diagnosa_utama")+"</td>"+
                               "<td valign='top'>"+rs.getString("diagnosa_sekunder")+"</td>"+
                               "<td valign='top'>"+rs.getString("prosedur_yang_sudah_dilakukan")+"</td>"+
                               "<td valign='top'>"+rs.getString("obat_yang_telah_diberikan")+"</td>"+
                               "<td valign='top'>"+rs.getString("pemeriksaan_penunjang_yang_dilakukan")+"</td>"+
                               "<td valign='top'>"+rs.getString("peralatan_yang_menyertai")+"</td>"+
                               "<td valign='top'>"+rs.getString("keterangan_peralatan_yang_menyertai")+"</td>"+
                               "<td valign='top'>"+rs.getString("pasien_keluarga_menyetujui")+"</td>"+
                               "<td valign='top'>"+rs.getString("nama_menyetujui")+"</td>"+
                               "<td valign='top'>"+rs.getString("hubungan_menyetujui")+"</td>"+
                               "<td valign='top'>"+rs.getString("keadaan_umum_sebelum_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("td_sebelum_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("nadi_sebelum_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("rr_sebelum_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("suhu_sebelum_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("keluhan_utama_sebelum_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("keadaan_umum_sesudah_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("td_sesudah_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("nadi_sesudah_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("rr_sesudah_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("suhu_sesudah_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("keluhan_utama_sesudah_transfer")+"</td>"+
                               "<td valign='top'>"+rs.getString("nip_menyerahkan")+"</td>"+
                               "<td valign='top'>"+rs.getString("petugasmenyerahkan")+"</td>"+
                               "<td valign='top'>"+rs.getString("nip_menerima")+"</td>"+
                               "<td valign='top'>"+rs.getString("petugasmenerima")+"</td>"+
                            "</tr>");
                    }
                    LoadHTML.setText(
                        "<html>"+
                          "<table width='4000' border='0' align='center' cellpadding='1px' cellspacing='0' class='tbl_form'>"+
                           htmlContent.toString()+
                          "</table>"+
                        "</html>"
                    );

                    File g = new File("file2.css");            
                    BufferedWriter bg = new BufferedWriter(new FileWriter(g));
                    bg.write(
                        ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                        ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                        ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                        ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                        ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                        ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                        ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                        ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                        ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
                    );
                    bg.close();

                    File f = new File("TransferPasienAntarRuang.html");            
                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));            
                    bw.write(LoadHTML.getText().replaceAll("<head>","<head>"+
                                "<link href=\"file2.css\" rel=\"stylesheet\" type=\"text/css\" />"+
                                "<table width='4000px' border='0' align='center' cellpadding='3px' cellspacing='0' class='tbl_form'>"+
                                    "<tr class='isi2'>"+
                                        "<td valign='top' align='center'>"+
                                            "<font size='4' face='Tahoma'>"+akses.getnamars()+"</font><br>"+
                                            akses.getalamatrs()+", "+akses.getkabupatenrs()+", "+akses.getpropinsirs()+"<br>"+
                                            akses.getkontakrs()+", E-mail : "+akses.getemailrs()+"<br><br>"+
                                            "<font size='2' face='Tahoma'>DATA TRANSFER PASIEN ANTAR RUANG<br><br></font>"+        
                                        "</td>"+
                                   "</tr>"+
                                "</table>")
                    );
                    bw.close();                         
                    Desktop.getDesktop().browse(f.toURI());
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }

            }catch(Exception e){
                System.out.println("Notifikasi : "+e);
            }
        }
        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                isPhoto();
                panggilPhoto();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
                getData();
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void MenyetujuiPemindahanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenyetujuiPemindahanKeyPressed
      
    }//GEN-LAST:event_MenyetujuiPemindahanKeyPressed

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{
            //Valid.pindah(evt,TCari,BtnDokter);
        }
    }//GEN-LAST:event_TNoRwKeyPressed

    private void TanggalMasukKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TanggalMasukKeyPressed
        Valid.pindah2(evt,TNoRw,TanggalPindah);
    }//GEN-LAST:event_TanggalMasukKeyPressed

    private void TanggalPindahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TanggalPindahKeyPressed
        Valid.pindah2(evt,TanggalMasuk,IndikasiPindah);
    }//GEN-LAST:event_TanggalPindahKeyPressed

    private void KeteranganIndikasiPindahRuangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeteranganIndikasiPindahRuangKeyPressed
        Valid.pindah(evt,IndikasiPindah,AsalRuang);
    }//GEN-LAST:event_KeteranganIndikasiPindahRuangKeyPressed

    private void RuangSelanjutnyaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RuangSelanjutnyaKeyPressed
        Valid.pindah(evt,AsalRuang,MetodePemindahan);
    }//GEN-LAST:event_RuangSelanjutnyaKeyPressed

    private void DiagnosaSekunderKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DiagnosaSekunderKeyPressed
        Valid.pindah2(evt,AlasanTransfer,ProsedurDilakukan);
    }//GEN-LAST:event_DiagnosaSekunderKeyPressed

    private void AlasanTransferKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AlasanTransferKeyPressed
        Valid.pindah(evt,MetodePemindahan,DiagnosaSekunder);
    }//GEN-LAST:event_AlasanTransferKeyPressed

    private void ProsedurDilakukanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ProsedurDilakukanKeyPressed
        Valid.pindah2(evt,DiagnosaSekunder,ObatYangDiberikan);
    }//GEN-LAST:event_ProsedurDilakukanKeyPressed

    private void ObatYangDiberikanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ObatYangDiberikanKeyPressed
        Valid.pindah2(evt,ProsedurDilakukan,PemeriksaanPenunjang);
    }//GEN-LAST:event_ObatYangDiberikanKeyPressed

    private void PemeriksaanPenunjangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PemeriksaanPenunjangKeyPressed
        Valid.pindah2(evt,ObatYangDiberikan,PeralatanMenyertai);
    }//GEN-LAST:event_PemeriksaanPenunjangKeyPressed

    private void IndikasiPindahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IndikasiPindahKeyPressed
        Valid.pindah(evt,TanggalPindah,KeteranganIndikasiPindahRuang);
    }//GEN-LAST:event_IndikasiPindahKeyPressed

    private void AsalRuangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AsalRuangKeyPressed
        Valid.pindah(evt,KeteranganIndikasiPindahRuang,RuangSelanjutnya);
    }//GEN-LAST:event_AsalRuangKeyPressed

    private void MetodePemindahanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MetodePemindahanKeyPressed
        Valid.pindah(evt,RuangSelanjutnya,AlasanTransfer);
    }//GEN-LAST:event_MetodePemindahanKeyPressed

    private void NamaMenyetujuiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NamaMenyetujuiKeyPressed
        Valid.pindah(evt,MenyetujuiPemindahan,HubunganMenyetujui);
    }//GEN-LAST:event_NamaMenyetujuiKeyPressed

    private void HubunganMenyetujuiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HubunganMenyetujuiKeyPressed
        Valid.pindah(evt,NamaMenyetujui,KeadaanUmumSebelumTransfer);
    }//GEN-LAST:event_HubunganMenyetujuiKeyPressed

    private void KeluhanUtamaSebelumTransferKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeluhanUtamaSebelumTransferKeyPressed
       
    }//GEN-LAST:event_KeluhanUtamaSebelumTransferKeyPressed

    private void KeadaanUmumSebelumTransferKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeadaanUmumSebelumTransferKeyPressed
        Valid.pindah(evt,HubunganMenyetujui,TDSebelumTransfer);
    }//GEN-LAST:event_KeadaanUmumSebelumTransferKeyPressed

    private void TDSebelumTransferKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TDSebelumTransferKeyPressed
        Valid.pindah(evt,KeadaanUmumSebelumTransfer,NadiSebelumTransfer);
    }//GEN-LAST:event_TDSebelumTransferKeyPressed

    private void NadiSebelumTransferKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NadiSebelumTransferKeyPressed
        Valid.pindah(evt,TDSebelumTransfer,RRSebelumTransfer);
    }//GEN-LAST:event_NadiSebelumTransferKeyPressed

    private void RRSebelumTransferKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RRSebelumTransferKeyPressed
        Valid.pindah(evt,NadiSebelumTransfer,SuhuSebelumTransfer);
    }//GEN-LAST:event_RRSebelumTransferKeyPressed

    private void SuhuSebelumTransferKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SuhuSebelumTransferKeyPressed
        Valid.pindah(evt,RRSebelumTransfer,KeluhanUtamaSebelumTransfer);
    }//GEN-LAST:event_SuhuSebelumTransferKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        pilihan=1;
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void ChkAccorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkAccorActionPerformed
        if(tbObat.getSelectedRow()!= -1){
            isPhoto();
            panggilPhoto();
        }else{
            ChkAccor.setSelected(false);
            JOptionPane.showMessageDialog(null,"Silahkan pilih No.Pernyataan..!!!");
        }
    }//GEN-LAST:event_ChkAccorActionPerformed

    private void btnAmbilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAmbilActionPerformed
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
            TCari.requestFocus();
        }else{
            if(tbObat.getSelectedRow()>-1){
                Sequel.queryu("delete from antripersetujuantransferantarruang");
                Sequel.queryu("insert into antripersetujuantransferantarruang values('"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"','"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+"')");
                Sequel.queryu("delete from bukti_persetujuan_transfer_pasien_antar_ruang where no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and tanggal_masuk='"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+"'");
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih No.Pernyataan terlebih dahulu..!!");
            }
        }
    }//GEN-LAST:event_btnAmbilActionPerformed

    private void BtnRefreshPhoto1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto1ActionPerformed
        if(tbObat.getSelectedRow()>-1){
            panggilPhoto();
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih No.Pernyataan terlebih dahulu..!!");
        }
    }//GEN-LAST:event_BtnRefreshPhoto1ActionPerformed

    private void BtnRefreshPhoto2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefreshPhoto2ActionPerformed
      if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),44).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),45).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),44).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()));
            finger_pj=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),36).toString());
            param.put("finger_pj","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\n Ditandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),47).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),46).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),50).toString()));
            
            Valid.MyReportqry("rptCetakTransferPasien.jasper","report","::[ Formulir Transfer Pasien Antar Ruang]::",
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.sttsumur,reg_periksa.umurdaftar,reg_periksa.tgl_registrasi,reg_periksa.jam_reg,CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,penjab.png_jawab, "+
                        "transfer_pasien_antar_ruang.tanggal_masuk,transfer_pasien_antar_ruang.tanggal_pindah,transfer_pasien_antar_ruang.asal_ruang,"+
                        "transfer_pasien_antar_ruang.ruang_selanjutnya,transfer_pasien_antar_ruang.diagnosa_sekunder,"+
                        "transfer_pasien_antar_ruang.indikasi_pindah_ruang,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang,"+
                        "transfer_pasien_antar_ruang.prosedur_yang_sudah_dilakukan,transfer_pasien_antar_ruang.obat_yang_telah_diberikan,"+
                        "transfer_pasien_antar_ruang.metode_pemindahan_pasien,transfer_pasien_antar_ruang.peralatan_yang_menyertai,"+
                        "transfer_pasien_antar_ruang.pemeriksaan_penunjang_yang_dilakukan,transfer_pasien_antar_ruang.alasan_transfer,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang, "+
                        "transfer_pasien_antar_ruang.pasien_keluarga_menyetujui,transfer_pasien_antar_ruang.nama_menyetujui,transfer_pasien_antar_ruang.hubungan_menyetujui,"+
                        "transfer_pasien_antar_ruang.keluhan_utama_sebelum_transfer,transfer_pasien_antar_ruang.keadaan_umum_sebelum_transfer,"+
                        "transfer_pasien_antar_ruang.td_sebelum_transfer,transfer_pasien_antar_ruang.nadi_sebelum_transfer,transfer_pasien_antar_ruang.rr_sebelum_transfer,"+
                        "transfer_pasien_antar_ruang.suhu_sebelum_transfer,transfer_pasien_antar_ruang.keluhan_utama_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.keadaan_umum_sesudah_transfer,transfer_pasien_antar_ruang.td_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.nadi_sesudah_transfer,transfer_pasien_antar_ruang.rr_sesudah_transfer,transfer_pasien_antar_ruang.suhu_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.nip_menyerahkan,petugasmenyerahkan.nama as petugasmenyerahkan,transfer_pasien_antar_ruang.nip_menerima,"+
                        "petugasmenerima.nama as petugasmenerima,transfer_pasien_antar_ruang.level,transfer_pasien_antar_ruang.rtl,ifnull(transfer_pasien_antar_ruang.tanggal_diterima,'0000-00-00') as tanggal_diterima,transfer_pasien_antar_ruang.waktu_diterima, "+
                        "transfer_pasien_antar_ruang.obat_yang_menyertai,transfer_pasien_antar_ruang.dokumen_RM_dikirim,transfer_pasien_antar_ruang.dokumen_LAB_dikirim,transfer_pasien_antar_ruang.dokumen_RAD_dikirim,transfer_pasien_antar_ruang.dokumen_lainnya_dikirim,transfer_pasien_antar_ruang.dokumen_lainnya_ket_dikirim, "+
                        "transfer_pasien_antar_ruang.dokumen_RM_diterima,transfer_pasien_antar_ruang.dokumen_LAB_diterima,transfer_pasien_antar_ruang.dokumen_RAD_diterima,transfer_pasien_antar_ruang.dokumen_lainnya_diterima,transfer_pasien_antar_ruang.dokumen_lainnya_ket_diterima, "+
                        "transfer_pasien_antar_ruang.S,transfer_pasien_antar_ruang.B,transfer_pasien_antar_ruang.A,transfer_pasien_antar_ruang.R "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join transfer_pasien_antar_ruang on reg_periksa.no_rawat=transfer_pasien_antar_ruang.no_rawat "+
                        "inner join petugas as petugasmenyerahkan on transfer_pasien_antar_ruang.nip_menyerahkan=petugasmenyerahkan.nip "+
                        "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                        "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                        "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                        "inner join penjab on penjab.kd_pj = reg_periksa.kd_pj " +
                        "inner join petugas as petugasmenerima on transfer_pasien_antar_ruang.nip_menerima=petugasmenerima.nip where "+
                        "transfer_pasien_antar_ruang.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and transfer_pasien_antar_ruang.tanggal_masuk='"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+"' and transfer_pasien_antar_ruang.tanggal_pindah='"+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"' ",param);
        }
    }//GEN-LAST:event_BtnRefreshPhoto2ActionPerformed

    private void levelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_levelKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_levelKeyPressed

    private void rtlKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rtlKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_rtlKeyPressed

    private void BtnSimpanVerifikasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanVerifikasiActionPerformed
        // START CUSTOM ANGGIT
        if(NoRawat.getText().trim().equals("")){
            Valid.textKosong(NoRawat,"Nomor Rawat");
        }else if(KdPetugasPJ.getText().trim().equals("")) {
            Valid.textKosong(NoRawat,"Petugas Penerima");
        }else{
            if(akses.getkode().equals("Admin Utama")){
            checkboxRM1 = "";
            checkboxRAD1 = "";
            checkboxLAB1 = "";
            checkboxLAINNYA1 = "";
             if(ChkRM1.isSelected()){
                checkboxRM1="Ya";
               }else{
                checkboxRM1="Tidak";
             }
             if(ChkRAD1.isSelected()){
                checkboxRAD1="Ya";
               }else{
                checkboxRAD1="Tidak";
             }
             if(ChkLAB1.isSelected()){
                checkboxLAB1="Ya";
               }else{
                checkboxLAB1="Tidak";
             }
             if(ChkLainnya1.isSelected()){
                checkboxLAINNYA1="Ya";
               }else{
                checkboxLAINNYA1="Tidak";
             }
                if(Sequel.queryu2tf("update transfer_pasien_antar_ruang set keluhan_utama_sesudah_transfer=?, keadaan_umum_sesudah_transfer=?, td_sesudah_transfer=?, nadi_sesudah_transfer=?,rr_sesudah_transfer=?,suhu_sesudah_transfer=?,nip_menerima=?, tanggal_diterima=?,waktu_diterima=?, dokumen_RM_diterima=?,dokumen_LAB_diterima=?,dokumen_RAD_diterima=?,dokumen_lainnya_diterima=?,dokumen_lainnya_ket_diterima=? where no_rawat=? and tanggal_masuk=? and tanggal_pindah=?",17,new String[]{
                        KeluhanUtamaSetelahTransfer1.getText(),
                        KeadaanUmumSetelahTransfer1.getSelectedItem().toString(),
                        TDSetelahTransfer1.getText(),
                        NadiSetelahTransfer1.getText(),
                        RRSetelahTransfer1.getText(),
                        SuhuSetelahTransfer1.getText(),
                        KdPetugasPJ.getText(),
                        Valid.SetTgl(DTPVerifikasi.getSelectedItem()+""),
                        Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                        checkboxRM1,
                        checkboxLAB1,
                        checkboxRAD1,
                        checkboxLAINNYA1,
                        ket_lainnya1.getText(),
                        NoRawat.getText(),
                        Valid.SetTgl(tanggalmasuk1.getSelectedItem()+"")+" "+tanggalmasuk1.getSelectedItem().toString().substring(11,19),
                        Valid.SetTgl(tanggalpindah1.getSelectedItem()+"")+" "+tanggalpindah1.getSelectedItem().toString().substring(11,19)
                      
                })==true){
                        JOptionPane.showMessageDialog(null,"Transfer Pasien "+ NmPasien.getText() +" berhasil di verifikasi");
                        DlgSP.dispose();
                        tampil();
                    }else{
                        JOptionPane.showMessageDialog(null,"Maaf, Verifikasi gagal disimpan");
                    }

            }else{
                if(KdPetugasPJ.getText().equals(akses.getkode())){
                    checkboxRM1 = "";
                    checkboxRAD1 = "";
                    checkboxLAB1 = "";
                    checkboxLAINNYA1 = "";
                     if(ChkRM1.isSelected()){
                        checkboxRM1="Ya";
                       }else{
                        checkboxRM1="Tidak";
                     }
                     if(ChkRAD1.isSelected()){
                        checkboxRAD1="Ya";
                       }else{
                        checkboxRAD1="Tidak";
                     }
                     if(ChkLAB1.isSelected()){
                        checkboxLAB1="Ya";
                       }else{
                        checkboxLAB1="Tidak";
                     }
                     if(ChkLainnya1.isSelected()){
                        checkboxLAINNYA1="Ya";
                       }else{
                        checkboxLAINNYA1="Tidak";
                     }
                    if(Sequel.queryu2tf("update transfer_pasien_antar_ruang set keluhan_utama_sesudah_transfer=?, keadaan_umum_sesudah_transfer=?, td_sesudah_transfer=?, nadi_sesudah_transfer=?,rr_sesudah_transfer=?,suhu_sesudah_transfer=?,nip_menerima=?, tanggal_diterima=?,waktu_diterima=?, dokumen_RM_diterima=?,dokumen_LAB_diterima=?,dokumen_RAD_diterima=?,dokumen_lainnya_diterima=?,dokumen_lainnya_ket_diterima=? where no_rawat=? and tanggal_masuk=? and tanggal_pindah=?",17,new String[]{
                        KeluhanUtamaSetelahTransfer1.getText(),
                        KeadaanUmumSetelahTransfer1.getSelectedItem().toString(),
                        TDSetelahTransfer1.getText(),
                        NadiSetelahTransfer1.getText(),
                        RRSetelahTransfer1.getText(),
                        SuhuSetelahTransfer1.getText(),
                        KdPetugasPJ.getText(),
                        Valid.SetTgl(DTPVerifikasi.getSelectedItem()+""),
                        Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                        checkboxRM1,
                        checkboxLAB1,
                        checkboxRAD1,
                        checkboxLAINNYA1,
                        ket_lainnya1.getText(),
                        NoRawat.getText(),
                        Valid.SetTgl(tanggalmasuk1.getSelectedItem()+"")+" "+tanggalmasuk1.getSelectedItem().toString().substring(11,19),
                        Valid.SetTgl(tanggalpindah1.getSelectedItem()+"")+" "+tanggalpindah1.getSelectedItem().toString().substring(11,19)
                      
                    })==true){
                        JOptionPane.showMessageDialog(null,"Transfer Pasien "+ NmPasien.getText() +" berhasil di verifikasi");
                        DlgSP.dispose();
                        tampil();
                    }else{
                        JOptionPane.showMessageDialog(null,"Maaf, Verifikasi gagal disimpan");
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Silahkan isi Petugas Penerima Transfer Pasien");
                }
            }

        }
        // END CUSTOM ANGGIT
    }//GEN-LAST:event_BtnSimpanVerifikasiActionPerformed

    private void BtnKeluarSPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarSPActionPerformed
        // TODO add your handling code here:
        DlgSP.dispose();
    }//GEN-LAST:event_BtnKeluarSPActionPerformed

    private void BtnCetakVerifikasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCetakVerifikasiActionPerformed
        if(NoRawat.getText().trim().equals("")){
          Valid.textKosong(NoRawat,"Silahkan Pilih pasien terlebih dahulu");
        } else {
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),44).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),45).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),44).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()));
            finger_pj=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),36).toString());
            param.put("finger_pj","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\n Ditandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),47).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),46).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),50).toString()));
            
            Valid.MyReportqry("rptCetakTransferPasien.jasper","report","::[ Formulir Transfer Pasien Antar Ruang]::",
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.sttsumur,reg_periksa.umurdaftar,reg_periksa.tgl_registrasi,reg_periksa.jam_reg,CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat,penjab.png_jawab, "+
                        "transfer_pasien_antar_ruang.tanggal_masuk,transfer_pasien_antar_ruang.tanggal_pindah,transfer_pasien_antar_ruang.asal_ruang,"+
                        "transfer_pasien_antar_ruang.ruang_selanjutnya,transfer_pasien_antar_ruang.diagnosa_sekunder,"+
                        "transfer_pasien_antar_ruang.indikasi_pindah_ruang,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang,"+
                        "transfer_pasien_antar_ruang.prosedur_yang_sudah_dilakukan,transfer_pasien_antar_ruang.obat_yang_telah_diberikan,"+
                        "transfer_pasien_antar_ruang.metode_pemindahan_pasien,transfer_pasien_antar_ruang.peralatan_yang_menyertai,"+
                        "transfer_pasien_antar_ruang.pemeriksaan_penunjang_yang_dilakukan,transfer_pasien_antar_ruang.alasan_transfer,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang, "+
                        "transfer_pasien_antar_ruang.pasien_keluarga_menyetujui,transfer_pasien_antar_ruang.nama_menyetujui,transfer_pasien_antar_ruang.hubungan_menyetujui,"+
                        "transfer_pasien_antar_ruang.keluhan_utama_sebelum_transfer,transfer_pasien_antar_ruang.keadaan_umum_sebelum_transfer,"+
                        "transfer_pasien_antar_ruang.td_sebelum_transfer,transfer_pasien_antar_ruang.nadi_sebelum_transfer,transfer_pasien_antar_ruang.rr_sebelum_transfer,"+
                        "transfer_pasien_antar_ruang.suhu_sebelum_transfer,transfer_pasien_antar_ruang.keluhan_utama_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.keadaan_umum_sesudah_transfer,transfer_pasien_antar_ruang.td_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.nadi_sesudah_transfer,transfer_pasien_antar_ruang.rr_sesudah_transfer,transfer_pasien_antar_ruang.suhu_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.nip_menyerahkan,petugasmenyerahkan.nama as petugasmenyerahkan,transfer_pasien_antar_ruang.nip_menerima,"+
                        "petugasmenerima.nama as petugasmenerima,transfer_pasien_antar_ruang.level,transfer_pasien_antar_ruang.rtl,ifnull(transfer_pasien_antar_ruang.tanggal_diterima,'0000-00-00') as tanggal_diterima,transfer_pasien_antar_ruang.waktu_diterima, "+
                        "transfer_pasien_antar_ruang.obat_yang_menyertai,transfer_pasien_antar_ruang.dokumen_RM_dikirim,transfer_pasien_antar_ruang.dokumen_LAB_dikirim,transfer_pasien_antar_ruang.dokumen_RAD_dikirim,transfer_pasien_antar_ruang.dokumen_lainnya_dikirim,transfer_pasien_antar_ruang.dokumen_lainnya_ket_dikirim, "+
                        "transfer_pasien_antar_ruang.dokumen_RM_diterima,transfer_pasien_antar_ruang.dokumen_LAB_diterima,transfer_pasien_antar_ruang.dokumen_RAD_diterima,transfer_pasien_antar_ruang.dokumen_lainnya_diterima,transfer_pasien_antar_ruang.dokumen_lainnya_ket_diterima, "+
                        "transfer_pasien_antar_ruang.S,transfer_pasien_antar_ruang.B,transfer_pasien_antar_ruang.A,transfer_pasien_antar_ruang.R "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join transfer_pasien_antar_ruang on reg_periksa.no_rawat=transfer_pasien_antar_ruang.no_rawat "+
                        "inner join petugas as petugasmenyerahkan on transfer_pasien_antar_ruang.nip_menyerahkan=petugasmenyerahkan.nip "+
                        "inner join kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                        "inner join kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                        "inner join kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                        "inner join penjab on penjab.kd_pj = reg_periksa.kd_pj " +
                        "inner join petugas as petugasmenerima on transfer_pasien_antar_ruang.nip_menerima=petugasmenerima.nip where "+
                        "transfer_pasien_antar_ruang.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and transfer_pasien_antar_ruang.tanggal_masuk='"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+"' and transfer_pasien_antar_ruang.tanggal_pindah='"+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"' ",param);
        }
    }//GEN-LAST:event_BtnCetakVerifikasiActionPerformed

    private void Jam1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1KeyPressed

    private void Menit1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit1KeyPressed

    private void Detik1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik1KeyPressed

    private void MnSerahTerimaTransferPasienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnSerahTerimaTransferPasienActionPerformed
         // TODO add your handling code here:
        if(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString().trim().equals("")){
            Valid.textKosong(TCari,"Nomor Rawat");
        }else{
            DlgSP.setSize(800,570);
            DlgSP.setLocationRelativeTo(internalFrame1);
            DlgSP.setVisible(true);
            NoRawat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            NoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            NmPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            dari.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            ke.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            level1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),48).toString());
            rtl1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),49).toString());
            Valid.SetTgl2(tanggalmasuk1,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            Valid.SetTgl2(tanggalpindah1,tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            checkboxRM1 = "";
            checkboxRAD1 = "";
            checkboxLAB1 = "";
            checkboxLAINNYA1 = "";
             if(ChkRM1.isSelected()){
                checkboxRM1="Ya";
               }else{
                checkboxRM1="Tidak";
             }
             if(ChkRAD1.isSelected()){
                checkboxRAD1="Ya";
               }else{
                checkboxRAD1="Tidak";
             }
             if(ChkLAB1.isSelected()){
                checkboxLAB1="Ya";
               }else{
                checkboxLAB1="Tidak";
             }
             if(ChkLainnya1.isSelected()){
                checkboxLAINNYA1="Ya";
               }else{
                checkboxLAINNYA1="Tidak";
             }
             
            if(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString().equals("Ya")){
                ChkRM1.setSelected(true);
            }else{
                ChkRM1.setSelected(false);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString().equals("Ya")){
                ChkLAB1.setSelected(true);
            }else{
                ChkLAB1.setSelected(false);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString().equals("Ya")){
                ChkRAD1.setSelected(true);
            }else{
                ChkRAD1.setSelected(false);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString().equals("Ya")){
                ChkLainnya1.setSelected(true);
            }else{
                ChkLainnya1.setSelected(false);
            }
            ket_lainnya1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
        } 
    }//GEN-LAST:event_MnSerahTerimaTransferPasienActionPerformed

    private void level1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_level1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_level1KeyPressed

    private void Jam1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jam1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1ActionPerformed

    private void tanggalmasuk1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tanggalmasuk1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tanggalmasuk1KeyPressed

    private void tanggalpindah1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tanggalpindah1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tanggalpindah1KeyPressed

    private void TCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TCariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TCariActionPerformed

    private void rtl1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rtl1KeyPressed
        
    }//GEN-LAST:event_rtl1KeyPressed

    private void BtnMenerima1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMenerima1ActionPerformed
        pilihan=3;
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);        // TODO add your handling code here:
    }//GEN-LAST:event_BtnMenerima1ActionPerformed

    private void BtnMenerima1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnMenerima1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnMenerima1KeyPressed

    private void KeadaanUmumSetelahTransfer1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeadaanUmumSetelahTransfer1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeadaanUmumSetelahTransfer1KeyPressed

    private void TDSetelahTransfer1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TDSetelahTransfer1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TDSetelahTransfer1KeyPressed

    private void NadiSetelahTransfer1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NadiSetelahTransfer1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_NadiSetelahTransfer1KeyPressed

    private void RRSetelahTransfer1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RRSetelahTransfer1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RRSetelahTransfer1KeyPressed

    private void SuhuSetelahTransfer1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SuhuSetelahTransfer1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_SuhuSetelahTransfer1KeyPressed

    private void KeluhanUtamaSetelahTransfer1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeluhanUtamaSetelahTransfer1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeluhanUtamaSetelahTransfer1KeyPressed

    private void PeralatanMenyertaiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PeralatanMenyertaiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PeralatanMenyertaiKeyPressed

    private void ObatMenyertaiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ObatMenyertaiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ObatMenyertaiKeyPressed

    private void AKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_AKeyPressed

    private void SKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_SKeyPressed

    private void BKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BKeyPressed

    private void RKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RKeyPressed

    private void ket_lainnyaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ket_lainnyaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ket_lainnyaKeyPressed

    private void ket_lainnya1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ket_lainnya1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ket_lainnya1KeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMTransferPasienAntarRuang dialog = new RMTransferPasienAntarRuang(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.TextArea A;
    private widget.TextBox AlasanTransfer;
    private widget.TextBox AsalRuang;
    private widget.TextArea B;
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnCetakVerifikasi;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnKeluarSP;
    private widget.Button BtnMenerima1;
    private widget.Button BtnPrint;
    private widget.Button BtnRefreshPhoto1;
    private widget.Button BtnRefreshPhoto2;
    private widget.Button BtnSimpan;
    private widget.Button BtnSimpanVerifikasi;
    private widget.CekBox ChkAccor;
    private widget.CekBox ChkLAB;
    private widget.CekBox ChkLAB1;
    private widget.CekBox ChkLainnya;
    private widget.CekBox ChkLainnya1;
    private widget.CekBox ChkRAD;
    private widget.CekBox ChkRAD1;
    private widget.CekBox ChkRM;
    private widget.CekBox ChkRM1;
    private widget.CekBox ChkTransfer;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.Tanggal DTPVerifikasi;
    private widget.ComboBox Detik1;
    private widget.TextArea DiagnosaSekunder;
    private javax.swing.JDialog DlgSP;
    private widget.PanelBiasa FormInput;
    private widget.PanelBiasa FormPass3;
    private widget.PanelBiasa FormPhoto;
    private widget.ComboBox HubunganMenyetujui;
    private widget.ComboBox IndikasiPindah;
    private widget.ComboBox Jam1;
    private widget.TextBox Jk;
    private widget.TextBox KdPetugasMenyerahkan;
    private widget.TextBox KdPetugasPJ;
    private widget.ComboBox KeadaanUmumSebelumTransfer;
    private widget.ComboBox KeadaanUmumSetelahTransfer1;
    private widget.TextArea KeluhanUtamaSebelumTransfer;
    private widget.TextArea KeluhanUtamaSetelahTransfer1;
    private widget.TextBox KeteranganIndikasiPindahRuang;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private widget.editorpane LoadHTML2;
    private widget.ComboBox Menit1;
    private widget.ComboBox MenyetujuiPemindahan;
    private widget.ComboBox MetodePemindahan;
    private javax.swing.JMenuItem MnSerahTerimaTransferPasien;
    private widget.TextBox NadiSebelumTransfer;
    private widget.TextBox NadiSetelahTransfer1;
    private widget.TextBox NamaMenyetujui;
    private widget.TextBox NmPasien;
    private widget.TextBox NmPetugasMenyerahkan;
    private widget.TextBox NmPetugasPJ;
    private widget.TextBox NoRM;
    private widget.TextBox NoRawat;
    private widget.TextArea ObatMenyertai;
    private widget.TextArea ObatYangDiberikan;
    private widget.PanelBiasa PanelAccor;
    private widget.TextArea PemeriksaanPenunjang;
    private widget.TextArea PeralatanMenyertai;
    private widget.TextArea ProsedurDilakukan;
    private widget.TextArea R;
    private widget.TextBox RRSebelumTransfer;
    private widget.TextBox RRSetelahTransfer1;
    private widget.TextBox RuangSelanjutnya;
    private widget.TextArea S;
    private widget.ScrollPane Scroll;
    private widget.ScrollPane Scroll5;
    private widget.TextBox SuhuSebelumTransfer;
    private widget.TextBox SuhuSetelahTransfer1;
    private widget.TextBox TCari;
    private widget.TextBox TDSebelumTransfer;
    private widget.TextBox TDSetelahTransfer1;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal TanggalMasuk;
    private widget.Tanggal TanggalPindah;
    private widget.TextBox TglLahir;
    private widget.Button btnAmbil;
    private widget.TextBox dari;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.InternalFrame internalFrame7;
    private widget.Label jLabel10;
    private widget.Label jLabel11;
    private widget.Label jLabel14;
    private widget.Label jLabel15;
    private widget.Label jLabel16;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel48;
    private widget.Label jLabel49;
    private widget.Label jLabel50;
    private widget.Label jLabel51;
    private widget.Label jLabel52;
    private widget.Label jLabel53;
    private widget.Label jLabel54;
    private widget.Label jLabel55;
    private widget.Label jLabel56;
    private widget.Label jLabel57;
    private widget.Label jLabel58;
    private widget.Label jLabel59;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel61;
    private widget.Label jLabel62;
    private widget.Label jLabel63;
    private widget.Label jLabel64;
    private widget.Label jLabel65;
    private widget.Label jLabel66;
    private widget.Label jLabel67;
    private widget.Label jLabel68;
    private widget.Label jLabel69;
    private widget.Label jLabel7;
    private widget.Label jLabel70;
    private widget.Label jLabel71;
    private widget.Label jLabel72;
    private widget.Label jLabel73;
    private widget.Label jLabel74;
    private widget.Label jLabel75;
    private widget.Label jLabel76;
    private widget.Label jLabel77;
    private widget.Label jLabel78;
    private widget.Label jLabel79;
    private widget.Label jLabel8;
    private widget.Label jLabel80;
    private widget.Label jLabel81;
    private widget.Label jLabel82;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private widget.TextBox ke;
    private widget.TextBox ket_lainnya;
    private widget.TextBox ket_lainnya1;
    private widget.Label label11;
    private widget.Label label12;
    private widget.Label label14;
    private widget.Label label16;
    private widget.ComboBox level;
    private widget.ComboBox level1;
    private widget.PanelBiasa panelBiasa6;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.TextArea rtl;
    private widget.TextArea rtl1;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane1;
    private widget.ScrollPane scrollPane10;
    private widget.ScrollPane scrollPane11;
    private widget.ScrollPane scrollPane12;
    private widget.ScrollPane scrollPane13;
    private widget.ScrollPane scrollPane14;
    private widget.ScrollPane scrollPane2;
    private widget.ScrollPane scrollPane3;
    private widget.ScrollPane scrollPane4;
    private widget.ScrollPane scrollPane5;
    private widget.ScrollPane scrollPane6;
    private widget.ScrollPane scrollPane7;
    private widget.ScrollPane scrollPane8;
    private widget.ScrollPane scrollPane9;
    private widget.Tanggal tanggalmasuk1;
    private widget.Tanggal tanggalpindah1;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().trim().equals("")){
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,"+
                        "transfer_pasien_antar_ruang.tanggal_masuk,transfer_pasien_antar_ruang.tanggal_pindah,transfer_pasien_antar_ruang.asal_ruang,"+
                        "transfer_pasien_antar_ruang.ruang_selanjutnya,transfer_pasien_antar_ruang.diagnosa_sekunder,"+
                        "transfer_pasien_antar_ruang.indikasi_pindah_ruang,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang,"+
                        "transfer_pasien_antar_ruang.prosedur_yang_sudah_dilakukan,transfer_pasien_antar_ruang.obat_yang_telah_diberikan,"+
                        "transfer_pasien_antar_ruang.metode_pemindahan_pasien,transfer_pasien_antar_ruang.peralatan_yang_menyertai,"+
                        "transfer_pasien_antar_ruang.pemeriksaan_penunjang_yang_dilakukan,transfer_pasien_antar_ruang.alasan_transfer,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang, "+
                        "transfer_pasien_antar_ruang.pasien_keluarga_menyetujui,transfer_pasien_antar_ruang.nama_menyetujui,transfer_pasien_antar_ruang.hubungan_menyetujui,"+
                        "transfer_pasien_antar_ruang.keluhan_utama_sebelum_transfer,transfer_pasien_antar_ruang.keadaan_umum_sebelum_transfer,"+
                        "transfer_pasien_antar_ruang.td_sebelum_transfer,transfer_pasien_antar_ruang.nadi_sebelum_transfer,transfer_pasien_antar_ruang.rr_sebelum_transfer,"+
                        "transfer_pasien_antar_ruang.suhu_sebelum_transfer,transfer_pasien_antar_ruang.keluhan_utama_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.keadaan_umum_sesudah_transfer,transfer_pasien_antar_ruang.td_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.nadi_sesudah_transfer,transfer_pasien_antar_ruang.rr_sesudah_transfer,transfer_pasien_antar_ruang.suhu_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.nip_menyerahkan,petugasmenyerahkan.nama as petugasmenyerahkan,transfer_pasien_antar_ruang.nip_menerima,"+
                        "petugasmenerima.nama as petugasmenerima,transfer_pasien_antar_ruang.level,transfer_pasien_antar_ruang.rtl,ifnull(transfer_pasien_antar_ruang.tanggal_diterima,'0000-00-00') as tanggal_diterima,transfer_pasien_antar_ruang.waktu_diterima, "+
                        "transfer_pasien_antar_ruang.obat_yang_menyertai,transfer_pasien_antar_ruang.dokumen_RM_dikirim,transfer_pasien_antar_ruang.dokumen_LAB_dikirim,transfer_pasien_antar_ruang.dokumen_RAD_dikirim,transfer_pasien_antar_ruang.dokumen_lainnya_dikirim,transfer_pasien_antar_ruang.dokumen_lainnya_ket_dikirim, "+
                        "transfer_pasien_antar_ruang.dokumen_RM_diterima,transfer_pasien_antar_ruang.dokumen_LAB_diterima,transfer_pasien_antar_ruang.dokumen_RAD_diterima,transfer_pasien_antar_ruang.dokumen_lainnya_diterima,transfer_pasien_antar_ruang.dokumen_lainnya_ket_diterima, "+
                        "transfer_pasien_antar_ruang.S,transfer_pasien_antar_ruang.B,transfer_pasien_antar_ruang.A,transfer_pasien_antar_ruang.R "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join transfer_pasien_antar_ruang on reg_periksa.no_rawat=transfer_pasien_antar_ruang.no_rawat "+
                        "inner join petugas as petugasmenyerahkan on transfer_pasien_antar_ruang.nip_menyerahkan=petugasmenyerahkan.nip "+
                        "inner join petugas as petugasmenerima on transfer_pasien_antar_ruang.nip_menerima=petugasmenerima.nip where "+
                        "transfer_pasien_antar_ruang.tanggal_pindah between ? and ? order by transfer_pasien_antar_ruang.tanggal_pindah");
            }else{
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,"+
                        "transfer_pasien_antar_ruang.tanggal_masuk,transfer_pasien_antar_ruang.tanggal_pindah,transfer_pasien_antar_ruang.asal_ruang,"+
                        "transfer_pasien_antar_ruang.ruang_selanjutnya,transfer_pasien_antar_ruang.diagnosa_sekunder,"+
                        "transfer_pasien_antar_ruang.indikasi_pindah_ruang,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang,"+
                        "transfer_pasien_antar_ruang.prosedur_yang_sudah_dilakukan,transfer_pasien_antar_ruang.obat_yang_telah_diberikan,"+
                        "transfer_pasien_antar_ruang.metode_pemindahan_pasien,transfer_pasien_antar_ruang.peralatan_yang_menyertai,"+
                        "transfer_pasien_antar_ruang.pemeriksaan_penunjang_yang_dilakukan,transfer_pasien_antar_ruang.alasan_transfer,transfer_pasien_antar_ruang.keterangan_indikasi_pindah_ruang, "+
                        "transfer_pasien_antar_ruang.pasien_keluarga_menyetujui,transfer_pasien_antar_ruang.nama_menyetujui,transfer_pasien_antar_ruang.hubungan_menyetujui,"+
                        "transfer_pasien_antar_ruang.keluhan_utama_sebelum_transfer,transfer_pasien_antar_ruang.keadaan_umum_sebelum_transfer,"+
                        "transfer_pasien_antar_ruang.td_sebelum_transfer,transfer_pasien_antar_ruang.nadi_sebelum_transfer,transfer_pasien_antar_ruang.rr_sebelum_transfer,"+
                        "transfer_pasien_antar_ruang.suhu_sebelum_transfer,transfer_pasien_antar_ruang.keluhan_utama_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.keadaan_umum_sesudah_transfer,transfer_pasien_antar_ruang.td_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.nadi_sesudah_transfer,transfer_pasien_antar_ruang.rr_sesudah_transfer,transfer_pasien_antar_ruang.suhu_sesudah_transfer,"+
                        "transfer_pasien_antar_ruang.nip_menyerahkan,petugasmenyerahkan.nama as petugasmenyerahkan,transfer_pasien_antar_ruang.nip_menerima,"+
                        "petugasmenerima.nama as petugasmenerima,transfer_pasien_antar_ruang.level,transfer_pasien_antar_ruang.rtl,ifnull(transfer_pasien_antar_ruang.tanggal_diterima,'0000-00-00') as tanggal_diterima,transfer_pasien_antar_ruang.waktu_diterima, "+
                        "transfer_pasien_antar_ruang.obat_yang_menyertai,transfer_pasien_antar_ruang.dokumen_RM_dikirim,transfer_pasien_antar_ruang.dokumen_LAB_dikirim,transfer_pasien_antar_ruang.dokumen_RAD_dikirim,transfer_pasien_antar_ruang.dokumen_lainnya_dikirim,transfer_pasien_antar_ruang.dokumen_lainnya_ket_dikirim, "+
                        "transfer_pasien_antar_ruang.dokumen_RM_diterima,transfer_pasien_antar_ruang.dokumen_LAB_diterima,transfer_pasien_antar_ruang.dokumen_RAD_diterima,transfer_pasien_antar_ruang.dokumen_lainnya_diterima,transfer_pasien_antar_ruang.dokumen_lainnya_ket_diterima, "+
                        "transfer_pasien_antar_ruang.S,transfer_pasien_antar_ruang.B,transfer_pasien_antar_ruang.A,transfer_pasien_antar_ruang.R "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join transfer_pasien_antar_ruang on reg_periksa.no_rawat=transfer_pasien_antar_ruang.no_rawat "+
                        "inner join petugas as petugasmenyerahkan on transfer_pasien_antar_ruang.nip_menyerahkan=petugasmenyerahkan.nip "+
                        "inner join petugas as petugasmenerima on transfer_pasien_antar_ruang.nip_menerima=petugasmenerima.nip where "+
                        "transfer_pasien_antar_ruang.tanggal_pindah between ? and ? and (reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
                        "transfer_pasien_antar_ruang.nip_menyerahkan like ? or petugasmenyerahkan.nama like ?) order by transfer_pasien_antar_ruang.tanggal_pindah");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("tgl_lahir"),rs.getString("jk"),rs.getString("tanggal_masuk"),
                        rs.getString("tanggal_pindah"),rs.getString("asal_ruang"),rs.getString("ruang_selanjutnya"),rs.getString("alasan_transfer"),
                        rs.getString("diagnosa_sekunder"),rs.getString("indikasi_pindah_ruang"),rs.getString("keterangan_indikasi_pindah_ruang"),rs.getString("prosedur_yang_sudah_dilakukan"),rs.getString("obat_yang_telah_diberikan"),rs.getString("metode_pemindahan_pasien"),
                        rs.getString("peralatan_yang_menyertai"),rs.getString("obat_yang_menyertai"),rs.getString("dokumen_RM_dikirim"),rs.getString("dokumen_LAB_dikirim"),rs.getString("dokumen_RAD_dikirim"),rs.getString("dokumen_lainnya_dikirim"),rs.getString("dokumen_lainnya_ket_dikirim"),
                        rs.getString("dokumen_RM_diterima"),rs.getString("dokumen_LAB_diterima"),rs.getString("dokumen_RAD_diterima"),rs.getString("dokumen_lainnya_diterima"),rs.getString("dokumen_lainnya_ket_diterima"),
                        rs.getString("pemeriksaan_penunjang_yang_dilakukan"),rs.getString("pasien_keluarga_menyetujui"),
                        rs.getString("nama_menyetujui"),rs.getString("hubungan_menyetujui"),
                        rs.getString("keluhan_utama_sebelum_transfer"),rs.getString("keadaan_umum_sebelum_transfer"),rs.getString("td_sebelum_transfer"),rs.getString("nadi_sebelum_transfer"),rs.getString("rr_sebelum_transfer"),rs.getString("suhu_sebelum_transfer"),
                        rs.getString("keluhan_utama_sesudah_transfer"),rs.getString("keadaan_umum_sesudah_transfer"),rs.getString("td_sesudah_transfer"),rs.getString("nadi_sesudah_transfer"),rs.getString("rr_sesudah_transfer"),rs.getString("suhu_sesudah_transfer"),
                        rs.getString("nip_menyerahkan"),rs.getString("petugasmenyerahkan"),rs.getString("nip_menerima"),rs.getString("petugasmenerima"),rs.getString("level"),rs.getString("rtl"),rs.getString("tanggal_diterima"),rs.getString("waktu_diterima"),
                        rs.getString("S"),rs.getString("B"),rs.getString("A"),rs.getString("R")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TanggalMasuk.setDate(new Date());
        TanggalPindah.setDate(new Date());
        IndikasiPindah.setSelectedIndex(0);
        KeteranganIndikasiPindahRuang.setText("");
        AsalRuang.setText("");
        RuangSelanjutnya.setText("");
        MetodePemindahan.setSelectedIndex(0);
        AlasanTransfer.setText("");
        DiagnosaSekunder.setText("");
        ProsedurDilakukan.setText("");
        ObatYangDiberikan.setText("");
        PemeriksaanPenunjang.setText("");
        PeralatanMenyertai.setText("");
        ObatMenyertai.setText("");
        MenyetujuiPemindahan.setSelectedIndex(0);
        NamaMenyetujui.setText("");
        HubunganMenyetujui.setSelectedIndex(0);
        KeadaanUmumSebelumTransfer.setSelectedIndex(0);
        TDSebelumTransfer.setText("");
        NadiSebelumTransfer.setText("");
        RRSebelumTransfer.setText("");
        SuhuSebelumTransfer.setText("");
        KeluhanUtamaSebelumTransfer.setText("");
        TabRawat.setSelectedIndex(0);
        level.setSelectedIndex(0);
        rtl.setText("");
        IndikasiPindah.requestFocus();
        S.setText("");
        B.setText("");
        A.setText("");
        R.setText("");
        ket_lainnya.setText("");
        PeralatanMenyertai.setText("");
        ObatMenyertai.setText("");
        ChkRM.setSelected(false);
        ChkRAD.setSelected(false);
        ChkLAB.setSelected(false);
        ChkLainnya.setSelected(false);
        
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            AsalRuang.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            RuangSelanjutnya.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString()); 
            AlasanTransfer.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString()); 
            DiagnosaSekunder.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            IndikasiPindah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString()); 
            KeteranganIndikasiPindahRuang.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            ProsedurDilakukan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString()); 
            ObatYangDiberikan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString()); 
            MetodePemindahan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            PemeriksaanPenunjang.setText(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString()); 
            PeralatanMenyertai.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString()); 
            ObatMenyertai.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString()); 
            if(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString().equals("Ya")){
                ChkRM.setSelected(true);
            }else{
                ChkRM.setSelected(false);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString().equals("Ya")){
                ChkLAB.setSelected(true);
            }else{
                ChkLAB.setSelected(false);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString().equals("Ya")){
                ChkRAD.setSelected(true);
            }else{
                ChkRAD.setSelected(false);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString().equals("Ya")){
                ChkLainnya.setSelected(true);
            }else{
                ChkLainnya.setSelected(false);
            }
            ket_lainnya.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            MenyetujuiPemindahan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            NamaMenyetujui.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());  
            HubunganMenyetujui.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            KeluhanUtamaSebelumTransfer.setText(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString()); 
            KeadaanUmumSebelumTransfer.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString());
            TDSebelumTransfer.setText(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString()); 
            NadiSebelumTransfer.setText(tbObat.getValueAt(tbObat.getSelectedRow(),35).toString()); 
            RRSebelumTransfer.setText(tbObat.getValueAt(tbObat.getSelectedRow(),36).toString()); 
            SuhuSebelumTransfer.setText(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString()); 
            KdPetugasMenyerahkan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString()); 
            NmPetugasMenyerahkan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),45).toString()); 
            level.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),48).toString()); 
            rtl.setText(tbObat.getValueAt(tbObat.getSelectedRow(),49).toString());
            Valid.SetTgl2(TanggalMasuk,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            Valid.SetTgl2(TanggalPindah,tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            S.setText(tbObat.getValueAt(tbObat.getSelectedRow(),52).toString());
            B.setText(tbObat.getValueAt(tbObat.getSelectedRow(),53).toString());
            A.setText(tbObat.getValueAt(tbObat.getSelectedRow(),54).toString());
            R.setText(tbObat.getValueAt(tbObat.getSelectedRow(),55).toString());
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    private void isRawatIGD() {
        try {
            psigd=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk, pasien.tgl_lahir, reg_periksa.tgl_registrasi, penilaian_medis_igd.no_rawat, penilaian_medis_igd.keluhan_utama, penilaian_medis_igd.gcs, penilaian_medis_igd.td, penilaian_medis_igd.nadi, penilaian_medis_igd.rr, penilaian_medis_igd.suhu, penilaian_medis_igd.spo, penilaian_medis_igd.tb, penilaian_medis_igd.bb, penilaian_medis_igd.diagnosis "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+ 
                    "left join penilaian_medis_igd on penilaian_medis_igd.no_rawat = reg_periksa.no_rawat "+
                    "where reg_periksa.no_rawat=?");
            try {
                psigd.setString(1,TNoRw.getText());
                rsigd=psigd.executeQuery();
                if(rsigd.next()){
                    TNoRM.setText(rsigd.getString("no_rkm_medis"));
                    DTPCari1.setDate(rsigd.getDate("tgl_registrasi"));
                    TPasien.setText(rsigd.getString("nm_pasien"));
                    Jk.setText(rsigd.getString("jk"));
                    TglLahir.setText(rsigd.getString("tgl_lahir"));
                    KeluhanUtamaSebelumTransfer.setText(rsigd.getString("keluhan_utama"));
                    DiagnosaSekunder.setText(rsigd.getString("diagnosis"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rsigd!=null){
                    rsigd.close();
                }
                if(psigd!=null){
                    psigd.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    private void isRawatRalan() {
        try {
            psralan=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "where reg_periksa.no_rawat=?");
            try {
                psralan.setString(1,TNoRw.getText());
                rsralan=psralan.executeQuery();
                if(rsralan.next()){
                    TNoRM.setText(rsralan.getString("no_rkm_medis"));
                    DTPCari1.setDate(rsralan.getDate("tgl_registrasi"));
                    TPasien.setText(rsralan.getString("nm_pasien"));
                    Jk.setText(rsralan.getString("jk"));
                    TglLahir.setText(rsralan.getString("tgl_lahir"));
                   
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rsralan!=null){
                    rsralan.close();
                }
                if(psralan!=null){
                    psralan.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    private void getSOAPIGD() {
        try {
            pssoapigd=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi,pemeriksaan_ralan.keluhan, pemeriksaan_ralan.pemeriksaan, pemeriksaan_ralan.penilaian, pemeriksaan_ralan.rtl "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "left join pemeriksaan_ralan on pemeriksaan_ralan.no_rawat = reg_periksa.no_rawat "+
                    "where reg_periksa.no_rawat=? order by pemeriksaan_ralan.no_rawat asc, pemeriksaan_ralan.tgl_perawatan desc,pemeriksaan_ralan.jam_rawat desc limit 1 ");
            try {
                pssoapigd.setString(1,TNoRw.getText());
                rssoapigd=pssoapigd.executeQuery();
                if(rssoapigd.next()){
                    S.setText(rssoapigd.getString("keluhan"));
                    B.setText(rssoapigd.getString("pemeriksaan"));
                    A.setText(rssoapigd.getString("penilaian"));
                    R.setText(rssoapigd.getString("rtl"));
                
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
                
            } finally{
                if(rssoapigd!=null){
                    rssoapigd.close();
                }
                if(pssoapigd!=null){
                    pssoapigd.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    private void getSOAPRALAN() {
        try {
            pssoapralan=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi,pemeriksaan_ralan.keluhan, pemeriksaan_ralan.pemeriksaan, pemeriksaan_ralan.penilaian, pemeriksaan_ralan.rtl "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "left join pemeriksaan_ralan on pemeriksaan_ralan.no_rawat = reg_periksa.no_rawat "+
                    "where reg_periksa.no_rawat=? order by pemeriksaan_ralan.no_rawat asc, pemeriksaan_ralan.tgl_perawatan desc,pemeriksaan_ralan.jam_rawat desc limit 1 ");
            try {
                pssoapralan.setString(1,TNoRw.getText());
                rssoapralan=pssoapralan.executeQuery();
                if(rssoapralan.next()){
                    KeluhanUtamaSebelumTransfer.setText(rssoapralan.getString("keluhan"));
                    S.setText(rssoapralan.getString("keluhan"));
                    B.setText(rssoapralan.getString("pemeriksaan"));
                    A.setText(rssoapralan.getString("penilaian"));
                    R.setText(rssoapralan.getString("rtl"));
                }
               
            } catch (Exception e) {
                System.out.println("Notif : "+e);
                
            } finally{
                if(rssoapralan!=null){
                    rssoapralan.close();
                }
                if(pssoapralan!=null){
                    pssoapralan.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    private void getSOAPRANAP() {
        try {
            pssoapranap=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi,pemeriksaan_ranap.keluhan, pemeriksaan_ranap.pemeriksaan, pemeriksaan_ranap.penilaian, pemeriksaan_ranap.rtl "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "left join pemeriksaan_ranap on pemeriksaan_ranap.no_rawat = reg_periksa.no_rawat "+
                    "where reg_periksa.no_rawat=? order by pemeriksaan_ranap.no_rawat asc, pemeriksaan_ranap.tgl_perawatan desc,pemeriksaan_ranap.jam_rawat desc limit 1 ");
            try {
                pssoapranap.setString(1,TNoRw.getText());
                rssoapranap=pssoapranap.executeQuery();
                if(rssoapranap.next()){
                    KeluhanUtamaSebelumTransfer.setText(rssoapranap.getString("keluhan"));
                    S.setText(rssoapranap.getString("keluhan"));
                    B.setText(rssoapranap.getString("pemeriksaan"));
                    A.setText(rssoapranap.getString("penilaian"));
                    R.setText(rssoapranap.getString("rtl"));
                }
               
            } catch (Exception e) {
                System.out.println("Notif : "+e);
                
            } finally{
                if(rssoapranap!=null){
                    rssoapranap.close();
                }
                if(pssoapranap!=null){
                    pssoapranap.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    public void kosongkanpetugas(){
        KdPetugasMenyerahkan.setText("");
        NmPetugasMenyerahkan.setText("");
    }
    public void setNoRm(String norwt,Date tgl2,Date tgl3) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari1.setDate(tgl2); 
        DTPCari2.setDate(tgl3);    
        isRawat(); 
        getSOAPRANAP();
        kosongkanpetugas();
    }
    
    public void setNoRmIGD(String norwt,Date tgl2,Date tgl3) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari1.setDate(tgl2); 
        DTPCari2.setDate(tgl3);    
        isRawatIGD();
        getSOAPIGD();
        kosongkanpetugas();
    }
    
    public void setNoRmRalan(String norwt,Date tgl2,Date tgl3) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari1.setDate(tgl2); 
        DTPCari2.setDate(tgl3);     
        isRawatRalan();
        getSOAPRALAN();
        kosongkanpetugas();
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnHapus.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnPrint.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnSimpanVerifikasi.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnCetakVerifikasi.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
    }
    
    public void setTampil(){
       TabRawat.setSelectedIndex(1);
    }

    private void hapus() {
        if(Sequel.queryu2tf("delete from transfer_pasien_antar_ruang where no_rawat=? and tanggal_masuk=?",2,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()
        })==true){
            tabMode.removeRow(tbObat.getSelectedRow());
            LCount.setText(""+tabMode.getRowCount());
            TabRawat.setSelectedIndex(1);
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }

    private void ganti() {
       checkboxRM = "";
       checkboxRAD = "";
       checkboxLAB = "";
       checkboxLAB = "";
       checkboxLAINNYA = "";
        if(ChkRM.isSelected()){
           checkboxRM="Ya";
          }else{
           checkboxRM="Tidak";
        }
        if(ChkRAD.isSelected()){
           checkboxRAD="Ya";
          }else{
           checkboxRAD="Tidak";
        }
        if(ChkLAB.isSelected()){
           checkboxLAB="Ya";
          }else{
           checkboxLAB="Tidak";
        }
        if(ChkLainnya.isSelected()){
           checkboxLAINNYA="Ya";
          }else{
           checkboxLAINNYA="Tidak";
        }
        if(Sequel.mengedittf("transfer_pasien_antar_ruang","no_rawat=? and tanggal_masuk=? and tanggal_pindah=?","no_rawat=?,tanggal_masuk=?,tanggal_pindah=?,asal_ruang=?,ruang_selanjutnya=?,"+
                "alasan_transfer=?,diagnosa_sekunder=?,indikasi_pindah_ruang=?,keterangan_indikasi_pindah_ruang=?,prosedur_yang_sudah_dilakukan=?,obat_yang_telah_diberikan=?,metode_pemindahan_pasien=?,"+
                "peralatan_yang_menyertai=?,obat_yang_menyertai=?,dokumen_RM_dikirim=?,dokumen_LAB_dikirim=?,dokumen_RAD_dikirim=?,dokumen_lainnya_dikirim=?,dokumen_lainnya_ket_dikirim=?, "+
                "dokumen_RM_diterima=?,dokumen_LAB_diterima=?,dokumen_RAD_diterima=?,dokumen_lainnya_diterima=?,dokumen_lainnya_ket_diterima=?,"+
                "pemeriksaan_penunjang_yang_dilakukan=?,pasien_keluarga_menyetujui=?,nama_menyetujui=?,hubungan_menyetujui=?,"+
                "keluhan_utama_sebelum_transfer=?,keadaan_umum_sebelum_transfer=?,td_sebelum_transfer=?,nadi_sebelum_transfer=?,rr_sebelum_transfer=?,suhu_sebelum_transfer=?,"+
                "keluhan_utama_sesudah_transfer=?,keadaan_umum_sesudah_transfer=?,td_sesudah_transfer=?,nadi_sesudah_transfer=?,rr_sesudah_transfer=?,suhu_sesudah_transfer=?,"+
                "nip_menyerahkan=?,nip_menerima=?,level=?,rtl=?,tanggal_diterima=?,waktu_diterima=?,S=?,B=?,A=?,R=?",53,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(TanggalMasuk.getSelectedItem()+"")+" "+TanggalMasuk.getSelectedItem().toString().substring(11,19),
                Valid.SetTgl(TanggalPindah.getSelectedItem()+"")+" "+TanggalPindah.getSelectedItem().toString().substring(11,19),
                AsalRuang.getText(),
                RuangSelanjutnya.getText(),
                AlasanTransfer.getText(),
                DiagnosaSekunder.getText(),
                IndikasiPindah.getSelectedItem().toString(),
                KeteranganIndikasiPindahRuang.getText(), 
                ProsedurDilakukan.getText(),
                ObatYangDiberikan.getText(),
                MetodePemindahan.getSelectedItem().toString(),
                PeralatanMenyertai.getText(),
                ObatMenyertai.getText(),
                checkboxRM,
                checkboxLAB,
                checkboxRAD,
                checkboxLAINNYA,
                ket_lainnya.getText(),
                "",
                "",
                "",
                "",
                "",
                PemeriksaanPenunjang.getText(),
                MenyetujuiPemindahan.getSelectedItem().toString(),
                NamaMenyetujui.getText(),
                HubunganMenyetujui.getSelectedItem().toString(),
                KeluhanUtamaSebelumTransfer.getText(),
                KeadaanUmumSebelumTransfer.getSelectedItem().toString(),
                TDSebelumTransfer.getText(),
                NadiSebelumTransfer.getText(),
                RRSebelumTransfer.getText(),
                SuhuSebelumTransfer.getText(),
                "-",
                "-",
                "-",
                "-",
                "-",
                "-",
                KdPetugasMenyerahkan.getText(),
                "-",
                level.getSelectedItem().toString(),
                rtl.getText(),
                "0000-00-00",
                "00:00:00",
                S.getText(),
                B.getText(),
                A.getText(),
                R.getText(),
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(), // where 
                tbObat.getValueAt(tbObat.getSelectedRow(),5).toString(),
                tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()
            })==true){
                emptTeks();
                TabRawat.setSelectedIndex(1);
                
        }
    }

    private void simpan() {
       checkboxRM = "";
       checkboxRAD = "";
       checkboxLAB = "";
       checkboxLAB = "";
       checkboxLAINNYA = "";
        if(ChkRM.isSelected()){
           checkboxRM="Ya";
          }else{
           checkboxRM="Tidak";
        }
        if(ChkRAD.isSelected()){
           checkboxRAD="Ya";
          }else{
           checkboxRAD="Tidak";
        }
        if(ChkLAB.isSelected()){
           checkboxLAB="Ya";
          }else{
           checkboxLAB="Tidak";
        }
        if(ChkLainnya.isSelected()){
           checkboxLAINNYA="Ya";
          }else{
           checkboxLAINNYA="Tidak";
        }
        if(Sequel.menyimpantf("transfer_pasien_antar_ruang","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat & Tanggal Masuk",50,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(TanggalMasuk.getSelectedItem()+"")+" "+TanggalMasuk.getSelectedItem().toString().substring(11,19),
                Valid.SetTgl(TanggalPindah.getSelectedItem()+"")+" "+TanggalPindah.getSelectedItem().toString().substring(11,19),
                AsalRuang.getText(), 
                RuangSelanjutnya.getText(),
                AlasanTransfer.getText(),
                DiagnosaSekunder.getText(),
                IndikasiPindah.getSelectedItem().toString(),
                KeteranganIndikasiPindahRuang.getText(), 
                ProsedurDilakukan.getText(),
                ObatYangDiberikan.getText(),
                MetodePemindahan.getSelectedItem().toString(),
                PeralatanMenyertai.getText(),
                ObatMenyertai.getText(),
                checkboxRM,
                checkboxLAB,
                checkboxRAD,
                checkboxLAINNYA,
                ket_lainnya.getText(),
                "-",
                "-",
                "-",
                "-",
                "-",
                PemeriksaanPenunjang.getText(),
                MenyetujuiPemindahan.getSelectedItem().toString(),
                NamaMenyetujui.getText(),
                HubunganMenyetujui.getSelectedItem().toString(),
                KeluhanUtamaSebelumTransfer.getText(),
                KeadaanUmumSebelumTransfer.getSelectedItem().toString(),
                TDSebelumTransfer.getText(),
                NadiSebelumTransfer.getText(),
                RRSebelumTransfer.getText(),
                SuhuSebelumTransfer.getText(),
                "-",
                "-",
                "-",
                "-",
                "-",
                "-",
                KdPetugasMenyerahkan.getText(),
                "-",
                level.getSelectedItem().toString(),
                rtl.getText(),
                "0000-00-00",
                "00:00:00",
                S.getText(),
                B.getText(),
                A.getText(),
                R.getText()
            })==true){
                emptTeks();
        }
    }
    
    private void isPhoto(){
        if(ChkAccor.isSelected()==true){
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(480,HEIGHT));
            FormPhoto.setVisible(true);  
            ChkAccor.setVisible(true);
        }else if(ChkAccor.isSelected()==false){    
            ChkAccor.setVisible(false);
            PanelAccor.setPreferredSize(new Dimension(15,HEIGHT));
            FormPhoto.setVisible(false);  
            ChkAccor.setVisible(true);
        }
    }

    private void panggilPhoto() {
        if(FormPhoto.isVisible()==true){
            try {
                ps=koneksi.prepareStatement("select bukti_persetujuan_transfer_pasien_antar_ruang.photo from bukti_persetujuan_transfer_pasien_antar_ruang where bukti_persetujuan_transfer_pasien_antar_ruang.no_rawat=? and bukti_persetujuan_transfer_pasien_antar_ruang.tanggal_masuk=?");
                try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    ps.setString(2,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
                    rs=ps.executeQuery();
                    if(rs.next()){
                        if(rs.getString("photo").equals("")||rs.getString("photo").equals("-")){
                            LoadHTML2.setText("<html><body><center><br><br><font face='tahoma' size='2' color='#434343'>Kosong</font></center></body></html>");
                        }else{
                            LoadHTML2.setText("<html><body><center><img src='http://"+koneksiDB.HOSTHYBRIDWEB()+":"+koneksiDB.PORTWEB()+"/"+koneksiDB.HYBRIDWEB()+"/persetujuantransferruang/"+rs.getString("photo")+"' alt='photo' width='500' height='500'/></center></body></html>");
                        }  
                    }else{
                        LoadHTML2.setText("<html><body><center><br><br><font face='tahoma' size='2' color='#434343'>Kosong</font></center></body></html>");
                    }
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            }
        }
    }
     
    private void jam2(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkTransfer.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkTransfer.isSelected()==false){
                    nilai_jam =Jam1.getSelectedIndex();
                    nilai_menit =Menit1.getSelectedIndex();
                    nilai_detik =Detik1.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam1.setSelectedItem(jam);
                Menit1.setSelectedItem(menit);
                Detik1.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
}
