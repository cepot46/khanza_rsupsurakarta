

package keuangan;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariPetugas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 *
 * @author perpustakaan
 */
public final class DlgDetailFarmasi extends javax.swing.JDialog {
    private final Connection koneksi=koneksiDB.condb();
    private final sekuel Sequel=new sekuel();
    private DefaultTableModel tabModeOperasi,tabModeOperasi2,tabModeResepRanap,tabModePetugasResepRanap,tabModePermintaanStok,tabModePetugasPermintaanStok,tabModeResepPulang,tabModePetugasResepPulang;
    private validasi Valid=new validasi();
    private ResultSet rs,rstindakan;
    private PreparedStatement ps,pstindakan;
    private String sql;
    private int i=0;
    private double total=0,biayaoperator1=0,biayaoperator2=0, 
            biayaoperator3=0,biayaasisten_operator1=0,biayaasisten_operator2=0,
            biayaasisten_operator3=0,biayainstrumen=0,biayadokter_anak=0,
            biayaperawaat_resusitas=0,biayadokter_anestesi=0,biayaasisten_anestesi=0,
            biayaasisten_anestesi2=0,biayabidan=0,biayabidan2=0,biayabidan3=0,
            biayaperawat_luar=0,biayaalat=0,biayasewaok=0,akomodasi=0,
            bagian_rs=0,biaya_omloop=0,biaya_omloop2=0,biaya_omloop3=0,
            biaya_omloop4=0,biaya_omloop5=0,biayasarpras=0,biaya_dokter_pjanak=0,
            biaya_dokter_umum=0;
    public  DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    int temp_ptg=0;

    /** Creates new form DlgLhtBiaya
     * @param parent
     * @param modal */
    public DlgDetailFarmasi(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(885,674);
        
        
        tabModeOperasi=new DefaultTableModel(null,new Object[]{
            "No. Resep","No. RM","Pasien","Petugas Entri","Petugas Ambil","Petugas Cek",
            "Petugas Racikan","Petugas Serah + KIE","Petugas Konseling","Petugas Obat Kronis"
        }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
             Class[] types = new Class[] {
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbOperasi.setModel(tabModeOperasi);
        tbOperasi.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbOperasi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 10; i++) {
            TableColumn column = tbOperasi.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(100);
            }else if(i==1){
                column.setPreferredWidth(100);
            }else if(i==2){
                column.setPreferredWidth(250);
            }else if(i==3){
                column.setPreferredWidth(250);
            }else if(i==4){
                column.setPreferredWidth(250);
            }else if(i==5){
                column.setPreferredWidth(250);
            }else if(i==6){
                column.setPreferredWidth(250);
            }else if(i==7){
                column.setPreferredWidth(250);
            }else if(i==8){
                column.setPreferredWidth(250);
            }else if(i==9){
                column.setPreferredWidth(250);
            }
        }
        tbOperasi.setDefaultRenderer(Object.class, new WarnaTable());
        
        tabModeOperasi2=new DefaultTableModel(null,new Object[]{
            "Nama","Petugas Entri","Petugas Ambil","Petugas Cek",
            "Petugas Racikan","Petugas Serah + KIE","Petugas Konseling","Petugas Obat Kronis"
        }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
             Class[] types = new Class[] {
                 java.lang.Object.class,
                 java.lang.Double.class,java.lang.Double.class,java.lang.Double.class,
                 java.lang.Double.class,java.lang.Double.class,java.lang.Double.class,
                 java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbOperasi1.setModel(tabModeOperasi2);
        tbOperasi1.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbOperasi1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 8; i++) {
            TableColumn column = tbOperasi1.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(250);
            }else if(i==1){
                column.setPreferredWidth(100);
            }else if(i==2){
                column.setPreferredWidth(100);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(100);
            }else if(i==5){
                column.setPreferredWidth(150);
            }else if(i==6){
                column.setPreferredWidth(100);
            }else if(i==7){
                column.setPreferredWidth(100);
            }
        }
        tbOperasi1.setDefaultRenderer(Object.class, new WarnaTable());
        
        tabModeResepRanap=new DefaultTableModel(null,new Object[]{
            "No. Resep","No. RM","Pasien","Petugas Entri","Petugas Ambil","Petugas Cek",
            "Petugas Racikan","Petugas Serah + KIE","Petugas Konseling","Petugas Obat Kronis"
        }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
             Class[] types = new Class[] {
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbResepRanap.setModel(tabModeResepRanap);
        tbResepRanap.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbResepRanap.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 10; i++) {
            TableColumn column = tbResepRanap.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(100);
            }else if(i==1){
                column.setPreferredWidth(100);
            }else if(i==2){
                column.setPreferredWidth(250);
            }else if(i==3){
                column.setPreferredWidth(250);
            }else if(i==4){
                column.setPreferredWidth(250);
            }else if(i==5){
                column.setPreferredWidth(250);
            }else if(i==6){
                column.setPreferredWidth(250);
            }else if(i==7){
                column.setPreferredWidth(250);
            }else if(i==8){
                column.setPreferredWidth(250);
            }else if(i==9){
                column.setPreferredWidth(250);
            }
        }
        tbResepRanap.setDefaultRenderer(Object.class, new WarnaTable());     
        
        tabModePetugasResepRanap=new DefaultTableModel(null,new Object[]{
            "Nama","Petugas Entri","Petugas Ambil","Petugas Cek",
            "Petugas Racikan","Petugas Serah + KIE","Petugas Konseling"
        }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
             Class[] types = new Class[] {
                 java.lang.Object.class,
                 java.lang.Double.class,java.lang.Double.class,java.lang.Double.class,
                 java.lang.Double.class,java.lang.Double.class,java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbPetugasResepRanap.setModel(tabModePetugasResepRanap);
        tbPetugasResepRanap.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPetugasResepRanap.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 7; i++) {
            TableColumn column = tbPetugasResepRanap.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(250);
            }else if(i==1){
                column.setPreferredWidth(100);
            }else if(i==2){
                column.setPreferredWidth(100);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(100);
            }else if(i==5){
                column.setPreferredWidth(150);
            }else if(i==6){
                column.setPreferredWidth(100);
            }
        }
        tbPetugasResepRanap.setDefaultRenderer(Object.class, new WarnaTable());        

        tabModePermintaanStok=new DefaultTableModel(null,new Object[]{
            "No. Permintaan","No. RM","Pasien","Petugas Entri","Petugas Ambil","Petugas Cek",
            "Petugas Racikan","Petugas Serah + KIE","Petugas Konseling"
        }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
             Class[] types = new Class[] {
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbPermintaanStok.setModel(tabModePermintaanStok);
        tbPermintaanStok.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPermintaanStok.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 9; i++) {
            TableColumn column = tbPermintaanStok.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(100);
            }else if(i==1){
                column.setPreferredWidth(100);
            }else if(i==2){
                column.setPreferredWidth(250);
            }else if(i==3){
                column.setPreferredWidth(250);
            }else if(i==4){
                column.setPreferredWidth(250);
            }else if(i==5){
                column.setPreferredWidth(250);
            }else if(i==6){
                column.setPreferredWidth(250);
            }else if(i==7){
                column.setPreferredWidth(250);
            }else if(i==8){
                column.setPreferredWidth(250);
            }
        }
        tbPermintaanStok.setDefaultRenderer(Object.class, new WarnaTable());  

        tabModePetugasPermintaanStok=new DefaultTableModel(null,new Object[]{
            "Nama","Petugas Entri","Petugas Ambil","Petugas Cek",
            "Petugas Racikan","Petugas Serah + KIE","Petugas Konseling"
        }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
             Class[] types = new Class[] {
                 java.lang.Object.class,
                 java.lang.Double.class,java.lang.Double.class,java.lang.Double.class,
                 java.lang.Double.class,java.lang.Double.class,java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbPetugasPermintaanStok.setModel(tabModePetugasPermintaanStok);
        tbPetugasPermintaanStok.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPetugasPermintaanStok.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 7; i++) {
            TableColumn column = tbPetugasPermintaanStok.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(250);
            }else if(i==1){
                column.setPreferredWidth(100);
            }else if(i==2){
                column.setPreferredWidth(100);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(100);
            }else if(i==5){
                column.setPreferredWidth(150);
            }else if(i==6){
                column.setPreferredWidth(100);
            }
        }
        tbPetugasPermintaanStok.setDefaultRenderer(Object.class, new WarnaTable());  

        tabModeResepPulang=new DefaultTableModel(null,new Object[]{
            "No. Permintaan","No. RM","Pasien","Petugas Entri","Petugas Ambil","Petugas Cek",
            "Petugas Racikan","Petugas Serah + KIE","Petugas Konseling"
        }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
             Class[] types = new Class[] {
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class,
                 java.lang.Object.class,java.lang.Object.class,java.lang.Object.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbResepPulang.setModel(tabModeResepPulang);
        tbResepPulang.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbResepPulang.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 9; i++) {
            TableColumn column = tbResepPulang.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(100);
            }else if(i==1){
                column.setPreferredWidth(100);
            }else if(i==2){
                column.setPreferredWidth(250);
            }else if(i==3){
                column.setPreferredWidth(250);
            }else if(i==4){
                column.setPreferredWidth(250);
            }else if(i==5){
                column.setPreferredWidth(250);
            }else if(i==6){
                column.setPreferredWidth(250);
            }else if(i==7){
                column.setPreferredWidth(250);
            }else if(i==8){
                column.setPreferredWidth(250);
            }
        }
        tbResepPulang.setDefaultRenderer(Object.class, new WarnaTable()); 

        tabModePetugasResepPulang=new DefaultTableModel(null,new Object[]{
            "Nama","Petugas Entri","Petugas Ambil","Petugas Cek",
            "Petugas Racikan","Petugas Serah + KIE","Petugas Konseling"
        }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
             Class[] types = new Class[] {
                 java.lang.Object.class,
                 java.lang.Double.class,java.lang.Double.class,java.lang.Double.class,
                 java.lang.Double.class,java.lang.Double.class,java.lang.Double.class
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbPetugasResepPulang.setModel(tabModePetugasResepPulang);
        tbPetugasResepPulang.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPetugasResepPulang.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 7; i++) {
            TableColumn column = tbPetugasResepPulang.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(250);
            }else if(i==1){
                column.setPreferredWidth(100);
            }else if(i==2){
                column.setPreferredWidth(100);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(100);
            }else if(i==5){
                column.setPreferredWidth(150);
            }else if(i==6){
                column.setPreferredWidth(100);
            }
        }
        tbPetugasResepPulang.setDefaultRenderer(Object.class, new WarnaTable()); 
        
        TCari.setDocument(new batasInput((byte)100).getKata(TCari));
            
        //START CUSTOM MUHSIN
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {;}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){ 
                    if(temp_ptg==7){
                        KdEntri1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrEntri1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrEntri1.requestFocus();
                    }else if(temp_ptg==8){
                        KdAmbil1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrAmbil1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrAmbil1.requestFocus();
                    }else if(temp_ptg==9){
                        KdCek1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrCek1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrCek1.requestFocus();
                    }else if(temp_ptg==10){
                        KdRacikan1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrRacikan1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrRacikan1.requestFocus();
                    }else if(temp_ptg==11){
                        KdSerahKIE1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrSerahKIE1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrSerahKIE1.requestFocus();
                    }else if(temp_ptg==12){
                        KdKonseling1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrKonseling1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrKonseling1.requestFocus();
                    }
                }                
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        //DlgCatatan.setSize(595,80); //CUSTOM MUHSIN
        //END CUSTOM
    }
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        WindowEditPetugasFarmasi = new javax.swing.JDialog();
        internalFrame6 = new widget.InternalFrame();
        BtnClosePetugasFarmasi1 = new widget.Button();
        BtnEditPetugasFarmasi = new widget.Button();
        jLabel35 = new widget.Label();
        jLabel36 = new widget.Label();
        jLabel37 = new widget.Label();
        jLabel38 = new widget.Label();
        jLabel39 = new widget.Label();
        jLabel40 = new widget.Label();
        jLabel41 = new widget.Label();
        jLabel42 = new widget.Label();
        CrAmbil1 = new widget.TextBox();
        CrEntri1 = new widget.TextBox();
        CrRacikan1 = new widget.TextBox();
        CrKonseling1 = new widget.TextBox();
        CrCek1 = new widget.TextBox();
        CrSerahKIE1 = new widget.TextBox();
        BtnSeekEntri1 = new widget.Button();
        BtnSeekKonseling1 = new widget.Button();
        BtnSeekAmbil1 = new widget.Button();
        BtnSeekCek1 = new widget.Button();
        BtnSeekRacikan1 = new widget.Button();
        BtnSeekSerahKIE1 = new widget.Button();
        KdKonseling1 = new widget.TextBox();
        KdEntri1 = new widget.TextBox();
        KdAmbil1 = new widget.TextBox();
        KdCek1 = new widget.TextBox();
        KdRacikan1 = new widget.TextBox();
        KdSerahKIE1 = new widget.TextBox();
        jLabel43 = new widget.Label();
        jLabel44 = new widget.Label();
        internalFrame1 = new widget.InternalFrame();
        panelGlass5 = new widget.panelisi();
        label9 = new widget.Label();
        Tgl1 = new widget.Tanggal();
        label18 = new widget.Label();
        Tgl2 = new widget.Tanggal();
        label10 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        label11 = new widget.Label();
        BtnPetugas = new widget.Button();
        BtnPrint = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame5 = new widget.InternalFrame();
        Scroll3 = new widget.ScrollPane();
        tbOperasi = new widget.Table();
        internalFrame12 = new widget.InternalFrame();
        Scroll10 = new widget.ScrollPane();
        tbOperasi1 = new widget.Table();
        internalFrame7 = new widget.InternalFrame();
        Scroll4 = new widget.ScrollPane();
        tbResepRanap = new widget.Table();
        internalFrame13 = new widget.InternalFrame();
        Scroll11 = new widget.ScrollPane();
        tbPetugasResepRanap = new widget.Table();
        internalFrame8 = new widget.InternalFrame();
        Scroll5 = new widget.ScrollPane();
        tbPermintaanStok = new widget.Table();
        internalFrame14 = new widget.InternalFrame();
        Scroll12 = new widget.ScrollPane();
        tbPetugasPermintaanStok = new widget.Table();
        internalFrame9 = new widget.InternalFrame();
        Scroll6 = new widget.ScrollPane();
        tbResepPulang = new widget.Table();
        internalFrame15 = new widget.InternalFrame();
        Scroll13 = new widget.ScrollPane();
        tbPetugasResepPulang = new widget.Table();

        WindowEditPetugasFarmasi.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        WindowEditPetugasFarmasi.setName("WindowEditPetugasFarmasi"); // NOI18N
        WindowEditPetugasFarmasi.setUndecorated(true);
        WindowEditPetugasFarmasi.setResizable(false);
        WindowEditPetugasFarmasi.setSize(new java.awt.Dimension(700, 270));

        internalFrame6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "::[ Data Petugas Farmasi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame6.setName("internalFrame6"); // NOI18N
        internalFrame6.setLayout(null);

        BtnClosePetugasFarmasi1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/cross.png"))); // NOI18N
        BtnClosePetugasFarmasi1.setMnemonic('U');
        BtnClosePetugasFarmasi1.setText("Tutup");
        BtnClosePetugasFarmasi1.setToolTipText("Alt+U");
        BtnClosePetugasFarmasi1.setName("BtnClosePetugasFarmasi1"); // NOI18N
        BtnClosePetugasFarmasi1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnClosePetugasFarmasi1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnClosePetugasFarmasi1);
        BtnClosePetugasFarmasi1.setBounds(620, 230, 80, 30);

        BtnEditPetugasFarmasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEditPetugasFarmasi.setMnemonic('S');
        BtnEditPetugasFarmasi.setText("Edit");
        BtnEditPetugasFarmasi.setToolTipText("Alt+S");
        BtnEditPetugasFarmasi.setName("BtnEditPetugasFarmasi"); // NOI18N
        BtnEditPetugasFarmasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditPetugasFarmasiActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnEditPetugasFarmasi);
        BtnEditPetugasFarmasi.setBounds(530, 230, 90, 30);

        jLabel35.setText("Petugas Serah + KIE :");
        jLabel35.setName("jLabel35"); // NOI18N
        internalFrame6.add(jLabel35);
        jLabel35.setBounds(10, 170, 110, 23);

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel36.setName("jLabel36"); // NOI18N
        internalFrame6.add(jLabel36);
        jLabel36.setBounds(130, 20, 70, 23);

        jLabel37.setText("Petugas Entri :");
        jLabel37.setName("jLabel37"); // NOI18N
        internalFrame6.add(jLabel37);
        jLabel37.setBounds(40, 50, 80, 23);

        jLabel38.setText("Petugas Ambil :");
        jLabel38.setName("jLabel38"); // NOI18N
        internalFrame6.add(jLabel38);
        jLabel38.setBounds(40, 80, 80, 23);

        jLabel39.setText("Petugas Cek :");
        jLabel39.setName("jLabel39"); // NOI18N
        internalFrame6.add(jLabel39);
        jLabel39.setBounds(40, 110, 80, 23);

        jLabel40.setText("Petugas Konseling :");
        jLabel40.setName("jLabel40"); // NOI18N
        internalFrame6.add(jLabel40);
        jLabel40.setBounds(20, 200, 100, 23);

        jLabel41.setText("Petugas Racikan :");
        jLabel41.setName("jLabel41"); // NOI18N
        internalFrame6.add(jLabel41);
        jLabel41.setBounds(20, 140, 100, 23);

        jLabel42.setText("Waktu Obat Jadi :");
        jLabel42.setName("jLabel42"); // NOI18N
        internalFrame6.add(jLabel42);
        jLabel42.setBounds(20, 20, 100, 23);

        CrAmbil1.setEditable(false);
        CrAmbil1.setName("CrAmbil1"); // NOI18N
        CrAmbil1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrAmbil1);
        CrAmbil1.setBounds(270, 80, 280, 23);

        CrEntri1.setEditable(false);
        CrEntri1.setName("CrEntri1"); // NOI18N
        CrEntri1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrEntri1);
        CrEntri1.setBounds(270, 50, 280, 23);

        CrRacikan1.setEditable(false);
        CrRacikan1.setName("CrRacikan1"); // NOI18N
        CrRacikan1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrRacikan1);
        CrRacikan1.setBounds(270, 140, 280, 23);

        CrKonseling1.setEditable(false);
        CrKonseling1.setName("CrKonseling1"); // NOI18N
        CrKonseling1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrKonseling1);
        CrKonseling1.setBounds(270, 200, 280, 23);

        CrCek1.setEditable(false);
        CrCek1.setName("CrCek1"); // NOI18N
        CrCek1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrCek1);
        CrCek1.setBounds(270, 110, 280, 23);

        CrSerahKIE1.setEditable(false);
        CrSerahKIE1.setName("CrSerahKIE1"); // NOI18N
        CrSerahKIE1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrSerahKIE1);
        CrSerahKIE1.setBounds(270, 170, 280, 23);

        BtnSeekEntri1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekEntri1.setMnemonic('4');
        BtnSeekEntri1.setToolTipText("ALt+4");
        BtnSeekEntri1.setName("BtnSeekEntri1"); // NOI18N
        BtnSeekEntri1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekEntri1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekEntri1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekEntri1);
        BtnSeekEntri1.setBounds(560, 50, 28, 23);

        BtnSeekKonseling1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekKonseling1.setMnemonic('4');
        BtnSeekKonseling1.setToolTipText("ALt+4");
        BtnSeekKonseling1.setName("BtnSeekKonseling1"); // NOI18N
        BtnSeekKonseling1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekKonseling1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekKonseling1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekKonseling1);
        BtnSeekKonseling1.setBounds(560, 200, 28, 23);

        BtnSeekAmbil1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekAmbil1.setMnemonic('4');
        BtnSeekAmbil1.setToolTipText("ALt+4");
        BtnSeekAmbil1.setName("BtnSeekAmbil1"); // NOI18N
        BtnSeekAmbil1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekAmbil1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekAmbil1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekAmbil1);
        BtnSeekAmbil1.setBounds(560, 80, 28, 23);

        BtnSeekCek1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekCek1.setMnemonic('4');
        BtnSeekCek1.setToolTipText("ALt+4");
        BtnSeekCek1.setName("BtnSeekCek1"); // NOI18N
        BtnSeekCek1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekCek1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekCek1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekCek1);
        BtnSeekCek1.setBounds(560, 110, 28, 23);

        BtnSeekRacikan1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekRacikan1.setMnemonic('4');
        BtnSeekRacikan1.setToolTipText("ALt+4");
        BtnSeekRacikan1.setName("BtnSeekRacikan1"); // NOI18N
        BtnSeekRacikan1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekRacikan1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekRacikan1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekRacikan1);
        BtnSeekRacikan1.setBounds(560, 140, 28, 23);

        BtnSeekSerahKIE1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekSerahKIE1.setMnemonic('4');
        BtnSeekSerahKIE1.setToolTipText("ALt+4");
        BtnSeekSerahKIE1.setName("BtnSeekSerahKIE1"); // NOI18N
        BtnSeekSerahKIE1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekSerahKIE1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekSerahKIE1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekSerahKIE1);
        BtnSeekSerahKIE1.setBounds(560, 170, 28, 23);

        KdKonseling1.setEditable(false);
        KdKonseling1.setHighlighter(null);
        KdKonseling1.setName("KdKonseling1"); // NOI18N
        KdKonseling1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdKonseling1ActionPerformed(evt);
            }
        });
        KdKonseling1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdKonseling1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdKonseling1);
        KdKonseling1.setBounds(130, 200, 130, 23);

        KdEntri1.setEditable(false);
        KdEntri1.setHighlighter(null);
        KdEntri1.setName("KdEntri1"); // NOI18N
        KdEntri1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdEntri1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdEntri1);
        KdEntri1.setBounds(130, 50, 130, 23);

        KdAmbil1.setEditable(false);
        KdAmbil1.setHighlighter(null);
        KdAmbil1.setName("KdAmbil1"); // NOI18N
        KdAmbil1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdAmbil1ActionPerformed(evt);
            }
        });
        KdAmbil1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdAmbil1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdAmbil1);
        KdAmbil1.setBounds(130, 80, 130, 23);

        KdCek1.setEditable(false);
        KdCek1.setHighlighter(null);
        KdCek1.setName("KdCek1"); // NOI18N
        KdCek1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdCek1ActionPerformed(evt);
            }
        });
        KdCek1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdCek1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdCek1);
        KdCek1.setBounds(130, 110, 130, 23);

        KdRacikan1.setEditable(false);
        KdRacikan1.setHighlighter(null);
        KdRacikan1.setName("KdRacikan1"); // NOI18N
        KdRacikan1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdRacikan1ActionPerformed(evt);
            }
        });
        KdRacikan1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdRacikan1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdRacikan1);
        KdRacikan1.setBounds(130, 140, 130, 23);

        KdSerahKIE1.setEditable(false);
        KdSerahKIE1.setHighlighter(null);
        KdSerahKIE1.setName("KdSerahKIE1"); // NOI18N
        KdSerahKIE1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdSerahKIE1ActionPerformed(evt);
            }
        });
        KdSerahKIE1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdSerahKIE1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdSerahKIE1);
        KdSerahKIE1.setBounds(130, 170, 130, 23);

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel43.setName("jLabel43"); // NOI18N
        internalFrame6.add(jLabel43);
        jLabel43.setBounds(500, 20, 180, 23);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel44.setName("jLabel44"); // NOI18N
        internalFrame6.add(jLabel44);
        jLabel44.setBounds(300, 20, 140, 23);

        WindowEditPetugasFarmasi.getContentPane().add(internalFrame6, java.awt.BorderLayout.CENTER);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Rekap Kinerja Farmasi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass5.setName("panelGlass5"); // NOI18N
        panelGlass5.setPreferredSize(new java.awt.Dimension(55, 55));
        panelGlass5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        label9.setText("Tanggal :");
        label9.setName("label9"); // NOI18N
        label9.setPreferredSize(new java.awt.Dimension(50, 23));
        panelGlass5.add(label9);

        Tgl1.setDisplayFormat("dd-MM-yyyy");
        Tgl1.setName("Tgl1"); // NOI18N
        Tgl1.setPreferredSize(new java.awt.Dimension(90, 23));
        Tgl1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Tgl1KeyPressed(evt);
            }
        });
        panelGlass5.add(Tgl1);

        label18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label18.setText("s.d.");
        label18.setName("label18"); // NOI18N
        label18.setPreferredSize(new java.awt.Dimension(25, 23));
        panelGlass5.add(label18);

        Tgl2.setDisplayFormat("dd-MM-yyyy");
        Tgl2.setName("Tgl2"); // NOI18N
        Tgl2.setPreferredSize(new java.awt.Dimension(90, 23));
        Tgl2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Tgl2KeyPressed(evt);
            }
        });
        panelGlass5.add(Tgl2);

        label10.setText("Key Word :");
        label10.setName("label10"); // NOI18N
        label10.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass5.add(label10);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(200, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass5.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('1');
        BtnCari.setToolTipText("Alt+1");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('2');
        BtnAll.setToolTipText("2Alt+2");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnAll);

        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(20, 23));
        panelGlass5.add(label11);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Agenda-1-16x16.png"))); // NOI18N
        BtnPetugas.setMnemonic('H');
        BtnPetugas.setText("Edit Petugas");
        BtnPetugas.setToolTipText("Alt+H");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(110, 30));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnPetugas);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        panelGlass5.add(BtnPrint);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnKeluar);

        internalFrame1.add(panelGlass5, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(255, 255, 253));
        TabRawat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(241, 246, 236)));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame5.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame5.setBorder(null);
        internalFrame5.setName("internalFrame5"); // NOI18N
        internalFrame5.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll3.setName("Scroll3"); // NOI18N
        Scroll3.setOpaque(true);

        tbOperasi.setName("tbOperasi"); // NOI18N
        tbOperasi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbOperasiMouseClicked(evt);
            }
        });
        tbOperasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbOperasiKeyPressed(evt);
            }
        });
        Scroll3.setViewportView(tbOperasi);

        internalFrame5.add(Scroll3, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Resep Ralan", internalFrame5);

        internalFrame12.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame12.setBorder(null);
        internalFrame12.setName("internalFrame12"); // NOI18N
        internalFrame12.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll10.setName("Scroll10"); // NOI18N
        Scroll10.setOpaque(true);

        tbOperasi1.setName("tbOperasi1"); // NOI18N
        tbOperasi1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbOperasi1MouseClicked(evt);
            }
        });
        tbOperasi1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbOperasi1KeyPressed(evt);
            }
        });
        Scroll10.setViewportView(tbOperasi1);

        internalFrame12.add(Scroll10, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Petugas Ralan", internalFrame12);

        internalFrame7.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame7.setBorder(null);
        internalFrame7.setName("internalFrame7"); // NOI18N
        internalFrame7.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll4.setName("Scroll4"); // NOI18N
        Scroll4.setOpaque(true);

        tbResepRanap.setName("tbResepRanap"); // NOI18N
        tbResepRanap.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbResepRanapMouseClicked(evt);
            }
        });
        tbResepRanap.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbResepRanapKeyPressed(evt);
            }
        });
        Scroll4.setViewportView(tbResepRanap);

        internalFrame7.add(Scroll4, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Resep Ranap", internalFrame7);

        internalFrame13.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame13.setBorder(null);
        internalFrame13.setName("internalFrame13"); // NOI18N
        internalFrame13.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll11.setName("Scroll11"); // NOI18N
        Scroll11.setOpaque(true);

        tbPetugasResepRanap.setName("tbPetugasResepRanap"); // NOI18N
        tbPetugasResepRanap.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPetugasResepRanapMouseClicked(evt);
            }
        });
        tbPetugasResepRanap.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPetugasResepRanapKeyPressed(evt);
            }
        });
        Scroll11.setViewportView(tbPetugasResepRanap);

        internalFrame13.add(Scroll11, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Petugas Ranap", internalFrame13);

        internalFrame8.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame8.setBorder(null);
        internalFrame8.setName("internalFrame8"); // NOI18N
        internalFrame8.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll5.setName("Scroll5"); // NOI18N
        Scroll5.setOpaque(true);

        tbPermintaanStok.setName("tbPermintaanStok"); // NOI18N
        tbPermintaanStok.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPermintaanStokMouseClicked(evt);
            }
        });
        tbPermintaanStok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPermintaanStokKeyPressed(evt);
            }
        });
        Scroll5.setViewportView(tbPermintaanStok);

        internalFrame8.add(Scroll5, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Permintaan Stok", internalFrame8);

        internalFrame14.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame14.setBorder(null);
        internalFrame14.setName("internalFrame14"); // NOI18N
        internalFrame14.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll12.setName("Scroll12"); // NOI18N
        Scroll12.setOpaque(true);

        tbPetugasPermintaanStok.setName("tbPetugasPermintaanStok"); // NOI18N
        tbPetugasPermintaanStok.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPetugasPermintaanStokMouseClicked(evt);
            }
        });
        tbPetugasPermintaanStok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPetugasPermintaanStokKeyPressed(evt);
            }
        });
        Scroll12.setViewportView(tbPetugasPermintaanStok);

        internalFrame14.add(Scroll12, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Petugas Permintaan Stok", internalFrame14);

        internalFrame9.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame9.setBorder(null);
        internalFrame9.setName("internalFrame9"); // NOI18N
        internalFrame9.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll6.setName("Scroll6"); // NOI18N
        Scroll6.setOpaque(true);

        tbResepPulang.setName("tbResepPulang"); // NOI18N
        tbResepPulang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbResepPulangMouseClicked(evt);
            }
        });
        tbResepPulang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbResepPulangKeyPressed(evt);
            }
        });
        Scroll6.setViewportView(tbResepPulang);

        internalFrame9.add(Scroll6, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Resep Pulang", internalFrame9);

        internalFrame15.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame15.setBorder(null);
        internalFrame15.setName("internalFrame15"); // NOI18N
        internalFrame15.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll13.setName("Scroll13"); // NOI18N
        Scroll13.setOpaque(true);

        tbPetugasResepPulang.setName("tbPetugasResepPulang"); // NOI18N
        tbPetugasResepPulang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPetugasResepPulangMouseClicked(evt);
            }
        });
        tbPetugasResepPulang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPetugasResepPulangKeyPressed(evt);
            }
        });
        Scroll13.setViewportView(tbPetugasResepPulang);

        internalFrame15.add(Scroll13, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Petugas Resep Pulang", internalFrame15);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            dispose();
        }else{Valid.pindah(evt,Tgl1,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        switch (TabRawat.getSelectedIndex()) {
//            case 0:                
//                if(tabModeOperasi.getRowCount()==0){
//                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//                    TCari.requestFocus();
//                }else if(tabModeOperasi.getRowCount()!=0){
//                    Map<String, Object> param = new HashMap<>();
//                    param.put("namars",akses.getnamars());
//                    param.put("alamatrs",akses.getalamatrs());
//                    param.put("kotars",akses.getkabupatenrs());
//                    param.put("propinsirs",akses.getpropinsirs());
//                    param.put("kontakrs",akses.getkontakrs());
//                    param.put("emailrs",akses.getemailrs());
//                    param.put("tanggal1",Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
//                    param.put("tanggal2",Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
//                    param.put("cari","%"+TCari.getText().trim()+"%");                    
//                    param.put("logo",Sequel.cariGambar("select logo from setting"));
//                    Valid.MyReport("rptDetailTindakanOperasi.jasper",param,"::[ Detail Tindakan Operasi ]::");                    
//                }   break;
//            case 1:
//                if(tabModeOperasi2.getRowCount()==0){
//                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//                    TCari.requestFocus();
//                }else if(tabModeOperasi2.getRowCount()!=0){
//                    Map<String, Object> param = new HashMap<>();
//                    param.put("namars",akses.getnamars());
//                    param.put("alamatrs",akses.getalamatrs());
//                    param.put("kotars",akses.getkabupatenrs());
//                    param.put("propinsirs",akses.getpropinsirs());
//                    param.put("kontakrs",akses.getkontakrs());
//                    param.put("emailrs",akses.getemailrs());
//                    param.put("tanggal1",Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
//                    param.put("tanggal2",Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
//                    param.put("cari","%"+TCari.getText().trim()+"%");                    
//                    param.put("logo",Sequel.cariGambar("select logo from setting"));
//                    Valid.MyReport("rptDetailTindakanOperasi.jasper",param,"::[ Detail Tindakan Operasi ]::");                    
//                }  break;
//            default:
//                    break;
//        }
        this.setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_BtnPrintActionPerformed

    private void Tgl1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Tgl1KeyPressed
        Valid.pindah(evt, BtnKeluar, Tgl2);
    }//GEN-LAST:event_Tgl1KeyPressed

    private void Tgl2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Tgl2KeyPressed
        
    }//GEN-LAST:event_Tgl2KeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        switch (TabRawat.getSelectedIndex()) {
            case 0:
                tampil();
                break;
            case 1:
                tampil2();
                break;
            case 2:
                tampil3();
                break;
            case 3:
                tampil4();
                break;
            case 4:
                tampil5();
                break;
            case 5:
                tampil6();
                break;
            case 6:
                tampil7();
                break;
            case 7:
                tampil8();
                break;
            default:
                break;
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            TabRawatMouseClicked(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        TabRawatMouseClicked(null);
    }//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnKeluar);
        }
    }//GEN-LAST:event_BtnCariKeyPressed

    private void tbOperasiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbOperasiMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbOperasiMouseClicked

    private void tbOperasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbOperasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbOperasiKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        TabRawatMouseClicked(null);
    }//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnCari, TCari);
        }
    }//GEN-LAST:event_BtnAllKeyPressed

    private void tbOperasi1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbOperasi1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbOperasi1MouseClicked

    private void tbOperasi1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbOperasi1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbOperasi1KeyPressed

    private void BtnClosePetugasFarmasi1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnClosePetugasFarmasi1ActionPerformed
        // TODO add your handling code here:
        WindowEditPetugasFarmasi.dispose();
    }//GEN-LAST:event_BtnClosePetugasFarmasi1ActionPerformed

    private void BtnEditPetugasFarmasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditPetugasFarmasiActionPerformed
        // TODO add your handling code here:
        if(jLabel44.getText().equalsIgnoreCase("Resep Ralan") || jLabel44.getText().equalsIgnoreCase("Resep Ranap")){
            if(Sequel.mengedittf("petugas_farmasi","no_resep=?","petugas_entri=?,petugas_ambil=?,petugas_cek=?,petugas_racikan=?,petugas_serahkie=?,petugas_konseling=?",7,new String[]{
                KdEntri1.getText(),KdAmbil1.getText(),KdCek1.getText(),KdRacikan1.getText(),
                KdSerahKIE1.getText(),KdKonseling1.getText(),jLabel43.getText()
            })==true){
                //BERHASIL EDIT DATA PETUGAS FARMASI
                WindowEditPetugasFarmasi.dispose();
                TabRawatMouseClicked(null);
            }else{
                JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal diedit !");
            }            
        }else if(jLabel44.getText().equalsIgnoreCase("Permintaan Stok")){
            if(Sequel.mengedittf("petugas_farmasi_permintaan_stok_obat_pasien","no_permintaan=?","petugas_entri=?,petugas_ambil=?,petugas_cek=?,petugas_racikan=?,petugas_serahkie=?,petugas_konseling=?",7,new String[]{
                KdEntri1.getText(),KdAmbil1.getText(),KdCek1.getText(),KdRacikan1.getText(),
                KdSerahKIE1.getText(),KdKonseling1.getText(),jLabel43.getText()
            })==true){
                //BERHASIL EDIT DATA PETUGAS FARMASI
                WindowEditPetugasFarmasi.dispose();
                TabRawatMouseClicked(null);
            }else{
                JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal diedit !");
            }              
        }else if(jLabel44.getText().equalsIgnoreCase("Resep Pulang")){
            if(Sequel.mengedittf("petugas_farmasi_resep_pulang","no_permintaan=?","petugas_entri=?,petugas_ambil=?,petugas_cek=?,petugas_racikan=?,petugas_serahkie=?,petugas_konseling=?",7,new String[]{
                KdEntri1.getText(),KdAmbil1.getText(),KdCek1.getText(),KdRacikan1.getText(),
                KdSerahKIE1.getText(),KdKonseling1.getText(),jLabel43.getText()
            })==true){
                //BERHASIL EDIT DATA PETUGAS FARMASI
                WindowEditPetugasFarmasi.dispose();
                TabRawatMouseClicked(null);
            }else{
                JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal diedit !");
            }              
        }else{
            JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal diedit !");
        }
    }//GEN-LAST:event_BtnEditPetugasFarmasiActionPerformed

    private void BtnSeekEntri1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekEntri1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=7;
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekEntri1ActionPerformed

    private void BtnSeekKonseling1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekKonseling1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=12;
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekKonseling1ActionPerformed

    private void BtnSeekAmbil1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekAmbil1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=8;
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekAmbil1ActionPerformed

    private void BtnSeekCek1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekCek1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=9;
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekCek1ActionPerformed

    private void BtnSeekRacikan1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekRacikan1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=10;
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekRacikan1ActionPerformed

    private void BtnSeekSerahKIE1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekSerahKIE1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=11;
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekSerahKIE1ActionPerformed

    private void KdKonseling1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdKonseling1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdKonseling1ActionPerformed

    private void KdKonseling1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdKonseling1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdKonseling1KeyPressed

    private void KdEntri1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdEntri1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdEntri1KeyPressed

    private void KdAmbil1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdAmbil1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdAmbil1ActionPerformed

    private void KdAmbil1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdAmbil1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdAmbil1KeyPressed

    private void KdCek1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdCek1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdCek1ActionPerformed

    private void KdCek1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdCek1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdCek1KeyPressed

    private void KdRacikan1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdRacikan1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdRacikan1ActionPerformed

    private void KdRacikan1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdRacikan1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdRacikan1KeyPressed

    private void KdSerahKIE1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdSerahKIE1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdSerahKIE1ActionPerformed

    private void KdSerahKIE1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdSerahKIE1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdSerahKIE1KeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed
        // TODO add your handling code here:
        if(TabRawat.getSelectedIndex()==0){ //RESEP RALAN
            if(akses.getberi_obat()==true){
                if(tbOperasi.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                    TCari.requestFocus();
                }else if(tbOperasi.getValueAt(tbOperasi.getSelectedRow(),1).toString().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                }else{
                    //SET NILAI VARIABEL YG AKAN DIEDIT
                    //WAKTU OBAT JADI
                    jLabel36.setText(Sequel.cariIsi("select jam_penyerahan from resep_obat where no_resep=?",tbOperasi.getValueAt(tbOperasi.getSelectedRow(),0).toString()));
                    //NO RESEP
                    jLabel43.setText(tbOperasi.getValueAt(tbOperasi.getSelectedRow(),0).toString());
                    KdEntri1.setText(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbOperasi.getValueAt(tbOperasi.getSelectedRow(),0).toString()));
                    CrEntri1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri1.getText()));
                    //AMBIL PETUGAS DARI JADWAL FARMASI
                    KdAmbil1.setText(Sequel.cariIsi("select petugas_ambil from petugas_farmasi where no_resep=?",tbOperasi.getValueAt(tbOperasi.getSelectedRow(),0).toString()));
                    CrAmbil1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil1.getText()));
                    KdCek1.setText(Sequel.cariIsi("select petugas_cek from petugas_farmasi where no_resep=?",tbOperasi.getValueAt(tbOperasi.getSelectedRow(),0).toString()));
                    CrCek1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek1.getText()));
                    KdRacikan1.setText(Sequel.cariIsi("select petugas_racikan from petugas_farmasi where no_resep=?",tbOperasi.getValueAt(tbOperasi.getSelectedRow(),0).toString()));
                    CrRacikan1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan1.getText()));
                    KdSerahKIE1.setText(Sequel.cariIsi("select petugas_serahkie from petugas_farmasi where no_resep=?",tbOperasi.getValueAt(tbOperasi.getSelectedRow(),0).toString()));
                    CrSerahKIE1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE1.getText()));
                    KdKonseling1.setText(Sequel.cariIsi("select petugas_konseling from petugas_farmasi where no_resep=?",tbOperasi.getValueAt(tbOperasi.getSelectedRow(),0).toString()));
                    CrKonseling1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling1.getText()));
                    //SET JUDUL
                    jLabel44.setText("Resep Ralan");

                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    WindowEditPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                    WindowEditPetugasFarmasi.setVisible(true);
                    this.setCursor(Cursor.getDefaultCursor());
                }
            }else{
                JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                TCari.requestFocus();
            }
        }else if(TabRawat.getSelectedIndex()==2){   //RESEP RANAP
            if(akses.getberi_obat()==true){
                if(tbResepRanap.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                    TCari.requestFocus();
                }else if(tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),1).toString().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                }else{
                    //SET NILAI VARIABEL YG AKAN DIEDIT
                    //WAKTU OBAT JADI
                    jLabel36.setText(Sequel.cariIsi("select jam_penyerahan from resep_obat where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                    //NO RESEP
                    jLabel43.setText(tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString());
                    KdEntri1.setText(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                    CrEntri1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri1.getText()));
                    //AMBIL PETUGAS DARI JADWAL FARMASI
                    KdAmbil1.setText(Sequel.cariIsi("select petugas_ambil from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                    CrAmbil1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil1.getText()));
                    KdCek1.setText(Sequel.cariIsi("select petugas_cek from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                    CrCek1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek1.getText()));
                    KdRacikan1.setText(Sequel.cariIsi("select petugas_racikan from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                    CrRacikan1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan1.getText()));
                    KdSerahKIE1.setText(Sequel.cariIsi("select petugas_serahkie from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                    CrSerahKIE1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE1.getText()));
                    KdKonseling1.setText(Sequel.cariIsi("select petugas_konseling from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                    CrKonseling1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling1.getText()));
                    //SET JUDUL
                    jLabel44.setText("Resep Ranap");

                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    WindowEditPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                    WindowEditPetugasFarmasi.setVisible(true);
                    this.setCursor(Cursor.getDefaultCursor());
                }
            }else{
                JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                TCari.requestFocus();
            }            
        }else if(TabRawat.getSelectedIndex()==4){   //PERMINTAAN STOK
            if(akses.getberi_obat()==true){
                if(tbPermintaanStok.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                    TCari.requestFocus();
                }else if(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),1).toString().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                }else{
                    //SET NILAI VARIABEL YG AKAN DIEDIT
                    //WAKTU OBAT JADI
                    jLabel36.setText(Sequel.cariIsi("select jam_penyerahan from permintaan_stok_obat_pasien_penyerahan where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                    //NO RESEP
                    jLabel43.setText(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString());
                    KdEntri1.setText(Sequel.cariIsi("select petugas_entri from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                    CrEntri1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri1.getText()));
                    //AMBIL PETUGAS DARI JADWAL FARMASI
                    KdAmbil1.setText(Sequel.cariIsi("select petugas_ambil from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                    CrAmbil1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil1.getText()));
                    KdCek1.setText(Sequel.cariIsi("select petugas_cek from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                    CrCek1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek1.getText()));
                    KdRacikan1.setText(Sequel.cariIsi("select petugas_racikan from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                    CrRacikan1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan1.getText()));
                    KdSerahKIE1.setText(Sequel.cariIsi("select petugas_serahkie from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                    CrSerahKIE1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE1.getText()));
                    KdKonseling1.setText(Sequel.cariIsi("select petugas_konseling from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                    CrKonseling1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling1.getText()));
                    //SET JUDUL
                    jLabel44.setText("Permintaan Stok");

                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    WindowEditPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                    WindowEditPetugasFarmasi.setVisible(true);
                    this.setCursor(Cursor.getDefaultCursor());
                }
            }else{
                JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                TCari.requestFocus();
            }            
        }else if(TabRawat.getSelectedIndex()==6){   //RESEP PULANG
            if(akses.getberi_obat()==true){
                if(tbResepPulang.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                    TCari.requestFocus();
                }else if(tbResepPulang.getValueAt(tbResepPulang.getSelectedRow(),1).toString().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                }else{
                    //SET NILAI VARIABEL YG AKAN DIEDIT
                    //WAKTU OBAT JADI
                    jLabel36.setText(Sequel.cariIsi("select jam_penyerahan from permintaan_resep_pulang_penyerahan where no_permintaan=?",tbResepPulang.getValueAt(tbResepPulang.getSelectedRow(),0).toString()));
                    //NO RESEP
                    jLabel43.setText(tbResepPulang.getValueAt(tbResepPulang.getSelectedRow(),0).toString());
                    KdEntri1.setText(Sequel.cariIsi("select petugas_entri from petugas_farmasi_resep_pulang where no_permintaan=?",tbResepPulang.getValueAt(tbResepPulang.getSelectedRow(),0).toString()));
                    CrEntri1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri1.getText()));
                    //AMBIL PETUGAS DARI JADWAL FARMASI
                    KdAmbil1.setText(Sequel.cariIsi("select petugas_ambil from petugas_farmasi_resep_pulang where no_permintaan=?",tbResepPulang.getValueAt(tbResepPulang.getSelectedRow(),0).toString()));
                    CrAmbil1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil1.getText()));
                    KdCek1.setText(Sequel.cariIsi("select petugas_cek from petugas_farmasi_resep_pulang where no_permintaan=?",tbResepPulang.getValueAt(tbResepPulang.getSelectedRow(),0).toString()));
                    CrCek1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek1.getText()));
                    KdRacikan1.setText(Sequel.cariIsi("select petugas_racikan from petugas_farmasi_resep_pulang where no_permintaan=?",tbResepPulang.getValueAt(tbResepPulang.getSelectedRow(),0).toString()));
                    CrRacikan1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan1.getText()));
                    KdSerahKIE1.setText(Sequel.cariIsi("select petugas_serahkie from petugas_farmasi_resep_pulang where no_permintaan=?",tbResepPulang.getValueAt(tbResepPulang.getSelectedRow(),0).toString()));
                    CrSerahKIE1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE1.getText()));
                    KdKonseling1.setText(Sequel.cariIsi("select petugas_konseling from petugas_farmasi_resep_pulang where no_permintaan=?",tbResepPulang.getValueAt(tbResepPulang.getSelectedRow(),0).toString()));
                    CrKonseling1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling1.getText()));
                    //SET JUDUL
                    jLabel44.setText("Resep Pulang");

                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    WindowEditPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                    WindowEditPetugasFarmasi.setVisible(true);
                    this.setCursor(Cursor.getDefaultCursor());
                }
            }else{
                JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                TCari.requestFocus();
            }            
        }else {
            JOptionPane.showMessageDialog(null,"Maaf, buka tab resep untuk edit petugas..!!!!");
            TCari.requestFocus();
        }
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void tbResepRanapMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbResepRanapMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbResepRanapMouseClicked

    private void tbResepRanapKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbResepRanapKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbResepRanapKeyPressed

    private void tbPetugasResepRanapMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPetugasResepRanapMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbPetugasResepRanapMouseClicked

    private void tbPetugasResepRanapKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPetugasResepRanapKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbPetugasResepRanapKeyPressed

    private void tbPermintaanStokMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPermintaanStokMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbPermintaanStokMouseClicked

    private void tbPermintaanStokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPermintaanStokKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbPermintaanStokKeyPressed

    private void tbPetugasPermintaanStokMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPetugasPermintaanStokMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbPetugasPermintaanStokMouseClicked

    private void tbPetugasPermintaanStokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPetugasPermintaanStokKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbPetugasPermintaanStokKeyPressed

    private void tbPetugasResepPulangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPetugasResepPulangMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbPetugasResepPulangMouseClicked

    private void tbPetugasResepPulangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPetugasResepPulangKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbPetugasResepPulangKeyPressed

    private void tbResepPulangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbResepPulangMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbResepPulangMouseClicked

    private void tbResepPulangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbResepPulangKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbResepPulangKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            DlgDetailVKOK dialog = new DlgDetailVKOK(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnCari;
    private widget.Button BtnClosePetugasFarmasi1;
    private widget.Button BtnEditPetugasFarmasi;
    private widget.Button BtnKeluar;
    private widget.Button BtnPetugas;
    private widget.Button BtnPrint;
    private widget.Button BtnSeekAmbil1;
    private widget.Button BtnSeekCek1;
    private widget.Button BtnSeekEntri1;
    private widget.Button BtnSeekKonseling1;
    private widget.Button BtnSeekRacikan1;
    private widget.Button BtnSeekSerahKIE1;
    private widget.TextBox CrAmbil1;
    private widget.TextBox CrCek1;
    private widget.TextBox CrEntri1;
    private widget.TextBox CrKonseling1;
    private widget.TextBox CrRacikan1;
    private widget.TextBox CrSerahKIE1;
    private widget.TextBox KdAmbil1;
    private widget.TextBox KdCek1;
    private widget.TextBox KdEntri1;
    private widget.TextBox KdKonseling1;
    private widget.TextBox KdRacikan1;
    private widget.TextBox KdSerahKIE1;
    private widget.ScrollPane Scroll10;
    private widget.ScrollPane Scroll11;
    private widget.ScrollPane Scroll12;
    private widget.ScrollPane Scroll13;
    private widget.ScrollPane Scroll3;
    private widget.ScrollPane Scroll4;
    private widget.ScrollPane Scroll5;
    private widget.ScrollPane Scroll6;
    private widget.TextBox TCari;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal Tgl1;
    private widget.Tanggal Tgl2;
    private javax.swing.JDialog WindowEditPetugasFarmasi;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame12;
    private widget.InternalFrame internalFrame13;
    private widget.InternalFrame internalFrame14;
    private widget.InternalFrame internalFrame15;
    private widget.InternalFrame internalFrame5;
    private widget.InternalFrame internalFrame6;
    private widget.InternalFrame internalFrame7;
    private widget.InternalFrame internalFrame8;
    private widget.InternalFrame internalFrame9;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label label10;
    private widget.Label label11;
    private widget.Label label18;
    private widget.Label label9;
    private widget.panelisi panelGlass5;
    private widget.Table tbOperasi;
    private widget.Table tbOperasi1;
    private widget.Table tbPermintaanStok;
    private widget.Table tbPetugasPermintaanStok;
    private widget.Table tbPetugasResepPulang;
    private widget.Table tbPetugasResepRanap;
    private widget.Table tbResepPulang;
    private widget.Table tbResepRanap;
    // End of variables declaration//GEN-END:variables
    
    public void tampil(){     
        Valid.tabelKosong(tabModeOperasi);
        try{
            pstindakan=koneksi.prepareStatement(
                    "select petugas_farmasi.no_resep,rp.no_rkm_medis,p.nm_pasien," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_entri) as petugas_entri," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_ambil) as petugas_ambil," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_cek) as petugas_cek," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_racikan) as petugas_racikan," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_serahkie) as petugas_serahkie," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_konseling) as petugas_konseling," +
                    "if(rok.obat_kronis='0','-',(select nama from petugas where petugas.nip=petugas_farmasi.petugas_entri)) as petugas_obat_kronis "+
                    "from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep " +
                    "inner join reg_periksa rp on ro.no_rawat = rp.no_rawat " +
                    "inner join pasien p on rp.no_rkm_medis = p.no_rkm_medis "+ 
                    "left join resep_obat_kronis rok on ro.no_resep = rok.no_resep "+        
                    "where tgl_peresepan between ? and ? and ro.status='ralan' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_entri) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ralan' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_ambil) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ralan' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_cek) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ralan' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_racikan) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ralan' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_serahkie) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ralan' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_konseling) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ralan' and petugas_farmasi.no_resep LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ralan' and rp.no_rkm_medis LIKE ? ");
            try {
                pstindakan.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(3,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(4,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(5,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(6,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(7,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(8,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(9,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(10,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(11,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(12,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(13,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(14,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(15,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(16,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(17,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(18,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(19,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(20,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(21,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(22,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(23,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(24,"%"+TCari.getText().trim()+"%");
                rstindakan=pstindakan.executeQuery();
                while(rstindakan.next()){
                    tabModeOperasi.addRow(new Object[]{
                        rstindakan.getString("no_resep"),rstindakan.getString("no_rkm_medis"),rstindakan.getString("nm_pasien"),
                        rstindakan.getString("petugas_entri"),
                        rstindakan.getString("petugas_ambil"),rstindakan.getString("petugas_cek"),
                        rstindakan.getString("petugas_racikan"),rstindakan.getString("petugas_serahkie"),
                        rstindakan.getString("petugas_konseling"),
                        rstindakan.getString("petugas_obat_kronis")
                    });
                     
                }
            } catch (Exception e) {
                System.out.println("keuangan.DlgDetailFarmasi.tampil() 1 : "+e);
            } finally{
                if(rstindakan!=null){
                    rstindakan.close();
                }
                if(pstindakan!=null){
                    pstindakan.close();
                }
            }           
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
    
    public void tampil2(){     
        Valid.tabelKosong(tabModeOperasi2);
        try{
            pstindakan=koneksi.prepareStatement(
                    "select nip,nama from petugas " +
                    "where kd_jbtn='J008' and nama like ? " +
                    "or kd_jbtn='J007' and nama like ? " +
                    "or nip='197606292007101001' and nama like ? " +
                    "order by nama asc");
            try {
                pstindakan.setString(1,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(2,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(3,"%"+TCari.getText().trim()+"%");
                rstindakan=pstindakan.executeQuery();
                while(rstindakan.next()){
                    tabModeOperasi2.addRow(new Object[]{
                        rstindakan.getString("nama"),
                        Sequel.cariInteger("select count(petugas_entri) as jml_entri from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_entri='"+rstindakan.getString("nip")+"' and ro.status='ralan' "),
                        Sequel.cariInteger("select count(petugas_ambil) as jml_ambil from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_ambil='"+rstindakan.getString("nip")+"' and ro.status='ralan'"),
                        Sequel.cariInteger("select count(petugas_cek) as jml_cek from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_cek='"+rstindakan.getString("nip")+"' and ro.status='ralan'"),
                        Sequel.cariInteger("select count(petugas_racikan) as jml_racikan from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_racikan='"+rstindakan.getString("nip")+"' and ro.status='ralan'"),
                        Sequel.cariInteger("select count(petugas_serahkie) as jml_serahkie from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_serahkie='"+rstindakan.getString("nip")+"' and ro.status='ralan'"),
                        Sequel.cariInteger("select count(petugas_konseling) as jml_konseling from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_konseling='"+rstindakan.getString("nip")+"' and ro.status='ralan'"),
                        Sequel.cariInteger("select count(petugas_entri) as jml_entri from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep inner join resep_obat_kronis rok on ro.no_resep = rok.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_entri='"+rstindakan.getString("nip")+"' and rok.obat_kronis <> '0'")  //PETUGAS CENTANG OBAT KRONIS SAMA DENGAN ENTRI
                    });
                }
            } catch (Exception e) {
                System.out.println("keuangan.DlgDetailFarmasi.tampil() 2 : "+e);
            } finally{
                if(rstindakan!=null){
                    rstindakan.close();
                }
                if(pstindakan!=null){
                    pstindakan.close();
                }
            }           
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
    
    public void tampil3(){     
        Valid.tabelKosong(tabModeResepRanap);
        try{
            pstindakan=koneksi.prepareStatement(
                    "select petugas_farmasi.no_resep,rp.no_rkm_medis,p.nm_pasien," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_entri) as petugas_entri," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_ambil) as petugas_ambil," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_cek) as petugas_cek," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_racikan) as petugas_racikan," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_serahkie) as petugas_serahkie," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi.petugas_konseling) as petugas_konseling," +
                    "if(rok.obat_kronis='0','-',(select nama from petugas where petugas.nip=petugas_farmasi.petugas_entri)) as petugas_obat_kronis "+
                    ",ro.status "+
                    "from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep " +
                    "inner join reg_periksa rp on ro.no_rawat = rp.no_rawat " +
                    "inner join pasien p on rp.no_rkm_medis = p.no_rkm_medis "+ 
                    "left join resep_obat_kronis rok on ro.no_resep = rok.no_resep "+        
                    "where tgl_peresepan between ? and ? and ro.status='ranap' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_entri) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ranap' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_ambil) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ranap' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_cek) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ranap' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_racikan) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ranap' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_serahkie) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ranap' and (select nama from petugas where petugas.nip=petugas_farmasi.petugas_konseling) LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ranap' and petugas_farmasi.no_resep LIKE ? " +
                    "  or tgl_peresepan between ? and ? and ro.status='ranap' and rp.no_rkm_medis LIKE ? ");
            try {
                pstindakan.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(3,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(4,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(5,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(6,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(7,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(8,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(9,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(10,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(11,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(12,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(13,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(14,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(15,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(16,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(17,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(18,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(19,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(20,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(21,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(22,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(23,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(24,"%"+TCari.getText().trim()+"%");
                rstindakan=pstindakan.executeQuery();
                while(rstindakan.next()){
                    tabModeResepRanap.addRow(new Object[]{
                        rstindakan.getString("no_resep"),rstindakan.getString("no_rkm_medis"),rstindakan.getString("nm_pasien"),
                        rstindakan.getString("petugas_entri"),
                        rstindakan.getString("petugas_ambil"),rstindakan.getString("petugas_cek"),
                        rstindakan.getString("petugas_racikan"),rstindakan.getString("petugas_serahkie"),
                        rstindakan.getString("petugas_konseling"),
                        rstindakan.getString("petugas_obat_kronis")
                    });      
                }
            } catch (Exception e) {
                System.out.println("keuangan.DlgDetailFarmasi.tampil() 3 : "+e);
            } finally{
                if(rstindakan!=null){
                    rstindakan.close();
                }
                if(pstindakan!=null){
                    pstindakan.close();
                }
            }           
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    } 
    
    public void tampil4(){     
        Valid.tabelKosong(tabModePetugasResepRanap);
        try{
            pstindakan=koneksi.prepareStatement(
                    "select nip,nama from petugas " +
                    "where kd_jbtn='J008' and nama like ? " +
                    "or kd_jbtn='J007' and nama like ? " +
                    "or nip='197606292007101001' and nama like ? " +
                    "order by nama asc");
            try {
                pstindakan.setString(1,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(2,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(3,"%"+TCari.getText().trim()+"%");
                rstindakan=pstindakan.executeQuery();
                while(rstindakan.next()){
                    tabModePetugasResepRanap.addRow(new Object[]{
                        rstindakan.getString("nama"),
                        Sequel.cariInteger("select count(petugas_entri) as jml_entri from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_entri='"+rstindakan.getString("nip")+"' and ro.status='ranap' "),
                        Sequel.cariInteger("select count(petugas_ambil) as jml_ambil from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_ambil='"+rstindakan.getString("nip")+"' and ro.status='ranap' "),
                        Sequel.cariInteger("select count(petugas_cek) as jml_cek from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_cek='"+rstindakan.getString("nip")+"' and ro.status='ranap' "),
                        Sequel.cariInteger("select count(petugas_racikan) as jml_racikan from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_racikan='"+rstindakan.getString("nip")+"' and ro.status='ranap' "),
                        Sequel.cariInteger("select count(petugas_serahkie) as jml_serahkie from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_serahkie='"+rstindakan.getString("nip")+"' and ro.status='ranap' "),
                        Sequel.cariInteger("select count(petugas_konseling) as jml_konseling from petugas_farmasi inner join resep_obat ro on petugas_farmasi.no_resep = ro.no_resep where tgl_peresepan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_konseling='"+rstindakan.getString("nip")+"' and ro.status='ranap' ")
                    });
                }
            } catch (Exception e) {
                System.out.println("keuangan.DlgDetailFarmasi.tampil() 4 : "+e);
            } finally{
                if(rstindakan!=null){
                    rstindakan.close();
                }
                if(pstindakan!=null){
                    pstindakan.close();
                }
            }           
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }    
    
    public void tampil5(){     
        Valid.tabelKosong(tabModePermintaanStok);
        try{
            pstindakan=koneksi.prepareStatement(
                    "select petugas_farmasi_permintaan_stok_obat_pasien.no_permintaan,rp.no_rkm_medis,p.nm_pasien," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_entri) as petugas_entri," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_ambil) as petugas_ambil," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_cek) as petugas_cek," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_racikan) as petugas_racikan," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_serahkie) as petugas_serahkie," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_konseling) as petugas_konseling," +
                    "ro.status "+
                    "from petugas_farmasi_permintaan_stok_obat_pasien inner join permintaan_stok_obat_pasien ro on petugas_farmasi_permintaan_stok_obat_pasien.no_permintaan = ro.no_permintaan " +
                    "inner join reg_periksa rp on ro.no_rawat = rp.no_rawat " +
                    "inner join pasien p on rp.no_rkm_medis = p.no_rkm_medis "+        
                    "where tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_entri) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_ambil) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_cek) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_racikan) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_serahkie) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_permintaan_stok_obat_pasien.petugas_konseling) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and petugas_farmasi_permintaan_stok_obat_pasien.no_permintaan LIKE ? " +
                    "  or tgl_permintaan between ? and ? and rp.no_rkm_medis LIKE ? ");
            try {
                pstindakan.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(3,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(4,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(5,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(6,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(7,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(8,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(9,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(10,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(11,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(12,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(13,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(14,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(15,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(16,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(17,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(18,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(19,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(20,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(21,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(22,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(23,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(24,"%"+TCari.getText().trim()+"%");
                rstindakan=pstindakan.executeQuery();
                while(rstindakan.next()){
                    tabModePermintaanStok.addRow(new Object[]{
                        rstindakan.getString("no_permintaan"),rstindakan.getString("no_rkm_medis"),rstindakan.getString("nm_pasien"),
                        rstindakan.getString("petugas_entri"),
                        rstindakan.getString("petugas_ambil"),rstindakan.getString("petugas_cek"),
                        rstindakan.getString("petugas_racikan"),rstindakan.getString("petugas_serahkie"),
                        rstindakan.getString("petugas_konseling")
                    });      
                }
            } catch (Exception e) {
                System.out.println("keuangan.DlgDetailFarmasi.tampil() 5 : "+e);
            } finally{
                if(rstindakan!=null){
                    rstindakan.close();
                }
                if(pstindakan!=null){
                    pstindakan.close();
                }
            }           
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    } 
    
    public void tampil6(){     
        Valid.tabelKosong(tabModePetugasPermintaanStok);
        try{
            pstindakan=koneksi.prepareStatement(
                    "select nip,nama from petugas " +
                    "where kd_jbtn='J008' and nama like ? " +
                    "or kd_jbtn='J007' and nama like ? " +
                    "or nip='197606292007101001' and nama like ? " +
                    "order by nama asc");
            try {
                pstindakan.setString(1,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(2,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(3,"%"+TCari.getText().trim()+"%");
                rstindakan=pstindakan.executeQuery();
                while(rstindakan.next()){
                    tabModePetugasPermintaanStok.addRow(new Object[]{
                        rstindakan.getString("nama"),
                        Sequel.cariInteger("select count(petugas_entri) as jml_entri from petugas_farmasi_permintaan_stok_obat_pasien inner join permintaan_stok_obat_pasien ro on petugas_farmasi_permintaan_stok_obat_pasien.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_entri='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_ambil) as jml_ambil from petugas_farmasi_permintaan_stok_obat_pasien inner join permintaan_stok_obat_pasien ro on petugas_farmasi_permintaan_stok_obat_pasien.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_ambil='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_cek) as jml_cek from petugas_farmasi_permintaan_stok_obat_pasien inner join permintaan_stok_obat_pasien ro on petugas_farmasi_permintaan_stok_obat_pasien.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_cek='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_racikan) as jml_racikan from petugas_farmasi_permintaan_stok_obat_pasien inner join permintaan_stok_obat_pasien ro on petugas_farmasi_permintaan_stok_obat_pasien.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_racikan='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_serahkie) as jml_serahkie from petugas_farmasi_permintaan_stok_obat_pasien inner join permintaan_stok_obat_pasien ro on petugas_farmasi_permintaan_stok_obat_pasien.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_serahkie='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_konseling) as jml_konseling from petugas_farmasi_permintaan_stok_obat_pasien inner join permintaan_stok_obat_pasien ro on petugas_farmasi_permintaan_stok_obat_pasien.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_konseling='"+rstindakan.getString("nip")+"'")
                    });
                }
            } catch (Exception e) {
                System.out.println("keuangan.DlgDetailFarmasi.tampil() 6 : "+e);
            } finally{
                if(rstindakan!=null){
                    rstindakan.close();
                }
                if(pstindakan!=null){
                    pstindakan.close();
                }
            }           
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }    

    public void tampil7(){     
        Valid.tabelKosong(tabModeResepPulang);
        try{
            pstindakan=koneksi.prepareStatement(
                    "select petugas_farmasi_resep_pulang.no_permintaan,rp.no_rkm_medis,p.nm_pasien," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_entri) as petugas_entri," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_ambil) as petugas_ambil," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_cek) as petugas_cek," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_racikan) as petugas_racikan," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_serahkie) as petugas_serahkie," +
                    "(select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_konseling) as petugas_konseling," +
                    "ro.status "+
                    "from petugas_farmasi_resep_pulang inner join permintaan_resep_pulang ro on petugas_farmasi_resep_pulang.no_permintaan = ro.no_permintaan " +
                    "inner join reg_periksa rp on ro.no_rawat = rp.no_rawat " +
                    "inner join pasien p on rp.no_rkm_medis = p.no_rkm_medis "+        
                    "where tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_entri) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_ambil) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_cek) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_racikan) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_serahkie) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and (select nama from petugas where petugas.nip=petugas_farmasi_resep_pulang.petugas_konseling) LIKE ? " +
                    "  or tgl_permintaan between ? and ? and petugas_farmasi_resep_pulang.no_permintaan LIKE ? " +
                    "  or tgl_permintaan between ? and ? and rp.no_rkm_medis LIKE ? ");
            try {
                pstindakan.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(3,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(4,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(5,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(6,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(7,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(8,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(9,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(10,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(11,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(12,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(13,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(14,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(15,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(16,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(17,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(18,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(19,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(20,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(21,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(22,Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00");
                pstindakan.setString(23,Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59");
                pstindakan.setString(24,"%"+TCari.getText().trim()+"%");
                rstindakan=pstindakan.executeQuery();
                while(rstindakan.next()){
                    tabModeResepPulang.addRow(new Object[]{
                        rstindakan.getString("no_permintaan"),rstindakan.getString("no_rkm_medis"),rstindakan.getString("nm_pasien"),
                        rstindakan.getString("petugas_entri"),
                        rstindakan.getString("petugas_ambil"),rstindakan.getString("petugas_cek"),
                        rstindakan.getString("petugas_racikan"),rstindakan.getString("petugas_serahkie"),
                        rstindakan.getString("petugas_konseling")
                    });      
                }
            } catch (Exception e) {
                System.out.println("keuangan.DlgDetailFarmasi.tampil() 7 : "+e);
            } finally{
                if(rstindakan!=null){
                    rstindakan.close();
                }
                if(pstindakan!=null){
                    pstindakan.close();
                }
            }           
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    } 
    
    public void tampil8(){     
        Valid.tabelKosong(tabModePetugasResepPulang);
        try{
            pstindakan=koneksi.prepareStatement(
                    "select nip,nama from petugas " +
                    "where kd_jbtn='J008' and nama like ? " +
                    "or kd_jbtn='J007' and nama like ? " +
                    "or nip='197606292007101001' and nama like ? " +
                    "order by nama asc");
            try {
                pstindakan.setString(1,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(2,"%"+TCari.getText().trim()+"%");
                pstindakan.setString(3,"%"+TCari.getText().trim()+"%");
                rstindakan=pstindakan.executeQuery();
                while(rstindakan.next()){
                    tabModePetugasResepPulang.addRow(new Object[]{
                        rstindakan.getString("nama"),
                        Sequel.cariInteger("select count(petugas_entri) as jml_entri from petugas_farmasi_resep_pulang inner join permintaan_resep_pulang ro on petugas_farmasi_resep_pulang.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_entri='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_ambil) as jml_ambil from petugas_farmasi_resep_pulang inner join permintaan_resep_pulang ro on petugas_farmasi_resep_pulang.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_ambil='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_cek) as jml_cek from petugas_farmasi_resep_pulang inner join permintaan_resep_pulang ro on petugas_farmasi_resep_pulang.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_cek='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_racikan) as jml_racikan from petugas_farmasi_resep_pulang inner join permintaan_resep_pulang ro on petugas_farmasi_resep_pulang.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_racikan='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_serahkie) as jml_serahkie from petugas_farmasi_resep_pulang inner join permintaan_resep_pulang ro on petugas_farmasi_resep_pulang.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_serahkie='"+rstindakan.getString("nip")+"'"),
                        Sequel.cariInteger("select count(petugas_konseling) as jml_konseling from petugas_farmasi_resep_pulang inner join permintaan_resep_pulang ro on petugas_farmasi_resep_pulang.no_permintaan = ro.no_permintaan where tgl_permintaan between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+" 00:00:00"+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+" 23:59:59"+"' and petugas_konseling='"+rstindakan.getString("nip")+"'")
                    });
                }
            } catch (Exception e) {
                System.out.println("keuangan.DlgDetailFarmasi.tampil() 8 : "+e);
            } finally{
                if(rstindakan!=null){
                    rstindakan.close();
                }
                if(pstindakan!=null){
                    pstindakan.close();
                }
            }           
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }
    
}
