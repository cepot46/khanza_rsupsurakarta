package inventory;
import fungsi.BackgroundMusic;
import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariDokter;
import simrskhanza.DlgCariBangsal;
import simrskhanza.DlgCariPoli;
import simrskhanza.DlgInputResepPulang;
//START CUSTOM MUHSIN
import kepegawaian.DlgCariPetugas;
import fungsi.WarnaTableValidasi;  
import fungsi.WarnaTableStokRanap;   
import bridging.BPJSCekHistoriPelayanan;    
import java.time.LocalDate; 
//END CUSTOM

public class DlgDaftarPermintaanResep extends javax.swing.JDialog {
    private final DefaultTableModel tabMode,tabMode2,tabMode3,tabMode4,tabMode5,tabMode6,tabMode7,tabMode8;
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private Connection koneksi=koneksiDB.condb();
    private PreparedStatement ps,ps2,ps3;
    private ResultSet rs,rs2,rs3;
    private DlgCariObat dlgobt=new DlgCariObat(null,false);
    private DlgCariObat2 dlgobt2=new DlgCariObat2(null,false);
    private DlgInputStokPasien dlgstok=new DlgInputStokPasien(null,false);
    private DlgInputResepPulang dlgresepulang=new DlgInputResepPulang(null,false);
    private String bangsal="",aktifkanparsial="no",kamar="",alarm="",
            formalarm="",nol_detik,detik,NoResep="",TglPeresepan="",JamPeresepan="",
            NoRawat="",NoRM="",Pasien="",DokterPeresep="",Status="",KodeDokter="",Ruang="",KodeRuang="";
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariPoli poli=new DlgCariPoli(null,false);
    private DlgCariBangsal ruang=new DlgCariBangsal(null,false);
    private int jmlparsial=0,nilai_detik,resepbaru=0,i=0;
    private BackgroundMusic music;
    private boolean aktif=false,semua;
    //START CUSTOM MUHSIN
    private int jam_jadi,menit_jadi,detik_jadi; 
    private PreparedStatement pscatatan;   
    private ResultSet rscatatan;    
    private String racik="",waktu="";    
    public  DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    int temp_ptg=0;
    private final DefaultTableModel tabModeObatKronis;
    private int jml=0,index=0;    
    private boolean[] pilih;    
    private String[] riw_obatkronis,jumlah_obat,signa,tgl_reg,nama_poli,nama_dokter;       
    //END CUSTOM
    
    /** Creates new form 
     * @param parent
     * @param modal */
    public DlgDaftarPermintaanResep(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
                "No.Resep","Tgl.Peresepan","Jam Peresepan","No.Rawat","No.RM","Pasien","Dokter Peresep",  
                "Status","Kode Dokter","Poli/Unit","Kode Poli","Jenis Bayar","Tgl.Validasi","Jam Validasi",
                "Tgl. Obat Jadi","Jam Obat Jadi","No.Urut","Keterangan" //CUSTOM MUHSIN TAMBAH NO URUT & KETERANGAN RACIKAN/NON-RACIKAN
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbResepRalan.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbResepRalan.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbResepRalan.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i <18; i++) {   //START CUSTOM MUHSIN -> TAMBAH 4
            TableColumn column = tbResepRalan.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(75);
            }else if(i==1){
                column.setPreferredWidth(80);
            }else if(i==2){
                column.setPreferredWidth(85);
            }else if(i==3){
                column.setPreferredWidth(105);
            }else if(i==4){
                column.setPreferredWidth(70);
            }else if(i==5){
                column.setPreferredWidth(190);
            }else if(i==6){
                column.setPreferredWidth(190);
            }else if(i==7){
                column.setPreferredWidth(100);
            }else if(i==8){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==9){ //POLI
                column.setPreferredWidth(140);
            }else if(i==10){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==11){    //PENJAB
                column.setPreferredWidth(120);
            }else if(i==12){
                column.setPreferredWidth(65);
            }else if(i==13){
                column.setPreferredWidth(70);
            }else if(i==14){
                column.setPreferredWidth(85);
            }else if(i==15){
                column.setPreferredWidth(90);
            }else if(i==16){    //NO URUT
                column.setPreferredWidth(50);
                //column.setMinWidth(0);
                //column.setMaxWidth(0);
            }else if(i==17){
                column.setPreferredWidth(100);
            }//END CUSTOM
        }
        tbResepRalan.setDefaultRenderer(Object.class, new WarnaTableValidasi());    //CUSTOM MUHSIN
        
        tabMode2=new DefaultTableModel(null,new Object[]{
                "No.Resep","Tgl.Resep","Poli/Unit","Status","Pasien","Dokter Peresep"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbDetailResepRalan.setModel(tabMode2);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbDetailResepRalan.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbDetailResepRalan.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 6; i++) {
            TableColumn column = tbDetailResepRalan.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(75);
            }else if(i==1){
                column.setPreferredWidth(110);
            }else if(i==2){
                column.setPreferredWidth(140);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(350);
            }else if(i==5){
                column.setPreferredWidth(190);
            }
        }
        tbDetailResepRalan.setDefaultRenderer(Object.class, new WarnaTable());
        
        //START CUSTOM -> TAB RESEP RANAP TAMBAH KOLOM UNTUK PENYERAHAN RESEP/RESEP JADI
        tabMode3=new DefaultTableModel(null,new Object[]{
                "No.Resep","Tgl.Peresepan","Jam Peresepan","No.Rawat","No.RM","Pasien","Dokter Peresep",
                "Status","Kode Dokter","Ruang/Kamar","Kode Bangsal","Jenis Bayar","Tgl.Validasi","Jam Validasi"
                ,"Tgl. Obat Jadi","Jam Obat Jadi"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbResepRanap.setModel(tabMode3);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbResepRanap.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbResepRanap.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i <16; i++) {   //14 -> 16
            TableColumn column = tbResepRanap.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(75);
            }else if(i==1){
                column.setPreferredWidth(80);
            }else if(i==2){
                column.setPreferredWidth(85);
            }else if(i==3){
                column.setPreferredWidth(105);
            }else if(i==4){
                column.setPreferredWidth(70);
            }else if(i==5){
                column.setPreferredWidth(190);
            }else if(i==6){
                column.setPreferredWidth(190);
            }else if(i==7){
                column.setPreferredWidth(100);
            }else if(i==8){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==9){
                column.setPreferredWidth(140);
            }else if(i==10){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==11){
                column.setPreferredWidth(120);
            }else if(i==12){
                column.setPreferredWidth(65);
            }else if(i==13){
                column.setPreferredWidth(70);
            }else if(i==14){
                column.setPreferredWidth(75);
            }else if(i==15){
                column.setPreferredWidth(80);
            }
        }
        tbResepRanap.setDefaultRenderer(Object.class, new WarnaTable());
        
        //INISIASI TAB RIWAYAT OBAT KRONIS
        tabModeObatKronis=new DefaultTableModel(null,new Object[]{
                "V","Obat","Jumlah","Signa","Tanggal","Poli","Dokter"
            }){
             @Override public boolean isCellEditable(int rowIndex, int colIndex){
                boolean a = false;
                if (colIndex==0) {
                    a=true;
                }
                return a;
             }
             Class[] types = new Class[] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class,
                java.lang.Object.class, java.lang.Object.class,java.lang.Object.class,
             };
             @Override
             public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
             }
        };
        tbObatKronis.setModel(tabModeObatKronis);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObatKronis.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObatKronis.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (i = 0; i < 7; i++) {
            TableColumn column = tbObatKronis.getColumnModel().getColumn(i);
            if(i==0){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==1){
                column.setPreferredWidth(350);
            }else if(i==2){
                column.setPreferredWidth(80);
            }else if(i==3){
                column.setPreferredWidth(350);
            }else if(i==4){
                column.setPreferredWidth(100);
            }else if(i==5){
                column.setPreferredWidth(100);
            }else if(i==6){
                column.setPreferredWidth(150);
            }
        }
        tbObatKronis.setDefaultRenderer(Object.class, new WarnaTable());        
        //END CUSTOM
        
        tabMode4=new DefaultTableModel(null,new Object[]{
                "No.Resep","Tgl.Resep","Ruang/Kamar","Status","Pasien","Dokter Peresep"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbDetailResepRanap.setModel(tabMode4);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbDetailResepRanap.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbDetailResepRanap.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 6; i++) {
            TableColumn column = tbDetailResepRanap.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(75);
            }else if(i==1){
                column.setPreferredWidth(110);
            }else if(i==2){
                column.setPreferredWidth(140);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(350);
            }else if(i==5){
                column.setPreferredWidth(190);
            }
        }
        tbDetailResepRanap.setDefaultRenderer(Object.class, new WarnaTable());
        //START CUSTOM 
        tabMode5=new DefaultTableModel(null,new Object[]{
                "No.Permintaan","Tanggal","Jam","No.Rawat","No.RM","Pasien","Dokter Yang Meminta",
                "Status","Kode Dokter","Ruang/Kamar","Kode Bangsal","Jenis Bayar","Spesialis Dokter",   
                "Tgl. Validasi","Jam Validasi","Tgl. Obat Jadi","Jam Obat Jadi"   
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbPermintaanStok.setModel(tabMode5);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbPermintaanStok.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPermintaanStok.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (int i = 0; i <17; i++) {   
            TableColumn column = tbPermintaanStok.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(95);
            }else if(i==1){
                column.setPreferredWidth(80);
            }else if(i==2){
                column.setPreferredWidth(85);
            }else if(i==3){
                column.setPreferredWidth(105);
            }else if(i==4){
                column.setPreferredWidth(70);
            }else if(i==5){
                column.setPreferredWidth(190);
            }else if(i==6){
                column.setPreferredWidth(190);
            }else if(i==7){
                column.setPreferredWidth(100);
            }else if(i==8){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==9){
                column.setPreferredWidth(140);
            }else if(i==10){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==11){
                column.setPreferredWidth(120);
            }else if(i==12){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==13){
                column.setPreferredWidth(65);
            }else if(i==14){
                column.setPreferredWidth(70);
            }else if(i==15){
                column.setPreferredWidth(75);
            }else if(i==16){
                column.setPreferredWidth(80);
            }
        }
        tbPermintaanStok.setDefaultRenderer(Object.class, new WarnaTableStokRanap());
        //END CUSTOM        
        tabMode6=new DefaultTableModel(null,new Object[]{
                "No.Permintaan","Tanggal","Ruang/Kamar","Status","Pasien","Dokter Yang Meminta"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbDetailPermintaanStok.setModel(tabMode6);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbDetailPermintaanStok.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbDetailPermintaanStok.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 6; i++) {
            TableColumn column = tbDetailPermintaanStok.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(95);
            }else if(i==1){
                column.setPreferredWidth(110);
            }else if(i==2){
                column.setPreferredWidth(140);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(350);
            }else if(i==5){
                column.setPreferredWidth(1030);
            }
        }
        tbDetailPermintaanStok.setDefaultRenderer(Object.class, new WarnaTable());
        //CUSTOM -> TAMBAH KOLOM VALIDASI DAN OBAT JADI UNTUK RESEP PULANG
        tabMode7=new DefaultTableModel(null,new Object[]{
                "No.Permintaan","Tanggal","Jam","No.Rawat","No.RM","Pasien","Dokter Yang Meminta",
                "Status","Kode Dokter","Ruang/Kamar","Kode Bangsal","Jenis Bayar"
                ,"Tgl. Validasi","Jam Validasi","Tgl. Obat Jadi","Jam Obat Jadi"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbPermintaanResepPulang.setModel(tabMode7);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbPermintaanResepPulang.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbPermintaanResepPulang.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        for (int i = 0; i <12; i++) {
            TableColumn column = tbPermintaanResepPulang.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(95);
            }else if(i==1){
                column.setPreferredWidth(80);
            }else if(i==2){
                column.setPreferredWidth(85);
            }else if(i==3){
                column.setPreferredWidth(105);
            }else if(i==4){
                column.setPreferredWidth(70);
            }else if(i==5){
                column.setPreferredWidth(190);
            }else if(i==6){
                column.setPreferredWidth(190);
            }else if(i==7){
                column.setPreferredWidth(100);
            }else if(i==8){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==9){
                column.setPreferredWidth(140);
            }else if(i==10){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==11){
                column.setPreferredWidth(120);
            }else if(i==12){
                column.setPreferredWidth(65);
            }else if(i==13){
                column.setPreferredWidth(70);
            }else if(i==14){
                column.setPreferredWidth(75);
            }else if(i==15){
                column.setPreferredWidth(80);
            }
        }
        tbPermintaanResepPulang.setDefaultRenderer(Object.class, new WarnaTable());
        //END CUSTOM
        tabMode8=new DefaultTableModel(null,new Object[]{
                "No.Permintaan","Tanggal","Ruang/Kamar","Status","Pasien","Dokter Yang Meminta"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbDetailPermintaanResepPulang.setModel(tabMode8);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbDetailPermintaanResepPulang.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbDetailPermintaanResepPulang.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < 6; i++) {
            TableColumn column = tbDetailPermintaanResepPulang.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(95);
            }else if(i==1){
                column.setPreferredWidth(110);
            }else if(i==2){
                column.setPreferredWidth(140);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(350);
            }else if(i==5){
                column.setPreferredWidth(270);
            }
        }
        tbDetailPermintaanResepPulang.setDefaultRenderer(Object.class, new WarnaTable());
        
        TCari.setDocument(new batasInput((byte)100).getKata(TCari));
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dlgobt.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                tampil();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        dlgobt2.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                tampil3();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        dlgstok.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                tampil5();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        dlgresepulang.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                tampil7();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {;}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){ 
                    if(TabPilihRawat.getSelectedIndex()==0){
                        CrDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                        CrDokter.requestFocus();
                    }else if(TabPilihRawat.getSelectedIndex()==1){
                        CrDokter2.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                        CrDokter2.requestFocus();
                    }                        
                }                
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        poli.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(poli.getTable().getSelectedRow()!= -1){   
                    CrPoli.setText(poli.getTable().getValueAt(poli.getTable().getSelectedRow(),1).toString());
                    CrPoli.requestFocus();
                }   
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
        ruang.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(ruang.getTable().getSelectedRow()!= -1){   
                    Kamar.setText(ruang.getTable().getValueAt(ruang.getTable().getSelectedRow(),1).toString());  
                    Kamar.requestFocus();
                }                      
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        try {
            aktifkanparsial=koneksiDB.AKTIFKANBILLINGPARSIAL();
            alarm=koneksiDB.ALARMAPOTEK();
            formalarm=koneksiDB.FORMALARMAPOTEK();
        } catch (Exception ex) {
            aktifkanparsial="no";
            alarm="no";
            formalarm="ralan + ranap";
        }
        
        if(alarm.equals("yes")){
            jam();
        }
        //START CUSTOM MUHSIN
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {;}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){ 
                    if(temp_ptg==1){
                        KdEntri.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrEntri.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrEntri.requestFocus();
                    }else if(temp_ptg==2){
                        KdAmbil.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrAmbil.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrAmbil.requestFocus();
                    }else if(temp_ptg==3){
                        KdCek.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrCek.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrCek.requestFocus();
                    }else if(temp_ptg==4){
                        KdRacikan.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrRacikan.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrRacikan.requestFocus();
                    }else if(temp_ptg==5){
                        KdSerahKIE.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrSerahKIE.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrSerahKIE.requestFocus();
                    }else if(temp_ptg==6){
                        KdKonseling.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrKonseling.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrKonseling.requestFocus();
                    }else if(temp_ptg==7){
                        KdEntri1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrEntri1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrEntri1.requestFocus();
                    }else if(temp_ptg==8){
                        KdAmbil1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrAmbil1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrAmbil1.requestFocus();
                    }else if(temp_ptg==9){
                        KdCek1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrCek1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrCek1.requestFocus();
                    }else if(temp_ptg==10){
                        KdRacikan1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrRacikan1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrRacikan1.requestFocus();
                    }else if(temp_ptg==11){
                        KdSerahKIE1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrSerahKIE1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrSerahKIE1.requestFocus();
                    }else if(temp_ptg==12){
                        KdKonseling1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                        CrKonseling1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                        CrKonseling1.requestFocus();
                    }
                }                
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        DlgCatatan.setSize(595,80); //CUSTOM MUHSIN
        //END CUSTOM
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        WindowPetugasFarmasi = new javax.swing.JDialog();
        internalFrame5 = new widget.InternalFrame();
        BtnClosePetugasFarmasi = new widget.Button();
        BtnSimpanPetugasFarmasi = new widget.Button();
        jLabel26 = new widget.Label();
        jLabel27 = new widget.Label();
        jLabel28 = new widget.Label();
        jLabel29 = new widget.Label();
        jLabel30 = new widget.Label();
        jLabel31 = new widget.Label();
        jLabel32 = new widget.Label();
        KetResep = new widget.Label();
        CrAmbil = new widget.TextBox();
        CrEntri = new widget.TextBox();
        CrRacikan = new widget.TextBox();
        CrKonseling = new widget.TextBox();
        CrCek = new widget.TextBox();
        CrSerahKIE = new widget.TextBox();
        BtnSeekEntri = new widget.Button();
        BtnSeekKonseling = new widget.Button();
        BtnSeekAmbil = new widget.Button();
        BtnSeekCek = new widget.Button();
        BtnSeekRacikan = new widget.Button();
        BtnSeekSerahKIE = new widget.Button();
        KdKonseling = new widget.TextBox();
        KdEntri = new widget.TextBox();
        KdAmbil = new widget.TextBox();
        KdCek = new widget.TextBox();
        KdRacikan = new widget.TextBox();
        KdSerahKIE = new widget.TextBox();
        jLabel34 = new widget.Label();
        jLabel44 = new widget.Label();
        jLabel45 = new widget.Label();
        Scroll6 = new widget.ScrollPane();
        tbObatKronis = new widget.Table();
        jLabel18 = new widget.Label();
        jLabel66 = new widget.Label();
        DlgCatatan = new javax.swing.JDialog();
        internalFrame7 = new widget.InternalFrame();
        jScrollPane1 = new javax.swing.JScrollPane();
        LabelCatatan = new widget.TextArea();
        WindowEditPetugasFarmasi = new javax.swing.JDialog();
        internalFrame6 = new widget.InternalFrame();
        BtnClosePetugasFarmasi1 = new widget.Button();
        BtnEditPetugasFarmasi = new widget.Button();
        jLabel35 = new widget.Label();
        jLabel36 = new widget.Label();
        jLabel37 = new widget.Label();
        jLabel38 = new widget.Label();
        jLabel39 = new widget.Label();
        jLabel40 = new widget.Label();
        jLabel41 = new widget.Label();
        jLabel42 = new widget.Label();
        CrAmbil1 = new widget.TextBox();
        CrEntri1 = new widget.TextBox();
        CrRacikan1 = new widget.TextBox();
        CrKonseling1 = new widget.TextBox();
        CrCek1 = new widget.TextBox();
        CrSerahKIE1 = new widget.TextBox();
        BtnSeekEntri1 = new widget.Button();
        BtnSeekKonseling1 = new widget.Button();
        BtnSeekAmbil1 = new widget.Button();
        BtnSeekCek1 = new widget.Button();
        BtnSeekRacikan1 = new widget.Button();
        BtnSeekSerahKIE1 = new widget.Button();
        KdKonseling1 = new widget.TextBox();
        KdEntri1 = new widget.TextBox();
        KdAmbil1 = new widget.TextBox();
        KdCek1 = new widget.TextBox();
        KdRacikan1 = new widget.TextBox();
        KdSerahKIE1 = new widget.TextBox();
        jLabel43 = new widget.Label();
        jLabel46 = new widget.Label();
        KetResepEdit = new widget.Label();
        internalFrame1 = new widget.InternalFrame();
        jPanel2 = new javax.swing.JPanel();
        panelisi2 = new widget.panelisi();
        jLabel20 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel12 = new widget.Label();
        cmbStatus = new widget.ComboBox();
        label9 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        panelisi1 = new widget.panelisi();
        BtnTambah = new widget.Button();
        BtnEdit = new widget.Button();
        BtnHapus = new widget.Button();
        BtnPenyerahan = new widget.Button();
        BtnPetugas = new widget.Button();
        BtnPrint = new widget.Button();
        BtnRekap = new widget.Button();
        BtnSalinan = new widget.Button();
        BtnAntrian = new widget.Button();
        BtnTelaah = new widget.Button();
        label10 = new widget.Label();
        LCount = new widget.Label();
        BtnKeluar = new widget.Button();
        TabPilihRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        TabRawatJalan = new javax.swing.JTabbedPane();
        scrollPane1 = new widget.ScrollPane();
        tbResepRalan = new widget.Table();
        scrollPane2 = new widget.ScrollPane();
        tbDetailResepRalan = new widget.Table();
        panelGlass8 = new widget.panelisi();
        jLabel14 = new widget.Label();
        CrDokter = new widget.TextBox();
        BtnSeek3 = new widget.Button();
        jLabel16 = new widget.Label();
        CrPoli = new widget.TextBox();
        BtnSeek4 = new widget.Button();
        internalFrame3 = new widget.InternalFrame();
        TabRawatInap = new javax.swing.JTabbedPane();
        scrollPane3 = new widget.ScrollPane();
        tbResepRanap = new widget.Table();
        scrollPane4 = new widget.ScrollPane();
        tbDetailResepRanap = new widget.Table();
        scrollPane5 = new widget.ScrollPane();
        tbPermintaanStok = new widget.Table();
        scrollPane6 = new widget.ScrollPane();
        tbDetailPermintaanStok = new widget.Table();
        scrollPane7 = new widget.ScrollPane();
        tbPermintaanResepPulang = new widget.Table();
        scrollPane8 = new widget.ScrollPane();
        tbDetailPermintaanResepPulang = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel15 = new widget.Label();
        CrDokter2 = new widget.TextBox();
        BtnSeek5 = new widget.Button();
        jLabel17 = new widget.Label();
        Kamar = new widget.TextBox();
        BtnSeek6 = new widget.Button();

        WindowPetugasFarmasi.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        WindowPetugasFarmasi.setName("WindowPetugasFarmasi"); // NOI18N
        WindowPetugasFarmasi.setUndecorated(true);
        WindowPetugasFarmasi.setResizable(false);
        WindowPetugasFarmasi.setSize(new java.awt.Dimension(1100, 270));

        internalFrame5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "::[ Data Petugas Farmasi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame5.setName("internalFrame5"); // NOI18N
        internalFrame5.setLayout(null);

        BtnClosePetugasFarmasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/cross.png"))); // NOI18N
        BtnClosePetugasFarmasi.setMnemonic('U');
        BtnClosePetugasFarmasi.setText("Tutup");
        BtnClosePetugasFarmasi.setToolTipText("Alt+U");
        BtnClosePetugasFarmasi.setName("BtnClosePetugasFarmasi"); // NOI18N
        BtnClosePetugasFarmasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnClosePetugasFarmasiActionPerformed(evt);
            }
        });
        internalFrame5.add(BtnClosePetugasFarmasi);
        BtnClosePetugasFarmasi.setBounds(620, 230, 80, 30);

        BtnSimpanPetugasFarmasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpanPetugasFarmasi.setMnemonic('S');
        BtnSimpanPetugasFarmasi.setText("Simpan");
        BtnSimpanPetugasFarmasi.setToolTipText("Alt+S");
        BtnSimpanPetugasFarmasi.setName("BtnSimpanPetugasFarmasi"); // NOI18N
        BtnSimpanPetugasFarmasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanPetugasFarmasiActionPerformed(evt);
            }
        });
        internalFrame5.add(BtnSimpanPetugasFarmasi);
        BtnSimpanPetugasFarmasi.setBounds(530, 230, 90, 30);

        jLabel26.setText("Petugas Serah + KIE :");
        jLabel26.setName("jLabel26"); // NOI18N
        internalFrame5.add(jLabel26);
        jLabel26.setBounds(10, 170, 110, 23);

        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel27.setName("jLabel27"); // NOI18N
        internalFrame5.add(jLabel27);
        jLabel27.setBounds(130, 20, 70, 23);

        jLabel28.setText("Petugas Entri :");
        jLabel28.setName("jLabel28"); // NOI18N
        internalFrame5.add(jLabel28);
        jLabel28.setBounds(40, 50, 80, 23);

        jLabel29.setText("Petugas Ambil :");
        jLabel29.setName("jLabel29"); // NOI18N
        internalFrame5.add(jLabel29);
        jLabel29.setBounds(40, 80, 80, 23);

        jLabel30.setText("Petugas Cek :");
        jLabel30.setName("jLabel30"); // NOI18N
        internalFrame5.add(jLabel30);
        jLabel30.setBounds(40, 110, 80, 23);

        jLabel31.setText("Petugas Konseling :");
        jLabel31.setName("jLabel31"); // NOI18N
        internalFrame5.add(jLabel31);
        jLabel31.setBounds(20, 200, 100, 23);

        jLabel32.setText("Petugas Racikan :");
        jLabel32.setName("jLabel32"); // NOI18N
        internalFrame5.add(jLabel32);
        jLabel32.setBounds(20, 140, 100, 23);

        KetResep.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        KetResep.setText("-");
        KetResep.setName("KetResep"); // NOI18N
        internalFrame5.add(KetResep);
        KetResep.setBounds(380, 20, 90, 23);

        CrAmbil.setEditable(false);
        CrAmbil.setName("CrAmbil"); // NOI18N
        CrAmbil.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame5.add(CrAmbil);
        CrAmbil.setBounds(270, 80, 280, 23);

        CrEntri.setEditable(false);
        CrEntri.setName("CrEntri"); // NOI18N
        CrEntri.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame5.add(CrEntri);
        CrEntri.setBounds(270, 50, 280, 23);

        CrRacikan.setEditable(false);
        CrRacikan.setName("CrRacikan"); // NOI18N
        CrRacikan.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame5.add(CrRacikan);
        CrRacikan.setBounds(270, 140, 280, 23);

        CrKonseling.setEditable(false);
        CrKonseling.setName("CrKonseling"); // NOI18N
        CrKonseling.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame5.add(CrKonseling);
        CrKonseling.setBounds(270, 200, 280, 23);

        CrCek.setEditable(false);
        CrCek.setName("CrCek"); // NOI18N
        CrCek.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame5.add(CrCek);
        CrCek.setBounds(270, 110, 280, 23);

        CrSerahKIE.setEditable(false);
        CrSerahKIE.setName("CrSerahKIE"); // NOI18N
        CrSerahKIE.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame5.add(CrSerahKIE);
        CrSerahKIE.setBounds(270, 170, 280, 23);

        BtnSeekEntri.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekEntri.setMnemonic('4');
        BtnSeekEntri.setToolTipText("ALt+4");
        BtnSeekEntri.setName("BtnSeekEntri"); // NOI18N
        BtnSeekEntri.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekEntri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekEntriActionPerformed(evt);
            }
        });
        internalFrame5.add(BtnSeekEntri);
        BtnSeekEntri.setBounds(560, 50, 28, 23);

        BtnSeekKonseling.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekKonseling.setMnemonic('4');
        BtnSeekKonseling.setToolTipText("ALt+4");
        BtnSeekKonseling.setName("BtnSeekKonseling"); // NOI18N
        BtnSeekKonseling.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekKonseling.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekKonselingActionPerformed(evt);
            }
        });
        internalFrame5.add(BtnSeekKonseling);
        BtnSeekKonseling.setBounds(560, 200, 28, 23);

        BtnSeekAmbil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekAmbil.setMnemonic('4');
        BtnSeekAmbil.setToolTipText("ALt+4");
        BtnSeekAmbil.setName("BtnSeekAmbil"); // NOI18N
        BtnSeekAmbil.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekAmbil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekAmbilActionPerformed(evt);
            }
        });
        internalFrame5.add(BtnSeekAmbil);
        BtnSeekAmbil.setBounds(560, 80, 28, 23);

        BtnSeekCek.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekCek.setMnemonic('4');
        BtnSeekCek.setToolTipText("ALt+4");
        BtnSeekCek.setName("BtnSeekCek"); // NOI18N
        BtnSeekCek.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekCek.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekCekActionPerformed(evt);
            }
        });
        internalFrame5.add(BtnSeekCek);
        BtnSeekCek.setBounds(560, 110, 28, 23);

        BtnSeekRacikan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekRacikan.setMnemonic('4');
        BtnSeekRacikan.setToolTipText("ALt+4");
        BtnSeekRacikan.setName("BtnSeekRacikan"); // NOI18N
        BtnSeekRacikan.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekRacikan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekRacikanActionPerformed(evt);
            }
        });
        internalFrame5.add(BtnSeekRacikan);
        BtnSeekRacikan.setBounds(560, 140, 28, 23);

        BtnSeekSerahKIE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekSerahKIE.setMnemonic('4');
        BtnSeekSerahKIE.setToolTipText("ALt+4");
        BtnSeekSerahKIE.setName("BtnSeekSerahKIE"); // NOI18N
        BtnSeekSerahKIE.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekSerahKIE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekSerahKIEActionPerformed(evt);
            }
        });
        internalFrame5.add(BtnSeekSerahKIE);
        BtnSeekSerahKIE.setBounds(560, 170, 28, 23);

        KdKonseling.setEditable(false);
        KdKonseling.setHighlighter(null);
        KdKonseling.setName("KdKonseling"); // NOI18N
        KdKonseling.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdKonselingActionPerformed(evt);
            }
        });
        KdKonseling.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdKonselingKeyPressed(evt);
            }
        });
        internalFrame5.add(KdKonseling);
        KdKonseling.setBounds(130, 200, 130, 23);

        KdEntri.setEditable(false);
        KdEntri.setHighlighter(null);
        KdEntri.setName("KdEntri"); // NOI18N
        KdEntri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdEntriKeyPressed(evt);
            }
        });
        internalFrame5.add(KdEntri);
        KdEntri.setBounds(130, 50, 130, 23);

        KdAmbil.setEditable(false);
        KdAmbil.setHighlighter(null);
        KdAmbil.setName("KdAmbil"); // NOI18N
        KdAmbil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdAmbilActionPerformed(evt);
            }
        });
        KdAmbil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdAmbilKeyPressed(evt);
            }
        });
        internalFrame5.add(KdAmbil);
        KdAmbil.setBounds(130, 80, 130, 23);

        KdCek.setEditable(false);
        KdCek.setHighlighter(null);
        KdCek.setName("KdCek"); // NOI18N
        KdCek.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdCekActionPerformed(evt);
            }
        });
        KdCek.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdCekKeyPressed(evt);
            }
        });
        internalFrame5.add(KdCek);
        KdCek.setBounds(130, 110, 130, 23);

        KdRacikan.setEditable(false);
        KdRacikan.setHighlighter(null);
        KdRacikan.setName("KdRacikan"); // NOI18N
        KdRacikan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdRacikanActionPerformed(evt);
            }
        });
        KdRacikan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdRacikanKeyPressed(evt);
            }
        });
        internalFrame5.add(KdRacikan);
        KdRacikan.setBounds(130, 140, 130, 23);

        KdSerahKIE.setEditable(false);
        KdSerahKIE.setHighlighter(null);
        KdSerahKIE.setName("KdSerahKIE"); // NOI18N
        KdSerahKIE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdSerahKIEActionPerformed(evt);
            }
        });
        KdSerahKIE.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdSerahKIEKeyPressed(evt);
            }
        });
        internalFrame5.add(KdSerahKIE);
        KdSerahKIE.setBounds(130, 170, 130, 23);

        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel34.setName("jLabel34"); // NOI18N
        internalFrame5.add(jLabel34);
        jLabel34.setBounds(500, 20, 180, 23);

        jLabel44.setText("Waktu Obat Jadi :");
        jLabel44.setName("jLabel44"); // NOI18N
        internalFrame5.add(jLabel44);
        jLabel44.setBounds(20, 20, 100, 23);

        jLabel45.setText("Keterangan :");
        jLabel45.setName("jLabel45"); // NOI18N
        internalFrame5.add(jLabel45);
        jLabel45.setBounds(270, 20, 100, 23);

        Scroll6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)));
        Scroll6.setName("Scroll6"); // NOI18N
        Scroll6.setOpaque(true);

        tbObatKronis.setName("tbObatKronis"); // NOI18N
        tbObatKronis.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatKronisMouseClicked(evt);
            }
        });
        tbObatKronis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKronisKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbObatKronisKeyReleased(evt);
            }
        });
        Scroll6.setViewportView(tbObatKronis);

        internalFrame5.add(Scroll6);
        Scroll6.setBounds(610, 50, 420, 130);

        jLabel18.setText("Riwayat Obat Kronis 3 Bulan Terakhir");
        jLabel18.setName("jLabel18"); // NOI18N
        internalFrame5.add(jLabel18);
        jLabel18.setBounds(750, 20, 190, 23);

        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel66.setText("Status PRB :");
        jLabel66.setName("jLabel66"); // NOI18N
        internalFrame5.add(jLabel66);
        jLabel66.setBounds(630, 200, 200, 20);

        WindowPetugasFarmasi.getContentPane().add(internalFrame5, java.awt.BorderLayout.CENTER);

        DlgCatatan.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        DlgCatatan.setName("DlgCatatan"); // NOI18N
        DlgCatatan.setUndecorated(true);
        DlgCatatan.setResizable(false);

        internalFrame7.setBorder(null);
        internalFrame7.setName("internalFrame7"); // NOI18N
        internalFrame7.setPreferredSize(new java.awt.Dimension(570, 60));
        internalFrame7.setWarnaAtas(new java.awt.Color(100, 100, 10));
        internalFrame7.setWarnaBawah(new java.awt.Color(100, 100, 10));
        internalFrame7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                internalFrame7MouseClicked(evt);
            }
        });

        jScrollPane1.setName("jScrollPane1"); // NOI18N
        jScrollPane1.setPreferredSize(new java.awt.Dimension(570, 60));

        LabelCatatan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        LabelCatatan.setColumns(20);
        LabelCatatan.setRows(5);
        LabelCatatan.setName("LabelCatatan"); // NOI18N
        LabelCatatan.setPreferredSize(new java.awt.Dimension(570, 60));
        LabelCatatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LabelCatatanKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(LabelCatatan);

        internalFrame7.add(jScrollPane1);

        DlgCatatan.getContentPane().add(internalFrame7, java.awt.BorderLayout.CENTER);

        WindowEditPetugasFarmasi.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        WindowEditPetugasFarmasi.setName("WindowEditPetugasFarmasi"); // NOI18N
        WindowEditPetugasFarmasi.setUndecorated(true);
        WindowEditPetugasFarmasi.setResizable(false);
        WindowEditPetugasFarmasi.setSize(new java.awt.Dimension(700, 270));

        internalFrame6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "::[ Data Petugas Farmasi ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame6.setName("internalFrame6"); // NOI18N
        internalFrame6.setLayout(null);

        BtnClosePetugasFarmasi1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/cross.png"))); // NOI18N
        BtnClosePetugasFarmasi1.setMnemonic('U');
        BtnClosePetugasFarmasi1.setText("Tutup");
        BtnClosePetugasFarmasi1.setToolTipText("Alt+U");
        BtnClosePetugasFarmasi1.setName("BtnClosePetugasFarmasi1"); // NOI18N
        BtnClosePetugasFarmasi1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnClosePetugasFarmasi1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnClosePetugasFarmasi1);
        BtnClosePetugasFarmasi1.setBounds(620, 230, 80, 30);

        BtnEditPetugasFarmasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEditPetugasFarmasi.setMnemonic('S');
        BtnEditPetugasFarmasi.setText("Edit");
        BtnEditPetugasFarmasi.setToolTipText("Alt+S");
        BtnEditPetugasFarmasi.setName("BtnEditPetugasFarmasi"); // NOI18N
        BtnEditPetugasFarmasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditPetugasFarmasiActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnEditPetugasFarmasi);
        BtnEditPetugasFarmasi.setBounds(530, 230, 90, 30);

        jLabel35.setText("Petugas Serah + KIE :");
        jLabel35.setName("jLabel35"); // NOI18N
        internalFrame6.add(jLabel35);
        jLabel35.setBounds(10, 170, 110, 23);

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel36.setName("jLabel36"); // NOI18N
        internalFrame6.add(jLabel36);
        jLabel36.setBounds(130, 20, 70, 23);

        jLabel37.setText("Petugas Entri :");
        jLabel37.setName("jLabel37"); // NOI18N
        internalFrame6.add(jLabel37);
        jLabel37.setBounds(40, 50, 80, 23);

        jLabel38.setText("Petugas Ambil :");
        jLabel38.setName("jLabel38"); // NOI18N
        internalFrame6.add(jLabel38);
        jLabel38.setBounds(40, 80, 80, 23);

        jLabel39.setText("Petugas Cek :");
        jLabel39.setName("jLabel39"); // NOI18N
        internalFrame6.add(jLabel39);
        jLabel39.setBounds(40, 110, 80, 23);

        jLabel40.setText("Petugas Konseling :");
        jLabel40.setName("jLabel40"); // NOI18N
        internalFrame6.add(jLabel40);
        jLabel40.setBounds(20, 200, 100, 23);

        jLabel41.setText("Petugas Racikan :");
        jLabel41.setName("jLabel41"); // NOI18N
        internalFrame6.add(jLabel41);
        jLabel41.setBounds(20, 140, 100, 23);

        jLabel42.setText("Waktu Obat Jadi :");
        jLabel42.setName("jLabel42"); // NOI18N
        internalFrame6.add(jLabel42);
        jLabel42.setBounds(20, 20, 100, 23);

        CrAmbil1.setEditable(false);
        CrAmbil1.setName("CrAmbil1"); // NOI18N
        CrAmbil1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrAmbil1);
        CrAmbil1.setBounds(270, 80, 280, 23);

        CrEntri1.setEditable(false);
        CrEntri1.setName("CrEntri1"); // NOI18N
        CrEntri1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrEntri1);
        CrEntri1.setBounds(270, 50, 280, 23);

        CrRacikan1.setEditable(false);
        CrRacikan1.setName("CrRacikan1"); // NOI18N
        CrRacikan1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrRacikan1);
        CrRacikan1.setBounds(270, 140, 280, 23);

        CrKonseling1.setEditable(false);
        CrKonseling1.setName("CrKonseling1"); // NOI18N
        CrKonseling1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrKonseling1);
        CrKonseling1.setBounds(270, 200, 280, 23);

        CrCek1.setEditable(false);
        CrCek1.setName("CrCek1"); // NOI18N
        CrCek1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrCek1);
        CrCek1.setBounds(270, 110, 280, 23);

        CrSerahKIE1.setEditable(false);
        CrSerahKIE1.setName("CrSerahKIE1"); // NOI18N
        CrSerahKIE1.setPreferredSize(new java.awt.Dimension(280, 23));
        internalFrame6.add(CrSerahKIE1);
        CrSerahKIE1.setBounds(270, 170, 280, 23);

        BtnSeekEntri1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekEntri1.setMnemonic('4');
        BtnSeekEntri1.setToolTipText("ALt+4");
        BtnSeekEntri1.setName("BtnSeekEntri1"); // NOI18N
        BtnSeekEntri1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekEntri1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekEntri1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekEntri1);
        BtnSeekEntri1.setBounds(560, 50, 28, 23);

        BtnSeekKonseling1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekKonseling1.setMnemonic('4');
        BtnSeekKonseling1.setToolTipText("ALt+4");
        BtnSeekKonseling1.setName("BtnSeekKonseling1"); // NOI18N
        BtnSeekKonseling1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekKonseling1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekKonseling1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekKonseling1);
        BtnSeekKonseling1.setBounds(560, 200, 28, 23);

        BtnSeekAmbil1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekAmbil1.setMnemonic('4');
        BtnSeekAmbil1.setToolTipText("ALt+4");
        BtnSeekAmbil1.setName("BtnSeekAmbil1"); // NOI18N
        BtnSeekAmbil1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekAmbil1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekAmbil1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekAmbil1);
        BtnSeekAmbil1.setBounds(560, 80, 28, 23);

        BtnSeekCek1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekCek1.setMnemonic('4');
        BtnSeekCek1.setToolTipText("ALt+4");
        BtnSeekCek1.setName("BtnSeekCek1"); // NOI18N
        BtnSeekCek1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekCek1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekCek1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekCek1);
        BtnSeekCek1.setBounds(560, 110, 28, 23);

        BtnSeekRacikan1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekRacikan1.setMnemonic('4');
        BtnSeekRacikan1.setToolTipText("ALt+4");
        BtnSeekRacikan1.setName("BtnSeekRacikan1"); // NOI18N
        BtnSeekRacikan1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekRacikan1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekRacikan1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekRacikan1);
        BtnSeekRacikan1.setBounds(560, 140, 28, 23);

        BtnSeekSerahKIE1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeekSerahKIE1.setMnemonic('4');
        BtnSeekSerahKIE1.setToolTipText("ALt+4");
        BtnSeekSerahKIE1.setName("BtnSeekSerahKIE1"); // NOI18N
        BtnSeekSerahKIE1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeekSerahKIE1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeekSerahKIE1ActionPerformed(evt);
            }
        });
        internalFrame6.add(BtnSeekSerahKIE1);
        BtnSeekSerahKIE1.setBounds(560, 170, 28, 23);

        KdKonseling1.setEditable(false);
        KdKonseling1.setHighlighter(null);
        KdKonseling1.setName("KdKonseling1"); // NOI18N
        KdKonseling1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdKonseling1ActionPerformed(evt);
            }
        });
        KdKonseling1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdKonseling1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdKonseling1);
        KdKonseling1.setBounds(130, 200, 130, 23);

        KdEntri1.setEditable(false);
        KdEntri1.setHighlighter(null);
        KdEntri1.setName("KdEntri1"); // NOI18N
        KdEntri1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdEntri1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdEntri1);
        KdEntri1.setBounds(130, 50, 130, 23);

        KdAmbil1.setEditable(false);
        KdAmbil1.setHighlighter(null);
        KdAmbil1.setName("KdAmbil1"); // NOI18N
        KdAmbil1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdAmbil1ActionPerformed(evt);
            }
        });
        KdAmbil1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdAmbil1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdAmbil1);
        KdAmbil1.setBounds(130, 80, 130, 23);

        KdCek1.setEditable(false);
        KdCek1.setHighlighter(null);
        KdCek1.setName("KdCek1"); // NOI18N
        KdCek1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdCek1ActionPerformed(evt);
            }
        });
        KdCek1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdCek1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdCek1);
        KdCek1.setBounds(130, 110, 130, 23);

        KdRacikan1.setEditable(false);
        KdRacikan1.setHighlighter(null);
        KdRacikan1.setName("KdRacikan1"); // NOI18N
        KdRacikan1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdRacikan1ActionPerformed(evt);
            }
        });
        KdRacikan1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdRacikan1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdRacikan1);
        KdRacikan1.setBounds(130, 140, 130, 23);

        KdSerahKIE1.setEditable(false);
        KdSerahKIE1.setHighlighter(null);
        KdSerahKIE1.setName("KdSerahKIE1"); // NOI18N
        KdSerahKIE1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KdSerahKIE1ActionPerformed(evt);
            }
        });
        KdSerahKIE1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdSerahKIE1KeyPressed(evt);
            }
        });
        internalFrame6.add(KdSerahKIE1);
        KdSerahKIE1.setBounds(130, 170, 130, 23);

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel43.setName("jLabel43"); // NOI18N
        internalFrame6.add(jLabel43);
        jLabel43.setBounds(500, 20, 180, 23);

        jLabel46.setText("Keterangan :");
        jLabel46.setName("jLabel46"); // NOI18N
        internalFrame6.add(jLabel46);
        jLabel46.setBounds(270, 20, 100, 23);

        KetResepEdit.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        KetResepEdit.setText("-");
        KetResepEdit.setName("KetResepEdit"); // NOI18N
        internalFrame6.add(KetResepEdit);
        KetResepEdit.setBounds(380, 20, 90, 23);

        WindowEditPetugasFarmasi.getContentPane().add(internalFrame6, java.awt.BorderLayout.CENTER);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowDeactivated(java.awt.event.WindowEvent evt) {
                formWindowDeactivated(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Peresepan Obat Oleh Dokter ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        jPanel2.setName("jPanel2"); // NOI18N
        jPanel2.setOpaque(false);
        jPanel2.setPreferredSize(new java.awt.Dimension(816, 100));
        jPanel2.setLayout(new java.awt.BorderLayout(1, 1));

        panelisi2.setBackground(new java.awt.Color(255, 150, 255));
        panelisi2.setName("panelisi2"); // NOI18N
        panelisi2.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 4, 9));

        jLabel20.setText("Tgl.Peresepan :");
        jLabel20.setName("jLabel20"); // NOI18N
        jLabel20.setPreferredSize(new java.awt.Dimension(85, 23));
        panelisi2.add(jLabel20);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "08-01-2025" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelisi2.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(24, 23));
        panelisi2.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "08-01-2025" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelisi2.add(DTPCari2);

        jLabel12.setText("Status :");
        jLabel12.setName("jLabel12"); // NOI18N
        jLabel12.setPreferredSize(new java.awt.Dimension(60, 23));
        panelisi2.add(jLabel12);

        cmbStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Semua", "Belum Terlayani", "Sudah Terlayani" }));
        cmbStatus.setName("cmbStatus"); // NOI18N
        cmbStatus.setPreferredSize(new java.awt.Dimension(130, 23));
        panelisi2.add(cmbStatus);

        label9.setText("Key Word :");
        label9.setName("label9"); // NOI18N
        label9.setPreferredSize(new java.awt.Dimension(70, 23));
        panelisi2.add(label9);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(210, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelisi2.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('1');
        BtnCari.setToolTipText("Alt+1");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelisi2.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelisi2.add(BtnAll);

        jPanel2.add(panelisi2, java.awt.BorderLayout.PAGE_START);

        panelisi1.setName("panelisi1"); // NOI18N
        panelisi1.setPreferredSize(new java.awt.Dimension(100, 44));
        panelisi1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnTambah.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambah.setMnemonic('S');
        BtnTambah.setText("Validasi");
        BtnTambah.setToolTipText("Alt+S");
        BtnTambah.setName("BtnTambah"); // NOI18N
        BtnTambah.setPreferredSize(new java.awt.Dimension(90, 30));
        BtnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahActionPerformed(evt);
            }
        });
        BtnTambah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnTambahKeyPressed(evt);
            }
        });
        panelisi1.add(BtnTambah);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('U');
        BtnEdit.setText("Ubah");
        BtnEdit.setToolTipText("Alt+U");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelisi1.add(BtnEdit);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelisi1.add(BtnHapus);

        BtnPenyerahan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Agenda-1-16x16.png"))); // NOI18N
        BtnPenyerahan.setMnemonic('H');
        BtnPenyerahan.setText("Obat Jadi");
        BtnPenyerahan.setToolTipText("Alt+H");
        BtnPenyerahan.setName("BtnPenyerahan"); // NOI18N
        BtnPenyerahan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPenyerahan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPenyerahanActionPerformed(evt);
            }
        });
        BtnPenyerahan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPenyerahanKeyPressed(evt);
            }
        });
        panelisi1.add(BtnPenyerahan);

        BtnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Agenda-1-16x16.png"))); // NOI18N
        BtnPetugas.setMnemonic('H');
        BtnPetugas.setText("Edit Petugas");
        BtnPetugas.setToolTipText("Alt+H");
        BtnPetugas.setName("BtnPetugas"); // NOI18N
        BtnPetugas.setPreferredSize(new java.awt.Dimension(110, 30));
        BtnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPetugasActionPerformed(evt);
            }
        });
        BtnPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPetugasKeyPressed(evt);
            }
        });
        panelisi1.add(BtnPetugas);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(75, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelisi1.add(BtnPrint);

        BtnRekap.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/preview-16x16.png"))); // NOI18N
        BtnRekap.setMnemonic('R');
        BtnRekap.setText("Rekap");
        BtnRekap.setToolTipText("Alt+R");
        BtnRekap.setName("BtnRekap"); // NOI18N
        BtnRekap.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnRekap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRekapActionPerformed(evt);
            }
        });
        BtnRekap.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnRekapKeyPressed(evt);
            }
        });
        panelisi1.add(BtnRekap);

        BtnSalinan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/editcopy.png"))); // NOI18N
        BtnSalinan.setMnemonic('R');
        BtnSalinan.setText("Salinan");
        BtnSalinan.setToolTipText("Alt+R");
        BtnSalinan.setName("BtnSalinan"); // NOI18N
        BtnSalinan.setPreferredSize(new java.awt.Dimension(90, 30));
        BtnSalinan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSalinanActionPerformed(evt);
            }
        });
        BtnSalinan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSalinanKeyPressed(evt);
            }
        });
        panelisi1.add(BtnSalinan);

        BtnAntrian.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnAntrian.setMnemonic('T');
        BtnAntrian.setText("Antrian");
        BtnAntrian.setToolTipText("Alt+T");
        BtnAntrian.setName("BtnAntrian"); // NOI18N
        BtnAntrian.setPreferredSize(new java.awt.Dimension(90, 30));
        BtnAntrian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAntrianActionPerformed(evt);
            }
        });
        BtnAntrian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAntrianKeyPressed(evt);
            }
        });
        panelisi1.add(BtnAntrian);

        BtnTelaah.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/tasksgroup.png"))); // NOI18N
        BtnTelaah.setMnemonic('T');
        BtnTelaah.setText("Telaah");
        BtnTelaah.setToolTipText("Alt+T");
        BtnTelaah.setName("BtnTelaah"); // NOI18N
        BtnTelaah.setPreferredSize(new java.awt.Dimension(95, 30));
        BtnTelaah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTelaahActionPerformed(evt);
            }
        });
        BtnTelaah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnTelaahKeyPressed(evt);
            }
        });
        panelisi1.add(BtnTelaah);

        label10.setText("Record :");
        label10.setName("label10"); // NOI18N
        label10.setPreferredSize(new java.awt.Dimension(55, 23));
        panelisi1.add(label10);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(43, 23));
        panelisi1.add(LCount);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(80, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelisi1.add(BtnKeluar);

        jPanel2.add(panelisi1, java.awt.BorderLayout.CENTER);

        internalFrame1.add(jPanel2, java.awt.BorderLayout.PAGE_END);

        TabPilihRawat.setBackground(new java.awt.Color(255, 255, 253));
        TabPilihRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabPilihRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabPilihRawat.setName("TabPilihRawat"); // NOI18N
        TabPilihRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabPilihRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout());

        TabRawatJalan.setBackground(new java.awt.Color(255, 255, 253));
        TabRawatJalan.setForeground(new java.awt.Color(50, 50, 50));
        TabRawatJalan.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawatJalan.setName("TabRawatJalan"); // NOI18N
        TabRawatJalan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatJalanMouseClicked(evt);
            }
        });

        scrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        scrollPane1.setName("scrollPane1"); // NOI18N
        scrollPane1.setOpaque(true);

        tbResepRalan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbResepRalan.setName("tbResepRalan"); // NOI18N
        tbResepRalan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbResepRalanMouseClicked(evt);
            }
        });
        tbResepRalan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbResepRalanKeyPressed(evt);
            }
        });
        scrollPane1.setViewportView(tbResepRalan);

        TabRawatJalan.addTab("Resep Rawat Jalan", scrollPane1);

        scrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        scrollPane2.setName("scrollPane2"); // NOI18N
        scrollPane2.setOpaque(true);

        tbDetailResepRalan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbDetailResepRalan.setName("tbDetailResepRalan"); // NOI18N
        scrollPane2.setViewportView(tbDetailResepRalan);

        TabRawatJalan.addTab("Detail Rawat Jalan", scrollPane2);

        internalFrame2.add(TabRawatJalan, java.awt.BorderLayout.CENTER);

        panelGlass8.setBorder(null);
        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 41));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel14.setText("Dokter :");
        jLabel14.setName("jLabel14"); // NOI18N
        jLabel14.setPreferredSize(new java.awt.Dimension(55, 23));
        panelGlass8.add(jLabel14);

        CrDokter.setEditable(false);
        CrDokter.setName("CrDokter"); // NOI18N
        CrDokter.setPreferredSize(new java.awt.Dimension(305, 23));
        panelGlass8.add(CrDokter);

        BtnSeek3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeek3.setMnemonic('6');
        BtnSeek3.setToolTipText("ALt+6");
        BtnSeek3.setName("BtnSeek3"); // NOI18N
        BtnSeek3.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeek3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeek3ActionPerformed(evt);
            }
        });
        panelGlass8.add(BtnSeek3);

        jLabel16.setText("Unit :");
        jLabel16.setName("jLabel16"); // NOI18N
        jLabel16.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass8.add(jLabel16);

        CrPoli.setEditable(false);
        CrPoli.setName("CrPoli"); // NOI18N
        CrPoli.setPreferredSize(new java.awt.Dimension(305, 23));
        panelGlass8.add(CrPoli);

        BtnSeek4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeek4.setMnemonic('5');
        BtnSeek4.setToolTipText("ALt+5");
        BtnSeek4.setName("BtnSeek4"); // NOI18N
        BtnSeek4.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeek4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeek4ActionPerformed(evt);
            }
        });
        panelGlass8.add(BtnSeek4);

        internalFrame2.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabPilihRawat.addTab("Rawat Jalan", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout());

        TabRawatInap.setBackground(new java.awt.Color(255, 255, 253));
        TabRawatInap.setForeground(new java.awt.Color(50, 50, 50));
        TabRawatInap.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawatInap.setName("TabRawatInap"); // NOI18N
        TabRawatInap.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatInapMouseClicked(evt);
            }
        });

        scrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        scrollPane3.setName("scrollPane3"); // NOI18N
        scrollPane3.setOpaque(true);

        tbResepRanap.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbResepRanap.setName("tbResepRanap"); // NOI18N
        tbResepRanap.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbResepRanapMouseClicked(evt);
            }
        });
        tbResepRanap.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbResepRanapKeyPressed(evt);
            }
        });
        scrollPane3.setViewportView(tbResepRanap);

        TabRawatInap.addTab("Resep Rawat Inap", scrollPane3);

        scrollPane4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        scrollPane4.setName("scrollPane4"); // NOI18N
        scrollPane4.setOpaque(true);

        tbDetailResepRanap.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbDetailResepRanap.setName("tbDetailResepRanap"); // NOI18N
        scrollPane4.setViewportView(tbDetailResepRanap);

        TabRawatInap.addTab("Detail Rawat Inap", scrollPane4);

        scrollPane5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        scrollPane5.setName("scrollPane5"); // NOI18N
        scrollPane5.setOpaque(true);

        tbPermintaanStok.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbPermintaanStok.setName("tbPermintaanStok"); // NOI18N
        tbPermintaanStok.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPermintaanStokMouseClicked(evt);
            }
        });
        tbPermintaanStok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPermintaanStokKeyPressed(evt);
            }
        });
        scrollPane5.setViewportView(tbPermintaanStok);

        TabRawatInap.addTab("Permintaan Stok Pasien", scrollPane5);

        scrollPane6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        scrollPane6.setName("scrollPane6"); // NOI18N
        scrollPane6.setOpaque(true);

        tbDetailPermintaanStok.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbDetailPermintaanStok.setName("tbDetailPermintaanStok"); // NOI18N
        scrollPane6.setViewportView(tbDetailPermintaanStok);

        TabRawatInap.addTab("Detail Stok Pasien", scrollPane6);

        scrollPane7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        scrollPane7.setName("scrollPane7"); // NOI18N
        scrollPane7.setOpaque(true);

        tbPermintaanResepPulang.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbPermintaanResepPulang.setName("tbPermintaanResepPulang"); // NOI18N
        tbPermintaanResepPulang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPermintaanResepPulangMouseClicked(evt);
            }
        });
        tbPermintaanResepPulang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbPermintaanResepPulangKeyPressed(evt);
            }
        });
        scrollPane7.setViewportView(tbPermintaanResepPulang);

        TabRawatInap.addTab("Permintaan Resep Pulang", scrollPane7);

        scrollPane8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        scrollPane8.setName("scrollPane8"); // NOI18N
        scrollPane8.setOpaque(true);

        tbDetailPermintaanResepPulang.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbDetailPermintaanResepPulang.setName("tbDetailPermintaanResepPulang"); // NOI18N
        scrollPane8.setViewportView(tbDetailPermintaanResepPulang);

        TabRawatInap.addTab("Detail Resep Pulang", scrollPane8);

        internalFrame3.add(TabRawatInap, java.awt.BorderLayout.CENTER);

        panelGlass9.setBorder(null);
        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 41));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel15.setText("Dokter :");
        jLabel15.setName("jLabel15"); // NOI18N
        jLabel15.setPreferredSize(new java.awt.Dimension(55, 23));
        panelGlass9.add(jLabel15);

        CrDokter2.setEditable(false);
        CrDokter2.setName("CrDokter2"); // NOI18N
        CrDokter2.setPreferredSize(new java.awt.Dimension(305, 23));
        panelGlass9.add(CrDokter2);

        BtnSeek5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeek5.setMnemonic('6');
        BtnSeek5.setToolTipText("ALt+6");
        BtnSeek5.setName("BtnSeek5"); // NOI18N
        BtnSeek5.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeek5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeek5ActionPerformed(evt);
            }
        });
        panelGlass9.add(BtnSeek5);

        jLabel17.setText("Ruang/Kamar :");
        jLabel17.setName("jLabel17"); // NOI18N
        jLabel17.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(jLabel17);

        Kamar.setEditable(false);
        Kamar.setName("Kamar"); // NOI18N
        Kamar.setPreferredSize(new java.awt.Dimension(305, 23));
        panelGlass9.add(Kamar);

        BtnSeek6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeek6.setMnemonic('5');
        BtnSeek6.setToolTipText("ALt+5");
        BtnSeek6.setName("BtnSeek6"); // NOI18N
        BtnSeek6.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeek6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeek6ActionPerformed(evt);
            }
        });
        panelGlass9.add(BtnSeek6);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabPilihRawat.addTab("Rawat Inap", internalFrame3);

        internalFrame1.add(TabPilihRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            pilihTab();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
            tbResepRalan.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        pilihTab();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            pilihTab();
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void tbResepRalanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbResepRalanMouseClicked
        DlgCatatan.dispose();   //CUSTOM MUHSIN
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if(evt.getClickCount()==2){
                //START CUSTOM MUHSIN
                i=tbResepRalan.getSelectedColumn();
                if(i==0){
                    String jabatan=Sequel.cariIsi("select nm_jbtn from jabatan where kd_jbtn=?",Sequel.cariIsi("select kd_jbtn from petugas where nip=?",akses.getkode()));
                    if(jabatan.equalsIgnoreCase("IT")||jabatan.equalsIgnoreCase("APOTEKER")||jabatan.equalsIgnoreCase("ASISTEN APOTEKER")){
                        if(akses.getberi_obat()==true){
                        BtnTambahActionPerformed(null);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"User login bukan Apoteker...!!");
                    }
                }else if(i==9){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        String raber = "";
                        i = 0;
                        //PAKAI PS
                        try {
                        pscatatan=koneksi.prepareStatement("select p.nm_poli,d.nm_dokter " +
                            "from rujukan_internal_poli " +
                            "inner join poliklinik p on rujukan_internal_poli.kd_poli = p.kd_poli " +
                            "inner join dokter d on rujukan_internal_poli.kd_dokter = d.kd_dokter " +
                            "where no_rawat=?");
                        try {
                            pscatatan.setString(1,tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),3).toString());
                            rscatatan=pscatatan.executeQuery();
                            while(rscatatan.next()){
                                i++;
                                raber = raber+rscatatan.getString("nm_poli")+" "+rscatatan.getString("nm_dokter")+";\n";
                                }
                            } catch (Exception e) {
                                System.out.println("Notif Plan : "+e);
                            } finally{
                                if(rscatatan!=null){
                                    rscatatan.close();
                                }
                                if(pscatatan!=null){
                                    pscatatan.close();
                                }
                            }            
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        //CARI REGISTRASI BEDA DI HARI YANG SAMA (CONTOH UTK POLI EKSE)
                        try {
                        pscatatan=koneksi.prepareStatement("select p.nm_poli,d.nm_dokter " +
                            "from reg_periksa " +
                            "inner join poliklinik p on reg_periksa.kd_poli = p.kd_poli " +
                            "inner join dokter d on reg_periksa.kd_dokter = d.kd_dokter " +
                            "where no_rkm_medis=? and tgl_registrasi between ? and ?");
                        try {
                            pscatatan.setString(1,tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),4).toString());
                            pscatatan.setString(2,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                            pscatatan.setString(3,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                            rscatatan=pscatatan.executeQuery();
                            while(rscatatan.next()){
                                i++;
                                raber = raber+rscatatan.getString("nm_poli")+" "+rscatatan.getString("nm_dokter")+";\n";
                                }
                            } catch (Exception e) {
                                System.out.println("Notif Plan : "+e);
                            } finally{
                                if(rscatatan!=null){
                                    rscatatan.close();
                                }
                                if(pscatatan!=null){
                                    pscatatan.close();
                                }
                            }            
                        } catch (Exception e) {
                            System.out.println(e);
                        }                        
                        if(!raber.equals("")){
                            LabelCatatan.setText(raber);
                        }else{
                            LabelCatatan.setText("");
                        }
                        if(!LabelCatatan.getText().equals("")){
                            DlgCatatan.setLocationRelativeTo(TabPilihRawat);
                            DlgCatatan.setBackground(new java.awt.Color(8, 146, 63));
                            internalFrame7.setWarnaAtas(new java.awt.Color(8, 146, 63));
                            internalFrame7.setWarnaBawah(new java.awt.Color(8, 146, 63));
                            DlgCatatan.setVisible(true);
                        }else{
                            DlgCatatan.setVisible(false);
                        }                            
                        this.setCursor(Cursor.getDefaultCursor());
                }else if(i==11){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    akses.setform("DlgDaftarPermintaanResep");
                    BPJSCekHistoriPelayanan dlgki=new BPJSCekHistoriPelayanan(null,false);
                    dlgki.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
                    dlgki.setLocationRelativeTo(internalFrame1);
                    dlgki.setKartu(Sequel.cariIsi("select no_peserta from pasien where no_rkm_medis=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),4).toString()));   
                    dlgki.setVisible(true);
                    dlgki.panggiltampil();
                    this.setCursor(Cursor.getDefaultCursor());
                }
                //END CUSTOM
            }
        }
}//GEN-LAST:event_tbResepRalanMouseClicked

    private void tbResepRalanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbResepRalanKeyPressed
        DlgCatatan.dispose();   //CUSTOM MUHSIN
        if(tabMode.getRowCount()!=0){
            if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
                if(akses.getberi_obat()==true){
                    //START CUSTOM MUHSIN
                    //BtnTambahActionPerformed(null);
                    String jabatan=Sequel.cariIsi("select nm_jbtn from jabatan where kd_jbtn=?",Sequel.cariIsi("select kd_jbtn from petugas where nip=?",akses.getkode()));
                    if(jabatan.equalsIgnoreCase("IT")||jabatan.equalsIgnoreCase("APOTEKER")||jabatan.equalsIgnoreCase("ASISTEN APOTEKER")){
                        if(akses.getberi_obat()==true){
                        BtnTambahActionPerformed(null);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"User login bukan Apoteker...!!");
                    }
                    //END CUSTOM
                }                    
            }
        }
}//GEN-LAST:event_tbResepRalanKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        if(TabPilihRawat.getSelectedIndex()==0){
            if(TabRawatJalan.getSelectedIndex()==0){
                if(tabMode.getRowCount()==0){            
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
                   // BtnBatal.requestFocus();
                }else if(tabMode.getRowCount()!=0){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));        
                    Sequel.queryu("truncate table temporary_resep");
                    
                    for(int i=0;i<tabMode.getRowCount();i++){  
                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                            "0",tabMode.getValueAt(i,0).toString(),tabMode.getValueAt(i,1).toString(),tabMode.getValueAt(i,2).toString(),
                            tabMode.getValueAt(i,3).toString(),tabMode.getValueAt(i,4).toString(),tabMode.getValueAt(i,5).toString(),
                            tabMode.getValueAt(i,6).toString(),tabMode.getValueAt(i,7).toString(),tabMode.getValueAt(i,8).toString(),
                            tabMode.getValueAt(i,9).toString(),tabMode.getValueAt(i,10).toString(),tabMode.getValueAt(i,11).toString(),
                            "","","","","","","","","","","","","","","","","","","","","","","","","",""
                        });
                    }
                    
                    Map<String, Object> param = new HashMap<>();  
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());   
                    param.put("logo",Sequel.cariGambar("select logo from setting")); 
                    Valid.MyReport2("rptDaftarPermintaanResep.jasper","report","::[ Laporan Daftar Permintaan Resep ]::",param);
                    this.setCursor(Cursor.getDefaultCursor());
                }   
            }else if(TabRawatJalan.getSelectedIndex()==1){
                if(tabMode2.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
                    TCari.requestFocus();
                }else if(tabMode2.getRowCount()!=0){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));        
                    Sequel.queryu("truncate table temporary_resep");
                    
                    for(int i=0;i<tabMode2.getRowCount();i++){  
                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                            "0",tabMode2.getValueAt(i,0).toString(),tabMode2.getValueAt(i,1).toString(),tabMode2.getValueAt(i,2).toString(),
                            tabMode2.getValueAt(i,3).toString(),tabMode2.getValueAt(i,4).toString(),tabMode2.getValueAt(i,5).toString(),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                        });
                    }
                    
                    Map<String, Object> param = new HashMap<>();  
                        param.put("namars",akses.getnamars());
                        param.put("alamatrs",akses.getalamatrs());
                        param.put("kotars",akses.getkabupatenrs());
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("kontakrs",akses.getkontakrs());
                        param.put("emailrs",akses.getemailrs());   
                        param.put("logo",Sequel.cariGambar("select logo from setting")); 
                    Valid.MyReport2("rptDaftarResep.jasper","report","::[ Daftar Resep Obat ]::",param);
                    this.setCursor(Cursor.getDefaultCursor());
                }
            }
        }else if(TabPilihRawat.getSelectedIndex()==1){
            if(TabRawatInap.getSelectedIndex()==0){
                if(tabMode3.getRowCount()==0){            
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
                   // BtnBatal.requestFocus();
                }else if(tabMode3.getRowCount()!=0){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    Sequel.queryu("truncate table temporary_resep");
                    
                    for(int i=0;i<tabMode3.getRowCount();i++){  
                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                            "0",tabMode3.getValueAt(i,0).toString(),tabMode3.getValueAt(i,1).toString(),tabMode3.getValueAt(i,2).toString(),
                            tabMode3.getValueAt(i,3).toString(),tabMode3.getValueAt(i,4).toString(),tabMode3.getValueAt(i,5).toString(),
                            tabMode3.getValueAt(i,6).toString(),tabMode3.getValueAt(i,7).toString(),tabMode3.getValueAt(i,8).toString(),
                            tabMode3.getValueAt(i,9).toString(),"",tabMode3.getValueAt(i,11).toString(),
                            "","","","","","","","","","","","","","","","","","","","","","","","","",""
                        });
                    }
                    
                    Map<String, Object> param = new HashMap<>();  
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());   
                    param.put("logo",Sequel.cariGambar("select logo from setting")); 
                    Valid.MyReport2("rptDaftarPermintaanResep2.jasper","report","::[ Laporan Daftar Permintaan Resep ]::",param);
                    this.setCursor(Cursor.getDefaultCursor());
                }   
            }else if(TabRawatInap.getSelectedIndex()==1){
                if(tabMode4.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
                    TCari.requestFocus();
                }else if(tabMode4.getRowCount()!=0){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));        
                    Sequel.queryu("truncate table temporary_resep");
                    
                    for(int i=0;i<tabMode4.getRowCount();i++){  
                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                            "0",tabMode4.getValueAt(i,0).toString(),tabMode4.getValueAt(i,1).toString(),tabMode4.getValueAt(i,2).toString(),
                            tabMode4.getValueAt(i,3).toString(),tabMode4.getValueAt(i,4).toString(),tabMode4.getValueAt(i,5).toString(),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                        });
                    }
                    
                    Map<String, Object> param = new HashMap<>();  
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());   
                    param.put("logo",Sequel.cariGambar("select logo from setting")); 
                    Valid.MyReport2("rptDaftarResep2.jasper","report","::[ Daftar Resep Obat ]::",param);
                    this.setCursor(Cursor.getDefaultCursor());
                }
            }else if(TabRawatInap.getSelectedIndex()==2){
                if(tabMode5.getRowCount()==0){            
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
                   // BtnBatal.requestFocus();
                }else if(tabMode5.getRowCount()!=0){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    Sequel.queryu("truncate table temporary_resep");
                    
                    for(int i=0;i<tabMode5.getRowCount();i++){  
                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                            "0",tabMode5.getValueAt(i,0).toString(),tabMode5.getValueAt(i,1).toString(),tabMode5.getValueAt(i,2).toString(),
                            tabMode5.getValueAt(i,3).toString(),tabMode5.getValueAt(i,4).toString(),tabMode5.getValueAt(i,5).toString(),
                            tabMode5.getValueAt(i,6).toString(),tabMode5.getValueAt(i,7).toString(),tabMode5.getValueAt(i,8).toString(),
                            tabMode5.getValueAt(i,9).toString(),"",tabMode5.getValueAt(i,11).toString(),
                            "","","","","","","","","","","","","","","","","","","","","","","","","",""
                        });
                    }
                    
                    Map<String, Object> param = new HashMap<>();  
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());   
                    param.put("logo",Sequel.cariGambar("select logo from setting")); 
                    Valid.MyReport2("rptDaftarPermintaanResep3.jasper","report","::[ Laporan Daftar Permintaan Stok Pasien ]::",param);
                    this.setCursor(Cursor.getDefaultCursor());
                }   
            }else if(TabRawatInap.getSelectedIndex()==3){
                if(tabMode6.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
                    TCari.requestFocus();
                }else if(tabMode6.getRowCount()!=0){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));        
                    Sequel.queryu("truncate table temporary_resep");
                    
                    for(int i=0;i<tabMode6.getRowCount();i++){  
                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                            "0",tabMode6.getValueAt(i,0).toString(),tabMode6.getValueAt(i,1).toString(),tabMode6.getValueAt(i,2).toString(),
                            tabMode6.getValueAt(i,3).toString(),tabMode6.getValueAt(i,4).toString(),tabMode6.getValueAt(i,5).toString(),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                        });
                    }
                    
                    Map<String, Object> param = new HashMap<>();  
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());   
                    param.put("logo",Sequel.cariGambar("select logo from setting")); 
                    Valid.MyReport2("rptDaftarResep3.jasper","report","::[ Daftar Permintaan Stok Obat Pasien ]::",param);
                    this.setCursor(Cursor.getDefaultCursor());
                }
            }else if(TabRawatInap.getSelectedIndex()==4){
                if(tabMode7.getRowCount()==0){            
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
                   // BtnBatal.requestFocus();
                }else if(tabMode7.getRowCount()!=0){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    Sequel.queryu("truncate table temporary_resep");
                    
                    for(int i=0;i<tabMode7.getRowCount();i++){  
                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                            "0",tabMode7.getValueAt(i,0).toString(),tabMode7.getValueAt(i,1).toString(),tabMode7.getValueAt(i,2).toString(),
                            tabMode7.getValueAt(i,3).toString(),tabMode7.getValueAt(i,4).toString(),tabMode7.getValueAt(i,5).toString(),
                            tabMode7.getValueAt(i,6).toString(),tabMode7.getValueAt(i,7).toString(),tabMode7.getValueAt(i,8).toString(),
                            tabMode7.getValueAt(i,9).toString(),"",tabMode7.getValueAt(i,11).toString(),
                            "","","","","","","","","","","","","","","","","","","","","","","","","",""
                        });
                    }
                    
                    Map<String, Object> param = new HashMap<>();  
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());   
                    param.put("logo",Sequel.cariGambar("select logo from setting")); 
                    Valid.MyReport2("rptDaftarPermintaanResepPulang.jasper","report","::[ Laporan Daftar Permintaan Resep Pulang ]::",param);
                    this.setCursor(Cursor.getDefaultCursor());
                }   
            }else if(TabRawatInap.getSelectedIndex()==5){
                if(tabMode8.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
                    TCari.requestFocus();
                }else if(tabMode8.getRowCount()!=0){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));        
                    Sequel.queryu("truncate table temporary_resep");
                    
                    for(int i=0;i<tabMode8.getRowCount();i++){  
                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                            "0",tabMode8.getValueAt(i,0).toString(),tabMode8.getValueAt(i,1).toString(),tabMode8.getValueAt(i,2).toString(),
                            tabMode8.getValueAt(i,3).toString(),tabMode8.getValueAt(i,4).toString(),tabMode8.getValueAt(i,5).toString(),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                        });
                    }
                    
                    Map<String, Object> param = new HashMap<>();  
                    param.put("namars",akses.getnamars());
                    param.put("alamatrs",akses.getalamatrs());
                    param.put("kotars",akses.getkabupatenrs());
                    param.put("propinsirs",akses.getpropinsirs());
                    param.put("kontakrs",akses.getkontakrs());
                    param.put("emailrs",akses.getemailrs());   
                    param.put("logo",Sequel.cariGambar("select logo from setting")); 
                    Valid.MyReport2("rptDaftarResepPulang.jasper","report","::[ Daftar Permintaan Resep Pulang ]::",param);
                    this.setCursor(Cursor.getDefaultCursor());
                }
            }
        }        
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            //Valid.pindah(evt,BtnEdit,BtnAll);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        if(TabPilihRawat.getSelectedIndex()==0){
            CrDokter.setText("");
            CrPoli.setText("");
            TCari.setText("");
            pilihRalan();
        }else if(TabPilihRawat.getSelectedIndex()==1){
            CrDokter2.setText("");
            Kamar.setText("");
            TCari.setText("");
            pilihRanap();
        }
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnPrint, BtnKeluar);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
            dispose();  
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){            
            dispose();              
        }else{Valid.pindah(evt,BtnAll,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahActionPerformed
        if(TabPilihRawat.getSelectedIndex()==0){
            if(TabRawatJalan.getSelectedIndex()==0){
                if(akses.getberi_obat()==true){
                    if(tabMode.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau divalidasi..!!");
                    }else{
                        if(Status.equals("Sudah Terlayani")){
                            JOptionPane.showMessageDialog(rootPane,"Resep sudah tervalidasi ..!!");
                        }else {
                            jmlparsial=0;
                            if(aktifkanparsial.equals("yes")){
                                jmlparsial=Sequel.cariInteger("select count(kd_pj) from set_input_parsial where kd_pj=?",Sequel.cariIsi("select kd_pj from reg_periksa where no_rawat=?",NoRawat));
                            }
                            if(jmlparsial>0){
                                Sequel.queryu("delete from antriapotek2");
                                Sequel.queryu("insert into antriapotek2 values('"+NoResep+"','1','"+NoRawat+"')");
                                panggilform();
                            }else{
                                if(Sequel.cariRegistrasi(NoRawat)>0){
                                    JOptionPane.showMessageDialog(rootPane,"Data billing sudah terverifikasi ..!!");
                                }else{ 
                                    Sequel.queryu("delete from antriapotek2");
                                    Sequel.queryu("insert into antriapotek2 values('"+NoResep+"','1','"+NoRawat+"')");
                                    panggilform();                             
                                }
                            }               
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }
            }else if(TabRawatJalan.getSelectedIndex()==1){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }
        }else if(TabPilihRawat.getSelectedIndex()==1){
            if(TabRawatInap.getSelectedIndex()==0){
                if(akses.getberi_obat()==true){
                    if(tabMode3.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau divalidasi..!!");
                    }else{
                        if(Status.equals("Sudah Terlayani")){
                            JOptionPane.showMessageDialog(rootPane,"Resep sudah tervalidasi ..!!");
                        }else {                           
                            if(Sequel.cariRegistrasi(NoRawat)>0){
                                JOptionPane.showMessageDialog(rootPane,"Data billing sudah terverifikasi ..!!");
                            }else{ 
                                panggilform2();                             
                            }                
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }
            }else if(TabRawatInap.getSelectedIndex()==1){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }else if(TabRawatInap.getSelectedIndex()==2){
                if(akses.getstok_obat_pasien()==true){
                    if(tabMode5.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan stok pasien yang mau divalidasi..!!");
                    }else{
                        if(Status.equals("Sudah Terlayani")){
                            JOptionPane.showMessageDialog(rootPane,"Permintaan Stok Pasien sudah tervalidasi ..!!");
                        }else {                           
                            if(Sequel.cariRegistrasi(NoRawat)>0){
                                JOptionPane.showMessageDialog(rootPane,"Data billing sudah terverifikasi ..!!");
                            }else{ 
                                panggilform3();                             
                            }                
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }
            } else if(TabRawatInap.getSelectedIndex()==3){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Permintaan Stok...!!!!");
                TCari.requestFocus();
            }else if(TabRawatInap.getSelectedIndex()==4){
                if(akses.getresep_pulang()==true){
                    if(tabMode7.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan resep pulang yang mau divalidasi..!!");
                    }else{
                        if(Status.equals("Sudah Terlayani")){
                            JOptionPane.showMessageDialog(rootPane,"Permintaan Resep Pulang sudah tervalidasi ..!!");
                        }else {                           
                            if(Sequel.cariRegistrasi(NoRawat)>0){
                                JOptionPane.showMessageDialog(rootPane,"Data billing sudah terverifikasi ..!!");
                            }else{ 
                                panggilform4();                             
                            }                
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }
            } else if(TabRawatInap.getSelectedIndex()==5){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Permintaan Resep Pulang...!!!!");
                TCari.requestFocus();
            }      
        }            
}//GEN-LAST:event_BtnTambahActionPerformed

    private void BtnTambahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnTambahKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnTambahActionPerformed(null);
        }else{
           Valid.pindah(evt,BtnEdit,BtnKeluar);
        }
}//GEN-LAST:event_BtnTambahKeyPressed
/*
private void KdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TKdKeyPressed
    Valid.pindah(evt,BtnCari,Nm);
}//GEN-LAST:event_TKdKeyPressed
*/

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        pilihTab();
    }//GEN-LAST:event_formWindowOpened

    private void TabRawatJalanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatJalanMouseClicked
        pilihRalan();
        DlgCatatan.dispose();   //CUSTOM MUHSIN
    }//GEN-LAST:event_TabRawatJalanMouseClicked

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TabPilihRawat.getSelectedIndex()==0){
            if(TabRawatJalan.getSelectedIndex()==0){
                if(tabMode.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                    TCari.requestFocus();
                }else if(NoRawat.equals("")){
                    JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diubah..!!");
                }else{
                    if(Status.equals("Sudah Terlayani")){
                        JOptionPane.showMessageDialog(rootPane,"Resep sudah tervalidasi ..!!");
                    }else {
                        DlgPeresepanDokter resep=new DlgPeresepanDokter(null,false);
                        resep.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
                        resep.setLocationRelativeTo(internalFrame1);
                        resep.MatikanJam();
                        resep.setNoRm(NoRawat,Valid.SetTgl2(TglPeresepan),JamPeresepan.substring(0,2),JamPeresepan.substring(3,5),JamPeresepan.substring(6,8),KodeDokter,DokterPeresep,"ralan");
                        resep.isCek();
                        resep.tampilobat(NoResep);
                        TeksKosong();
                        resep.setVisible(true);                         
                    }                    
                }
            }else if(TabRawatJalan.getSelectedIndex()==1){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }
        }else if(TabPilihRawat.getSelectedIndex()==1){
            if(TabRawatInap.getSelectedIndex()==0){
                if(tabMode3.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                    TCari.requestFocus();
                }else if(NoRawat.equals("")){
                    JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau divalidasi..!!");
                }else{
                    if(Status.equals("Sudah Terlayani")){
                        JOptionPane.showMessageDialog(rootPane,"Resep sudah tervalidasi ..!!");
                    }else {
                        kamar=Sequel.cariIsi("select kd_bangsal from kamar inner join kamar_inap on kamar_inap.kd_kamar=kamar.kd_kamar "+
                                "where kamar_inap.no_rawat=? order by kamar_inap.tgl_masuk desc limit 1 ",NoRawat); 
                        bangsal=Sequel.cariIsi("select kd_depo from set_depo_ranap where kd_bangsal=?",kamar);
                        if(bangsal.equals("")){
                            if(Sequel.cariIsi("select asal_stok from set_lokasi").equals("Gunakan Stok Bangsal")){
                                akses.setkdbangsal(kamar);
                            }else{
                                akses.setkdbangsal(Sequel.cariIsi("select kd_bangsal from set_lokasi"));
                            }
                        }else{
                            akses.setkdbangsal(bangsal);
                        }
                        DlgPeresepanDokter resep=new DlgPeresepanDokter(null,false);
                        resep.setSize(internalFrame1.getWidth(),internalFrame1.getHeight());
                        resep.setLocationRelativeTo(internalFrame1);
                        resep.MatikanJam();
                        resep.setNoRm(NoRawat,Valid.SetTgl2(TglPeresepan),JamPeresepan.substring(0,2),JamPeresepan.substring(3,5),JamPeresepan.substring(6,8),KodeDokter,DokterPeresep,"ranap");
                        resep.isCek();
                        resep.tampilobat(NoResep);
                        TeksKosong();
                        resep.setVisible(true);                          
                    }                    
                }
            }else if(TabRawatInap.getSelectedIndex()==1){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }else if(TabRawatInap.getSelectedIndex()==2){
                if(tabMode5.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                    TCari.requestFocus();
                }else if(NoRawat.equals("")){
                    JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan stok yang mau divalidasi..!!");
                }else{
                    if(Status.equals("Sudah Terlayani")){
                        JOptionPane.showMessageDialog(rootPane,"Permintaan stok sudah tervalidasi ..!!");
                    }else {
                        kamar=Sequel.cariIsi("select kd_bangsal from kamar inner join kamar_inap on kamar_inap.kd_kamar=kamar.kd_kamar "+
                                "where kamar_inap.no_rawat=? order by kamar_inap.tgl_masuk desc limit 1 ",NoRawat); 
                        bangsal=Sequel.cariIsi("select kd_depo from set_depo_ranap where kd_bangsal=?",kamar);
                        if(bangsal.equals("")){
                            if(Sequel.cariIsi("select asal_stok from set_lokasi").equals("Gunakan Stok Bangsal")){
                                akses.setkdbangsal(kamar);
                            }else{
                                akses.setkdbangsal(Sequel.cariIsi("select kd_bangsal from set_lokasi"));
                            }
                        }else{
                            akses.setkdbangsal(bangsal);
                        }
                        DlgPermintaanStokPasien permintaan=new DlgPermintaanStokPasien(null,false);
                        permintaan.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
                        permintaan.setLocationRelativeTo(internalFrame1);
                        permintaan.isCek();
                        permintaan.setNoRm(NoRawat,DTPCari1.getDate()); 
                        permintaan.tampilObat(NoResep);
                        permintaan.setVisible(true);
                        this.setCursor(Cursor.getDefaultCursor());
                        TeksKosong();
                        permintaan.setVisible(true);                          
                    }                    
                }
            }else if(TabRawatInap.getSelectedIndex()==3){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Permintaan Stok...!!!!");
                TCari.requestFocus();
            }else if(TabRawatInap.getSelectedIndex()==4){
                if(tabMode7.getRowCount()==0){
                    JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                    TCari.requestFocus();
                }else if(NoRawat.equals("")){
                    JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan stok yang mau divalidasi..!!");
                }else{
                    if(Status.equals("Sudah Terlayani")){
                        JOptionPane.showMessageDialog(rootPane,"Permintaan stok sudah tervalidasi ..!!");
                    }else {
                        kamar=Sequel.cariIsi("select kd_bangsal from kamar inner join kamar_inap on kamar_inap.kd_kamar=kamar.kd_kamar "+
                                "where kamar_inap.no_rawat=? order by kamar_inap.tgl_masuk desc limit 1 ",NoRawat); 
                        bangsal=Sequel.cariIsi("select kd_depo from set_depo_ranap where kd_bangsal=?",kamar);
                        if(bangsal.equals("")){
                            if(Sequel.cariIsi("select asal_stok from set_lokasi").equals("Gunakan Stok Bangsal")){
                                akses.setkdbangsal(kamar);
                            }else{
                                akses.setkdbangsal(Sequel.cariIsi("select kd_bangsal from set_lokasi"));
                            }
                        }else{
                            akses.setkdbangsal(bangsal);
                        }
                        DlgPermintaanResepPulang permintaan=new DlgPermintaanResepPulang(null,false);
                        permintaan.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
                        permintaan.setLocationRelativeTo(internalFrame1);
                        permintaan.isCek();
                        permintaan.setNoRm(NoRawat,DTPCari1.getDate()); 
                        permintaan.tampilObat(NoResep);
                        permintaan.setVisible(true);
                        this.setCursor(Cursor.getDefaultCursor());
                        TeksKosong();
                        permintaan.setVisible(true);                          
                    }                    
                }
            }else if(TabRawatInap.getSelectedIndex()==5){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Permintaan Resep...!!!!");
                TCari.requestFocus();
            }  
        }
            
    }//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnTambah, BtnPrint);
        }
    }//GEN-LAST:event_BtnEditKeyPressed

    private void BtnRekapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRekapActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        DlgResepObat resep=new DlgResepObat(null,false);
        resep.tampil();
        resep.emptTeks();
        resep.isCek();
        resep.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        resep.setLocationRelativeTo(internalFrame1);
        resep.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_BtnRekapActionPerformed

    private void BtnRekapKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnRekapKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnRekapKeyPressed

    private void TabPilihRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabPilihRawatMouseClicked
        pilihTab();
        DlgCatatan.dispose();   //CUSTOM MUHSIN
    }//GEN-LAST:event_TabPilihRawatMouseClicked

    private void tbResepRanapMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbResepRanapMouseClicked
        if(tabMode3.getRowCount()!=0){
            try {
                getData2();
            } catch (java.lang.NullPointerException e) {
            }
            if(evt.getClickCount()==2){
                if(akses.getberi_obat()==true){
                    //START CUSTOM MUHSIN
                    //BtnTambahActionPerformed(null);
                    String jabatan=Sequel.cariIsi("select nm_jbtn from jabatan where kd_jbtn=?",Sequel.cariIsi("select kd_jbtn from petugas where nip=?",akses.getkode()));
                    if(jabatan.equalsIgnoreCase("IT")||jabatan.equalsIgnoreCase("APOTEKER")||jabatan.equalsIgnoreCase("ASISTEN APOTEKER")){
                        if(akses.getberi_obat()==true){
                        BtnTambahActionPerformed(null);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"User login bukan Apoteker...!!");
                    }
                    //END CUSTOM
                }
            }
        }
    }//GEN-LAST:event_tbResepRanapMouseClicked

    private void tbResepRanapKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbResepRanapKeyPressed
        if(tabMode3.getRowCount()!=0){
            if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData2();
                } catch (java.lang.NullPointerException e) {
                }
                if(akses.getberi_obat()==true){
                    //START CUSTOM MUHSIN
                    //BtnTambahActionPerformed(null);
                    String jabatan=Sequel.cariIsi("select nm_jbtn from jabatan where kd_jbtn=?",Sequel.cariIsi("select kd_jbtn from petugas where nip=?",akses.getkode()));
                    if(jabatan.equalsIgnoreCase("IT")||jabatan.equalsIgnoreCase("APOTEKER")||jabatan.equalsIgnoreCase("ASISTEN APOTEKER")){
                        if(akses.getberi_obat()==true){
                        BtnTambahActionPerformed(null);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"User login bukan Apoteker...!!");
                    }
                    //END CUSTOM
                }                    
            }
        }
    }//GEN-LAST:event_tbResepRanapKeyPressed

    private void TabRawatInapMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatInapMouseClicked
        pilihRanap();
    }//GEN-LAST:event_TabRawatInapMouseClicked

    private void BtnSeek3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeek3ActionPerformed
        dokter.isCek();
        dokter.TCari.requestFocus();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnSeek3ActionPerformed

    private void BtnSeek4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeek4ActionPerformed
        poli.isCek();
        poli.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        poli.setLocationRelativeTo(internalFrame1);
        poli.setVisible(true);
    }//GEN-LAST:event_BtnSeek4ActionPerformed

    private void BtnSeek5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeek5ActionPerformed
        dokter.isCek();
        dokter.TCari.requestFocus();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnSeek5ActionPerformed

    private void BtnSeek6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeek6ActionPerformed
        ruang.isCek();
        ruang.emptTeks();        
        ruang.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        ruang.setLocationRelativeTo(internalFrame1);
        ruang.setVisible(true);
    }//GEN-LAST:event_BtnSeek6ActionPerformed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(TabPilihRawat.getSelectedIndex()==0){
            if(TabRawatJalan.getSelectedIndex()==0){
                if(akses.getresep_dokter()==true){
                    if(tabMode.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau dihapus..!!");
                    }else{
                        if(Status.equals("Sudah Terlayani")){
                            JOptionPane.showMessageDialog(rootPane,"Resep sudah tervalidasi ..!!");
                        }else {
                            Sequel.meghapus("resep_obat","no_resep",NoResep);    
                            TeksKosong();
                            tampil();
                        }                    
                    }
                }
            }else if(TabRawatJalan.getSelectedIndex()==1){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }
        }else if(TabPilihRawat.getSelectedIndex()==1){
            if(TabRawatInap.getSelectedIndex()==0){
                if(akses.getresep_dokter()==true){
                    if(tabMode3.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau dihapus..!!");
                    }else{
                        if(Status.equals("Sudah Terlayani")){
                            JOptionPane.showMessageDialog(rootPane,"Resep sudah tervalidasi ..!!");
                        }else {
                            Sequel.meghapus("resep_obat","no_resep",NoResep); 
                            TeksKosong();
                            tampil3();
                        }                    
                    }
                }
            }else if(TabRawatInap.getSelectedIndex()==1){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }else if(TabRawatInap.getSelectedIndex()==2){
                if(akses.getpermintaan_stok_obat_pasien()==true){
                    if(tabMode5.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan stok yang mau dihapus..!!");
                    }else{
                        if(Status.equals("Sudah Terlayani")){
                            JOptionPane.showMessageDialog(rootPane,"permintaan stok sudah tervalidasi ..!!");
                        }else {
                            Sequel.meghapus("permintaan_stok_obat_pasien","no_permintaan",NoResep); 
                            TeksKosong();
                            tampil5();
                        }                    
                    }
                }
            }else if(TabRawatInap.getSelectedIndex()==3){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Permintaan Stok...!!!!");
                TCari.requestFocus();
            }else if(TabRawatInap.getSelectedIndex()==4){
                if(akses.getpermintaan_resep_pulang()==true){
                    if(tabMode7.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan stok yang mau dihapus..!!");
                    }else{
                        if(Status.equals("Sudah Terlayani")){
                            JOptionPane.showMessageDialog(rootPane,"permintaan resep pulang sudah tervalidasi ..!!");
                        }else {
                            Sequel.meghapus("permintaan_resep_pulang","no_permintaan",NoResep); 
                            TeksKosong();
                            tampil7();
                        }                    
                    }
                }
            }else if(TabRawatInap.getSelectedIndex()==5){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Permintaan Resep Pulag...!!!!");
                TCari.requestFocus();
            }   
        }
    }//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            BtnHapusActionPerformed(null);
        } else {
            Valid.pindah(evt, BtnHapus, BtnEdit);
        }
    }//GEN-LAST:event_BtnHapusKeyPressed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        aktif=true;
    }//GEN-LAST:event_formWindowActivated

    private void formWindowDeactivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowDeactivated
        aktif=false;
    }//GEN-LAST:event_formWindowDeactivated

    private void tbPermintaanStokMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPermintaanStokMouseClicked
        if(tabMode5.getRowCount()!=0){
            try {
                getData3();
            } catch (java.lang.NullPointerException e) {
            }
            if(evt.getClickCount()==2){
                if(akses.getstok_obat_pasien()==true){
                    //START CUSTOM MUHSIN
                    //BtnTambahActionPerformed(null);
                    String jabatan=Sequel.cariIsi("select nm_jbtn from jabatan where kd_jbtn=?",Sequel.cariIsi("select kd_jbtn from petugas where nip=?",akses.getkode()));
                    if(jabatan.equalsIgnoreCase("IT")||jabatan.equalsIgnoreCase("APOTEKER")||jabatan.equalsIgnoreCase("ASISTEN APOTEKER")){
                        if(akses.getberi_obat()==true){
                        BtnTambahActionPerformed(null);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"User login bukan Apoteker...!!");
                    }
                    //END CUSTOM
                }
            }
        }
    }//GEN-LAST:event_tbPermintaanStokMouseClicked

    private void tbPermintaanStokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPermintaanStokKeyPressed
        if(tabMode5.getRowCount()!=0){
            if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData3();
                } catch (java.lang.NullPointerException e) {
                }
                if(akses.getstok_obat_pasien()==true){
                    //START CUSTOM MUHSIN
                    //BtnTambahActionPerformed(null);
                    String jabatan=Sequel.cariIsi("select nm_jbtn from jabatan where kd_jbtn=?",Sequel.cariIsi("select kd_jbtn from petugas where nip=?",akses.getkode()));
                    if(jabatan.equalsIgnoreCase("IT")||jabatan.equalsIgnoreCase("APOTEKER")||jabatan.equalsIgnoreCase("ASISTEN APOTEKER")){
                        if(akses.getberi_obat()==true){
                        BtnTambahActionPerformed(null);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"User login bukan Apoteker...!!");
                    }
                    //END CUSTOM
                }                    
            }
        }
    }//GEN-LAST:event_tbPermintaanStokKeyPressed

    private void BtnPenyerahanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPenyerahanActionPerformed
        if(TabPilihRawat.getSelectedIndex()==0){
            if(TabRawatJalan.getSelectedIndex()==0){
                if(akses.getberi_obat()==true){
                    if(tabMode.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                    }else{
                        //Sequel.queryu("delete from antriapotek3");
                        //Sequel.queryu("insert into antriapotek3 values('"+NoResep+"','1','"+NoRawat+"')");
                        //Sequel.queryu("delete from bukti_penyerahan_resep_obat where no_resep='"+NoResep+"'");
                        
                        //START CUSTOM MUHSIN
                        if(tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),12).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Resep obat belum divalidasi !");
                        }else if(!(tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),14).toString().isBlank())){
                            JOptionPane.showMessageDialog(null,"Resep obat sudah jadi !");
                        }else if(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString()).isEmpty()){
                            JOptionPane.showMessageDialog(null,"Maaf, telaah resep belum dilakukan!");
                        }else{
                            LocalDate now_date =  java.time.LocalDate.now();
                            waktu = now_date.toString();
                            String waktu_jadi = jam_jadi+":"+menit_jadi+":"+detik_jadi;
                            jLabel27.setText(waktu_jadi);
                            jLabel34.setText(tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString());
                            
                            KdEntri.setText(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString()));
                            CrEntri.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri.getText()));
                            KdSerahKIE.setText(akses.getkode().toString());
                            CrSerahKIE.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE.getText()));
                            //AMBIL PETUGAS DARI JADWAL FARMASI
                            KdAmbil.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi where bagian='AMBIL' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrAmbil.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil.getText()));
                            KdCek.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi where bagian='CEK' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrCek.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek.getText()));
                            KdRacikan.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi where bagian='RACIKAN' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrRacikan.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan.getText()));
                            KdKonseling.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi where bagian='KONSELING' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrKonseling.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling.getText()));
                            
                            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));  
                            KetResep.setText("-");
                            tampilObatKronis(tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),4).toString(),tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),3).toString());
                            WindowPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                            WindowPetugasFarmasi.setVisible(true);
                            this.setCursor(Cursor.getDefaultCursor());
                        }
                        //END CUSTOM
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }
            }else if(TabRawatJalan.getSelectedIndex()==1){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }
        }else if(TabPilihRawat.getSelectedIndex()==1){
            //START CUSTOM -> RANAP BISA PENYERAHAN RESEP
//            JOptionPane.showMessageDialog(null,"Maaf, Penyerahan resep hanya untuk rawat jalan..!!!!");
//            TCari.requestFocus();
            if(TabRawatInap.getSelectedIndex()==0){
                if(akses.getberi_obat()==true){
                    if(tabMode3.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                    }else{
                        //Sequel.queryu("delete from antriapotek3");
                        //Sequel.queryu("insert into antriapotek3 values('"+NoResep+"','1','"+NoRawat+"')");
                        //Sequel.queryu("delete from bukti_penyerahan_resep_obat where no_resep='"+NoResep+"'");
                        
                        if(tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),12).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Resep obat belum divalidasi !");
                        }else if(!(tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),14).toString().isBlank())){
                            JOptionPane.showMessageDialog(null,"Resep obat sudah jadi !");
                        }else if(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()).isEmpty()){
                            JOptionPane.showMessageDialog(null,"Maaf, telaah resep belum dilakukan!");
                        }else{
                            LocalDate now_date =  java.time.LocalDate.now();
                            waktu = now_date.toString();
                            String waktu_jadi = jam_jadi+":"+menit_jadi+":"+detik_jadi;
                            jLabel27.setText(waktu_jadi);
                            jLabel34.setText(tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString());
                            
                            KdEntri.setText(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                            CrEntri.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri.getText()));
                            KdSerahKIE.setText(akses.getkode().toString());
                            CrSerahKIE.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE.getText()));
                            //AMBIL PETUGAS DARI JADWAL FARMASI
                            KdAmbil.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='AMBIL' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrAmbil.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil.getText()));
                            KdCek.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='CEK' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrCek.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek.getText()));
                            KdRacikan.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='RACIKAN' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrRacikan.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan.getText()));
                            KdKonseling.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='KONSELING' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrKonseling.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling.getText()));
                            
                            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)); 
                            KetResep.setText("-");
                            WindowPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                            WindowPetugasFarmasi.setVisible(true);
                            this.setCursor(Cursor.getDefaultCursor());
                        }
                        //END CUSTOM
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }
            }else if(TabRawatInap.getSelectedIndex()==4){
                if(akses.getberi_obat()==true){
                    if(tabMode7.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                    }else{
                        //Sequel.queryu("delete from antriapotek3");
                        //Sequel.queryu("insert into antriapotek3 values('"+NoResep+"','1','"+NoRawat+"')");
                        //Sequel.queryu("delete from bukti_penyerahan_resep_obat where no_resep='"+NoResep+"'");
                        
                        if(tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),12).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Resep pulang belum divalidasi !");
                        }else if(!(tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),14).toString().isBlank())){
                            JOptionPane.showMessageDialog(null,"Resep pulang sudah jadi !");
                        }
//                        else if(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString()).isEmpty()){
//                            JOptionPane.showMessageDialog(null,"Maaf, telaah resep belum dilakukan!");
//                        }
                        else{
                            LocalDate now_date =  java.time.LocalDate.now();
                            waktu = now_date.toString();
                            String waktu_jadi = jam_jadi+":"+menit_jadi+":"+detik_jadi;
                            jLabel27.setText(waktu_jadi);
                            jLabel34.setText(tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString());
                            
                            KdEntri.setText("-");
                            CrEntri.setText("-");
                            KdSerahKIE.setText(akses.getkode().toString());
                            CrSerahKIE.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE.getText()));
                            //AMBIL PETUGAS DARI JADWAL FARMASI
                            KdAmbil.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='AMBIL' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrAmbil.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil.getText()));
                            KdCek.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='CEK' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrCek.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek.getText()));
                            KdRacikan.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='RACIKAN' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrRacikan.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan.getText()));
                            KdKonseling.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='KONSELING' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrKonseling.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling.getText()));
                            
                            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));  
                            KetResep.setText("Resep Pulang");
                            WindowPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                            WindowPetugasFarmasi.setVisible(true);
                            this.setCursor(Cursor.getDefaultCursor());
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }                
            }else if(TabRawatInap.getSelectedIndex()==2){
                if(akses.getberi_obat()==true){
                    if(tabMode5.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan stok yang mau diserahkan..!!");
                    }else{
                        //Sequel.queryu("delete from antriapotek3");
                        //Sequel.queryu("insert into antriapotek3 values('"+NoResep+"','1','"+NoRawat+"')");
                        //Sequel.queryu("delete from bukti_penyerahan_resep_obat where no_resep='"+NoResep+"'");
                        
                        if(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),13).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Permintaan stok belum divalidasi !");
                        }else if(!(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),15).toString().isBlank())){
                            JOptionPane.showMessageDialog(null,"Permintaan stok sudah jadi !");
                        }
//                        else if(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString()).isEmpty()){
//                            JOptionPane.showMessageDialog(null,"Maaf, telaah resep belum dilakukan!");
//                        }
                        else{
                            LocalDate now_date =  java.time.LocalDate.now();
                            waktu = now_date.toString();
                            String waktu_jadi = jam_jadi+":"+menit_jadi+":"+detik_jadi;
                            jLabel27.setText(waktu_jadi);
                            jLabel34.setText(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString());
                            
                            KdEntri.setText("-");
                            CrEntri.setText("-");
                            KdSerahKIE.setText(akses.getkode().toString());
                            CrSerahKIE.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE.getText()));
                            //AMBIL PETUGAS DARI JADWAL FARMASI
                            KdAmbil.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='AMBIL' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrAmbil.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil.getText()));
                            KdCek.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='CEK' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrCek.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek.getText()));
                            KdRacikan.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='RACIKAN' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrRacikan.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan.getText()));
                            KdKonseling.setText(Sequel.cariIsi("select kd_petugas from jadwal_farmasi_ranap where bagian='KONSELING' and hari_kerja=?",now_date.getDayOfWeek().toString()));
                            CrKonseling.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling.getText()));
                            
                            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));  
                            KetResep.setText("Permintaan Stok");
                            WindowPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                            WindowPetugasFarmasi.setVisible(true);
                            this.setCursor(Cursor.getDefaultCursor());
                        }
                    }
                }
            }else {
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }
            //END CUSTOM
        }
    }//GEN-LAST:event_BtnPenyerahanActionPerformed

    private void BtnPenyerahanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPenyerahanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPenyerahanActionPerformed(null);
        }else{
           Valid.pindah(evt,BtnHapus,BtnPrint);
        }
    }//GEN-LAST:event_BtnPenyerahanKeyPressed

    private void tbPermintaanResepPulangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPermintaanResepPulangMouseClicked
        if(tabMode7.getRowCount()!=0){
            try {
                getData4();
            } catch (java.lang.NullPointerException e) {
            }
            if(evt.getClickCount()==2){
                if(akses.getresep_pulang()==true){
                    //START CUSTOM MUHSIN
                    //BtnTambahActionPerformed(null);
                    String jabatan=Sequel.cariIsi("select nm_jbtn from jabatan where kd_jbtn=?",Sequel.cariIsi("select kd_jbtn from petugas where nip=?",akses.getkode()));
                    if(jabatan.equalsIgnoreCase("IT")||jabatan.equalsIgnoreCase("APOTEKER")||jabatan.equalsIgnoreCase("ASISTEN APOTEKER")){
                        if(akses.getberi_obat()==true){
                        BtnTambahActionPerformed(null);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"User login bukan Apoteker...!!");
                    }
                    //END CUSTOM
                }
            }
        }
    }//GEN-LAST:event_tbPermintaanResepPulangMouseClicked

    private void tbPermintaanResepPulangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbPermintaanResepPulangKeyPressed
        if(tabMode7.getRowCount()!=0){
            if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData4();
                } catch (java.lang.NullPointerException e) {
                }
                if(akses.getresep_pulang()==true){
                    //START CUSTOM MUHSIN
                    //BtnTambahActionPerformed(null);
                    String jabatan=Sequel.cariIsi("select nm_jbtn from jabatan where kd_jbtn=?",Sequel.cariIsi("select kd_jbtn from petugas where nip=?",akses.getkode()));
                    if(jabatan.equalsIgnoreCase("IT")||jabatan.equalsIgnoreCase("APOTEKER")||jabatan.equalsIgnoreCase("ASISTEN APOTEKER")){
                        if(akses.getberi_obat()==true){
                        BtnTambahActionPerformed(null);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"User login bukan Apoteker...!!");
                    }
                    //END CUSTOM
                }                    
            }
        }
    }//GEN-LAST:event_tbPermintaanResepPulangKeyPressed
//START CUSTOM
    private void BtnSalinanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSalinanActionPerformed
        // TODO add your handling code here:
        //RAWAT JALAN
        if(TabPilihRawat.getSelectedIndex()==0){
            if(TabRawatJalan.getSelectedIndex()==0){
                if(akses.getberi_obat()==true){
                    if(tabMode.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau divalidasi..!!");
                    }else{
                        //CETAK SALINAN
                        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        Sequel.queryu("truncate table temporary_resep");
                        try {
                            ps=koneksi.prepareStatement(
                                "select d.nama_brng,aturan_pakai,jml,satuan from resep_dokter " +
                                "inner join databarang d on resep_dokter.kode_brng = d.kode_brng " +
                                "inner join kodesatuan k on d.kode_sat = k.kode_sat " +
                                "where no_resep=?");
                            try {
                                ps.setString(1,NoResep);
                                rs=ps.executeQuery();
                                while(rs.next()){
                                    Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                                        "0",rs.getString("nama_brng"),rs.getString("aturan_pakai"),rs.getString("jml"),rs.getString("satuan"),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notif 1 : "+e);
                            } finally{
                                if(rs!=null){
                                    rs.close();
                                }
                                if(ps!=null){
                                    ps.close();
                                }
                            }

                            ps3=koneksi.prepareStatement(
                                "select nama_racik,jml_dr,nm_racik,aturan_pakai,no_racik from resep_dokter_racikan " +
                                "inner join metode_racik mr on resep_dokter_racikan.kd_racik = mr.kd_racik " +
                                "where no_resep=?");
                            try {
                                ps3.setString(1,NoResep);
                                rs3=ps3.executeQuery();
                                while(rs3.next()){
                                    String rincianobat="";
                                    ps2=koneksi.prepareStatement(
                                        "select d.nama_brng,jml from resep_dokter_racikan_detail " +
                                        "inner join databarang d on resep_dokter_racikan_detail.kode_brng = d.kode_brng " +
                                        "where no_resep=? and no_racik=? order by d.kode_brng");
                                    try {
                                        ps2.setString(1,NoResep);
                                        ps2.setString(2,rs3.getString("no_racik"));
                                        rs2=ps2.executeQuery();
                                        int total=0;
                                        while(rs2.next()){
                                            rincianobat=rs2.getString("nama_brng")+" "+rs2.getString("jml")+","+rincianobat;
                                        }                                
                                    } catch (Exception e) {
                                        System.out.println("Notifikasi Detail Racikan : "+e);
                                    } finally{
                                        if(rs2!=null){
                                            rs2.close();
                                        }
                                        if(ps2!=null){
                                            ps2.close();
                                        }
                                    }
                                    //START CUSTOM MUHSIN -> SOLUSI SEMENTARA UNTUK TETAP PRINT NAMA RACIKAN
                                    if(rincianobat.equals("")){
                                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                                        "0",rs3.getString("nama_racik")+" ("+rincianobat+")",rs3.getString("aturan_pakai"),rs3.getString("jml_dr"),rs3.getString("nm_racik"),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                                    });
                                    }else{
                                        rincianobat = rincianobat.substring(0,rincianobat.length() - 1);
                                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                                            "0",rs3.getString("nama_racik")+" ("+rincianobat+")",rs3.getString("aturan_pakai"),rs3.getString("jml_dr"),rs3.getString("nm_racik"),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                                        });
                                    }
                                    //END CUSTOM
                                }
                            } catch (Exception e) {
                                System.out.println("Notif Racikan : "+e);
                            } finally{
                                if(rs3!=null){
                                    rs3.close();
                                }
                                if(ps3!=null){
                                    ps3.close();
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notif : "+e);
                        }
            
                        Map<String, Object> param = new HashMap<>();  
                        param.put("namars",akses.getnamars());
                        param.put("alamatrs",akses.getalamatrs());
                        param.put("kotars",akses.getkabupatenrs());
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("emailrs",akses.getemailrs());
                        param.put("kontakrs",akses.getkontakrs());
                        param.put("penanggung",Sequel.cariIsi("select png_jawab from penjab where kd_pj=?",Sequel.cariIsi("select kd_pj from reg_periksa where no_rawat=?",NoRawat)));               
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("apoteker", Sequel.cariIsi("select nama from pegawai where nik=?", akses.getkode()));
                        param.put("norawat",NoRawat);
                        param.put("pasien",Pasien);
                        param.put("norm",NoRM);
                        param.put("peresep",DokterPeresep);
                        param.put("noresep",NoResep);
                        param.put("logo",Sequel.cariGambar("select logo from setting"));
                        param.put("tgl_lahir",Sequel.cariIsi("select DATE_FORMAT(tgl_lahir,'%d-%m-%Y') from pasien where no_rkm_medis=?",NoRM));
                        param.put("jam_peresepan",Sequel.cariIsi("select jam_peresepan from resep_obat where no_resep=?",NoResep));
                        param.put("jam_pelayanan",Sequel.cariIsi("select jam from resep_obat where no_resep=?",NoResep));
                        param.put("alamat",Sequel.cariIsi("select concat(pasien.alamat,', ',k.nm_kel,', ',k2.nm_kec,', ',k3.nm_kab) as alamat\n" +
                            "from pasien\n" +
                            "inner join kelurahan k on pasien.kd_kel = k.kd_kel\n" +
                            "inner join kecamatan k2 on pasien.kd_kec = k2.kd_kec\n" +
                            "inner join kabupaten k3 on pasien.kd_kab = k3.kd_kab\n" +
                            "where no_rkm_medis=?",NoRM));
                                //if reg periksa stts lanjut ranap
                            String kamarinap = Sequel.cariIsi("select concat(ki.kd_kamar,' ',b.nm_bangsal) as kamar from\n" +
                                "reg_periksa\n" +
                                "inner join kamar_inap ki on reg_periksa.no_rawat = ki.no_rawat\n" +
                                "inner join kamar k on ki.kd_kamar = k.kd_kamar\n" +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal\n" +
                                "where status_lanjut='ranap' and ki.no_rawat=?",NoRawat);
                            if(kamarinap.equalsIgnoreCase("")){
                                //kosong
                                param.put("kamarinap","");
                            }else{
                                //ranap
                                param.put("kamarinap","Kamar    : "+kamarinap);
                            }
                        Valid.MyReport("rptSalinanResep.jasper",param,"::[ Copy Resep ]::");
                        this.setCursor(Cursor.getDefaultCursor());
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                        TCari.requestFocus();
                    }
                }else if(TabRawatJalan.getSelectedIndex()==1){
                    JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                    TCari.requestFocus();
                }
        }
        //RAWAT INAP
        else if(TabPilihRawat.getSelectedIndex()==1){
            if(TabRawatInap.getSelectedIndex()==0){ //RESEP RAWAT INAP
                if(akses.getberi_obat()==true){
                    if(tabMode3.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau divalidasi..!!");
                    }else{
                        //CETAK SALINAN RESEP
                        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        Sequel.queryu("truncate table temporary_resep");
                        try {
                            ps=koneksi.prepareStatement(
                                "select d.nama_brng,aturan_pakai,jml,satuan from resep_dokter " +
                                "inner join databarang d on resep_dokter.kode_brng = d.kode_brng " +
                                "inner join kodesatuan k on d.kode_sat = k.kode_sat " +
                                "where no_resep=?");
                            try {
                                ps.setString(1,NoResep);
                                rs=ps.executeQuery();
                                while(rs.next()){
                                    Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                                        "0",rs.getString("nama_brng"),rs.getString("aturan_pakai"),rs.getString("jml"),rs.getString("satuan"),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notif 1 : "+e);
                            } finally{
                                if(rs!=null){
                                    rs.close();
                                }
                                if(ps!=null){
                                    ps.close();
                                }
                            }

                            ps3=koneksi.prepareStatement(
                                "select nama_racik,jml_dr,nm_racik,aturan_pakai from resep_dokter_racikan " +
                                "inner join metode_racik mr on resep_dokter_racikan.kd_racik = mr.kd_racik " +
                                "where no_resep=?");
                            try {
                                ps3.setString(1,NoResep);
                                rs3=ps3.executeQuery();
                                while(rs3.next()){
                                    String rincianobat="";
                                    ps2=koneksi.prepareStatement(
                                        "select d.nama_brng,jml from resep_dokter_racikan_detail " +
                                        "inner join databarang d on resep_dokter_racikan_detail.kode_brng = d.kode_brng " +
                                        "where no_resep=? order by d.kode_brng");
                                    try {
                                        ps2.setString(1,NoResep);
                                        rs2=ps2.executeQuery();
                                        int total=0;
                                        while(rs2.next()){
                                            rincianobat=rs2.getString("nama_brng")+" "+rs2.getString("jml")+","+rincianobat;
                                        }                                
                                    } catch (Exception e) {
                                        System.out.println("Notifikasi Detail Racikan : "+e);
                                    } finally{
                                        if(rs2!=null){
                                            rs2.close();
                                        }
                                        if(ps2!=null){
                                            ps2.close();
                                        }
                                    }
                                    //START CUSTOM -> SOLUSI SEMENTARA UNTUK TETAP PRINT NAMA RACIKAN
                                    if(rincianobat.equals("")){
                                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                                        "0",rs3.getString("nama_racik")+" ("+rincianobat+")",rs3.getString("aturan_pakai"),rs3.getString("jml_dr"),rs3.getString("nm_racik"),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                                    });
                                    }else{
                                        rincianobat = rincianobat.substring(0,rincianobat.length() - 1);
                                        Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                                            "0",rs3.getString("nama_racik")+" ("+rincianobat+")",rs3.getString("aturan_pakai"),rs3.getString("jml_dr"),rs3.getString("nm_racik"),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                                        });
                                    }
                                    //END CUSTOM
                                }
                            } catch (Exception e) {
                                System.out.println("Notif Racikan : "+e);
                            } finally{
                                if(rs3!=null){
                                    rs3.close();
                                }
                                if(ps3!=null){
                                    ps3.close();
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notif : "+e);
                        }
            
                        Map<String, Object> param = new HashMap<>();  
                        param.put("namars",akses.getnamars());
                        param.put("alamatrs",akses.getalamatrs());
                        param.put("kotars",akses.getkabupatenrs());
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("emailrs",akses.getemailrs());
                        param.put("kontakrs",akses.getkontakrs());
                        param.put("penanggung",Sequel.cariIsi("select png_jawab from penjab where kd_pj=?",Sequel.cariIsi("select kd_pj from reg_periksa where no_rawat=?",NoRawat)));               
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("apoteker", Sequel.cariIsi("select nama from pegawai where nik=?", akses.getkode()));
                        param.put("norawat",NoRawat);
                        param.put("pasien",Pasien);
                        param.put("norm",NoRM);
                        param.put("peresep",DokterPeresep);
                        param.put("noresep",NoResep);
                        param.put("logo",Sequel.cariGambar("select logo from setting"));
                        param.put("tgl_lahir",Sequel.cariIsi("select DATE_FORMAT(tgl_lahir,'%d-%m-%Y') from pasien where no_rkm_medis=?",NoRM));
                        param.put("jam_peresepan",Sequel.cariIsi("select jam_peresepan from resep_obat where no_resep=?",NoResep));
                        param.put("jam_pelayanan",Sequel.cariIsi("select jam from resep_obat where no_resep=?",NoResep));
                        param.put("alamat",Sequel.cariIsi("select concat(pasien.alamat,', ',k.nm_kel,', ',k2.nm_kec,', ',k3.nm_kab) as alamat\n" +
                            "from pasien\n" +
                            "inner join kelurahan k on pasien.kd_kel = k.kd_kel\n" +
                            "inner join kecamatan k2 on pasien.kd_kec = k2.kd_kec\n" +
                            "inner join kabupaten k3 on pasien.kd_kab = k3.kd_kab\n" +
                            "where no_rkm_medis=?",NoRM));
                                //if reg periksa stts lanjut ranap
                            String kamarinap = Sequel.cariIsi("select concat(ki.kd_kamar,' ',b.nm_bangsal) as kamar from\n" +
                                "reg_periksa\n" +
                                "inner join kamar_inap ki on reg_periksa.no_rawat = ki.no_rawat\n" +
                                "inner join kamar k on ki.kd_kamar = k.kd_kamar\n" +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal\n" +
                                "where status_lanjut='ranap' and ki.no_rawat=?",NoRawat);
                            if(kamarinap.equalsIgnoreCase("")){
                                //kosong
                                param.put("kamarinap","");
                            }else{
                                //ranap
                                param.put("kamarinap","Kamar    : "+kamarinap);
                            }
                        Valid.MyReport("rptSalinanResep.jasper",param,"::[ Copy Resep ]::");
                        this.setCursor(Cursor.getDefaultCursor());
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                    }
            }else if(TabRawatInap.getSelectedIndex()==1){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }else if(TabRawatInap.getSelectedIndex()==2){   //PERMINTAAN STOK
                if(akses.getstok_obat_pasien()==true){
                    if(tabMode5.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan stok pasien yang mau divalidasi..!!");
                    }else{
                        //CETAK SALINAN RESEP
                        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        Sequel.queryu("truncate table temporary_resep");
                        try {
                            ps=koneksi.prepareStatement(
                                "select databarang.kode_brng,databarang.nama_brng,detail_permintaan_stok_obat_pasien.jml," +
                                "databarang.kode_sat,detail_permintaan_stok_obat_pasien.aturan_pakai " +
                                "from detail_permintaan_stok_obat_pasien inner join databarang on detail_permintaan_stok_obat_pasien.kode_brng=databarang.kode_brng "+
                                "where detail_permintaan_stok_obat_pasien.no_permintaan=? order by databarang.kode_brng");
                            try {
                                ps.setString(1,NoResep);
                                rs=ps.executeQuery();
                                while(rs.next()){
                                    Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                                        "0",rs.getString("nama_brng"),rs.getString("aturan_pakai"),rs.getString("jml"),rs.getString("kode_sat"),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notif 1 : "+e);
                            } finally{
                                if(rs!=null){
                                    rs.close();
                                }
                                if(ps!=null){
                                    ps.close();
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notif : "+e);
                        }
            
                        Map<String, Object> param = new HashMap<>();  
                        param.put("namars",akses.getnamars());
                        param.put("alamatrs",akses.getalamatrs());
                        param.put("kotars",akses.getkabupatenrs());
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("emailrs",akses.getemailrs());
                        param.put("kontakrs",akses.getkontakrs());
                        param.put("penanggung",Sequel.cariIsi("select png_jawab from penjab where kd_pj=?",Sequel.cariIsi("select kd_pj from reg_periksa where no_rawat=?",NoRawat)));               
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("apoteker", Sequel.cariIsi("select nama from pegawai where nik=?", akses.getkode()));
                        param.put("norawat",NoRawat);
                        param.put("pasien",Pasien);
                        param.put("norm",NoRM);
                        param.put("peresep",DokterPeresep);
                        param.put("noresep",NoResep);
                        param.put("logo",Sequel.cariGambar("select logo from setting"));
                        param.put("tgl_lahir",Sequel.cariIsi("select DATE_FORMAT(tgl_lahir,'%d-%m-%Y') from pasien where no_rkm_medis=?",NoRM));
                        param.put("jam_peresepan",Sequel.cariIsi("select jam_peresepan from resep_obat where no_resep=?",NoResep));
                        param.put("jam_pelayanan",Sequel.cariIsi("select jam from resep_obat where no_resep=?",NoResep));
                        param.put("alamat",Sequel.cariIsi("select concat(pasien.alamat,', ',k.nm_kel,', ',k2.nm_kec,', ',k3.nm_kab) as alamat\n" +
                            "from pasien\n" +
                            "inner join kelurahan k on pasien.kd_kel = k.kd_kel\n" +
                            "inner join kecamatan k2 on pasien.kd_kec = k2.kd_kec\n" +
                            "inner join kabupaten k3 on pasien.kd_kab = k3.kd_kab\n" +
                            "where no_rkm_medis=?",NoRM));
                        //if reg periksa stts lanjut ranap
                        String kamarinap = Sequel.cariIsi("select concat(ki.kd_kamar,' ',b.nm_bangsal) as kamar from\n" +
                            "reg_periksa\n" +
                            "inner join kamar_inap ki on reg_periksa.no_rawat = ki.no_rawat\n" +
                            "inner join kamar k on ki.kd_kamar = k.kd_kamar\n" +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal\n" +
                            "where status_lanjut='ranap' and ki.no_rawat=?",NoRawat);
                        if(kamarinap.equalsIgnoreCase("")){
                            //kosong
                            param.put("kamarinap","");
                        }else{
                            //ranap
                            param.put("kamarinap","Kamar    : "+kamarinap);
                        }
                        Valid.MyReport("rptSalinanResep.jasper",param,"::[ Copy Resep ]::");
                        this.setCursor(Cursor.getDefaultCursor());                        
                        }   
                    }else{
                        JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                        TCari.requestFocus();
                    }
            }else if(TabRawatInap.getSelectedIndex()==3){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Permintaan Stok...!!!!");
                TCari.requestFocus();
            }else if(TabRawatInap.getSelectedIndex()==4){   //PERMINTAAN RESEP PULANG
                if(akses.getresep_pulang()==true){
                    if(tabMode7.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan resep pulang yang mau divalidasi..!!");
                    }else{
                        //CETAK SALINAN RESEP
                        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        Sequel.queryu("truncate table temporary_resep");
                        try {
                            ps=koneksi.prepareStatement(
                                    "select databarang.kode_brng,databarang.nama_brng,detail_permintaan_resep_pulang.jml," +
                                    "databarang.kode_sat,detail_permintaan_resep_pulang.dosis " +
                                    "from detail_permintaan_resep_pulang inner join databarang on detail_permintaan_resep_pulang.kode_brng=databarang.kode_brng "+
                                    "where detail_permintaan_resep_pulang.no_permintaan=? order by databarang.kode_brng");
                            try {
                                ps.setString(1,NoResep);
                                rs=ps.executeQuery();
                                while(rs.next()){
                                    Sequel.menyimpan("temporary_resep","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",38,new String[]{
                                        "0",rs.getString("nama_brng"),rs.getString("dosis"),rs.getString("jml"),rs.getString("kode_sat"),"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notif 1 : "+e);
                            } finally{
                                if(rs!=null){
                                    rs.close();
                                }
                                if(ps!=null){
                                    ps.close();
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notif : "+e);
                        }
            
                        Map<String, Object> param = new HashMap<>();  
                        param.put("namars",akses.getnamars());
                        param.put("alamatrs",akses.getalamatrs());
                        param.put("kotars",akses.getkabupatenrs());
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("emailrs",akses.getemailrs());
                        param.put("kontakrs",akses.getkontakrs());
                        param.put("penanggung",Sequel.cariIsi("select png_jawab from penjab where kd_pj=?",Sequel.cariIsi("select kd_pj from reg_periksa where no_rawat=?",NoRawat)));               
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("apoteker", Sequel.cariIsi("select nama from pegawai where nik=?", akses.getkode()));
                        param.put("norawat",NoRawat);
                        param.put("pasien",Pasien);
                        param.put("norm",NoRM);
                        param.put("peresep",DokterPeresep);
                        param.put("noresep",NoResep);
                        param.put("logo",Sequel.cariGambar("select logo from setting"));
                        param.put("tgl_lahir",Sequel.cariIsi("select DATE_FORMAT(tgl_lahir,'%d-%m-%Y') from pasien where no_rkm_medis=?",NoRM));
                        param.put("jam_peresepan",Sequel.cariIsi("select jam_peresepan from resep_obat where no_resep=?",NoResep));
                        param.put("jam_pelayanan",Sequel.cariIsi("select jam from resep_obat where no_resep=?",NoResep));
                        param.put("alamat",Sequel.cariIsi("select concat(pasien.alamat,', ',k.nm_kel,', ',k2.nm_kec,', ',k3.nm_kab) as alamat\n" +
                            "from pasien\n" +
                            "inner join kelurahan k on pasien.kd_kel = k.kd_kel\n" +
                            "inner join kecamatan k2 on pasien.kd_kec = k2.kd_kec\n" +
                            "inner join kabupaten k3 on pasien.kd_kab = k3.kd_kab\n" +
                            "where no_rkm_medis=?",NoRM));
                        //if reg periksa stts lanjut ranap
                        String kamarinap = Sequel.cariIsi("select concat(ki.kd_kamar,' ',b.nm_bangsal) as kamar from\n" +
                            "reg_periksa\n" +
                            "inner join kamar_inap ki on reg_periksa.no_rawat = ki.no_rawat\n" +
                            "inner join kamar k on ki.kd_kamar = k.kd_kamar\n" +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal\n" +
                            "where status_lanjut='ranap' and ki.no_rawat=?",NoRawat);
                        if(kamarinap.equalsIgnoreCase("")){
                            //kosong
                            param.put("kamarinap","");
                        }else{
                            //ranap
                            param.put("kamarinap","Kamar    : "+kamarinap);
                        }
                        Valid.MyReport("rptSalinanResep.jasper",param,"::[ Copy Resep ]::");
                        this.setCursor(Cursor.getDefaultCursor());                           
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }
            } else if(TabRawatInap.getSelectedIndex()==5){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Permintaan Resep Pulang...!!!!");
                TCari.requestFocus();
            }      
        }     
    }//GEN-LAST:event_BtnSalinanActionPerformed

    private void BtnSalinanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSalinanKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSalinanActionPerformed(null);
        }else{
           Valid.pindah(evt,BtnEdit,BtnKeluar);
        }
    }//GEN-LAST:event_BtnSalinanKeyPressed

    private void LabelCatatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LabelCatatanKeyPressed

    }//GEN-LAST:event_LabelCatatanKeyPressed

    private void internalFrame7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_internalFrame7MouseClicked
        DlgCatatan.dispose();
    }//GEN-LAST:event_internalFrame7MouseClicked

    private void BtnAntrianActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAntrianActionPerformed
        // TODO add your handling code here:
        //RAWAT JALAN
        if(TabPilihRawat.getSelectedIndex()==0){
            if(TabRawatJalan.getSelectedIndex()==0){
                if(akses.getberi_obat()==true){
                    if(tabMode.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau divalidasi..!!");
                    }else{
                        //CETAK ANTRIAN RESEP
                        Map<String, Object> param = new HashMap<>();  
                        param.put("namars",akses.getnamars());
                        param.put("alamatrs",akses.getalamatrs());
                        param.put("kotars",akses.getkabupatenrs());
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("emailrs",akses.getemailrs());
                        param.put("kontakrs",akses.getkontakrs());
                        param.put("penanggung",Sequel.cariIsi("select png_jawab from penjab where kd_pj=?",Sequel.cariIsi("select kd_pj from reg_periksa where no_rawat=?",NoRawat)));               
                        param.put("propinsirs",akses.getpropinsirs());
                        param.put("peresep",DokterPeresep);
                        param.put("noresep",NoResep);
                        param.put("norawat",NoRawat);
                        
                        param.put("pasien",Pasien);
                        param.put("norm",NoRM);
                        param.put("poli", tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),9).toString());
                        param.put("antrian",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),16).toString());
                        
                        String temp = Sequel.cariIsi("select no_rkm_medis from reg_periksa inner join rujukan_internal_poli rip on reg_periksa.no_rawat = rip.no_rawat where reg_periksa.no_rawat = '"+NoRawat+"'");
                        if(!(temp.isBlank())){
                            param.put("raber", "RABER");
                        }else{
                            param.put("raber", "TIDAK RABER");
                        }
                        
                        int racik = Sequel.cariInteger("select count(resep_dokter_racikan.no_resep) from resep_dokter_racikan where resep_dokter_racikan.no_resep='"+NoResep+"'");
                        if(racik>0){
                            param.put("racik", "RACIKAN");
                        }else{
                            param.put("racik", "NON-RACIKAN");
                        }
                        param.put("waktu",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),1).toString()+" "+tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),2).toString());
                        
                        Valid.MyReport("rptAntrianResep.jasper",param,"::[ No. Antrian Resep ]::");
                        this.setCursor(Cursor.getDefaultCursor());
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                        TCari.requestFocus();
                    }
                }else if(TabRawatJalan.getSelectedIndex()==1){
                    JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                    TCari.requestFocus();
                }
        }else{
            JOptionPane.showMessageDialog(null,"Maaf, cetak antrian resep hanya untuk rawat jalan");
        }
    }//GEN-LAST:event_BtnAntrianActionPerformed

    private void BtnAntrianKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAntrianKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAntrianActionPerformed(null);
        }else{
           Valid.pindah(evt,BtnSalinan,BtnAntrian);
        }
    }//GEN-LAST:event_BtnAntrianKeyPressed

    private void BtnClosePetugasFarmasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnClosePetugasFarmasiActionPerformed
        WindowPetugasFarmasi.dispose();
    }//GEN-LAST:event_BtnClosePetugasFarmasiActionPerformed

    private void BtnSimpanPetugasFarmasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanPetugasFarmasiActionPerformed
    if(KetResep.getText().equals("-")&&Sequel.cariIsi("select if(resep_obat.tgl_perawatan='0000-00-00','',resep_obat.tgl_perawatan) as tgl_perawatan from resep_obat where no_resep=?",NoResep).equals("")){
        JOptionPane.showMessageDialog(null,"Resep obat belum divalidasi !");
    }else if(KetResep.getText().equals("Resep Pulang")&&Sequel.cariIsi("select if(permintaan_resep_pulang.tgl_validasi='0000-00-00','',permintaan_resep_pulang.tgl_validasi) as tgl_validasi from permintaan_resep_pulang where no_permintaan=?",NoResep).equals("")){
        JOptionPane.showMessageDialog(null,"Resep pulang belum divalidasi !");
    }else if(KetResep.getText().equals("Permintaan Stok")&&Sequel.cariIsi("select if(permintaan_stok_obat_pasien.tgl_validasi='0000-00-00','',permintaan_stok_obat_pasien.tgl_validasi) as tgl_validasi from permintaan_stok_obat_pasien where no_permintaan=?",NoResep).equals("")){
        JOptionPane.showMessageDialog(null,"Permintaan stok belum divalidasi !");
    }else{
        int reply = JOptionPane.showConfirmDialog(rootPane,"MOHON PERHATIAN ! Apakah resep obat "+NoResep+" untuk "+Pasien+" sudah selesai pada "+waktu+" "+jLabel27.getText()+" ?","Konfirmasi",JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            Sequel.queryu("delete from antriapotek3");
            Sequel.queryu("insert into antriapotek3 values('"+NoResep+"','1','"+NoRawat+"')");
            Sequel.queryu("delete from bukti_penyerahan_resep_obat where no_resep='"+NoResep+"'");
            if(!NoResep.equals("")){ 
                //APAKAH RESEP OBAT, RESEP PULANG, ATAU PERMINTAAN OBAT
                if(KetResep.getText().equals("Resep Pulang")){
                    //RESEP PULANG
                    if(Sequel.menyimpantf("permintaan_resep_pulang_penyerahan","?,?,?",3,
                            new String[]{
                            NoResep,waktu,jLabel27.getText()
                            },
                            "no_resep=?","?,?",3,
                            new String[]{
                            waktu,jLabel27.getText(),NoResep
                    })==true){
                        //BERHASIL EDIT WAKTU JADI -> TGL & JAM PENYERAHAN
                        //SIMPAN DATA PETUGAS FARMASI
                        if(Sequel.menyimpantf("petugas_farmasi_resep_pulang","?,?,?,?,?,?,?","Petugas Farmasi",7,new String[]{
                            NoResep,KdEntri.getText(),KdAmbil.getText(),
                            KdCek.getText(),KdRacikan.getText(),KdSerahKIE.getText(),KdKonseling.getText()
                        })==true){
                            WindowPetugasFarmasi.dispose();
                            pilihTab();
                        }else{
                            JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal disimpan !");
                        }
                    }else{
                    //GAGAL EDIT
                        JOptionPane.showMessageDialog(null,"Data waktu obat jadi gagal disimpan !");
                    }                   
                }else if(KetResep.getText().equals("Permintaan Stok")){
                    //PERMINTAAN STOK
                    if(Sequel.menyimpantf("permintaan_stok_obat_pasien_penyerahan","?,?,?",3,
                            new String[]{
                            NoResep,waktu,jLabel27.getText()
                            },
                            "no_resep=?","?,?",3,
                            new String[]{
                            waktu,jLabel27.getText(),NoResep
                    })==true){
                        //BERHASIL EDIT WAKTU JADI -> TGL & JAM PENYERAHAN
                        //SIMPAN DATA PETUGAS FARMASI
                        if(Sequel.menyimpantf("petugas_farmasi_permintaan_stok_obat_pasien","?,?,?,?,?,?,?","Petugas Farmasi",7,new String[]{
                            NoResep,KdEntri.getText(),KdAmbil.getText(),
                            KdCek.getText(),KdRacikan.getText(),KdSerahKIE.getText(),KdKonseling.getText()
                        })==true){
                            WindowPetugasFarmasi.dispose();
                            pilihTab();
                        }else{
                            JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal disimpan !");
                        }
                    }else{
                    //GAGAL EDIT
                        JOptionPane.showMessageDialog(null,"Data waktu obat jadi gagal disimpan !");
                    }                   
                }else{
                    //RESEP OBAT
                    if(Sequel.mengedittf("resep_obat","no_resep=?","tgl_penyerahan=?,jam_penyerahan=?",3,new String[]{
                        waktu,jLabel27.getText(),NoResep
                    })==true){
                        //BERHASIL EDIT WAKTU JADI -> TGL & JAM PENYERAHAN
                        //SIMPAN DATA PETUGAS FARMASI
                        if(Sequel.menyimpantf("petugas_farmasi","?,?,?,?,?,?,?","Petugas Farmasi",7,new String[]{
                            NoResep,KdEntri.getText(),KdAmbil.getText(),
                            KdCek.getText(),KdRacikan.getText(),KdSerahKIE.getText(),KdKonseling.getText()
                        })==true){
                            WindowPetugasFarmasi.dispose();
                            pilihTab();
                        }else{
                            JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal disimpan !");
                        }
                    }else{
                    //GAGAL EDIT
                        JOptionPane.showMessageDialog(null,"Data waktu obat jadi gagal disimpan !");
                    }                    
                }
            }
        }        
    }
    }//GEN-LAST:event_BtnSimpanPetugasFarmasiActionPerformed

    private void BtnSeekEntriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekEntriActionPerformed
        temp_ptg=1;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekEntriActionPerformed

    private void BtnSeekKonselingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekKonselingActionPerformed
        temp_ptg=6;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekKonselingActionPerformed

    private void BtnSeekAmbilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekAmbilActionPerformed
        // TODO add your handling code here:
        temp_ptg=2;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekAmbilActionPerformed

    private void BtnSeekCekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekCekActionPerformed
        // TODO add your handling code here:
        temp_ptg=3;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekCekActionPerformed

    private void BtnSeekRacikanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekRacikanActionPerformed
        // TODO add your handling code here:
        temp_ptg=4;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekRacikanActionPerformed

    private void BtnSeekSerahKIEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekSerahKIEActionPerformed
        // TODO add your handling code here:
        temp_ptg=5;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekSerahKIEActionPerformed

    private void KdKonselingKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdKonselingKeyPressed
//        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
//            isNumber();
//            Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?",TDokter,KdKonseling.getText());
//        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
//            BtnDokterActionPerformed(null);
//        }else{
//            Valid.pindah(evt,CmbDetik,kdpoli);
//        }
    }//GEN-LAST:event_KdKonselingKeyPressed

    private void KdEntriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdEntriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdEntriKeyPressed

    private void KdKonselingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdKonselingActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdKonselingActionPerformed

    private void KdAmbilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdAmbilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdAmbilActionPerformed

    private void KdAmbilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdAmbilKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdAmbilKeyPressed

    private void KdCekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdCekActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdCekActionPerformed

    private void KdCekKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdCekKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdCekKeyPressed

    private void KdRacikanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdRacikanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdRacikanActionPerformed

    private void KdRacikanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdRacikanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdRacikanKeyPressed

    private void KdSerahKIEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdSerahKIEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdSerahKIEActionPerformed

    private void KdSerahKIEKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdSerahKIEKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdSerahKIEKeyPressed

    private void BtnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPetugasActionPerformed
        // TODO add your handling code here:
        if(TabPilihRawat.getSelectedIndex()==0){
            if(TabRawatJalan.getSelectedIndex()==0){
                if(akses.getberi_obat()==true){
                    if(tabMode.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                    }else{
                        //START CUSTOM MUHSIN
                        if(tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),12).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Resep obat belum divalidasi !");
                        }else if(tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),14).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Obat belum diserahkan!");
                        }else if(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString()).isEmpty()){
                            JOptionPane.showMessageDialog(null,"Maaf, telaah resep belum dilakukan!");
                        }else{
                            //SET NILAI VARIABEL YG AKAN DIEDIT
                            jLabel36.setText(tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),15).toString());
                            jLabel43.setText(tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString());
                            KdEntri1.setText(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString()));
                            CrEntri1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri1.getText()));
                            //AMBIL PETUGAS DARI JADWAL FARMASI
                            KdAmbil1.setText(Sequel.cariIsi("select petugas_ambil from petugas_farmasi where no_resep=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString()));
                            CrAmbil1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil1.getText()));
                            KdCek1.setText(Sequel.cariIsi("select petugas_cek from petugas_farmasi where no_resep=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString()));
                            CrCek1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek1.getText()));
                            KdRacikan1.setText(Sequel.cariIsi("select petugas_racikan from petugas_farmasi where no_resep=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString()));
                            CrRacikan1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan1.getText()));
                            KdSerahKIE1.setText(Sequel.cariIsi("select petugas_serahkie from petugas_farmasi where no_resep=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString()));
                            CrSerahKIE1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE1.getText()));
                            KdKonseling1.setText(Sequel.cariIsi("select petugas_konseling from petugas_farmasi where no_resep=?",tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString()));
                            CrKonseling1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling1.getText()));
                            KetResepEdit.setText("-");
                            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));        
                            WindowEditPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                            WindowEditPetugasFarmasi.setVisible(true);
                            this.setCursor(Cursor.getDefaultCursor());
                        }
                        //END CUSTOM
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }
            }else if(TabRawatJalan.getSelectedIndex()==1){
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Resep...!!!!");
                TCari.requestFocus();
            }
        }else if(TabPilihRawat.getSelectedIndex()==1){
            if(TabRawatInap.getSelectedIndex()==0){
                if(akses.getberi_obat()==true){
                    if(tabMode3.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                    }else{
                        if(tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),12).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Resep obat belum divalidasi !");
                        }else if(tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),14).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Obat belum diserahkan!");
                        }else if(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()).isEmpty()){
                            JOptionPane.showMessageDialog(null,"Maaf, telaah resep belum dilakukan!");
                        }else{
                            //SET NILAI VARIABEL YG AKAN DIEDIT
                            jLabel36.setText(tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),15).toString());
                            jLabel43.setText(tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString());
                            KdEntri1.setText(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                            CrEntri1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri1.getText()));
                            //AMBIL PETUGAS DARI JADWAL FARMASI
                            KdAmbil1.setText(Sequel.cariIsi("select petugas_ambil from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                            CrAmbil1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil1.getText()));
                            KdCek1.setText(Sequel.cariIsi("select petugas_cek from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                            CrCek1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek1.getText()));
                            KdRacikan1.setText(Sequel.cariIsi("select petugas_racikan from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                            CrRacikan1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan1.getText()));
                            KdSerahKIE1.setText(Sequel.cariIsi("select petugas_serahkie from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                            CrSerahKIE1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE1.getText()));
                            KdKonseling1.setText(Sequel.cariIsi("select petugas_konseling from petugas_farmasi where no_resep=?",tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString()));
                            CrKonseling1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling1.getText()));
                            KetResepEdit.setText("-");
                            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));        
                            WindowEditPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                            WindowEditPetugasFarmasi.setVisible(true);
                            this.setCursor(Cursor.getDefaultCursor());
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }            
            }else if(TabRawatInap.getSelectedIndex()==2){
                if(akses.getberi_obat()==true){
                    if(tabMode5.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                    }else{
                        if(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),13).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Resep obat belum divalidasi !");
                        }else if(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),15).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Obat belum diserahkan!");
                        }
//                        else if(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()).isEmpty()){
//                            JOptionPane.showMessageDialog(null,"Maaf, telaah resep belum dilakukan!");
//                        }
                        else{
                            //SET NILAI VARIABEL YG AKAN DIEDIT
                            jLabel36.setText(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),16).toString());
                            jLabel43.setText(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString());
                            KdEntri1.setText(Sequel.cariIsi("select petugas_entri from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                            CrEntri1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri1.getText()));
                            KdAmbil1.setText(Sequel.cariIsi("select petugas_ambil from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                            CrAmbil1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil1.getText()));
                            KdCek1.setText(Sequel.cariIsi("select petugas_cek from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                            CrCek1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek1.getText()));
                            KdRacikan1.setText(Sequel.cariIsi("select petugas_racikan from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                            CrRacikan1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan1.getText()));
                            KdSerahKIE1.setText(Sequel.cariIsi("select petugas_serahkie from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                            CrSerahKIE1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE1.getText()));
                            KdKonseling1.setText(Sequel.cariIsi("select petugas_konseling from petugas_farmasi_permintaan_stok_obat_pasien where no_permintaan=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()));
                            CrKonseling1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling1.getText()));
                            KetResepEdit.setText("Permintaan Stok");
                            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));        
                            WindowEditPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                            WindowEditPetugasFarmasi.setVisible(true);
                            this.setCursor(Cursor.getDefaultCursor());
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }              
            }else if(TabRawatInap.getSelectedIndex()==4){
                if(akses.getberi_obat()==true){
                    if(tabMode7.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep dokter yang mau diserahkan..!!");
                    }else{
                        if(tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),12).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Resep obat belum divalidasi !");
                        }else if(tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),14).toString().isBlank()){
                            JOptionPane.showMessageDialog(null,"Obat belum diserahkan!");
                        }
//                        else if(Sequel.cariIsi("select nip from telaah_farmasi where no_resep=?",tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString()).isEmpty()){
//                            JOptionPane.showMessageDialog(null,"Maaf, telaah resep belum dilakukan!");
//                        }
                        else{
                            //SET NILAI VARIABEL YG AKAN DIEDIT
                            jLabel36.setText(tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),15).toString());
                            jLabel43.setText(tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString());
                            KdEntri1.setText(Sequel.cariIsi("select petugas_entri from petugas_farmasi_resep_pulang where no_permintaan=?",tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString()));
                            CrEntri1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdEntri1.getText()));
                            KdAmbil1.setText(Sequel.cariIsi("select petugas_ambil from petugas_farmasi_resep_pulang where no_permintaan=?",tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString()));
                            CrAmbil1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdAmbil1.getText()));
                            KdCek1.setText(Sequel.cariIsi("select petugas_cek from petugas_farmasi_resep_pulang where no_permintaan=?",tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString()));
                            CrCek1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdCek1.getText()));
                            KdRacikan1.setText(Sequel.cariIsi("select petugas_racikan from petugas_farmasi_resep_pulang where no_permintaan=?",tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString()));
                            CrRacikan1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdRacikan1.getText()));
                            KdSerahKIE1.setText(Sequel.cariIsi("select petugas_serahkie from petugas_farmasi_resep_pulang where no_permintaan=?",tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString()));
                            CrSerahKIE1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdSerahKIE1.getText()));
                            KdKonseling1.setText(Sequel.cariIsi("select petugas_konseling from petugas_farmasi_resep_pulang where no_permintaan=?",tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString()));
                            CrKonseling1.setText(Sequel.cariIsi("select nama from petugas where nip=?",KdKonseling1.getText()));
                            KetResepEdit.setText("Resep Pulang");
                            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));        
                            WindowEditPetugasFarmasi.setLocationRelativeTo(internalFrame1);
                            WindowEditPetugasFarmasi.setVisible(true);
                            this.setCursor(Cursor.getDefaultCursor());
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }            
            }
        }
    }//GEN-LAST:event_BtnPetugasActionPerformed

    private void BtnPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPetugasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPetugasKeyPressed

    private void BtnClosePetugasFarmasi1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnClosePetugasFarmasi1ActionPerformed
        // TODO add your handling code here:
        WindowEditPetugasFarmasi.dispose();
    }//GEN-LAST:event_BtnClosePetugasFarmasi1ActionPerformed

    private void BtnEditPetugasFarmasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditPetugasFarmasiActionPerformed
        // TODO add your handling code here:
        if(KetResepEdit.getText().equals("Resep Pulang")){
            if(Sequel.mengedittf("petugas_farmasi_resep_pulang","no_permintaan=?","petugas_entri=?,petugas_ambil=?,petugas_cek=?,petugas_racikan=?,petugas_serahkie=?,petugas_konseling=?",7,new String[]{
                KdEntri1.getText(),KdAmbil1.getText(),KdCek1.getText(),KdRacikan1.getText(),
                KdSerahKIE1.getText(),KdKonseling1.getText(),NoResep
            })==true){
                //BERHASIL EDIT DATA PETUGAS FARMASI
                WindowEditPetugasFarmasi.dispose();
                pilihTab();
            }else{
                JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal diedit !");
            }            
        }else if(KetResepEdit.getText().equals("Permintaan Stok")){
            if(Sequel.mengedittf("petugas_farmasi_permintaan_stok_obat_pasien","no_permintaan=?","petugas_entri=?,petugas_ambil=?,petugas_cek=?,petugas_racikan=?,petugas_serahkie=?,petugas_konseling=?",7,new String[]{
                KdEntri1.getText(),KdAmbil1.getText(),KdCek1.getText(),KdRacikan1.getText(),
                KdSerahKIE1.getText(),KdKonseling1.getText(),NoResep
            })==true){
                //BERHASIL EDIT DATA PETUGAS FARMASI
                WindowEditPetugasFarmasi.dispose();
                pilihTab();
            }else{
                JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal diedit !");
            }             
        }else{
            if(Sequel.mengedittf("petugas_farmasi","no_resep=?","petugas_entri=?,petugas_ambil=?,petugas_cek=?,petugas_racikan=?,petugas_serahkie=?,petugas_konseling=?",7,new String[]{
                KdEntri1.getText(),KdAmbil1.getText(),KdCek1.getText(),KdRacikan1.getText(),
                KdSerahKIE1.getText(),KdKonseling1.getText(),NoResep
            })==true){
                //BERHASIL EDIT DATA PETUGAS FARMASI
                WindowEditPetugasFarmasi.dispose();
                pilihTab();
            }else{
                JOptionPane.showMessageDialog(null,"Data petugas farmasi gagal diedit !");
            }              
        }
    }//GEN-LAST:event_BtnEditPetugasFarmasiActionPerformed

    private void BtnSeekEntri1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekEntri1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=7;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekEntri1ActionPerformed

    private void BtnSeekKonseling1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekKonseling1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=12;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);        
    }//GEN-LAST:event_BtnSeekKonseling1ActionPerformed

    private void BtnSeekAmbil1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekAmbil1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=8;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekAmbil1ActionPerformed

    private void BtnSeekCek1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekCek1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=9;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnSeekCek1ActionPerformed

    private void BtnSeekRacikan1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekRacikan1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=10;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);        
    }//GEN-LAST:event_BtnSeekRacikan1ActionPerformed

    private void BtnSeekSerahKIE1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeekSerahKIE1ActionPerformed
        // TODO add your handling code here:
        temp_ptg=11;
        petugas.isCek();        
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);        
    }//GEN-LAST:event_BtnSeekSerahKIE1ActionPerformed

    private void KdKonseling1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdKonseling1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdKonseling1ActionPerformed

    private void KdKonseling1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdKonseling1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdKonseling1KeyPressed

    private void KdEntri1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdEntri1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdEntri1KeyPressed

    private void KdAmbil1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdAmbil1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdAmbil1ActionPerformed

    private void KdAmbil1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdAmbil1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdAmbil1KeyPressed

    private void KdCek1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdCek1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdCek1ActionPerformed

    private void KdCek1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdCek1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdCek1KeyPressed

    private void KdRacikan1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdRacikan1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdRacikan1ActionPerformed

    private void KdRacikan1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdRacikan1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdRacikan1KeyPressed

    private void KdSerahKIE1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KdSerahKIE1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdSerahKIE1ActionPerformed

    private void KdSerahKIE1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdSerahKIE1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KdSerahKIE1KeyPressed

    private void BtnTelaahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTelaahActionPerformed
        // TODO add your handling code here:
        if(TabPilihRawat.getSelectedIndex()==0){
            JOptionPane.showMessageDialog(null,"Maaf, menu telaah digunakan untuk resep pulang dan permintaan stok ranap");
            TCari.requestFocus();  
        }else if(TabPilihRawat.getSelectedIndex()==1){
            if(TabRawatInap.getSelectedIndex()==2){
                if(akses.gettelaah_resep()==true){
                    if(tabMode5.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data permintaan stok yang mau ditelaah..!!");
                    }else{
                        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        InventoryTelaahPermintaanStok aplikasi=new InventoryTelaahPermintaanStok(null,false);
                        aplikasi.emptTeks();
                        aplikasi.isCek();
                        aplikasi.setNoRm(tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString(),tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),3).toString(),DTPCari2.getDate());
                        aplikasi.tampil();
                        aplikasi.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
                        aplikasi.setLocationRelativeTo(internalFrame1);
                        aplikasi.setVisible(true);
                        this.setCursor(Cursor.getDefaultCursor());   
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk mengvalidasi...!!!!");
                    TCari.requestFocus();
                }
            }else if(TabRawatInap.getSelectedIndex()==4){
                if(akses.gettelaah_resep()==true){
                    if(tabMode7.getRowCount()==0){
                        JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
                        TCari.requestFocus();
                    }else if(NoRawat.equals("")){
                        JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data resep pulang yang mau ditelaah..!!");
                    }else{
                        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        InventoryTelaahResepPulang aplikasi=new InventoryTelaahResepPulang(null,false);
                        aplikasi.emptTeks();
                        aplikasi.isCek();
                        aplikasi.setNoRm(tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString(),tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),3).toString(),DTPCari2.getDate());
                        aplikasi.tampil();
                        aplikasi.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
                        aplikasi.setLocationRelativeTo(internalFrame1);
                        aplikasi.setVisible(true);
                        this.setCursor(Cursor.getDefaultCursor());                        
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Maaf, Anda tidak punya hak akses untuk melakukan telaah...!!!!");
                    TCari.requestFocus();
                }
            } else {
                JOptionPane.showMessageDialog(null,"Maaf, silahkan buka Daftar Permintaan Stok / Resep Pulang...!!!!");
                TCari.requestFocus();
            }     
        }             
    }//GEN-LAST:event_BtnTelaahActionPerformed

    private void BtnTelaahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnTelaahKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnTelaahKeyPressed

    private void tbObatKronisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatKronisMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbObatKronisMouseClicked

    private void tbObatKronisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKronisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbObatKronisKeyPressed

    private void tbObatKronisKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKronisKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tbObatKronisKeyReleased
//END CUSTOM
    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            DlgDaftarPermintaanResep dialog = new DlgDaftarPermintaanResep(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnAntrian;
    private widget.Button BtnCari;
    private widget.Button BtnClosePetugasFarmasi;
    private widget.Button BtnClosePetugasFarmasi1;
    private widget.Button BtnEdit;
    private widget.Button BtnEditPetugasFarmasi;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPenyerahan;
    private widget.Button BtnPetugas;
    private widget.Button BtnPrint;
    private widget.Button BtnRekap;
    private widget.Button BtnSalinan;
    private widget.Button BtnSeek3;
    private widget.Button BtnSeek4;
    private widget.Button BtnSeek5;
    private widget.Button BtnSeek6;
    private widget.Button BtnSeekAmbil;
    private widget.Button BtnSeekAmbil1;
    private widget.Button BtnSeekCek;
    private widget.Button BtnSeekCek1;
    private widget.Button BtnSeekEntri;
    private widget.Button BtnSeekEntri1;
    private widget.Button BtnSeekKonseling;
    private widget.Button BtnSeekKonseling1;
    private widget.Button BtnSeekRacikan;
    private widget.Button BtnSeekRacikan1;
    private widget.Button BtnSeekSerahKIE;
    private widget.Button BtnSeekSerahKIE1;
    private widget.Button BtnSimpanPetugasFarmasi;
    private widget.Button BtnTambah;
    private widget.Button BtnTelaah;
    private widget.TextBox CrAmbil;
    private widget.TextBox CrAmbil1;
    private widget.TextBox CrCek;
    private widget.TextBox CrCek1;
    private widget.TextBox CrDokter;
    private widget.TextBox CrDokter2;
    private widget.TextBox CrEntri;
    private widget.TextBox CrEntri1;
    private widget.TextBox CrKonseling;
    private widget.TextBox CrKonseling1;
    private widget.TextBox CrPoli;
    private widget.TextBox CrRacikan;
    private widget.TextBox CrRacikan1;
    private widget.TextBox CrSerahKIE;
    private widget.TextBox CrSerahKIE1;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private javax.swing.JDialog DlgCatatan;
    private widget.TextBox Kamar;
    private widget.TextBox KdAmbil;
    private widget.TextBox KdAmbil1;
    private widget.TextBox KdCek;
    private widget.TextBox KdCek1;
    private widget.TextBox KdEntri;
    private widget.TextBox KdEntri1;
    private widget.TextBox KdKonseling;
    private widget.TextBox KdKonseling1;
    private widget.TextBox KdRacikan;
    private widget.TextBox KdRacikan1;
    private widget.TextBox KdSerahKIE;
    private widget.TextBox KdSerahKIE1;
    private widget.Label KetResep;
    private widget.Label KetResepEdit;
    private widget.Label LCount;
    private widget.TextArea LabelCatatan;
    private widget.ScrollPane Scroll6;
    private widget.TextBox TCari;
    private javax.swing.JTabbedPane TabPilihRawat;
    private javax.swing.JTabbedPane TabRawatInap;
    private javax.swing.JTabbedPane TabRawatJalan;
    private javax.swing.JDialog WindowEditPetugasFarmasi;
    private javax.swing.JDialog WindowPetugasFarmasi;
    private widget.ComboBox cmbStatus;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.InternalFrame internalFrame5;
    private widget.InternalFrame internalFrame6;
    private widget.InternalFrame internalFrame7;
    private widget.Label jLabel12;
    private widget.Label jLabel14;
    private widget.Label jLabel15;
    private widget.Label jLabel16;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel29;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel66;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private widget.Label label10;
    private widget.Label label9;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.panelisi panelisi1;
    private widget.panelisi panelisi2;
    private widget.ScrollPane scrollPane1;
    private widget.ScrollPane scrollPane2;
    private widget.ScrollPane scrollPane3;
    private widget.ScrollPane scrollPane4;
    private widget.ScrollPane scrollPane5;
    private widget.ScrollPane scrollPane6;
    private widget.ScrollPane scrollPane7;
    private widget.ScrollPane scrollPane8;
    private widget.Table tbDetailPermintaanResepPulang;
    private widget.Table tbDetailPermintaanStok;
    private widget.Table tbDetailResepRalan;
    private widget.Table tbDetailResepRanap;
    private widget.Table tbObatKronis;
    private widget.Table tbPermintaanResepPulang;
    private widget.Table tbPermintaanStok;
    private widget.Table tbResepRalan;
    private widget.Table tbResepRanap;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{  
            semua=CrDokter.getText().trim().equals("")&&CrPoli.getText().trim().equals("")&&TCari.getText().trim().equals("");
            ps=koneksi.prepareStatement("select resep_obat.no_resep,resep_obat.tgl_peresepan,resep_obat.jam_peresepan,"+
                    " resep_obat.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,resep_obat.kd_dokter,dokter.nm_dokter,"+
                    " if(resep_obat.tgl_perawatan='0000-00-00','Belum Terlayani','Sudah Terlayani') as status,poliklinik.nm_poli, "+
                    " reg_periksa.kd_poli,penjab.png_jawab,if(resep_obat.tgl_perawatan='0000-00-00','',resep_obat.tgl_perawatan) as tgl_perawatan,"+
                    " if(resep_obat.jam='00:00:00','',resep_obat.jam) as jam,"+
                    " if(resep_obat.tgl_penyerahan='0000-00-00','',resep_obat.tgl_penyerahan) as tgl_penyerahan,"+
                    " if(resep_obat.jam_penyerahan='00:00:00','',resep_obat.jam_penyerahan) as jam_penyerahan,ru.no_urut from resep_obat "+ //CUSTOM MUHSIN -> TMBAH NO URUT
                    " inner join reg_periksa on resep_obat.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on resep_obat.kd_dokter=dokter.kd_dokter "+
                    " inner join poliklinik on reg_periksa.kd_poli=poliklinik.kd_poli "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " inner join resep_urutan ru on resep_obat.no_resep = ru.no_resep "+    //CUSTOM MUHSIN -> TMBAH NO URUT
                    " where resep_obat.tgl_peresepan<>'0000-00-00' and resep_obat.status='ralan' and resep_obat.tgl_peresepan between ? and ? "+
                    (semua?"":"and dokter.nm_dokter like ? and poliklinik.nm_poli like ? and "+
                    "(resep_obat.no_resep like ? or resep_obat.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
                    "dokter.nm_dokter like ? or penjab.png_jawab like ?)")+" order by resep_obat.tgl_peresepan desc,ru.no_urut desc,resep_obat.jam_peresepan desc");    //CUSTOM MUHSIN -> ORDER BY NO URUT
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter.getText().trim()+"%");
                    ps.setString(4,"%"+CrPoli.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                } 
                rs=ps.executeQuery();
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM MUHSIN -> CEK RACIKAN
                        racik="";
                        if(Sequel.cariInteger("select count(no_racik) from obat_racikan where no_rawat='"+rs.getString("no_rawat")+"' and tgl_perawatan='"+rs.getString("tgl_perawatan")+"'and jam='"+rs.getString("jam")+"'") > 0){
                            racik = "RACIKAN";
                        }else{
                            if(rs.getString("status").equalsIgnoreCase("Belum Terlayani")){
                                racik = ""; 
                            }else{
                                racik = "NON-RACIKAN";
                            }
                        }
                        //END CUSTOM
                        tabMode.addRow(new String[]{
                            rs.getString("no_resep"),rs.getString("tgl_peresepan"),rs.getString("jam_peresepan"),rs.getString("no_rawat"),
                            rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                            rs.getString("kd_dokter"),rs.getString("nm_poli"),rs.getString("kd_poli"),rs.getString("png_jawab"),
                            rs.getString("tgl_perawatan"),rs.getString("jam"),rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                            ,rs.getString("no_urut"),racik   //CUSTOM MUHSIN -> NOURUT & KETERANGAN RACIKAN
                        });     
                    }  
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM MUHSIN -> CEK RACIKAN
                            racik="";
                            if(Sequel.cariInteger("select count(no_racik) from obat_racikan where no_rawat='"+rs.getString("no_rawat")+"' and tgl_perawatan='"+rs.getString("tgl_perawatan")+"'and jam='"+rs.getString("jam")+"'") > 0){
                                racik = "RACIKAN";
                            }else{
                                if(rs.getString("status").equalsIgnoreCase("Belum Terlayani")){
                                    racik = ""; 
                                }else{
                                    racik = "NON-RACIKAN";
                                }
                            }
                            //END CUSTOM
                            tabMode.addRow(new String[]{
                                rs.getString("no_resep"),rs.getString("tgl_peresepan"),rs.getString("jam_peresepan"),rs.getString("no_rawat"),
                                rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                                rs.getString("kd_dokter"),rs.getString("nm_poli"),rs.getString("kd_poli"),rs.getString("png_jawab"),
                                rs.getString("tgl_perawatan"),rs.getString("jam"),rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                                ,rs.getString("no_urut"),racik      //CUSTOM MUHSIN -> NOURUT & KETERANGAN RACIKAN
                            });
                        }                    
                    }  
                }              
                
                LCount.setText(""+tabMode.getRowCount());
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }                
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }        
    }

    public void emptTeks() {
        TCari.setText("");
        TCari.requestFocus();
    }

    private void getData() {
        if(tbResepRalan.getSelectedRow()!= -1){
            NoResep=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),0).toString();
            TglPeresepan=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),1).toString();
            JamPeresepan=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),2).toString();
            NoRawat=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),3).toString();
            NoRM=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),4).toString();
            Pasien=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),5).toString();
            DokterPeresep=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),6).toString();
            Status=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),7).toString();
            KodeDokter=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),8).toString();
            Ruang=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),9).toString();
            KodeRuang=tbResepRalan.getValueAt(tbResepRalan.getSelectedRow(),10).toString();
        }
    }
    
    private void getData2() {
        if(tbResepRanap.getSelectedRow()!= -1){
            NoResep=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),0).toString();
            TglPeresepan=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),1).toString();
            JamPeresepan=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),2).toString();
            NoRawat=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),3).toString();
            NoRM=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),4).toString();
            Pasien=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),5).toString();
            DokterPeresep=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),6).toString();
            Status=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),7).toString();
            KodeDokter=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),8).toString();
            Ruang=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),9).toString();
            KodeRuang=tbResepRanap.getValueAt(tbResepRanap.getSelectedRow(),10).toString();
        }
    }

    private void getData3() {
        if(tbPermintaanStok.getSelectedRow()!= -1){
            NoResep=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),0).toString();
            TglPeresepan=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),1).toString();
            JamPeresepan=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),2).toString();
            NoRawat=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),3).toString();
            NoRM=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),4).toString();
            Pasien=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),5).toString();
            DokterPeresep=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),6).toString();
            Status=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),7).toString();
            KodeDokter=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),8).toString();
            Ruang=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),9).toString();
            KodeRuang=tbPermintaanStok.getValueAt(tbPermintaanStok.getSelectedRow(),10).toString();
        }
    }
    
    private void getData4() {
        if(tbPermintaanResepPulang.getSelectedRow()!= -1){
            NoResep=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),0).toString();
            TglPeresepan=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),1).toString();
            JamPeresepan=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),2).toString();
            NoRawat=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),3).toString();
            NoRM=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),4).toString();
            Pasien=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),5).toString();
            DokterPeresep=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),6).toString();
            Status=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),7).toString();
            KodeDokter=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),8).toString();
            Ruang=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),9).toString();
            KodeRuang=tbPermintaanResepPulang.getValueAt(tbPermintaanResepPulang.getSelectedRow(),10).toString();
        }
    }
    
    public JTable getTable(){
        return tbResepRalan;
    }
    
    public void isCek(){
        BtnEdit.setEnabled(akses.getresep_dokter());
        BtnPrint.setEnabled(akses.getresep_dokter());
        BtnRekap.setEnabled(akses.getresep_obat());
    }
    
    public void setCari(String cari){
        TCari.setText(cari);
    }

    private void tampil2() {
        Valid.tabelKosong(tabMode2);
        try{  
            semua=CrDokter.getText().trim().equals("")&&CrPoli.getText().trim().equals("")&&TCari.getText().trim().equals("");
            ps=koneksi.prepareStatement("select resep_obat.no_resep,resep_obat.tgl_perawatan,resep_obat.jam,resep_obat.no_rawat,pasien.no_rkm_medis,"+
                    " pasien.nm_pasien,resep_obat.kd_dokter,dokter.nm_dokter,if(resep_obat.tgl_perawatan='0000-00-00','Belum Terlayani','Sudah Terlayani') as status,"+
                    " poliklinik.nm_poli,resep_obat.status as status_asal,penjab.png_jawab from resep_obat "+
                    " inner join reg_periksa on resep_obat.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on resep_obat.kd_dokter=dokter.kd_dokter "+
                    " inner join poliklinik on reg_periksa.kd_poli=poliklinik.kd_poli "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " where resep_obat.tgl_peresepan<>'0000-00-00' and resep_obat.status='ralan' and resep_obat.tgl_peresepan between ? and ? "+
                    (semua?"":"and dokter.nm_dokter like ? and poliklinik.nm_poli like ? and "+
                    "(resep_obat.no_resep like ? or resep_obat.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
                    "dokter.nm_dokter like ? or penjab.png_jawab like ?) ")+"order by resep_obat.tgl_perawatan desc,resep_obat.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter.getText().trim()+"%");
                    ps.setString(4,"%"+CrPoli.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                
                rs=ps.executeQuery();
                i=0;
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        tabMode2.addRow(new String[]{
                            rs.getString("no_resep"),rs.getString("tgl_perawatan")+" "+rs.getString("jam"),
                            rs.getString("nm_poli"),rs.getString("status"),
                            rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                            rs.getString("nm_dokter")
                        });
                        tabMode2.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                        ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter.jml,"+
                            "databarang.kode_sat,resep_dokter.aturan_pakai from resep_dokter inner join databarang on "+
                            "resep_dokter.kode_brng=databarang.kode_brng where resep_dokter.no_resep=? order by databarang.kode_brng");
                        try {
                            ps2.setString(1,rs.getString("no_resep"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode2.addRow(new String[]{
                                    "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("aturan_pakai")
                                });
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        ps2=koneksi.prepareStatement(
                                "select resep_dokter_racikan.no_racik,resep_dokter_racikan.nama_racik,"+
                                "resep_dokter_racikan.kd_racik,metode_racik.nm_racik as metode,"+
                                "resep_dokter_racikan.jml_dr,resep_dokter_racikan.aturan_pakai,"+
                                "resep_dokter_racikan.keterangan from resep_dokter_racikan inner join metode_racik "+
                                "on resep_dokter_racikan.kd_racik=metode_racik.kd_racik where "+
                                "resep_dokter_racikan.no_resep=? ");
                        try {
                            ps2.setString(1,rs.getString("no_resep"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode2.addRow(new String[]{
                                    "","",rs2.getString("jml_dr")+" "+rs2.getString("metode"),"No.Racik : "+rs2.getString("no_racik"),rs2.getString("nama_racik"),rs2.getString("aturan_pakai")
                                });
                                ps3=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter_racikan_detail.jml,"+
                                    "databarang.kode_sat from resep_dokter_racikan_detail inner join databarang on resep_dokter_racikan_detail.kode_brng=databarang.kode_brng "+
                                    "where resep_dokter_racikan_detail.no_resep=? and resep_dokter_racikan_detail.no_racik=? order by databarang.kode_brng");
                                try {
                                    ps3.setString(1,rs.getString("no_resep"));
                                    ps3.setString(2,rs2.getString("no_racik"));
                                    rs3=ps3.executeQuery();
                                    while(rs3.next()){
                                        tabMode2.addRow(new String[]{
                                            "","","   "+rs3.getString("jml")+" "+rs3.getString("kode_sat"),"   "+rs3.getString("kode_brng"),"   "+rs3.getString("nama_brng"),""
                                        });
                                    }
                                } catch (Exception e) {
                                    System.out.println("Notifikasi 3 : "+e);
                                } finally{
                                    if(rs3!=null){
                                        rs3.close();
                                    }
                                    if(ps3!=null){
                                        ps3.close();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        i++;
                    }
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            tabMode2.addRow(new String[]{
                                rs.getString("no_resep"),rs.getString("tgl_perawatan")+" "+rs.getString("jam"),
                                rs.getString("nm_poli"),rs.getString("status"),
                                rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                                rs.getString("nm_dokter")
                            });
                            tabMode2.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                            ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter.jml,"+
                                "databarang.kode_sat,resep_dokter.aturan_pakai from resep_dokter inner join databarang on "+
                                "resep_dokter.kode_brng=databarang.kode_brng where resep_dokter.no_resep=? order by databarang.kode_brng");
                            try {
                                ps2.setString(1,rs.getString("no_resep"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode2.addRow(new String[]{
                                        "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("aturan_pakai")
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            ps2=koneksi.prepareStatement(
                                    "select resep_dokter_racikan.no_racik,resep_dokter_racikan.nama_racik,"+
                                    "resep_dokter_racikan.kd_racik,metode_racik.nm_racik as metode,"+
                                    "resep_dokter_racikan.jml_dr,resep_dokter_racikan.aturan_pakai,"+
                                    "resep_dokter_racikan.keterangan from resep_dokter_racikan inner join metode_racik "+
                                    "on resep_dokter_racikan.kd_racik=metode_racik.kd_racik where "+
                                    "resep_dokter_racikan.no_resep=? ");
                            try {
                                ps2.setString(1,rs.getString("no_resep"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode2.addRow(new String[]{
                                        "","",rs2.getString("jml_dr")+" "+rs2.getString("metode"),"No.Racik : "+rs2.getString("no_racik"),rs2.getString("nama_racik"),rs2.getString("aturan_pakai")
                                    });
                                    ps3=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter_racikan_detail.jml,"+
                                        "databarang.kode_sat from resep_dokter_racikan_detail inner join databarang on resep_dokter_racikan_detail.kode_brng=databarang.kode_brng "+
                                        "where resep_dokter_racikan_detail.no_resep=? and resep_dokter_racikan_detail.no_racik=? order by databarang.kode_brng");
                                    try {
                                        ps3.setString(1,rs.getString("no_resep"));
                                        ps3.setString(2,rs2.getString("no_racik"));
                                        rs3=ps3.executeQuery();
                                        while(rs3.next()){
                                            tabMode2.addRow(new String[]{
                                                "","","   "+rs3.getString("jml")+" "+rs3.getString("kode_sat"),"   "+rs3.getString("kode_brng"),"   "+rs3.getString("nama_brng"),""
                                            });
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Notifikasi 3 : "+e);
                                    } finally{
                                        if(rs3!=null){
                                            rs3.close();
                                        }
                                        if(ps3!=null){
                                            ps3.close();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            i++;
                        }
                    }
                }
                LCount.setText(""+i);
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }                
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }  
    }

    private void panggilform() {
        dlgobt.setNoRm(NoRawat,NoRM,Pasien,TglPeresepan,JamPeresepan);
        dlgobt.isCek();
        dlgobt.tampilobat2(NoResep);
        dlgobt.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dlgobt.setLocationRelativeTo(internalFrame1);
        TeksKosong();
        dlgobt.setVisible(true);   
    }
    
    private void TeksKosong(){
        NoResep="";
        TglPeresepan="";
        JamPeresepan="";
        NoRawat="";
        NoRM="";
        Pasien="";
        DokterPeresep="";
        Status="";
        KodeDokter="";
        Ruang="";
        KodeRuang="";
    }
    
    private void panggilform2() {
        kamar=KodeRuang;
        bangsal=Sequel.cariIsi("select kd_depo from set_depo_ranap where kd_bangsal=?",kamar);
        if(bangsal.equals("")){
            if(Sequel.cariIsi("select asal_stok from set_lokasi").equals("Gunakan Stok Bangsal")){
                akses.setkdbangsal(kamar);
            }else{
                akses.setkdbangsal(Sequel.cariIsi("select kd_bangsal from set_lokasi"));
            }
        }else{
            akses.setkdbangsal(bangsal);
        }
        dlgobt2.setNoRm(NoRawat,NoRM,Pasien,Valid.SetTgl2(TglPeresepan));
        dlgobt2.isCek();
        dlgobt2.tampilobat2(NoResep);
        dlgobt2.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dlgobt2.setLocationRelativeTo(internalFrame1);
        TeksKosong();
        dlgobt2.setVisible(true);         
    }
    
    private void panggilform3() {
        kamar=KodeRuang;
        bangsal=Sequel.cariIsi("select kd_depo from set_depo_ranap where kd_bangsal=?",kamar);
        if(bangsal.equals("")){
            if(Sequel.cariIsi("select asal_stok from set_lokasi").equals("Gunakan Stok Bangsal")){
                akses.setkdbangsal(kamar);
            }else{
                akses.setkdbangsal(Sequel.cariIsi("select kd_bangsal from set_lokasi"));
            }
        }else{
            akses.setkdbangsal(bangsal);
        }
        dlgstok.setNoRm(NoRawat,NoRM+" "+Pasien);
        dlgstok.isCek();
        dlgstok.tampil2(NoResep);
        dlgstok.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dlgstok.setLocationRelativeTo(internalFrame1);
        TeksKosong();
        dlgstok.setVisible(true);         
    }
    
    private void panggilform4() {
        kamar=KodeRuang;
        bangsal=Sequel.cariIsi("select kd_depo from set_depo_ranap where kd_bangsal=?",kamar);
        if(bangsal.equals("")){
            if(Sequel.cariIsi("select asal_stok from set_lokasi").equals("Gunakan Stok Bangsal")){
                akses.setkdbangsal(kamar);
            }else{
                akses.setkdbangsal(Sequel.cariIsi("select kd_bangsal from set_lokasi"));
            }
        }else{
            akses.setkdbangsal(bangsal);
        }
        dlgresepulang.setNoRm(NoRawat,NoRM,Pasien,"-",DTPCari1.getSelectedItem().toString(),Sequel.cariIsi("select current_time()"));
        dlgresepulang.isCek();
        dlgresepulang.tampil2(NoResep);
        dlgresepulang.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dlgresepulang.setLocationRelativeTo(internalFrame1);
        TeksKosong();
        dlgresepulang.setVisible(true);         
    }
    
    public void pilihTab(){
        if(TabPilihRawat.getSelectedIndex()==0){
            pilihRalan();
        }else if(TabPilihRawat.getSelectedIndex()==1){
            pilihRanap();
        }
    }
    
    public void pilihRalan(){
        if(TabRawatJalan.getSelectedIndex()==0){
            tampil();
        }else if(TabRawatJalan.getSelectedIndex()==1){
            tampil2();
        }
    }
    
    public void pilihRanap(){
        if(TabRawatInap.getSelectedIndex()==0){
            tampil3();
        }else if(TabRawatInap.getSelectedIndex()==1){
            tampil4();
        }else if(TabRawatInap.getSelectedIndex()==2){
            tampil5();
        }else if(TabRawatInap.getSelectedIndex()==3){
            tampil6();
        }else if(TabRawatInap.getSelectedIndex()==4){
            tampil7();
        }else if(TabRawatInap.getSelectedIndex()==5){
            tampil8();
        }
    }
    
    public void tampil3() {
        Valid.tabelKosong(tabMode3);
        try{  
            semua=CrDokter2.getText().trim().equals("")&&Kamar.getText().trim().equals("")&&TCari.getText().trim().equals("");
            ps=koneksi.prepareStatement("select resep_obat.no_resep,resep_obat.tgl_peresepan,resep_obat.jam_peresepan,"+
                    " resep_obat.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,resep_obat.kd_dokter,dokter.nm_dokter, "+
                    " if(resep_obat.tgl_perawatan='0000-00-00','Belum Terlayani','Sudah Terlayani') as status,"+
                    " bangsal.nm_bangsal,kamar.kd_bangsal,penjab.png_jawab,"+
                    " if(resep_obat.tgl_perawatan='0000-00-00','',resep_obat.tgl_perawatan) as tgl_perawatan,"+
                    " if(resep_obat.tgl_penyerahan='0000-00-00','',resep_obat.tgl_penyerahan) as tgl_penyerahan,"+  //CUSTOM
                    " if(resep_obat.jam_penyerahan='00:00:00','',resep_obat.jam_penyerahan) as jam_penyerahan,"+    //CUSTOM
                    " if(resep_obat.jam='00:00:00','',resep_obat.jam) as jam from resep_obat  "+
                    " inner join reg_periksa on resep_obat.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on resep_obat.kd_dokter=dokter.kd_dokter "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " inner join kamar_inap on reg_periksa.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " where resep_obat.tgl_peresepan<>'0000-00-00' and resep_obat.status='ranap' and resep_obat.tgl_peresepan between ? and ? "+    //CUSTOM MUHSIN HAPUS -> and kamar_inap.stts_pulang='-'
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(resep_obat.no_resep like ? or resep_obat.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by resep_obat.no_resep order by resep_obat.tgl_perawatan desc,resep_obat.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
           
                rs=ps.executeQuery();
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode3.addRow(new String[]{
                            rs.getString("no_resep"),rs.getString("tgl_peresepan"),rs.getString("jam_peresepan"),rs.getString("no_rawat"),
                            rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                            rs.getString("kd_dokter"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("kd_bangsal"),rs.getString("png_jawab"),
                            rs.getString("tgl_perawatan"),rs.getString("jam")
                            //START CUSSTOM -> TAMBAH KOLOM OBAT JADI
                            ,rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                            //END CUSTOM    
                        });           
                        //END CUSTOM
                    } 
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode3.addRow(new String[]{
                                rs.getString("no_resep"),rs.getString("tgl_peresepan"),rs.getString("jam_peresepan"),rs.getString("no_rawat"),
                                rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                                rs.getString("kd_dokter"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("kd_bangsal"),rs.getString("png_jawab"),
                                rs.getString("tgl_perawatan"),rs.getString("jam")
                                //START CUSSTOM -> TAMBAH KOLOM OBAT JADI
                                ,rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                                //END CUSTOM
                            });  
                            //END CUSTOM
                        }                  
                    } 
                }               
                
                LCount.setText(""+tabMode3.getRowCount());
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }      
            
            ps=koneksi.prepareStatement("select resep_obat.no_resep,resep_obat.tgl_peresepan,resep_obat.jam_peresepan,"+
                    " resep_obat.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,resep_obat.kd_dokter,dokter.nm_dokter, "+
                    " if(resep_obat.tgl_perawatan='0000-00-00','Belum Terlayani','Sudah Terlayani') as status,"+
                    " bangsal.nm_bangsal,kamar.kd_bangsal,penjab.png_jawab,"+
                    " if(resep_obat.tgl_perawatan='0000-00-00','',resep_obat.tgl_perawatan) as tgl_perawatan,"+
                    " if(resep_obat.tgl_penyerahan='0000-00-00','',resep_obat.tgl_penyerahan) as tgl_penyerahan,"+  //CUSTOM
                    " if(resep_obat.jam_penyerahan='00:00:00','',resep_obat.jam_penyerahan) as jam_penyerahan,"+    //CUSTOM 
                    " if(resep_obat.jam='00:00:00','',resep_obat.jam) as jam from resep_obat  "+
                    " inner join ranap_gabung on ranap_gabung.no_rawat2=resep_obat.no_rawat "+
                    " inner join reg_periksa on resep_obat.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on resep_obat.kd_dokter=dokter.kd_dokter "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " inner join kamar_inap on ranap_gabung.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " where resep_obat.tgl_peresepan<>'0000-00-00' and kamar_inap.stts_pulang='-' and resep_obat.status='ranap' and resep_obat.tgl_peresepan between ? and ? "+
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(resep_obat.no_resep like ? or resep_obat.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by resep_obat.no_resep order by resep_obat.tgl_perawatan desc,resep_obat.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                
                rs=ps.executeQuery();
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        tabMode3.addRow(new String[]{
                            rs.getString("no_resep"),rs.getString("tgl_peresepan"),rs.getString("jam_peresepan"),rs.getString("no_rawat"),
                            rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                            rs.getString("kd_dokter"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("kd_bangsal"),rs.getString("png_jawab"),
                            rs.getString("tgl_perawatan"),rs.getString("jam")
                            //START CUSSTOM -> TAMBAH KOLOM OBAT JADI
                            ,rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                            //END CUSTOM                                
                        });            
                    } 
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            tabMode3.addRow(new String[]{
                                rs.getString("no_resep"),rs.getString("tgl_peresepan"),rs.getString("jam_peresepan"),rs.getString("no_rawat"),
                                rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                                rs.getString("kd_dokter"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("kd_bangsal"),rs.getString("png_jawab"),
                                rs.getString("tgl_perawatan"),rs.getString("jam")
                                //START CUSSTOM -> TAMBAH KOLOM OBAT JADI
                                ,rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                                //END CUSTOM                            
                            });  
                        }                  
                    } 
                }               
                
                LCount.setText(""+tabMode3.getRowCount());
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }  
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }        
    }
    
    public void tampil4() {
        Valid.tabelKosong(tabMode4);
        try{  
            semua=CrDokter2.getText().trim().equals("")&&Kamar.getText().trim().equals("")&&TCari.getText().trim().equals("");
            ps=koneksi.prepareStatement("select resep_obat.no_resep,resep_obat.tgl_perawatan,resep_obat.jam,resep_obat.no_rawat,pasien.no_rkm_medis,"+
                    " pasien.nm_pasien,resep_obat.kd_dokter,dokter.nm_dokter,if(resep_obat.tgl_perawatan='0000-00-00','Belum Terlayani','Sudah Terlayani') as status,"+
                    " bangsal.nm_bangsal,resep_obat.status as status_asal,penjab.png_jawab from resep_obat "+
                    " inner join reg_periksa on resep_obat.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on resep_obat.kd_dokter=dokter.kd_dokter "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " inner join kamar_inap on reg_periksa.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " where resep_obat.tgl_peresepan<>'0000-00-00' and resep_obat.status='ranap' and resep_obat.tgl_peresepan between ? and ? "+    //CUSTOM MUHSIN --> HAPUS and kamar_inap.stts_pulang='-' 
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(resep_obat.no_resep like ? or resep_obat.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by resep_obat.no_resep order by resep_obat.tgl_perawatan desc,resep_obat.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                rs=ps.executeQuery();
                i=0;
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode4.addRow(new String[]{
                            rs.getString("no_resep"),rs.getString("tgl_perawatan")+" "+rs.getString("jam"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("status"),
                            rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                            rs.getString("nm_dokter")
                        });
                        //END CUSTOM
                        tabMode4.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                        ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter.jml,"+
                            "databarang.kode_sat,resep_dokter.aturan_pakai from resep_dokter inner join databarang on "+
                            "resep_dokter.kode_brng=databarang.kode_brng where resep_dokter.no_resep=? order by databarang.kode_brng");
                        try {
                            ps2.setString(1,rs.getString("no_resep"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode4.addRow(new String[]{
                                    "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("aturan_pakai")
                                });
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        ps2=koneksi.prepareStatement(
                                "select resep_dokter_racikan.no_racik,resep_dokter_racikan.nama_racik,"+
                                "resep_dokter_racikan.kd_racik,metode_racik.nm_racik as metode,"+
                                "resep_dokter_racikan.jml_dr,resep_dokter_racikan.aturan_pakai,"+
                                "resep_dokter_racikan.keterangan from resep_dokter_racikan inner join metode_racik "+
                                "on resep_dokter_racikan.kd_racik=metode_racik.kd_racik where "+
                                "resep_dokter_racikan.no_resep=? ");
                        try {
                            ps2.setString(1,rs.getString("no_resep"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode4.addRow(new String[]{
                                    "","",rs2.getString("jml_dr")+" "+rs2.getString("metode"),"No.Racik : "+rs2.getString("no_racik"),rs2.getString("nama_racik"),rs2.getString("aturan_pakai")
                                });
                                ps3=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter_racikan_detail.jml,"+
                                    "databarang.kode_sat from resep_dokter_racikan_detail inner join databarang on resep_dokter_racikan_detail.kode_brng=databarang.kode_brng "+
                                    "where resep_dokter_racikan_detail.no_resep=? and resep_dokter_racikan_detail.no_racik=? order by databarang.kode_brng");
                                try {
                                    ps3.setString(1,rs.getString("no_resep"));
                                    ps3.setString(2,rs2.getString("no_racik"));
                                    rs3=ps3.executeQuery();
                                    while(rs3.next()){
                                        tabMode4.addRow(new String[]{
                                            "","","   "+rs3.getString("jml")+" "+rs3.getString("kode_sat"),"   "+rs3.getString("kode_brng"),"   "+rs3.getString("nama_brng"),""
                                        });
                                    }
                                } catch (Exception e) {
                                    System.out.println("Notifikasi 3 : "+e);
                                } finally{
                                    if(rs3!=null){
                                        rs3.close();
                                    }
                                    if(ps3!=null){
                                        ps3.close();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        i++;
                    }
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode4.addRow(new String[]{
                                rs.getString("no_resep"),rs.getString("tgl_perawatan")+" "+rs.getString("jam"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("status"),
                                rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                                rs.getString("nm_dokter")
                            });
                            //END CUSTOM
                            tabMode4.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                            ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter.jml,"+
                                "databarang.kode_sat,resep_dokter.aturan_pakai from resep_dokter inner join databarang on "+
                                "resep_dokter.kode_brng=databarang.kode_brng where resep_dokter.no_resep=? order by databarang.kode_brng");
                            try {
                                ps2.setString(1,rs.getString("no_resep"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode4.addRow(new String[]{
                                        "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("aturan_pakai")
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            ps2=koneksi.prepareStatement(
                                    "select resep_dokter_racikan.no_racik,resep_dokter_racikan.nama_racik,"+
                                    "resep_dokter_racikan.kd_racik,metode_racik.nm_racik as metode,"+
                                    "resep_dokter_racikan.jml_dr,resep_dokter_racikan.aturan_pakai,"+
                                    "resep_dokter_racikan.keterangan from resep_dokter_racikan inner join metode_racik "+
                                    "on resep_dokter_racikan.kd_racik=metode_racik.kd_racik where "+
                                    "resep_dokter_racikan.no_resep=? ");
                            try {
                                ps2.setString(1,rs.getString("no_resep"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode4.addRow(new String[]{
                                        "","",rs2.getString("jml_dr")+" "+rs2.getString("metode"),"No.Racik : "+rs2.getString("no_racik"),rs2.getString("nama_racik"),rs2.getString("aturan_pakai")
                                    });
                                    ps3=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter_racikan_detail.jml,"+
                                        "databarang.kode_sat from resep_dokter_racikan_detail inner join databarang on resep_dokter_racikan_detail.kode_brng=databarang.kode_brng "+
                                        "where resep_dokter_racikan_detail.no_resep=? and resep_dokter_racikan_detail.no_racik=? order by databarang.kode_brng");
                                    try {
                                        ps3.setString(1,rs.getString("no_resep"));
                                        ps3.setString(2,rs2.getString("no_racik"));
                                        rs3=ps3.executeQuery();
                                        while(rs3.next()){
                                            tabMode4.addRow(new String[]{
                                                "","","   "+rs3.getString("jml")+" "+rs3.getString("kode_sat"),"   "+rs3.getString("kode_brng"),"   "+rs3.getString("nama_brng"),""
                                            });
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Notifikasi 3 : "+e);
                                    } finally{
                                        if(rs3!=null){
                                            rs3.close();
                                        }
                                        if(ps3!=null){
                                            ps3.close();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            i++;
                        }
                    }
                }
                    
                LCount.setText(""+i++);
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            } 
            
            ps=koneksi.prepareStatement("select resep_obat.no_resep,resep_obat.tgl_perawatan,resep_obat.jam,resep_obat.no_rawat,pasien.no_rkm_medis,"+
                    " pasien.nm_pasien,resep_obat.kd_dokter,dokter.nm_dokter,if(resep_obat.tgl_perawatan='0000-00-00','Belum Terlayani','Sudah Terlayani') as status,"+
                    " bangsal.nm_bangsal,resep_obat.status as status_asal,penjab.png_jawab from resep_obat "+
                    " inner join ranap_gabung on ranap_gabung.no_rawat2=resep_obat.no_rawat "+
                    " inner join reg_periksa on resep_obat.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on resep_obat.kd_dokter=dokter.kd_dokter "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " inner join kamar_inap on ranap_gabung.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " where resep_obat.tgl_peresepan<>'0000-00-00' and kamar_inap.stts_pulang='-' and resep_obat.status='ranap' and resep_obat.tgl_peresepan between ? and ? "+
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(resep_obat.no_resep like ? or resep_obat.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by resep_obat.no_resep order by resep_obat.tgl_perawatan desc,resep_obat.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                rs=ps.executeQuery();
                i=0;
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode4.addRow(new String[]{
                            rs.getString("no_resep"),rs.getString("tgl_perawatan")+" "+rs.getString("jam"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("status"),
                            rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                            rs.getString("nm_dokter")
                        });
                        //END CUSTOM
                        tabMode4.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                        ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter.jml,"+
                            "databarang.kode_sat,resep_dokter.aturan_pakai from resep_dokter inner join databarang on "+
                            "resep_dokter.kode_brng=databarang.kode_brng where resep_dokter.no_resep=? order by databarang.kode_brng");
                        try {
                            ps2.setString(1,rs.getString("no_resep"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode4.addRow(new String[]{
                                    "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("aturan_pakai")
                                });
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        ps2=koneksi.prepareStatement(
                                "select resep_dokter_racikan.no_racik,resep_dokter_racikan.nama_racik,"+
                                "resep_dokter_racikan.kd_racik,metode_racik.nm_racik as metode,"+
                                "resep_dokter_racikan.jml_dr,resep_dokter_racikan.aturan_pakai,"+
                                "resep_dokter_racikan.keterangan from resep_dokter_racikan inner join metode_racik "+
                                "on resep_dokter_racikan.kd_racik=metode_racik.kd_racik where "+
                                "resep_dokter_racikan.no_resep=? ");
                        try {
                            ps2.setString(1,rs.getString("no_resep"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode4.addRow(new String[]{
                                    "","",rs2.getString("jml_dr")+" "+rs2.getString("metode"),"No.Racik : "+rs2.getString("no_racik"),rs2.getString("nama_racik"),rs2.getString("aturan_pakai")
                                });
                                ps3=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter_racikan_detail.jml,"+
                                    "databarang.kode_sat from resep_dokter_racikan_detail inner join databarang on resep_dokter_racikan_detail.kode_brng=databarang.kode_brng "+
                                    "where resep_dokter_racikan_detail.no_resep=? and resep_dokter_racikan_detail.no_racik=? order by databarang.kode_brng");
                                try {
                                    ps3.setString(1,rs.getString("no_resep"));
                                    ps3.setString(2,rs2.getString("no_racik"));
                                    rs3=ps3.executeQuery();
                                    while(rs3.next()){
                                        tabMode4.addRow(new String[]{
                                            "","","   "+rs3.getString("jml")+" "+rs3.getString("kode_sat"),"   "+rs3.getString("kode_brng"),"   "+rs3.getString("nama_brng"),""
                                        });
                                    }
                                } catch (Exception e) {
                                    System.out.println("Notifikasi 3 : "+e);
                                } finally{
                                    if(rs3!=null){
                                        rs3.close();
                                    }
                                    if(ps3!=null){
                                        ps3.close();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        i++;
                    }
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode4.addRow(new String[]{
                                rs.getString("no_resep"),rs.getString("tgl_perawatan")+" "+rs.getString("jam"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("status"),
                                rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                                rs.getString("nm_dokter")
                            });
                            //END CUSTOM
                            tabMode4.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                            ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter.jml,"+
                                "databarang.kode_sat,resep_dokter.aturan_pakai from resep_dokter inner join databarang on "+
                                "resep_dokter.kode_brng=databarang.kode_brng where resep_dokter.no_resep=? order by databarang.kode_brng");
                            try {
                                ps2.setString(1,rs.getString("no_resep"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode4.addRow(new String[]{
                                        "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("aturan_pakai")
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            ps2=koneksi.prepareStatement(
                                    "select resep_dokter_racikan.no_racik,resep_dokter_racikan.nama_racik,"+
                                    "resep_dokter_racikan.kd_racik,metode_racik.nm_racik as metode,"+
                                    "resep_dokter_racikan.jml_dr,resep_dokter_racikan.aturan_pakai,"+
                                    "resep_dokter_racikan.keterangan from resep_dokter_racikan inner join metode_racik "+
                                    "on resep_dokter_racikan.kd_racik=metode_racik.kd_racik where "+
                                    "resep_dokter_racikan.no_resep=? ");
                            try {
                                ps2.setString(1,rs.getString("no_resep"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode4.addRow(new String[]{
                                        "","",rs2.getString("jml_dr")+" "+rs2.getString("metode"),"No.Racik : "+rs2.getString("no_racik"),rs2.getString("nama_racik"),rs2.getString("aturan_pakai")
                                    });
                                    ps3=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,resep_dokter_racikan_detail.jml,"+
                                        "databarang.kode_sat from resep_dokter_racikan_detail inner join databarang on resep_dokter_racikan_detail.kode_brng=databarang.kode_brng "+
                                        "where resep_dokter_racikan_detail.no_resep=? and resep_dokter_racikan_detail.no_racik=? order by databarang.kode_brng");
                                    try {
                                        ps3.setString(1,rs.getString("no_resep"));
                                        ps3.setString(2,rs2.getString("no_racik"));
                                        rs3=ps3.executeQuery();
                                        while(rs3.next()){
                                            tabMode4.addRow(new String[]{
                                                "","","   "+rs3.getString("jml")+" "+rs3.getString("kode_sat"),"   "+rs3.getString("kode_brng"),"   "+rs3.getString("nama_brng"),""
                                            });
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Notifikasi 3 : "+e);
                                    } finally{
                                        if(rs3!=null){
                                            rs3.close();
                                        }
                                        if(ps3!=null){
                                            ps3.close();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            i++;
                        }
                    }
                }
                    
                LCount.setText(""+i++);
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }  
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }  
    }
    
    public void tampil5() {
        Valid.tabelKosong(tabMode5);
        try{  
            semua=CrDokter2.getText().trim().equals("")&&Kamar.getText().trim().equals("")&&TCari.getText().trim().equals("");
            ps=koneksi.prepareStatement("select permintaan_stok_obat_pasien.no_permintaan,permintaan_stok_obat_pasien.tgl_permintaan,permintaan_stok_obat_pasien.jam,"+
                    " permintaan_stok_obat_pasien.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,permintaan_stok_obat_pasien.kd_dokter,dokter.nm_dokter, "+
                    " if(permintaan_stok_obat_pasien.status='Belum','Belum Terlayani','Sudah Terlayani') as status,"+
                    " dokter.kd_sps,"+
                    " if(permintaan_stok_obat_pasien.tgl_validasi='0000-00-00','',permintaan_stok_obat_pasien.tgl_validasi) as tgl_validasi,"+  //CUSTOM
                    " if(permintaan_stok_obat_pasien.jam_validasi='00:00:00','',permintaan_stok_obat_pasien.jam_validasi) as jam_validasi,"+ //CUSTOM
                    " ifnull(permintaan_stok_obat_pasien_penyerahan.tgl_penyerahan,'') as tgl_penyerahan,"+  //CUSTOM
                    " ifnull(permintaan_stok_obat_pasien_penyerahan.jam_penyerahan,'') as jam_penyerahan,"+ //CUSTOM
                    " bangsal.nm_bangsal,kamar.kd_bangsal,penjab.png_jawab from permintaan_stok_obat_pasien  "+
                    " inner join reg_periksa on permintaan_stok_obat_pasien.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on permintaan_stok_obat_pasien.kd_dokter=dokter.kd_dokter "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " inner join kamar_inap on reg_periksa.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " left join permintaan_stok_obat_pasien_penyerahan on permintaan_stok_obat_pasien.no_permintaan=permintaan_stok_obat_pasien_penyerahan.no_permintaan "+   //CUSTOM
                    " where permintaan_stok_obat_pasien.tgl_permintaan between ? and ? "+ //CUSTOM MENAMPILKAN SEMUA RESEP -> MENGHAPUS kamar_inap.stts_pulang='-' and 
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(permintaan_stok_obat_pasien.no_permintaan like ? or permintaan_stok_obat_pasien.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by permintaan_stok_obat_pasien.no_permintaan order by permintaan_stok_obat_pasien.tgl_permintaan desc,permintaan_stok_obat_pasien.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                
                rs=ps.executeQuery();
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode5.addRow(new String[]{
                            rs.getString("no_permintaan"),rs.getString("tgl_permintaan"),rs.getString("jam"),rs.getString("no_rawat"),
                            rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                            rs.getString("kd_dokter"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("kd_bangsal"),rs.getString("png_jawab")
                            //START CUSTOM
                            ,rs.getString("kd_sps"),
                            rs.getString("tgl_validasi"),rs.getString("jam_validasi"),
                            rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                            //END CUSTOM   
                        });           
                        //END CUSTOM
                    } 
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode5.addRow(new String[]{
                                rs.getString("no_permintaan"),rs.getString("tgl_permintaan"),rs.getString("jam"),rs.getString("no_rawat"),
                                rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                                rs.getString("kd_dokter"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("kd_bangsal"),rs.getString("png_jawab")
                                //START CUSTOM
                                ,rs.getString("kd_sps"),
                                rs.getString("tgl_validasi"),rs.getString("jam_validasi"),
                                rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                                //END CUSTOM   
                            });  
                            //END CUSTOM
                        }                  
                    } 
                }               
                
                LCount.setText(""+tabMode5.getRowCount());
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }      
            
            ps=koneksi.prepareStatement("select permintaan_stok_obat_pasien.no_permintaan,permintaan_stok_obat_pasien.tgl_permintaan,permintaan_stok_obat_pasien.jam,"+
                    " permintaan_stok_obat_pasien.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,permintaan_stok_obat_pasien.kd_dokter,dokter.nm_dokter, "+
                    " if(permintaan_stok_obat_pasien.status='Belum','Belum Terlayani','Sudah Terlayani') as status,"+
                    " dokter.kd_sps,"+
                    " if(permintaan_stok_obat_pasien.tgl_validasi='0000-00-00','',permintaan_stok_obat_pasien.tgl_validasi) as tgl_validasi,"+  //CUSTOM
                    " if(permintaan_stok_obat_pasien.jam_validasi='00:00:00','',permintaan_stok_obat_pasien.jam_validasi) as jam_validasi,"+ //CUSTOM
                    " ifnull(permintaan_stok_obat_pasien_penyerahan.tgl_penyerahan,'') as tgl_penyerahan,"+  //CUSTOM
                    " ifnull(permintaan_stok_obat_pasien_penyerahan.jam_penyerahan,'') as jam_penyerahan,"+ //CUSTOM
                    " bangsal.nm_bangsal,kamar.kd_bangsal,penjab.png_jawab from permintaan_stok_obat_pasien  "+
                    " inner join ranap_gabung on ranap_gabung.no_rawat2=permintaan_stok_obat_pasien.no_rawat "+
                    " inner join reg_periksa on permintaan_stok_obat_pasien.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on permintaan_stok_obat_pasien.kd_dokter=dokter.kd_dokter "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " inner join kamar_inap on ranap_gabung.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " left join permintaan_stok_obat_pasien_penyerahan on permintaan_stok_obat_pasien.no_permintaan=permintaan_stok_obat_pasien_penyerahan.no_permintaan "+   //CUSTOM
                    " where permintaan_stok_obat_pasien.tgl_permintaan between ? and ? "+ //CUSTOM MUHSIN MENAMPILKAN SEMUA RESEP -> MENGHAPUS kamar_inap.stts_pulang='-' and 
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(permintaan_stok_obat_pasien.no_permintaan like ? or permintaan_stok_obat_pasien.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by permintaan_stok_obat_pasien.no_permintaan order by permintaan_stok_obat_pasien.tgl_permintaan desc,permintaan_stok_obat_pasien.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                
                rs=ps.executeQuery();
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode5.addRow(new String[]{
                            rs.getString("no_permintaan"),rs.getString("tgl_permintaan"),rs.getString("jam"),rs.getString("no_rawat"),
                            rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                            rs.getString("kd_dokter"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("kd_bangsal"),rs.getString("png_jawab")
                            //START CUSTOM
                            ,rs.getString("kd_sps"),
                            rs.getString("tgl_validasi"),rs.getString("jam_validasi"),
                            rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                            //END CUSTOM    
                        });           
                        //END CUSTOM
                    } 
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode5.addRow(new String[]{
                                rs.getString("no_permintaan"),rs.getString("tgl_permintaan"),rs.getString("jam"),rs.getString("no_rawat"),
                                rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                                rs.getString("kd_dokter"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("kd_bangsal"),rs.getString("png_jawab")
                                //START CUSTOM
                                ,rs.getString("kd_sps"),
                                rs.getString("tgl_validasi"),rs.getString("jam_validasi"),
                                rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                                //END CUSTOM   
                            });  
                            //END CUSTOM
                        }                  
                    } 
                }               
                
                LCount.setText(""+tabMode5.getRowCount());
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }        
    }
    
    public void tampil6() {
        Valid.tabelKosong(tabMode6);
        try{  
            semua=CrDokter2.getText().trim().equals("")&&Kamar.getText().trim().equals("")&&TCari.getText().trim().equals("");
            ps=koneksi.prepareStatement("select permintaan_stok_obat_pasien.no_permintaan,permintaan_stok_obat_pasien.tgl_permintaan,permintaan_stok_obat_pasien.jam,permintaan_stok_obat_pasien.no_rawat,pasien.no_rkm_medis,"+
                    " pasien.nm_pasien,permintaan_stok_obat_pasien.kd_dokter,dokter.nm_dokter,if(permintaan_stok_obat_pasien.status='Belum','Belum Terlayani','Sudah Terlayani') as status,"+
                    " bangsal.nm_bangsal,permintaan_stok_obat_pasien.status as status_asal,penjab.png_jawab from permintaan_stok_obat_pasien "+
                    " inner join reg_periksa on permintaan_stok_obat_pasien.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on permintaan_stok_obat_pasien.kd_dokter=dokter.kd_dokter "+
                    " inner join kamar_inap on reg_periksa.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " where permintaan_stok_obat_pasien.tgl_permintaan between ? and ? "+ //CUSTOM MUHSIN MENAMPILKAN SEMUA RESEP -> MENGHAPUS kamar_inap.stts_pulang='-' and 
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(permintaan_stok_obat_pasien.no_permintaan like ? or permintaan_stok_obat_pasien.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by permintaan_stok_obat_pasien.no_permintaan order by permintaan_stok_obat_pasien.tgl_permintaan desc,permintaan_stok_obat_pasien.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                rs=ps.executeQuery();
                i=0;
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode6.addRow(new String[]{
                            rs.getString("no_permintaan"),rs.getString("tgl_permintaan")+" "+rs.getString("jam"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("status"),
                            rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                            rs.getString("nm_dokter")
                        });
                        //END CUSTOM
                        tabMode6.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                        ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,detail_permintaan_stok_obat_pasien.jml,"+
                            "databarang.kode_sat,detail_permintaan_stok_obat_pasien.aturan_pakai,detail_permintaan_stok_obat_pasien.jam00,"+
                            "detail_permintaan_stok_obat_pasien.jam01,detail_permintaan_stok_obat_pasien.jam02,detail_permintaan_stok_obat_pasien.jam03,"+
                            "detail_permintaan_stok_obat_pasien.jam04,detail_permintaan_stok_obat_pasien.jam05,detail_permintaan_stok_obat_pasien.jam06,"+
                            "detail_permintaan_stok_obat_pasien.jam07,detail_permintaan_stok_obat_pasien.jam08,detail_permintaan_stok_obat_pasien.jam09,"+
                            "detail_permintaan_stok_obat_pasien.jam10,detail_permintaan_stok_obat_pasien.jam11,detail_permintaan_stok_obat_pasien.jam12,"+
                            "detail_permintaan_stok_obat_pasien.jam13,detail_permintaan_stok_obat_pasien.jam14,detail_permintaan_stok_obat_pasien.jam15,"+
                            "detail_permintaan_stok_obat_pasien.jam16,detail_permintaan_stok_obat_pasien.jam17,detail_permintaan_stok_obat_pasien.jam18,"+
                            "detail_permintaan_stok_obat_pasien.jam19,detail_permintaan_stok_obat_pasien.jam20,detail_permintaan_stok_obat_pasien.jam21,"+
                            "detail_permintaan_stok_obat_pasien.jam22,detail_permintaan_stok_obat_pasien.jam23 "+
                            "from detail_permintaan_stok_obat_pasien inner join databarang on detail_permintaan_stok_obat_pasien.kode_brng=databarang.kode_brng "+
                            "where detail_permintaan_stok_obat_pasien.no_permintaan=? order by databarang.kode_brng");
                        try {
                            ps2.setString(1,rs.getString("no_permintaan"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode6.addRow(new String[]{
                                    "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),
                                    "00 : "+rs2.getString("jam00").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "01 : "+rs2.getString("jam01").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "02 : "+rs2.getString("jam02").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "03 : "+rs2.getString("jam03").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "04 : "+rs2.getString("jam04").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "05 : "+rs2.getString("jam05").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "06 : "+rs2.getString("jam06").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "07 : "+rs2.getString("jam07").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "08 : "+rs2.getString("jam08").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "09 : "+rs2.getString("jam09").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "10 : "+rs2.getString("jam10").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "11 : "+rs2.getString("jam11").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "12 : "+rs2.getString("jam12").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "13 : "+rs2.getString("jam13").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "14 : "+rs2.getString("jam14").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "15 : "+rs2.getString("jam15").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "16 : "+rs2.getString("jam16").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "17 : "+rs2.getString("jam17").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "18 : "+rs2.getString("jam18").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "19 : "+rs2.getString("jam19").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "20 : "+rs2.getString("jam20").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "21 : "+rs2.getString("jam21").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "22 : "+rs2.getString("jam22").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "23 : "+rs2.getString("jam23").replaceAll("true","✓").replaceAll("false","✕")+"  |  "+
                                    rs2.getString("aturan_pakai")
                                });
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        i++;
                    }
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode6.addRow(new String[]{
                                rs.getString("no_permintaan"),rs.getString("tgl_permintaan")+" "+rs.getString("jam"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("status"),
                                rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                                rs.getString("nm_dokter")
                            });
                            //END CUSTOM
                            tabMode6.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                            ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,detail_permintaan_stok_obat_pasien.jml,"+
                                "databarang.kode_sat,detail_permintaan_stok_obat_pasien.aturan_pakai,detail_permintaan_stok_obat_pasien.jam00,"+
                                "detail_permintaan_stok_obat_pasien.jam01,detail_permintaan_stok_obat_pasien.jam02,detail_permintaan_stok_obat_pasien.jam03,"+
                                "detail_permintaan_stok_obat_pasien.jam04,detail_permintaan_stok_obat_pasien.jam05,detail_permintaan_stok_obat_pasien.jam06,"+
                                "detail_permintaan_stok_obat_pasien.jam07,detail_permintaan_stok_obat_pasien.jam08,detail_permintaan_stok_obat_pasien.jam09,"+
                                "detail_permintaan_stok_obat_pasien.jam10,detail_permintaan_stok_obat_pasien.jam11,detail_permintaan_stok_obat_pasien.jam12,"+
                                "detail_permintaan_stok_obat_pasien.jam13,detail_permintaan_stok_obat_pasien.jam14,detail_permintaan_stok_obat_pasien.jam15,"+
                                "detail_permintaan_stok_obat_pasien.jam16,detail_permintaan_stok_obat_pasien.jam17,detail_permintaan_stok_obat_pasien.jam18,"+
                                "detail_permintaan_stok_obat_pasien.jam19,detail_permintaan_stok_obat_pasien.jam20,detail_permintaan_stok_obat_pasien.jam21,"+
                                "detail_permintaan_stok_obat_pasien.jam22,detail_permintaan_stok_obat_pasien.jam23 from detail_permintaan_stok_obat_pasien inner join databarang on "+
                                "detail_permintaan_stok_obat_pasien.kode_brng=databarang.kode_brng where detail_permintaan_stok_obat_pasien.no_permintaan=? order by databarang.kode_brng");
                            try {
                                ps2.setString(1,rs.getString("no_permintaan"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode6.addRow(new String[]{
                                        "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),
                                        "00 : "+rs2.getString("jam00").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "01 : "+rs2.getString("jam01").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "02 : "+rs2.getString("jam02").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "03 : "+rs2.getString("jam03").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "04 : "+rs2.getString("jam04").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "05 : "+rs2.getString("jam05").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "06 : "+rs2.getString("jam06").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "07 : "+rs2.getString("jam07").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "08 : "+rs2.getString("jam08").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "09 : "+rs2.getString("jam09").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "10 : "+rs2.getString("jam10").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "11 : "+rs2.getString("jam11").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "12 : "+rs2.getString("jam12").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "13 : "+rs2.getString("jam13").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "14 : "+rs2.getString("jam14").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "15 : "+rs2.getString("jam15").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "16 : "+rs2.getString("jam16").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "17 : "+rs2.getString("jam17").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "18 : "+rs2.getString("jam18").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "19 : "+rs2.getString("jam19").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "20 : "+rs2.getString("jam20").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "21 : "+rs2.getString("jam21").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "22 : "+rs2.getString("jam22").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "23 : "+rs2.getString("jam23").replaceAll("true","✓").replaceAll("false","✕")+"  |  "+
                                        rs2.getString("aturan_pakai")
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            i++;
                        }
                    }
                }
                    
                LCount.setText(""+i++);
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }             
            
            ps=koneksi.prepareStatement("select permintaan_stok_obat_pasien.no_permintaan,permintaan_stok_obat_pasien.tgl_permintaan,permintaan_stok_obat_pasien.jam,permintaan_stok_obat_pasien.no_rawat,pasien.no_rkm_medis,"+
                    " pasien.nm_pasien,permintaan_stok_obat_pasien.kd_dokter,dokter.nm_dokter,if(permintaan_stok_obat_pasien.status='Belum','Belum Terlayani','Sudah Terlayani') as status,"+
                    " bangsal.nm_bangsal,permintaan_stok_obat_pasien.status as status_asal,penjab.png_jawab from permintaan_stok_obat_pasien "+
                    " inner join ranap_gabung on ranap_gabung.no_rawat2=permintaan_stok_obat_pasien.no_rawat "+
                    " inner join reg_periksa on permintaan_stok_obat_pasien.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on permintaan_stok_obat_pasien.kd_dokter=dokter.kd_dokter "+
                    " inner join kamar_inap on ranap_gabung.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " where permintaan_stok_obat_pasien.tgl_permintaan between ? and ? "+ //CUSTOM MUHSIN MENAMPILKAN SEMUA RESEP -> MENGHAPUS kamar_inap.stts_pulang='-' and 
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(permintaan_stok_obat_pasien.no_permintaan like ? or permintaan_stok_obat_pasien.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by permintaan_stok_obat_pasien.no_permintaan order by permintaan_stok_obat_pasien.tgl_permintaan desc,permintaan_stok_obat_pasien.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                rs=ps.executeQuery();
                i=0;
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode6.addRow(new String[]{
                            rs.getString("no_permintaan"),rs.getString("tgl_permintaan")+" "+rs.getString("jam"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("status"),
                            rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                            rs.getString("nm_dokter")
                        });
                        //END CUSTOM
                        tabMode6.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                        ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,detail_permintaan_stok_obat_pasien.jml,"+
                            "databarang.kode_sat,detail_permintaan_stok_obat_pasien.aturan_pakai,detail_permintaan_stok_obat_pasien.jam00,"+
                            "detail_permintaan_stok_obat_pasien.jam01,detail_permintaan_stok_obat_pasien.jam02,detail_permintaan_stok_obat_pasien.jam03,"+
                            "detail_permintaan_stok_obat_pasien.jam04,detail_permintaan_stok_obat_pasien.jam05,detail_permintaan_stok_obat_pasien.jam06,"+
                            "detail_permintaan_stok_obat_pasien.jam07,detail_permintaan_stok_obat_pasien.jam08,detail_permintaan_stok_obat_pasien.jam09,"+
                            "detail_permintaan_stok_obat_pasien.jam10,detail_permintaan_stok_obat_pasien.jam11,detail_permintaan_stok_obat_pasien.jam12,"+
                            "detail_permintaan_stok_obat_pasien.jam13,detail_permintaan_stok_obat_pasien.jam14,detail_permintaan_stok_obat_pasien.jam15,"+
                            "detail_permintaan_stok_obat_pasien.jam16,detail_permintaan_stok_obat_pasien.jam17,detail_permintaan_stok_obat_pasien.jam18,"+
                            "detail_permintaan_stok_obat_pasien.jam19,detail_permintaan_stok_obat_pasien.jam20,detail_permintaan_stok_obat_pasien.jam21,"+
                            "detail_permintaan_stok_obat_pasien.jam22,detail_permintaan_stok_obat_pasien.jam23 from detail_permintaan_stok_obat_pasien inner join databarang on "+
                            "detail_permintaan_stok_obat_pasien.kode_brng=databarang.kode_brng where detail_permintaan_stok_obat_pasien.no_permintaan=? order by databarang.kode_brng");
                        try {
                            ps2.setString(1,rs.getString("no_permintaan"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode6.addRow(new String[]{
                                    "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),
                                    "00 : "+rs2.getString("jam00").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "01 : "+rs2.getString("jam01").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "02 : "+rs2.getString("jam02").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "03 : "+rs2.getString("jam03").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "04 : "+rs2.getString("jam04").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "05 : "+rs2.getString("jam05").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "06 : "+rs2.getString("jam06").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "07 : "+rs2.getString("jam07").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "08 : "+rs2.getString("jam08").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "09 : "+rs2.getString("jam09").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "10 : "+rs2.getString("jam10").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "11 : "+rs2.getString("jam11").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "12 : "+rs2.getString("jam12").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "13 : "+rs2.getString("jam13").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "14 : "+rs2.getString("jam14").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "15 : "+rs2.getString("jam15").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "16 : "+rs2.getString("jam16").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "17 : "+rs2.getString("jam17").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "18 : "+rs2.getString("jam18").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "19 : "+rs2.getString("jam19").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "20 : "+rs2.getString("jam20").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "21 : "+rs2.getString("jam21").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "22 : "+rs2.getString("jam22").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                    "23 : "+rs2.getString("jam23").replaceAll("true","✓").replaceAll("false","✕")+"  |  "+
                                    rs2.getString("aturan_pakai")
                                });
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        i++;
                    }
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode6.addRow(new String[]{
                                rs.getString("no_permintaan"),rs.getString("tgl_permintaan")+" "+rs.getString("jam"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("status"),
                                rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                                rs.getString("nm_dokter")
                            });
                            //END CUSTOM
                            tabMode6.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                            ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,detail_permintaan_stok_obat_pasien.jml,"+
                                "databarang.kode_sat,detail_permintaan_stok_obat_pasien.aturan_pakai,detail_permintaan_stok_obat_pasien.jam00,"+
                                "detail_permintaan_stok_obat_pasien.jam01,detail_permintaan_stok_obat_pasien.jam02,detail_permintaan_stok_obat_pasien.jam03,"+
                                "detail_permintaan_stok_obat_pasien.jam04,detail_permintaan_stok_obat_pasien.jam05,detail_permintaan_stok_obat_pasien.jam06,"+
                                "detail_permintaan_stok_obat_pasien.jam07,detail_permintaan_stok_obat_pasien.jam08,detail_permintaan_stok_obat_pasien.jam09,"+
                                "detail_permintaan_stok_obat_pasien.jam10,detail_permintaan_stok_obat_pasien.jam11,detail_permintaan_stok_obat_pasien.jam12,"+
                                "detail_permintaan_stok_obat_pasien.jam13,detail_permintaan_stok_obat_pasien.jam14,detail_permintaan_stok_obat_pasien.jam15,"+
                                "detail_permintaan_stok_obat_pasien.jam16,detail_permintaan_stok_obat_pasien.jam17,detail_permintaan_stok_obat_pasien.jam18,"+
                                "detail_permintaan_stok_obat_pasien.jam19,detail_permintaan_stok_obat_pasien.jam20,detail_permintaan_stok_obat_pasien.jam21,"+
                                "detail_permintaan_stok_obat_pasien.jam22,detail_permintaan_stok_obat_pasien.jam23 from detail_permintaan_stok_obat_pasien inner join databarang on "+
                                "detail_permintaan_stok_obat_pasien.kode_brng=databarang.kode_brng where detail_permintaan_stok_obat_pasien.no_permintaan=? order by databarang.kode_brng");
                            try {
                                ps2.setString(1,rs.getString("no_permintaan"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode6.addRow(new String[]{
                                        "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),
                                        "00 : "+rs2.getString("jam00").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "01 : "+rs2.getString("jam01").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "02 : "+rs2.getString("jam02").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "03 : "+rs2.getString("jam03").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "04 : "+rs2.getString("jam04").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "05 : "+rs2.getString("jam05").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "06 : "+rs2.getString("jam06").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "07 : "+rs2.getString("jam07").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "08 : "+rs2.getString("jam08").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "09 : "+rs2.getString("jam09").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "10 : "+rs2.getString("jam10").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "11 : "+rs2.getString("jam11").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "12 : "+rs2.getString("jam12").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "13 : "+rs2.getString("jam13").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "14 : "+rs2.getString("jam14").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "15 : "+rs2.getString("jam15").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "16 : "+rs2.getString("jam16").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "17 : "+rs2.getString("jam17").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "18 : "+rs2.getString("jam18").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "19 : "+rs2.getString("jam19").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "20 : "+rs2.getString("jam20").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "21 : "+rs2.getString("jam21").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "22 : "+rs2.getString("jam22").replaceAll("true","✓").replaceAll("false","✕")+"  "+
                                        "23 : "+rs2.getString("jam23").replaceAll("true","✓").replaceAll("false","✕")+"  |  "+
                                        rs2.getString("aturan_pakai")
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            i++;
                        }
                    }
                }
                    
                LCount.setText(""+i++);
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }   
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }  
    }
    
    public void tampil7() {
        Valid.tabelKosong(tabMode7);
        try{  
            semua=CrDokter2.getText().trim().equals("")&&Kamar.getText().trim().equals("")&&TCari.getText().trim().equals("");
            ps=koneksi.prepareStatement("select permintaan_resep_pulang.no_permintaan,permintaan_resep_pulang.tgl_permintaan,permintaan_resep_pulang.jam,"+
                    " permintaan_resep_pulang.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,permintaan_resep_pulang.kd_dokter,dokter.nm_dokter, "+
                    " if(permintaan_resep_pulang.status='Belum','Belum Terlayani','Sudah Terlayani') as status,"+
                    " if(permintaan_resep_pulang.tgl_validasi='0000-00-00','',permintaan_resep_pulang.tgl_validasi) as tgl_validasi,"+  //CUSTOM
                    " if(permintaan_resep_pulang.jam_validasi='00:00:00','',permintaan_resep_pulang.jam_validasi) as jam_validasi,"+ //CUSTOM
                    " ifnull(permintaan_resep_pulang_penyerahan.tgl_penyerahan,'') as tgl_penyerahan,"+  //CUSTOM
                    " ifnull(permintaan_resep_pulang_penyerahan.jam_penyerahan,'') as jam_penyerahan,"+ //CUSTOM
                    " bangsal.nm_bangsal,kamar.kd_bangsal,penjab.png_jawab from permintaan_resep_pulang  "+
                    " inner join reg_periksa on permintaan_resep_pulang.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on permintaan_resep_pulang.kd_dokter=dokter.kd_dokter "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " inner join kamar_inap on reg_periksa.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " left join permintaan_resep_pulang_penyerahan on permintaan_resep_pulang.no_permintaan=permintaan_resep_pulang_penyerahan.no_permintaan "+   //CUSTOM
                    " where permintaan_resep_pulang.tgl_permintaan between ? and ? "+   //CUSTOM MUHSIN MENAMPILKAN SEMUA RESEP -> MENGHAPUS kamar_inap.stts_pulang='-' and 
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(permintaan_resep_pulang.no_permintaan like ? or permintaan_resep_pulang.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by permintaan_resep_pulang.no_permintaan order by permintaan_resep_pulang.tgl_permintaan desc,permintaan_resep_pulang.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                
                rs=ps.executeQuery();
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode7.addRow(new String[]{
                            rs.getString("no_permintaan"),rs.getString("tgl_permintaan"),rs.getString("jam"),rs.getString("no_rawat"),
                            rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                            rs.getString("kd_dokter"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("kd_bangsal"),rs.getString("png_jawab")
                            //START CUSTOM
                            ,rs.getString("tgl_validasi"),rs.getString("jam_validasi"),
                            rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                            //END CUSTOM    
                        });           
                        //END CUSTOM
                    } 
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode7.addRow(new String[]{
                                rs.getString("no_permintaan"),rs.getString("tgl_permintaan"),rs.getString("jam"),rs.getString("no_rawat"),
                                rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                                rs.getString("kd_dokter"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("kd_bangsal"),rs.getString("png_jawab")
                                //START CUSTOM
                                ,rs.getString("tgl_validasi"),rs.getString("jam_validasi"),
                                rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                                //END CUSTOM    
                            });  
                            //END CUSTOM
                        }                  
                    } 
                }               
                
                LCount.setText(""+tabMode7.getRowCount());
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }      
            
            ps=koneksi.prepareStatement("select permintaan_resep_pulang.no_permintaan,permintaan_resep_pulang.tgl_permintaan,permintaan_resep_pulang.jam,"+
                    " permintaan_resep_pulang.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,permintaan_resep_pulang.kd_dokter,dokter.nm_dokter, "+
                    " if(permintaan_resep_pulang.status='Belum','Belum Terlayani','Sudah Terlayani') as status,"+
                    " if(permintaan_resep_pulang.tgl_validasi='0000-00-00','',permintaan_resep_pulang.tgl_validasi) as tgl_validasi,"+  //CUSTOM
                    " if(permintaan_resep_pulang.jam_validasi='00:00:00','',permintaan_resep_pulang.jam_validasi) as jam_validasi,"+ //CUSTOM
                    " ifnull(permintaan_resep_pulang_penyerahan.tgl_penyerahan,'') as tgl_penyerahan,"+  //CUSTOM
                    " ifnull(permintaan_resep_pulang_penyerahan.jam_penyerahan,'') as jam_penyerahan,"+ //CUSTOM
                    " bangsal.nm_bangsal,kamar.kd_bangsal,penjab.png_jawab from permintaan_resep_pulang  "+
                    " inner join ranap_gabung on ranap_gabung.no_rawat2=permintaan_resep_pulang.no_rawat "+
                    " inner join reg_periksa on permintaan_resep_pulang.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on permintaan_resep_pulang.kd_dokter=dokter.kd_dokter "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " inner join kamar_inap on ranap_gabung.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " left join permintaan_resep_pulang_penyerahan on permintaan_resep_pulang.no_permintaan=permintaan_resep_pulang_penyerahan.no_permintaan "+   //CUSTOM
                    " where permintaan_resep_pulang.tgl_permintaan between ? and ? "+   //CUSTOM MUHSIN MENAMPILKAN SEMUA RESEP -> MENGHAPUS kamar_inap.stts_pulang='-' and 
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(permintaan_resep_pulang.no_permintaan like ? or permintaan_resep_pulang.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by permintaan_resep_pulang.no_permintaan order by permintaan_resep_pulang.tgl_permintaan desc,permintaan_resep_pulang.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                
                rs=ps.executeQuery();
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode7.addRow(new String[]{
                            rs.getString("no_permintaan"),rs.getString("tgl_permintaan"),rs.getString("jam"),rs.getString("no_rawat"),
                            rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                            rs.getString("kd_dokter"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("kd_bangsal"),rs.getString("png_jawab")
                            //START CUSTOM
                            ,rs.getString("tgl_validasi"),rs.getString("jam_validasi"),
                            rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                            //END CUSTOM    
                        });           
                        //END CUSTOM
                    } 
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode7.addRow(new String[]{
                                rs.getString("no_permintaan"),rs.getString("tgl_permintaan"),rs.getString("jam"),rs.getString("no_rawat"),
                                rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),rs.getString("status"),
                                rs.getString("kd_dokter"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("kd_bangsal"),rs.getString("png_jawab")
                                //START CUSTOM
                                ,rs.getString("tgl_validasi"),rs.getString("jam_validasi"),
                                rs.getString("tgl_penyerahan"),rs.getString("jam_penyerahan")
                                //END CUSTOM    
                            });  
                            //END CUSTOM
                        }                  
                    } 
                }               
                
                LCount.setText(""+tabMode7.getRowCount());
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }        
    }
    
    public void tampil8() {
        Valid.tabelKosong(tabMode8);
        try{  
            semua=CrDokter2.getText().trim().equals("")&&Kamar.getText().trim().equals("")&&TCari.getText().trim().equals("");
            ps=koneksi.prepareStatement("select permintaan_resep_pulang.no_permintaan,permintaan_resep_pulang.tgl_permintaan,permintaan_resep_pulang.jam,permintaan_resep_pulang.no_rawat,pasien.no_rkm_medis,"+
                    " pasien.nm_pasien,permintaan_resep_pulang.kd_dokter,dokter.nm_dokter,if(permintaan_resep_pulang.status='Belum','Belum Terlayani','Sudah Terlayani') as status,"+
                    " bangsal.nm_bangsal,permintaan_resep_pulang.status as status_asal,penjab.png_jawab from permintaan_resep_pulang "+
                    " inner join reg_periksa on permintaan_resep_pulang.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on permintaan_resep_pulang.kd_dokter=dokter.kd_dokter "+
                    " inner join kamar_inap on reg_periksa.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " where permintaan_resep_pulang.tgl_permintaan between ? and ? "+   //CUSTOM MUHSIN MENAMPILKAN SEMUA RESEP PULANG -> HAPUS kamar_inap.stts_pulang='-' and 
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(permintaan_resep_pulang.no_permintaan like ? or permintaan_resep_pulang.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by permintaan_resep_pulang.no_permintaan order by permintaan_resep_pulang.tgl_permintaan desc,permintaan_resep_pulang.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                rs=ps.executeQuery();
                i=0;
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode8.addRow(new String[]{
                            rs.getString("no_permintaan"),rs.getString("tgl_permintaan")+" "+rs.getString("jam"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("status"),
                            rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                            rs.getString("nm_dokter")
                        });
                        //END CUSTOM
                        tabMode8.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                        ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,detail_permintaan_resep_pulang.jml,"+
                            "databarang.kode_sat,detail_permintaan_resep_pulang.dosis "+
                            "from detail_permintaan_resep_pulang inner join databarang on detail_permintaan_resep_pulang.kode_brng=databarang.kode_brng "+
                            "where detail_permintaan_resep_pulang.no_permintaan=? order by databarang.kode_brng");
                        try {
                            ps2.setString(1,rs.getString("no_permintaan"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode8.addRow(new String[]{
                                    "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("dosis")
                                });
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        i++;
                    }
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode8.addRow(new String[]{
                                rs.getString("no_permintaan"),rs.getString("tgl_permintaan")+" "+rs.getString("jam"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("status"),
                                rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                                rs.getString("nm_dokter")
                            });
                            //END CUSTOM
                            tabMode8.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                            ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,detail_permintaan_resep_pulang.jml,"+
                                "databarang.kode_sat,detail_permintaan_resep_pulang.dosis from detail_permintaan_resep_pulang inner join databarang on "+
                                "detail_permintaan_resep_pulang.kode_brng=databarang.kode_brng where detail_permintaan_resep_pulang.no_permintaan=? order by databarang.kode_brng");
                            try {
                                ps2.setString(1,rs.getString("no_permintaan"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode8.addRow(new String[]{
                                        "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("dosis")
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            i++;
                        }
                    }
                }
                    
                LCount.setText(""+i++);
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }             
            
            ps=koneksi.prepareStatement("select permintaan_resep_pulang.no_permintaan,permintaan_resep_pulang.tgl_permintaan,permintaan_resep_pulang.jam,permintaan_resep_pulang.no_rawat,pasien.no_rkm_medis,"+
                    " pasien.nm_pasien,permintaan_resep_pulang.kd_dokter,dokter.nm_dokter,if(permintaan_resep_pulang.status='Belum','Belum Terlayani','Sudah Terlayani') as status,"+
                    " bangsal.nm_bangsal,permintaan_resep_pulang.status as status_asal,penjab.png_jawab from permintaan_resep_pulang "+
                    " inner join ranap_gabung on ranap_gabung.no_rawat2=permintaan_resep_pulang.no_rawat "+
                    " inner join reg_periksa on permintaan_resep_pulang.no_rawat=reg_periksa.no_rawat "+
                    " inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    " inner join dokter on permintaan_resep_pulang.kd_dokter=dokter.kd_dokter "+
                    " inner join kamar_inap on ranap_gabung.no_rawat=kamar_inap.no_rawat "+
                    " inner join kamar on kamar_inap.kd_kamar=kamar.kd_kamar "+
                    " inner join bangsal on kamar.kd_bangsal=bangsal.kd_bangsal "+
                    " inner join penjab on reg_periksa.kd_pj=penjab.kd_pj "+
                    " where permintaan_resep_pulang.tgl_permintaan between ? and ? "+   //CUSTOM MUHSIN MENAMPILKAN SEMUA RESEP -> MENGHAPUS kamar_inap.stts_pulang='-' and 
                    (semua?"":"and dokter.nm_dokter like ? and bangsal.nm_bangsal like ? and "+
                    "(permintaan_resep_pulang.no_permintaan like ? or permintaan_resep_pulang.no_rawat like ? or "+
                    "pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or dokter.nm_dokter like ? or penjab.png_jawab like ?)")+
                    " group by permintaan_resep_pulang.no_permintaan order by permintaan_resep_pulang.tgl_permintaan desc,permintaan_resep_pulang.jam desc");
            try{
                ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                if(!semua){
                    ps.setString(3,"%"+CrDokter2.getText().trim()+"%");
                    ps.setString(4,"%"+Kamar.getText().trim()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,"%"+TCari.getText()+"%");
                }
                rs=ps.executeQuery();
                i=0;
                if(cmbStatus.getSelectedItem().toString().equals("Semua")){
                    while(rs.next()){
                        //START CUSTOM
                        tabMode8.addRow(new String[]{
                            rs.getString("no_permintaan"),rs.getString("tgl_permintaan")+" "+rs.getString("jam"),
                            Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                            "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                            "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                            "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                            rs.getString("status"),
                            rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                            rs.getString("nm_dokter")
                        });
                        //END CUSTOM
                        tabMode8.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                        ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,detail_permintaan_resep_pulang.jml,"+
                            "databarang.kode_sat,detail_permintaan_resep_pulang.dosis from detail_permintaan_resep_pulang inner join databarang on "+
                            "detail_permintaan_resep_pulang.kode_brng=databarang.kode_brng where detail_permintaan_resep_pulang.no_permintaan=? order by databarang.kode_brng");
                        try {
                            ps2.setString(1,rs.getString("no_permintaan"));
                            rs2=ps2.executeQuery();
                            while(rs2.next()){
                                tabMode8.addRow(new String[]{
                                    "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("dosis")
                                });
                            }
                        } catch (Exception e) {
                            System.out.println("Notifikasi 2 : "+e);
                        } finally{
                            if(rs2!=null){
                                rs2.close();
                            }
                            if(ps2!=null){
                                ps2.close();
                            }
                        }
                        i++;
                    }
                }else{
                    while(rs.next()){
                        if(rs.getString("status").equals(cmbStatus.getSelectedItem().toString())){
                            //START CUSTOM
                            tabMode8.addRow(new String[]{
                                rs.getString("no_permintaan"),rs.getString("tgl_permintaan")+" "+rs.getString("jam"),
                                Sequel.cariIsi("select concat(kamar_inap.kd_kamar,' ',b.nm_bangsal) as kamar " +
                                "from kamar_inap inner join kamar k on kamar_inap.kd_kamar = k.kd_kamar " +
                                "inner join bangsal b on k.kd_bangsal = b.kd_bangsal " +
                                "where no_rawat=? order by kamar_inap.tgl_masuk desc limit 1",rs.getString("no_rawat")),
                                rs.getString("status"),
                                rs.getString("no_rawat")+" "+rs.getString("no_rkm_medis")+" "+rs.getString("nm_pasien")+" ("+rs.getString("png_jawab")+")",
                                rs.getString("nm_dokter")
                            });
                            //END CUSTOM
                            tabMode8.addRow(new String[]{"","","Jumlah","Kode Obat","Nama Obat","Aturan Pakai"});                
                            ps2=koneksi.prepareStatement("select databarang.kode_brng,databarang.nama_brng,detail_permintaan_resep_pulang.jml,"+
                                "databarang.kode_sat,detail_permintaan_resep_pulang.dosis from detail_permintaan_resep_pulang inner join databarang on "+
                                "detail_permintaan_resep_pulang.kode_brng=databarang.kode_brng where detail_permintaan_resep_pulang.no_permintaan=? order by databarang.kode_brng");
                            try {
                                ps2.setString(1,rs.getString("no_permintaan"));
                                rs2=ps2.executeQuery();
                                while(rs2.next()){
                                    tabMode8.addRow(new String[]{
                                        "","",rs2.getString("jml")+" "+rs2.getString("kode_sat"),rs2.getString("kode_brng"),rs2.getString("nama_brng"),rs2.getString("dosis")
                                    });
                                }
                            } catch (Exception e) {
                                System.out.println("Notifikasi 2 : "+e);
                            } finally{
                                if(rs2!=null){
                                    rs2.close();
                                }
                                if(ps2!=null){
                                    ps2.close();
                                }
                            }
                            i++;
                        }
                    }
                }
                    
                LCount.setText(""+i++);
            } catch(Exception ex){
                System.out.println("Notifikasi : "+ex);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }   
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }  
    }
    
    private void jam(){
        ActionListener taskPerformer = (ActionEvent e) -> {
            if(aktif==true){
                nol_detik = "";
                Date now = Calendar.getInstance().getTime();
                nilai_detik = now.getSeconds();
                if (nilai_detik <= 9) {
                    nol_detik = "0";
                }
                //START CUSTOM MUHSIN
                jam_jadi = now.getHours();
                menit_jadi = now.getMinutes();
                detik_jadi = now.getSeconds();
                //END CUSTOM

                detik = nol_detik + Integer.toString(nilai_detik);
                if(detik.equals("05")){
                    resepbaru=0;
                    if(formalarm.contains("ralan")){
                        tampil();
                        for(i=0;i<tbResepRalan.getRowCount();i++){
                            if(tbResepRalan.getValueAt(i,7).toString().equals("Belum Terlayani")){
                                resepbaru++;
                            }
                        }
                    }

                    if(formalarm.contains("ranap")){
                        tampil3();
                        for(i=0;i<tbResepRanap.getRowCount();i++){
                            if(tbResepRanap.getValueAt(i,7).toString().equals("Belum Terlayani")){
                                resepbaru++;
                            }
                        }
                        
                        tampil5();
                        for(i=0;i<tbPermintaanStok.getRowCount();i++){
                            if(tbPermintaanStok.getValueAt(i,7).toString().equals("Belum Terlayani")){
                                resepbaru++;
                            }
                        }
                        
                        tampil7();
                        for(i=0;i<tbPermintaanResepPulang.getRowCount();i++){
                            if(tbPermintaanResepPulang.getValueAt(i,7).toString().equals("Belum Terlayani")){
                                resepbaru++;
                            }
                        }
                    }

                    if(resepbaru>0){
                        try {
                            music = new BackgroundMusic("./suara/alarm.mp3");
                            music.start();
                        } catch (Exception ex) {
                            System.out.println(ex);
                        }
                    }
                }
            }                
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
    //START CUSTOM
    private void tampilObatKronis(String norm_pasienkronis, String norawat_pasienkronis) {
        try{
            jml=0;
            for(i=0;i<tbObatKronis.getRowCount();i++){
                if(tbObatKronis.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            riw_obatkronis=null;
            riw_obatkronis=new String[jml];
            jumlah_obat=null;
            jumlah_obat=new String[jml];
            signa=null;
            signa=new String[jml];
            tgl_reg=null;
            tgl_reg=new String[jml];
            nama_poli=null;
            nama_poli=new String[jml];
            nama_dokter=null;
            nama_dokter=new String [jml];

            index=0;        
            for(i=0;i<tbObatKronis.getRowCount();i++){
                if(tbObatKronis.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    riw_obatkronis[index]=tbObatKronis.getValueAt(i,1).toString();
                    jumlah_obat[index]=tbObatKronis.getValueAt(i,2).toString();
                    signa[index]=tbObatKronis.getValueAt(i,3).toString();
                    tgl_reg[index]=tbObatKronis.getValueAt(i,4).toString();
                    nama_poli[index]=tbObatKronis.getValueAt(i,5).toString();
                    nama_dokter[index]=tbObatKronis.getValueAt(i,6).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeObatKronis);

            for(i=0;i<jml;i++){
                tabModeObatKronis.addRow(new Object[] {
                    pilih[i],riw_obatkronis[i],jumlah_obat[i],signa[i],tgl_reg[i],nama_poli[i],nama_dokter[i]
                });
            }
            ps=koneksi.prepareStatement("SELECT d.kode_brng,d.tgl_perawatan,db.nama_brng,\n" +
                "(SELECT dpo.jml FROM detail_pemberian_obat dpo WHERE dpo.no_rawat=d.no_rawat AND dpo.kode_brng=d.kode_brng) AS jumlah,\n" +
                "(select aturan from aturan_pakai ap where ap.tgl_perawatan=d.tgl_perawatan and ap.jam=d.jam and ap.no_rawat=d.no_rawat and ap.kode_brng=d.kode_brng) AS signa,\n" +
                "poliklinik.nm_poli,dokter.nm_dokter\n" +
                "FROM pasien p\n" +
                "INNER JOIN reg_periksa rp ON rp.no_rkm_medis = p.no_rkm_medis\n" +
                "INNER JOIN detail_pemberian_obat_kronis d ON d.no_rawat = rp.no_rawat\n" +
                "INNER JOIN databarang db ON db.kode_brng=d.kode_brng\n" +
                "INNER JOIN poliklinik ON poliklinik.kd_poli=rp.kd_poli\n" +
                "INNER JOIN dokter ON dokter.kd_dokter=rp.kd_dokter\n" +
                "WHERE p.no_rkm_medis = ?\n" +
                "AND d.tgl_perawatan >= CURDATE() - INTERVAL 3 MONTH "+
                "ORDER BY d.tgl_perawatan desc");
            try {
                ps.setString(1,norm_pasienkronis);
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeObatKronis.addRow(new Object[]{
                        false,rs.getString("nama_brng"),rs.getString("jumlah"),rs.getString("signa"),
                        rs.getString("tgl_perawatan"),rs.getString("nm_poli"),rs.getString("nm_dokter")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif ps obatkronis: "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi tampilObatkronis: "+e);
        }

        //CARI STATUS PRB
        jLabel66.setText("  "+Sequel.cariIsi("select prb from bpjs_prb where no_sep=?",Sequel.cariIsi("select no_sep from bridging_sep where no_rawat=?",norawat_pasienkronis)));
        if(!jLabel66.getText().equals("  ")){
            jLabel66.setOpaque(true); // Mengaktifkan opaque agar background bisa terlihat
            jLabel66.setBackground(java.awt.Color.RED); // Mengatur latar belakang merah
            jLabel66.setForeground(java.awt.Color.WHITE); // Mengatur warna teks menjadi hitam
        }else{
            jLabel66.setOpaque(false); // Menonaktifkan opaque agar background bisa terlihat
        }        
    }      
    //END CUSTOM
}
