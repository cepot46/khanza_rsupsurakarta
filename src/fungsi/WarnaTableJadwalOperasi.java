/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fungsi;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Owner
 */
public class WarnaTableJadwalOperasi extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        //compare string dengan value status dilayani
        String kategori = (String)table.getModel().getValueAt(row, 18);
        if (row % 2 == 1){
            //JIKA CITO
            if(kategori.equalsIgnoreCase("Cito")){
                component.setBackground(new Color(255,201,102));
            }else{
                component.setBackground(new Color(255,246,244));
            }
        }else{
            if(kategori.equalsIgnoreCase("Cito")){
                component.setBackground(new Color(255,201,102));
            }else{
                component.setBackground(new Color(255,255,255));
            }
        }
        
        return component;
    }

}