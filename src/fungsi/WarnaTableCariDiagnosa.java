package fungsi;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Renderer untuk pewarnaan tabel berdasarkan status.
 */
public class WarnaTableCariDiagnosa extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        // Panggil super class untuk mendapatkan komponen dasar
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        // Periksa jika ini adalah kolom ke-4 (indeks 3)
        if (column == 3) {
            // Jika kolom ke-3 berisi "Diagnosa Faskes 1", ubah warna latar belakang dan teks
            if (value != null && value.toString().equals("Diagnosa Faskes 1")) {
                component.setBackground(new Color(255, 0, 0));  // Merah
                component.setForeground(new Color(255, 255, 0));  // Kuning
                component.setFont(component.getFont().deriveFont(Font.BOLD));  // Set font menjadi bold
            } else {
                // Jika tidak ada "Diagnosa Faskes 1", gunakan warna default
                if (row % 2 == 1) {
                    component.setBackground(new Color(255, 244, 244));  // Warna baris ganjil
                    component.setForeground(new Color(50, 50, 50));  // Warna teks standar
                } else {
                    component.setBackground(new Color(255, 255, 255));  // Warna baris genap
                    component.setForeground(new Color(50, 50, 50));  // Warna teks standar
                }
                component.setFont(component.getFont().deriveFont(Font.PLAIN));  // Set font menjadi normal (tidak bold)
            }
        } else {
            // Untuk kolom selain kolom ke-3, berikan warna bergantian untuk baris
            if (row % 2 == 1) {
                component.setBackground(new Color(255, 244, 244));  // Warna baris ganjil
                component.setForeground(new Color(50, 50, 50));  // Warna teks standar
            } else {
                component.setBackground(new Color(255, 255, 255));  // Warna baris genap
                component.setForeground(new Color(50, 50, 50));  // Warna teks standar
            }
            component.setFont(component.getFont().deriveFont(Font.PLAIN));  // Set font menjadi normal (tidak bold)
        }

        return component;
    }
}
