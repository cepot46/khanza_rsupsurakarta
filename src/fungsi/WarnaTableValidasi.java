/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fungsi;

import java.awt.Color;
import java.awt.Component;
import java.time.Duration;
import java.time.LocalDate;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Owner
 */
public class WarnaTableValidasi extends DefaultTableCellRenderer {
    public int kolom;
    private final sekuel sek=new sekuel();
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        String temp = sek.cariIsi("select no_rkm_medis from reg_periksa inner join rujukan_internal_poli rip on reg_periksa.no_rawat = rip.no_rawat where reg_periksa.no_rawat = '"+table.getValueAt(row,3)+"'");
        if (row % 2 == 1){  //GANJIL
            //CEK JIKA POLI EKSEKUTIF
            if (table.getValueAt(row, 9).toString().toLowerCase().contains("eksekutif")){
                component.setBackground(new Color(0, 230, 0));
            }
            //CEK JIKA IGD
            else if(table.getValueAt(row,9).equals("IGD")){
                component.setBackground(new Color(255, 201, 102));
            }
            //CEK JIKA RABER
            else if(!(temp.isBlank())){  //NGGAK KOSONG ALIAS RABER
                component.setBackground(new Color(255,255,128));
            }else{  //ENGGAK RABER
                component.setBackground(new Color(255,246,244));
            }
        }else{  //GENAP
            //CEK JIKA POLI EKSEKUTIF
            if (table.getValueAt(row, 9).toString().toLowerCase().contains("eksekutif")){
                component.setBackground(new Color(0, 230, 0));
            }
            //CEK JIKA IGD
            else if(table.getValueAt(row,9).equals("IGD")){
                component.setBackground(new Color(255, 201, 102));
            }
            //CEK JIKA RABER
            else if(!(temp.isBlank())){  //NGGAK KOSONG ALIAS RABER
                component.setBackground(new Color(255,255,128));
            }else{  //ENGGAK RABER
                component.setBackground(new Color(255,255,255));
            }
        }
        return component;
    }

}
