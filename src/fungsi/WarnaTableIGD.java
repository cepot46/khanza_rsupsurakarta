/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fungsi;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Owner
 */
public class WarnaTableIGD extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        //compare string dengan value status dilayani
        String status = (String)table.getModel().getValueAt(row, 18);
        String statusbayar = (String)table.getModel().getValueAt(row, 20);
        //belum SOAP dengan perawat
        if("Belum".equalsIgnoreCase(status)||"Berkas Diterima".equalsIgnoreCase(status)){
            component.setBackground(new Color(255,255,153));
            //sudah SOAP dengan perawat
        }
        else if("Sudah".equalsIgnoreCase(status)||"Dirujuk".equalsIgnoreCase(status)||"Dirawat".equalsIgnoreCase(status)){
                //belum bayar
                if("Belum Bayar".equalsIgnoreCase(statusbayar)){
                    component.setBackground(new Color(102,204,255));
                }
                //sudah bayar
                else if("Sudah Bayar".equalsIgnoreCase(statusbayar)){
                    component.setBackground(new Color(255,255,255));
                }
        }
        else if("Batal".equalsIgnoreCase(status)||"Meninggal".equalsIgnoreCase(status)||"Pulang Paksa".equalsIgnoreCase(status)){
            component.setBackground(new Color(255,255,255));
        }//tambahan status periksa lain
        
        return component;
    }

}