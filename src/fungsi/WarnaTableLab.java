/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fungsi;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Owner
 */
public class WarnaTableLab extends DefaultTableCellRenderer {
    public int kolom;
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (row % 2 == 1){  //GANJIL
            //CEK JIKA POLI EKSEKUTIF
            if(table.getValueAt(row,11).toString().toLowerCase().contains("eksekutif")){
                component.setBackground(new Color(0, 230, 0));
            }else{  //ENGGAK EKSE
                component.setBackground(new Color(255,246,244));
            }
        }else{  //GENAP
            //CEK JIKA POLI EKSEKUTIF
            if(table.getValueAt(row,11).toString().toLowerCase().contains("eksekutif")){
                component.setBackground(new Color(0, 230, 0));
            }else{  //ENGGAK EKSE
                component.setBackground(new Color(255,255,255));
            }
        }
        return component;
    }

}
