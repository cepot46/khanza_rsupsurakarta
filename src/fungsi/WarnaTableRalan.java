package fungsi;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Renderer untuk pewarnaan tabel berdasarkan status.
 */
public class WarnaTableRalan extends DefaultTableCellRenderer {
    private static final Color COLOR_BELUM = new Color(255, 255, 153);
    private static final Color COLOR_SUDAH = new Color(0, 230, 0);
    private static final Color COLOR_BELUM_BAYAR_RESEP_SEMUA_TERVALIDASI = new Color(255, 201, 102);
    private static final Color COLOR_BELUM_BAYAR_RESEP_TIDAK_TERVALIDASI = new Color(102, 204, 255);
    private static final Color COLOR_SUDAH_BAYAR = new Color(255, 255, 255);
    private static final Color COLOR_BATAL = new Color(255, 255, 255);
    private static final Color COLOR_BERKAS_LENGKAP = new Color(255, 153, 206);

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        String status = (String) table.getModel().getValueAt(row, 10);
        String statusBayar = (String) table.getModel().getValueAt(row, 15);

        switch (status.toLowerCase()) {
            case "belum":
            case "berkas diterima":
                component.setBackground(COLOR_BELUM);
                if (table.getModel().getValueAt(row, 4) != null && table.getModel().getValueAt(row, 4).toString().toLowerCase().contains("eksekutif")) {
                    component.setBackground(COLOR_SUDAH);
                }
                break;
            case "sudah":
            case "dirujuk":
            case "dirawat":
                if ("belum bayar".equalsIgnoreCase(statusBayar)) {
                    if (!"0".equals(table.getModel().getValueAt(row, 21))) {
                        if ("0".equals(table.getModel().getValueAt(row, 22))) {
                            component.setBackground(COLOR_BELUM_BAYAR_RESEP_SEMUA_TERVALIDASI);
                        } else {
                            component.setBackground(COLOR_BELUM_BAYAR_RESEP_TIDAK_TERVALIDASI);
                        }
                    } else {
                        component.setBackground(COLOR_BELUM_BAYAR_RESEP_TIDAK_TERVALIDASI);
                    }
                } else if ("sudah bayar".equalsIgnoreCase(statusBayar)) {
                    component.setBackground(COLOR_SUDAH_BAYAR);
                }
                break;
            case "batal":
            case "meninggal":
            case "pulang paksa":
                component.setBackground(COLOR_BATAL);
                break;
            case "berkas lengkap":
                component.setBackground(COLOR_BERKAS_LENGKAP);
                break;
            default:
                component.setBackground(COLOR_SUDAH_BAYAR); // default color if no status matches
                break;
        }
        
        return component;
    }
}
