/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fungsi;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Owner
 */
public class WarnaTableRanap extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        //compare string dengan value status dilayani
        String dpjp = (String)table.getModel().getValueAt(row, 4);
        //jika DPJP null atau empty
        if(dpjp == null || dpjp.isEmpty()){
            component.setBackground(new Color(255,255,153));
            //dpjp null atau kosong
        }
        else {
            //CEK PERKIRAAN BIAYA RANAP
            if(table.getModel().getValueAt(row, 23).equals("Aman")){
                component.setBackground(new Color(255,255,255));
            }else if(table.getModel().getValueAt(row, 23).equals("75%")){
                //75% DARI PLAFON
                component.setBackground(new Color(255, 201, 102));
            }else{
                //MELEBIHI PLAFON
                component.setBackground(new Color(255,128,128));
            }
        }
        
        return component;
    }

}