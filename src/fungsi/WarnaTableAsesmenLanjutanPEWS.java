/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fungsi;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Owner
 */
public class WarnaTableAsesmenLanjutanPEWS extends DefaultTableCellRenderer {
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (row % 2 == 1){
            component.setBackground(new Color(255,246,244));
            component.setForeground(new Color(50,50,50));
        }else{
            component.setBackground(new Color(255,255,255));
            component.setForeground(new Color(50,50,50));
        } 
        //TOTAL
        if ((column == 6)||(column == 7)){
            if(table.getValueAt(row,7).toString().equalsIgnoreCase("Blue")){
                component.setBackground(Color.BLUE);
                component.setForeground(Color.WHITE);
            }else if(Integer.parseInt(table.getValueAt(row,7).toString())>4){
                component.setBackground(Color.RED);
                component.setForeground(Color.WHITE);
            }else if((Integer.parseInt(table.getValueAt(row,7).toString())>2 && Integer.parseInt(table.getValueAt(row,7).toString())<5)){
                component.setBackground(Color.ORANGE);
                component.setForeground(Color.WHITE);
            }else if(Integer.parseInt(table.getValueAt(row,7).toString())>0 && Integer.parseInt(table.getValueAt(row,7).toString())<3){
                component.setBackground(Color.GREEN);
                component.setForeground(Color.WHITE);
            }
        }
        //semua
        if ((column >= 8) && (column <= 15)){
            if(column%2==0){
                if(Integer.parseInt(table.getValueAt(row,column+1).toString())==0){
                    component.setBackground(Color.WHITE);
                    component.setForeground(Color.BLACK);
                }else if(Integer.parseInt(table.getValueAt(row,column+1).toString())==1){
                    component.setBackground(Color.GREEN);
                    component.setForeground(Color.WHITE);
                }else if(Integer.parseInt(table.getValueAt(row,column+1).toString())==2){
                    component.setBackground(Color.ORANGE);
                    component.setForeground(Color.WHITE);
                }else if(Integer.parseInt(table.getValueAt(row,column+1).toString())==3){
                    component.setBackground(Color.RED);
                    component.setForeground(Color.WHITE);
                }                
            }else{
                if(Integer.parseInt(table.getValueAt(row,column).toString())==0){
                    component.setBackground(Color.WHITE);
                    component.setForeground(Color.BLACK);
                }else if(Integer.parseInt(table.getValueAt(row,column).toString())==1){
                    component.setBackground(Color.GREEN);
                    component.setForeground(Color.WHITE);
                }else if(Integer.parseInt(table.getValueAt(row,column).toString())==2){
                    component.setBackground(Color.ORANGE);
                    component.setForeground(Color.WHITE);
                }else if(Integer.parseInt(table.getValueAt(row,column).toString())==3){
                    component.setBackground(Color.RED);
                    component.setForeground(Color.WHITE);
                }                
            }
        }
        return component;
    }

}
