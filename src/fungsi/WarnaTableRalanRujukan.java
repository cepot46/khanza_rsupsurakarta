package fungsi;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Renderer untuk pewarnaan tabel berdasarkan status.
 */
public class WarnaTableRalanRujukan extends DefaultTableCellRenderer {
    private static final Color COLOR_DEFAULT = new Color(255, 255, 255);
    private static final Color COLOR_KONSULTASI = new Color(204, 204, 204);
    private static final Color COLOR_RUJUKAN = new Color(204, 204, 255);

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        String statusKonsul = (String) table.getModel().getValueAt(row, 18);
        String jawabanKonsul = (String) table.getModel().getValueAt(row, 19);

        switch (statusKonsul.toLowerCase()) {
            case "konsultasi":
                if ("belum dijawab".equalsIgnoreCase(jawabanKonsul.toLowerCase())) {
                    component.setBackground(COLOR_KONSULTASI);
                }else{
                    component.setBackground(COLOR_DEFAULT);
                }
                break;
            case "alih rawat":
                if ("belum dijawab".equalsIgnoreCase(jawabanKonsul.toLowerCase())) {
                    component.setBackground(COLOR_RUJUKAN);
                }else{
                    component.setBackground(COLOR_DEFAULT);
                }
                break;
            default:
                component.setBackground(COLOR_DEFAULT); // default color if no status matches
                break;
        }
        
        return component;
    }
}
