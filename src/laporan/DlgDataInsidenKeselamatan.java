/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DlgRujuk.java
 *
 * Created on 31 Mei 10, 20:19:56
 */

package laporan;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariPetugas;
import simrskhanza.DlgCariListpasien;
import simrskhanza.DlgCariListpasienRanap;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;


/**
 *
 * @author perpustakaan
 */
public final class DlgDataInsidenKeselamatan extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;
    private String finger="",finger_pj="";
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private DlgCariListpasien caripasien=new DlgCariListpasien(null,false);
    private DlgCariListpasienRanap caripasienranap=new DlgCariListpasienRanap(null,false);
    private DlgCariPetugas pj=new DlgCariPetugas(null,false);
    private DlgInsidenKeselamatan insiden=new DlgInsidenKeselamatan(null,false);
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public DlgDataInsidenKeselamatan(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(628,674);


        Object[] row={
            "No.Rawat","No.R.M.","Nama Pasien","Umur","Tgl.Kejadian","Jam Kejadian",
            "Tgl.Lapor","Jam Lapor","NIP","Petugas","Insiden","Jenis Insiden","Tempat Insiden","Kronologis","Akibat",
            "Tindakan Saat Insiden","Pelapor Pertama","Keterangan Pelapor","Terjadi Pada","Keterangan",
            "Menyangkut Pasien","Keterangan","Tempat Insiden","Keterangan","Unit / Departemen Terkait","Tindakan","Keterangan Tindakan","Pernah Terjadi","Keterangan Pencegahan","NIP Penanggung Jawab","Nama Penanggung Jawab","Status","Tanggal Verifikasi","Jam Verifikasi","Grading"
        };
        tabMode=new DefaultTableModel(null,row){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

       
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 35; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(110);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(200);
            }else if(i==3){
                column.setPreferredWidth(40);
            }else if(i==4){
                column.setPreferredWidth(70);
            }else if(i==5){
                column.setPreferredWidth(70);
            }else if(i==6){
                column.setPreferredWidth(70);
            }else if(i==7){
                column.setPreferredWidth(100);
            }else if(i==8){
                column.setPreferredWidth(120);
            }else if(i==9){
                column.setPreferredWidth(150);
            }else if(i==10){
                column.setPreferredWidth(150);
            }else if(i==11){
                column.setPreferredWidth(80);
            }else if(i==12){
                column.setPreferredWidth(140);
            }else if(i==13){
                column.setPreferredWidth(150);
            }else if(i==14){
                column.setPreferredWidth(150);
            }else if(i==15){
                column.setPreferredWidth(150);
            }else if(i==16){
                column.setPreferredWidth(150);
            }else if(i==17){
                column.setPreferredWidth(150);
            }else if(i==18){
                column.setPreferredWidth(150);
            }else if(i==19){
                column.setPreferredWidth(150);
            }else if(i==20){
                column.setPreferredWidth(250);
            }else if(i==21){
                column.setPreferredWidth(150);
            }else if(i==22){
                column.setPreferredWidth(150);
            }else if(i==23){
                column.setPreferredWidth(150);
            }else if(i==24){
                column.setPreferredWidth(150);
            }else if(i==25){
                column.setPreferredWidth(150);
            }else if(i==26){
                column.setPreferredWidth(150);
            }else if(i==27){
                column.setPreferredWidth(150);
            }else if(i==28){
                column.setPreferredWidth(80);
            }else if(i==29){
                column.setPreferredWidth(150);
            }else if(i==30){
                column.setPreferredWidth(150);
            }else if(i==31){
                column.setPreferredWidth(220);
            }else if(i==32){
                column.setPreferredWidth(220);
            }else if(i==33){
                column.setPreferredWidth(100);
            }else if(i==34){
                column.setPreferredWidth(100);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());

        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        nip.setDocument(new batasInput((byte)20).getKata(nip));
        Insiden.setDocument(new batasInput((int)60).getKata(Insiden));
        UnitTerkait.setDocument(new batasInput((int)60).getKata(UnitTerkait));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){
                    if(i==1){
                    nip.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    namapetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                    } else {
                    nip1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    namapetugas1.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());
                    }
                }  
                nip.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
       
        
        ChkInput.setSelected(false);
        isForm();
        
        jam();
        jam2();
        jam3();
        caripasien.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(caripasien.getTable().getSelectedRow()!= -1){
                    TNoRw.setText(caripasien.getTable().getValueAt(caripasien.getTable().getSelectedRow(),0).toString());
                    TNoRM.setText(caripasien.getTable().getValueAt(caripasien.getTable().getSelectedRow(),1).toString());
                    TPasien.setText(caripasien.getTable().getValueAt(caripasien.getTable().getSelectedRow(),3).toString());
            
                    
                }  
                nip.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        caripasienranap.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(caripasienranap.getTable().getSelectedRow()!= -1){
                    TNoRw.setText(caripasienranap.getTable().getValueAt(caripasienranap.getTable().getSelectedRow(),0).toString());
                    TNoRM.setText(caripasienranap.getTable().getValueAt(caripasienranap.getTable().getSelectedRow(),1).toString());
                    TPasien.setText(caripasienranap.getTable().getValueAt(caripasienranap.getTable().getSelectedRow(),3).toString());
            
                    
                }  
                nip.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnCetakFormulirIKP = new javax.swing.JMenuItem();
        MnVerifikasiIKP = new javax.swing.JMenuItem();
        ppGrafikBatangKejadianIKPPerDampak = new javax.swing.JMenuItem();
        ppGrafikPieKejadianIKPPerDampak = new javax.swing.JMenuItem();
        ppGrafikBatangKejadianIKPPerJenis = new javax.swing.JMenuItem();
        ppGrafikPieKejadianIKPPerJenis = new javax.swing.JMenuItem();
        ppGrafikBatangKejadianIKPPerTanggal = new javax.swing.JMenuItem();
        ppGrafikBatangKejadianIKPPerbulan = new javax.swing.JMenuItem();
        ppGrafikBatangKejadianIKPPerTahun = new javax.swing.JMenuItem();
        DlgSP = new javax.swing.JDialog();
        internalFrame7 = new widget.InternalFrame();
        panelBiasa6 = new widget.PanelBiasa();
        BtnSimpanVerifikasi = new widget.Button();
        BtnKeluarSP = new widget.Button();
        jLabel41 = new widget.Label();
        jLabel42 = new widget.Label();
        BtnCetakVerifikasi = new widget.Button();
        cbGradingAtasan = new widget.ComboBox();
        DTPVerifikasi = new widget.Tanggal();
        NoRawat = new widget.TextBox();
        jLabel43 = new widget.Label();
        jLabel44 = new widget.Label();
        NoRM = new widget.TextBox();
        jLabel45 = new widget.Label();
        KejadianIKP = new widget.TextBox();
        jLabel46 = new widget.Label();
        NmPasien = new widget.TextBox();
        jLabel47 = new widget.Label();
        KodeIKP = new widget.TextBox();
        jLabel48 = new widget.Label();
        TanggalKejadianIKP = new widget.Tanggal();
        jLabel49 = new widget.Label();
        Jam = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Detik = new widget.ComboBox();
        Jam1 = new widget.ComboBox();
        Menit1 = new widget.ComboBox();
        Detik1 = new widget.ComboBox();
        ChkKejadian2 = new widget.CekBox();
        jLabel50 = new widget.Label();
        NmPetugasPJ = new widget.TextBox();
        KdPetugasPJ = new widget.TextBox();
        BtnTolakVerifikasi = new widget.Button();
        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        jPanel3 = new javax.swing.JPanel();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        BtnKeluar = new widget.Button();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        PanelInput = new javax.swing.JPanel();
        ChkInput = new widget.CekBox();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        jLabel4 = new widget.Label();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        Kejadian = new widget.Tanggal();
        TNoRM = new widget.TextBox();
        jLabel16 = new widget.Label();
        Insiden = new widget.TextBox();
        jLabel23 = new widget.Label();
        JamKejadian = new widget.ComboBox();
        MenitKejadian = new widget.ComboBox();
        DetikKejadian = new widget.ComboBox();
        ChkKejadian = new widget.CekBox();
        jLabel17 = new widget.Label();
        Lapor = new widget.Tanggal();
        JamLapor = new widget.ComboBox();
        MenitLapor = new widget.ComboBox();
        DetikLapor = new widget.ComboBox();
        ChkLapor = new widget.CekBox();
        jLabel18 = new widget.Label();
        nip = new widget.TextBox();
        namapetugas = new widget.TextBox();
        btnPetugas = new widget.Button();
        jLabel24 = new widget.Label();
        UnitTerkait = new widget.TextBox();
        jLabel25 = new widget.Label();
        jLabel20 = new widget.Label();
        jLabel26 = new widget.Label();
        jLabel27 = new widget.Label();
        jLabel29 = new widget.Label();
        cbMelaporkanpertama = new widget.ComboBox();
        KetPelapor = new widget.TextBox();
        jLabel31 = new widget.Label();
        cbTerjadipada = new widget.ComboBox();
        KetTerjadipada = new widget.TextBox();
        jLabel32 = new widget.Label();
        cbMenyangkutpasien = new widget.ComboBox();
        KetMenyangkutpasien = new widget.TextBox();
        jLabel33 = new widget.Label();
        cbTempatinsiden = new widget.ComboBox();
        KetTempatinsiden = new widget.TextBox();
        jLabel34 = new widget.Label();
        UnitMenyebabkan = new widget.TextBox();
        jLabel35 = new widget.Label();
        cbTindakanYangDilakukan = new widget.ComboBox();
        KetTindakan = new widget.TextBox();
        jLabel36 = new widget.Label();
        cbPernahTerjadi = new widget.ComboBox();
        jLabel37 = new widget.Label();
        jLabel38 = new widget.Label();
        jLabel39 = new widget.Label();
        jScrollPane1 = new javax.swing.JScrollPane();
        KetPencegahan = new javax.swing.JTextArea();
        jLabel40 = new widget.Label();
        cbGrading = new widget.ComboBox();
        btnPetugas1 = new widget.Button();
        namapetugas1 = new widget.TextBox();
        nip1 = new widget.TextBox();
        jLabel22 = new widget.Label();
        jScrollPane5 = new javax.swing.JScrollPane();
        Kronologis = new javax.swing.JTextArea();
        scrollPane1 = new widget.ScrollPane();
        Tindakan = new widget.TextArea();
        akibat = new widget.ComboBox();
        cbinsiden = new widget.ComboBox();
        btnListpasien = new widget.Button();
        btnListpasienRanap = new widget.Button();
        jLabel51 = new widget.Label();
        jLabel52 = new widget.Label();

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnCetakFormulirIKP.setBackground(new java.awt.Color(255, 255, 254));
        MnCetakFormulirIKP.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakFormulirIKP.setForeground(new java.awt.Color(50, 50, 50));
        MnCetakFormulirIKP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetakFormulirIKP.setText("Formulir Insiden Keselamatan Pasien");
        MnCetakFormulirIKP.setName("MnCetakFormulirIKP"); // NOI18N
        MnCetakFormulirIKP.setPreferredSize(new java.awt.Dimension(240, 26));
        MnCetakFormulirIKP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakFormulirIKPActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakFormulirIKP);

        MnVerifikasiIKP.setBackground(new java.awt.Color(255, 255, 254));
        MnVerifikasiIKP.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnVerifikasiIKP.setForeground(new java.awt.Color(50, 50, 50));
        MnVerifikasiIKP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnVerifikasiIKP.setText("Verifikasi IKP");
        MnVerifikasiIKP.setName("MnVerifikasiIKP"); // NOI18N
        MnVerifikasiIKP.setPreferredSize(new java.awt.Dimension(240, 26));
        MnVerifikasiIKP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnVerifikasiIKPActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnVerifikasiIKP);

        ppGrafikBatangKejadianIKPPerDampak.setBackground(new java.awt.Color(255, 255, 254));
        ppGrafikBatangKejadianIKPPerDampak.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppGrafikBatangKejadianIKPPerDampak.setForeground(new java.awt.Color(50, 50, 50));
        ppGrafikBatangKejadianIKPPerDampak.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Create-Ticket24.png"))); // NOI18N
        ppGrafikBatangKejadianIKPPerDampak.setText("Grafik Batang Kejadian IKP Per Dampak");
        ppGrafikBatangKejadianIKPPerDampak.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppGrafikBatangKejadianIKPPerDampak.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppGrafikBatangKejadianIKPPerDampak.setName("ppGrafikBatangKejadianIKPPerDampak"); // NOI18N
        ppGrafikBatangKejadianIKPPerDampak.setPreferredSize(new java.awt.Dimension(280, 26));
        ppGrafikBatangKejadianIKPPerDampak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppGrafikBatangKejadianIKPPerDampakActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppGrafikBatangKejadianIKPPerDampak);

        ppGrafikPieKejadianIKPPerDampak.setBackground(new java.awt.Color(255, 255, 254));
        ppGrafikPieKejadianIKPPerDampak.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppGrafikPieKejadianIKPPerDampak.setForeground(new java.awt.Color(50, 50, 50));
        ppGrafikPieKejadianIKPPerDampak.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Create-Ticket24.png"))); // NOI18N
        ppGrafikPieKejadianIKPPerDampak.setText("Grafik Pie Kejadian IKP Per Dampak");
        ppGrafikPieKejadianIKPPerDampak.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppGrafikPieKejadianIKPPerDampak.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppGrafikPieKejadianIKPPerDampak.setName("ppGrafikPieKejadianIKPPerDampak"); // NOI18N
        ppGrafikPieKejadianIKPPerDampak.setPreferredSize(new java.awt.Dimension(280, 26));
        ppGrafikPieKejadianIKPPerDampak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppGrafikPieKejadianIKPPerDampakActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppGrafikPieKejadianIKPPerDampak);

        ppGrafikBatangKejadianIKPPerJenis.setBackground(new java.awt.Color(255, 255, 254));
        ppGrafikBatangKejadianIKPPerJenis.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppGrafikBatangKejadianIKPPerJenis.setForeground(new java.awt.Color(50, 50, 50));
        ppGrafikBatangKejadianIKPPerJenis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Create-Ticket24.png"))); // NOI18N
        ppGrafikBatangKejadianIKPPerJenis.setText("Grafik Batang Kejadian IKP Per Jenis");
        ppGrafikBatangKejadianIKPPerJenis.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppGrafikBatangKejadianIKPPerJenis.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppGrafikBatangKejadianIKPPerJenis.setName("ppGrafikBatangKejadianIKPPerJenis"); // NOI18N
        ppGrafikBatangKejadianIKPPerJenis.setPreferredSize(new java.awt.Dimension(280, 26));
        ppGrafikBatangKejadianIKPPerJenis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppGrafikBatangKejadianIKPPerJenisActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppGrafikBatangKejadianIKPPerJenis);

        ppGrafikPieKejadianIKPPerJenis.setBackground(new java.awt.Color(255, 255, 254));
        ppGrafikPieKejadianIKPPerJenis.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppGrafikPieKejadianIKPPerJenis.setForeground(new java.awt.Color(50, 50, 50));
        ppGrafikPieKejadianIKPPerJenis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Create-Ticket24.png"))); // NOI18N
        ppGrafikPieKejadianIKPPerJenis.setText("Grafik Pie Kejadian IKP Per Jenis");
        ppGrafikPieKejadianIKPPerJenis.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppGrafikPieKejadianIKPPerJenis.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppGrafikPieKejadianIKPPerJenis.setName("ppGrafikPieKejadianIKPPerJenis"); // NOI18N
        ppGrafikPieKejadianIKPPerJenis.setPreferredSize(new java.awt.Dimension(280, 26));
        ppGrafikPieKejadianIKPPerJenis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppGrafikPieKejadianIKPPerJenisActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppGrafikPieKejadianIKPPerJenis);

        ppGrafikBatangKejadianIKPPerTanggal.setBackground(new java.awt.Color(255, 255, 254));
        ppGrafikBatangKejadianIKPPerTanggal.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppGrafikBatangKejadianIKPPerTanggal.setForeground(new java.awt.Color(50, 50, 50));
        ppGrafikBatangKejadianIKPPerTanggal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Create-Ticket24.png"))); // NOI18N
        ppGrafikBatangKejadianIKPPerTanggal.setText("Grafik Batang Kejadian IKP Per Tanggal");
        ppGrafikBatangKejadianIKPPerTanggal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppGrafikBatangKejadianIKPPerTanggal.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppGrafikBatangKejadianIKPPerTanggal.setName("ppGrafikBatangKejadianIKPPerTanggal"); // NOI18N
        ppGrafikBatangKejadianIKPPerTanggal.setPreferredSize(new java.awt.Dimension(280, 26));
        ppGrafikBatangKejadianIKPPerTanggal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppGrafikBatangKejadianIKPPerTanggalActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppGrafikBatangKejadianIKPPerTanggal);

        ppGrafikBatangKejadianIKPPerbulan.setBackground(new java.awt.Color(255, 255, 254));
        ppGrafikBatangKejadianIKPPerbulan.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppGrafikBatangKejadianIKPPerbulan.setForeground(new java.awt.Color(50, 50, 50));
        ppGrafikBatangKejadianIKPPerbulan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Create-Ticket24.png"))); // NOI18N
        ppGrafikBatangKejadianIKPPerbulan.setText("Grafik Batang Kejadian IKP Per Bulan");
        ppGrafikBatangKejadianIKPPerbulan.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppGrafikBatangKejadianIKPPerbulan.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppGrafikBatangKejadianIKPPerbulan.setName("ppGrafikBatangKejadianIKPPerbulan"); // NOI18N
        ppGrafikBatangKejadianIKPPerbulan.setPreferredSize(new java.awt.Dimension(280, 26));
        ppGrafikBatangKejadianIKPPerbulan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppGrafikBatangKejadianIKPPerbulanActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppGrafikBatangKejadianIKPPerbulan);

        ppGrafikBatangKejadianIKPPerTahun.setBackground(new java.awt.Color(255, 255, 254));
        ppGrafikBatangKejadianIKPPerTahun.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppGrafikBatangKejadianIKPPerTahun.setForeground(new java.awt.Color(50, 50, 50));
        ppGrafikBatangKejadianIKPPerTahun.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Create-Ticket24.png"))); // NOI18N
        ppGrafikBatangKejadianIKPPerTahun.setText("Grafik Batang Kejadian IKP Per Tahun");
        ppGrafikBatangKejadianIKPPerTahun.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppGrafikBatangKejadianIKPPerTahun.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppGrafikBatangKejadianIKPPerTahun.setName("ppGrafikBatangKejadianIKPPerTahun"); // NOI18N
        ppGrafikBatangKejadianIKPPerTahun.setPreferredSize(new java.awt.Dimension(280, 26));
        ppGrafikBatangKejadianIKPPerTahun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppGrafikBatangKejadianIKPPerTahunActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppGrafikBatangKejadianIKPPerTahun);

        DlgSP.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        DlgSP.setName("DlgSP"); // NOI18N
        DlgSP.setUndecorated(true);
        DlgSP.setResizable(false);

        internalFrame7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(230, 235, 225)), "::[Verifikasi Insiden Keselamatan Pasien ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 70, 50))); // NOI18N
        internalFrame7.setName("internalFrame7"); // NOI18N
        internalFrame7.setLayout(new java.awt.BorderLayout(1, 1));

        panelBiasa6.setName("panelBiasa6"); // NOI18N
        panelBiasa6.setLayout(null);

        BtnSimpanVerifikasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpanVerifikasi.setMnemonic('T');
        BtnSimpanVerifikasi.setText("Simpan");
        BtnSimpanVerifikasi.setToolTipText("Alt+T");
        BtnSimpanVerifikasi.setName("BtnSimpanVerifikasi"); // NOI18N
        BtnSimpanVerifikasi.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpanVerifikasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanVerifikasiActionPerformed(evt);
            }
        });
        panelBiasa6.add(BtnSimpanVerifikasi);
        BtnSimpanVerifikasi.setBounds(20, 240, 130, 30);

        BtnKeluarSP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluarSP.setMnemonic('K');
        BtnKeluarSP.setText("Tutup");
        BtnKeluarSP.setToolTipText("Alt+K");
        BtnKeluarSP.setName("BtnKeluarSP"); // NOI18N
        BtnKeluarSP.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluarSP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarSPActionPerformed(evt);
            }
        });
        panelBiasa6.add(BtnKeluarSP);
        BtnKeluarSP.setBounds(630, 240, 100, 30);

        jLabel41.setText("Tgl. Verifikasi :");
        jLabel41.setName("jLabel41"); // NOI18N
        jLabel41.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel41);
        jLabel41.setBounds(10, 190, 70, 23);

        jLabel42.setText("Grading :");
        jLabel42.setName("jLabel42"); // NOI18N
        jLabel42.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel42);
        jLabel42.setBounds(510, 190, 60, 23);

        BtnCetakVerifikasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnCetakVerifikasi.setMnemonic('T');
        BtnCetakVerifikasi.setText("Cetak Verifikasi");
        BtnCetakVerifikasi.setToolTipText("Alt+T");
        BtnCetakVerifikasi.setName("BtnCetakVerifikasi"); // NOI18N
        BtnCetakVerifikasi.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnCetakVerifikasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCetakVerifikasiActionPerformed(evt);
            }
        });
        panelBiasa6.add(BtnCetakVerifikasi);
        BtnCetakVerifikasi.setBounds(170, 240, 140, 30);

        cbGradingAtasan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Biru", "Hijau", "Kuning", "Merah" }));
        cbGradingAtasan.setName("cbGradingAtasan"); // NOI18N
        cbGradingAtasan.setPreferredSize(new java.awt.Dimension(150, 23));
        panelBiasa6.add(cbGradingAtasan);
        cbGradingAtasan.setBounds(580, 190, 150, 23);

        DTPVerifikasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "23-07-2024" }));
        DTPVerifikasi.setDisplayFormat("dd-MM-yyyy");
        DTPVerifikasi.setName("DTPVerifikasi"); // NOI18N
        DTPVerifikasi.setOpaque(false);
        DTPVerifikasi.setPreferredSize(new java.awt.Dimension(90, 23));
        panelBiasa6.add(DTPVerifikasi);
        DTPVerifikasi.setBounds(90, 190, 110, 23);

        NoRawat.setEditable(false);
        NoRawat.setName("NoRawat"); // NOI18N
        NoRawat.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NoRawat);
        NoRawat.setBounds(100, 10, 150, 23);

        jLabel43.setText("No Rawat :");
        jLabel43.setName("jLabel43"); // NOI18N
        jLabel43.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel43);
        jLabel43.setBounds(30, 10, 60, 23);

        jLabel44.setText("Nama Pasien :");
        jLabel44.setName("jLabel44"); // NOI18N
        jLabel44.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel44);
        jLabel44.setBounds(10, 40, 80, 23);

        NoRM.setEditable(false);
        NoRM.setName("NoRM"); // NOI18N
        NoRM.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NoRM);
        NoRM.setBounds(100, 40, 150, 23);

        jLabel45.setText("Kejadian IKP :");
        jLabel45.setName("jLabel45"); // NOI18N
        jLabel45.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel45);
        jLabel45.setBounds(10, 70, 80, 23);

        KejadianIKP.setEditable(false);
        KejadianIKP.setName("KejadianIKP"); // NOI18N
        KejadianIKP.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(KejadianIKP);
        KejadianIKP.setBounds(100, 70, 390, 23);

        jLabel46.setText("Jam. Verifikasi :");
        jLabel46.setName("jLabel46"); // NOI18N
        jLabel46.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel46);
        jLabel46.setBounds(200, 190, 80, 23);

        NmPasien.setEditable(false);
        NmPasien.setName("NmPasien"); // NOI18N
        NmPasien.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NmPasien);
        NmPasien.setBounds(260, 40, 230, 23);

        jLabel47.setText("Jenis Insiden :");
        jLabel47.setName("jLabel47"); // NOI18N
        jLabel47.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel47);
        jLabel47.setBounds(260, 10, 70, 23);

        KodeIKP.setEditable(false);
        KodeIKP.setName("KodeIKP"); // NOI18N
        KodeIKP.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(KodeIKP);
        KodeIKP.setBounds(330, 10, 160, 23);

        jLabel48.setText("Tgl. Kejadian :");
        jLabel48.setName("jLabel48"); // NOI18N
        jLabel48.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel48);
        jLabel48.setBounds(530, 10, 70, 23);

        TanggalKejadianIKP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "23-07-2024" }));
        TanggalKejadianIKP.setDisplayFormat("dd-MM-yyyy");
        TanggalKejadianIKP.setEnabled(false);
        TanggalKejadianIKP.setName("TanggalKejadianIKP"); // NOI18N
        TanggalKejadianIKP.setOpaque(false);
        TanggalKejadianIKP.setPreferredSize(new java.awt.Dimension(90, 23));
        panelBiasa6.add(TanggalKejadianIKP);
        TanggalKejadianIKP.setBounds(610, 10, 120, 23);

        jLabel49.setText("Jam Kejadian :");
        jLabel49.setName("jLabel49"); // NOI18N
        jLabel49.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel49);
        jLabel49.setBounds(530, 40, 70, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setEnabled(false);
        Jam.setName("Jam"); // NOI18N
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        panelBiasa6.add(Jam);
        Jam.setBounds(530, 70, 62, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setEnabled(false);
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        panelBiasa6.add(Menit);
        Menit.setBounds(600, 70, 62, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setEnabled(false);
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        panelBiasa6.add(Detik);
        Detik.setBounds(670, 70, 62, 23);

        Jam1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam1.setName("Jam1"); // NOI18N
        Jam1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Jam1KeyPressed(evt);
            }
        });
        panelBiasa6.add(Jam1);
        Jam1.setBounds(280, 190, 62, 23);

        Menit1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit1.setName("Menit1"); // NOI18N
        Menit1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Menit1KeyPressed(evt);
            }
        });
        panelBiasa6.add(Menit1);
        Menit1.setBounds(350, 190, 62, 23);

        Detik1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik1.setName("Detik1"); // NOI18N
        Detik1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Detik1KeyPressed(evt);
            }
        });
        panelBiasa6.add(Detik1);
        Detik1.setBounds(420, 190, 62, 23);

        ChkKejadian2.setBorder(null);
        ChkKejadian2.setSelected(true);
        ChkKejadian2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkKejadian2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkKejadian2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkKejadian2.setName("ChkKejadian2"); // NOI18N
        panelBiasa6.add(ChkKejadian2);
        ChkKejadian2.setBounds(490, 190, 23, 23);

        jLabel50.setText("Penanggung Jawab Laporan:");
        jLabel50.setName("jLabel50"); // NOI18N
        jLabel50.setPreferredSize(new java.awt.Dimension(60, 23));
        panelBiasa6.add(jLabel50);
        jLabel50.setBounds(10, 100, 150, 23);

        NmPetugasPJ.setEditable(false);
        NmPetugasPJ.setName("NmPetugasPJ"); // NOI18N
        NmPetugasPJ.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(NmPetugasPJ);
        NmPetugasPJ.setBounds(170, 130, 320, 23);

        KdPetugasPJ.setEditable(false);
        KdPetugasPJ.setName("KdPetugasPJ"); // NOI18N
        KdPetugasPJ.setPreferredSize(new java.awt.Dimension(300, 23));
        panelBiasa6.add(KdPetugasPJ);
        KdPetugasPJ.setBounds(20, 130, 140, 23);

        BtnTolakVerifikasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/cross.png"))); // NOI18N
        BtnTolakVerifikasi.setMnemonic('T');
        BtnTolakVerifikasi.setText("Tolak Laporan IKP");
        BtnTolakVerifikasi.setToolTipText("Alt+T");
        BtnTolakVerifikasi.setName("BtnTolakVerifikasi"); // NOI18N
        BtnTolakVerifikasi.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnTolakVerifikasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTolakVerifikasiActionPerformed(evt);
            }
        });
        panelBiasa6.add(BtnTolakVerifikasi);
        BtnTolakVerifikasi.setBounds(330, 240, 170, 30);

        internalFrame7.add(panelBiasa6, java.awt.BorderLayout.CENTER);

        DlgSP.getContentPane().add(internalFrame7, java.awt.BorderLayout.CENTER);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Insiden Keselamatan Pasien ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setComponentPopupMenu(jPopupMenu1);
        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(44, 100));
        jPanel3.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass8.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass8.add(LCount);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        jPanel3.add(panelGlass8, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Kejadian :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(67, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "23-07-2024" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "23-07-2024" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(310, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnAll);

        jPanel3.add(panelGlass9, java.awt.BorderLayout.PAGE_START);

        internalFrame1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        PanelInput.setName("PanelInput"); // NOI18N
        PanelInput.setOpaque(false);
        PanelInput.setLayout(new java.awt.BorderLayout(1, 1));

        ChkInput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setMnemonic('I');
        ChkInput.setText(".: Input Data");
        ChkInput.setToolTipText("Alt+I");
        ChkInput.setBorderPainted(true);
        ChkInput.setBorderPaintedFlat(true);
        ChkInput.setFocusable(false);
        ChkInput.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ChkInput.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ChkInput.setName("ChkInput"); // NOI18N
        ChkInput.setPreferredSize(new java.awt.Dimension(192, 20));
        ChkInput.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkInputActionPerformed(evt);
            }
        });
        PanelInput.add(ChkInput, java.awt.BorderLayout.PAGE_END);

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(100, 225));

        FormInput.setBackground(new java.awt.Color(250, 255, 245));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(100, 1100));
        FormInput.setLayout(null);

        jLabel4.setText("No.Rawat :");
        jLabel4.setName("jLabel4"); // NOI18N
        FormInput.add(jLabel4);
        jLabel4.setBounds(0, 10, 100, 23);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(104, 10, 141, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        TPasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TPasienKeyPressed(evt);
            }
        });
        FormInput.add(TPasien);
        TPasien.setBounds(361, 10, 360, 23);

        Kejadian.setForeground(new java.awt.Color(50, 70, 50));
        Kejadian.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "23-07-2024" }));
        Kejadian.setDisplayFormat("dd-MM-yyyy");
        Kejadian.setName("Kejadian"); // NOI18N
        Kejadian.setOpaque(false);
        Kejadian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KejadianKeyPressed(evt);
            }
        });
        FormInput.add(Kejadian);
        Kejadian.setBounds(104, 40, 90, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        TNoRM.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRMKeyPressed(evt);
            }
        });
        FormInput.add(TNoRM);
        TNoRM.setBounds(247, 10, 112, 23);

        jLabel16.setText("Kejadian :");
        jLabel16.setName("jLabel16"); // NOI18N
        jLabel16.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel16);
        jLabel16.setBounds(0, 40, 100, 23);

        Insiden.setHighlighter(null);
        Insiden.setName("Insiden"); // NOI18N
        Insiden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                InsidenKeyPressed(evt);
            }
        });
        FormInput.add(Insiden);
        Insiden.setBounds(100, 100, 280, 23);

        jLabel23.setText("Insiden :");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(30, 100, 60, 23);

        JamKejadian.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        JamKejadian.setName("JamKejadian"); // NOI18N
        JamKejadian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKejadianKeyPressed(evt);
            }
        });
        FormInput.add(JamKejadian);
        JamKejadian.setBounds(197, 40, 62, 23);

        MenitKejadian.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        MenitKejadian.setName("MenitKejadian"); // NOI18N
        MenitKejadian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKejadianKeyPressed(evt);
            }
        });
        FormInput.add(MenitKejadian);
        MenitKejadian.setBounds(262, 40, 62, 23);

        DetikKejadian.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        DetikKejadian.setName("DetikKejadian"); // NOI18N
        DetikKejadian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKejadianKeyPressed(evt);
            }
        });
        FormInput.add(DetikKejadian);
        DetikKejadian.setBounds(327, 40, 62, 23);

        ChkKejadian.setBorder(null);
        ChkKejadian.setSelected(true);
        ChkKejadian.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkKejadian.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkKejadian.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkKejadian.setName("ChkKejadian"); // NOI18N
        FormInput.add(ChkKejadian);
        ChkKejadian.setBounds(392, 40, 23, 23);

        jLabel17.setText("Lapor :");
        jLabel17.setName("jLabel17"); // NOI18N
        jLabel17.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel17);
        jLabel17.setBounds(420, 40, 50, 23);

        Lapor.setForeground(new java.awt.Color(50, 70, 50));
        Lapor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "23-07-2024" }));
        Lapor.setDisplayFormat("dd-MM-yyyy");
        Lapor.setName("Lapor"); // NOI18N
        Lapor.setOpaque(false);
        Lapor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                LaporKeyPressed(evt);
            }
        });
        FormInput.add(Lapor);
        Lapor.setBounds(474, 40, 90, 23);

        JamLapor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        JamLapor.setName("JamLapor"); // NOI18N
        JamLapor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamLaporKeyPressed(evt);
            }
        });
        FormInput.add(JamLapor);
        JamLapor.setBounds(567, 40, 62, 23);

        MenitLapor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        MenitLapor.setName("MenitLapor"); // NOI18N
        MenitLapor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitLaporKeyPressed(evt);
            }
        });
        FormInput.add(MenitLapor);
        MenitLapor.setBounds(632, 40, 62, 23);

        DetikLapor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        DetikLapor.setName("DetikLapor"); // NOI18N
        DetikLapor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikLaporKeyPressed(evt);
            }
        });
        FormInput.add(DetikLapor);
        DetikLapor.setBounds(697, 40, 62, 23);

        ChkLapor.setBorder(null);
        ChkLapor.setSelected(true);
        ChkLapor.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkLapor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkLapor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkLapor.setName("ChkLapor"); // NOI18N
        FormInput.add(ChkLapor);
        ChkLapor.setBounds(762, 40, 23, 23);

        jLabel18.setText("Penanggung Jawab :");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(400, 70, 110, 23);

        nip.setEditable(false);
        nip.setHighlighter(null);
        nip.setName("nip"); // NOI18N
        nip.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nipKeyPressed(evt);
            }
        });
        FormInput.add(nip);
        nip.setBounds(104, 70, 70, 23);

        namapetugas.setEditable(false);
        namapetugas.setName("namapetugas"); // NOI18N
        FormInput.add(namapetugas);
        namapetugas.setBounds(176, 70, 184, 23);

        btnPetugas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnPetugas.setMnemonic('2');
        btnPetugas.setToolTipText("ALt+2");
        btnPetugas.setName("btnPetugas"); // NOI18N
        btnPetugas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPetugasActionPerformed(evt);
            }
        });
        FormInput.add(btnPetugas);
        btnPetugas.setBounds(362, 70, 28, 23);

        jLabel24.setText("Tempat Insiden :");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(0, 430, 100, 23);

        UnitTerkait.setHighlighter(null);
        UnitTerkait.setName("UnitTerkait"); // NOI18N
        UnitTerkait.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UnitTerkaitKeyPressed(evt);
            }
        });
        FormInput.add(UnitTerkait);
        UnitTerkait.setBounds(110, 430, 280, 23);

        jLabel25.setText("Akibat Insiden Terhadap Pasien :");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(-30, 600, 220, 23);

        jLabel20.setText("Jenis Insiden :");
        jLabel20.setName("jLabel20"); // NOI18N
        FormInput.add(jLabel20);
        jLabel20.setBounds(-10, 210, 100, 23);

        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel26.setText("Orang Yang Pertama Kali melaporkan Insiden :");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(20, 240, 230, 23);

        jLabel27.setText("Tindakan Dilakukan :");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(0, 630, 120, 23);

        jLabel29.setText("Kronologis :");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(0, 160, 91, 23);

        cbMelaporkanpertama.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Dokter", "Perawat", "Petugas lainnya", "Pasien", "Keluarga / Pendamping  Pasien", "Pengunjung", "Lain-lain" }));
        cbMelaporkanpertama.setName("cbMelaporkanpertama"); // NOI18N
        cbMelaporkanpertama.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbMelaporkanpertamaItemStateChanged(evt);
            }
        });
        cbMelaporkanpertama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMelaporkanpertamaActionPerformed(evt);
            }
        });
        cbMelaporkanpertama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbMelaporkanpertamaKeyPressed(evt);
            }
        });
        FormInput.add(cbMelaporkanpertama);
        cbMelaporkanpertama.setBounds(20, 270, 170, 23);

        KetPelapor.setHighlighter(null);
        KetPelapor.setName("KetPelapor"); // NOI18N
        KetPelapor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetPelaporKeyPressed(evt);
            }
        });
        FormInput.add(KetPelapor);
        KetPelapor.setBounds(200, 270, 190, 23);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("Insiden terjadi pada :");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(20, 300, 230, 23);

        cbTerjadipada.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pasien", "Lain-lain" }));
        cbTerjadipada.setName("cbTerjadipada"); // NOI18N
        cbTerjadipada.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbTerjadipadaItemStateChanged(evt);
            }
        });
        cbTerjadipada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbTerjadipadaActionPerformed(evt);
            }
        });
        cbTerjadipada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbTerjadipadaKeyPressed(evt);
            }
        });
        FormInput.add(cbTerjadipada);
        cbTerjadipada.setBounds(20, 330, 170, 23);

        KetTerjadipada.setHighlighter(null);
        KetTerjadipada.setName("KetTerjadipada"); // NOI18N
        KetTerjadipada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetTerjadipadaKeyPressed(evt);
            }
        });
        FormInput.add(KetTerjadipada);
        KetTerjadipada.setBounds(200, 330, 190, 23);

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel32.setText("Insiden menyangkut pasien :");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(20, 360, 230, 23);

        cbMenyangkutpasien.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pasien Rawat Inap", "Pasien Rawat Jalan", "Pasien UGD", "Lain-lain" }));
        cbMenyangkutpasien.setName("cbMenyangkutpasien"); // NOI18N
        cbMenyangkutpasien.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbMenyangkutpasienItemStateChanged(evt);
            }
        });
        cbMenyangkutpasien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMenyangkutpasienActionPerformed(evt);
            }
        });
        cbMenyangkutpasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbMenyangkutpasienKeyPressed(evt);
            }
        });
        FormInput.add(cbMenyangkutpasien);
        cbMenyangkutpasien.setBounds(20, 390, 170, 23);

        KetMenyangkutpasien.setHighlighter(null);
        KetMenyangkutpasien.setName("KetMenyangkutpasien"); // NOI18N
        KetMenyangkutpasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetMenyangkutpasienKeyPressed(evt);
            }
        });
        FormInput.add(KetMenyangkutpasien);
        KetMenyangkutpasien.setBounds(200, 390, 190, 23);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel33.setText("Insiden Terjadi Pada Pasien ( Sesuai Kasus Penyakit / Spesialis )");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(20, 460, 370, 23);

        cbTempatinsiden.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Penyakit Dalam dan Subspesialisnya", "Anak dan Subspesialisnya", "Paru dan Subspesialisnya", "Bedah dan Subspesialisnya", "ObstetriGynecology dan Subspesialisnya", "THT dan Subspesialisnya", "Mata dan Subspesialisnya", "Saraf dan Subspesialisnya", "Anastesi dan Subspesialisnya", "Jantung dan Subspesialisnya", "Jiwa dan Subspesialisnya", "Lain-lain" }));
        cbTempatinsiden.setName("cbTempatinsiden"); // NOI18N
        cbTempatinsiden.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbTempatinsidenItemStateChanged(evt);
            }
        });
        cbTempatinsiden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbTempatinsidenActionPerformed(evt);
            }
        });
        cbTempatinsiden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbTempatinsidenKeyPressed(evt);
            }
        });
        FormInput.add(cbTempatinsiden);
        cbTempatinsiden.setBounds(20, 490, 170, 23);

        KetTempatinsiden.setHighlighter(null);
        KetTempatinsiden.setName("KetTempatinsiden"); // NOI18N
        KetTempatinsiden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetTempatinsidenKeyPressed(evt);
            }
        });
        FormInput.add(KetTempatinsiden);
        KetTempatinsiden.setBounds(200, 490, 190, 23);

        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel34.setText("Unit / Departemen terkait yang menyebabkan insiden :");
        jLabel34.setName("jLabel34"); // NOI18N
        FormInput.add(jLabel34);
        jLabel34.setBounds(20, 530, 320, 23);

        UnitMenyebabkan.setHighlighter(null);
        UnitMenyebabkan.setName("UnitMenyebabkan"); // NOI18N
        UnitMenyebabkan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UnitMenyebabkanKeyPressed(evt);
            }
        });
        FormInput.add(UnitMenyebabkan);
        UnitMenyebabkan.setBounds(20, 560, 370, 23);

        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel35.setText("Cari Px Ralan");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(860, 10, 70, 23);

        cbTindakanYangDilakukan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TIM", "Dokter", "Perawat", "Petugas lainnya" }));
        cbTindakanYangDilakukan.setName("cbTindakanYangDilakukan"); // NOI18N
        cbTindakanYangDilakukan.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbTindakanYangDilakukanItemStateChanged(evt);
            }
        });
        cbTindakanYangDilakukan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbTindakanYangDilakukanActionPerformed(evt);
            }
        });
        cbTindakanYangDilakukan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbTindakanYangDilakukanKeyPressed(evt);
            }
        });
        FormInput.add(cbTindakanYangDilakukan);
        cbTindakanYangDilakukan.setBounds(20, 730, 160, 23);

        KetTindakan.setHighlighter(null);
        KetTindakan.setName("KetTindakan"); // NOI18N
        KetTindakan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KetTindakanKeyPressed(evt);
            }
        });
        FormInput.add(KetTindakan);
        KetTindakan.setBounds(190, 730, 200, 23);

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel36.setText("untuk mencegah terulangnya kejadian yang sama ?");
        jLabel36.setName("jLabel36"); // NOI18N
        FormInput.add(jLabel36);
        jLabel36.setBounds(20, 820, 370, 23);

        cbPernahTerjadi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak", "Ya" }));
        cbPernahTerjadi.setName("cbPernahTerjadi"); // NOI18N
        cbPernahTerjadi.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbPernahTerjadiItemStateChanged(evt);
            }
        });
        cbPernahTerjadi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbPernahTerjadiActionPerformed(evt);
            }
        });
        cbPernahTerjadi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbPernahTerjadiKeyPressed(evt);
            }
        });
        FormInput.add(cbPernahTerjadi);
        cbPernahTerjadi.setBounds(320, 760, 70, 23);

        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel37.setText("Apakah kerjadian yang sama pernah terjadi di Unit Kerja lain ?");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(20, 760, 300, 23);

        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel38.setText("Apabila Ya");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(20, 780, 50, 23);

        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel39.setText("Kapan ? dan Langkah / Tindakan apa yang telah diambil pada Unit tersebut");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(20, 800, 370, 23);

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setName("jScrollPane1"); // NOI18N

        KetPencegahan.setColumns(20);
        KetPencegahan.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        KetPencegahan.setLineWrap(true);
        KetPencegahan.setRows(5);
        KetPencegahan.setName("KetPencegahan"); // NOI18N
        jScrollPane1.setViewportView(KetPencegahan);

        FormInput.add(jScrollPane1);
        jScrollPane1.setBounds(20, 850, 370, 80);

        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel40.setText("Tindakan dilakukan oleh :");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(20, 700, 230, 23);

        cbGrading.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Biru", "Hijau", "Kuning", "Merah" }));
        cbGrading.setEnabled(false);
        cbGrading.setName("cbGrading"); // NOI18N
        cbGrading.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbGradingItemStateChanged(evt);
            }
        });
        cbGrading.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbGradingActionPerformed(evt);
            }
        });
        cbGrading.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbGradingKeyPressed(evt);
            }
        });
        FormInput.add(cbGrading);
        cbGrading.setBounds(410, 130, 130, 23);

        btnPetugas1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnPetugas1.setMnemonic('2');
        btnPetugas1.setToolTipText("ALt+2");
        btnPetugas1.setName("btnPetugas1"); // NOI18N
        btnPetugas1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPetugas1ActionPerformed(evt);
            }
        });
        FormInput.add(btnPetugas1);
        btnPetugas1.setBounds(760, 70, 28, 23);

        namapetugas1.setEditable(false);
        namapetugas1.setName("namapetugas1"); // NOI18N
        FormInput.add(namapetugas1);
        namapetugas1.setBounds(590, 70, 170, 23);

        nip1.setEditable(false);
        nip1.setHighlighter(null);
        nip1.setName("nip1"); // NOI18N
        nip1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nip1KeyPressed(evt);
            }
        });
        FormInput.add(nip1);
        nip1.setBounds(520, 70, 70, 23);

        jLabel22.setText("Petugas / Pelapor :");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(0, 70, 100, 23);

        jScrollPane5.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane5.setName("jScrollPane5"); // NOI18N

        Kronologis.setColumns(20);
        Kronologis.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        Kronologis.setLineWrap(true);
        Kronologis.setRows(5);
        Kronologis.setName("Kronologis"); // NOI18N
        jScrollPane5.setViewportView(Kronologis);

        FormInput.add(jScrollPane5);
        jScrollPane5.setBounds(100, 130, 290, 70);

        scrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane1.setName("scrollPane1"); // NOI18N

        Tindakan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Tindakan.setColumns(20);
        Tindakan.setRows(5);
        Tindakan.setName("Tindakan"); // NOI18N
        Tindakan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TindakanKeyPressed(evt);
            }
        });
        scrollPane1.setViewportView(Tindakan);

        FormInput.add(scrollPane1);
        scrollPane1.setBounds(120, 630, 270, 70);

        akibat.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Kematian", "Cedera Irreversibel/Cedera Berat", "Cedera Reversibel/Cedera Sedang", "Cedera Ringan", "Tidak ada cedera" }));
        akibat.setName("akibat"); // NOI18N
        akibat.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                akibatItemStateChanged(evt);
            }
        });
        akibat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                akibatActionPerformed(evt);
            }
        });
        akibat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                akibatKeyPressed(evt);
            }
        });
        FormInput.add(akibat);
        akibat.setBounds(190, 600, 200, 23);

        cbinsiden.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Kejadian Potensi Cidera (KPC)", "Kejadian Nyaris Cedera/KNC (Near miss)", "Kejadian Tidak Cedera/KTC (No Harm)", "Kejadian Tidak Diharapkan/KTD (Adverse Event)", "Kejadian Sentinel (Sentinel Event)" }));
        cbinsiden.setName("cbinsiden"); // NOI18N
        cbinsiden.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbinsidenItemStateChanged(evt);
            }
        });
        cbinsiden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbinsidenActionPerformed(evt);
            }
        });
        cbinsiden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbinsidenKeyPressed(evt);
            }
        });
        FormInput.add(cbinsiden);
        cbinsiden.setBounds(100, 210, 290, 23);

        btnListpasien.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnListpasien.setMnemonic('2');
        btnListpasien.setToolTipText("ALt+2");
        btnListpasien.setName("btnListpasien"); // NOI18N
        btnListpasien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListpasienActionPerformed(evt);
            }
        });
        FormInput.add(btnListpasien);
        btnListpasien.setBounds(830, 10, 28, 23);

        btnListpasienRanap.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnListpasienRanap.setMnemonic('2');
        btnListpasienRanap.setToolTipText("ALt+2");
        btnListpasienRanap.setName("btnListpasienRanap"); // NOI18N
        btnListpasienRanap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListpasienRanapActionPerformed(evt);
            }
        });
        FormInput.add(btnListpasienRanap);
        btnListpasienRanap.setBounds(730, 10, 28, 23);

        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel51.setText("Grading Resiko Kejadian ( Diisi Oleh Atasan Pelapor ) :");
        jLabel51.setName("jLabel51"); // NOI18N
        FormInput.add(jLabel51);
        jLabel51.setBounds(410, 100, 370, 23);

        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel52.setText("Cari Px Ranap");
        jLabel52.setName("jLabel52"); // NOI18N
        FormInput.add(jLabel52);
        jLabel52.setBounds(760, 10, 70, 23);

        scrollInput.setViewportView(FormInput);

        PanelInput.add(scrollInput, java.awt.BorderLayout.CENTER);

        internalFrame1.add(PanelInput, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
            isPsien();
        }else{            
            Valid.pindah(evt,TCari,Kejadian);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void TPasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TPasienKeyPressed
        Valid.pindah(evt,TCari,BtnSimpan);
}//GEN-LAST:event_TPasienKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(nip.getText().trim().equals("")||namapetugas.getText().trim().equals("")){
            Valid.textKosong(nip,"Petugas");
        }else if(nip1.getText().trim().equals("")||namapetugas.getText().trim().equals("")){
            Valid.textKosong(nip1,"Penanggung Jawab");
        }else if(cbinsiden.getSelectedItem().equals("-")){
            Valid.textKosong(cbinsiden,"Insiden");
        }else if(Insiden.getText().trim().equals("")){
            Valid.textKosong(Insiden,"Insiden");
        }else if(UnitTerkait.getText().trim().equals("")){
            Valid.textKosong(UnitTerkait,"Tempat Insiden");
        }else if(akibat.getSelectedItem().toString().equals("-")){
            Valid.textKosong(akibat,"Akibat");
        }else if(Tindakan.getText().trim().equals("")){
            Valid.textKosong(Tindakan,"Tindakan saat insiden");
        }else if(Kronologis.getText().trim().equals("")){
            Valid.textKosong(Kronologis,"Kronologis");
        }else{
            if(Sequel.menyimpantf("insiden_keselamatan_pasien","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Data",30,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(Kejadian.getSelectedItem()+""),
                JamKejadian.getSelectedItem()+":"+MenitKejadian.getSelectedItem()+":"+DetikKejadian.getSelectedItem(),
                Valid.SetTgl(Lapor.getSelectedItem()+""),
                JamLapor.getSelectedItem()+":"+MenitLapor.getSelectedItem()+":"+DetikLapor.getSelectedItem(),
                cbinsiden.getSelectedItem().toString(),
                nip.getText(),
                Insiden.getText(),
                Kronologis.getText(),
                UnitTerkait.getText(),
                akibat.getSelectedItem().toString(),
                Tindakan.getText(),
                cbMelaporkanpertama.getSelectedItem().toString(),
                KetPelapor.getText(),
                cbTerjadipada.getSelectedItem().toString(),
                KetTerjadipada.getText(),
                cbTempatinsiden.getSelectedItem().toString(),
                KetMenyangkutpasien.getText(),
                cbMenyangkutpasien.getSelectedItem().toString(),
                KetTempatinsiden.getText(),
                UnitMenyebabkan.getText(),
                cbTindakanYangDilakukan.getSelectedItem().toString(),
                KetTindakan.getText(),
                cbPernahTerjadi.getSelectedItem().toString(),
                KetPencegahan.getText(),
                "-",
                nip1.getText(),
                "Menunggu Verifikasi",
                "0000-00-00",
                "00:00:00"
            })==true){
                tampil();
                emptTeks();
                JOptionPane.showMessageDialog(null,"Berhasil Menyimpan , Silahkan login menggunakan akun pelapor untuk melihat data inputan Insiden Keselamatan");
            }   
            
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
           
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
        ChkInput.setSelected(true);
        isForm(); 
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()!= -1){
            // START CUSTOM CEK HAK AKSES ADMIN INSIDEN
            if((Sequel.cariInteger("select count(nip) from set_admin_insiden where nip='"+akses.getkode()+"'")>0)||akses.getkode().equals("Admin Utama")){
                if(Sequel.queryu2tf("delete from insiden_keselamatan_pasien where tgl_kejadian=? and jam_kejadian=? and no_rawat=?",3,new String[]{
                    tbObat.getValueAt(tbObat.getSelectedRow(),4).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),5).toString(),
                    tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
                })==true){
                    tampil();
                    emptTeks();
                }else{
                    JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                }
            }else {
                if(tbObat.getValueAt(tbObat.getSelectedRow(),31).toString().equals("Diverifikasi")){
                    JOptionPane.showMessageDialog(rootPane,"Data IKP sudah terverifikasi, silahkan hubungi administrator");
                }else{
                    if(Sequel.queryu2tf("delete from insiden_keselamatan_pasien where tgl_kejadian=? and jam_kejadian=? and no_rawat=?",3,new String[]{
                        tbObat.getValueAt(tbObat.getSelectedRow(),4).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),5).toString(),
                        tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
                    })==true){
                        tampil();
                        emptTeks();
                    }else{
                        JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
                    }
                }
            }
            
            
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(nip.getText().trim().equals("")||namapetugas.getText().trim().equals("")){
            Valid.textKosong(nip,"Petugas");
        }else if(cbinsiden.getSelectedItem().equals("-")){
            Valid.textKosong(cbinsiden,"Insiden");
        }else if(Insiden.getText().trim().equals("")){
            Valid.textKosong(Insiden,"Insiden");
        }else if(UnitTerkait.getText().trim().equals("")){
            Valid.textKosong(UnitTerkait,"Tempat Insiden");
        }else if(akibat.getSelectedItem().toString().equals("-")){
            Valid.textKosong(akibat,"Akibat");
        }else if(Tindakan.getText().trim().equals("")){
            Valid.textKosong(Tindakan,"Tindakan saat insiden");
        }else if(Kronologis.getText().trim().equals("")){
            Valid.textKosong(Kronologis,"Kronologis");
        }else {
            //SEMUA BISA MENGEDIT
            Sequel.mengedit("insiden_keselamatan_pasien", "tgl_kejadian=? and jam_kejadian=? and no_rawat=?", "no_rawat=?,tgl_kejadian=?,jam_kejadian=?,tgl_lapor=?,jam_lapor=?, "+
                        "jenis_insiden=?,nip=?,insiden=?,unit_terkait=?,akibat=?,tindakan_insiden=?,kronologis=?, "+
                        "pelapor_pertama=?,ket_pelapor=?,terjadi_pada=?,ket_terjadipada=?,menyangkut_pasien=?,ket_pasien=?,tempat_insiden=?,ket_tempat=?, "+
                        "yang_menyebabkan_insiden=?,tindakan_oleh=?,ket_tindakan=?,pernah_terjadi=?,ket_pencegahan=?,nip_pj=?",29,new String[]{
            TNoRw.getText(),
            Valid.SetTgl(Kejadian.getSelectedItem()+""),
            JamKejadian.getSelectedItem()+":"+MenitKejadian.getSelectedItem()+":"+DetikKejadian.getSelectedItem(),
            Valid.SetTgl(Lapor.getSelectedItem()+""),
            JamLapor.getSelectedItem()+":"+MenitLapor.getSelectedItem()+":"+DetikLapor.getSelectedItem(),
            cbinsiden.getSelectedItem().toString(),
            nip.getText(),
            Insiden.getText(),
            UnitTerkait.getText(),
            akibat.getSelectedItem().toString(),
            Tindakan.getText(),
            Kronologis.getText(),
            cbMelaporkanpertama.getSelectedItem().toString(),
            KetPelapor.getText(),
            cbTerjadipada.getSelectedItem().toString(),
            KetTerjadipada.getText(),
            cbMenyangkutpasien.getSelectedItem().toString(),
            KetMenyangkutpasien.getText(),
            cbTempatinsiden.getSelectedItem().toString(),
            KetTempatinsiden.getText(),
            UnitMenyebabkan.getText(),
            cbTindakanYangDilakukan.getSelectedItem().toString(),
            KetTindakan.getText(),
            cbPernahTerjadi.getSelectedItem().toString(),
            KetPencegahan.getText(),
            nip1.getText(),
            tbObat.getValueAt(tbObat.getSelectedRow(),4).toString(), // tgl
            tbObat.getValueAt(tbObat.getSelectedRow(),5).toString(), // jam
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString() // no rawat
            });
            if(tabMode.getRowCount()!=0){tampil();}
            emptTeks();
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        petugas.dispose();
        insiden.dispose();
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if(! TCari.getText().trim().equals("")){
            BtnCariActionPerformed(evt);
        }
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
            BtnBatal.requestFocus();
        }else if(tabMode.getRowCount()!=0){
            Map<String, Object> param = new HashMap<>(); 
                param.put("namars",akses.getnamars());
                param.put("alamatrs",akses.getalamatrs());
                param.put("kotars",akses.getkabupatenrs());
                param.put("propinsirs",akses.getpropinsirs());
                param.put("kontakrs",akses.getkontakrs());
                param.put("emailrs",akses.getemailrs());   
                param.put("tanggal1",Valid.SetTgl(DTPCari1.getSelectedItem()+""));   
                param.put("tanggal2",Valid.SetTgl(DTPCari2.getSelectedItem()+""));   
                param.put("parameter","%"+TCari.getText().trim()+"%");   
                param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan")); 
                param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan")); 
                param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan")); 
                Valid.MyReport("rptDataInsidenKeselamatanPasien.jasper",param,"::[ Data Insiden Keselamatan Pasien ]::");
        }
        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            tampil();
            TCari.setText("");
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void KejadianKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KejadianKeyPressed
        Valid.pindah(evt,TCari,JamKejadian);
}//GEN-LAST:event_KejadianKeyPressed

    private void TNoRMKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRMKeyPressed
        // Valid.pindah(evt, TNm, BtnSimpan);
}//GEN-LAST:event_TNoRMKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void ChkInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkInputActionPerformed
        isForm();
    }//GEN-LAST:event_ChkInputActionPerformed

    private void InsidenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_InsidenKeyPressed
        Valid.pindah(evt,nip,cbinsiden);
    }//GEN-LAST:event_InsidenKeyPressed

    private void JamKejadianKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKejadianKeyPressed
        Valid.pindah(evt,Kejadian,MenitKejadian);
    }//GEN-LAST:event_JamKejadianKeyPressed

    private void MenitKejadianKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKejadianKeyPressed
        Valid.pindah(evt,JamKejadian,DetikKejadian);
    }//GEN-LAST:event_MenitKejadianKeyPressed

    private void DetikKejadianKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKejadianKeyPressed
        Valid.pindah(evt,MenitKejadian,Lapor);
    }//GEN-LAST:event_DetikKejadianKeyPressed

    private void LaporKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LaporKeyPressed
        Valid.pindah(evt,DetikKejadian,JamLapor);
    }//GEN-LAST:event_LaporKeyPressed

    private void JamLaporKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamLaporKeyPressed
        Valid.pindah(evt,Lapor,MenitLapor);
    }//GEN-LAST:event_JamLaporKeyPressed

    private void MenitLaporKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitLaporKeyPressed
        Valid.pindah(evt,JamLapor,DetikLapor);
    }//GEN-LAST:event_MenitLaporKeyPressed

    private void DetikLaporKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikLaporKeyPressed
        Valid.pindah(evt,MenitLapor,nip);
    }//GEN-LAST:event_DetikLaporKeyPressed

    private void nipKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nipKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            Sequel.cariIsi("select nama from petugas where nip=?",namapetugas,nip.getText());
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            DetikLapor.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            Insiden.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
            btnPetugasActionPerformed(null);
        }
    }//GEN-LAST:event_nipKeyPressed

    private void btnPetugasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPetugasActionPerformed
        i=1;
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_btnPetugasActionPerformed

    private void UnitTerkaitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UnitTerkaitKeyPressed
        Valid.pindah(evt,cbinsiden,Kronologis);
    }//GEN-LAST:event_UnitTerkaitKeyPressed

    private void ppGrafikBatangKejadianIKPPerbulanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppGrafikBatangKejadianIKPPerbulanActionPerformed
        DefaultCategoryDataset dcd = new DefaultCategoryDataset();
        try {                
            rs = koneksi.prepareStatement("select DATE_FORMAT(insiden_keselamatan_pasien.tgl_kejadian, '%y-%m'),count(DATE_FORMAT(insiden_keselamatan_pasien.tgl_kejadian, '%y-%m')) as jumlah "+
                "from insiden_keselamatan_pasien where tgl_kejadian between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' group by DATE_FORMAT(insiden_keselamatan_pasien.tgl_kejadian, '%y-%m')").executeQuery();
            while(rs.next()) {
                dcd.setValue(rs.getDouble(2),rs.getString(1)+"("+rs.getString(2)+")",rs.getString(1));
            }
            
            if(rs!=null){
                rs.close();
            }
        } catch (Exception e) {
            System.out.println("Notifikasi : " + e);
        }
        JFreeChart freeChart = ChartFactory.createBarChart("Grafik Kejadian IKP Per Bulan Tanggal "+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" S.D. "+Valid.SetTgl(DTPCari2.getSelectedItem()+""),"Bulan","Jumlah Pasien", dcd, PlotOrientation.VERTICAL,true, true,true); 
        ChartFrame cf = new ChartFrame("Grafik Kejadian IKP Per Bulan",freeChart);
        cf.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);   
        cf.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        cf.setLocationRelativeTo(internalFrame1);
        cf.setAlwaysOnTop(true);
        cf.setIconImage(new ImageIcon(super.getClass().getResource("/picture/addressbook-edit24.png")).getImage());
        cf.setVisible(true);  

    }//GEN-LAST:event_ppGrafikBatangKejadianIKPPerbulanActionPerformed

    private void ppGrafikBatangKejadianIKPPerDampakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppGrafikBatangKejadianIKPPerDampakActionPerformed
        DefaultCategoryDataset dcd = new DefaultCategoryDataset();
        try {                
            rs = koneksi.prepareStatement("select insiden_keselamatan.dampak,count(insiden_keselamatan.dampak) as jumlah "+
                   "from insiden_keselamatan_pasien inner join insiden_keselamatan on insiden_keselamatan_pasien.kode_insiden=insiden_keselamatan.kode_insiden "+
                   "where tgl_kejadian between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' group by insiden_keselamatan.dampak").executeQuery();
            while(rs.next()) {
                dcd.setValue(rs.getDouble(2),rs.getString(1)+"("+rs.getString(2)+")",rs.getString(1));
            }
        } catch (Exception e) {
            System.out.println("Notifikasi : " + e);
        }
        
       JFreeChart freeChart = ChartFactory.createBarChart("Grafik Kejadian IKP Per Dampak Periode "+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" S.D. "+Valid.SetTgl(DTPCari2.getSelectedItem()+""),"Dampak IKP","Jumlah", dcd, PlotOrientation.VERTICAL,true, true,true); 
        ChartFrame cf = new ChartFrame("Grafik Kejadian IKP Per Dampak",freeChart);
        cf.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);   
        cf.setLocationRelativeTo(internalFrame1);
        cf.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        cf.setAlwaysOnTop(true);
        cf.setIconImage(new ImageIcon(super.getClass().getResource("/picture/addressbook-edit24.png")).getImage());
        cf.setVisible(true);  
    }//GEN-LAST:event_ppGrafikBatangKejadianIKPPerDampakActionPerformed

    private void ppGrafikPieKejadianIKPPerDampakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppGrafikPieKejadianIKPPerDampakActionPerformed
        DefaultPieDataset dpd = new DefaultPieDataset();
        try {                
            rs = koneksi.prepareStatement("select insiden_keselamatan.dampak,count(insiden_keselamatan.dampak) as jumlah "+
                   "from insiden_keselamatan_pasien inner join insiden_keselamatan on insiden_keselamatan_pasien.kode_insiden=insiden_keselamatan.kode_insiden "+
                   "where tgl_kejadian between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' group by insiden_keselamatan.dampak").executeQuery();
            while(rs.next()) {
                dpd.setValue(rs.getString(1)+"("+rs.getString(2)+")",rs.getDouble(2));
            }
        } catch (Exception e) {
            System.out.println("Notifikasi : " + e);
        }
        
        JFreeChart freeChart = ChartFactory.createPieChart("Grafik Kejadian IKP Per Dampak Periode "+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" S.D. "+Valid.SetTgl(DTPCari2.getSelectedItem()+""),dpd,true,true, false);
        ChartFrame cf = new ChartFrame("Grafik Kejadian IKP Per Dampak",freeChart);
        cf.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);    
        cf.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        cf.setLocationRelativeTo(internalFrame1);
        cf.setAlwaysOnTop(true);
        cf.setIconImage(new ImageIcon(super.getClass().getResource("/picture/addressbook-edit24.png")).getImage());
        cf.setVisible(true);
    }//GEN-LAST:event_ppGrafikPieKejadianIKPPerDampakActionPerformed

    private void ppGrafikBatangKejadianIKPPerJenisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppGrafikBatangKejadianIKPPerJenisActionPerformed
        DefaultCategoryDataset dcd = new DefaultCategoryDataset();
        try {                
            rs = koneksi.prepareStatement("select insiden_keselamatan.jenis_insiden,count(insiden_keselamatan.jenis_insiden) as jumlah "+
                   "from insiden_keselamatan_pasien inner join insiden_keselamatan on insiden_keselamatan_pasien.kode_insiden=insiden_keselamatan.kode_insiden "+
                   "where tgl_kejadian between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' group by insiden_keselamatan.jenis_insiden").executeQuery();
            while(rs.next()) {
                dcd.setValue(rs.getDouble(2),rs.getString(1)+"("+rs.getString(2)+")",rs.getString(1));
            }
        } catch (Exception e) {
            System.out.println("Notifikasi : " + e);
        }
        
       JFreeChart freeChart = ChartFactory.createBarChart("Grafik Kejadian IKP Per Jenis Periode "+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" S.D. "+Valid.SetTgl(DTPCari2.getSelectedItem()+""),"Jenis IKP","Jumlah", dcd, PlotOrientation.VERTICAL,true, true,true); 
        ChartFrame cf = new ChartFrame("Grafik Kejadian IKP Per Jenis",freeChart);
        cf.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);   
        cf.setLocationRelativeTo(internalFrame1);
        cf.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        cf.setAlwaysOnTop(true);
        cf.setIconImage(new ImageIcon(super.getClass().getResource("/picture/addressbook-edit24.png")).getImage());
        cf.setVisible(true);   
    }//GEN-LAST:event_ppGrafikBatangKejadianIKPPerJenisActionPerformed

    private void ppGrafikPieKejadianIKPPerJenisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppGrafikPieKejadianIKPPerJenisActionPerformed
        DefaultPieDataset dpd = new DefaultPieDataset();
        try {                
            rs = koneksi.prepareStatement("select insiden_keselamatan.jenis_insiden,count(insiden_keselamatan.jenis_insiden) as jumlah "+
                   "from insiden_keselamatan_pasien inner join insiden_keselamatan on insiden_keselamatan_pasien.kode_insiden=insiden_keselamatan.kode_insiden "+
                   "where tgl_kejadian between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' group by insiden_keselamatan_pasien.jenis_insiden").executeQuery();
            while(rs.next()) {
                dpd.setValue(rs.getString(1)+"("+rs.getString(2)+")",rs.getDouble(2));
            }
        } catch (Exception e) {
            System.out.println("Notifikasi : " + e);
        }
        
        JFreeChart freeChart = ChartFactory.createPieChart("Grafik Kejadian IKP Per Jenis Periode "+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" S.D. "+Valid.SetTgl(DTPCari2.getSelectedItem()+""),dpd,true,true, false);
        ChartFrame cf = new ChartFrame("Grafik Kejadian IKP Per Jenis",freeChart);
        cf.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);   
        cf.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        cf.setLocationRelativeTo(internalFrame1);
        cf.setAlwaysOnTop(true);
        cf.setIconImage(new ImageIcon(super.getClass().getResource("/picture/addressbook-edit24.png")).getImage());
        cf.setVisible(true);
    }//GEN-LAST:event_ppGrafikPieKejadianIKPPerJenisActionPerformed

    private void ppGrafikBatangKejadianIKPPerTanggalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppGrafikBatangKejadianIKPPerTanggalActionPerformed
        DefaultCategoryDataset dcd = new DefaultCategoryDataset();
        try {                
            rs = koneksi.prepareStatement("select DATE_FORMAT(insiden_keselamatan_pasien.tgl_kejadian, '%y-%m-%d'),count(DATE_FORMAT(insiden_keselamatan_pasien.tgl_kejadian, '%y-%m-%d')) as jumlah "+
                "from insiden_keselamatan_pasien where tgl_kejadian between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' group by DATE_FORMAT(insiden_keselamatan_pasien.tgl_kejadian, '%y-%m-%d')").executeQuery();
            while(rs.next()) {
                dcd.setValue(rs.getDouble(2),rs.getString(1)+"("+rs.getString(2)+")",rs.getString(1));
            }
            
            if(rs!=null){
                rs.close();
            }
        } catch (Exception e) {
            System.out.println("Notifikasi : " + e);
        }
        JFreeChart freeChart = ChartFactory.createBarChart("Grafik Kejadian IKP Per Tanggal Tanggal "+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" S.D. "+Valid.SetTgl(DTPCari2.getSelectedItem()+""),"Tanggal","Jumlah Pasien", dcd, PlotOrientation.VERTICAL,true, true,true); 
        ChartFrame cf = new ChartFrame("Grafik Kejadian IKP Per Tanggal",freeChart);
        cf.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);   
        cf.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        cf.setLocationRelativeTo(internalFrame1);
        cf.setAlwaysOnTop(true);
        cf.setIconImage(new ImageIcon(super.getClass().getResource("/picture/addressbook-edit24.png")).getImage());
        cf.setVisible(true);  
    }//GEN-LAST:event_ppGrafikBatangKejadianIKPPerTanggalActionPerformed

    private void ppGrafikBatangKejadianIKPPerTahunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppGrafikBatangKejadianIKPPerTahunActionPerformed
        DefaultCategoryDataset dcd = new DefaultCategoryDataset();
        try {                
            rs = koneksi.prepareStatement("select year(insiden_keselamatan_pasien.tgl_kejadian),count(year(insiden_keselamatan_pasien.tgl_kejadian)) as jumlah "+
                "from insiden_keselamatan_pasien where tgl_kejadian between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' group by year(insiden_keselamatan_pasien.tgl_kejadian)").executeQuery();
            while(rs.next()) {
                dcd.setValue(rs.getDouble(2),rs.getString(1)+"("+rs.getString(2)+")",rs.getString(1));
            }
            
            if(rs!=null){
                rs.close();
            }
        } catch (Exception e) {
            System.out.println("Notifikasi : " + e);
        }
        JFreeChart freeChart = ChartFactory.createBarChart("Grafik Kejadian IKP Per Tahun Tanggal "+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" S.D. "+Valid.SetTgl(DTPCari2.getSelectedItem()+""),"Tahun","Jumlah Pasien", dcd, PlotOrientation.VERTICAL,true, true,true); 
        ChartFrame cf = new ChartFrame("Grafik Kejadian IKP Per Tahun",freeChart);
        cf.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);   
        cf.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        cf.setLocationRelativeTo(internalFrame1);
        cf.setAlwaysOnTop(true);
        cf.setIconImage(new ImageIcon(super.getClass().getResource("/picture/addressbook-edit24.png")).getImage());
        cf.setVisible(true); 
    }//GEN-LAST:event_ppGrafikBatangKejadianIKPPerTahunActionPerformed

    private void cbMelaporkanpertamaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbMelaporkanpertamaItemStateChanged
     
    }//GEN-LAST:event_cbMelaporkanpertamaItemStateChanged

    private void cbMelaporkanpertamaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbMelaporkanpertamaKeyPressed
        
    }//GEN-LAST:event_cbMelaporkanpertamaKeyPressed

    private void cbMelaporkanpertamaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMelaporkanpertamaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbMelaporkanpertamaActionPerformed

    private void KetPelaporKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetPelaporKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetPelaporKeyPressed

    private void cbTerjadipadaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbTerjadipadaItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTerjadipadaItemStateChanged

    private void cbTerjadipadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbTerjadipadaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTerjadipadaActionPerformed

    private void cbTerjadipadaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbTerjadipadaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTerjadipadaKeyPressed

    private void KetTerjadipadaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetTerjadipadaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetTerjadipadaKeyPressed

    private void cbMenyangkutpasienItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbMenyangkutpasienItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbMenyangkutpasienItemStateChanged

    private void cbMenyangkutpasienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMenyangkutpasienActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbMenyangkutpasienActionPerformed

    private void cbMenyangkutpasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbMenyangkutpasienKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbMenyangkutpasienKeyPressed

    private void KetMenyangkutpasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetMenyangkutpasienKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetMenyangkutpasienKeyPressed

    private void cbTempatinsidenItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbTempatinsidenItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTempatinsidenItemStateChanged

    private void cbTempatinsidenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbTempatinsidenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTempatinsidenActionPerformed

    private void cbTempatinsidenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbTempatinsidenKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTempatinsidenKeyPressed

    private void KetTempatinsidenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetTempatinsidenKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetTempatinsidenKeyPressed

    private void UnitMenyebabkanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UnitMenyebabkanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UnitMenyebabkanKeyPressed

    private void cbTindakanYangDilakukanItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbTindakanYangDilakukanItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTindakanYangDilakukanItemStateChanged

    private void cbTindakanYangDilakukanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbTindakanYangDilakukanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTindakanYangDilakukanActionPerformed

    private void cbTindakanYangDilakukanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbTindakanYangDilakukanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTindakanYangDilakukanKeyPressed

    private void KetTindakanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KetTindakanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KetTindakanKeyPressed

    private void cbPernahTerjadiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbPernahTerjadiItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbPernahTerjadiItemStateChanged

    private void cbPernahTerjadiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbPernahTerjadiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbPernahTerjadiActionPerformed

    private void cbPernahTerjadiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbPernahTerjadiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbPernahTerjadiKeyPressed

    private void cbGradingItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbGradingItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbGradingItemStateChanged

    private void cbGradingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbGradingActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbGradingActionPerformed

    private void cbGradingKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbGradingKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbGradingKeyPressed

    private void MnCetakFormulirIKPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakFormulirIKPActionPerformed
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),9).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),8).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()));
            finger_pj=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            param.put("finger_pj","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\n"+tbObat.getValueAt(tbObat.getSelectedRow(),32).toString()+" secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),31).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),30).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString()));
            Valid.MyReportqry("rptCetakInsidenkeselamatanpasien.jasper","report","::[ Formulir Insiden Keselamatan Pasien ]::",
                "select insiden_keselamatan_pasien.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,CONCAT( pasien.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) AS alamat, "+
                "insiden_keselamatan_pasien.tgl_kejadian,insiden_keselamatan_pasien.jam_kejadian,insiden_keselamatan_pasien.tgl_lapor,insiden_keselamatan_pasien.jam_lapor,insiden_keselamatan_pasien.jenis_insiden,insiden_keselamatan_pasien.nip,insiden_keselamatan_pasien.insiden,insiden_keselamatan_pasien.kronologis,insiden_keselamatan_pasien.unit_terkait, "+
                "insiden_keselamatan_pasien.akibat,insiden_keselamatan_pasien.tindakan_insiden,insiden_keselamatan_pasien.pelapor_pertama,ifnull(insiden_keselamatan_pasien.ket_pelapor,'')as ket_pelapor,insiden_keselamatan_pasien.terjadi_pada,ifnull(insiden_keselamatan_pasien.ket_terjadipada,'') as ket_terjadipada,insiden_keselamatan_pasien.menyangkut_pasien, ifnull(insiden_keselamatan_pasien.ket_pasien,'') as ket_pasien,"+
                "insiden_keselamatan_pasien.tempat_insiden,ifnull(insiden_keselamatan_pasien.ket_tempat,'') as ket_tempat,insiden_keselamatan_pasien.yang_menyebabkan_insiden,insiden_keselamatan_pasien.tindakan_oleh,ifnull(insiden_keselamatan_pasien.ket_tindakan,'') as ket_tindakan,insiden_keselamatan_pasien.pernah_terjadi,ifnull(insiden_keselamatan_pasien.ket_pencegahan,'') as ket_pencegahan,ifnull(insiden_keselamatan_pasien.grading,'') as grading,"+
                "petugas2.nip as nip2,petugas2.nama as nama2 ,petugas1.nip as nip1,petugas1.nama as nama1, reg_periksa.umurdaftar,reg_periksa.sttsumur,reg_periksa.kd_pj,penjab.png_jawab, reg_periksa.tgl_registrasi,reg_periksa.jam_reg,CASE reg_periksa.status_lanjut WHEN 'Ralan' THEN poliklinik.nm_poli WHEN 'Ranap' THEN CONCAT( kamar_inap.kd_kamar, ' ',bangsal.nm_bangsal ) END AS ruangan,pasien.jk,"+
                "insiden_keselamatan_pasien.status,ifnull(insiden_keselamatan_pasien.tgl_verifikasi,'00-00-0000') as tgl_verifikasi,ifnull(insiden_keselamatan_pasien.jam_verifikasi,'00:00:00') as jam_verifikasi "+
                "from insiden_keselamatan_pasien inner join reg_periksa on insiden_keselamatan_pasien.no_rawat=reg_periksa.no_rawat "+
                "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penjab on penjab.kd_pj = reg_periksa.kd_pj "+
                "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli "+
                "LEFT JOIN kamar_inap ON kamar_inap.no_rawat = reg_periksa.no_rawat " +
                "LEFT JOIN kamar ON kamar.kd_kamar = kamar_inap.kd_kamar " +
                "LEFT JOIN bangsal ON kamar.kd_bangsal = bangsal.kd_bangsal "+
                "inner join petugas as petugas1 on insiden_keselamatan_pasien.nip=petugas1.nip "+
                "inner join petugas as petugas2 on insiden_keselamatan_pasien.nip_pj=petugas2.nip "+
                "INNER JOIN kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                "INNER JOIN kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                "INNER JOIN kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                "where insiden_keselamatan_pasien.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and insiden_keselamatan_pasien.jenis_insiden ='"+tbObat.getValueAt(tbObat.getSelectedRow(),11).toString()+"' and tgl_kejadian ='"+tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()+"' and jam_kejadian = '"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+"' "+
                "ORDER BY tgl_masuk DESC, jam_masuk DESC LIMIT 1 ",param);
        }
    }//GEN-LAST:event_MnCetakFormulirIKPActionPerformed

    private void btnPetugas1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPetugas1ActionPerformed
        i=2;
        petugas.emptTeks();
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setVisible(true);
    }//GEN-LAST:event_btnPetugas1ActionPerformed

    private void nip1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nip1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_nip1KeyPressed

    private void BtnSimpanVerifikasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanVerifikasiActionPerformed
       // START CUSTOM ANGGIT
        if(NoRawat.getText().trim().equals("")){
            Valid.textKosong(NoRawat,"Nomor Rawat");
        }else if(cbGradingAtasan.getSelectedItem().equals("-")){
            Valid.textKosong(cbGradingAtasan,"Grading IKP");
        }else{
            // START CUSTOM CEK HAK AKSES ADMIN INSIDEN
            if((Sequel.cariInteger("select count(nip) from set_admin_insiden where nip='"+akses.getkode()+"'")>0)||akses.getkode().equals("Admin Utama")){
                  if(Sequel.queryu2tf("update insiden_keselamatan_pasien set status=?, tgl_verifikasi=?,jam_verifikasi=?,grading=? where no_rawat=? and jenis_insiden=? and tgl_kejadian=? and jam_kejadian=? ",8,new String[]{
                        "Diverifikasi",
                        Valid.SetTgl(DTPVerifikasi.getSelectedItem()+"").toString(),
                        (Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem()).toString(),
                        cbGradingAtasan.getSelectedItem().toString(),
                        NoRawat.getText().toString(),
                        KodeIKP.getText().toString(),
                        Valid.SetTgl(TanggalKejadianIKP.getSelectedItem()+"").toString(),
                        (Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem()).toString()
                    })==true){
                    
                    JOptionPane.showMessageDialog(null,"Kejadian IKP "+ NmPasien.getText() +" berhasil di verifikasi");
                    DlgSP.dispose();
                    tampil();
                    }else{
                  
                    JOptionPane.showMessageDialog(null,"Maaf, Verifikasi gagal disimpan");
                    }
                  
                }else{
                    if(KdPetugasPJ.getText().equals(akses.getkode())){               
                        if(Sequel.queryu2tf("update insiden_keselamatan_pasien set status=?, tgl_verifikasi=?,jam_verifikasi=?,grading=? where no_rawat=? and jenis_insiden=? and tgl_kejadian=? and jam_kejadian=? ",8,new String[]{
                        "Diverifikasi",
                        Valid.SetTgl(DTPVerifikasi.getSelectedItem()+""),
                        Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                        cbGradingAtasan.getSelectedItem().toString(),
                        NoRawat.getText(),
                        KodeIKP.getText(),
                        Valid.SetTgl(TanggalKejadianIKP.getSelectedItem()+""),
                        Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem()
                        })==true){
                        JOptionPane.showMessageDialog(null,"Kejadian IKP "+ NmPasien.getText() +" berhasil di verifikasi");
                        DlgSP.dispose();
                        tampil();
                        }else{
                        JOptionPane.showMessageDialog(null,"Maaf, Verifikasi gagal disimpan");
                        }
                    }else{
                       JOptionPane.showMessageDialog(null,"Hanya bisa diverifikasi oleh Penanggung Jawab yang bersangkutan..!!");
                    }
                }
            
        }
    // END CUSTOM ANGGIT
    }//GEN-LAST:event_BtnSimpanVerifikasiActionPerformed

    private void BtnKeluarSPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarSPActionPerformed
        // TODO add your handling code here:
        DlgSP.dispose();
    }//GEN-LAST:event_BtnKeluarSPActionPerformed

    private void BtnCetakVerifikasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCetakVerifikasiActionPerformed
        if(NoRawat.getText().trim().equals("")){
            
        } else {
          Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
            param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
            param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
            param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
            param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
            param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
            param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),9).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),8).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString()));
            finger_pj=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),31).toString());
            param.put("finger_pj","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\n"+tbObat.getValueAt(tbObat.getSelectedRow(),32).toString()+" secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),31).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),30).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString()));
            Valid.MyReportqry("rptCetakInsidenkeselamatanpasien.jasper","report","::[ Formulir Insiden Keselamatan Pasien ]::",
                "select insiden_keselamatan_pasien.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,pasien.tgl_lahir,CONCAT( pasien.alamat, ' ', kelurahan.nm_kel, ', ', kecamatan.nm_kec, ', ', kabupaten.nm_kab ) AS alamat, "+
                "insiden_keselamatan_pasien.tgl_kejadian,insiden_keselamatan_pasien.jam_kejadian,insiden_keselamatan_pasien.tgl_lapor,insiden_keselamatan_pasien.jam_lapor,insiden_keselamatan_pasien.jenis_insiden,insiden_keselamatan_pasien.nip,insiden_keselamatan_pasien.insiden,insiden_keselamatan_pasien.kronologis,insiden_keselamatan_pasien.unit_terkait, "+
                "insiden_keselamatan_pasien.akibat,insiden_keselamatan_pasien.tindakan_insiden,insiden_keselamatan_pasien.pelapor_pertama,ifnull(insiden_keselamatan_pasien.ket_pelapor,'')as ket_pelapor,insiden_keselamatan_pasien.terjadi_pada,ifnull(insiden_keselamatan_pasien.ket_terjadipada,'') as ket_terjadipada,insiden_keselamatan_pasien.menyangkut_pasien, ifnull(insiden_keselamatan_pasien.ket_pasien,'') as ket_pasien,"+
                "insiden_keselamatan_pasien.tempat_insiden,ifnull(insiden_keselamatan_pasien.ket_tempat,'') as ket_tempat,insiden_keselamatan_pasien.yang_menyebabkan_insiden,insiden_keselamatan_pasien.tindakan_oleh,ifnull(insiden_keselamatan_pasien.ket_tindakan,'') as ket_tindakan,insiden_keselamatan_pasien.pernah_terjadi,ifnull(insiden_keselamatan_pasien.ket_pencegahan,'') as ket_pencegahan,ifnull(insiden_keselamatan_pasien.grading,'') as grading,"+
                "petugas2.nip as nip2,petugas2.nama as nama2 ,petugas1.nip as nip1,petugas1.nama as nama1, reg_periksa.umurdaftar,reg_periksa.sttsumur,reg_periksa.kd_pj,penjab.png_jawab, reg_periksa.tgl_registrasi,reg_periksa.jam_reg,CASE reg_periksa.status_lanjut WHEN 'Ralan' THEN poliklinik.nm_poli WHEN 'Ranap' THEN CONCAT( kamar_inap.kd_kamar, ' ',bangsal.nm_bangsal ) END AS ruangan,pasien.jk,"+
                "insiden_keselamatan_pasien.status,ifnull(insiden_keselamatan_pasien.tgl_verifikasi,'00-00-0000') as tgl_verifikasi,ifnull(insiden_keselamatan_pasien.jam_verifikasi,'00:00:00') as jam_verifikasi "+
                "from insiden_keselamatan_pasien inner join reg_periksa on insiden_keselamatan_pasien.no_rawat=reg_periksa.no_rawat "+
                "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penjab on penjab.kd_pj = reg_periksa.kd_pj "+
                "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli "+
                "LEFT JOIN kamar_inap ON kamar_inap.no_rawat = reg_periksa.no_rawat " +
                "LEFT JOIN kamar ON kamar.kd_kamar = kamar_inap.kd_kamar " +
                "LEFT JOIN bangsal ON kamar.kd_bangsal = bangsal.kd_bangsal "+
                "inner join petugas as petugas1 on insiden_keselamatan_pasien.nip=petugas1.nip "+
                "inner join petugas as petugas2 on insiden_keselamatan_pasien.nip_pj=petugas2.nip "+
                "INNER JOIN kelurahan ON kelurahan.kd_kel = pasien.kd_kel " +
                "INNER JOIN kecamatan ON kecamatan.kd_kec = pasien.kd_kec " +
                "INNER JOIN kabupaten ON kabupaten.kd_kab = pasien.kd_kab " +
                "where insiden_keselamatan_pasien.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"' and insiden_keselamatan_pasien.jenis_insiden ='"+tbObat.getValueAt(tbObat.getSelectedRow(),11).toString()+"' and tgl_kejadian ='"+tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()+"' and jam_kejadian = '"+tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()+"' "+
                "ORDER BY tgl_masuk DESC, jam_masuk DESC LIMIT 1 ",param);
        }
    }//GEN-LAST:event_BtnCetakVerifikasiActionPerformed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
      
    }//GEN-LAST:event_JamKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        Valid.pindah(evt,Jam,Detik);
    }//GEN-LAST:event_MenitKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        Valid.pindah(evt,Menit,btnPetugas);
    }//GEN-LAST:event_DetikKeyPressed

    private void Jam1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Jam1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Jam1KeyPressed

    private void Menit1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Menit1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Menit1KeyPressed

    private void Detik1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Detik1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_Detik1KeyPressed

    private void MnVerifikasiIKPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnVerifikasiIKPActionPerformed
        // TODO add your handling code here:
        if(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString().trim().equals("")){
            Valid.textKosong(TCari,"Nomor Rawat");
        }else{
            DlgSP.setSize(800,300);
            DlgSP.setLocationRelativeTo(internalFrame1);
            DlgSP.setVisible(true);
            NoRawat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            NoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            NmPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            Valid.SetTgl(TanggalKejadianIKP,tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());
            KodeIKP.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            KdPetugasPJ.setText(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            NmPetugasPJ.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
            cbGradingAtasan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());
            KejadianIKP.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            Jam.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(), 5).toString().substring(0, 2));
            Menit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(), 5).toString().substring(3, 5));
            Detik.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(), 5).toString().substring(6, 8));
            if (tbObat.getValueAt(tbObat.getSelectedRow(), 31).toString().equals("Diverifikasi")) {
                ChkKejadian2.setSelected(false);
                Jam1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(), 33).toString().substring(0, 2));
                Menit1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(), 33).toString().substring(3, 5));
                Detik1.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(), 33).toString().substring(6, 8));
                Valid.SetTgl(DTPVerifikasi,tbObat.getValueAt(tbObat.getSelectedRow(),32).toString());
            } else {
                ChkKejadian2.setSelected(true);
            }
           
        } 
    }//GEN-LAST:event_MnVerifikasiIKPActionPerformed

    private void BtnTolakVerifikasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTolakVerifikasiActionPerformed
      // START CUSTOM ANGGIT
        if(NoRawat.getText().trim().equals("")){
            Valid.textKosong(NoRawat,"Nomor Rawat");
        }else if(cbGradingAtasan.getSelectedItem().equals("-")){
            Valid.textKosong(cbGradingAtasan,"Grading IKP");
        }else{
            // START CUSTOM CEK HAK AKSES ADMIN INSIDEN
            if((Sequel.cariInteger("select count(nip) from set_admin_insiden where nip='"+akses.getkode()+"'")>0)||akses.getkode().equals("Admin Utama")){
                  if(Sequel.queryu2tf("update insiden_keselamatan_pasien set status=?, tgl_verifikasi=?,jam_verifikasi=? where no_rawat=? and jenis_insiden=? and tgl_kejadian=? and jam_kejadian=? ",7,new String[]{
                        "Ditolak",
                        Valid.SetTgl(DTPVerifikasi.getSelectedItem()+"").toString(),
                        (Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem()).toString(),
                        NoRawat.getText().toString(),
                        KodeIKP.getText().toString(),
                        Valid.SetTgl(TanggalKejadianIKP.getSelectedItem()+"").toString(),
                        (Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem()).toString()
                    })==true){
                    
                    JOptionPane.showMessageDialog(null,"Kejadian IKP "+ NmPasien.getText() +" berhasil di tolak");
                    DlgSP.dispose();
                    tampil();
                    }else{
                  
                    JOptionPane.showMessageDialog(null,"Maaf, Proses penolakan laporan IKP gagal disimpan");
                    }
                  
                }else{
                    if(KdPetugasPJ.getText().equals(akses.getkode())){               
                        if(Sequel.queryu2tf("update insiden_keselamatan_pasien set status=?, tgl_verifikasi=?,jam_verifikasi=? where no_rawat=?,jenis_insiden=? ,tgl_kejadian=?, jam_kejadian=? ",7,new String[]{
                        "Ditolak",
                        Valid.SetTgl(DTPVerifikasi.getSelectedItem()+""),
                        Jam1.getSelectedItem()+":"+Menit1.getSelectedItem()+":"+Detik1.getSelectedItem(),
                        NoRawat.getText(),
                        KodeIKP.getText(),
                        Valid.SetTgl(TanggalKejadianIKP.getSelectedItem()+""),
                        Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem()
                        })==true){
                        JOptionPane.showMessageDialog(null,"Laporan Kejadian IKP "+ NmPasien.getText() +" berhasil di tolak");
                        DlgSP.dispose();
                        tampil();
                        }else{
                        JOptionPane.showMessageDialog(null,"Maaf, Proses penolakan gagal disimpan");
                        }
                    }else{
                       JOptionPane.showMessageDialog(null,"Hanya bisa ditolak oleh Penanggung Jawab yang bersangkutan..!!");
                    }
                }
            
        }
    // END CUSTOM ANGGIT
    }//GEN-LAST:event_BtnTolakVerifikasiActionPerformed

    private void TindakanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TindakanKeyPressed
         //CUSTOM MUHSIN
    }//GEN-LAST:event_TindakanKeyPressed

    private void akibatItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_akibatItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_akibatItemStateChanged

    private void akibatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_akibatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_akibatActionPerformed

    private void akibatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_akibatKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_akibatKeyPressed

    private void cbinsidenItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbinsidenItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbinsidenItemStateChanged

    private void cbinsidenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbinsidenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbinsidenActionPerformed

    private void cbinsidenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbinsidenKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbinsidenKeyPressed

    private void btnListpasienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListpasienActionPerformed
       
        caripasien.emptTeks();
        caripasien.isCek();
        caripasien.settgl(DTPCari1.getDate(),DTPCari2.getDate());
        caripasien.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        caripasien.setLocationRelativeTo(internalFrame1);
        caripasien.setVisible(true);
    }//GEN-LAST:event_btnListpasienActionPerformed

    private void btnListpasienRanapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListpasienRanapActionPerformed
   
        caripasienranap.emptTeks();
        caripasienranap.settgl(DTPCari1.getDate(),DTPCari2.getDate());
        caripasienranap.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        caripasienranap.setLocationRelativeTo(internalFrame1);
        caripasienranap.setVisible(true);
    }//GEN-LAST:event_btnListpasienRanapActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            DlgDataInsidenKeselamatan dialog = new DlgDataInsidenKeselamatan(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnCetakVerifikasi;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnKeluarSP;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Button BtnSimpanVerifikasi;
    private widget.Button BtnTolakVerifikasi;
    private widget.CekBox ChkInput;
    private widget.CekBox ChkKejadian;
    private widget.CekBox ChkKejadian2;
    private widget.CekBox ChkLapor;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.Tanggal DTPVerifikasi;
    private widget.ComboBox Detik;
    private widget.ComboBox Detik1;
    private widget.ComboBox DetikKejadian;
    private widget.ComboBox DetikLapor;
    private javax.swing.JDialog DlgSP;
    private widget.PanelBiasa FormInput;
    private widget.TextBox Insiden;
    private widget.ComboBox Jam;
    private widget.ComboBox Jam1;
    private widget.ComboBox JamKejadian;
    private widget.ComboBox JamLapor;
    private widget.TextBox KdPetugasPJ;
    private widget.Tanggal Kejadian;
    private widget.TextBox KejadianIKP;
    private widget.TextBox KetMenyangkutpasien;
    private widget.TextBox KetPelapor;
    private javax.swing.JTextArea KetPencegahan;
    private widget.TextBox KetTempatinsiden;
    private widget.TextBox KetTerjadipada;
    private widget.TextBox KetTindakan;
    private widget.TextBox KodeIKP;
    private javax.swing.JTextArea Kronologis;
    private widget.Label LCount;
    private widget.Tanggal Lapor;
    private widget.ComboBox Menit;
    private widget.ComboBox Menit1;
    private widget.ComboBox MenitKejadian;
    private widget.ComboBox MenitLapor;
    private javax.swing.JMenuItem MnCetakFormulirIKP;
    private javax.swing.JMenuItem MnVerifikasiIKP;
    private widget.TextBox NmPasien;
    private widget.TextBox NmPetugasPJ;
    private widget.TextBox NoRM;
    private widget.TextBox NoRawat;
    private javax.swing.JPanel PanelInput;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.Tanggal TanggalKejadianIKP;
    private widget.TextArea Tindakan;
    private widget.TextBox UnitMenyebabkan;
    private widget.TextBox UnitTerkait;
    private widget.ComboBox akibat;
    private widget.Button btnListpasien;
    private widget.Button btnListpasienRanap;
    private widget.Button btnPetugas;
    private widget.Button btnPetugas1;
    private widget.ComboBox cbGrading;
    private widget.ComboBox cbGradingAtasan;
    private widget.ComboBox cbMelaporkanpertama;
    private widget.ComboBox cbMenyangkutpasien;
    private widget.ComboBox cbPernahTerjadi;
    private widget.ComboBox cbTempatinsiden;
    private widget.ComboBox cbTerjadipada;
    private widget.ComboBox cbTindakanYangDilakukan;
    private widget.ComboBox cbinsiden;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame7;
    private widget.Label jLabel16;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel29;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel4;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel48;
    private widget.Label jLabel49;
    private widget.Label jLabel50;
    private widget.Label jLabel51;
    private widget.Label jLabel52;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane5;
    private widget.TextBox namapetugas;
    private widget.TextBox namapetugas1;
    private widget.TextBox nip;
    private widget.TextBox nip1;
    private widget.PanelBiasa panelBiasa6;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private javax.swing.JMenuItem ppGrafikBatangKejadianIKPPerDampak;
    private javax.swing.JMenuItem ppGrafikBatangKejadianIKPPerJenis;
    private javax.swing.JMenuItem ppGrafikBatangKejadianIKPPerTahun;
    private javax.swing.JMenuItem ppGrafikBatangKejadianIKPPerTanggal;
    private javax.swing.JMenuItem ppGrafikBatangKejadianIKPPerbulan;
    private javax.swing.JMenuItem ppGrafikPieKejadianIKPPerDampak;
    private javax.swing.JMenuItem ppGrafikPieKejadianIKPPerJenis;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane1;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        // START CUSTOM CEK HAK AKSES ADMIN INSIDEN
        if((Sequel.cariInteger("select count(nip) from set_admin_insiden where nip='"+akses.getkode()+"'")>0)||akses.getkode().equals("Admin Utama")){
            // TAMPIL SEMUA LAPORAN IKP
                try{
                ps=koneksi.prepareStatement(
                    "select insiden_keselamatan_pasien.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,"+
                    "reg_periksa.umurdaftar,reg_periksa.sttsumur,insiden_keselamatan_pasien.tgl_kejadian,"+
                    "insiden_keselamatan_pasien.jam_kejadian,insiden_keselamatan_pasien.tgl_lapor,insiden_keselamatan_pasien.jam_lapor,"+
                    "petugas1.nip,petugas1.nama,insiden_keselamatan_pasien.insiden,"+
                    "insiden_keselamatan_pasien.jenis_insiden,insiden_keselamatan_pasien.unit_terkait,"+
                    "insiden_keselamatan_pasien.kronologis,insiden_keselamatan_pasien.akibat,insiden_keselamatan_pasien.tindakan_insiden,"+
                    "insiden_keselamatan_pasien.pelapor_pertama,insiden_keselamatan_pasien.ket_pelapor,insiden_keselamatan_pasien.terjadi_pada,"+
                    "insiden_keselamatan_pasien.ket_terjadipada,insiden_keselamatan_pasien.menyangkut_pasien,insiden_keselamatan_pasien.ket_pasien,"+
                    "insiden_keselamatan_pasien.tempat_insiden,insiden_keselamatan_pasien.ket_tempat,insiden_keselamatan_pasien.yang_menyebabkan_insiden,"+
                    "insiden_keselamatan_pasien.tindakan_oleh,insiden_keselamatan_pasien.pernah_terjadi,insiden_keselamatan_pasien.ket_tindakan,insiden_keselamatan_pasien.ket_pencegahan,petugas2.nip,petugas2.nama,insiden_keselamatan_pasien.status,ifnull(insiden_keselamatan_pasien.tgl_verifikasi,'00-00-0000') as tgl_verifikasi,insiden_keselamatan_pasien.jam_verifikasi,insiden_keselamatan_pasien.grading "+
                    "from insiden_keselamatan_pasien "+ 
                    "inner join reg_periksa on insiden_keselamatan_pasien.no_rawat = reg_periksa.no_rawat "+ 
                    "inner join pasien on reg_periksa.no_rkm_medis = pasien.no_rkm_medis "+
                    "inner join petugas as petugas1 on insiden_keselamatan_pasien.nip=petugas1.nip "+
                    "inner join petugas as petugas2 on insiden_keselamatan_pasien.nip_pj=petugas2.nip "+
                    "where "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and ( insiden_keselamatan_pasien.no_rawat like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and reg_periksa.no_rkm_medis like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and pasien.nm_pasien like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.nip like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and petugas1.nama like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.insiden like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.jenis_insiden like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.unit_terkait like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.kronologis like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.akibat like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.tindakan_insiden like ? )"+
                    "order by insiden_keselamatan_pasien.tgl_kejadian ");
                try {
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(5,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(8,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(11,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(12,"%"+TCari.getText()+"%");
                    ps.setString(13,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(14,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(15,"%"+TCari.getText()+"%");
                    ps.setString(16,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(17,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(18,"%"+TCari.getText()+"%");
                    ps.setString(19,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(20,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(21,"%"+TCari.getText()+"%");
                    ps.setString(22,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(23,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(24,"%"+TCari.getText()+"%");
                    ps.setString(25,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(26,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(27,"%"+TCari.getText()+"%");
                    ps.setString(28,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(29,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(30,"%"+TCari.getText()+"%");
                    ps.setString(31,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(32,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(33,"%"+TCari.getText()+"%");
         
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabMode.addRow(new String[]{
                            rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),
                            rs.getString("umurdaftar")+" "+rs.getString("sttsumur"),rs.getString("tgl_kejadian"),rs.getString("jam_kejadian"),
                            rs.getString("tgl_lapor"),rs.getString("jam_lapor"),rs.getString("petugas1.nip"),
                            rs.getString("petugas1.nama"),rs.getString("insiden"),rs.getString("jenis_insiden"),
                            rs.getString("unit_terkait"),rs.getString("kronologis"),rs.getString("akibat"),
                            rs.getString("tindakan_insiden"),
                            rs.getString("pelapor_pertama"),rs.getString("ket_pelapor"),rs.getString("terjadi_pada"),
                            rs.getString("ket_terjadipada"),rs.getString("menyangkut_pasien"),rs.getString("ket_pasien"),
                            rs.getString("tempat_insiden"),rs.getString("ket_tempat"),rs.getString("yang_menyebabkan_insiden"),
                            rs.getString("tindakan_oleh"),rs.getString("ket_tindakan"),rs.getString("pernah_terjadi"),rs.getString("ket_pencegahan"),rs.getString("petugas2.nip"),rs.getString("petugas2.nama"),rs.getString("status"),rs.getString("tgl_verifikasi"),rs.getString("jam_verifikasi"),rs.getString("grading")

                        });
                    }
                    
                } catch (Exception e) {
                    System.out.println("Notif : "+e);

                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } 
            catch(SQLException e){
                System.out.println("Notifikasi : "+e);
            }
        } else {
            // TAMPIL SESUAI ID LOGIN
                try{
                ps=koneksi.prepareStatement(
                    "select insiden_keselamatan_pasien.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,"+
                    "reg_periksa.umurdaftar,reg_periksa.sttsumur,insiden_keselamatan_pasien.tgl_kejadian,"+
                    "insiden_keselamatan_pasien.jam_kejadian,insiden_keselamatan_pasien.tgl_lapor,insiden_keselamatan_pasien.jam_lapor,"+
                    "petugas1.nip,petugas1.nama,insiden_keselamatan_pasien.insiden,"+
                    "insiden_keselamatan_pasien.jenis_insiden,insiden_keselamatan_pasien.unit_terkait,"+
                    "insiden_keselamatan_pasien.kronologis,insiden_keselamatan_pasien.akibat,insiden_keselamatan_pasien.tindakan_insiden,"+
                    "insiden_keselamatan_pasien.pelapor_pertama,insiden_keselamatan_pasien.ket_pelapor,insiden_keselamatan_pasien.terjadi_pada,"+
                    "insiden_keselamatan_pasien.ket_terjadipada,insiden_keselamatan_pasien.menyangkut_pasien,insiden_keselamatan_pasien.ket_pasien,"+
                    "insiden_keselamatan_pasien.tempat_insiden,insiden_keselamatan_pasien.ket_tempat,insiden_keselamatan_pasien.yang_menyebabkan_insiden,"+
                    "insiden_keselamatan_pasien.tindakan_oleh,insiden_keselamatan_pasien.pernah_terjadi,insiden_keselamatan_pasien.ket_tindakan,insiden_keselamatan_pasien.ket_pencegahan,petugas2.nip,petugas2.nama,insiden_keselamatan_pasien.status,ifnull(insiden_keselamatan_pasien.tgl_verifikasi,'00-00-0000') as tgl_verifikasi,insiden_keselamatan_pasien.jam_verifikasi,insiden_keselamatan_pasien.grading  "+
                    "from insiden_keselamatan_pasien "+ 
                    "inner join reg_periksa on insiden_keselamatan_pasien.no_rawat = reg_periksa.no_rawat "+ 
                    "inner join pasien on reg_periksa.no_rkm_medis = pasien.no_rkm_medis "+
                    "inner join petugas as petugas1 on insiden_keselamatan_pasien.nip=petugas1.nip "+
                    "inner join petugas as petugas2 on insiden_keselamatan_pasien.nip_pj=petugas2.nip "+
                    "where "+
                    "(insiden_keselamatan_pasien.nip=? or insiden_keselamatan_pasien.nip_pj=? ) and "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and ( insiden_keselamatan_pasien.no_rawat like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and reg_periksa.no_rkm_medis like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and pasien.nm_pasien like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.nip like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and petugas1.nama like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.insiden like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.jenis_insiden like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.unit_terkait like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.kronologis like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.akibat like ? or "+
                    "insiden_keselamatan_pasien.tgl_kejadian between ? and ? and insiden_keselamatan_pasien.tindakan_insiden like ? ) "+
                    "order by insiden_keselamatan_pasien.tgl_kejadian ");
                try {
                    ps.setString(1,akses.getkode());
                    ps.setString(2,akses.getkode());
                    ps.setString(3,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(4,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(7,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(8,"%"+TCari.getText()+"%");
                    ps.setString(9,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(10,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(11,"%"+TCari.getText()+"%");
                    ps.setString(12,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(13,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(14,"%"+TCari.getText()+"%");
                    ps.setString(15,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(16,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(17,"%"+TCari.getText()+"%");
                    ps.setString(18,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(19,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(20,"%"+TCari.getText()+"%");
                    ps.setString(21,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(22,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(23,"%"+TCari.getText()+"%");
                    ps.setString(24,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(25,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(26,"%"+TCari.getText()+"%");
                    ps.setString(27,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(28,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(29,"%"+TCari.getText()+"%");
                    ps.setString(30,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(31,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(32,"%"+TCari.getText()+"%");
                    ps.setString(33,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(34,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(35,"%"+TCari.getText()+"%");
                    rs=ps.executeQuery();
                    while(rs.next()){
                        tabMode.addRow(new String[]{
                            rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),
                            rs.getString("umurdaftar")+" "+rs.getString("sttsumur"),rs.getString("tgl_kejadian"),rs.getString("jam_kejadian"),
                            rs.getString("tgl_lapor"),rs.getString("jam_lapor"),rs.getString("petugas1.nip"),
                            rs.getString("petugas1.nama"),rs.getString("insiden"),rs.getString("jenis_insiden"),
                            rs.getString("unit_terkait"),rs.getString("kronologis"),rs.getString("akibat"),
                            rs.getString("tindakan_insiden"),
                            rs.getString("pelapor_pertama"),rs.getString("ket_pelapor"),rs.getString("terjadi_pada"),
                            rs.getString("ket_terjadipada"),rs.getString("menyangkut_pasien"),rs.getString("ket_pasien"),
                            rs.getString("tempat_insiden"),rs.getString("ket_tempat"),rs.getString("yang_menyebabkan_insiden"),
                            rs.getString("tindakan_oleh"),rs.getString("ket_tindakan"),rs.getString("pernah_terjadi"),rs.getString("ket_pencegahan"),rs.getString("petugas2.nip"),rs.getString("petugas2.nama"),rs.getString("status"),rs.getString("tgl_verifikasi"),rs.getString("jam_verifikasi"),rs.getString("grading")

                        });
                    }
                    
                } catch (Exception e) {
                    System.out.println("Notif : "+e);

                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }
            } 
            catch(SQLException e){
                System.out.println("Notifikasi : "+e);
            }
        }
        
        // END CUSTOM ANGGIT
        int b=tabMode.getRowCount();
        LCount.setText(""+b);
    }

    public void emptTeks() {
        nip.setText("");
        namapetugas.setText("");
        nip1.setText("");
        namapetugas1.setText("");
        cbinsiden.setSelectedItem("-");
        Insiden.setText("");
        UnitTerkait.setText("");
        Kronologis.setText("");
        akibat.setSelectedItem("-");
        Tindakan.setText("");
        Kejadian.setDate(new Date());
        Lapor.setDate(new Date());
        Kejadian.requestFocus();
        cbMelaporkanpertama.setSelectedItem("Dokter");
        KetPelapor.setText("");
        cbTerjadipada.setSelectedItem("Pasien");
        KetTerjadipada.setText("");
        cbTempatinsiden.setSelectedItem("Penyakit Dalam dan Subspesialisnya");
        KetTempatinsiden.setText("");
        cbMenyangkutpasien.setSelectedItem("Pasien Rawat Inap");
        KetMenyangkutpasien.setText("");
        UnitMenyebabkan.setText("");
        cbTindakanYangDilakukan.setSelectedItem("TIM");
        KetTindakan.setText("");
        cbPernahTerjadi.setSelectedItem("Tidak");
        KetPencegahan.setText("");
        cbGrading.setSelectedItem("-");
    } 
    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            Valid.SetTgl(Kejadian,tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());
            JamKejadian.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString().substring(0,2));
            MenitKejadian.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString().substring(3,5));
            DetikKejadian.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString().substring(6,8));
            Valid.SetTgl(Lapor,tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            JamLapor.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString().substring(0,2));
            MenitLapor.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString().substring(3,5));
            DetikLapor.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString().substring(6,8));
            nip.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            namapetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            Insiden.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            cbinsiden.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            UnitTerkait.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            Kronologis.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            akibat.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            Tindakan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            cbMelaporkanpertama.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            KetPelapor.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            cbTerjadipada.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),18));
            KetTerjadipada.setText(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            cbMenyangkutpasien.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),20));
            KetMenyangkutpasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            cbTempatinsiden.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),22));
            KetTempatinsiden.setText(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            UnitMenyebabkan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            cbTindakanYangDilakukan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),25));
            KetTindakan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString());
            cbPernahTerjadi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),27));
            KetPencegahan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString());
            nip1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),29).toString());
            namapetugas1.setText(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString());
            cbGrading.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),34));
        }
    }

    private void isRawat() {
         Sequel.cariIsi("select no_rkm_medis from reg_periksa where no_rawat='"+TNoRw.getText()+"' ",TNoRM);
    }

    private void isPsien() {
        Sequel.cariIsi("select nm_pasien from pasien where no_rkm_medis='"+TNoRM.getText()+"' ",TPasien);
    }
    
    public void setNoRm(String norwt, Date tgl1, Date tgl2,String unit) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari1.setDate(tgl1);
        DTPCari2.setDate(tgl2);       
        isRawat();
        isPsien();              
        ChkInput.setSelected(true);
        isForm();
    }
    
    private void isForm(){
        if(ChkInput.isSelected()==true){
            ChkInput.setVisible(false);
            PanelInput.setPreferredSize(new Dimension(WIDTH,246));
            FormInput.setVisible(true);      
            ChkInput.setVisible(true);
        }else if(ChkInput.isSelected()==false){           
            ChkInput.setVisible(false);            
            PanelInput.setPreferredSize(new Dimension(WIDTH,20));
            FormInput.setVisible(false);      
            ChkInput.setVisible(true);
        }
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getinsiden_keselamatan_pasien());
        BtnHapus.setEnabled(akses.getinsiden_keselamatan_pasien());
        BtnPrint.setEnabled(akses.getinsiden_keselamatan_pasien()); 
        if(akses.getjml2()>=1){
            nip.setText(akses.getkode());
            nip.setEditable(false);
            btnPetugas.setEnabled(false);
            Sequel.cariIsi("select nama from petugas where nip=?",namapetugas,nip.getText());
            namapetugas.setText(petugas.tampil3(nip.getText()));
        }            
    }

    private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkKejadian.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkKejadian.isSelected()==false){
                    nilai_jam =JamKejadian.getSelectedIndex();
                    nilai_menit =MenitKejadian.getSelectedIndex();
                    nilai_detik =DetikKejadian.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                JamKejadian.setSelectedItem(jam);
                MenitKejadian.setSelectedItem(menit);
                DetikKejadian.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
    private void jam2(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkLapor.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkLapor.isSelected()==false){
                    nilai_jam =JamLapor.getSelectedIndex();
                    nilai_menit =MenitLapor.getSelectedIndex();
                    nilai_detik =DetikLapor.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                JamLapor.setSelectedItem(jam);
                MenitLapor.setSelectedItem(menit);
                DetikLapor.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    private void jam3(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkKejadian2.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkKejadian2.isSelected()==false){
                    nilai_jam =Jam1.getSelectedIndex();
                    nilai_menit =Menit1.getSelectedIndex();
                    nilai_detik =Detik1.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam1.setSelectedItem(jam);
                Menit1.setSelectedItem(menit);
                Detik1.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
}
