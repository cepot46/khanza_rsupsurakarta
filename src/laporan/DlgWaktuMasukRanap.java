/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DlgLhtBiaya.java
 *
 * Created on 12 Jul 10, 16:21:34
 */

package laporan;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.KeyEvent; 
import java.awt.event.KeyListener;  //CUSTOM MUHSIN
import java.awt.event.WindowEvent;  //CUSTOM MUHSIN
import java.awt.event.WindowListener;   //CUSTOM MUHSIN
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import simrskhanza.DlgCariBangsal;  //CUSTOM MUHSIN

/**
 *
 * @author perpustakaan
 */
public final class DlgWaktuMasukRanap extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0,limabelas=0,tigapuluh=0,satujam=0,lebihsatujam=0,
            limabelas2=0,tigapuluh2=0,satujam2=0,lebihsatujam2=0,
            limabelas3=0,tigapuluh3=0,satujam3=0,lebihsatujam3=0;
    private double lamajam=0,lamajam2=0,lamajam3=0,kurang60=0,sampel60=0,kurang24=0;
    private DlgCariBangsal ruangan=new DlgCariBangsal(null,false);  //CUSTOM MUHSIN   
    /** Creates new form DlgLhtBiaya
     * @param parent
     * @param modal */
    public DlgWaktuMasukRanap(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(885,674);

        tabMode=new DefaultTableModel(null,new Object[]{
            "No.","No.RM","Nama Pasien","DPJP","Ruangan","No. Rawat",
            "Waktu Admisi","Waktu Terima/Waktu Pilih DPJP","Waktu tunggu (menit)"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbBangsal.setModel(tabMode);
        //tbBangsal.setDefaultRenderer(Object.class, new WarnaTable(jPanel2.getBackground(),tbBangsal.getBackground()));
        tbBangsal.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbBangsal.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 9; i++) {
            TableColumn column = tbBangsal.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(35);
            }else if(i==1){
                column.setPreferredWidth(70);
            }else if(i==2){
                column.setPreferredWidth(185);
            }else if(i==3){
                column.setPreferredWidth(185);
            }else if(i==4){
                column.setPreferredWidth(150);
            }else if(i==5){
                column.setPreferredWidth(110);
            }else if(i==6){
                column.setPreferredWidth(200);
            }else if(i==7){
                column.setPreferredWidth(200);
            }else if(i==8){
                column.setPreferredWidth(110);
            }
        }
        tbBangsal.setDefaultRenderer(Object.class, new WarnaTable());
        
        //START CUSTOM MUHSIN
        ruangan.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(ruangan.getTable().getSelectedRow()!= -1){
                    KdRuangan.setText(ruangan.getTable().getValueAt(ruangan.getTable().getSelectedRow(),0).toString());
                    NmRuangan.setText(ruangan.getTable().getValueAt(ruangan.getTable().getSelectedRow(),1).toString());
                    BtnRuangan.requestFocus();                  
                }      
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {ruangan.emptTeks();}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });   
        
        ruangan.getTable().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_SPACE){
                    ruangan.dispose();
                }
            }
            @Override
            public void keyReleased(KeyEvent e) {}
        });
        //END CUSTOM

        TKd.setDocument(new batasInput((byte)20).getKata(TKd));
        TCari.setDocument(new batasInput((byte)100).getKata(TCari));
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }  
    }    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TKd = new widget.TextBox();
        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbBangsal = new widget.Table();
        panelGlass5 = new widget.panelisi();
        label11 = new widget.Label();
        Tgl1 = new widget.Tanggal();
        label18 = new widget.Label();
        Tgl2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        label34 = new widget.Label();
        KdRuangan = new widget.TextBox();
        NmRuangan = new widget.TextBox();
        BtnRuangan = new widget.Button();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        jLabel7 = new widget.Label();
        BtnPrint = new widget.Button();
        BtnKeluar = new widget.Button();

        TKd.setForeground(new java.awt.Color(255, 255, 255));
        TKd.setName("TKd"); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Waktu Masuk Rawat Inap (KPI 6) ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);

        tbBangsal.setName("tbBangsal"); // NOI18N
        tbBangsal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbBangsalMouseClicked(evt);
            }
        });
        tbBangsal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbBangsalKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbBangsal);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass5.setBackground(new java.awt.Color(255, 250, 248));
        panelGlass5.setName("panelGlass5"); // NOI18N
        panelGlass5.setPreferredSize(new java.awt.Dimension(55, 55));
        panelGlass5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(50, 23));
        panelGlass5.add(label11);

        Tgl1.setDisplayFormat("dd-MM-yyyy");
        Tgl1.setName("Tgl1"); // NOI18N
        Tgl1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass5.add(Tgl1);

        label18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label18.setText("s.d.");
        label18.setName("label18"); // NOI18N
        label18.setPreferredSize(new java.awt.Dimension(25, 23));
        panelGlass5.add(label18);

        Tgl2.setDisplayFormat("dd-MM-yyyy");
        Tgl2.setName("Tgl2"); // NOI18N
        Tgl2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass5.add(Tgl2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass5.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(155, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass5.add(TCari);

        label34.setText("Ruangan :");
        label34.setName("label34"); // NOI18N
        label34.setPreferredSize(new java.awt.Dimension(75, 23));
        panelGlass5.add(label34);

        KdRuangan.setEditable(false);
        KdRuangan.setName("KdRuangan"); // NOI18N
        KdRuangan.setPreferredSize(new java.awt.Dimension(50, 23));
        panelGlass5.add(KdRuangan);

        NmRuangan.setEditable(false);
        NmRuangan.setName("NmRuangan"); // NOI18N
        NmRuangan.setPreferredSize(new java.awt.Dimension(150, 23));
        panelGlass5.add(NmRuangan);

        BtnRuangan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnRuangan.setMnemonic('3');
        BtnRuangan.setToolTipText("Alt+3");
        BtnRuangan.setName("BtnRuangan"); // NOI18N
        BtnRuangan.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnRuangan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRuanganActionPerformed(evt);
            }
        });
        panelGlass5.add(BtnRuangan);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('2');
        BtnCari.setToolTipText("Alt+2");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnAll);

        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(30, 23));
        panelGlass5.add(jLabel7);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnPrint);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnKeluar);

        internalFrame1.add(panelGlass5, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
//        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        if(tabMode.getRowCount()==0){
//            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
//            //TCari.requestFocus();
//        }else if(tabMode.getRowCount()!=0){
//            
//            Map<String, Object> param = new HashMap<>();         
//            param.put("namars",akses.getnamars());
//            param.put("alamatrs",akses.getalamatrs());
//            param.put("kotars",akses.getkabupatenrs());
//            param.put("propinsirs",akses.getpropinsirs());
//            param.put("kontakrs",akses.getkontakrs());
//            param.put("emailrs",akses.getemailrs());   
//            param.put("tanggal",Tgl1.getSelectedItem()+" s.d. "+Tgl2.getSelectedItem());
//            Sequel.queryu("truncate table temporary_lama_pelayanan_radiologi");
//            for(int r=0;r<tabMode.getRowCount();r++){ 
//                Sequel.menyimpan("temporary_lama_pelayanan_radiologi","'0','"+
//                    tabMode.getValueAt(r,0).toString()+"','"+
//                    tabMode.getValueAt(r,1).toString()+"','"+
//                    tabMode.getValueAt(r,2).toString()+"','"+
//                    tabMode.getValueAt(r,3).toString()+"','"+
//                    tabMode.getValueAt(r,4).toString()+"','"+
//                    tabMode.getValueAt(r,5).toString()+"','"+
//                    tabMode.getValueAt(r,6).toString()+"','"+
//                    tabMode.getValueAt(r,7).toString()+"','"+
//                    tabMode.getValueAt(r,8).toString()+"','"+
//                    tabMode.getValueAt(r,9).toString()+"','"+
//                    tabMode.getValueAt(r,10).toString()+"','','','','','','','','','','','','','','','','','','','','','','','','','',''","Rekap Nota Pembayaran");
//            }
//               
//            Valid.MyReport("rptPelayananLab.jasper","report","::[ Laporan Lama Pelayanan Laboratorium ]::",param);
//        }
//        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            //Valid.pindah(evt, BtnHapus, BtnAll);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            dispose();
        }else{Valid.pindah(evt,BtnKeluar,TKd);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void tbBangsalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbBangsalMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
        }
}//GEN-LAST:event_tbBangsalMouseClicked

    private void tbBangsalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbBangsalKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbBangsalKeyPressed

private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
       tampil();
}//GEN-LAST:event_BtnCariActionPerformed

private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)); 
            tampil();
            this.setCursor(Cursor.getDefaultCursor());
        }else{
            Valid.pindah(evt, TKd, BtnPrint);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        tampil();
    }//GEN-LAST:event_formWindowOpened

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
           TCari.setText("");
           KdRuangan.setText("");
           NmRuangan.setText("");
           tampil();
    }//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }else{
            
        }
    }//GEN-LAST:event_BtnAllKeyPressed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        tampil();

    }//GEN-LAST:event_formWindowActivated

    private void BtnRuanganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRuanganActionPerformed
        // TODO add your handling code here:
        ruangan.isCek();
        ruangan.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        ruangan.setLocationRelativeTo(internalFrame1);
        ruangan.setAlwaysOnTop(false);
        ruangan.setVisible(true);
    }//GEN-LAST:event_BtnRuanganActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            DlgPelayananLab dialog = new DlgPelayananLab(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnCari;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnRuangan;
    private widget.TextBox KdRuangan;
    private widget.TextBox NmRuangan;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TKd;
    private widget.Tanggal Tgl1;
    private widget.Tanggal Tgl2;
    private widget.InternalFrame internalFrame1;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label label11;
    private widget.Label label18;
    private widget.Label label34;
    private widget.panelisi panelGlass5;
    private widget.Table tbBangsal;
    // End of variables declaration//GEN-END:variables

    public void tampil(){        
        try{   
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)); 
            Valid.tabelKosong(tabMode);   
            ps=koneksi.prepareStatement(
                "SELECT p.no_rkm_medis,p.nm_pasien,d.nm_dokter,\n" +
                "    kamar_inap.stts_pulang,p2.png_jawab,kamar_inap.no_rawat,\n" +
                "    DATE_FORMAT(kamar_inap.tgl_masuk, '%d-%m-%Y') AS tgl_masuk,\n" +
                "    kamar_inap.jam_masuk,DATE_FORMAT(mr.tgl_masuk, '%d-%m-%Y') AS tgl_terima,\n" +
                "    mr.jam_masuk AS jam_terima,TIMESTAMPDIFF(MINUTE, CONCAT(kamar_inap.tgl_masuk, ' ', kamar_inap.jam_masuk), CONCAT(mr.tgl_masuk, ' ', mr.jam_masuk)) AS selisih,\n" +
                "    CONCAT(kamar_inap.kd_kamar, ' ', bangsal.nm_bangsal) AS kamar,bangsal.kd_bangsal,\n" +
                "    GROUP_CONCAT(d.nm_dokter SEPARATOR ', ') AS nama_dokter -- Menggabungkan nama dokter menjadi satu baris\n" +
                "FROM \n" +
                "    (SELECT \n" +
                "        kamar_inap.no_rawat,\n" +
                "        MIN(kamar_inap.tgl_masuk) AS tgl_masuk,\n" +
                "        MIN(kamar_inap.jam_masuk) AS jam_masuk\n" +
                "    FROM kamar_inap\n" +
                "    INNER JOIN reg_periksa rp ON kamar_inap.no_rawat = rp.no_rawat \n" +
                "    WHERE rp.tgl_registrasi BETWEEN ? AND ? \n" +
                "    GROUP BY kamar_inap.no_rawat) AS first_kamar_inap\n" +
                "INNER JOIN kamar_inap ON first_kamar_inap.no_rawat = kamar_inap.no_rawat AND first_kamar_inap.tgl_masuk = kamar_inap.tgl_masuk AND first_kamar_inap.jam_masuk = kamar_inap.jam_masuk\n" +
                "INNER JOIN reg_periksa rp ON kamar_inap.no_rawat = rp.no_rawat \n" +
                "INNER JOIN pasien p ON rp.no_rkm_medis = p.no_rkm_medis \n" +
                "INNER JOIN penjab p2 ON p.kd_pj = p2.kd_pj \n" +
                "INNER JOIN dpjp_ranap dr ON rp.no_rawat = dr.no_rawat \n" +
                "INNER JOIN dokter d ON dr.kd_dokter = d.kd_dokter \n" +
                "INNER JOIN masuk_ranap mr ON rp.no_rawat = mr.no_rawat \n" +
                "INNER JOIN kamar ON kamar_inap.kd_kamar = kamar.kd_kamar \n" +
                "INNER JOIN bangsal ON kamar.kd_bangsal = bangsal.kd_bangsal\n" +
                "WHERE p.nm_pasien LIKE ? AND (kamar_inap.stts_pulang <> 'APS' or kamar_inap.stts_pulang <> 'Atas Permintaan Sendiri')\n" +
                "GROUP BY kamar_inap.no_rawat");
            try {
                ps.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                ps.setString(3,"%"+TCari.getText().trim()+"%");
                rs=ps.executeQuery();
                i=1;
                kurang60=0;
                while(rs.next()){
                    if(!(rs.getString("stts_pulang").equalsIgnoreCase("Atas Permintaan Sendiri") || rs.getString("stts_pulang").equalsIgnoreCase("APS"))){
                        if(KdRuangan.getText().equals("")){
                            //JIKA FILTER RUANGAN KOSONG
                            tabMode.addRow(new Object[]{
                                i,rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),
                                rs.getString("kamar"),
                                rs.getString("no_rawat"),rs.getString("tgl_masuk")+" "+rs.getString("jam_masuk"),
                                rs.getString("tgl_terima")+" "+rs.getString("jam_terima"),rs.getString("selisih")
                            });
                            i++;        
                            if(rs.getDouble("selisih")<=60 && rs.getDouble("selisih")>=0){
                                kurang60++;
                            }                            
                        }else{
                            //JIKA FILTER RUANGAN DIISI
                            if(KdRuangan.getText().equals(rs.getString("kd_bangsal"))){
                                tabMode.addRow(new Object[]{
                                    i,rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("nm_dokter"),
                                    rs.getString("kamar"),
                                    rs.getString("no_rawat"),rs.getString("tgl_masuk")+" "+rs.getString("jam_masuk"),
                                    rs.getString("tgl_terima")+" "+rs.getString("jam_terima"),rs.getString("selisih")
                                });
                                i++;        
                                if(rs.getDouble("selisih")<=60 && rs.getDouble("selisih")>=0){
                                    kurang60++;
                                }                                
                            }
                        }                    
                    }
                }
                if(i>0){
                    tabMode.addRow(new Object[]{
                        "","","Waktu tunggu < 60 menit",": ","","","","",(int)kurang60
                    });
                    tabMode.addRow(new Object[]{
                        "","","Jumlah pasien ranap",": ","","","","",(i-1)
                    });
                    tabMode.addRow(new Object[]{
                        "","","Persentase waktu tunggu < 60 menit",": ","","","","",""+Valid.SetAngka6(kurang60/(i-1)*100)+" %"
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            this.setCursor(Cursor.getDefaultCursor());
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }

    private void getData() {
        int row=tbBangsal.getSelectedRow();
        if(row!= -1){
            TKd.setText(tabMode.getValueAt(row,0).toString());
        }
    }

}
