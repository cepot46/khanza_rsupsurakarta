/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DlgLhtBiaya.java
 *
 * Created on 12 Jul 10, 16:21:34
 */

package laporan;

import bridging.ApiKemenkesSITT;
import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariDokter;
import simrskhanza.DlgCariPoli;
import simrskhanza.DlgKabupaten;
import simrskhanza.DlgKecamatan;
import simrskhanza.DlgKelurahan;
import simrskhanza.DlgCariCaraBayar;
import bridging.DlgDataTB;
import bridging.YaskiReferensiKabupaten;
import bridging.YaskiReferensiKecamatan;
import bridging.YaskiReferensiKelurahan;
import bridging.YaskiReferensiPropinsi;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileReader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author perpustakaan
 */
public final class DlgDiagnosaTBC extends javax.swing.JDialog {
    private final DefaultTableModel tabMode,tabMode2;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps,ps2,psdiagnosapasien;
    private ResultSet rs,rs2;
    private DlgCariPoli poli=new DlgCariPoli(null,false);
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
//    private DlgKabupaten kabupaten=new DlgKabupaten(null,false);
//    private DlgKecamatan kecamatan=new DlgKecamatan(null,false);
//    private DlgKelurahan kelurahan=new DlgKelurahan(null,false);
    private DlgCariCaraBayar penjab=new DlgCariCaraBayar(null,false);
    private int i=0,lama=0,baru=0,laki=0,per=0,count=0;   
    private String setbaru="",setlama="",umurlk="",umurpr="",kddiangnosa="",diagnosa="",status="";
    private String perujuk="";  //CUSTOM MUHSIN
    private HttpHeaders headers ;
    private HttpEntity requestEntity;
    private RestTemplate rest;
    private ObjectMapper mapper = new ObjectMapper();
    private JsonNode root;
    private JsonNode nameNode;
    private JsonNode response;
    private ApiKemenkesSITT api=new ApiKemenkesSITT();
    private YaskiReferensiPropinsi propinsi=new YaskiReferensiPropinsi(null,false);
    private YaskiReferensiKabupaten kabupaten=new YaskiReferensiKabupaten(null,false);
    private YaskiReferensiKecamatan kecamatan=new YaskiReferensiKecamatan(null,false);
    private YaskiReferensiKelurahan kelurahan=new YaskiReferensiKelurahan(null,false);
    private DlgCariPenyakit penyakit=new DlgCariPenyakit(null,false);
    private String id_tb_03="",kdwasor="",idrs="",URL="",requestJson="";
    private FileReader myObj;
    private String obatpulang="",kd_prop = "",kd_kab = "",kd_kec = "",kd_kel = "",alamat = "",jk = "",no_ktp = "",tgl_lahir = "";   //CUSTOM MUHSIN
    /** Creates new form DlgLhtBiaya
     * @param parent
     * @param modal */
    public DlgDiagnosaTBC(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(885,674);

        tabMode=new DefaultTableModel(null,new Object[]{"No.","Tgl. Perawatan","No. Rawat","No. RM","Nama Pasien","Kode","Nama Penyakit","Status","Kasus","No. Urut Kode","Jenis Pasien","Penjamin","SEP",
        "NIK","JK","Tgl. Lahir / Umur","BB (kg)","Alamat","Tgl. Diagnosis","Asal Poli"}){  
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        table1.setModel(tabMode);
        //tbBangsal.setDefaultRenderer(Object.class, new WarnaTable(jPanel2.getBackground(),tbBangsal.getBackground()));
        table1.setPreferredScrollableViewportSize(new Dimension(500,500));
        table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 20; i++) {
            TableColumn column = table1.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(35);
            }else if(i==1){
                column.setPreferredWidth(80);
            }else if(i==2){
                column.setPreferredWidth(100);
            }else if(i==3){
                column.setPreferredWidth(50);
            }else if(i==4){
                column.setPreferredWidth(200);
            }else if(i==5){
                column.setPreferredWidth(70);
            }else if(i==6){
                column.setPreferredWidth(500);
            }else if(i==7){
                column.setPreferredWidth(70);
            }else if(i==8){
                column.setPreferredWidth(70);
            }else if(i==9){
                column.setPreferredWidth(70);
            }else if(i==10){
                column.setPreferredWidth(70);
            }else if(i==11){
                column.setPreferredWidth(70);
            }else if(i==12){
                column.setPreferredWidth(150);
            }else if(i==13){    //NIK
                column.setPreferredWidth(150);
            }else if(i==14){    //JK
                column.setPreferredWidth(50);
            }else if(i==15){    //TGL LAHIR
                column.setPreferredWidth(100);
            }else if(i==16){    //BB
                column.setPreferredWidth(50);
            }else if(i==17){    //ALAMAT
                column.setPreferredWidth(400);
            }else if(i==18){    //TGL DIAGNOSIS
                column.setPreferredWidth(100);
            }else if(i==19){    //ASAL POLI
                column.setPreferredWidth(120);
            }
        }
        table1.setDefaultRenderer(Object.class, new WarnaTable());
        
        tabMode2=new DefaultTableModel(null,new Object[]{"No.","Lama","Baru","Nama Pasien","L","P","Alamat","Kode","Diagnosa","Dokter","Poliklinik","Cara Bayar","Asal Rujukan"}){   //CUSTOM MUHSIN
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };

        TCari.setDocument(new batasInput((int)90).getKata(TCari));
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        try {
            kdwasor = koneksiDB.KABUPATENSITT();
            idrs=koneksiDB.IDSITT();
            URL = koneksiDB.URLAPISITT();
        } catch (Exception e) {
            System.out.println("E : "+e);
        }
        
        ChkInput.setSelected(false);
        isForm();
        
    }    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TKd = new widget.TextBox();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        ppSITB = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass5 = new widget.panelisi();
        label11 = new widget.Label();
        Tgl1 = new widget.Tanggal();
        label18 = new widget.Label();
        Tgl2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        jLabel7 = new widget.Label();
        BtnPrint = new widget.Button();
        BtnKeluar = new widget.Button();
        PanelInput = new javax.swing.JPanel();
        ChkInput = new widget.CekBox();
        FormInput = new widget.panelisi();
        label19 = new widget.Label();
        JenisPasien = new javax.swing.JComboBox<>();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        table1 = new widget.Table();

        TKd.setForeground(new java.awt.Color(255, 255, 255));
        TKd.setName("TKd"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        ppSITB.setBackground(new java.awt.Color(255, 255, 254));
        ppSITB.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppSITB.setForeground(java.awt.Color.darkGray);
        ppSITB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        ppSITB.setText("Teridentifikasi TB Kemenkes");
        ppSITB.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppSITB.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppSITB.setName("ppSITB"); // NOI18N
        ppSITB.setPreferredSize(new java.awt.Dimension(175, 25));
        ppSITB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppSITBBtnPrintActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppSITB);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Diagnosa TBC ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass5.setName("panelGlass5"); // NOI18N
        panelGlass5.setPreferredSize(new java.awt.Dimension(55, 55));
        panelGlass5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(50, 23));
        panelGlass5.add(label11);

        Tgl1.setDisplayFormat("dd-MM-yyyy");
        Tgl1.setName("Tgl1"); // NOI18N
        Tgl1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass5.add(Tgl1);

        label18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label18.setText("s.d.");
        label18.setName("label18"); // NOI18N
        label18.setPreferredSize(new java.awt.Dimension(25, 23));
        panelGlass5.add(label18);

        Tgl2.setDisplayFormat("dd-MM-yyyy");
        Tgl2.setName("Tgl2"); // NOI18N
        Tgl2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass5.add(Tgl2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass5.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(155, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass5.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('2');
        BtnCari.setToolTipText("Alt+2");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnAll);

        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(30, 23));
        panelGlass5.add(jLabel7);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/upload1-24.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Upload");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnPrint);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnKeluar);

        internalFrame1.add(panelGlass5, java.awt.BorderLayout.PAGE_END);

        PanelInput.setBackground(new java.awt.Color(255, 255, 255));
        PanelInput.setName("PanelInput"); // NOI18N
        PanelInput.setOpaque(false);
        PanelInput.setLayout(new java.awt.BorderLayout(1, 1));

        ChkInput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setMnemonic('M');
        ChkInput.setText(".: Filter Data");
        ChkInput.setBorderPainted(true);
        ChkInput.setBorderPaintedFlat(true);
        ChkInput.setFocusable(false);
        ChkInput.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ChkInput.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ChkInput.setName("ChkInput"); // NOI18N
        ChkInput.setPreferredSize(new java.awt.Dimension(192, 20));
        ChkInput.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkInputActionPerformed(evt);
            }
        });
        PanelInput.add(ChkInput, java.awt.BorderLayout.PAGE_END);

        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(100, 50));
        FormInput.setLayout(null);

        label19.setText("Jenis Pasien :");
        label19.setName("label19"); // NOI18N
        label19.setPreferredSize(new java.awt.Dimension(100, 23));
        FormInput.add(label19);
        label19.setBounds(0, 10, 75, 23);

        JenisPasien.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-", "Baru", "Lama" }));
        JenisPasien.setName("JenisPasien"); // NOI18N
        FormInput.add(JenisPasien);
        JenisPasien.setBounds(80, 10, 210, 20);

        PanelInput.add(FormInput, java.awt.BorderLayout.CENTER);

        internalFrame1.add(PanelInput, java.awt.BorderLayout.PAGE_START);

        TabRawat.setBackground(new java.awt.Color(255, 255, 254));
        TabRawat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(241, 246, 236)));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll.setComponentPopupMenu(jPopupMenu1);
        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);

        table1.setComponentPopupMenu(jPopupMenu1);
        table1.setName("table1"); // NOI18N
        Scroll.setViewportView(table1);

        internalFrame2.add(Scroll, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Terdiagnosa TBC", internalFrame2);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
            //TCari.requestFocus();
        }else if(tabMode.getRowCount()!=0){
            //LANGSUNG UPLOAD SEMUA
            for(i=0;i<tabMode.getRowCount();i++){
                sitbsemua(table1.getValueAt(i,1).toString(),table1.getValueAt(i,2).toString(),table1.getValueAt(i,3).toString(),table1.getValueAt(i,4).toString(),table1.getValueAt(i,5).toString(),table1.getValueAt(i,6).toString(),table1.getValueAt(i,8).toString());
            }
        }
        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            //Valid.pindah(evt, BtnHapus, BtnAll);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            dispose();
        }else{Valid.pindah(evt,BtnKeluar,TKd);}
}//GEN-LAST:event_BtnKeluarKeyPressed

private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
       if(TabRawat.getSelectedIndex()==0){
            tampil();
            //tampildiagnosa();
        }
}//GEN-LAST:event_BtnCariActionPerformed

private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)); 
            tampil();
            this.setCursor(Cursor.getDefaultCursor());
        }else{
            Valid.pindah(evt, TKd, BtnPrint);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
           TCari.setText("");
           JenisPasien.setSelectedIndex(0);
           status="";
           if(TabRawat.getSelectedIndex()==0){
                tampil();
            }
    }//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }
    }//GEN-LAST:event_BtnAllKeyPressed

    private void ChkInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkInputActionPerformed
        isForm();
    }//GEN-LAST:event_ChkInputActionPerformed

    private void ppSITBBtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppSITBBtnPrintActionPerformed
        if(table1.getValueAt(table1.getSelectedRow(),4).toString().isBlank()){
            JOptionPane.showMessageDialog(null,"Maaf, Silahkan anda pilih dulu pasien...!!!");
            table1.requestFocus();
        }else{
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            DlgDataTB resep=new DlgDataTB(null,false);
            resep.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            resep.setLocationRelativeTo(internalFrame1);
            resep.isCek();
            resep.emptTeks();
            resep.setNoRM(table1.getValueAt(table1.getSelectedRow(),2).toString());
            resep.setVisible(true);
            this.setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_ppSITBBtnPrintActionPerformed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==0){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            DlgDiagnosaTBC dialog = new DlgDiagnosaTBC(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnCari;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.CekBox ChkInput;
    private widget.panelisi FormInput;
    private javax.swing.JComboBox<String> JenisPasien;
    private javax.swing.JPanel PanelInput;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TKd;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal Tgl1;
    private widget.Tanggal Tgl2;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private javax.swing.JPopupMenu jPopupMenu1;
    private widget.Label label11;
    private widget.Label label18;
    private widget.Label label19;
    private widget.panelisi panelGlass5;
    private javax.swing.JMenuItem ppSITB;
    private widget.Table table1;
    // End of variables declaration//GEN-END:variables

    public void tampil(){        
        try{   
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)); 
            Valid.tabelKosong(tabMode);   
            if(JenisPasien.getSelectedItem().equals("-")){
                ps=koneksi.prepareStatement(
                    "select reg_periksa.tgl_registrasi,diagnosa_pasien.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,"+
                    "diagnosa_pasien.kd_penyakit,penyakit.nm_penyakit, diagnosa_pasien.status,diagnosa_pasien.status_penyakit,diagnosa_pasien.prioritas,reg_periksa.stts_daftar "+
                    ",penjab.png_jawab,pasien.no_ktp,pasien.jk,DATE_FORMAT(tgl_lahir,'%d-%m-%Y') AS tgl_lahir, "+
                    "poliklinik.nm_poli,concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab) as alamat,"+        
                    "reg_periksa.umurdaftar,reg_periksa.sttsumur "+
                    "from diagnosa_pasien inner join reg_periksa inner join pasien inner join penyakit "+
                    "on diagnosa_pasien.no_rawat=reg_periksa.no_rawat and reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "and diagnosa_pasien.kd_penyakit=penyakit.kd_penyakit "+
                    "inner join penjab on reg_periksa.kd_pj = penjab.kd_pj "+
                    "inner join poliklinik on reg_periksa.kd_poli = poliklinik.kd_poli "+ 
                    "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                    "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                    "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+        
                    "where diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and reg_periksa.tgl_registrasi like ? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and diagnosa_pasien.no_rawat like ? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and reg_periksa.no_rkm_medis like ? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and pasien.nm_pasien like ? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and penyakit.nm_penyakit like ? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and diagnosa_pasien.status_penyakit like ? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and diagnosa_pasien.status like ? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and penjab.png_jawab like ? "+
                    "order by reg_periksa.tgl_registrasi,diagnosa_pasien.prioritas ");
            }else{
                ps=koneksi.prepareStatement(
                    "select reg_periksa.tgl_registrasi,diagnosa_pasien.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,"+
                    "diagnosa_pasien.kd_penyakit,penyakit.nm_penyakit, diagnosa_pasien.status,diagnosa_pasien.status_penyakit,diagnosa_pasien.prioritas,reg_periksa.stts_daftar "+
                    ",penjab.png_jawab,pasien.no_ktp,pasien.jk,DATE_FORMAT(tgl_lahir,'%d-%m-%Y') AS tgl_lahir, "+
                    "poliklinik.nm_poli,concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab) as alamat "+        
                    "reg_periksa.umurdaftar,reg_periksa.sttsumur "+
                    "from diagnosa_pasien inner join reg_periksa inner join pasien inner join penyakit "+
                    "on diagnosa_pasien.no_rawat=reg_periksa.no_rawat and reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "and diagnosa_pasien.kd_penyakit=penyakit.kd_penyakit "+
                    "inner join penjab on reg_periksa.kd_pj = penjab.kd_pj "+
                    "inner join poliklinik on reg_periksa.kd_poli = poliklinik.kd_poli "+   
                    "inner join kelurahan on pasien.kd_kel=kelurahan.kd_kel "+
                    "inner join kecamatan on pasien.kd_kec=kecamatan.kd_kec "+
                    "inner join kabupaten on pasien.kd_kab=kabupaten.kd_kab "+         
                    "where diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and reg_periksa.tgl_registrasi like ? and reg_periksa.stts_daftar=? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and diagnosa_pasien.no_rawat like ? and reg_periksa.stts_daftar=? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and reg_periksa.no_rkm_medis like ? and reg_periksa.stts_daftar=? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and pasien.nm_pasien like ? and reg_periksa.stts_daftar=? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and penyakit.nm_penyakit like ? and reg_periksa.stts_daftar=? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and diagnosa_pasien.status_penyakit like ? and reg_periksa.stts_daftar=? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and diagnosa_pasien.status like ? and reg_periksa.stts_daftar=? or "+
                    "diagnosa_pasien.kd_penyakit like '%A1%' and reg_periksa.tgl_registrasi between ? and ? and penjab.png_jawab like ? and reg_periksa.stts_daftar=? "+
                    "order by reg_periksa.tgl_registrasi,diagnosa_pasien.prioritas ");
            }
            try {
                if(JenisPasien.getSelectedItem().equals("-")){
                    i=1;
                    ps.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(3,"%"+TCari.getText()+"%");   
                    ps.setString(4,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(5,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(6,"%"+TCari.getText()+"%");  
                    ps.setString(7,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(8,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(9,"%"+TCari.getText()+"%");  
                    ps.setString(10,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(11,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(12,"%"+TCari.getText()+"%");  
                    ps.setString(13,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(14,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(15,"%"+TCari.getText()+"%");  
                    ps.setString(16,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(17,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(18,"%"+TCari.getText()+"%");  
                    ps.setString(19,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(20,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(21,"%"+TCari.getText()+"%");
                    ps.setString(22,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(23,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(24,"%"+TCari.getText()+"%");
                }else{
                    i=1;
                    ps.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,JenisPasien.getSelectedItem().toString());
                    ps.setString(5,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(6,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(7,"%"+TCari.getText()+"%");  
                    ps.setString(8,JenisPasien.getSelectedItem().toString());
                    ps.setString(9,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(10,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(11,"%"+TCari.getText()+"%");  
                    ps.setString(12,JenisPasien.getSelectedItem().toString());
                    ps.setString(13,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(14,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(15,"%"+TCari.getText()+"%");  
                    ps.setString(16,JenisPasien.getSelectedItem().toString());
                    ps.setString(17,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(18,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(19,"%"+TCari.getText()+"%");  
                    ps.setString(20,JenisPasien.getSelectedItem().toString());
                    ps.setString(21,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(22,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(23,"%"+TCari.getText()+"%");  
                    ps.setString(24,JenisPasien.getSelectedItem().toString());
                    ps.setString(25,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(26,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(27,"%"+TCari.getText()+"%");
                    ps.setString(28,JenisPasien.getSelectedItem().toString());
                    ps.setString(29,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                    ps.setString(30,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                    ps.setString(31,"%"+TCari.getText()+"%");
                    ps.setString(32,JenisPasien.getSelectedItem().toString());
                }
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new Object[]{i,rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7),
                    rs.getString(8),
                    rs.getString(9),
                    rs.getString(10),
                    rs.getString(11),
                    Sequel.cariString("select no_sep from bridging_sep " +
                    " inner join reg_periksa on bridging_sep.no_rawat = reg_periksa.no_rawat " +
                    " inner join pasien p on reg_periksa.no_rkm_medis = p.no_rkm_medis " +
                    " where p.no_rkm_medis='"+rs.getString("no_rkm_medis")+"' and reg_periksa.no_rawat like '%"+rs.getString("no_rawat").substring(0, 9)+"%'"),
                    rs.getString("no_ktp"),
                    rs.getString("jk"),
                    rs.getString("tgl_lahir")+" / "+rs.getString("umurdaftar")+" "+rs.getString("sttsumur"),
                    Sequel.cariIsi("select bb from penilaian_awal_keperawatan_ralan where no_rawat='"+rs.getString("no_rawat")+"'"),
                    rs.getString("alamat"),
                    rs.getString("tgl_registrasi"),
                    rs.getString("nm_poli")
                    });
                    i++;
                }
            } catch (Exception e) {
                System.out.println("laporan.DlgKunjunganRalan.tampil() : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }       
            this.setCursor(Cursor.getDefaultCursor());
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
    }

    private void getData() {
        int row=table1.getSelectedRow();
        if(row!= -1){
            TKd.setText(tabMode.getValueAt(row,0).toString());
        }
    }
    
    private void isForm(){
        if(ChkInput.isSelected()==true){
            ChkInput.setVisible(false);
            PanelInput.setPreferredSize(new Dimension(WIDTH,71));
            FormInput.setVisible(true);      
            ChkInput.setVisible(true);
        }else if(ChkInput.isSelected()==false){           
            ChkInput.setVisible(false);            
            PanelInput.setPreferredSize(new Dimension(WIDTH,20));
            FormInput.setVisible(false);      
            ChkInput.setVisible(true);
        }
    }
    
    private void sitbsemua(String tgl_perawatan,String norawat,String norm,String pasien, String kode, String penyakit,String kasus){
        try{
            ps=koneksi.prepareStatement(
                "select detail_pemberian_obat.tgl_perawatan,detail_pemberian_obat.jam,databarang.nama_brng, detail_pemberian_obat.jml, "+
                "databarang.kode_sat from detail_pemberian_obat inner join databarang on detail_pemberian_obat.kode_brng=databarang.kode_brng "+
                "inner join golongan_barang gb on databarang.kode_golongan = gb.kode "+
                "where detail_pemberian_obat.no_rawat=? and gb.kode = 'G11' group by databarang.kode_brng");    //CUMAN OBAT OAT AJA
            try{
                ps.setString(1,norawat);
                obatpulang="";
                rs=ps.executeQuery();
                while(rs.next()){
                    if(rs.getString(3).equalsIgnoreCase("Administrasi Resep Obat Jadi") || rs.getString(3).equalsIgnoreCase("Administrasi Resep Obat Racikan") || rs.getString(3).equalsIgnoreCase("Obat Racikan")){
                        //NGGAK MASUK
                    }else{
                        obatpulang = obatpulang+rs.getString(3)+", ";
                    }
                }
            }catch(Exception ex){
                System.out.println(ex);
            }finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }  
        
        try{
            ps=koneksi.prepareStatement(
                "select * from pasien where no_rkm_medis=?");
            try{
                ps.setString(1,norm);
                kd_prop = "";
                kd_kab = "";
                kd_kec = "";
                kd_kel = "";
                alamat = "";
                jk = "";
                no_ktp = "";
                tgl_lahir = "";
                
                rs=ps.executeQuery();
                while(rs.next()){
                    kd_prop = rs.getString("kd_prop");
                    kd_kab = rs.getString("kd_kab");
                    kd_kec = rs.getString("kd_kec");
                    kd_kel = rs.getString("kd_kel");
                    alamat = rs.getString("alamat");
                    jk = rs.getString("jk");
                    no_ktp = rs.getString("no_ktp");
                    tgl_lahir = rs.getString("tgl_lahir"); 
                }
            }catch(Exception ex){
                System.out.println(ex);
            }finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }        
        //CEK APAKAH ADA DI DATA TB
        count=Sequel.cariInteger("select count(*) from data_tb inner join reg_periksa rp on data_tb.no_rawat = rp.no_rawat where rp.no_rkm_medis=?",norm);
        
        //CEK DAPAT OBAT OAT TIDAK
        if(!obatpulang.equals("") && count < 4){
            id_tb_03="";
            try {
                headers = new HttpHeaders();
                headers.add("X-rs-id",idrs);
                headers.add("X-Timestamp",String.valueOf(api.GetUTCdatetimeAsString())); 
                headers.add("X-pass",koneksiDB.PASSSITT()); 
                headers.add("Content-Type","application/json");
                requestJson ="{" +
                    "\"id_tb_03\":\"\"," +
                    "\"kd_pasien\":\""+pasien.toUpperCase()+"\"," +    
                    "\"nik\":\""+no_ktp+"\"," +
                    "\"jenis_kelamin\":\""+jk+"\"," +  
                    "\"alamat_lengkap\":\""+alamat+"\"," +
                    "\"id_propinsi_faskes\":\""+"33"+"\"," +
                    "\"kd_kabupaten_faskes\":\""+"3372"+"\"," +
                    "\"id_propinsi_pasien\":\""+kd_prop+"\"," +
                    "\"kd_kabupaten_pasien\":\""+kd_kab+"\"," +
                    "\"kd_fasyankes\":\""+idrs+"\"," +
                    "\"kode_icd_x\":\""+kode+"\"," +    
                    "\"tipe_diagnosis\":\""+"2"+"\"," +
                    "\"klasifikasi_lokasi_anatomi\":\""+""+"\"," +
                    "\"klasifikasi_riwayat_pengobatan\":\""+kasus.replaceAll("Baru","1").replaceAll("Kambuh","2").replaceAll("Diobati setelah gagal","3").replaceAll("Diobati Setelah Putus Berobat","4").replaceAll("Lama","5").replaceAll("Riwayat Pengobatan Sebelumnya Tidak Diketahui","6").replaceAll("Pindahan","7")+"\"," +
                    "\"tanggal_mulai_pengobatan\":\""+tgl_perawatan.replaceAll("-","")+"\"," +  
                    "\"paduan_oat\":\""+obatpulang+"\"," +
                    "\"sebelum_pengobatan_hasil_mikroskopis\":\""+"Tidak dilakukan"+"\"," +
                    "\"sebelum_pengobatan_hasil_tes_cepat\":\""+"Tidak dilakukan"+"\"," +
                    "\"sebelum_pengobatan_hasil_biakan\":\""+"Tidak dilakukan"+"\"," +
                    "\"hasil_mikroskopis_bulan_2\":\""+""+"\"," +
                    "\"hasil_mikroskopis_bulan_3\":\""+""+"\"," +
                    "\"hasil_mikroskopis_bulan_5\":\""+""+"\"," +
                    "\"akhir_pengobatan_hasil_mikroskopis\":\""+""+"\"," +
                    "\"tanggal_hasil_akhir_pengobatan\":\""+tgl_perawatan.replaceAll("-","")+"\"," +
                    "\"hasil_akhir_pengobatan\":\""+""+"\"," +
                    "\"tgl_lahir\":\""+tgl_lahir.replaceAll("-","")+"\"," + 
                    "\"foto_toraks\":\""+"Tidak dilakukan"+"\"" +
                "}";
                System.out.println(requestJson);
                requestEntity = new HttpEntity(requestJson,headers);
                requestJson=api.getRest().exchange(URL, HttpMethod.POST, requestEntity, String.class).getBody();
                System.out.println(requestJson);
                root = mapper.readTree(requestJson);
                id_tb_03=root.path("id_tb_03").asText();
            } catch (Exception ex) {
                System.out.println("Notifikasi Bridging : "+ex);
                if(ex.toString().contains("UnknownHostException")){
                    JOptionPane.showMessageDialog(null,"Koneksi ke server SITB terputus...!");
                }else if(ex.toString().contains("502")){
                    JOptionPane.showMessageDialog(null,"Connection timed out. Hayati lelah bang...!");
                }
            }
        
            if(Sequel.menyimpantf("data_tb","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Data",50,new String[]{
                norawat,id_tb_03,"1=Januari - Maret",tgl_perawatan, 
                tgl_perawatan.substring(0,4),kdwasor,"0",kd_prop,kd_kab,kd_kec,kd_kel,"Inisiatif pasien/Keluarga",
                "","Terdiagnosis klinis","Paru",kasus.replaceAll("Baru","Baru").toString().replaceAll("2. Kambuh","Kambuh").toString().replaceAll("3. Diobati setelah gagal","Diobati setelah gagal").toString().replaceAll("4. Diobati Setelah Putus Berobat","Diobati Setelah Putus Berobat").toString().replaceAll("Lama","Lain-lain").toString().replaceAll("6. Riwayat Pengobatan Sebelumnya Tidak Diketahui","Riwayat Pengobatan Sebelumnya Tidak Diketahui").toString().replaceAll("7. Pindahan","Pindahan"),
                "Tidak diketahui","1","Uji Tuberkulin Positif","Ada Kontak TB Paru",tgl_perawatan, 
                obatpulang,"Program TB","","Tidak dilakukan","Tidak dilakukan",
                "Tidak dilakukan","","Tidak dilakukan", 
                "","Tidak dilakukan","",
                "Tidak dilakukan","","Tidak dilakukan",
                tgl_perawatan,"Sembuh",tgl_perawatan, 
                tgl_perawatan,"Non Reaktif","Ya","Ya",
                "Ya","OHO","Tidak","Sesuai Standar","Tidak dilakukan",
                "Tidak dilakukan","",kode
            })==true){
                System.out.println("Simpan data tb pasien "+pasien+"/"+norawat+" berhasil");
            }else{
                System.out.println("Gagal simpan data tb pasien "+pasien+"/"+norawat);
            }            
        }
    }
}
