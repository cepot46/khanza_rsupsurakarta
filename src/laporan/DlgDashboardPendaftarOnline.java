/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DlgLhtBiaya.java
 *
 * Created on 12 Jul 10, 16:21:34
 */

package laporan;

import fungsi.WarnaTable;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariDokter;
import simrskhanza.DlgCariPoli;
import simrskhanza.DlgKabupaten;
import simrskhanza.DlgCariCaraBayar;

/**
 *
 * @author perpustakaan
 */
public final class DlgDashboardPendaftarOnline extends javax.swing.JDialog {
    private DefaultTableModel tabmode,tabmode2;
    private final Connection koneksi=koneksiDB.condb();
    private final sekuel Sequel=new sekuel();
    private final validasi Valid=new validasi();
    private PreparedStatement ps,ps2,psbpjs,psbooking,psbookingonlineapm,psbookingonlinefo,psmjkn;
    private DlgCariPoli poli=new DlgCariPoli(null,false);
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgKabupaten kabupaten=new DlgKabupaten(null,false);
    private DlgCariCaraBayar penjab=new DlgCariCaraBayar(null,false);
    private ResultSet rs,rs2,rsmjkn,rsbooking_online,rsbooking_online_onsite_apm,rsbooking_online_onsite_fo,rsbpjs;
    private int i=0,jkl=0,jkp=0,rujukan=0,pengunjungbaru=0,pengunjunglama=0,statuspolibaru=0,statuspolilama=0;
    private double jum_pasien=0,jum_booking=0,jum_tidak_booking=0,jum_booking_apm=0,jum_booking_fo=0,jum_booking_mjkn=0,jum_booking_onlinemjkn=0;
    private String diagnosautama="",diagnosasekunder="",perujuk="",status="";
    private double per_booking=0,per_tidak_booking=0,per_apm=0,per_fo=0,per_onlinemjkn=0,per_mjkn=0,per_non_offline=0,per_with_offline=0;
    /** Creates new form DlgLhtBiaya
     * @param parent
     * @param modal */
    public DlgDashboardPendaftarOnline(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(885,674);
        
        tabmode=new DefaultTableModel(null,new String[]{ // DESIGN TABEL TAB MODE 1
                "No.","Nomor Rawat","Nomor RM","Nama Pasien","Tgl.Daftar","Tgl.Booking","Poliklinik","Dokter","Cara Bayar","Cara Daftar",
                "Waktu Regis","Waktu Askep","Waktu Poli","Waktu Kasir","Umur","J.K.","Pekerjaan","Pendidikan"
            }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        
        table.setModel(tabmode);
        table.setPreferredScrollableViewportSize(new Dimension(500,500));
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 17; i++) {
            TableColumn column = table.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(40);
            }else if(i==1){
                column.setPreferredWidth(230);
            }else if(i==2){
                column.setPreferredWidth(70);
            }else if(i==3){
                column.setPreferredWidth(240);
            }else if(i==4){
                column.setPreferredWidth(70);
            }else if(i==5){
                column.setPreferredWidth(70);
            }else if(i==6){
                column.setPreferredWidth(160);
            }else if(i==7){
                column.setPreferredWidth(240);
            }else if(i==8){
                column.setPreferredWidth(60);
            }else if(i==9){
                column.setPreferredWidth(150);
            }else {
                column.setPreferredWidth(120);
            }
        }
        table.setDefaultRenderer(Object.class, new WarnaTable());
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        if(TabRawat.getSelectedIndex()==0){
                            tampil();
                        }
                    }                        
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        if(TabRawat.getSelectedIndex()==0){
                            tampil();
                        }
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        if(TabRawat.getSelectedIndex()==0){
                            tampil();
                        }
                    }
                }
            });
        }  
        
        poli.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(poli.getTable().getSelectedRow()!= -1){
                    kdpoli.setText(poli.getTable().getValueAt(poli.getTable().getSelectedRow(),0).toString());
                    nmpoli.setText(poli.getTable().getValueAt(poli.getTable().getSelectedRow(),1).toString());
                }      
                kdpoli.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {poli.emptTeks();}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });   
        
        penjab.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(penjab.getTable().getSelectedRow()!= -1){
                    kdpenjab.setText(penjab.getTable().getValueAt(penjab.getTable().getSelectedRow(),1).toString());
                    nmpenjab.setText(penjab.getTable().getValueAt(penjab.getTable().getSelectedRow(),2).toString());
                }      
                kdpenjab.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {penjab.emptTeks();}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });   
        
        penjab.getTable().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_SPACE){
                    penjab.dispose();
                }
            }
            @Override
            public void keyReleased(KeyEvent e) {}
        });
        
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){
                    kddokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    nmdokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                }      
                kddokter.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {dokter.emptTeks();}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });   
        
        dokter.getTable().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_SPACE){
                    dokter.dispose();
                }
            }
            @Override
            public void keyReleased(KeyEvent e) {}
        });
        
        
    }    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TKd = new widget.TextBox();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        ppTampilkanBaru = new javax.swing.JMenuItem();
        ppTampilkanLama = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass5 = new widget.panelisi();
        label11 = new widget.Label();
        Tgl1 = new widget.Tanggal();
        label18 = new widget.Label();
        Tgl2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        jLabel8 = new widget.Label();
        BtnPrint = new widget.Button();
        BtnKeluar = new widget.Button();
        panelisi4 = new widget.panelisi();
        label17 = new widget.Label();
        kdpoli = new widget.TextBox();
        nmpoli = new widget.TextBox();
        BtnSeek2 = new widget.Button();
        label19 = new widget.Label();
        kdpenjab = new widget.TextBox();
        nmpenjab = new widget.TextBox();
        BtnSeek3 = new widget.Button();
        label20 = new widget.Label();
        kddokter = new widget.TextBox();
        nmdokter = new widget.TextBox();
        BtnSeek4 = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        table = new widget.Table();

        TKd.setForeground(new java.awt.Color(255, 255, 255));
        TKd.setName("TKd"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        ppTampilkanBaru.setBackground(new java.awt.Color(255, 255, 254));
        ppTampilkanBaru.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppTampilkanBaru.setForeground(java.awt.Color.darkGray);
        ppTampilkanBaru.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        ppTampilkanBaru.setText("Tampilkan Pasien Baru");
        ppTampilkanBaru.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppTampilkanBaru.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppTampilkanBaru.setName("ppTampilkanBaru"); // NOI18N
        ppTampilkanBaru.setPreferredSize(new java.awt.Dimension(175, 25));
        ppTampilkanBaru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppTampilkanBaruBtnPrintActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppTampilkanBaru);

        ppTampilkanLama.setBackground(new java.awt.Color(255, 255, 254));
        ppTampilkanLama.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        ppTampilkanLama.setForeground(java.awt.Color.darkGray);
        ppTampilkanLama.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        ppTampilkanLama.setText("Tampilkan Pasien Lama");
        ppTampilkanLama.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ppTampilkanLama.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ppTampilkanLama.setName("ppTampilkanLama"); // NOI18N
        ppTampilkanLama.setPreferredSize(new java.awt.Dimension(175, 25));
        ppTampilkanLama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ppTampilkanLamaBtnPrintActionPerformed(evt);
            }
        });
        jPopupMenu1.add(ppTampilkanLama);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Dashboard Pendaftar Online ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 12), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass5.setName("panelGlass5"); // NOI18N
        panelGlass5.setPreferredSize(new java.awt.Dimension(55, 55));
        panelGlass5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(50, 23));
        panelGlass5.add(label11);

        Tgl1.setDisplayFormat("dd-MM-yyyy");
        Tgl1.setName("Tgl1"); // NOI18N
        Tgl1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass5.add(Tgl1);

        label18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label18.setText("s.d.");
        label18.setName("label18"); // NOI18N
        label18.setPreferredSize(new java.awt.Dimension(25, 23));
        panelGlass5.add(label18);

        Tgl2.setDisplayFormat("dd-MM-yyyy");
        Tgl2.setName("Tgl2"); // NOI18N
        Tgl2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass5.add(Tgl2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass5.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(155, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass5.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('2');
        BtnCari.setToolTipText("Alt+2");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnAll);

        jLabel8.setName("jLabel8"); // NOI18N
        jLabel8.setPreferredSize(new java.awt.Dimension(30, 23));
        panelGlass5.add(jLabel8);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnPrint);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass5.add(BtnKeluar);

        internalFrame1.add(panelGlass5, java.awt.BorderLayout.PAGE_END);

        panelisi4.setName("panelisi4"); // NOI18N
        panelisi4.setPreferredSize(new java.awt.Dimension(100, 74));
        panelisi4.setLayout(null);

        label17.setText("Poli :");
        label17.setName("label17"); // NOI18N
        label17.setPreferredSize(new java.awt.Dimension(35, 23));
        panelisi4.add(label17);
        label17.setBounds(6, 10, 45, 23);

        kdpoli.setEditable(false);
        kdpoli.setName("kdpoli"); // NOI18N
        kdpoli.setPreferredSize(new java.awt.Dimension(75, 23));
        kdpoli.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kdpoliKeyPressed(evt);
            }
        });
        panelisi4.add(kdpoli);
        kdpoli.setBounds(56, 10, 75, 23);

        nmpoli.setEditable(false);
        nmpoli.setName("nmpoli"); // NOI18N
        nmpoli.setPreferredSize(new java.awt.Dimension(215, 23));
        panelisi4.add(nmpoli);
        nmpoli.setBounds(136, 10, 215, 23);

        BtnSeek2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeek2.setMnemonic('3');
        BtnSeek2.setToolTipText("Alt+3");
        BtnSeek2.setName("BtnSeek2"); // NOI18N
        BtnSeek2.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeek2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeek2ActionPerformed(evt);
            }
        });
        BtnSeek2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSeek2KeyPressed(evt);
            }
        });
        panelisi4.add(BtnSeek2);
        BtnSeek2.setBounds(356, 10, 28, 23);

        label19.setText("Cara Bayar :");
        label19.setName("label19"); // NOI18N
        label19.setPreferredSize(new java.awt.Dimension(100, 23));
        panelisi4.add(label19);
        label19.setBounds(379, 10, 100, 23);

        kdpenjab.setEditable(false);
        kdpenjab.setName("kdpenjab"); // NOI18N
        kdpenjab.setPreferredSize(new java.awt.Dimension(75, 23));
        kdpenjab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kdpenjabKeyPressed(evt);
            }
        });
        panelisi4.add(kdpenjab);
        kdpenjab.setBounds(484, 10, 75, 23);

        nmpenjab.setEditable(false);
        nmpenjab.setName("nmpenjab"); // NOI18N
        nmpenjab.setPreferredSize(new java.awt.Dimension(215, 23));
        panelisi4.add(nmpenjab);
        nmpenjab.setBounds(564, 10, 215, 23);

        BtnSeek3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeek3.setMnemonic('3');
        BtnSeek3.setToolTipText("Alt+3");
        BtnSeek3.setName("BtnSeek3"); // NOI18N
        BtnSeek3.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeek3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeek3ActionPerformed(evt);
            }
        });
        BtnSeek3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSeek3KeyPressed(evt);
            }
        });
        panelisi4.add(BtnSeek3);
        BtnSeek3.setBounds(784, 10, 28, 23);

        label20.setText("Dokter :");
        label20.setName("label20"); // NOI18N
        label20.setPreferredSize(new java.awt.Dimension(35, 23));
        panelisi4.add(label20);
        label20.setBounds(6, 40, 45, 23);

        kddokter.setEditable(false);
        kddokter.setName("kddokter"); // NOI18N
        kddokter.setPreferredSize(new java.awt.Dimension(75, 23));
        kddokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kddokterKeyPressed(evt);
            }
        });
        panelisi4.add(kddokter);
        kddokter.setBounds(56, 40, 75, 23);

        nmdokter.setEditable(false);
        nmdokter.setName("nmdokter"); // NOI18N
        nmdokter.setPreferredSize(new java.awt.Dimension(215, 23));
        panelisi4.add(nmdokter);
        nmdokter.setBounds(136, 40, 215, 23);

        BtnSeek4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnSeek4.setMnemonic('3');
        BtnSeek4.setToolTipText("Alt+3");
        BtnSeek4.setName("BtnSeek4"); // NOI18N
        BtnSeek4.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnSeek4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSeek4ActionPerformed(evt);
            }
        });
        BtnSeek4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSeek4KeyPressed(evt);
            }
        });
        panelisi4.add(BtnSeek4);
        BtnSeek4.setBounds(356, 40, 28, 23);

        internalFrame1.add(panelisi4, java.awt.BorderLayout.PAGE_START);

        TabRawat.setBackground(new java.awt.Color(255, 255, 254));
        TabRawat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(241, 246, 236)));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11));
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBackground(new java.awt.Color(235, 255, 235));
        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        Scroll.setComponentPopupMenu(jPopupMenu1);
        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);

        table.setComponentPopupMenu(jPopupMenu1);
        table.setName("table"); // NOI18N
        Scroll.setViewportView(table);

        internalFrame2.add(Scroll, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("DATA PENDAFTARAN PASIEN", internalFrame2);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);
        TabRawat.getAccessibleContext().setAccessibleName("");

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
                    Map<String, Object> param = new HashMap<>();    
                                 param.put("namars",akses.getnamars());
                                 param.put("alamatrs",akses.getalamatrs());
                                 param.put("kotars",akses.getkabupatenrs());
                                 param.put("propinsirs",akses.getpropinsirs());
                                 param.put("kontakrs",akses.getkontakrs());
                                 param.put("emailrs",akses.getemailrs());   
                                 param.put("logo",Sequel.cariGambar("select logo_rsup from gambar_tambahan"));
                                 param.put("logo_kemenkes",Sequel.cariGambar("select logo_kemenkes_baru from gambar_tambahan"));
                                 param.put("logo_akreditasi",Sequel.cariGambar("select logo_akreditasi from gambar_tambahan"));
                                 param.put("logo_blu",Sequel.cariGambar("select logo_blu from gambar_tambahan"));
                                 param.put("icon_maps",Sequel.cariGambar("select icon_maps from gambar_tambahan"));
                                 param.put("icon_telpon",Sequel.cariGambar("select icon_telpon from gambar_tambahan"));
                                 param.put("icon_website",Sequel.cariGambar("select icon_website from gambar_tambahan"));
                                 String tgl_mulai = Valid.SetTgl(Tgl1.getSelectedItem()+"");
                                 String tgl_selesai = Valid.SetTgl(Tgl1.getSelectedItem()+"");
                                 param.put("tgl_mulai",tgl_mulai);
                                 param.put("tgl_selesai",tgl_selesai);
                                 
                                 // AMBIL DATA PERSENTASE
                                 
                                 try {
                                    if(TCari.getText().toString().trim().equals("")){
                                        ps=koneksi.prepareStatement(
                                        "select reg_periksa.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,reg_periksa.tgl_registrasi,ifnull(booking_registrasi.tanggal_periksa,'') as tanggal_periksa,reg_periksa.kd_poli,poliklinik.nm_poli,reg_periksa.kd_dokter,dokter.nm_dokter,penjab.png_jawab,COALESCE(CONCAT(CASE when booking_registrasi.limit_reg='1' then 'Booking Online' else '' end,CASE when booking_registrasi.limit_reg='0' then 'Booking Offline' else '' end, Case when referensi_mobilejkn_bpjs.no_rawat IS NOT NULL and referensi_mobilejkn_bpjs.tanggalperiksa=reg_periksa.tgl_registrasi then 'MJKN' else '' end),'Onsite') AS cara_daftar  " +
                                        ",reg_periksa.jam_reg, "+
                                        "(SELECT DATE_FORMAT(tanggal,'%H:%i:%s')FROM penilaian_awal_keperawatan_ralan WHERE penilaian_awal_keperawatan_ralan.no_rawat = reg_periksa.no_rawat ) AS waktu_askep," +
                                        "(SELECT jam_rawat FROM pemeriksaan_ralan WHERE pemeriksaan_ralan.no_rawat = reg_periksa.no_rawat ORDER BY tgl_perawatan ASC, jam_rawat ASC LIMIT 1) AS jam_rawat," +
                                        "(SELECT jam FROM nota_jalan WHERE nota_jalan.no_rawat = reg_periksa.no_rawat ) AS jam_kasir "+ 
                                        "from reg_periksa left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis " +
                                        "inner join dokter on dokter.kd_dokter = reg_periksa.kd_dokter " +
                                        "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli " +
                                        "inner join penjab on reg_periksa.kd_pj = penjab.kd_pj "+
                                        "left join booking_registrasi on booking_registrasi.no_rkm_medis = reg_periksa.no_rkm_medis and booking_registrasi.tanggal_periksa = reg_periksa.tgl_registrasi " +
                                        "left join referensi_mobilejkn_bpjs on referensi_mobilejkn_bpjs.no_rawat =reg_periksa.no_rawat "+
                                        "where reg_periksa.tgl_registrasi between ? and ? and reg_periksa.stts !='Batal' AND reg_periksa.kd_poli NOT IN ('IGDK', 'LAB', 'U0040', 'U0043', 'MCU', 'KGZ', 'KTB', 'KPITC', 'KBM','U0041','RAD','PAK','U0043','U0044','U0045','U0046','U0047','ASMA') " +
                                        "order by reg_periksa.no_rawat ASC ");
                                    } else {
                                        ps=koneksi.prepareStatement(
                                        "select reg_periksa.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,reg_periksa.tgl_registrasi,ifnull(booking_registrasi.tanggal_periksa,'') as tanggal_periksa,reg_periksa.kd_poli,poliklinik.nm_poli,reg_periksa.kd_dokter,dokter.nm_dokter,penjab.png_jawab,COALESCE(CONCAT(CASE when booking_registrasi.limit_reg='1' then 'Booking Online' else '' end,CASE when booking_registrasi.limit_reg='0' then 'Booking Offline' else '' end, Case when referensi_mobilejkn_bpjs.no_rawat IS NOT NULL and referensi_mobilejkn_bpjs.tanggalperiksa=reg_periksa.tgl_registrasi then 'MJKN' else '' end),'Onsite') AS cara_daftar  " +
                                        ",reg_periksa.jam_reg, "+
                                        "(SELECT DATE_FORMAT(tanggal,'%H:%i:%s')FROM penilaian_awal_keperawatan_ralan WHERE penilaian_awal_keperawatan_ralan.no_rawat = reg_periksa.no_rawat ) AS waktu_askep," +
                                        "(SELECT jam_rawat FROM pemeriksaan_ralan WHERE pemeriksaan_ralan.no_rawat = reg_periksa.no_rawat ORDER BY tgl_perawatan ASC, jam_rawat ASC LIMIT 1) AS jam_rawat," +
                                        "(SELECT jam FROM nota_jalan WHERE nota_jalan.no_rawat = reg_periksa.no_rawat ) AS jam_kasir "+        
                                        "from reg_periksa left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis " +
                                        "inner join dokter on dokter.kd_dokter = reg_periksa.kd_dokter " +
                                        "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli " +
                                        "inner join penjab on reg_periksa.kd_pj = penjab.kd_pj " +
                                        "left join booking_registrasi on booking_registrasi.no_rkm_medis = reg_periksa.no_rkm_medis and booking_registrasi.tanggal_periksa = reg_periksa.tgl_registrasi " +
                                        "left join referensi_mobilejkn_bpjs on referensi_mobilejkn_bpjs.no_rawat =reg_periksa.no_rawat " +
                                        "where reg_periksa.tgl_registrasi between ? and ?  and reg_periksa.stts !='Batal' AND reg_periksa.kd_poli NOT IN ('IGDK', 'LAB', 'U0040', 'U0043', 'MCU', 'KGZ', 'KTB', 'KPITC', 'KBM','U0041') and " +
                                        "(pasien.nm_pasien like ? or poliklinik.nm_poli like ? or reg_periksa.no_rawat like ? or reg_periksa.no_rkm_medis like ? ) "+
                                        "order by reg_periksa.no_rawat ASC");
                                    }

                                    try {
                                        if(TCari.getText().trim().equals("")){
                                        ps.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                                        ps.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                                        } else {
                                        ps.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                                        ps.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                                        ps.setString(3,"%"+TCari.getText().trim()+"%");
                                        ps.setString(4,"%"+TCari.getText().trim()+"%");
                                        ps.setString(5,"%"+TCari.getText().trim()+"%");
                                        ps.setString(6,"%"+TCari.getText().trim()+"%");
                                        }
                                        rs=ps.executeQuery();

                                        i=1;jum_pasien=0;jum_booking_onlinemjkn=0;
                                        jum_booking=0;jum_tidak_booking=0;jum_booking_apm=0;jum_booking_fo=0;jum_booking_mjkn=0;
                                        while(rs.next()){
                                            if(rs.getString("cara_daftar").equals("MJKN")){
                                                jum_booking_mjkn++;
                                            }if(rs.getString("cara_daftar").equals("Booking Online")) {
                                                jum_booking_apm++;
                                            }if(rs.getString("cara_daftar").equals("Booking Offline")) {
                                                jum_booking_fo++;
                                            }if(rs.getString("cara_daftar").isEmpty()) {
                                                jum_tidak_booking++;
                                            }if( !(rs.getString("cara_daftar").isEmpty())) {
                                                jum_booking++;
                                            }if(rs.getString("cara_daftar").equals("Booking OnlineMJKN")) {
                                                jum_booking_mjkn++;
                                            }if(rs.getString("cara_daftar").equals("Booking OfflineMJKN")) {
                                                jum_booking_mjkn++;
                                            }
                                            i++;
                                            jum_pasien++;
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Notif : "+e);
                                    } finally{
                                        if(rs!=null){
                                            rs.close();
                                        }
                                        if(ps!=null){
                                            ps.close();
                                        }
                                    }
                                    if(rs!=null){
                                        per_tidak_booking=0;
                                        per_booking=0;
                                        per_apm=0;
                                        per_fo=0;
                                        per_mjkn=0;
                                        per_onlinemjkn=0;
                                        per_non_offline=0;
                                        per_with_offline=0;
                                        per_booking = (jum_booking/jum_pasien)*100 ;
                                        per_tidak_booking = (jum_tidak_booking/jum_pasien)*100 ;
                                        per_apm =(jum_booking_apm/jum_pasien)*100 ;
                                        per_fo = (jum_booking_fo/jum_pasien)*100 ;
                                        per_mjkn = (jum_booking_mjkn/jum_pasien)*100;
                                        per_with_offline = (jum_booking_mjkn+jum_booking_onlinemjkn+jum_booking_apm+jum_booking_fo)/jum_pasien*100 ;
                                        per_non_offline = (jum_booking_mjkn+jum_booking_onlinemjkn+jum_booking_apm)/jum_pasien*100 ;

                                    }            
                                } catch (Exception e) {
                                    System.out.println("Notif : "+e);
                                } 
                                 
                                 // SELESAI AMBIL DATA PERSENTASE
                                 param.put("per_booking",Valid.SetAngka6(per_booking));
                                 param.put("per_tidak_booking",Valid.SetAngka6(per_tidak_booking));
                                 param.put("per_apm",Valid.SetAngka6(per_apm));
                                 param.put("per_fo",Valid.SetAngka6(per_fo));
                                 param.put("per_mjkn",Valid.SetAngka6(per_mjkn));
                                 param.put("per_with_offline",Valid.SetAngka6(per_with_offline));
                                 param.put("per_non_offline",Valid.SetAngka2(per_non_offline));
                                 param.put("jum_booking",Valid.SetAngka2(jum_booking));
                                 param.put("jum_tidak_booking",Valid.SetAngka2(jum_tidak_booking));
                                 param.put("jum_pasien",Valid.SetAngka2(jum_pasien));
                                 param.put("jum_booking_apm",Valid.SetAngka2(jum_booking_apm));
                                 param.put("jum_booking_fo",Valid.SetAngka2(jum_booking_fo));
                                 param.put("jum_booking_mjkn",Valid.SetAngka2(jum_booking_mjkn));
                                 this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                                 Valid.MyReportqry("rptCetakDashboardDaftarOnline.jasper","report","::[ Data Pendaftar Online ]::", 
                                    "select reg_periksa.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,reg_periksa.tgl_registrasi,ifnull(booking_registrasi.tanggal_periksa,'') as tanggal_periksa,reg_periksa.kd_poli,poliklinik.nm_poli,reg_periksa.kd_dokter,dokter.nm_dokter,penjab.png_jawab,COALESCE(CONCAT(CASE when booking_registrasi.limit_reg='1' then 'Booking Online' else '' end,CASE when booking_registrasi.limit_reg='0' then 'Booking Offline' else '' end, Case when referensi_mobilejkn_bpjs.no_rawat IS NOT NULL and referensi_mobilejkn_bpjs.tanggalperiksa=reg_periksa.tgl_registrasi then 'MJKN' else '' end),'Onsite') AS cara_daftar  " +
                                    ",reg_periksa.jam_reg as jam_masuk,pasien.jk,CONCAT( pasien.alamat,'', kelurahan.nm_kel,',', kecamatan.nm_kec,',', kabupaten.nm_kab) AS alamat, "+
                                    "(SELECT DATE_FORMAT(tanggal,'%H:%i:%s')FROM penilaian_awal_keperawatan_ralan WHERE penilaian_awal_keperawatan_ralan.no_rawat = reg_periksa.no_rawat ) AS waktu_askep," +
                                    "(SELECT jam_rawat FROM pemeriksaan_ralan WHERE pemeriksaan_ralan.no_rawat = reg_periksa.no_rawat ORDER BY tgl_perawatan ASC, jam_rawat ASC LIMIT 1) AS jam_rawat," +
                                    "(SELECT jam FROM nota_jalan WHERE nota_jalan.no_rawat = reg_periksa.no_rawat ) AS jam_kasir "+ 
                                    "from reg_periksa left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis " +
                                    "inner join dokter on dokter.kd_dokter = reg_periksa.kd_dokter " +
                                    "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli " +
                                    "inner join penjab on reg_periksa.kd_pj = penjab.kd_pj "+
                                    "left join kelurahan on kelurahan.kd_kel = pasien.kd_kel "+
                                    "left join kecamatan on kecamatan.kd_kec = pasien.kd_kec "+
                                    "left join kabupaten on kabupaten.kd_kab = pasien.kd_kab "+
                                    "left join booking_registrasi on booking_registrasi.no_rkm_medis = reg_periksa.no_rkm_medis and booking_registrasi.tanggal_periksa = reg_periksa.tgl_registrasi " +
                                    "left join referensi_mobilejkn_bpjs on referensi_mobilejkn_bpjs.no_rawat =reg_periksa.no_rawat "+
                                    "where reg_periksa.tgl_registrasi between '"+Valid.SetTgl(Tgl1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(Tgl2.getSelectedItem()+"")+"' and reg_periksa.stts !='Batal' AND reg_periksa.kd_poli NOT IN ('IGDK', 'LAB', 'U0040', 'U0043', 'MCU', 'KGZ', 'KTB', 'KPITC', 'KBM','U0041','RAD','PAK','U0043','U0044','U0045','U0046','U0047','ASMA') " +
                                    "order by reg_periksa.tgl_registrasi,reg_periksa.no_rawat ASC ",param);
                                 this.setCursor(Cursor.getDefaultCursor());  
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            dispose();
        }else{Valid.pindah(evt,BtnKeluar,TKd);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void kdpoliKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kdpoliKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            Sequel.cariIsi("select nm_poli from poliklinik where kd_poli=?", nmpoli,kdpoli.getText());
        }else if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            Sequel.cariIsi("select nm_poli from poliklinik where kd_poli=?", nmpoli,kdpoli.getText());
            BtnAll.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            Sequel.cariIsi("select nm_poli from poliklinik where kd_poli=?", nmpoli,kdpoli.getText());
            Tgl2.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
            BtnSeek2ActionPerformed(null);
        }
    }//GEN-LAST:event_kdpoliKeyPressed

    private void BtnSeek2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeek2ActionPerformed
        poli.isCek();
        poli.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        poli.setLocationRelativeTo(internalFrame1);
        poli.setAlwaysOnTop(false);
        poli.setVisible(true);
    }//GEN-LAST:event_BtnSeek2ActionPerformed

    private void BtnSeek2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSeek2KeyPressed
        //Valid.pindah(evt,DTPCari2,TCari);
    }//GEN-LAST:event_BtnSeek2KeyPressed

    private void kdpenjabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kdpenjabKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            Sequel.cariIsi("select png_jawab from penjab where kd_pj=?", nmpenjab,kdpenjab.getText());
        }else if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            Sequel.cariIsi("select png_jawab from penjab where kd_pj=?", nmpenjab,kdpenjab.getText());
            BtnAll.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            Sequel.cariIsi("select png_jawab from penjab where kd_pj=?", nmpenjab,kdpenjab.getText());
            Tgl2.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
            BtnSeek2ActionPerformed(null);
        }
    }//GEN-LAST:event_kdpenjabKeyPressed

    private void BtnSeek3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeek3ActionPerformed
        penjab.isCek();
        penjab.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        penjab.setLocationRelativeTo(internalFrame1);
        penjab.setAlwaysOnTop(false);
        penjab.setVisible(true);
    }//GEN-LAST:event_BtnSeek3ActionPerformed

    private void BtnSeek3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSeek3KeyPressed
        //Valid.pindah(evt,DTPCari2,TCari);
    }//GEN-LAST:event_BtnSeek3KeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
    }//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        if(TabRawat.getSelectedIndex()==0){
            tampil();
        }
    }//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            tampil();
            this.setCursor(Cursor.getDefaultCursor());
        }else{
            Valid.pindah(evt, TKd, BtnPrint);
        }
    }//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        kdpoli.setText("");
        nmpoli.setText("");
        kdpenjab.setText("");
        nmpenjab.setText("");
        kddokter.setText("");
        nmdokter.setText("");
        status="";
        if(TabRawat.getSelectedIndex()==0){
            tampil();
        }
    }//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnAllActionPerformed(null);
        }
    }//GEN-LAST:event_BtnAllKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==0){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void kddokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kddokterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_kddokterKeyPressed

    private void BtnSeek4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSeek4ActionPerformed
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setAlwaysOnTop(false);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnSeek4ActionPerformed

    private void BtnSeek4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSeek4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnSeek4KeyPressed

    private void ppTampilkanBaruBtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppTampilkanBaruBtnPrintActionPerformed
        status="Baru";
        BtnCariActionPerformed(null);
    }//GEN-LAST:event_ppTampilkanBaruBtnPrintActionPerformed

    private void ppTampilkanLamaBtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ppTampilkanLamaBtnPrintActionPerformed
        status="Lama";
        BtnCariActionPerformed(null);
    }//GEN-LAST:event_ppTampilkanLamaBtnPrintActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            DlgDashboardPendaftarOnline dialog = new DlgDashboardPendaftarOnline(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnCari;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSeek2;
    private widget.Button BtnSeek3;
    private widget.Button BtnSeek4;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TKd;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal Tgl1;
    private widget.Tanggal Tgl2;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.Label jLabel6;
    private widget.Label jLabel8;
    private javax.swing.JPopupMenu jPopupMenu1;
    private widget.TextBox kddokter;
    private widget.TextBox kdpenjab;
    private widget.TextBox kdpoli;
    private widget.Label label11;
    private widget.Label label17;
    private widget.Label label18;
    private widget.Label label19;
    private widget.Label label20;
    private widget.TextBox nmdokter;
    private widget.TextBox nmpenjab;
    private widget.TextBox nmpoli;
    private widget.panelisi panelGlass5;
    private widget.panelisi panelisi4;
    private javax.swing.JMenuItem ppTampilkanBaru;
    private javax.swing.JMenuItem ppTampilkanLama;
    private widget.Table table;
    // End of variables declaration//GEN-END:variables

    public void tampil(){  // TAB MODE 1      
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            Valid.tabelKosong(tabmode); 
            if(TCari.getText().toString().trim().equals("")){
                ps=koneksi.prepareStatement(
                "select reg_periksa.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,reg_periksa.tgl_registrasi,ifnull(booking_registrasi.tanggal_periksa,'') as tanggal_periksa,reg_periksa.kd_poli,poliklinik.nm_poli,reg_periksa.kd_dokter,dokter.nm_dokter,penjab.png_jawab,COALESCE(CONCAT(CASE when booking_registrasi.limit_reg='1' then 'Booking Online' else '' end,CASE when booking_registrasi.limit_reg='0' then 'Booking Offline' else '' end, Case when referensi_mobilejkn_bpjs.no_rawat IS NOT NULL and referensi_mobilejkn_bpjs.tanggalperiksa=reg_periksa.tgl_registrasi then 'MJKN' else '' end),'Onsite') AS cara_daftar  " +
                ",reg_periksa.jam_reg, "+
                "(SELECT DATE_FORMAT(tanggal,'%H:%i:%s')FROM penilaian_awal_keperawatan_ralan WHERE penilaian_awal_keperawatan_ralan.no_rawat = reg_periksa.no_rawat ) AS waktu_askep," +
                "(SELECT jam_rawat FROM pemeriksaan_ralan WHERE pemeriksaan_ralan.no_rawat = reg_periksa.no_rawat ORDER BY tgl_perawatan ASC, jam_rawat ASC LIMIT 1) AS jam_rawat," +
                "(SELECT jam FROM nota_jalan WHERE nota_jalan.no_rawat = reg_periksa.no_rawat ) AS jam_kasir,"+ 
                "CONCAT(reg_periksa.umurdaftar,' ',reg_periksa.sttsumur) as umur,pasien.jk,pasien.pekerjaan,pasien.pnd "+
                "from reg_periksa left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis " +
                "inner join dokter on dokter.kd_dokter = reg_periksa.kd_dokter " +
                "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli " +
                "inner join penjab on reg_periksa.kd_pj = penjab.kd_pj "+
                "left join booking_registrasi on booking_registrasi.no_rkm_medis = reg_periksa.no_rkm_medis and booking_registrasi.tanggal_periksa = reg_periksa.tgl_registrasi " +
                "left join referensi_mobilejkn_bpjs on referensi_mobilejkn_bpjs.no_rawat =reg_periksa.no_rawat "+
                "where reg_periksa.tgl_registrasi between ? and ? and reg_periksa.stts !='Batal' AND reg_periksa.kd_poli NOT IN ('IGDK', 'LAB', 'U0040', 'U0043', 'MCU', 'KGZ', 'KTB', 'KPITC', 'KBM','U0041','RAD','PAK','U0043','U0044','U0045','U0046','U0047','ASMA') " +
                "order by reg_periksa.no_rawat ASC ");
            } else {
                ps=koneksi.prepareStatement(
                "select reg_periksa.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,reg_periksa.tgl_registrasi,ifnull(booking_registrasi.tanggal_periksa,'') as tanggal_periksa,reg_periksa.kd_poli,poliklinik.nm_poli,reg_periksa.kd_dokter,dokter.nm_dokter,penjab.png_jawab,COALESCE(CONCAT(CASE when booking_registrasi.limit_reg='1' then 'Booking Online' else '' end,CASE when booking_registrasi.limit_reg='0' then 'Booking Offline' else '' end, Case when referensi_mobilejkn_bpjs.no_rawat IS NOT NULL and referensi_mobilejkn_bpjs.tanggalperiksa=reg_periksa.tgl_registrasi then 'MJKN' else '' end),'Onsite') AS cara_daftar ,reg_periksa.jam_reg,"  +
                "(SELECT DATE_FORMAT(tanggal,'%H:%i:%s')FROM penilaian_awal_keperawatan_ralan WHERE penilaian_awal_keperawatan_ralan.no_rawat = reg_periksa.no_rawat ) AS waktu_askep," +
                "(SELECT jam_rawat FROM pemeriksaan_ralan WHERE pemeriksaan_ralan.no_rawat = reg_periksa.no_rawat ORDER BY tgl_perawatan ASC, jam_rawat ASC LIMIT 1) AS jam_rawat," +
                "(SELECT jam FROM nota_jalan WHERE nota_jalan.no_rawat = reg_periksa.no_rawat ) AS jam_kasir,"+        
                "CONCAT(reg_periksa.umurdaftar,' ',reg_periksa.sttsumur) as umur,pasien.jk,pasien.pekerjaan,pasien.pnd "+
                "from reg_periksa left join pasien on pasien.no_rkm_medis = reg_periksa.no_rkm_medis " +
                "inner join dokter on dokter.kd_dokter = reg_periksa.kd_dokter " +
                "inner join poliklinik on poliklinik.kd_poli = reg_periksa.kd_poli " +
                "inner join penjab on reg_periksa.kd_pj = penjab.kd_pj " +
                "left join booking_registrasi on booking_registrasi.no_rkm_medis = reg_periksa.no_rkm_medis and booking_registrasi.tanggal_periksa = reg_periksa.tgl_registrasi " +
                "left join referensi_mobilejkn_bpjs on referensi_mobilejkn_bpjs.no_rawat =reg_periksa.no_rawat " +
                "where reg_periksa.tgl_registrasi between ? and ?  and reg_periksa.stts !='Batal' AND reg_periksa.kd_poli NOT IN ('IGDK', 'LAB', 'U0040', 'U0043', 'MCU', 'KGZ', 'KTB', 'KPITC', 'KBM','U0041') and " +
                "(pasien.nm_pasien like ? or poliklinik.nm_poli like ? or reg_periksa.no_rawat like ? or reg_periksa.no_rkm_medis like ? ) "+
                "order by reg_periksa.no_rawat ASC");
            }
                
            try {
                if(TCari.getText().trim().equals("")){
                ps.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                } else {
                ps.setString(1,Valid.SetTgl(Tgl1.getSelectedItem()+""));
                ps.setString(2,Valid.SetTgl(Tgl2.getSelectedItem()+""));
                ps.setString(3,"%"+TCari.getText().trim()+"%");
                ps.setString(4,"%"+TCari.getText().trim()+"%");
                ps.setString(5,"%"+TCari.getText().trim()+"%");
                ps.setString(6,"%"+TCari.getText().trim()+"%");
                }
                rs=ps.executeQuery();
                
                i=1;jum_pasien=0;jum_booking_onlinemjkn=0;
                jum_booking=0;jum_tidak_booking=0;jum_booking_apm=0;jum_booking_fo=0;jum_booking_mjkn=0;
                while(rs.next()){
                    if(rs.getString("cara_daftar").equals("MJKN")){
                        jum_booking_mjkn++;
                    }if(rs.getString("cara_daftar").equals("Booking Online")) {
                        jum_booking_apm++;
                    }if(rs.getString("cara_daftar").equals("Booking Offline")) {
                        jum_booking_fo++;
                    }if(rs.getString("cara_daftar").isEmpty()) {
                        jum_tidak_booking++;
                    }if( !(rs.getString("cara_daftar").isEmpty())) {
                        jum_booking++;
                    }if(rs.getString("cara_daftar").equals("Booking OnlineMJKN")) {
                        jum_booking_mjkn++;
                    }if(rs.getString("cara_daftar").equals("Booking OfflineMJKN")) {
                        jum_booking_mjkn++;
                    }
                    
                    
                    tabmode.addRow(new String[]{
                        i+"",
                        rs.getString("no_rawat"),
                        rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),
                        rs.getString("tgl_registrasi"),
                        rs.getString("tanggal_periksa"),
                        rs.getString("nm_poli"),
                        rs.getString("nm_dokter"),
                        rs.getString("png_jawab"),
                        rs.getString("cara_daftar"),
                        rs.getString("jam_reg"),
                        rs.getString("waktu_askep"),
                        rs.getString("jam_rawat"),
                        rs.getString("jam_kasir"),
                        rs.getString("umur"),
                        rs.getString("jk"),
                        rs.getString("pekerjaan"),
                        rs.getString("pnd")
                     
                    });
                    i++;
                    jum_pasien++;
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            if(rs!=null){
                per_tidak_booking=0;
                per_booking=0;
                per_apm=0;
                per_fo=0;
                per_mjkn=0;
                per_onlinemjkn=0;
                per_non_offline=0;
                per_with_offline=0;
                
 
                per_booking = (jum_booking/jum_pasien)*100 ;
                per_tidak_booking = (jum_tidak_booking/jum_pasien)*100 ;
                
                per_apm =(jum_booking_apm/jum_pasien)*100 ;
                per_fo = (jum_booking_fo/jum_pasien)*100 ;
                per_mjkn = (jum_booking_mjkn/jum_pasien)*100;
                
                per_with_offline = (jum_booking_mjkn+jum_booking_onlinemjkn+jum_booking_apm+jum_booking_fo)/jum_pasien*100 ;
                per_non_offline = (jum_booking_mjkn+jum_booking_onlinemjkn+jum_booking_apm)/jum_pasien*100 ;
                
                tabmode.addRow(new String[]{
                    "","","","","","","","","","","","","","","","","","","","",""
                }); 
                tabmode.addRow(new String[]{
                    "","Jumlah Pasien",": "+Valid.SetAngka2(jum_pasien),"","","","",""
                });  
                tabmode.addRow(new String[]{
                    "","","","Pasien Booking",": "+Valid.SetAngka2(jum_booking),"",""+Valid.SetAngka6(per_booking)+" %","","","",""
                });
                tabmode.addRow(new String[]{
                    "","","","Pasien Onsite",": "+Valid.SetAngka2(jum_tidak_booking),"",""+Valid.SetAngka6(per_tidak_booking)+" %","","","",""
                });
                tabmode.addRow(new String[]{
                    "","","","","","","","","","","","","","","","","","","","",""
                }); 
                tabmode.addRow(new String[]{
                    "","","","Pasien Booking Online",": "+Valid.SetAngka2(jum_booking_apm),"",""+Valid.SetAngka6(per_apm)+" %","","","",""
                });
                tabmode.addRow(new String[]{
                    "","","","Pasien Booking Offline",": "+Valid.SetAngka2(jum_booking_fo),"",""+Valid.SetAngka6(per_fo)+" %","","","",""
                });
                tabmode.addRow(new String[]{
                    "","","","Pasien Booking MJKN",": "+Valid.SetAngka2(jum_booking_mjkn),"",""+Valid.SetAngka6(per_mjkn)+" %","","","",""
                });
                tabmode.addRow(new String[]{
                    "","","","","","","","","","","","","","","","","","","","",""
                }); 
                tabmode.addRow(new String[]{
                    "","Persentase Booking(Online & Offline + MJKN)",": "+Valid.SetAngka6(per_with_offline)+" %","Persentase Booking (Online + MJKN)",":" + Valid.SetAngka6(per_non_offline)+" %","","","","","","","","","","","","","","","",""
                }); 
            }            
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        } 
        this.setCursor(Cursor.getDefaultCursor());
    }

}
